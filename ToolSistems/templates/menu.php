<?php 
if ( !ToolSistems::isDevIp() ) ToolSistems::all_bad();
$google_page_insights_url = 'https://developers.google.com/speed/pagespeed/insights/?tab=desktop&url=';
?>

<style>
<?php include(dirname(__FILE__) . '/css/menu.css'); ?>
</style>

<!-- TOOLSISTEMS SIDENAV 1 -->
<div id='ToolSistems-Sidenav' class='ToolSistems-sidenav'>

	<div id='ToolSistems-server-info'>
		<span><b>versi&oacute;: </b><?php echo ToolSistems::get_version(); ?></span>
		<span><b>versi&oacute; source: </b><?php echo ToolSistems::get_version_source(); ?></span>
	</div>
	
	<div style="clear:both; margin-bottom: 20px;"></div>
	
	<div id='ToolSistems-server-info'>
	</div>
	
	<span class="ToolSistems-titol">
		ToolSistems
		<a href='javascript:void(0)' class='ToolSistems-closebtn' onclick='ToolSistems_closeNav()'>x</a>
	</span>
	
	<div style='clear:both;'></div>
	
	<div class="ToolSistems-elements">
		<a href='http://proves3.6tems.es/developers/' target='_blank'>6TEMS Developers</a>
		<a href='http://eines.6temscomunicacio.com/' target='_blank'>6TEMS Eines</a>
		<a href='http://eines.6temscomunicacio.com/system/?sec=inici' target='_blank'>6TEMS Servidors</a>
		<a href='<?php echo _JIRA_URL_; ?>' target='_blank'>JIRA</a>
		<a href='<?php echo $canvi_domini_url; ?>' target='_blank'>Canvi de domini</a>
		<a href='<?php echo $url_consola; ?>' target='_blank'>Consola</a>
		<a href='<?php echo $url_emergency; ?>' target='_blank'>WP-Emergency</a>
		<a id="ToolSistems-GooglePageInsights" href='javascript:void(0);' onclick="ToolSistems_toggleSubMenu(this);">
			PageSpeed Insights
		</a>
		<div id="ToolSistems-GooglePageInsights-submenu" class="ToolSistems-submenu">
			<a href='<?php echo $google_page_insights_url . urlencode(ToolSistems::getDomain()); ?>' target='_blank'>Home</a>
			<a href='<?php echo $google_page_insights_url . urlencode(ToolSistems::getUrl()); ?>' target='_blank'>P&agrave;gina Actual</a>
		</div>
	</div>
	
</div>
<!-- /TOOLSISTEMS SIDENAV 1 -->

<!-- TOOLSISTEMS SIDENAV 2 -->
<div id='ToolSistems-Sidenav-2' class='ToolSistems-sidenav'>
	
	<div style="clear:both; margin-bottom: 40px;"></div>

	<span class="ToolSistems-titol">
		Servidor
		<a href='javascript:void(0)' class='ToolSistems-closebtn' onclick='ToolSistems_closeNav_2()'>x</a>
	</span>
	
	<div style="clear:both; margin-bottom: 10px;"></div>
	
	<div id='ToolSistems-server-info'>
		<span><b>CMS: </b><?php echo ToolSistems::checkCMS(); ?></span>
		<span>
			<a href="<?php echo ToolSistems::getPleskUrl(); ?>" target="_blank" style="display: inline-block;">
				<?php echo ToolSistems::getPleskHostname(); ?>
			</a> - <?php echo ToolSistems::get_system_ip(); ?>
		</span>
		<span>
			<a id="ToolSistems-modal-btn" href="javascript:void(0);" style="display: inline-block; font-weight: bold;">Connexions: </a>
			<?php echo ToolSistems::get_system_http_connections(); ?>
		</span>
		<span><b>Uptime: </b><?php echo ToolSistems::get_system_uptime(); ?></span>
	<?php /*
		<span>
			<b>Disc: </b>
			<table border=0 width='100%'>
				<tr><td>Filesystem</td><td>Total</td><td>Used</td><td>Free</td><td>Used</td></tr>
			<?php foreach ( ToolSistems::get_system_disk_usage() as $line ) { ?>
				<tr>
					<td><?php echo $line['Filesystem'];?></td>
					<td><?php echo $line['Size'];?></td>
					<td><?php echo $line['Used'];?></td>
					<td><?php echo $line['Avail'];?></td>
					<td><?php echo $line['Used%'];?></td>
				</tr>
			<?php } ?>
			</table>
		</span>
		<span>
			<b>Mem&ograve;ria: </b>
			<table border=0 width='100%'>
				<tr><td>&nbsp;</td><td>Total</td><td>Used</td><td>Free</td></tr>
			<?php foreach ( ToolSistems::get_system_memory_usage() as $line ) { ?>
				<tr>
					<td><?php echo $line['Name'];?></td>
					<td><?php echo ToolSistems::getSymbolByQuantity($line['Total']);?></td>
					<td><?php echo ToolSistems::getSymbolByQuantity($line['Used']);?></td>
					<td><?php echo ToolSistems::getSymbolByQuantity($line['Free']);?></td>
				</tr>
			<?php } ?>
			</table>
		</span>
	*/ ?>
	
	</div>
	
</div>
<!-- /TOOLSISTEMS SIDENAV 2 -->

<!-- TOOLSISTEMS SIDENAV DEBUG -->
<div id='ToolSistems-Sidenav-debug' class='ToolSistems-sidenav'>
	<?php
		$debug_array = array(
			ToolSistems::getDomain(),
			urlencode(ToolSistems::getDomain()),
		);
		//ToolSistems::show( $debug_array, false );
	?>
</div>
<!-- /TOOLSISTEMS SIDENAV DEBUG -->

<!-- TOOLSISTEMS MODAL -->
<div id="ToolSistems-modal" class="ToolSistems-modal">
	<div class="ToolSistems-modal-content">
		<span class="ToolSistems-modal-close">&times;</span>
		
		<p style="font-size: 28px; margin-bottom: 20px;"><b>Connexions (ip &uacute;niques)</b><p>
	
	<?php foreach( ToolSistems::get_system_http_connections(false) as $ip_client => $contador_connexio ) { ?>
		<p style="font-weight: normal;">
			<?php echo $ip_client; ?> 
			<?php echo $ip_client == ToolSistems::getServerVar('REMOTE_ADDR') ? '<b>(tu)</b>' : ''; ?>
			: <?php echo $contador_connexio; ?> <?php echo $contador_connexio == 1 ? 'petici&oacute;' : 'peticions'; ?>
		</p>
	<?php } ?>
	
	</div>
</div>
<!-- /TOOLSISTEMS MODAL -->

<script type="text/javascript">
document.addEventListener("keyup", function(e){
	// ALT + 6
	if (e.altKey && e.keyCode == 54)
	{
		if ( parseInt(document.getElementById("ToolSistems-Sidenav").style.width) > 0 )
			ToolSistems_closeNav();
		else
			ToolSistems_openNav();
	}
	
	// ALT + 7
	if (e.altKey && e.keyCode == 55)
	{
		if ( parseInt(document.getElementById("ToolSistems-Sidenav-2").style.width) > 0 )
			ToolSistems_closeNav_2();
		else
			ToolSistems_openNav_2();
	}
	
	// ALT + 5
	if (e.altKey && e.keyCode == 53)
	{
		if ( parseInt(document.getElementById("ToolSistems-Sidenav-debug").style.height.replace('%','')) > 0 )
			ToolSistems_closeNav_debug();
		else
			ToolSistems_openNav_debug();
	}
}, false);
function ToolSistems_openNav()
{ 
	document.getElementById("ToolSistems-Sidenav").style.width = "250px";
	//if ( document.getElementById("ToolSistems-Sidenav-2").innerHTML.trim() != '' ) document.getElementById("ToolSistems-Sidenav-2").style.width = "250px";
}
function ToolSistems_closeNav()
{
	document.getElementById("ToolSistems-Sidenav").style.width = "0";
	//document.getElementById("ToolSistems-Sidenav-2").style.width = "0";
}
function ToolSistems_openNav_2()
{ 
	document.getElementById("ToolSistems-Sidenav-2").style.width = "250px";
}
function ToolSistems_closeNav_2()
{
	document.getElementById("ToolSistems-Sidenav-2").style.width = "0";
}
function ToolSistems_openNav_debug()
{
	if ( document.getElementById("ToolSistems-Sidenav-debug").innerHTML.trim() != '' )
		document.getElementById("ToolSistems-Sidenav-debug").style.height = "100%";
}
function ToolSistems_closeNav_debug()
{
	document.getElementById("ToolSistems-Sidenav-debug").style.height = "0";
}

function ToolSistems_toggleSubMenu(pare)
{
	if ( document.getElementById(pare.id + "-submenu").style.height == 'auto' )
		document.getElementById(pare.id + "-submenu").style.height = "0";
	else
		document.getElementById(pare.id + "-submenu").style.height = "auto";
}

/* MODAL */
var modal_ToolSistems = document.getElementById('ToolSistems-modal');
var btn_ToolSistems = document.getElementById("ToolSistems-modal-btn");
var span_ToolSistems = document.getElementsByClassName("ToolSistems-modal-close")[0];

btn_ToolSistems.onclick = function() { modal_ToolSistems.style.display = "block"; }

span_ToolSistems.onclick = function() { modal_ToolSistems.style.display = "none"; }

window.onclick = function(event) {
    if (event.target == modal_ToolSistems) modal_ToolSistems.style.display = "none";
}
/* /MODAL */
</script>