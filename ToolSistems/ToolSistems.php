<?php

require_once('toolsistems-config.php');

class ToolSistems
{
	
	public static function getServerVar($key) { return isset($_SERVER[$key]) ? $_SERVER[$key] : ''; }
	public static function get_version() { return file_get_contents(dirname(__FILE__) . '/version'); }
	public static function get_version_source() {
		$url = _6TEMS_DEV_SOURCE_ . '/version';
		$context = stream_context_create([
			"http" => [
				"header" => "Authorization: Basic " . _6TEMS_DEV_SOURCE_AUTH_
			]
		]);
		$result = file_get_contents($url, false, $context );
		return $result;
	}
	
	/* ToolSistemsMessages */
	public static function all_good($die = true) { ToolSistemsMessages::all_good($die); }
	public static function all_bad($die = true) { ToolSistemsMessages::all_bad($die); }
	public static function showDump($objeto, $die = true) { ToolSistemsMessages::showDump($objeto, $die); }
	public static function show($objeto, $die = true) { ToolSistemsMessages::show($objeto, $die); }
	public static function showPub($objeto, $die = true) { ToolSistemsMessages::showPub($objeto, $die); }
	public static function showpol($objeto, $die = true) { ToolSistemsMessages::showpol($objeto, $die); }
	public static function wp_mail( $to, $subject, $output ) { ToolSistemsMessages::wp_mail( $to, $subject, $output ); }
	/* ToolSistemsUrls */
	public static function getHost() { return ToolSistemsUrls::getHost(); }
	public static function getUrl() { return ToolSistemsUrls::getUrl(); }
	public static function getSeccions() { return ToolSistemsUrls::getSeccions(); }
	public static function getUltimaSeccio() { return ToolSistemsUrls::getUltimaSeccio(); }
	public static function getSeccio() { return ToolSistemsUrls::getSeccio(); }
	public static function getDomain() { return ToolSistemsUrls::getDomain(); }
	public static function isMobil() { return ToolSistemsUrls::isMobil(); }
	/* ToolSistemsErrors */
	public static function showErrors($active, $isDev = true) { ToolSistemsErrors::showErrors($active, $isDev); }
	/* ToolSistemsButton */
	public static function isNotAdmin() { ToolSistemsButton::isNotAdmin(); }
	public static function show6TEMSButton($active = false) { ToolSistemsButton::show6TEMSButton($active); }
	public static function put_on_debug($output, $label = '') { ToolSistemsButton::put_on_debug($output, $label); }
	public static function get_show_message_format($output, $label = '') { ToolSistemsButton::get_show_message_format($output, $label); }
	/* ToolSistemsIps */
	public static function isDevIp() { return ToolSistemsIps::isDevIp(); }
	public static function isPol() { return ToolSistemsIps::isPol(); }
	public static function isLocalhost() { return ToolSistemsIps::isLocalhost(); }
	public static function isAjax() { return ToolSistemsIps::isAjax(); }	
	public static function isCli() { return ToolSistemsIps::isCli(); }
	public static function isMobile() { return ToolSistemsIps::isMobile(); }
	/* ToolSistemsVaris */
	public static function contains($param, $search) { return ToolSistemsVaris::contains($param, $search); }
	public static function validateDate($date, $format = 'Y-m-d H:i:s') { return ToolSistemsVaris::validateDate($date, $format); }
	public static function isBetweenDates($from, $to, $format = 'Y-m-d H:i:s') { return ToolSistemsVaris::isBetweenDates($from, $to, $format); }
	public static function crearCookie($cookie_name = null, $cookie_value = null, $days = 1) { return ToolSistemsVaris::crearCookie($cookie_name, $cookie_value, $days); }
	public static function remoteFileExists($url) { return ToolSistemsVaris::remoteFileExists($url); }
	public static function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80) { return ToolSistemsVaris::resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality); }
	public static function get_img_src_data($img_data) { return ToolSistemsVaris::get_img_src_data($img_data); }
	public static function formatear_fecha($fecha, $fromFormat, $toFormat) { return ToolSistemsVaris::formatear_fecha($fecha, $fromFormat, $toFormat); }
	public static function like_match($pattern, $subject) { return ToolSistemsVaris::like_match($pattern, $subject); }
	public static function random_string($length = 32, $with_numbers = false) { return ToolSistemsVaris::random_string($length, $with_numbers); }
	public static function array_search_for($array, $camp, $valor, $propietat = null) { return ToolSistemsVaris::array_search_for($array, $camp, $valor, $propietat); }
	/* ToolSistemsServer */
	public static function get_system_http_connections($count = true) { return ToolSistemsServer::get_system_http_connections($count); }
	public static function get_system_ip() { return ToolSistemsServer::get_system_ip(); }
	public static function getSymbolByQuantity($bytes) { return ToolSistemsServer::getSymbolByQuantity($bytes); }
	public static function getPleskHostname() { return ToolSistemsServer::getPleskHostname(); }
	public static function getPleskUrl() { return ToolSistemsServer::getPleskUrl(); }
	public static function get_system_disk_usage() { return ToolSistemsServer::get_system_disk_usage(); }
	public static function get_system_uptime() { return ToolSistemsServer::get_system_uptime(); }
	public static function get_system_memory_usage() { return ToolSistemsServer::get_system_memory_usage(); }
	/* ToolSistemsCMS */
	public static function checkCMS() { return ToolSistemsCMS::checkCMS(); }
	
}

?>