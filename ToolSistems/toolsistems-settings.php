<?php

if (!defined('_6TEMS_DEV_IPS_'))
	define('_6TEMS_DEV_IPS_', '91.126.218.219#80.64.37.127#192.168.10.1'); /* 6TEMS DEVELOPMENT IPS */

if (!defined('_6TEMS_DEV_SOURCE_'))
	define('_6TEMS_DEV_SOURCE_', 'http://proves3.6tems.es/developers/ToolSistems');

if (!defined('_6TEMS_DEV_SOURCE_AUTH_'))
	define('_6TEMS_DEV_SOURCE_AUTH_', 'c3Vwb3J0OnIwbjRsZDFuMA==');

if ( empty($currentDir) ) $currentDir = dirname(__FILE__);
if (!defined('_TOOLSISTEMS_ABS_PATH_'))	define('_TOOLSISTEMS_ABS_PATH_', $currentDir);

/* INCLUYE TODOS LOS PHP DEL extend-functions */
$extend_functions_dir = dirname(__FILE__) . '/extend-functions/';
foreach ( scandir($extend_functions_dir) as $file_name ) {
	$file_name_explode = array_reverse(explode('.', $file_name));
	if ( $file_name_explode[0] == 'php' )
		include($extend_functions_dir . $file_name);
}
/* /INCLUYE TODOS LOS PHP DEL extend-functions */

?>