<?php

/*
 * 	autor: pol.gomez
 * 	versió: 20180307b
 */
 
class ToolSistemsErrors
{

	public static function showErrors($active, $isDev = true)
	{
		if ( !$isDev ) self::activeErrors($active);
		else if ( $isDev && ToolSistems::isDevIp() ) self::activeErrors($active);
		else self::activeErrors(false);
	}
	
	public static function activeErrors($active)
	{
		if ($active) {
			ini_set('display_errors', 1);
			ini_set('display_startup_errors', 1);
			error_reporting(E_ALL);
		}
		else {
			error_reporting(0);
			ini_set('log_errors','On');
			ini_set('display_errors','Off');
			ini_set('error_reporting', E_ALL );
		}
	}
	
}