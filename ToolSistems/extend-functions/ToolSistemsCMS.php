<?php

/*
 * 	autor: pol.gomez
 * 	versió: 20180511a
 */
 
class ToolSistemsCMS
{
	public static function checkCMS()
	{
		if ( $res = self::isWordpress() ) return $res;
		if ( $res = self::isPrestashop() ) return $res;
		if ( $res = self::isEndo() ) return $res;
		if ( $res = self::isYii() ) return $res;
		if ( $res = self::isSymfony() ) return $res;
		
		return 'Unknown';
	}
	
	public static function isWordpress()
	{
		if ( !file_exists(_TOOLSISTEMS_ABS_PATH_ . '/wp-config.php') ) return false;
		
		global $wp_version;
				
		return 'Wordpress ' . $wp_version;
	}
	
	public static function isPrestashop()
	{
		if ( !file_exists(_TOOLSISTEMS_ABS_PATH_ . '/config/config.inc.php') ) return false;
		return !defined('_PS_VERSION_') ? 'Prestashop' : 'Prestashop ' . _PS_VERSION_;
	}
	
	public static function isEndo()
	{
		if ( !file_exists(_TOOLSISTEMS_ABS_PATH_ . '/admin/core/config.inc.php') ) return false;
		
		global $AD_ver;
		
		return 'Endo ' . $AD_ver;
	}
	
	public static function isYii()
	{
		if ( !file_exists(_TOOLSISTEMS_ABS_PATH_ . '/protected/yiic.php') ) return false;
		
		$version = Yii::getVersion();
				
		return 'Yii ' . $version;
	}
	
	public static function isSymfony()
	{
		$path = _TOOLSISTEMS_ABS_PATH_ . '/app.php';
		if ( !file_exists($path) ) return false;
		if ( !ToolSistems::contains('Symfony', file_get_contents($path)) ) return false;
		
		$version = \Symfony\Component\HttpKernel\Kernel::VERSION;
				
		return 'Symfony ' . $version;
	}
}

?>