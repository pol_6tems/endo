<?php

/*
 * 	autor: pol.gomez
 * 	versió: 20180309a
 */
 
class ToolSistemsVaris
{
	
	public static function contains($variable, $string)
	{
        return strpos($variable, $string) !== false;
    }
	
	public static function validateDate($date, $format = 'Y-m-d H:i:s')
	{
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
	
	public static function isBetweenDates($from, $to, $format = 'Y-m-d H:i:s')
	{
		$now = date_create('now')->format($format);
		$from = date_create($from)->format($format);
		$to = date_create($to)->format($format);
		
		return $now >= $from && $now <= $to;
	}
	
	public static function crearCookie($cookie_name = null, $cookie_value = null, $days = 1)
	{
		if ($cookie_name != null && $cookie_value != null) {
			setcookie($cookie_name, $cookie_value, (time() + (86400 * 30)) * $days, "/"); // 86400 = 1 day
		}
	}

	public static function remoteFileExists($url)
	{
		$ret = false;
		
		$curl = curl_init($url);

		//don't fetch the actual page, you only want to check the connection is ok
		curl_setopt($curl, CURLOPT_NOBODY, true);

		//do request
		$result = curl_exec($curl);

		//if request did not fail
		if ($result !== false)
		{
			//if request was ok, check response code
			$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  

			$ret = in_array($statusCode, array(200, 301, 302, 304));
		}

		curl_close($curl);

		return $ret;
	}
	
	public static function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80)
	{
		/* ex: $url_temp_data = ToolSistems::resize_crop_image(1263, 487, $url_temp, NULL); */
		/* FIX PER QUAN XAPEN LA IP */
		/*
		$source_file_uri = str_replace('//', '/',
			str_replace('https://', '',
				str_replace('http://', '',
					str_replace(ToolSistems::getDomain(), ABSPATH, $source_file)
				)
			)
		);
		
		if ( file_exists($source_file_uri) ) $source_file = $source_file_uri;
		*/
		/* /FIX PER QUAN XAPEN LA IP */
		
		$imgsize = getimagesize($source_file);
		
		$width = $imgsize[0];
		$height = $imgsize[1];
		$mime = $imgsize['mime'];
		
		switch($mime)
		{
			case 'image/gif':
				$image_create = "imagecreatefromgif";
				$image = "imagegif";
				break;
	 
			case 'image/png':
				$image_create = "imagecreatefrompng";
				$image = "imagepng";
				$quality = 7;
				break;
			
			case 'image/jpeg':
				$image_create = "imagecreatefromjpeg";
				$image = "imagejpeg";
				$quality = 80;
				break;
	 
			default:
				return false;
				break;
			
		}
		 
		$dst_img = imagecreatetruecolor($max_width, $max_height);
		$src_img = $image_create($source_file);
		
		$width_new = $height * $max_width / $max_height;
		$height_new = $width * $max_height / $max_width;
		
		//if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
		if ( $width_new > $width )
		{
			//cut point by height
			$h_point = (($height - $height_new) / 2);
			//copy image
			imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
		}
		else
		{
			//cut point by width
			$w_point = (($width - $width_new) / 2);
			imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
		}
		 
		//$image($dst_img, $dst_dir, $quality);
	
		ob_start();
		$image($dst_img, NULL, $quality);
		$output = ob_get_contents();
		ob_end_clean();
		
		
		if($dst_img)imagedestroy($dst_img);
		if($src_img)imagedestroy($src_img);
		
		return $output;
	}
	
	public static function get_img_src_data($img_data) { return "data:image/png;base64," . base64_encode($img_data); }
	
	public static function formatear_fecha($fecha, $fromFormat, $toFormat)
	{
		$date = DateTime::createFromFormat($fromFormat, $fecha);
		
		if ( empty($date) ) return $fecha;
		
		return $date->format($toFormat);
	}
	
	public static function like_match($pattern, $subject)
	{
		$pattern = str_replace('%', '.*', preg_quote($pattern, '/'));
		return (bool) preg_match("/^{$pattern}$/i", $subject);
	}
	
	/* RANDOM STRING */
	public static function random_string($length = 32, $with_numbers = false) {
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$numbers = '0123456789';
		if ( $with_numbers ) $characters .= $numbers;
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	/* BUSCAR UN INDEX D'UNA ARRAY MULTIDIMENSIONAL AMB PROPIETAT (OPCIONAL) */
	public static function array_search_for($array, $camp, $valor, $propietat = null) {
	   foreach ($array as $key => $item) {
		   if ( !empty($propietat) ) {
			   $item = is_object($item) ? $item->$propietat : $item[$propietat];
		   }
		   $value_aux = is_object($item) ? $item->$camp : $item[$camp];
		   if ($value_aux == $valor) {
			   return $key;
		   }
	   }
	   return null;
	}

}
?>