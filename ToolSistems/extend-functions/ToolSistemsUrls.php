<?php

/*
 * 	autor: pol.gomez
 * 	versió: 20180309a
 */
 
class ToolSistemsUrls
{
	
	public static function getDomain() { return defined('_6TEMS_DEV_DOMAIN_') ? _6TEMS_DEV_DOMAIN_ : ''; }
	
	public static function getHost()
	{
		$actual_link = '';
		if ( ToolSistems::getServerVar('HTTPS') != '' ) $actual_link .= 'https://';
		else if ( ToolSistems::getServerVar('HTTP') != '' ) $actual_link .= 'http://';
		$actual_link .= ToolSistems::getServerVar('HTTP_HOST');
		return $actual_link;
	}
	
	public static function getUrl()
	{
		$actual_link = self::getHost();
		$actual_link .= ToolSistems::getServerVar('REQUEST_URI');
        return $actual_link;
    }

    public static function getSeccions()
	{
        $url = self::getUrl();
        $url = str_replace(_6TEMS_DEV_DOMAIN_, '', $url);
        $url = str_replace('http://', '', str_replace('https://', '', $url));
        $parts = explode('/', $url);
        $parts_f = array();
        foreach ($parts as $part)
		{
			if ( ToolSistems::contains($part, '?') ) $part = substr($part, 0, strpos($part, '?'));
			if ( ToolSistems::contains($part, '.') ) $part = substr($part, 0, strpos($part, '.'));
            if ($part != '') $parts_f[] = $part;
		}
        return $parts_f;
    }

    public static function getUltimaSeccio()
	{
        $seccions = self::getSeccions();
        return $seccions[(count($seccions) - 1)];
    }

    public static function getSeccio()
	{
        $seccions = self::getSeccions();
        return (count($seccions) > 0 ? $seccions[0] : '');
    }
	
	public static function isMobil()
	{
		$esmobil = false;
		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			$mobile_agents = '!(tablet|pad|mobile|phone|symbian|android|ipod|ios|blackberry|webos)!i';
			if (preg_match($mobile_agents, $_SERVER['HTTP_USER_AGENT'])) {
				$esmobil = true;
			}
		}
		return $esmobil;
	}
	
}

?>