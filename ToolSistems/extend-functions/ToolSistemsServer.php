<?php

/*
 * 	autor: pol.gomez
 * 	versió: 20180510a
 */
 
class ToolSistemsServer
{
	
	public static function get_system_http_connections($count = true)
	{	
		if ( function_exists('exec') )
		{
			$unique = array();
			$llista_ips = array();
			
			$www_unique_count = 0;
			$www_total_count = 0;
			@exec ('netstat -an | egrep \':80|:443\' | awk \'{print $5}\' | grep -v \':::\*\' |  grep -v \'0.0.0.0\'', $results);
			
			foreach ($results as $result)
			{
				$array = explode(':', $result);
				$www_total_count ++;
				
				if (preg_match('/^::/', $result)) {
					$ipaddr = $array[3];
				} else {
					$ipaddr = $array[0];
				}
				
				if (!in_array($ipaddr, $unique)) {
					$unique[] = $ipaddr;
					$llista_ips[$ipaddr] = 1;
					$www_unique_count ++;
				}
				else $llista_ips[$ipaddr]++;
			}
			
			unset ($results);
			
			if ($count)
				return count($unique);
			else
				return $llista_ips;
			
		}
		
	}
		
	public static function get_system_ip()
	{
		return ToolSistems::getServerVar('SERVER_ADDR');
	}
	
	public static function getSymbolByQuantity($size)
	{
		if ( is_string($size) ) return '';
		
		$base = log($size) / log(1024);
		$suffix = array("KB", "MB", "GB", "TB");
		$f_base = floor($base);
		return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
	}
	
	public static function getPleskHostname()
	{
		$host_name = !ToolSistems::contains('dns6tems.com', gethostname()) ? gethostname() . '.dns6tems.com' : gethostname();
		return str_replace('dns6tems.com.dns6tems.com', 'dns6tems.com', $host_name);
	}
	
	public static function getPleskUrl()
	{
		$host_name = self::getPleskHostname();
		
		$port = 8443;
		if ( self::check_port("https://$host_name", 8445) ) $port = 8445;
		
		$url = "https://$host_name:$port";
		return $url;
	}
	
	public static function check_port($ip, $port)
	{
		$fp = @fsockopen($ip, $port, $errno, $errstr, 0.1);
		if (!$fp) return false;
		else
		{
			fclose($fp);
			return true;
		}
	}
	
	public static function get_system_disk_usage()
	{
		$llistat = array();
		
		if ( function_exists('exec') )
		{
			@exec ('df -h', $results);
			
			if ( !empty($results) ) 
			{
				$i = 0;
				foreach ($results as $key => $result)
				{
					if ( $key == 0 ) continue;
					
					$fields_aux = explode(' ', $result);
					$fields = array();
					foreach ( $fields_aux as $field ) if ( $field != '' ) $fields[] = $field;
					
					$llistat[$i] = array(
						'Filesystem' => $fields[0],
						'Size' => $fields[1],
						'Used' => $fields[2],
						'Avail' => $fields[3],
						'Used%' => $fields[4],
						'Mounted' => $fields[5]
					);
					$i++;
				}
			}
		}
		return $llistat;
	}
	
	public static function get_system_uptime()
	{
		$results = array();
		
		if ( function_exists('exec') )
		{
			@exec ('uptime', $results);
		}
		return !empty($results) ? $results[0] : '';
	}
	
	public static function get_system_memory_usage()
	{
		$llistat = array();
		
		if ( function_exists('exec') )
		{
			@exec ('free -t', $results);
			
			if ( !empty($results) ) 
			{
				$i = 0;
				foreach ($results as $key => $result)
				{
					if ( $key == 0 ) continue;
					
					$fields_aux = explode(' ', $result);
					$fields = array();
					foreach ( $fields_aux as $field ) if ( $field != '' ) $fields[] = $field;
					
					$llistat[$i] = array(
						'Name' => $fields[0],
						'Total' => $fields[1],
						'Used' => $fields[2],
						'Free' => $fields[3],
					);
					$i++;
				}
			}
		}
		return $llistat;
	}
}

?>