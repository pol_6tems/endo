<?php

/*
 * 	autor: pol.gomez
 * 	versió: 20180309a
 */
 
class ToolSistemsMessages
{

	public static function all_good($die = true) 
	{
		$content = '
			  ___   _      _       _____ _____  ___________ _ 
			 / _ \ | |    | |     |  __ \  _  ||  _  |  _  \ |
			/ /_\ \| |    | |     | |  \/ | | || | | | | | | |
			|  _  || |    | |     | | __| | | || | | | | | | |
			| | | || |____| |____ | |_\ \ \_/ /\ \_/ / |/ /|_|
			\_| |_/\_____/\_____/  \____/\___/  \___/|___/ (_)
		';
		self::showPub($content, $die);
	}

	public static function all_bad($die = true) 
	{
		$content = '		
			  ___                     ______           _          _ 
			 / _ \                    |  _  \         (_)        | |
			/ /_\ \ ___ ___ ___  ___  | | | |___ _ __  _  ___  __| |
			|  _  |/ __/ __/ _ \/ __| | | | / _ \ \'_ \| |/ _ \/ _` |
			| | | | (_| (_|  __/\__ \ | |/ /  __/ | | | |  __/ (_| |
			\_| |_/\___\___\___||___/ |___/ \___|_| |_|_|\___|\__,_|
		';
		self::showPub($content, $die);
	}
	
	public static function showDump($objeto, $die = true) 
	{
		if (ToolSistems::isDevIp()) var_dump($objeto);
		if ($die) die();
    }

    public static function show($objeto, $die = false) 
	{
		if (ToolSistems::isDevIp()) {
            echo "<div style='position: relative; z-index: 9999; display: block !important;'>";
            echo "Dev Output:<br/>";
            echo "<pre style='display: block !important;padding: 9.5px;margin: 0 0 10px;font-size: 13px;line-height: 1.42857143;color: #333;word-break: break-all;word-wrap: break-word;background-color: #f5f5f5;border: 1px solid #ccc;border-radius: 4px;'>";
            print_r($objeto);
            echo "</pre>";
            echo "</div>";
			
            if ($die) die();
        }
    }
	
	public static function showPub($objeto, $die = false) 
	{
		echo "<div style='position: relative; z-index: 9999; display: block !important;'>";
		echo "<pre style='display: block !important;padding: 9.5px;margin: 0 0 10px;font-size: 13px;line-height: 1.42857143;color: #333;word-break: break-all;word-wrap: break-word;background-color: #f5f5f5;border: 1px solid #ccc;border-radius: 4px;'>";
		print_r($objeto);
		echo "</pre>";
		echo "</div>";
			
		if ($die) die();
    }
	
	public static function showpol($objeto, $die = true) {
        if (ToolSistems::isPol()) self::show($objeto, $die);
    }
	
	public static function wp_mail( $to, $subject, $output ) {
		if ( function_exists('wp_mail') && ToolSistems::isDevIp() ) {
			wp_mail( $to, '[ToolSistems Debug] ' . $subject, print_r($output, true) );
		}
	}
	
}

?>