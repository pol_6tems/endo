<?php

/*
 * 	autor: pol.gomez
 * 	versió: 20180309a
 */

class ToolSistemsIps
{
	public static function getDevIps() { return explode('#', _6TEMS_DEV_IPS_); }
	
	public static function isDevIp()
	{
		if ( self::isLocalhost() || self::isCli() ) return true;
        
		$result = false;
		
		if ( defined('_6TEMS_DEV_IPS_') ) $result = in_array(ToolSistems::getServerVar('REMOTE_ADDR'), self::getDevIps());

        return $result;
    }
	
	public static function isPol() { return self::isDevIp() && strpos(ToolSistems::getServerVar('HTTP_USER_AGENT'), 'Vivaldi') !== false; }
	
	public static function isLocalhost() { return in_array( ToolSistems::getServerVar('REMOTE_ADDR'), array( '127.0.0.1', '::1' ) ); }
	
	public static function isCli() { return php_sapi_name() == 'cli'; }
	
	public static function isAjax()
	{
		$isAjax = strtolower(ToolSistems::getServerVar('HTTP_X_REQUESTED_WITH')) == 'xmlhttprequest';
		$header_content_type = ToolSistems::contains(ToolSistems::getServerVar('CONTENT_TYPE'), 'multipart/form-data');
		
		return $isAjax || $header_content_type;
	}
	
	public static function isMobile()
	{
		$esmobil = false;
		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			$mobile_agents = '!(tablet|pad|mobile|phone|symbian|android|ipod|ios|blackberry|webos)!i';
			if (preg_match($mobile_agents, $_SERVER['HTTP_USER_AGENT'])) {
				$esmobil = true;
			}
		}
		return $esmobil;
	}
	
}


?>