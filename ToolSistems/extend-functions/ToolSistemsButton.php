<?php

/*
 * 	autor: pol.gomez
 * 	versió: 20180309a
 */
 
class ToolSistemsButton
{
	
	public static function isNotAdmin()
	{
		$seccio = ToolSistems::getSeccio();
		$avoid_pages = defined('_PAGES_AVOID_6TEMS_BUTTON_') ? explode('#', _PAGES_AVOID_6TEMS_BUTTON_) : array();
		return !in_array( $seccio, array_merge(array('wp-login', 'wp-admin', 'editor', 'edicio', 'admin', 'panel'), $avoid_pages) );
	}

	public static function show6TEMSButton($active = false)
	{
		if ( ToolSistems::isDevIp() && !ToolSistems::isAjax() && $active && self::isNotAdmin() && !ToolSistems::isCli() )
		{
			// Canvi Domini
			$canvi_domini_url = _6TEMS_DEV_DOMAIN_ . '/ToolSistems/canvi-domini.php';
			if ( !ToolSistems::contains($canvi_domini_url, 'http') ) $canvi_domini_url = 'http://' . $canvi_domini_url;
			// WP-Emergency
			$url_emergency = _6TEMS_DEV_DOMAIN_ . '/ToolSistems/wp-emergency.php';
			if ( !ToolSistems::contains($url_emergency, 'http') ) $url_emergency = 'http://' . $url_emergency;
			// Consola
			$url_consola = _6TEMS_DEV_DOMAIN_ . '/ToolSistems/consola.php';
			if ( !ToolSistems::contains($url_consola, 'http') ) $url_consola = 'http://' . $url_consola;
			
			include(str_replace('extend-functions', 'templates', dirname(__FILE__)) . '/menu.php');
		}
	}
	
	public static function put_on_debug($output, $label)
	{
		if ( ToolSistems::isDevIp() && !ToolSistems::isAjax() && self::isNotAdmin() && !ToolSistems::isCli() )
		{
			echo '
			<script type="text/javascript">
				window.addEventListener("load",function(event) {
					var debug_output = `' . self::get_show_message_format($output, $label) . '`;
					var debug_window = document.getElementById("ToolSistems-Sidenav-debug");
					debug_window.innerHTML += debug_output;
				},false);
			</script>
			';
		}
	}
	
	public static function get_show_message_format($output, $label)
	{
		$msg = '<br/>';
		$msg .= "<pre style='display: block !important;padding: 9.5px;margin: 0 0 10px;font-size: 13px;line-height: 1.42857143;color: #333;word-break: break-all;word-wrap: break-word;background-color: #f5f5f5;border: 1px solid #ccc;border-radius: 4px;'>";
		$msg .= !empty($label) ? '### DEBUG: ' . $label . ' ###<br/>' : '';
        $msg .= print_r($output, true);
        $msg .= "</pre>";
        $msg .= "</div>";
		return $msg;
	}
	
}