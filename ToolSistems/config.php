<?php

/*
 * 	autor: pol.gomez
 * 	versió: 20180307b
 */
 
if (!defined('_6TEMS_DEV_IPS_'))
	define('_6TEMS_DEV_IPS_', '213.96.34.17#80.64.37.127'); /* 6TEMS DEVELOPMENT IPS */

if (!defined('_6TEMS_DEV_DOMAIN_'))
	define('_6TEMS_DEV_DOMAIN_', 'proves3.6tems.es/developers');

if (!defined('_JIRA_URL_'))
	define('_JIRA_URL_', 'https://jira6tems.atlassian.net/browse/');

if (!defined('_TOOLSISTEMS_ABS_PATH_'))	define('_TOOLSISTEMS_ABS_PATH_', $currentDir);

/*if (!defined('_PAGES_AVOID_6TEMS_BUTTON_'))
	define('_PAGES_AVOID_6TEMS_BUTTON_', '');*/

/* INCLUYE TODOS LOS PHP DEL extend-functions */
$extend_functions_dir = dirname(__FILE__) . '/extend-functions/';
foreach ( scandir($extend_functions_dir) as $file_name ) {
	$file_name_explode = array_reverse(explode('.', $file_name));
	if ( $file_name_explode[0] == 'php' )
		include($extend_functions_dir . $file_name);
}
/* /INCLUYE TODOS LOS PHP DEL extend-functions */

?>