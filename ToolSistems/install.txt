/* 6TEMS DEV TOOLS */
$currentDir = dirname(__FILE__);
if (is_file($currentDir.'/ToolSistems/ToolSistems.php')) {
    if ( !in_array('ToolSistems', get_declared_classes())) {
        include_once($currentDir.'/ToolSistems/ToolSistems.php');
    }
    ToolSistems::showErrors(false);
	function ToolSistemsButton() { ToolSistems::show6TEMSButton(true); }
	register_shutdown_function('ToolSistemsButton');
}
/* /6TEMS DEV TOOLS */