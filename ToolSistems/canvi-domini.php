<?php 

/*
 * 	autor: pol.gomez
 * 	versió: 20180307b
 */
 
require_once('ToolSistems.php');
if ( !ToolSistems::isDevIp() ) ToolSistems::all_bad();

?>

<html>
<head>

	<meta name="robots" content="noindex">
	<meta name="googlebot" content="noindex">

	<style>
		td, input[type=text] { width: 500px; }
		td.label { width: 20%; }
	</style>
</head>
<body>

<?php

if (!ToolSistems::isDevIp()) ToolSistems::all_bad();

if ( isset($_POST['send']) )
{
	$domini_vell = $_POST['domini_vell'];
	$domini_nou = $_POST['domini_nou'];
	$mysql_host = $_POST['mysql_host'];
	$mysql_database = $_POST['mysql_database'];
	$mysql_user = $_POST['mysql_user'];
	$mysql_password = $_POST['mysql_password'];
	
	$tables = array();
	
	ToolSistems::showPub($_POST, false);
	
	$conn = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_database);
	if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);
	
	$sql = "SELECT table_name FROM information_schema.tables where table_schema = '" . $mysql_database . "';";
	$result = $conn->query($sql);
	
	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {
			$table = new stdClass();
			$table->name = $row['table_name'];
			$table->count = 0;
			$table->fields = array();
			
			$sql2 = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" . $mysql_database . "' AND TABLE_NAME = '" . $table->name . "';";
			$result2 = $conn->query($sql2);
			if ($result2->num_rows > 0) {
				while($row2 = $result2->fetch_assoc()) {
					$table->fields[] = $row2['COLUMN_NAME'];
				}
			}
			
			$tables[] = $table;
		}
	}
	
	echo '<table style="width: 100%;">';
	foreach ($tables as $table)
	{
		foreach ($table->fields as $field)
		{
			$sql_replace = "UPDATE ".$table->name." SET ".$field." = REPLACE(".$field.", '" . $domini_vell . "', '" . $domini_nou . "');";
			$count = $conn->query($sql_replace);
			$table->count += $conn->affected_rows;
		}
		
		if ($table->count > 0) 
			echo '<tr><td class="label">' . $table->name . '</td><td>' . $table->count . '</td></tr>';
	}
	echo '</table>';
	
	$conn->close();
	
	ToolSistems::all_good();
}

?>

<form method="post" action="">

<table style="width: 100%;">
<tr>
	<td class="label">Domini vell</td>
	<td style="width: 80%;"><input type="text" name="domini_vell" value="" /></td>
</tr>
<tr>
	<td class="label">Domini nou</td>
	<td><input type="text" name="domini_nou" value="" /></td>
</tr>
<tr>
	<td class="label">MYSQL HOST</td>
	<td><input type="text" name="mysql_host" value="localhost" /></td>
</tr>
<tr>
	<td class="label">MYSQL_DATABASE</td>
	<td><input type="text" name="mysql_database" value="" /></td>
</tr>
<tr>
	<td class="label">MYSQL_USER</td>
	<td><input type="text" name="mysql_user" value="" /></td>
</tr>
<tr>
	<td class="label">MYSQL_PASSWORD</td>
	<td><input type="text" name="mysql_password" value="" /></td>
</tr>
<tr>
	<td>
		<input name="send" type="submit" value="Enviar" />
	</td>
</tr>
</table>

</form>
</body>
</html>