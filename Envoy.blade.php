@servers(['dns12' => 'root@188.93.75.92 -p2220', 'dns51' => 'root@185.209.60.88 -p2220', 'dns36' => 'root@188.93.75.52 -p2220', 'dns59' => 'root@185.136.91.166 -p2220'])

@setup
    $branch = isset($branch) ? $branch : "master";
@endsetup

@task('pre-marimon', ['on' => 'dns12'])
    cd /var/www/vhosts/6tems.es/subdomains/marimon/httpdocs

    git fetch -v origin {{ $branch }}

    git reset --hard FETCH_HEAD

    git checkout {{ $branch }}

    git pull origin {{ $branch }}

    cp -n composer.json.example composer.json

    /opt/plesk/php/7.1/bin/php /usr/lib64/plesk-9.0/composer.phar install

    /opt/plesk/php/7.1/bin/php artisan cache:clear
    /opt/plesk/php/7.1/bin/php artisan view:clear

    /opt/plesk/php/7.1/bin/php artisan clear-compiled

    cd /var/www/vhosts/6tems.es/subdomains/marimon

    chown -R es6tems:psacln httpdocs
    chown es6tems:psaserv httpdocs
@endtask

@task('prod-marimon', ['on' => 'dns51'])
    cd /var/www/vhosts/compliancearea.com/httpdocs

    git fetch -v origin {{ $branch }}

    git reset --hard FETCH_HEAD

    git checkout {{ $branch }}

    git pull origin {{ $branch }}

    cp -n composer.json.example composer.json

    /opt/plesk/php/7.1/bin/php /usr/lib64/plesk-9.0/composer.phar install

    /opt/plesk/php/7.1/bin/php artisan cache:clear
    /opt/plesk/php/7.1/bin/php artisan view:clear

    /opt/plesk/php/7.1/bin/php artisan clear-compiled

    cd /var/www/vhosts/compliancearea.com

    chown -R complianceftp:psacln httpdocs
    chown complianceftp:psaserv httpdocs
@endtask

@task('pre-zehnder', ['on' => 'dns12'])
    cd /var/www/vhosts/6tems.es/subdomains/zehnder/httpdocs

    git fetch -v origin {{ $branch }}

    git reset --hard FETCH_HEAD

    git checkout {{ $branch }}

    git pull origin {{ $branch }}

    cp -n composer.json.example composer.json

    /opt/plesk/php/7.1/bin/php /usr/lib64/plesk-9.0/composer.phar install

    /opt/plesk/php/7.1/bin/php artisan cache:clear
    /opt/plesk/php/7.1/bin/php artisan view:clear

    /opt/plesk/php/7.1/bin/php artisan clear-compiled

    cd /var/www/vhosts/6tems.es/subdomains/zehnder

    chown -R es6tems:psacln httpdocs
    chown es6tems:psaserv httpdocs
@endtask

@task('pro-zehnder', ['on' => 'dns51'])
    cd /var/www/vhosts/runtal-zehnder-docs.com/httpdocs

    git fetch -v origin {{ $branch }}

    git reset --hard FETCH_HEAD

    git checkout {{ $branch }}

    git pull origin {{ $branch }}

    cp -n composer.json.example composer.json

    /opt/plesk/php/7.1/bin/php /usr/lib64/plesk-9.0/composer.phar install

    /opt/plesk/php/7.1/bin/php artisan cache:clear
    /opt/plesk/php/7.1/bin/php artisan view:clear

    /opt/plesk/php/7.1/bin/php artisan clear-compiled

    cd /var/www/vhosts/runtal-zehnder-docs.com

    chown -R runtalzehnderdocsftp:psacln httpdocs
    chown runtalzehnderdocsftp:psaserv httpdocs
@endtask


@task('pre-stocktextil', ['on' => 'dns12'])
    cd /var/www/vhosts/6tems.es/subdomains/stocktextil/httpdocs

    git fetch -v origin {{ $branch }}

    git reset --hard FETCH_HEAD

    git checkout {{ $branch }}

    git pull origin {{ $branch }}

    cp -n composer.json.example composer.json

    /opt/plesk/php/7.1/bin/php /usr/lib64/plesk-9.0/composer.phar install

    /opt/plesk/php/7.1/bin/php artisan cache:clear
    /opt/plesk/php/7.1/bin/php artisan view:clear

    /opt/plesk/php/7.1/bin/php artisan clear-compiled

    cd /var/www/vhosts/6tems.es/subdomains/stocktextil

    chown -R es6tems:psacln httpdocs
    chown es6tems:psaserv httpdocs
@endtask

@task('pro-stocktextil', ['on' => 'dns36'])
    cd /var/www/vhosts/stocksone.net/httpdocs

    git fetch -v origin {{ $branch }}

    git reset --hard FETCH_HEAD

    git checkout {{ $branch }}

    git pull origin {{ $branch }}

    cp -n composer.json.example composer.json

    /opt/plesk/php/7.1/bin/php /usr/lib64/plesk-9.0/composer.phar install

    /opt/plesk/php/7.1/bin/php artisan cache:clear
    /opt/plesk/php/7.1/bin/php artisan view:clear

    /opt/plesk/php/7.1/bin/php artisan clear-compiled

    cd /var/www/vhosts/stocksone.net

    chown -R stocksoneftp:psacln httpdocs
    chown stocksoneftp:psaserv httpdocs
@endtask


@task('pre-aravell', ['on' => 'dns12'])
    cd /var/www/vhosts/6tems.es/subdomains/aravellgolfclub/httpdocs

    git fetch -v origin {{ $branch }}

    git reset --hard FETCH_HEAD

    git checkout {{ $branch }}

    git pull origin {{ $branch }}

    cp -n composer.json.example composer.json

    /opt/plesk/php/7.1/bin/php /usr/lib64/plesk-9.0/composer.phar install

    /opt/plesk/php/7.1/bin/php artisan cache:clear
    /opt/plesk/php/7.1/bin/php artisan view:clear

    /opt/plesk/php/7.1/bin/php artisan clear-compiled

    cd /var/www/vhosts/6tems.es/subdomains/aravellgolfclub

    chown -R es6tems:psacln httpdocs
    chown es6tems:psaserv httpdocs
@endtask

@task('pre-array', ['on' => 'dns12'])
    cd /var/www/vhosts/6tems.es/subdomains/array/httpdocs

    git fetch -v origin {{ $branch }}

    git reset --hard FETCH_HEAD

    git checkout {{ $branch }}

    git pull origin {{ $branch }}

    cp -n composer.json.example composer.json

    /opt/plesk/php/7.1/bin/php /usr/lib64/plesk-9.0/composer.phar install

    /opt/plesk/php/7.1/bin/php artisan cache:clear
    /opt/plesk/php/7.1/bin/php artisan view:clear

    /opt/plesk/php/7.1/bin/php artisan clear-compiled

    cd /var/www/vhosts/6tems.es/subdomains/array

    chown -R es6tems:psacln httpdocs
    chown es6tems:psaserv httpdocs
@endtask

@task('pro-array', ['on' => 'dns59'])
    cd /var/www/vhosts/arrayplastics.com/subdomains/documents/httpdocs

    git fetch -v origin {{ $branch }}

    git reset --hard FETCH_HEAD

    git checkout {{ $branch }}

    git pull origin {{ $branch }}

    cp -n composer.json.example composer.json

    /opt/plesk/php/7.1/bin/php /usr/lib64/plesk-9.0/composer.phar install

    /opt/plesk/php/7.1/bin/php artisan cache:clear
    /opt/plesk/php/7.1/bin/php artisan view:clear

    /opt/plesk/php/7.1/bin/php artisan clear-compiled

    cd /var/www/vhosts/arrayplastics.com/subdomains/documents

    chown -R docsarrayftp:psacln httpdocs
    chown docsarrayftp:psaserv httpdocs
@endtask

@task('pre-prodaisa', ['on' => 'dns12'])
    cd /var/www/vhosts/6tems.es/subdomains/prodaisa/httpdocs

    git fetch -v origin {{ $branch }}

    git reset --hard FETCH_HEAD

    git checkout {{ $branch }}

    git pull origin {{ $branch }}

    cp -n composer.json.example composer.json

    /opt/plesk/php/7.1/bin/php /usr/lib64/plesk-9.0/composer.phar install

    /opt/plesk/php/7.1/bin/php artisan cache:clear
    /opt/plesk/php/7.1/bin/php artisan view:clear

    /opt/plesk/php/7.1/bin/php artisan clear-compiled

    cd /var/www/vhosts/6tems.es/subdomains/prodaisa

    chown -R es6tems:psacln httpdocs
    chown es6tems:psaserv httpdocs
@endtask