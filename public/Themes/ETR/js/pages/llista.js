Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function trim (s, c) {
    if (c === "]") c = "\\]";
    if (c === "\\") c = "\\\\";
    return s.replace(new RegExp(
        "^[" + c + "]+|[" + c + "]+$", "g"
    ), "");
}

function removeDups(names) {
    let unique = {};
    names.forEach(function(i) {
        if(!unique[i]) {
        unique[i] = true;
        }
    });
    return Object.keys(unique);
}

function arrayRemove(arr, value) {
    return arr.filter(function(ele){
        return ele != value;
    });
}

$('input.mobile').click(function(e) {
    $filters = $(this).closest('.filters');
    $filters.removeClass('visible');
    $filters.animate({
        'left': '-=100%'
    });
});


$('#form_retiro').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        if ($("#search").length && $("#search").val() == '') {
            changeLocation();
        }

        e.preventDefault();
        return false;
    }
});

/* search drop down script */
$('.search-accordion-toggle').click(function() {      
    $(this).parent().toggleClass('active');
	$(this).parent().next().toggleClass('active');
});

$(".search-sec.search-hom ul li.ser .text").focus(function(){   
    $(this).next().addClass('hide');
    $('.search-sec.search-hom ul li.date').find(".search-modal").removeClass('hide');
});

$(".search-sec.search-hom ul li.date .text1").focus(function(){   
    $(this).next().addClass('hide');
    $('.search-sec.search-hom ul li.ser').find(".search-modal").removeClass('hide');
});

$('.desplegable').click(function() {
    var parent = $(this).parent();
    var child = parent.siblings('.child');

    $(this).toggleClass('open');
    child.toggleClass('open');
});


$('.recom select').change(function(e) {
    $('.recom select').val($(this).val());
});

function setOrUnsetChildren(node) {
    var parent = $(node).closest(".input-container");
    var childs = parent.find(".child").first().children();

    childs.each(function() {
        var current = $(this).find('input').first();
        current.prop("checked", node.checked);
        if (current) setOrUnsetChildren(current.get(0));
    });
}

function filterRebuildURL(node) {
    var url = new URL(window.location.href);
    url.searchParams.delete("loc[]");

    var contenidor = $(node).closest('.accordion_in .acc_content');
    contenidor.children().each(function() {
        // Country
        var country = $(this).find('input').first();
        if (! country.get(0).checked ) {
            var communities = $(this).children('.child').children();
            communities.each(function() {
                // Comunity
                var comunity = $(this).find('input').first();

                if (! comunity.get(0).checked ) {
                    var provinces = $(this).children().children('.child').children();
                    if (provinces.find('input').length > 0 && provinces.find('input').length == provinces.find('input:checked').length) {
                        comunity.prop("checked", true);
                        url.searchParams.append("loc[]", comunity.data('post_name')); // Afegir a URL
                        return true;
                    } else {
                        provinces.each(function() {
                            // Province
                            var province = $(this).find('input').first();
                            if ( province.get(0).checked ) {
                                url.searchParams.append("loc[]", province.data('post_name')); // Afegir a URL
                                return true;
                            }
                            // End Province
                        });
                    }
                } else {
                    url.searchParams.append("loc[]", comunity.data('post_name')); // Afegir a URL
                    return true;
                }
                // End Comunity
            });
        } else {
            url.searchParams.append("loc[]", country.data('post_name')); // Afegir a URL
            return true;
        }
        // End Country
    });

    return url;
}


$(".child.provincia").each(function() {
    if ($(this).find('input:checked').length > 0) {
        $(this).addClass("open");
    }

    if ($(this).find('input').length > 0 && $(this).find('input:checked').length == $(this).find('input').length) {
        $(this).closest('.input-container').find('input.comunidad ').prop("checked", true);
    }
});


function propagateUnclick(node) {
    var current = $(node);
    var input = current.closest('li');
    var i = 0;

    do {
        input = input.closest('li').closest('.input-container').find('input').first();
        input.prop('checked', false);
        i++;
    } while(input && i < 10);
}


$('.filters input.post, .recom select').change(function(e) {
    e.preventDefault();

    $('body').removeClass('hide');

    var post_name = $(this).data('post_name');
    var url = new URL(window.location.href);

    if ( $(this).hasClass('location') ) {
        post_name = $(this).attr('name');
        $(`input.location[name="${post_name}"]`).prop('checked', $(this).prop('checked'));

        setOrUnsetChildren(this); // Set or Unset all childs

        if (this.checked !== true) {
            propagateUnclick(this);
        }

        url = filterRebuildURL(this);
        window.history.pushState({},"", decodeURIComponent(url.href));
        isLocation = true;
    } else {
        $(`input.post[data-post_name="${post_name}"]`).prop('checked', $(this).prop('checked'));
    }

    if ($(this).is(':checked')) {
        if (!$(`.tag[data-filter="${post_name}"]`).length) {
            var custom_field = $(this).data('cf');
            var value = $(this).data('title');
            var buscador = $(this).data('buscador');

            var tag = `<div class="tag" data-cf="${custom_field}" data-filter="${post_name}">${value}<span>x</span>`;

            if (typeof buscador !== 'undefined' && buscador) {
                tag = tag + `<input type="hidden" name="buscador[]" value="${buscador}">`;
            }

            tag = tag + '</div>';

            if (typeof filters_container !== 'undefined') {
                $(filters_container).append(tag);
                $(filters_container).removeClass('hide');
            }
        }
    } else {
        $(`.tag[data-filter="${post_name}"]`).remove();

        if (typeof filters_container !== 'undefined' && !$('.tag').length) {
            $(filters_container).addClass('hide');
        }
    }

    if ($(this).data('cf') == 'duracion') {
        if ($(this).is(':checked')) {
            $(`.searcher-duration[data-filter=${post_name}]`).addClass('selected');
        } else {
            $(`.searcher-duration[data-filter=${post_name}]`).removeClass('selected');
        }

        var duraciones = $('.searcher-duration.selected');
        var duracionInput = $('#duracion-y-llegada');
        duraciones.each(function (index) {
            if (index === 0) {
                duracionInput.val($(this).text());
            } else {
                duracionInput.val(duracionInput.val() + ', ' + $(this).text());
            }
        });

        var date = '';
        if ($('.arrival-date.selected').length) {
            date = $('.arrival-date.selected').text();
        } else {
            var selectedCell = $('.datepicker--cell.datepicker--cell-day.-selected-');
            if (selectedCell.length) {
                date = [selectedCell.data('date'), (parseInt(selectedCell.data('month')) + 1), selectedCell.data('year')].join('/');
            }
        }

        if (duraciones.length) {
            if (date === '') {
                duracionInput.val(duracionInput.val());
            } else {
                duracionInput.val(duracionInput.val() + ' | ' + date);
            }
        } else {
            duracionInput.val(date);
        }
    }

    if (typeof isLocation !== 'undefined' && isLocation) [url, metas] = process_url();
    else {
        [url, metas] = process_url(post_name, $(this).get(0).checked);
    }

    window.history.pushState({},"", decodeURIComponent(url.href));
    $('.retiros-sec').addClass('loading');
    carregarRetiros(metas, function() {
        $('.retiros-sec').removeClass('loading');
    });
});

$(document).click(function(event) {
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if (!$(event.target).closest(".es-pad").length && (typeof isDatePickerElement !== 'function' || !isDatePickerElement(event.target.className))) {
        $("body").find(".search-modal").removeClass('hide');
    }
});

/* search drop down script */
var timeout = false;
$(document).ready(function() {
    $(".accordion_example1, .accordion_example2").smk_Accordion({ closeAble: true });

    $('#slide, .filter-menu h4 a').click(function() {
        var hidden = $('.filter-menu');
        if (hidden.hasClass('visible')){
            hidden.animate({"left":"-1000px"}, "slow").removeClass('visible');
            $('body').removeClass('hide');
        } else {
            hidden.animate({"left":"0px"}, "slow").addClass('visible');
            $('body').addClass('hide');
        }
    });

    $("ul.showmore").showmore({
        visible:10,
        showMoreText : "<span class='btn'>Ver mas...</span>",
        showLessText : "<span class='btn'></span>",});
        
    $("ul.showmore1").showmore({
        visible:10,
        showMoreText : "<span class='btn'>Ver mas...</span>",
        showLessText : "<span class='btn'></span>",});
});

function changeLocation(place = null) {
    // Update URL + AJAX
    var url = new URL(window.location.href);
    url.searchParams.delete('coords');
    url.searchParams.delete('city');
    url.searchParams.delete('province');
    url.searchParams.delete('region');
    url.searchParams.delete('country');
    
    if (place) {
        coords = place.geometry.location.lat() + "," +  place.geometry.location.lng();
        url.searchParams.set('coords', coords);
        url.searchParams.set('city', JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'locality'; })[0]));
        url.searchParams.set('province', JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'administrative_area_level_2'; })[0]));
        url.searchParams.set('region', JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'administrative_area_level_1'; })[0]));
        url.searchParams.set('country', JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'country'; })[0]));
    }

    window.history.pushState({},"", decodeURIComponent(url.href));
    carregarRetiros();
    
    if (place) {
        document.getElementById('localidad').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'locality'; })[0]);
        document.getElementById('provincia').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'administrative_area_level_2'; })[0]);
        document.getElementById('region').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'administrative_area_level_1'; })[0]);
        document.getElementById('pais').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'country'; })[0]);
    }
}