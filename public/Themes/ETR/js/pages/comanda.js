var $ = jQuery.noConflict();
// $j is now an alias to the jQuery function; creating the new alias is optional.
$(document).ready(function() {
    $('.fancybox1').fancybox();
    /*FancyBox*/	
});

function renderPaypal()
{
	$('#paypal-button').html('');
	paypal.Button.render({
		env: paypalOptions.env,
		locale: 'es_ES',

		style: {
			background: 'tranparent',
			color: 'blue',
			size: 'responsive',
			label: 'pay'
		},

		client: paypalOptions.client,
		commit: true,
		validate: function(actions) {
			if (!$('#policy-usr').is(':checked') || !$('#policy').is(':checked')) {
				actions.disable(); // Allow for validation in onClick()
			} else {
				actions.enable()
			}
		},

		// Called for every click on the PayPal button even if actions.disabled
		onClick: function(e) {
			if (!$('#policy-usr').is(':checked') || !$('#policy').is(':checked')) {
				var $myForm = $("#command-form");
				if(! $myForm[0].checkValidity()) {
					// If the form is invalid, submit it. The form won't actually submit;
					// this will just cause the browser to display the native HTML5 error messages.
					$myForm.find(':submit').click();
				}
			}
		},
		payment: function(data, actions) {

			return actions.payment.create({
				transactions: [
					{
						amount: { total: $('input[name="deposito"]').val(), currency: 'EUR' }
					}
				]
			});
		},
		onAuthorize: function(data, actions) {
			var mData = {
				_token: $('meta[name="csrf-token"]').attr('content'),
				method: 'paypal',
				paymentID: data.paymentID,
				payerID:   data.payerID,
			};

			var $inputs = $('#command-form :input');

			$inputs.each(function() {
				mData[this.name] = $(this).val();
			});

			return paypal.request.post(paypalOptions.post_url, mData).then(function (response) {
				if (response.url) {
					window.location = response.url;
				}
			});
		}
	}, '#paypal-button');
}


$(document).ready(function(){
	var observer = new IntersectionObserver(function(entries, observer) { 
        if (window.innerWidth < 639) {
            for (entry of entries) {
                if (entry.isIntersecting) {
                    $('.reserva-mbl').fadeOut();
                } else {
                    if (entry.boundingClientRect.y > 0) {
                        $('.reserva-mbl').fadeIn();
                    }
                }
            }
        } else {
            $('.reserva-mbl').hide();
        }
    });

    observer.observe(document.querySelector('div.frm-ryt'));

	$('.toggle-btn').click(function(e){
		e.preventDefault();
		var getId = $(this).parents('.divider').attr('id');
		var divider = $('#'+getId);
		divider.toggleClass('open');

		var openTxt = $(this).data('open-txt');
		var closeTxt = $(this).data('closed-txt');

		if (typeof openTxt !== 'undefined' && typeof closeTxt !== 'undefined') {
			$(this).html(divider.hasClass('open') ? openTxt : closeTxt)
		}
	});		
	//card function	
	
	// For the blue theme
	if (typeof Creditly !== 'undefined') {
		var blueThemeCreditly = Creditly.initialize(
			'.expiration-month-and-year',
			'.credit-card-number',
			'.security-code'
		);

		$(".creditly-blue-theme-submit").click(function (e) {
			e.preventDefault();
			var output = blueThemeCreditly.validate();
			if (output) {

			}
		});
	}
	
	//card Input
	$('.credit-card-number').keyup(function(){
		var getValue = $('.credit-card-number').val();					
		var splitText = getValue.split(" ");
		console.log(splitText);
		var getlength= 0;
		if(splitText.length > 0 || splitText.length != undefined)
		{
			getlength = splitText.length;
		}
		console.log(getlength);
		$('.card-num span').empty();
		for(var i=0; i<=getlength; i++)
		{
			console.log(splitText[i])
			$('.card-num span:nth-child('+(i+1)+')').text(splitText[i]);
		}				
	});
	
	//Changing month
	$('.expiration-month-and-year').keyup(function(){
		var getValue = $(this).val();
		$('.month-number').empty();
		$('.month-number').text(getValue);
	});

	var couponInput = $('.js-coupon-input');
	var couponSubmit = $('.js-coupon-submit');

	couponInput.on('keyup', function (e) {
		if (e.keyCode == 13) {
			couponSubmit.click();
		}
	});

	couponSubmit.on('click', function () {
		var code = couponInput.val();

		if (typeof code !== 'undefined' && code !== '') {
			$.ajax({
				url: ajaxURL,
				method: 'post',
				data: {
					action: 'setCoupon',
					parameters: {
						code: code,
						amount: couponInput.data('amount')
					}
				},
				success: function (data) {
					if (data.result.original) {
						var reservePayment = parseFloat($('.js-reserve-payment').text());
						var organizerPayment = parseFloat($('.js-organizer-payment').text());
						var priceEuro = parseFloat($('input[name="preu"]').val());

						var deposit = Math.round((reservePayment - data.result.original.value_user_currency) * 100) / 100;
						
						$('.js-coupon-message').html(data.result.original.message);
						$('.js-coupon-value').html(data.result.original.value_user_currency);
						$('.js-coupon-code').html(data.result.original.code);

						$('.js-total-order').html(Math.round((reservePayment + organizerPayment - data.result.original.value_user_currency) * 100) / 100);
						$('.js-pay-now').html(deposit);
						$('.js-reserve-button-amount').html(deposit);
						$('.js-add-dharmas').html(Math.round(priceEuro - data.result.original.value));
						
						$('.js-dharmas-line').hide();
						$('.js-coupon-line').show();

						$('input[name="deposito"]').val(deposit);
						$('input[name="coupon_code"]').val(data.result.original.code);
						$('input[name="dharmas"]').val(0);

						renderPaypal();
					}
				},
				error: function (data) {
					var message = JSON.parse(data.responseText).message;

					if (typeof message !== 'undefined' && message) {
						$('.js-coupon-message').html(message);
					} else {
						$('.js-coupon-message').html('Error!');
					}

					var reservePayment = parseFloat($('.js-reserve-payment').text());
					var organizerPayment = parseFloat($('.js-organizer-payment').text());
					var priceEuro = parseFloat($('input[name="preu"]').val());
					var dharmasValue = parseFloat($('.js-dharmas-value').text());

					var deposit = Math.round((reservePayment - dharmasValue) * 100) / 100;

					$('.js-total-order').html(Math.round((reservePayment + organizerPayment - dharmasValue) * 100) / 100);
					$('.js-pay-now').html(deposit);
					$('.js-reserve-button-amount').html(deposit);
					$('.js-add-dharmas').html(Math.round(priceEuro - dharmasValue));

					$('.js-dharmas-line').show();
					$('.js-coupon-line').hide();

					$('input[name="deposito"]').val(deposit);
					$('input[name="coupon_code"]').val('');
					$('input[name="dharmas"]').val(dharmasValue);
				}
			});
		}
	});

	var paypalButton = $('.paypal-button');

	if (typeof paypalButton !== 'undefined' && paypalButton !== null && paypalButton.length > 0) {
		$('#policy-usr, #policy').on('change', function () {
			renderPaypal();
		});

		renderPaypal();
	}
});

