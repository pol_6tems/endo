var owl = $('#abt-car');
	owl.owlCarousel({
		goToFirstSpeed :1000,
		loop:true,
		items:1,
		margin:0,
		autoplay:true,
		autoplayTimeout:5500,
		autoplayHoverPause:true,		
	    nav : false,
	    dots : true,
		responsive: {
			0:{ items:1, margin:0,},
			480:{ items:1},
			640:{ items:1},
			768:{ items:1},
			1024:{ items:1}
		},
	});