(function(window, $) {
    var regInputs = [
        {name: 'nombre', is_array:false, is_email:false, after_name:'nombre'},
        {name: 'apellido', is_array:false, is_email:false, after_name:'apellidos'},
        {name: 'correo', is_array:false, is_email:true, after_name:'email'},
        {name: 'contrasena', is_array:false, is_email:false, after_name:false},
        {name: 'fecha_nacimiento', is_array:true, items:['dia', 'mes', 'ano'], after_name:['dia', 'mes', 'ano']},
        {name: 'genero', is_array:false, is_email:false, after_name:'genero'},
        {name: 'phone-number', is_array:false, after_name:'telefono'},
        {name: 'city', is_array:false, is_email:false, after_name:'city'},
        {name: 'country', is_array:false, is_email:false, after_name:'country'}
    ];

    var options;

    var ImmediateOrder = function(mOptions) {
        options = mOptions;
        this.init();
    };

    ImmediateOrder.prototype = {
        init: function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });

            this.bindings();
        },

        bindings: function () {
            var me = this;
            $('.js-reg-button').on('click', me.submitRegister.bind(me));

            $(document).ready(function() {
                var observer = new IntersectionObserver(function(entries, observer) {
                    if (window.innerWidth < 639) {
                        for (entry of entries) {
                            if (entry.isIntersecting) {
                                $('.reserva-mbl').fadeOut();
                            } else {
                                if (entry.boundingClientRect.y > 0) {
                                    $('.reserva-mbl').fadeIn();
                                }
                            }
                        }
                    } else {
                        $('.reserva-mbl').hide();
                    }
                });

                observer.observe(document.querySelector('div.frm-ryt'));

                $('.zopim').remove();
            });
        },

        submitRegister: function() {
            var me = this;
            var data = this.getRegData();

            if (data) {
                $.ajax({
                    url: options.regUrl,
                    method: "POST",
                    data: data,
                    success: me.regCallback.bind(me),
                    error: function (data) {

                    }
                });
            }
        },

        regCallback: function(data) {
            if (data.status != 'ok') {
                console.log(data.msg);
                toastr.error(data.msg);
                return;
            }

            var me = this;

            $('.js-reg-button').slideUp('fast', function () {
                $('.js-payment-buttons').slideDown('fast', function () {
                    me.renderPaypal();
                });
            });
            $('.js-step-2').removeClass('active');
            $('.js-step-3').addClass('active');

            this.updateInputs();

            this.gtagProgress();
        },

        getRegData: function() {
            var data = {};
            var isOk = true;
            var me = this;

            $.each(regInputs, function (index, item) {
                if (!item.is_array) {
                    var input = $('.frm-ryt .frm-grp input[name="' + item.name + '"]');

                    if (typeof input === 'undefined' || !input.length) {
                        input = $('.frm-ryt .frm-grp select[name="' + item.name + '"]');
                    }
                    var value = input.val();

                    if (input.attr('required') && value == '') {
                        input.css('border', '1px solid red');
                        isOk = false;
                    } else {
                        input.css('border', '1px solid #e0e0e0');
                    }

                    if (item.is_email) {
                        var isEmail = me.isEmail(input.val());

                        if (!isEmail) {
                            input.css('border', '1px solid red');
                            isOk = false;
                        } else {
                            input.css('border', '1px solid #e0e0e0');
                        }
                    }

                    data[item.name] = input.val();
                } else {
                    data[item.name] = {};
                    $.each(item.items, function (i, child) {
                        var input = $('.frm-ryt .frm-grp input[name="' + item.name + '[' + child + ']' + '"]');

                        if (typeof input === 'undefined' || !input.length) {
                            input = $('.frm-ryt .frm-grp select[name="' + item.name + '[' + child + ']' + '"]');
                        }

                        data[item.name][child] = input.val();
                    })
                }
            });

            var dateOk = me.checkBirthDate();

            if (isOk) {
                isOk = dateOk
            }

            var cbOk = me.checkCheckboxs();

            if (isOk) {
                isOk = cbOk
            }

            if (isOk) {
                return data;
            } else {
                return false;
            }
        },

        updateInputs: function() {
            $.each(regInputs, function (index, item) {
                if (!item.is_array) {
                    var input = $('.frm-ryt .frm-grp input[name="' + item.name + '"]');

                    if (typeof input === 'undefined' || !input.length) {
                        input = $('.frm-ryt .frm-grp select[name="' + item.name + '"]');
                    }

                    if (item.after_name) {
                        $('input[name="acompanantes[1][' + item.after_name + ']"]').val(input.val());
                    }
                    input.attr('name', '');
                    input.attr('disabled','disabled');
                } else {
                    $.each(item.items, function (i, child) {
                        var input = $('.frm-ryt .frm-grp input[name="' + item.name + '[' + child + ']' + '"]');

                        if (typeof input === 'undefined' || !input.length) {
                            input = $('.frm-ryt .frm-grp select[name="' + item.name + '[' + child + ']' + '"]');
                        }
                        if (item.after_name) {
                            $('input[name="acompanantes[1][' + item.name + ']' + '[' + child + ']' + '"]').val(input.val());
                        }
                        input.attr('name', '');
                        input.attr('disabled','disabled');
                    })
                }
            });
        },

        checkBirthDate: function() {
            var birthInputs = $('.fecha-nacimiento');
            var dia = birthInputs.find('select.dia');
            var mes = birthInputs.find('select.mes');
            var any = birthInputs.find('select.any');

            var date1 = new Date();
            var date2 = new Date(any.val(), (mes.val() - 1), dia.val());

            var diffTime = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            var years = diffDays / 365;

            if (typeof any.val() === 'undefined' || any.val() == 0 || years < 18) {
                dia.siblings('.sbHolder').css('border', '1px solid red');
                mes.siblings('.sbHolder').css('border', '1px solid red');
                any.siblings('.sbHolder').css('border', '1px solid red');
                $('html, body').animate({
                    scrollTop: $("#input-data").offset().top
                }, 500);

                return false;
            } else {
                dia.siblings('.sbHolder').css('border', '1px solid #e0e0e0');
                mes.siblings('.sbHolder').css('border', '1px solid #e0e0e0');
                any.siblings('.sbHolder').css('border', '1px solid #e0e0e0');

                return true;
            }
        },

        isEmail: function (email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        },

        checkCheckboxs: function() {
            var $myForm = $("#command-form");
            if(! $myForm[0].checkValidity()) {
                $myForm.find(':submit').click();
            }

            return $('#policy-usr').is(':checked') && $('#policy').is(':checked');
        },

        gtagProgress: function() {
            gtag('event', 'checkout_progress', options.gtagData);
        },

        renderPaypal: function () {
            var me = this;

            paypal.Button.render({
                env: options.paypalOptions.env,
                locale: 'es_ES',

                style: {
                    background: 'tranparent',
                    color: 'blue',
                    size: 'responsive',
                    label: 'pay'
                },

                client: options.paypalOptions.client,
                commit: true,
                validate: function(actions) {
                    if (me.checkCheckboxs()) {
                        actions.enable();
                    } else {
                        actions.disable(); // Allow for validation in onClick()
                    }
                },

                // Called for every click on the PayPal button even if actions.disabled
                onClick: function(e) {
                    if (!$('#policy-usr').is(':checked') || !$('#policy').is(':checked')) {
                        var $myForm = $("#command-form");
                        if(! $myForm[0].checkValidity()) {
                            // If the form is invalid, submit it. The form won't actually submit;
                            // this will just cause the browser to display the native HTML5 error messages.
                            $myForm.find(':submit').click();
                        }
                    }
                },
                payment: function(data, actions) {

                    return actions.payment.create({
                        transactions: [
                            {
                                amount: { total: $('input[name="deposito"]').val(), currency: 'EUR' }
                            }
                        ]
                    });
                },
                onAuthorize: function(data, actions) {
                    var mData = {
                        _token: $('meta[name="csrf-token"]').attr('content'),
                        method: 'paypal',
                        paymentID: data.paymentID,
                        payerID:   data.payerID,
                    };

                    var $inputs = $('#command-form :input');

                    $inputs.each(function() {
                        mData[this.name] = $(this).val();
                    });

                    return paypal.request.post(options.paypalOptions.post_url, mData).then(function (response) {
                        if (response.url) {
                            window.location = response.url;
                        }
                    });
                }
            }, '#paypal-button');
        }
    };

    if (typeof self !== 'undefined') {
        self.ImmediateOrder = ImmediateOrder;
    }

    // Expose as a CJS module
    if (typeof exports === 'object') {
        module.exports = ImmediateOrder;
    }
})(window, $);