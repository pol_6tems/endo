Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}


$(".banner-load .demo-container").hover(
    function () {
        $(this).addClass("on_hover");
    },
    function () {
        $(this).removeClass("on_hover");
    }
);


if ($('.rangepicker').length > 0) {
    var rangepicker = $('.rangepicker').datepicker({
        dateFormat: 'dd/mm/yyyy',
        timeFormat: null,
        language: lang,
        minDate: new Date(),
        onSelect: function (fd, d, picker) {
            rangepicker.hide();
            // Do nothing if selection was cleared
            if (!d) return;
            
            $('.rangepicker2').attr('readonly', 'false');
            
            var datepicker = $('.rangepicker2').datepicker({
                dateFormat: 'dd/mm/yyyy',
                timeFormat: null,
                language: lang,
                minDate: d.addDays(1),
                onSelect: function (fd, d, picker) {
                    datepicker.hide();
                    $('input[name="plazas"]').focus();
                }
            }).data('datepicker');
            datepicker.clear();
            datepicker.show();
        }
    }).data('datepicker');
}


$(document).ready(function () {
    $(".fancybox, .fancybox1").fancybox();

    $('#horizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#tabInfo');
            var $name = $('span', $info);

            $name.text($tab.text());

            $info.show();
        }
    });

    $('#verticalTab').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true
    });
});

/* quantity  script */
jQuery(document).ready(function () {
    // This button will increment the value
    $('.qtyplus').click(function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('input[name=' + fieldName + ']').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name=' + fieldName + ']').val(0);
        }
    });

    // new jquery for increament 	
    var incrementPlus;
    var incrementMinus;

    var buttonPlus = $(".cart-qty-plus");
    var buttonMinus = $(".cart-qty-minus");

    var incrementPlus = buttonPlus.click(function () {
        var $n = $(this)
            .parent(".button-container")
            .find(".qty");
        $n.val(Number($n.val()) + 1);
    });

    var incrementMinus = buttonMinus.click(function () {
        var $n = $(this)
            .parent(".button-container")
            .find(".qty");
        var amount = Number($n.val());
        if (amount > 0) {
            $n.val(amount - 1);
        }
    });
});