$("#newsletter").click(function(e) {
    subscribeToNewsletter($(this).siblings('input[type="email"]'));
});

$('input[name="email_newsletter"]').keyup(function (e) {
    if (e.keyCode == 13) {
        subscribeToNewsletter($(this));
    }
});

function subscribeToNewsletter(input) {
    var email = $(input).val();
    var url = $(input).data('url');
    var span = $('#policy').siblings('label').children('span');

    if (input.val() == '') input.css("border", "2px solid red");
    else input.css("border", "0");

    if ( !$('#policy').prop("checked") ) span.css('border', '2px solid red');
    else span.css('border', '1px solid #f9af02');

    if ( input.val() != '' && $('#policy').prop("checked") ) {
        $.ajax({
            url: url,
            method: "POST",
            data: {
                email: email,
                policy: $('#policy').val(),
                acumba_list: $('input[name="acumba_list"]').val()
            },
            success: function(data) {
                var res = JSON.parse(data);
                $('.form .message').addClass('correcte');
                if (res.result.status) {
                    $('.form .message').html("Ya estaba suscrito a nuestra lista");
                } else {
                    $('.form .message').html("Se ha suscrito correctamente a nuestra lista");
                }
            },
            error: function(data) {
                var message = JSON.parse(data.responseText);
                var errors = message.errors.email[0];
                $('.form .message').addClass('errors');
                $('.form .message').html(errors);
            },
            complete: function() {
                $('.form .message').slideDown();
                setTimeout(function() {
                    $('.form .message').slideUp(400, function() {
                        $('.form .message').removeClass('correcte');
                        $('.form .message').removeClass('errors');
                    });
                }, 2500);
            }
        });
    }
} 