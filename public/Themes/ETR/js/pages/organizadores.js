$(window).load(function() {
    $('.flexslider').flexslider({
      animation: "fade",
      controlNav: "thumbnails",
      slideshowSpeed:5000,
      animationDuration: 3000,
      directionNav: false,
    });
});

var $ = jQuery.noConflict();
// $j is now an alias to the jQuery function; creating the new alias is optional.
$(document).ready(function() {
    $('.fancybox1').fancybox();
/*FancyBox*/	
});

$(document).ready(function() {
    var elements = $('.description').find('div, p, span');

    elements.each(function(i, el) {
        $(el).replaceWith(function() {
            return $("<p />").append($(this).contents());
        });
    });

    $('#slide, .filter-menu h1 a').click(function(){
    var hidden = $('.filter-menu');
    if (hidden.hasClass('visible')){
        hidden.animate({"left":"-1000px"}, "slow").removeClass('visible');
		$('body').removeClass('hide');
    } else {
        hidden.animate({"left":"0px"}, "slow").addClass('visible');
		$('body').addClass('hide');
    }
    });	

});

jQuery(document).ready(function() {
    jQuery("ul.showmore").showmore({
        visible:10,
        showMoreText : "<span class='btn'>Ver mas...</span>",
        showLessText : "<span class='btn'></span>",});

    jQuery("ul.showmore1").showmore({
        visible:10,
        showMoreText : "<span class='btn'>Ver mas...</span>",
        showLessText : "<span class='btn'></span>",});
});

jQuery(document).ready(function($){
    $(".accordion_example1, .accordion_example2").smk_Accordion();
});
    
$("#ex0 .mb_slider").mbSlider({
    formatValue: function(val){
        return val;
    }
});