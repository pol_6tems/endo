var $ = jQuery.noConflict();

function selectDia(selDia) {
    var aux = selDia.clone();
    if ($('[data-date="'+aux.toDate().getTime()+'"]').length == 0) {
        aux.subtract(1, 'hour');
    }
    
    return $('[data-date="'+aux.toDate().getTime()+'"]');
}

function pintarDies() {
    dies.forEach(function(e) {
        var $dia = selectDia(moment(e).add(2, 'hours'));
        $dia.addClass('available');
    });

    if (dia_inici && dia_fi) {
        pintarSeleccio(dia_inici, dia_fi);
    }
}

function pintarSeleccio(inici, fi) {
    var ini = inici.clone();
    for(var i = ini; i <= fi; i.add(1, 'day')) {
        var day = selectDia(i);
        day.addClass('highlight');
    }
}

$(window).on('load', function() {
    // flexslider
    $('#flexslider2').flexslider({
        animation:"slide",
        slideshowSpeed:5000,
        animationDuration: 3000,
        easing: "swing",
        directionNav: true,
        controlNav: false,
        pausePlay: false			          
    });		
});

$(function () {
    // Corregim estils de Summernote
    $('.description p').removeAttr('style');
    
    // Col·loquem el fons taronja a la ofeta seleccionada
    $('.bck-orange').closest('.offer').removeClass('bck-orange');
    $('[name="offer"]:checked').closest('.offer').addClass('bck-orange');

    $(".fancybox, .fancybox1").fancybox();

    $('#horizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#tabInfo');
            var $name = $('span', $info);

            $name.text($tab.text());

            $info.show();
        }
    });

    $('#verticalTab').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true
    });

    if (typeof dies !== 'undefined' && dies && typeof dateFirst !== 'undefined') {
        $('#retiro-reserva-form').on('submit', function (e) {
            var diaInput = $('input[name="dia"]');

            if (!diaInput.length || !diaInput.val()) {
                e.preventDefault();
                // TODO: alert?
                return false;
            }
        });

        $datepicker = $('.calendario').datepicker({
            dateFormat: 'dd/mm/yyyy',
            inline: true,
            language: 'es',
            minDate: dateFirst,
            updateViewDate: false,
            startDate: dateFirst,
            defaultViewDate: dateFirst,
            weekStart: 1,
            beforeShowDay: function(date) {
                setTimeout(function() { 
                    var date = moment(date).add('2', 'hours');
                    var dia = selectDia(date);
                    if (dies.includes(date.format('YYYY-MM-DD'))) {
                        dia.addClass('available');
                    } else {
                        dia.addClass('disabled');
                    }
                }, 200);
            },
        }).on('changeDate', function(date) {
            $('.highlight').removeClass('highlight');
            var data = moment(date.date).add(2, 'hours');
            $('input[name="dia"]').remove();

            if (dies.includes(data.format('YYYY-MM-DD'))) {                
                dia_inici = data.clone();
                dia_fi = data.clone().add(duracion - 1, 'days');
                
                pintarDies();

                $('.highlight').addClass('selected');                

                $('.calendario')
                   .closest('form')
                   .prepend(`<input type="hidden" name="dia" value="${dia_inici.format('DD/MM/YYYY')}@${dia_fi.format('DD/MM/YYYY')}" />`);
                   
                $('.str-pay-date').html(data.clone().subtract(pay_days_before, 'days').format('LL'));
                $('.str-cancel-date').html(data.clone().subtract(cancel_days_before, 'days').format('DD/MM/YYYY'));
            }
        }).on('changeMonth', function(date) {
            setTimeout(function() {
                pintarDies();
                selectDia(dia_inici).trigger('click');
            }, 200);
        });
        
        setTimeout(function() {
            pintarDies();
            selectDia(dia_inici).trigger('click');
        }, 200);
    }
});

$('.dat-select.cal .unic').on('click', function(e) {
    var date = $(this).data('date');
    $('#dia').remove();
    $('.dat-select.cal .unic.selected').removeClass('selected');
    $(this).addClass('selected');
    $(this).prepend(`<input id="dia" type="hidden" name="dia" value="${date}" />`);

    var startDate = date.split('@')[0];
    startDate = moment(startDate, 'DD/MM/YYYY');

    $('.str-pay-date').html(startDate.subtract(pay_days_before, 'days').format('LL'));
    $('.str-cancel-date').html(startDate.subtract(cancel_days_before, 'days').format('DD/MM/YYYY'));
});

$('.offer input').on('change', function(e) {
    e.stopPropagation();
    $('.offer.bck-orange').removeClass('bck-orange');
    $(this).closest('.offer').addClass('bck-orange')

    var editPrice = $(this).closest('.offer').find('.edit-price');

    var preu = editPrice.data('price');
    deposito = editPrice.data('deposit');

    var dharmas = $('#dharmas').val();

    if (!$('#chkdharm').get(0) || $('#chkdharm').get(0).checked) {
        dharmas = 0;
    }

    var mDeposito = Math.round(100 * (deposito - dharmas)) / 100;
    var total = Math.round(100 * (preu - dharmas)) / 100;

    $('#form_deposito').val(mDeposito);
    $('#total_deposito').html(mDeposito);
    $('#precio_total').html(`${total}`);
    $('#precio_total').attr('data-total', total)
    $('.get-more-dharmas').html(Math.round(total * toEuroRate));
    $('#total_resumen').html(Math.round(100 * (total - mDeposito)) / 100);
});

$('#chkdharm').on('change', function() {
    var preu = $('.offer.bck.bck-orange').find('.edit-price').data('price');
    var dharmas = $('#dharmas').val();
    if ($(this).get(0).checked) {
        dharmas = 0;
    }

    var mDeposito = Math.round(100 * (deposito - dharmas)) / 100;
    var total = Math.round(100 * (preu - dharmas)) / 100;

    $('#form_deposito').val(mDeposito);
    $('#total_deposito').html(mDeposito);
    $('#precio_total').html(`${total}`);
    $('#precio_total').attr('data-total', total);
    $('.get-more-dharmas').html(Math.round(total * toEuroRate));
    $('#total_resumen').html( Math.round(100 * (total - mDeposito)) / 100);
});


$('#submit_form').on('click', function(e) {
    e.preventDefault();
    $(this).closest('form').submit();
});



/* Calendar */
$(document).on("mouseenter", '.datepicker .day:not(.disabled)', function() {
    var dia = selectDia(moment($(this).data("date")));
    
    if (dia.hasClass('available')) {
        var actual = moment($(this).data("date"));
        var final = actual.clone().add(duracion - 1, "days");

        pintarSeleccio(actual, final);
    }
});

$(document).on("mouseleave", '.datepicker .day', function() {
    var dia = selectDia(moment($(this).data("date")));
    $('.highlight').removeClass('highlight');
    pintarDies();
});

$(".banner-load .demo-container").on('hover', function () {
        $(this).addClass("on_hover");
    }, function () {
        $(this).removeClass("on_hover");
    }
);