// Input Box Placeholder
function fnremove(arg,val)	{
	if (arg.value == '') {arg.value = val}
}	
function fnshow(arg,val)	{
	if (arg.value == val) {arg.value = ''}
}

function fixCurrencySelectorText() {
	var currencySelector = $('#currency-select-desk .sbHolder .sbSelector');
	var currencySelectorText = currencySelector.html();

	if (currencySelectorText) {
		currencySelectorText = currencySelectorText.split('-');
		currencySelector.html(currencySelectorText[0]);
	}
}

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
	e = e || window.event;
	if (e.preventDefault)
		e.preventDefault();
	e.returnValue = false;  
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
	if (window.addEventListener) // older FF
		window.addEventListener('DOMMouseScroll', preventDefault, false);
	document.addEventListener('wheel', preventDefault, {passive: false}); // Disable scrolling in Chrome
	window.onwheel = preventDefault; // modern standard
	window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
	window.ontouchmove  = preventDefault; // mobile
	document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    document.removeEventListener('wheel', preventDefault, {passive: false}); // Enable scrolling in Chrome
    window.onmousewheel = document.onmousewheel = null; 
    window.onwheel = null; 
    window.ontouchmove = null;  
    document.onkeydown = null;  
}

var scroll = true;
$('.logo-mbl').click(function(e) {
	e.preventDefault();
	if (scroll) disableScroll();
	else enableScroll();
	scroll = !scroll;
});

$('.toggle-btn').click(function(e) {
	e.preventDefault();
	var divider = $(this).closest('.divider');
	divider.toggleClass('open');

	var openTxt = $(this).data('open-txt');
	var closeTxt = $(this).data('closed-txt');

	if (typeof openTxt !== 'undefined' && typeof closeTxt !== 'undefined') {
		$(this).html(divider.hasClass('open') ? openTxt : closeTxt)
	}
});

$('.smooth-link').click(function(e) {
	e.preventDefault();
	$('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top - 60
	}, 800);
});

$("[type='submit']").click(function() {
	var form = $(this).closest('form');
	var requireds = form.find("input[required]");
	requireds.each(function() {
		if ($(this).attr('type') == 'checkbox') {
			if ( !$(this).prop('checked') ) {
				$(this).siblings('label').children('span').css("border", "2px solid red");
			} else {
				$(this).siblings('label').children('span').css("border", "1px solid #f9af02");
			}
		} else {
			if ($(this).val() == "") {
				$(this).css("border", "1px solid red");
			} else {
				$(this).css("border", "1px solid #f2f2f2");
			}
		}
	});
});

$(document).ready(function () {	
	// Sticky Header
	$("header.inner-header").sticky({topSpacing:0});
	
	$(".select_box").selectbox();

	fixCurrencySelectorText();

	$(".select_box").on('change', function () {
		fixCurrencySelectorText();
	});
	
	// Mmenu - Mobile Menu
	//$('#mobNav').mmenu();	
	$( ".mmoverly" ).appendTo( $( "body" ) );
   
   jsUpdateSize();
});

/*
$('#login').on('click', function(e) {
	e.preventDefault();
	$.ajax({
        url: ajaxURL,
        method: 'post',
        data: {
            action: 'loginUser',
            parameters: {
                email: $('#logEmail').val(),
				password: $('#logPassword').val()
            }
        },
        success: function(data) {
			location.reload();
			$.fancybox.close();
        }
    });
});
*/
$(window).resize(function(){	
	jsUpdateSize();
});
function jsUpdateSize()
{
	var width = $(window).width();
    var height = $(window).height();	
	if(width<=767)
	{
		$('.banner-home .flexslider .slides li').each(function()
		{
			var img = $('img', this).attr('src');
			$(this).css('background-image','url(' + img + ')');		
			$(".banner-home").css('height',415);
			$(this).css('height',415);		
		});
	}
	else{
		$('.banner-home .flexslider .slides li').each(function()
		{
			$(this).css('background-image','');
			$(".banner-home").css('height','auto');
			$(this).css('height','auto');
		});	
		
	}
	
	if(width<=767)
	{
		$('.banner-home1 .flexslider .slides li').each(function()
		{
			var img = $('img', this).attr('src');
			$(this).css('background-image','url(' + img + ')');		
			$(".banner-home1").css('height',530);
			$(this).css('height',530);		
		});
	}
	else{
		$('.banner-home1 .flexslider .slides li').each(function()
		{
			$(this).css('background-image','');
			$(".banner-home1").css('height','auto');
			$(this).css('height','auto');
		});	
		
	}
	if(width<=480)
	{
		$('.resp-banner .bann-img').each(function()
		{
			var img = $('img', this).attr('src');
			$(this).css('background-image','url(' + img + ')');		
			$(".bann-img").css('height',200);
			$(this).css('height',200);		
		});
	}
	else{
		$('.resp-banner .bann-img').each(function()
		{
			$(this).css('background-image','');
			$(".bann-img").css('height','auto');
			$(this).css('height','auto');
		});	
		
	}
	if(width<=767)
	{
		$('.necesit .necesit-img').each(function()
		{
			var img = $('img', this).attr('src');
			$(this).css('background-image','url(' + img + ')');		
			$(".necesit-img").css('height',260);
			$(this).css('height',260);		
		});
	}
	else{
		$('.necesit .necesit-img').each(function()
		{
			$(this).css('background-image','');
			$(".necesit-img").css('height','auto');
			$(this).css('height','auto');
		});	
		
	}
	
}

	/* mobile menu */
$('.logo-mbl').click(function(e){
	$('.mbl-menu').toggleClass("mbl-menuon");
});


$('.lang-mbl').click(function(e){
	$(this).next().toggleClass('show');
	$('.rs-div').removeClass('show');
});
$('.lang-rs').click(function(e){
	$(this).next().toggleClass('show');
	$('.lang-div').removeClass('show');
});


function fadeNumbers() {
    var interval = 3000;
    var count = 1;
	var max = $('.passos li.number').length;
    frame(count);
    //var id = setInterval(frame, interval);
    var step = interval / 3; // Numero d'elements a cada step

    function frame(mCount) {
		if (mCount > max) {
			return;
		}

		$('li[data-number="' + mCount + '"] .numero').css('opacity', '1');
		setTimeout(function() {
			$('li[data-number="' + mCount + '"] h2').css('opacity', '1');
			setTimeout(function() {
				$('li[data-number="' + mCount + '"]').addClass('next');
				setTimeout(function() {
					mCount++;
					frame(mCount)
				}, step);
			}, step);
		}, step);

    	/*console.log(count);
        if (count == 6) {
        	console.log('clear');
            clearInterval(id);
        } else {
            $('li[data-number="' + count + '"] .numero').css('opacity', '1');
            setTimeout(function() {
				console.log('show h2');
                $('li[data-number="' + count + '"] h2').css('opacity', '1');
                setTimeout(function() {
					console.log('show arrow');
                    $('li[data-number="' + count + '"]').addClass('next');
					count++;
                }, step);
            }, step);
        }*/
    }
}


function changeCurrency(value) {
	$.ajax({
		url: ajaxURL,
		method: 'POST',
		data: {
			action: 'setCurrencyCookie',
			parameters: {
				value: value
			}
		},
		success: function (data) {
			window.location.reload();
		},
		error: function (data) {
			console.log(data);
		}
	});
}

$('.currency_select').on('change', function () {
	changeCurrency($(this).val());
});

$('.login-reg-buttons a').on('click', function () {
	$('.js-reg-type[data-type="' + $(this).data('type') + '"]').click();
});