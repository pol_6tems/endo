var current = [];
var buscador_el = document.getElementById('buscador');
var filters_container = document.querySelectorAll('.retiros-fil.filters>.row1');
var filter_home = $(".buscador-results .filter-container.filters");
var buscador_results = document.querySelector('.buscador-results');

$('#buscador').focus(function(e) {
    $('.buscador-results').addClass('show');
});


function isDatePickerElement(className) {
    var classes = className.split(/\s/);
    for (var i = 0, len = classes.length; i < len; i++) {
        if (classes[i].startsWith('datepicker--')) {
            return true;
        }
    }

    return false;
}

function updateArribalAndDuration() {
    var duraciones = $('.searcher-duration.selected');
    var duracionInput = $('#duracion-y-llegada');
    var inputText = '';

    duraciones.each(function (index) {
        if (index === 0) {
            inputText = $(this).text();
        } else {
            inputText = inputText + ', ' + $(this).text();
        }
    });

    var date = '';
    if ($('.arrival-date.selected').length) {
        date = $('.arrival-date.selected').text();
    } else {
        var selectedCell = $('.datepicker--cell.datepicker--cell-day.-selected-');
        if (selectedCell.length) {
            date = [selectedCell.data('date'), (parseInt(selectedCell.data('month')) + 1), selectedCell.data('year')].join('/');
        }
    }

    if (date !== '') {
        if (inputText !== '') {
            inputText = inputText + ' | ' + date;
        } else {
            inputText = date;
        }
    }

    duracionInput.val(inputText);
}

$('.close-filters').click(function(e) {
    e.preventDefault();
    $(e.target).closest('.hide').removeClass('hide');
    $(e.target).closest('.show').removeClass('show');
});

if (buscador_el) {
    buscador_el.addEventListener('keyup', function(e) {
        var value = buscador_el.value;

        if (value !== '') {
            $('.buscador-results .busquedas-populares ul').hide();
            $('.buscador-results .busquedas-populares h2').css({opacity: 0});
            $('.buscador-results .accordion_in .acc_head').hide();
            $('.buscador-results .accordion_in .acc_content').css({'border-bottom': 'none'});
            if (! $('.buscador-results').hasClass('show') ) {
                $('.buscador-results').addClass('show');
            }
        } else {
            $('.buscador-results .busquedas-populares ul').show();
            $('.buscador-results .busquedas-populares h2').css({opacity: 1});
            $('.buscador-results .accordion_in .acc_head').show();
            $('.buscador-results .accordion_in .acc_content').css({'border-bottom': 'inherit'});
        }

        [...document.querySelectorAll('.buscador-results li')].filter(function(element) {
            element.classList.remove('hide');
            let aux = element.dataset.value;
            var index = aux.toLowerCase().indexOf(value.toLowerCase());
            if (index >= 0) {
                aux = aux.substring(0,index) + "<span class='highlight'>" + aux.substring(index,index+value.length) + "</span>" + aux.substring(index + value.length);
                element.innerHTML = aux;
                return true;
            } else {
                element.classList.add('hide');
            }
        });

        $('.accordion_in').each(function(i, e) {
            $(e).removeClass('hide');
            $(e).removeClass('acc_active');
            // $(e).find('.acc_head').hide();
            if ($(e).find('li:not(.hide)').length == 0) {
                $(e).addClass('hide');
            } else {
                $(e).addClass('acc_active');
                $(e).find('.acc_content').css('display', 'block');
            }
        });
    });
} else {
    $(filters_container).click(function(e) {
        var element = e.target;
        if ($(element).hasClass('tag')) {
            element = $(element).children().get(0);
        }
        
        if (element.tagName == 'SPAN') {
            var parent = element.parentNode;
            var post_name = parent.dataset.filter;
            var filter = $(`[data-post_name="${post_name}"]`);
            var cf = parent.dataset.cf;
    
            [url, metas] = process_url();
            var url2 = new URL($archive_retiros_url);
            entries = url.searchParams.getAll('filter[]');
            var locs = url.searchParams.getAll('loc[]');
    
            // Elimina els paràmetres que siguin el suq acabem de clicar
            entries = entries.filter(item => item !== post_name);
            entries.forEach(function(e) { url2.searchParams.append('filter[]', e); });
            locs = locs.filter(item => item !== post_name);
            locs.forEach(function(e) { url2.searchParams.append('loc[]', e); });
            
            window.history.pushState({},"", decodeURIComponent(url2.href));
    
            [url, metas] = process_url();
            $('.retiros-sec').addClass('loading');
            carregarRetiros(metas, function() {
                $('.retiros-sec').removeClass('loading');
                if (typeof filter !== 'undefined' && filter) {
                    filter.attr('checked', false);
    
                }
            });
    
            $(`.tag[data-filter="${post_name}"]`).remove();
    
            $(filter_home).each(function() {
                if ($(this).children().length == 0) {
                    $(this).addClass('hide');
                }
            });
        }
        e.preventDefault();
    });
}

if (buscador_results) {
    buscador_results.addEventListener('click', function(e) {
        var element = e.target;
        if (element.tagName == 'LI') {
            // Si no te cap element, afegim el tag d'eliminar tots
            if (filter_home.children.length == 0) {
                $(filter_home).append(`<div class="tag" data-filter="${post_name}">${value}<span>x</span></div>`);    
            }
            var post_name = element.dataset.filter;
            var value = element.dataset.value;
            var buscador = element.dataset.buscador;
            var custom_field = element.dataset.cf;

            if (!$(`.tag[data-filter="${post_name}"]`).length) {
                var tag = `<div class="tag" data-cf="${custom_field}" data-filter="${post_name}">${value}<span>x</span>`;

                if (typeof buscador !== 'undefined' && buscador) {
                    tag = tag + `<input type="hidden" name="buscador[]" value="${buscador}">`;
                }

                tag = tag + '</div>';

                $(filter_home).append(tag);
                $(filter_home).removeClass('hide');

                var filter = document.querySelector(`[data-post_name="${post_name}"]`);

                if (typeof filter !== 'undefined' && filter) {
                    filter.click();
                } else {
                    if (typeof metas !== 'undefined') {
                        metas.push([custom_field, 'like', `%"${buscador}"%`]);
                        metas_or.push(custom_field);
                        var url_aux = new URL(window.location.href);
                        url_aux.searchParams.append('filter[]', post_name);
                        window.history.pushState({},"", decodeURIComponent(url_aux));

                        $('.buscador-results').removeClass('show');

                        $('.retiros-sec').addClass('loading');
                        carregarRetiros(metas, function() {
                            $('.retiros-sec').removeClass('loading');
                        });
                    }
                }

                var searcherForm = $('#formbuscador');
                if (typeof searcherForm.data('is-home') !== "undefined") {
                    searcherForm.submit();
                }
            }
        }
    });
}


$(document).click(function(e) {
    if ($(e.target).closest('li.ser').length == 0) {
        if ($('.buscador-results').hasClass('show')) {
            $('.buscador-results').removeClass('show');
        }
    }
});

$(document).ready(function() {
    $('.search-accordion').smk_Accordion({
        closeAble: true,
        showIcon: true,
        mainclass: 'buscador-acc'
    });

    var datepickers = $('.datepicker-here');

    datepickers.datepicker({
        timepicker: false,
        language: lang,
        minDate: new Date()
    });

    datepickers.each(function () {
        var datepicker = $(this).data('datepicker');

        if (datepicker) {
            datepicker.update('onSelect', function (formattedDate, date, inst) {
                $(".post-date").each(function() {
                    $(this).prop('checked', false)
                });

                var url = new URL(window.location.href);

                if (typeof date === 'object') {
                    var month = String(date.getMonth() + 1);
                    var day = String(date.getDate());

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;

                    url.searchParams.set('date', [String(date.getFullYear()), month, day].join('-'));
                    url.searchParams.set('range', 3);
                } else {
                    var url2 = new URL(window.location.origin + window.location.pathname);
                    var mEntries = new URL(window.location.href).searchParams.getAll('filter[]');
                    // Elimina els paràmetres que siguin el suq acabem de clicar
                    mEntries.forEach(function(e) { url2.searchParams.append('filter[]', e); });
                    url = url2;
                }

                if (inst.$el.hasClass('mobile')) {
                    var $filters = inst.$el.closest('.filters');
                    $filters.removeClass('visible');
                    $filters.animate({
                        'left': '-=100%'
                    });
                    $("body.hide").removeClass("hide");
                }

                window.history.pushState({},"", decodeURIComponent(url.href));
                [url, metas] = process_url();
                window.history.pushState({},"", decodeURIComponent(url.href));

                $('.search-sec.search-hom ul li.date').find(".search-modal").removeClass('hide');

                $('.retiros-sec').addClass('loading');
                carregarRetiros(metas, function() {
                    $('.retiros-sec').removeClass('loading');
                });
            });
        }
    });
});

$('.search-accordion-toggle').click(function() {
    $(this).parent().toggleClass('active');
	$(this).parent().next().toggleClass('active');
});

$(document).on('click', '.duracion .search-accordion li', function(e) {
    e.preventDefault();
    if ($(this).hasClass('arrival-date')) {
        $('.arrival-date.selected').removeClass('selected');
        $(this).addClass('selected');
    } else {
        $(this).toggleClass('selected');
    }

    updateArribalAndDuration();

    if (!$(this).hasClass('arrival-date ')) {
        var searcherForm = $('#formbuscador');

        if (typeof searcherForm.data('is-home') !== "undefined") {
            searcherForm.submit();
        }
    }
});

$('select[name="range"]').on('change', function(e) {
    updateArribalAndDuration();

    var searcherForm = $('#formbuscador');

    if (typeof searcherForm.data('is-home') !== "undefined") {
        searcherForm.submit();
    }
    
    var url = new URL(window.location.href);
    url.searchParams.set('range', $(this).val());
    window.history.pushState({},"", decodeURIComponent(url.href));
    [url, metas] = process_url();

    $('.retiros-sec').addClass('loading');
    $("body").find(".search-modal").removeClass('hide');
    carregarRetiros(metas, function() {
        $('.retiros-sec').removeClass('loading');
    });
});

$(document).on('click', '.datepicker--cell.datepicker--cell-day', function(e) {
    $('.duracion .search-accordion li.arrival-date').removeClass('selected');

    updateArribalAndDuration();

    var searcherForm = $('#formbuscador');

    if (typeof searcherForm.data('is-home') !== "undefined") {
        searcherForm.submit();
    }
});

/*
$(document).on('click', '.arrival-date', function () {
    $('.sear-calendar li a[rel="15"]').click();
    var date = new Date($(this).data('date'));
    $('.sear-calendar .datepicker-here').data('datepicker').selectDate(date);

    var searcherForm = $('#formbuscador');

    if (typeof searcherForm.data('is-home') !== "undefined") {
        searcherForm.submit();
    }
});*/

$(document).on('click', function(event) {
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if (!$(event.target).closest('.es-pad').length && !isDatePickerElement(event.target.className)) {
        $("body").find(".search-modal").removeClass('hide');
    }
});

$(document).on('click', '.datepicker--nav-action', function(e) {
    e.stopPropagation();
});

$('#formbuscador').on('submit', function (e) {
    if ($('.arrival-date.selected').length) {
        e.preventDefault();
        var selectedCell = $('.datepicker--cell.datepicker--cell-day.-selected-');

        var duraciones = $('.searcher-duration.selected');
        var duracionInput = $('#duracion-y-llegada');

        var inputText = '';

        duraciones.each(function (index) {
            if (index === 0) {
                inputText = $(this).text();
            } else {
                inputText = inputText + ', ' + $(this).text();
            }
        });

        if (inputText !== '') {
            inputText = inputText + ' | ' + [selectedCell.data('date'), (parseInt(selectedCell.data('month')) + 1), selectedCell.data('year')].join('/');
        } else {
            inputText = [selectedCell.data('date'), (parseInt(selectedCell.data('month')) + 1), selectedCell.data('year')].join('/');
        }

        duracionInput.val(inputText);

        $(this).unbind('submit').submit();
    }
});