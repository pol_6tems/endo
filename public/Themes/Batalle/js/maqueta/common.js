// Input Box Placeholder
function fnremove(arg, val) {
    if (arg.value == '') { arg.value = val }
}

function fnshow(arg, val) {
    if (arg.value == val) { arg.value = '' }
}

$(document).ready(function() {
    $('.user-rht').click(function() {
        $('.user-popup').toggleClass('show');
        $('.user-rht').addClass('open');
    });
    /*$('.close-btn').click(function()
     {
       $('.user-popup').slideUp();
     });*/
});

$(document).ready(function() {
    // Sticky Header		
    //$("header").sticky({topSpacing:0});
    // Mmenu - Mobile Menu	
    //drmenu();

    if ($(window).width() > 1023) {
        $("header").sticky({ topSpacing: 0 });
    }
});

// $(window).resize(function() {
//   drmenu();
// });

// function drmenu(){
//   var width_pg = $(window).width();
//   console.log(width_pg);  

//   if(width_pg < 767){
// 	$('body').click(function() {
// 	  $('.active-drop').removeClass();
// 	});

// 	$('.menu').click(function(event){
// 	  event.stopPropagation();
// 	});
//   }

// }


$('body').click(function(e) {

    var target = $(e.target);

    //console.log(target.parents('.button-dropdown').length);

    if (!target.is('.m_menu')) {

        if (!target.parents('.button-dropdown').length) {

            if ($(window).width() < 767) {
                $('body').removeClass('active-drop');
            }

        }

    } else {

        if (!$('body').hasClass('active-drop')) {
            $('body').addClass('active-drop');
        }

    }
});

// if($(window).width() < 767 && $('.active-drop').length){
//   $('body').removeClass('active-drop'); 
// }

// sticky header
$(window).resize(function() {
    $("header").unstick();
    $(".sticky-wrapper").removeAttr('id');
    $(".sticky-wrapper").removeAttr("class");
    if ($(window).width() > 768) {
        $("header").sticky({ topSpacing: 0 });
    }
});

// mobile menu
//$('body').click(function() {
//   $('.active-drop').removeClass();
//});
//
//$('.menu').click(function(event){
//   event.stopPropagation();
//});




$(document).click(function(event) {
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if (!$(event.target).closest(".user-rht").length) {
        $("body").find(".user-popup").removeClass('show');
    }
});





/* jquery responsive */
$('.menu-item a.dropdown-toggle').click(function(e) {
    e.preventDefault();
    var $this = $(this);

    if ($this.hasClass("open")) {
        $this.removeClass("open");
    } else {

        $('.menu-item a.dropdown-toggle').removeClass("open");
        $this.addClass("open");

    }
    if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp();
    } else {
        $this.parent().parent().parent().find('li .dropdown-menu').removeClass('show');
        $this.parent().parent().parent().find('li .dropdown-menu').slideUp();
        $this.next().toggleClass('show');
        $this.next().slideToggle();
    }

});