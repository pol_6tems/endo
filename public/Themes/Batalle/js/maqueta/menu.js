var width1;

// Window Resize
$(window).resize(function(){
	MobileView();
});

$(document).ready(function(){
	
	// Window Resize Function
	MobileView();
	
	var defaultMainMenuHeight = $('.button-dropdown-int ul').innerHeight();
	var defaultscrollHeight = $('#divfocus').innerHeight();
	//$('#divfocus .row').css('height',defaultscrollHeight+'px');
		
	$('#m-menu').click(function(){
		//setTimeout(function(){
          if($('body').hasClass('active-drop'))
			{
			  $('body').removeClass('active-drop');  
			}else{
			  $('body').addClass('active-drop');
			  $('#m-menu-close').addClass('open');
			}		
		//}, 100);
	});
	$('#m-menu-close').click(function(){
		$('body').removeClass('active-drop');
		$('.button-dropdown-int ul li a').each(function(){
			$(this).next().removeClass('dropdown');	
			$(this).removeClass('active-menu');
			$(this).show();	
		});
		
		$('#divfocus').scrollTop(0);			
	});
	$('.btn-back').click(function(){
		$('.button-dropdown-int ul li a').each(function(){
			$(this).next().removeClass('dropdown');
			$(this).removeClass('active-menu');
			$(this).show();	
			$('.button-dropdown-int ul').css('height','auto');	
		});
	});
	
	//$('.button-dropdown-int ul > li > a').click(function(event){
//		
//		if($(this).closest( "a" ).attr('class')=='btn-back' || $(this).closest( "ul" ).attr('class')=='sub-menu')
//		{
//			event.stopPropagation();
//			return;			
//		}
//		
//		width1 = $(window).width();
//		var subMenuActiveClass='dropdown';
//		var objSubMenu=$(this).next();
//		var currentSubMenuClassName=$(this).next().attr('class');
//		if($(this).parent().children(".submenu").length == 1 && width1<639 )
//	    {
//		  $('.button-dropdown-int ul li a').css("display","none");
//		  $('.submenu a').css("display","block");
//	    }
//				
//		$('.button-dropdown-int ul li a').each(function(){
//			$(this).next().removeClass(subMenuActiveClass);	
//			$(this).removeClass('active-menu');
//			
//		});	
//				
//		if(currentSubMenuClassName!=subMenuActiveClass && currentSubMenuClassName!='submenu dropdown')
//		{
//			
//		   if(width1<639)
//		   {
//			 $(this).parent().parent().css('height',objSubMenu.innerHeight()+'px');   
//		   }else{
//		     if(objSubMenu.innerHeight()>defaultMainMenuHeight)
//		     {
//		       $(this).parent().parent().css('height',objSubMenu.innerHeight()+'px');
//		     }else{
//			   $(this).parent().parent().css('height',defaultMainMenuHeight+'px');
//		     }
//		   }
//		   
//		   $('#divfocus').animate({
//			  'scrollTop' :0
//		   },600);		   
//		  	   
//		   objSubMenu.addClass(subMenuActiveClass);
//		   $(this).addClass('active-menu'); 
//		}
//	});
});

function MobileView()
{
	
	var width = $(window).width();
    var height = $(window).height();
		
	if(width<639)
	{		
		if ($(".submenu").hasClass("dropdown"))
		{
			$('.button-dropdown-int ul li a').hide();
			$('.submenu a').show();
		}
		
	}
	else
	{ 
		$('.button-dropdown-int ul li a').css("display","block");
	}
}