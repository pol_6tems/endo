var $calendar;
var months = { '01': 'gener', '02': 'febrer', '03': 'març', '04': 'abril', '05': 'maig', '06': 'juny', '07': 'juliol', '08': 'agost', '09': 'setembre', '10': 'octubre', '11': 'novembre', '12': 'desembre' };
$(document).ready(function() {

    $('.show-pop').click(function() {
        $('#pop-info').addClass('p-open');
        $('body').addClass('overlay');
    });

    $('.close-btn').click(function() {
        $('body').removeClass('overlay');
        $('.simplePopup').removeClass('p-open');
    });

    /* 6TEMS MOD */
    var eventos_init = [];

    $.each($colors, function($color, $dies) {
        if ($dies.length > 0) {
            for (var $i = 0; $i <= $dies.length; $i++) {
                if (typeof($dies[$i]) !== 'undefined') {
                    var $start = $dies[$i]['start'];
                    var $end = $dies[$i]['end'];
                    if (typeof($start) !== 'undefined') {

                        // Semana o dia i afegir a end un dia mes
                        var semana_o_dia = 'dia';
                        if ($start != $end) {
                            semana_o_dia = 'semana';
                        }

                        eventos_init.push({
                            id: $start,
                            start: $start,
                            end: $end,
                            allDay: false,
                            className: "fc-event-" + $color,
                            editable: true,
                            eventDurationEditable: true,
                            estado: $color,
                            semana_o_dia: semana_o_dia,
                        });
                    }
                }
            }
        }
    });

    $calendar = $('#calendar');
    $calendar.fullCalendar({
        header: {
            left: 'prev,title, next',
        },
        dayNamesShortest: ['dg', 'dl', 'dt', 'dc', 'dj', 'dv', 'ds'],
        firstDay: 1,
        editable: true,
        viewRender: function(view) {
            $('#dropdown-title').text(view.name);
        },
        events: eventos_init
    });
    /* end 6TEMS MOD */

    $(".tab-red a").click();
    eRed = $colors.red.map(function(f) { return 'red-' + f.start + '-' + f.year });
    eBlue = $colors.blue.map(function(f) { return 'blue-' + f.start + '-' + f.year });
    eGreen = $colors.green.map(function(f) { return 'green-' + f.start + '-' + f.year });
    eYellow = $colors.yellow.map(function(f) { return 'yellow-' + f.start + '-' + f.year });
    eOrange = $colors.orange.map(function(f) { return 'orange-' + f.start + '-' + f.year });

    $('.dies_pendents_blue').html(pendents_blue);
    eRed.sort(SortByDate);
    eRed.forEach(function(dia) {
        var a = dia.split('-');
        addTabList(a[1] + '-' + a[2] + '-' + a[3], a[0], true);
    });

    var tots = eBlue.concat(eGreen).concat(eYellow).concat(eOrange);
    tots.sort(SortByDate);
    tots.forEach(function(dia) {
        var a = dia.split('-');
        addTabList(a[1] + '-' + a[2] + '-' + a[3], a[0], true, a[4]);
    });

    updateTabs(); // Initiated to display the calendar year in TABS

    $(window).resize(function() {
        var width_pg = $(window).width();
        setTimeout(function() {
            if (width_pg > 767) {
                $('.tab_out_calender').show();
                $('.tab_cal .nav-tabs').show();
                // $('.tab_calender').show();
                $('.mbl_backward').hide();
            } else {
                //if(!$('.tab_calender').is(":visible")){
                $('.tab_out_calender').show();
                $('.tab_cal .nav-tabs').hide();
                // $('.tab_calender').hide();
                $('.mbl_backward').show();
                //}
            }
        }, 100);
        removeCal(1);
    });

    // mobile dropdown
    $("#hotel-name, .arw-toggle").click(function() {
        $("#hotel-list").slideToggle();
        $(".dropdown").slideUp();
    });
    /*
    $("#hotel-list li").click(function() {
        $("#hotel-name").html($(this).html());
        $("#hotel-list").slideUp();
    });*/
    $('.nav-tabs > li > a').click(function(event) {
        event.preventDefault(); //stop browser to take action for clicked anchor
        //get displaying tab content jQuery selector
        var active_tab_selector = $('.nav-tabs > li.active > a').attr('href');
        //find actived navigation and remove 'active' css
        var actived_nav = $('.nav-tabs > li.active');
        actived_nav.removeClass('active');
        //add 'active' css into clicked navigation
        $(this).parents('li').addClass('active');
        //hide displaying tab content
        $(active_tab_selector).removeClass('active');
        $(active_tab_selector).addClass('hide');
        //show target tab content
        var target_tab_selector = $(this).attr('href');
        $(target_tab_selector).removeClass('hide');
        $(target_tab_selector).addClass('active');
    });
    $('.nav-tabs1 > li > a').click(function(event) {
        event.preventDefault(); //stop browser to take action for clicked anchor
        //get displaying tab content jQuery selector
        var active_tab_selector = $('.nav-tabs1 > li.active > a').attr('href');
        //find actived navigation and remove 'active' css
        var actived_nav = $('.nav-tabs1 > li.active');
        actived_nav.removeClass('active');
        //add 'active' css into clicked navigation
        $(this).parents('li').addClass('active');
        //hide displaying tab content
        $(active_tab_selector).removeClass('active');
        $(active_tab_selector).addClass('hide');
        //show target tab content
        var target_tab_selector = $(this).attr('href');
        $(target_tab_selector).removeClass('hide');
        $(target_tab_selector).addClass('active');
    });
});

function getCalender(e) {
    $('html, body').animate({
      scrollTop: $('.tab_calender').offset().top
    });
    $('a[href="#tab1"]').trigger('click')
}

function getMessage(e) {
    $('html, body').animate({
      scrollTop: $('.tab_calender').offset().top
    });
    $('a[href="#tab2"]').trigger('click')
}

function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate.clone();
    var stop = moment(stopDate);
    while (currentDate <= stop) {
        if (!busca_evento(currentDate, true) && currentDate.format('Y-MM-DD') > moment().format('Y-MM-DD')) {
            dateArray.push(currentDate.clone());
        }
        currentDate = currentDate.add(1, 'd');
    }
    return dateArray;
}

// Converteix una data MomentJS a format string
function date_to_str(d) {
    return d.format('Y-MM-DD');
}

function busca_evento(dia, si_red = false) {
    dia_str = date_to_str(dia);
    eventos = $('#calendar').fullCalendar('clientEvents', dia_str);
    if (eventos.length > 0) {
        evt = eventos[0];
        if (evt.estado == 'green' || evt.estado == 'yellow' || (evt.estado == 'red' && si_red) || evt.estado == 'grey') {
            return true;
        } else if (dia.format('Y-MM-DD') == moment().format('Y-MM-DD')) {
            return true;
        } else if (eYellow.indexOf(dia_str) >= 0 || (eRed.indexOf(dia_str) >= 0 && si_red)) {
            return true;
        } else {
            return false;
        }
    } else if (eYellow.indexOf(dia_str) >= 0 || (eRed.indexOf(dia_str) >= 0 && si_red)) {
        return true;
    } else return false;
}

function scrollToBottom() {
    $(".mCustomScrollbar").mCustomScrollbar("scrollTo", "bottom", { scrollInertia: 0 });
}

$(function() {
    // DayClick
    $(".fc-widget-content").click(function() {

        if ($bloquejar_enviar_peticio) return;

        if ($(this).hasClass("fc-other-month")) return true;

        var add = !$(this).hasClass('fc-event-blue') && !$(this).hasClass('fc-event-pink') && !$(this).hasClass('fc-event-red') && !$(this).hasClass('fc-event-yellow') && !$(this).hasClass('fc-event-orange');
        var tipus_cela = '';
        if ($(this).hasClass('fc-event-blue')) tipus_cela = 'blue';
        else if ($(this).hasClass('fc-event-pink')) tipus_cela = 'pink';
        else if ($(this).hasClass('fc-event-orange')) tipus_cela = 'orange';

        var date = moment($(this).attr('data-day').replace('fc-day-', ''));
        //if (date < moment()) return true;

        var afegit = false;
        var semana_o_dia = '';
        eventos = $('#calendar').fullCalendar('clientEvents', date_to_str(moment(date)));
        if (eventos.length > 0) {
            evt = eventos[0];
            semana_o_dia = evt.semana_o_dia;
        }

        var aquest_any = moment().startOf('year'); // Aquest any és en veritat l'any anterior
        var any_seguent = aquest_any.clone().add(1, 'y');
        var any_seguent_2 = aquest_any.clone().add(2, 'y');
        var $tipus = 'fc-event-' + tab_actual;
        var data_limit_pink = aquest_any.clone().endOf('month');
        var data_limit_blue = aquest_any.clone();
        var data_limit_orange = any_seguent.clone();
        var pendents_aux = 0;

        if (tab_actual == 'pink') {
            pendents_aux = pendents_pink;
            if (date > data_limit_pink) return; // data_maxim 31 gener del següent any
            if (date < moment()) return; // data_maxim 31 gener del següent any
        } else if (tab_actual == 'blue') {
            pendents_aux = pendents_blue;
            if (date < moment() || date < data_limit_blue || date > any_seguent.clone().endOf('month')) return; // data_maxim 31 gener del següent any
        } else if (tab_actual == 'orange') {
            pendents_aux = pendents_orange;
            if (date < moment() || date < data_limit_orange || date > any_seguent_2.clone().endOf('month')) return; // data_maxim 31 gener del següent any
        } else if (add) return;

        // Si no en tinc de pendents i cal afegir-ne un, abortem
        if (pendents_aux <= 0 && add) return;

        var eventos = $('#calendar').fullCalendar('clientEvents');
        var actual = moment(date).clone();
        var dilluns = actual.clone().startOf('isoweek');
        var divendres = actual.clone().endOf('isoweek').subtract(2, 'd');

        var dies = getDates(dilluns, divendres);

        if ((dies.length > pendents_aux && add) || $sel_dia_o_semana == 'dia') {
            dies = [dies.find(function(d) { return d.format('Y-MM-DD') == actual.format('Y-MM-DD'); })];
            semana_o_dia = 'dia';
        } else if (add) {
            semana_o_dia = 'semana';
        }

        if (add) {
            // La Cel·la no conté cap event actiu, caldrà afegir-lo
            if (semana_o_dia == 'semana') {
                dies.forEach(function(d) {
                    $('#calendar').fullCalendar('renderEvent', {
                        id: date_to_str(d),
                        start: d.clone().format(),
                        end: d.clone().add(1, 'd').format(),
                        allDay: false,
                        className: $tipus,
                        editable: true,
                        estado: tab_actual,
                        semana_o_dia: semana_o_dia,
                    }, true);
                });
            } else {
                $('#calendar').fullCalendar('renderEvent', {
                    id: date_to_str(actual),
                    start: actual.clone().format(),
                    end: actual.clone().add(1, 'd').format(),
                    allDay: false,
                    className: $tipus,
                    editable: true,
                    estado: tab_actual,
                    semana_o_dia: semana_o_dia,
                }, true);
            }
        } else {
            // La cel·la ja està sel·leccionada, pertant caldrà eliminar-ne el contingut.
            var events = [];
            $.each(dies, function(index, id) {
                id_str = date_to_str(id);
                events = events.concat($('#calendar').fullCalendar('clientEvents', id_str));
                $('#calendar').fullCalendar('removeEvents', id_str);
                $('.fc-widget-content.fc-day-' + id_str).removeClass('fc-event-blue');
                $('.fc-widget-content.fc-day-' + id_str).removeClass('fc-event-pink');
                $('.fc-widget-content.fc-day-' + id_str).removeClass('fc-event-orange');
                $('li.dies_fc-event-' + tipus_cela + '_' + id_str).remove();
            });
        }
        var dies_no_contables = 0;
        var dies_a_canviar = 0;
        $.each(dies, function(idx, value) {
            id_str = date_to_str(value);
            tipus_cela_aux = $tipus;
            if (!add) {
                if (!events.find(function(f) { return f.id == id_str })) return true;
                dies_a_canviar++;
                tipus_cela_aux = 'fc-event-' + tipus_cela;
            }

            if (!busca_evento(value, true)) {
                if ( tab_actual == 'orange' ) addTabList(id_str, 'orange', add);
                else addTabList(id_str, 'blue', add);
                // processSel(date_to_str(value), tipus_cela_aux, add);
            }
            if (busca_evento(value, true)) dies_no_contables++;
        });

        if ((add && tab_actual == 'pink' && afegit) || (!add && tipus_cela == 'pink')) {
            if (add) pendents_pink -= dies.length - dies_no_contables;
            else pendents_pink += dies_a_canviar - dies_no_contables;

            $('.dies_pendents_pink').html(pendents_pink);
            $('.tab-pink a span:not(.tooltiptext)').html(pendents_pink);
        } else if ((add && tab_actual == 'blue') || (!add && tipus_cela == 'blue')) {
            if (add) pendents_blue -= dies.length - dies_no_contables;
            else pendents_blue += dies_a_canviar - dies_no_contables;

            $('.dies_pendents_blue').html(pendents_blue);
            $('.tab-blue a span:not(.tooltiptext)').html(pendents_blue);
        } else if ((add && tab_actual == 'orange') || (!add && tipus_cela == 'orange')) {
            if (add) pendents_orange -= dies.length - dies_no_contables;
            else pendents_orange += dies_a_canviar - dies_no_contables;

            $('.dies_pendents_orange').html(pendents_orange);
            $('.tab-orange a span:not(.tooltiptext)').html(pendents_orange);
        }
        getDies();
    });
    $(".fc-button-next").click(function() {
        updateTabs();
    });
    $(".fc-button-prev").click(function() {
        updateTabs();
    });
    $(".fc-widget-content").mouseenter(function() {
        if (($(this).hasClass("fc-event-pink") || $(this).hasClass("fc-event-blue") || $(this).hasClass("fc-event-red") || $(this).hasClass("fc-event-yellow") || $(this).hasClass("fc-event-green")) &&
            !$(this).hasClass("tooltip") && !$(this).hasClass("fc-event-orange")) {
            var tipText = '',
                year = $('#calendar').fullCalendar('getDate').getFullYear();
            $(this).hasClass("fc-event-pink") &&
                (tipText = 'Dies escollit ' + year),
                $(this).hasClass("fc-event-blue") &&
                (tipText = 'Dies solicitats ' + year),
                $(this).hasClass("fc-event-red") &&
                (tipText = 'Dies festius ' + year),
                $(this).hasClass("fc-event-yellow") &&
                (tipText = 'Dies denegats ' + year),
                $(this).hasClass("fc-event-green") &&
                (tipText = 'Dies aprovats ' + year);

            $(this).find("div:first").append('<div class="tooltip"><span class="tooltiptext">' + tipText + '</span></div>');
            $(this).addClass("tooltip");
        }
    });
    $(".fc-widget-content").mouseleave(function() {
        $(this).hasClass("tooltip") &&
            ($(this).find(".tooltip").remove(), $(this).removeClass("tooltip"))
    });
});

function updateTabs() {
    var year = $('#calendar').fullCalendar('getDate').getFullYear();

    /* 6TEMS fix fletxes canvi any */
    var aquest_any = new Date().getFullYear() - 1; // Aquest any és en veritat l'any anterior
    if (year <= aquest_any) {
        $('#calendar').find('.fc-button-prev').hide();
    } else {
        $('#calendar').find('.fc-button-prev').show();
    }

    // Acceptem desde 2018 fins a 2020
    if (year >= aquest_any + 3) {
        $('#calendar').find('.fc-button-next').hide();
    } else {
        $('#calendar').find('.fc-button-next').show();
    }

    /* end 6TEMS fix fletxes canvi any */
    $('.ultimate-div .tab-content ul li').css("display", "block");

    // Actualitza la informació per any de la 
    $('.ultimate-div #tab6.tab-content ul li').css("display", "none");
    $('.ultimate-div #tab6.tab-content ul li.yr_' + moment().format('Y')).css("display", "block");
    $('.ultimate-div a#tab6 .tooltip span').html($('.ultimate-div #tab6.tab-content ul li.yr_' + moment().format('Y')).length);
}

function addTabList(date, color, add, year = null) {
    var tab;
    if (color == 'red') tab = '#tab6';
    else if (color == 'green') tab = '#tab5';
    else if (color == 'blue') tab = '#tab5';
    else if (color == 'yellow') tab = '#tab5';
    else if (color == 'orange') tab = '#tab7';

    if ( year != null && !isNaN(year) ) {
        aquest_any = new Date().getFullYear();
        if ( year < aquest_any ) tab = '#tab4';
        else if ( year == aquest_any ) tab = '#tab5';
        else if ( year > aquest_any ) tab = '#tab7';
    }

    var a = date.split('-');
    if (add) {
        $(tab + ' ul').append('<li class="yr_' + a[0] + ' dies_' + color + '_' + date + '">' + a[2] + ' ' + months[a[1]] + ' ' + a[0] + ' <span class="' + color + '"></span></li>');
    } else {
        $(tab + ' ul li.yr_' + a[0] + '.dies_' + color + '_' + date).remove();
    }
}

function SortByDate(a, b) {
    var amyDate = a.split("-");
    var aNewDate = new Date(amyDate[1] + "," + amyDate[2] + "," + amyDate[0]).getTime();
    var bmyDate = b.split("-");
    var bNewDate = new Date(bmyDate[1] + "," + bmyDate[2] + "," + bmyDate[0]).getTime();
    return ((aNewDate < bNewDate) ? -1 : ((aNewDate > bNewDate) ? 1 : 0));
}


function removeCal(back) {
    $('.tab_out_calender').show();
    $('.tab_cal .nav-tabs').show();
    // $('.tab_calender').hide();
    if (back) {
        $('.cal_info').trigger('click');
    }
    $('.mbl_backward').hide();
}

function afterLd() {
    var width_pg = $(window).width();
    if (width_pg < 768) {
        // $('.tab_calender').hide();
    }
}

var tab_actual;
$(document).ready(function() {
    $('.tab-pad #pink').click(function() {
        tab_actual = 'pink';
    });
    $('.tab-pad #blue').click(function() {
        tab_actual = 'blue';
    });
    $('.tab-pad #orange').click(function() {
        tab_actual = 'orange';
    });
});

function getDies() {
    // var dies = (pendents_pink + pendents_blue) - (eBlue.length + ePink.length);
    var dies = pendents_blue + pendents_pink + pendents_orange;
    $('.dies h2 span').html(dies + ' dies');
}

$(window).load(function() {
    afterLd();
    getDies();
});