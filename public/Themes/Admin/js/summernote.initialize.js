function textareaKeyDown(e, max) {
    var t = e.currentTarget.innerText;
    if (t.trim().length >= max) {
    //delete key
    if (e.keyCode != 8)
        e.preventDefault();
    // add other keys ...
    }
}

function textareaKeyup(e, max, element) {
    if (element) {
        var t = e.currentTarget.value.length;

        var current = ((max) - t)*100/(max);
        element.attr('aria-valuenow', (400 - max))
        element.css('width', current+'%')

    } else {
        var t = e.currentTarget.innerText;
        var lang = $(e.currentTarget).closest('.form-group').children('textarea').attr('data-lang');

        $('#descripcio_count_'+lang).text(t.length)
    }
}

// Module Name is AutoLink
// @param {Object} context - states of editor
$.extend($.summernote.plugins, {
    myModule: function (context) {
        // you can get current editor's elements from layoutInfo
        var layoutInfo = context.layoutInfo;
        var $toolbar = layoutInfo.toolbar;

        // ui is a set of renderers to build ui elements.
        var ui = $.summernote.ui;
    
        // this method will be called when editor is initialized by $('..').summernote();
        // You can attach events and created elements on editor elements(eg, editable, ...).
        this.initialize = function () {
            // create button
            var button = ui.button({
                className: 'note-btn-bold',
                contents: '<i class="fa fa-picture-o" aria-hidden="true"></i>',
                click: function (e) {
                    method = 'insert';
                    show_media_modal(context.$note);
                }
            });
        
            // generate jQuery element from button instance.
            this.$button = button.render();
            $toolbar.append(this.$button);
        }
    
        // this method will be called when editor is destroyed by $('..').summernote('destroy');
        // You should detach events and remove elements on `initialize`.
        this.destroy = function () {
            this.$button.remove();
            this.$button = null;
        }
    }
});


/* FUNCIÓ PER REGISTRAR SUMMERNOTE */
function registerSummernote(element, placeholder, max, lang) {
    $(element).summernote({
        maxHeight: 650,
        minHeight: 340,
        toolbar: [
			["style", ["style"]],
			["style", ["bold", "italic", "underline", "clear"]],
			["fontsize", ["fontsize"]],
			["color", ["color"]],
			["para", ["ul", "ol", "paragraph"]],
			["height", ["height"]],
            ["insert", ["link", "video", "hr"]],
            ['view', ['fullscreen', 'codeview']],
			["help", ["help"]]
        ],
        cleaner:{
            action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
            newline: '<br>', // Summernote's default is to use '<p><br></p>'
            notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
            icon: '<i class="note-icon">[Your Button]</i>',
            keepHtml: false, // Remove all Html formats
            keepOnlyTags: ['<p>', '<br>', '<ul>', '<li>', '<b>', '<strong>','<i>', '<a>'], // If keepHtml is true, remove all tags except these
            keepClasses: false, // Remove Classes
            badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
            badAttributes: ['style', 'start'], // Remove attributes from remaining tags
            limitChars: false, // 0/false|# 0/false disables option
            limitDisplay: 'both', // text|html|both
            limitStop: false // true/false
        },
        styleTags: ['p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
        codemirror: { // codemirror options
            theme: 'monokai'
        },
        lang: lang,
        dialogsInBody: true,
        popover: {
            image: [],
            link: [],
            air: []
        },
        placeholder,
        callbacks: {
            onKeydown: textareaKeyDown,
            onKeyup: textareaKeyup,
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                console.log(bufferText);
                document.execCommand('insertText', false, bufferText);
            }
        }
    });
}