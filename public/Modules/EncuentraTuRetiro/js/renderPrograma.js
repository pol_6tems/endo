onmessage = function (e) {
    var data = e.data;

    // Contenidors
    var pills = [];
    var dies = [];

    for (var i = data.pestanyesActuals; i < data.numDies + 1; i++) {
        pills.push(`
            <li class="nav-item added">
                <a data-toggle="tab" class="nav-link" href="#dies${i}" role="tablist">Dia ${i}</a>
            </li>
        `);

        dies.push(`
            <div id="dies${i}" data-dia="${i}" class="pestanya tab-pane fade in added" role="tabpanel">
                <select class="selectpicker"></select>
                <button class="duplicate_field btn btn-md btn-info"><i class="material-icons">file_copy</i></button>
                <br><br>
                <div class="activitats"></div>
                <hr>
                <div class="adding-input input-group mb-3" style="justify-content: flex-end;">
                    <button class="add_field btn btn-primary btn-sm" data-dia="${i}">Añadir</button>
                </div>
            </div>
        `);
    }

	options = '<select class="selectpicker">';
	for(var k = 1; k <= data.numDies; k++) {
		options += `<option value="dies${k}">Dia ${k}</option>`;
	}
	options += '</select>';

    data.options = options;
    data.pills = pills.join('');
    data.dies = dies.join('');

    postMessage(data);
};