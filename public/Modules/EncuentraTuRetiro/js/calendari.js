/**
 * @author Oriol Testart <oriol.testart@6tems.com>
 * @copyright 6TEMS Comunicació Interactiva <6tems.com>
 * @create Fri, 08 March 2019
 * @file calendari.js
 * 
 * --
 *  
 * @param {tipus} -> Tipus actual a la DB, es modificarà en l'execució
 * 
 * @param field_type -> Tipus del actual tipus de dades (disponibilitat)
 * 
 * @param cal_dies -> Conjunt de dies disponibles que es mostraràn al calendari
 * 
 * @param duracio -> Número de dies sel·leccionats al mateix temps.
 *
 * Eventos Únicos: La sel·lecció serà la duració, només es podrà fer una vegada
 * Eventos Repetitivos: La sel·lecció serà la duració, es podrà fer tantes vegades com es vulgui
 * Serie de dias fijo: La sel·lecció serà de 1, la duració del retir s'especifica a part
 * Serie de dias variable: La sel·lecció serà amb rang, la duració del retir s'especifica a part
 */

/**
 * Funció que transforma números de només 1 xifra a 2 xifres
 * ex: 2 -> 02
 * 
 * @param {integer} d 
 */
function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}

function removeDups(names) {
    let unique = {};
    names.forEach(function(i) {
        if(!unique[i]) {
        unique[i] = true;
        }
    });
    return Object.keys(unique);
}

function chunk(arr, chunkSize) {
    var R = [];
    for (var i=0,len=arr.length; i<len; i+=chunkSize)
        R.push(arr.slice(i,i+chunkSize));
    return R;
}

Object.defineProperty(Array.prototype, 'chunk', {
    value: function(chunkSize) {
        var R = [];
        for (var i=0; i<this.length; i+=chunkSize)
            R.push(this.slice(i,i+chunkSize));
        return R;
    }
});

function calendari(selector, dies, tipus) {
    /**
     * Definim les constants. Canviar en cas de migració o modificació de l'id del tipus de reserva.
     */
    var calendar;

    var tipus_unico = '1',
        tipus_repet = '2',
        tipus_serief = '3',
        tipus_seriev = '4';

    function pintarBorde(date) {
        var current = moment(date);
        var actual;

        for (var i = 0; i < duracio; i++) {
            actual = obtenirDia(current.format('Y-MM-DD'));

            var prevDay = obtenirDia(current.clone().subtract(1, 'd').format('Y-MM-DD'));
            var nextDay = obtenirDia(current.clone().add(1, 'd').format('Y-MM-DD'));

            var nextWeek = obtenirDia(current.clone().add(1, 'w').format('Y-MM-DD'));
            var prevWeek = obtenirDia(current.clone().subtract(1, 'w').format('Y-MM-DD'));


            if (actual.attr('dia-start') != prevDay.attr('dia-start')) {
                actual.addClass('border-left');
            }

            if (actual.attr('dia-start') != nextDay.attr('dia-start')) {
                actual.addClass('border-bottom');
                actual.addClass('border-right');
            }

            if (actual.attr('dia-start') != prevWeek.attr('dia-start')) {
                actual.addClass('border-top');
            }

            if (actual.attr('dia-start') != nextWeek.attr('dia-start')) {
                actual.addClass('border-bottom');
            }
            current.add(1, 'd');
        }
    }

    /**
     * Agafa una data en el format especificat i retorna l'element
     * del dia en questió encapsulat en format JQuery
     * 
     * @param {data} dia en format 'YYYY-MM-DD'
     */
    function obtenirDia(dia) {
        return $(selector + ' .day[data-dia="' + dia + '"]');
    }

    function clear() {
        $(selector).find('input').remove();
        $(selector).find('.selected').removeClass('selected');
        $(selector).find('.disabled').removeClass('disabled');
        
        clearBorder();
        $("#calendar_input").val("");
        // Border
        dies.length = 0;
    }

    function clearBorder() {
        $(selector + ' .day:not(.selected).border-bottom').removeClass('border-bottom');
        $(selector + ' .day:not(.selected).border-top').removeClass('border-top');
        $(selector + ' .day:not(.selected).border-left').removeClass('border-left');
        $(selector + ' .day:not(.selected).border-right').removeClass('border-right');
    }

    /**
     * Event Listeners
     */
    $('button[data-role="clear"]').click(function(e) {
        e.preventDefault();
        clear();
    });

    $inputs = $('.calendar_selector, .duracion');
    $inputs.focusout(function(e) {
        e.preventDefault();
        $inputs.val($(this).val());
        duracio = this.value;
        renderitzarPrograma();
    });

    $('.tipus').change(function(e) {
        e.preventDefault();
        tipus = this.value;
        if (tipus == tipus_serief) {
            $('.calendar_selector').val(1);
            $('.duracion, .calendar_selector').unbind('change');
            duracio = 1;
        } else {
            $('.calendar_selector').val($('.duracion').val());
            duracio = $('.duracion').val();
            var $inputs = $('.duracion, .calendar_selector');
            $inputs.change(function(e) {
                e.preventDefault();
                $inputs.val($(this).val());
                duracio = this.value;
            });
        }

        if (tipus == tipus_seriev) {
            calendar.setEnableRangeSelection(true);
            $('.field-group.dias').append(`
                <span class="bmd-form-group minmax">
                    <div class="input-group suffix">
                        <input type="number" style="margin-top: 10px;" class="form-control" min="1" step="1" name="custom_fields[${lang}][${post.id}][duracion][min]" placeholder="Min" onhover="(function (e) {e.preventDefault();})(event);">
                        <span class="input-group-addon" style="border: 1px solid #d8d8d8;background: #f6f6f6;margin-top: 10px;">Min</span>
                    </div>
                </span>
            `);
            $('.field-group.dias').append(`
                <span class="bmd-form-group minmax">
                    <div class="input-group suffix">
                        <input type="number" style="margin-top: 10px;" class="form-control" min="1" step="1" name="custom_fields[${lang}][${post.id}][duracion][max]" placeholder="Max" onhover="(function (e) {e.preventDefault();})(event);">
                        <span class="input-group-addon" style="border: 1px solid #d8d8d8;background: #f6f6f6;margin-top: 10px;">Max</span>
                    </div>
                </span>
            `);
        } else {
            $('.minmax').remove();
        }
        clear();
    });


    /**
     * Aquesta funció es cridarà cada vegada que es cliqui a un dia
     * 
     * @param {jquery-calendar-event} e 
     */
    function onDayClick(e) {
        if (tipus != tipus_seriev) {
            // Obtenim els seleccionats
            lastAdded = $(selector + ' .hovered').map(function(idx, element) { return $(element).attr('data-dia'); });
            if (lastAdded.length > 0 && lastAdded.length < duracio) {
                for(var i = lastAdded.length; i < duracio; i++) {
                    var aux = moment(lastAdded[lastAdded.length - 1]).add(1, 'd');
                    lastAdded.push(aux.format('Y-MM-DD'));
                }
            }
            
            // Si tenim sel·leccionat tipus repetitiu
            if (tipus != tipus_unico) {
                dies = $.merge(dies, lastAdded);
            } else {
                dies = $.merge([], lastAdded);
            }

            dies = removeDups(dies);
            dies.sort();
            $('#calendar_input').val(JSON.stringify(dies));
            calendar.setDisabledDays(); // Force refresh
        }
    }

    /**
     * Aquesta funció es cridarà cada vegada que el cursor entri dins
     * de un dia
     * 
     * @param {jquery-calendar-event} e 
     */
    function onMouseOnDay(e) {
        if (tipus == tipus_seriev && duracio != "") return;
        
        var current = moment(e.date);
        var diaInici = current.clone().format('Y-MM-DD');

        
        if (dies.length < 1 || tipus != tipus_unico) {
            // Col·loquem classe hovered al dia actual
            var actual = obtenirDia(current.format('Y-MM-DD')).addClass('hovered');
            var inici = obtenirDia(current.format('Y-MM-DD')).addClass('hovered');

            actual.attr('dia-start', diaInici);
            
            // Per cada un dels següents dies especificats en la duració, col·loquem la classe hovered a cada un d'ells
            for (var i = 1; i < duracio; i++) {
                current.add(1, 'd');
                var aux = obtenirDia(current.format('Y-MM-DD'));
                if (aux.length) {
                    actual = aux.addClass('hovered');
                    actual.attr('dia-start', diaInici);
                }
            }

            if (actual.hasClass('selected') || inici.hasClass('selected')) {
                $(selector + ' .hovered').addClass('canceled');
                $(selector + ' .hovered').removeClass('hovered');
            } else {
                pintarBorde(e.date);
            }
        }
    }

    /**
     * Aquesta funció es cridarà cada vegada que el cursor marxi de
     * de un dia
     */
    function onMouseOutDay() {
        $(selector + ' .hovered[dia-start]').attr('dia-start','');
        clearBorder();
        $(selector + ' .canceled').removeClass('canceled');
        $(selector + ' .hovered').removeClass('hovered');
    }

    /**
     * Aquesta funció es crida cada vegada que es renderitzarà de nou
     * el calendari
     */
    function onRender(calendari) {
        
        var currentYear = calendari.currentYear;

        var startYear = moment(currentYear.toString() + "-01-01").startOf('year');
        var endYear = moment(currentYear.toString() + "-01-01").endOf('year');
        var retiros = (tipus == tipus_seriev) ? dies : (tipus == tipus_serief) ? chunk(dies, 1) : chunk(dies, parseInt(duracio));

        for (var i = startYear; i <= endYear; i.add(1, 'd')) {
            var mes = parseInt(i.format('M')) - 1,
                dia = parseInt(i.format('D')) - 1,
                mesEl = $($(selector + ' .month-container').get(mes));

            var aux = mesEl.find('.day .day-content').filter(function(el) {
                return el == dia;
            });

            aux.parent().attr('data-dia', i.format('Y-MM-DD'));
        }

        if (dies.length > 0) {
            if (tipus == tipus_unico) { // Evento Único
                $(selector + ' .day:not(.old):not(.new)').addClass('disabled');
            }

            for (i = 0; i < dies.length; i++) {
                obtenirDia(dies[i]).addClass('selected');
            }

            for (i = 0; i < retiros.length; i++) {
                for (var j = 0; j < retiros[i].length; j++) {
                    if (tipus == tipus_serief) {
                        obtenirDia(retiros[i][j]).attr('dia-start', retiros[i][j]);
                    } else {
                        obtenirDia(retiros[i][j]).attr('dia-start', retiros[i][0]);
                    }
                }
                pintarBorde(retiros[i][0]);
            }
        }
    }

    function onSelected(e) {
        if (tipus == tipus_seriev) {
            var startDay = moment(e.startDate);
            var endDay = moment(e.endDate);

            for(var i = startDay; i <= endDay; i.add(1, 'd')) {
                var dia = obtenirDia(i.format('Y-MM-DD'));
                dia.addClass('selected');
            }
            dies = $('.selected').map(function(idx, element) { return $(element).attr('data-dia'); });
            pintarBorde(startDay.format('Y-MM-DD'));
        }
    }

    // Inicialització del calendari
    calendar = $(selector).calendar({
        enableContextMenu: true,
        language: 'es',
        enableRangeSelection: true,
        startYear: new Date().getFullYear(),
        clickDay: onDayClick,
        minDate: new Date(),
        mouseOnDay: onMouseOnDay,
        mouseOutDay: onMouseOutDay,
        renderEnd: onRender,
        selectRange: onSelected,
    });
}