Number.prototype.pad = function (size) {
	var sign = Math.sign(this) === -1 ? '-' : '';
	return sign + new Array(size).concat([Math.abs(this)]).join('0').slice(-size);
}

var rep = false;
var dies = document.querySelectorAll('.pestanya').length + 1;

function afegirActivitat(e) {
	e.preventDefault();

	var $dia = $(e.target).data('dia');
	var $contenidor = $(e.target).closest('.pestanya');
	var $activitats = $contenidor.find('.activitats');

	var new_idx = 0;
	$activitats.find('input.act').each(function() {
		var value = parseInt($(this).data('order'));
		new_idx = (value > new_idx) ? value : new_idx;
	});
	new_idx++;

	// Faltaria marcar que falta la data

	$('#dies' + $dia + ' .adding-input .programa.text-muted').remove();
	$('.added_' + $dia + '.repetit').css('background-color', 'inherit');

	$activitats.append(`
		<div class="added_${$dia} input-group mb-1">
			<div class="input-group-prepend">
				<input type="text" class="clockpicker hora" name="${name}[${$dia}][${new_idx}][hora]" value="" required/>
			</div>
			<input
				type="text"
				class="form-control act"
				id="basic-url"
				data-order="${new_idx}"
				name="${name}[${$dia}][${new_idx}][valor]"
				required />

			<button class="remove_field btn btn-sm btn-danger" style="padding: 0 15px;">
				<i class="fa fa-minus" aria-hidden="true"></i>
			</button>
		</div>
	`);

	$(`.added_${$dia} .clockpicker`).clockpicker({
		placement: 'top',
		align: 'left',
		afterDone: function() {
			var actividades = $activitats.children().sort(function (a, b) {
				var elA = $(a).find('.hora').val();
				var elB = $(b).find('.hora').val();
				if (elA != '' && elB != '') return false;
				return ((Date.parse('01/01/2000 ' + elA + ':00') < Date.parse('01/01/2000 ' + elB + ':00')) ? -1 : 1);
			}).clone();

			actividades.each(function(i, e) {
				$(e).attr('data-order', i);
				$(e).find('input').each(function(idx, el) {
					var name = $(el).attr('name');
					name = name.split('[');
					name[5] = (i + 1) + ']';
					name = name.join('[');
					$(el).attr('name', name);
				});
			});

			$activitats.html(actividades);
		},
		autoclose: true
	});
	
	$(this).prev().val('');
	$(this).prev().focus();
}

function enterKey(e) {
	if (e.which == 13) {
		e.preventDefault();
		$(e.target).parent('.input-group')
			.find('.add_field')
			.trigger('click');
	}
}


function eliminarDia(e) {
	e.preventDefault();
	$pestanya = $(this).parents('.pestanya').remove();
	$('a[href="#' + $pestanya.attr('id') + '"]').parent().remove();

	var idx = parseInt($pestanya.attr('id').split('dies')[1]) - 1;

	$('a[href="#dies' + idx + '"]').trigger('click');
}


/**
 * Quan entrem dins la pestanya de programa, agafem el valor del camp
 * Duración del retiro, i mostrem els dies equivalents amb el que s'hi
 * especifica
 */
$(".programa .nav.nav-pills").on("show.bs.tab", function (e) {
	// Variable emplenada a la pestana de Fechas y Disponibilidad
	$($(e.relatedTarget).attr('href')).removeClass('active').removeClass('show');
	$($(e.target).attr('href')).addClass('active').addClass('show');
});

function renderitzarPrograma() {
	var numDies = parseInt(duracio);
	var pestanyesActuals = $('.pestanya').length + 1;

	if (numDies > 30) numDies = 30;
	
	/**
	 * Eliminem els dies que sobren
	 */
	if (pestanyesActuals > numDies) {
		for (var i = pestanyesActuals; i > numDies; i--) {
			$('a[href="#dies' + i + '"]').parent().remove();
			$("#dies" + i).remove();
		}
	}

	// Generem l'HTML a un Worker separat del thread Principal per evitar bloqueig
	// de la finestra principal i habilitar la paral·lelització
	var myWorker = new Worker(workerFile);
	myWorker.postMessage({
		pestanyesActuals,
		numDies
	});

	myWorker.addEventListener("message", function (e) {
		var data = e.data

		$('.programa .nav.nav-pills').append(data.pills);
		$('.programa .tab-content').append(data.dies);
		document.querySelectorAll('.pestanya .selectpicker').forEach(function(el) {
			el.innerHTML = data.options;
		});
	
		$('.pestanya .selectpicker').selectpicker('refresh');
	});
}

$(document).on('click', '.duplicate_field', function(e) {
	e.preventDefault();

	var $selector = $(this).siblings('.dropdown.bootstrap-select').children('select');
	var $targetContainer = $('#'+$selector.val());
	var $currentContainer = $selector.closest('.pestanya');
	var $activitats = $targetContainer.find('.activitats').clone();

	var $currentDay = $currentContainer.data('dia');

	$activitats.find('input').each(function(idx, el) {
		var name = el.name;
		name = name.split('[');
		name[4] = $currentDay + ']';
		name = name.join('[');
		el.name = name;
	});

	// PENDENT
	$currentContainer.find('.activitats').html($activitats.html());
});

$(function () {
	renderitzarPrograma();
});