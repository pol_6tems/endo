#### Entorn de desenvolupament Windows

* Instal·lar Virtual Box[https://www.virtualbox.org/wiki/Downloads]
* Instal·lar Vagrant[https://www.vagrantup.com/downloads.html]
* Afegir la màquina virtual de Homestead a Vagrant

``vagrant box add laravel/homestead``

* Clonar el repositori de Homestead 

``git clone https://github.com/laravel/homestead.git``

* Fer checkout de la versió estable (de vegades la ``master`` no es estable)

``git checkout v7.16.1``

* A dins del directori del Homestead, executar el script d'inicialització

``bash init.sh``

* Configurar el fitxer ``Homestead.yaml`` per tal de mapejar la carpeta del projecte a configurar. Per exemple (posar en format YAML): 

``folders: - map: ../endo to: /home/vagrant/code``

`` sites: - map: endo.test to: /home/vagrant/code/public ``

* Afegir el domini al fitxer ``C:\Windows\System32\drivers\etc\hosts``

``192.168.10.10  endo.test``

* Fer un ``vagrant up`` per aixecar la màquina virtual
> Si falla per temes de SSH, cal generar una clau SSH nova anant a ~/.ssh i executant ``ssh-keygen -t rsa -C "correu@example.com"``

* Definir un fitxer ``.env`` a l'arrel del projecte amb les dades del fiter ``.env.example``

* Definir un fitxer ``.htaccess`` a la carpeta ``public``  del projecte amb les dades del fiter ``htaccess_sample``

* Fer un ``composer install`` a l'arrel del projecte per instal·lar totes les dependències de PHP

* Fer un ``npm install`` a l'arrel del projecte per instal·lar totes les dependències de JS

* Fer un ``php artisan storage:link`` per tal de crear el soft-link de la carpeta on es pujaran les imatges