let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');*/
mix.setPublicPath('public/Themes/' + process.env.PROJECT_NAME);

if (process.env.PROJECT_NAME === 'ArrayPlastics' || process.env.PROJECT_NAME === 'Prodaisa') {
    mix.copy('node_modules/blueimp-file-upload/js/jquery.fileupload.js', 'public/Themes/' + process.env.PROJECT_NAME + '/js')
        .copy('node_modules/blueimp-file-upload/js/jquery.iframe-transport.js', 'public/Themes/' + process.env.PROJECT_NAME + '/js')
        .copy('node_modules/blueimp-file-upload/js/vendor/jquery.ui.widget.js', 'public/Themes/' + process.env.PROJECT_NAME + '/js/vendor');
}

mix.js('resources/themes/' + process.env.PROJECT_NAME + '/assets/js/app.js', 'js')
    .sass('resources/themes/' + process.env.PROJECT_NAME + '/assets/sass/app.scss', 'css')
    .options({
        processCssUrls: false
    })
    .version();

if (process.env.PROJECT_NAME === 'FormacioiTreball') {
    mix.js('resources/themes/' + process.env.PROJECT_NAME + '/admin/assets/js/app.js', 'admin/js')
        .sass('resources/themes/' + process.env.PROJECT_NAME + '/admin/assets/sass/app.scss', 'admin/css')
        .options({
            processCssUrls: false
        })
        .version();
}