Number.prototype.pad = function(size) {
    var sign = Math.sign(this) === -1 ? '-' : '';
    return sign + new Array(size).concat([Math.abs(this)]).join('0').slice(-size);
  }
  
  var rep = false;
  var dies = document.querySelectorAll('.pestanya').length + 1;
  
  function afegirActivitat(e) {
      e.preventDefault();
      var $datetime = $(e.target).parent('.input-group').find('.data');
      var $input = $(e.target).parent('.input-group').find('.activitat');
      var $contenidor = $(e.target).parents('.pestanya');
      var $dia = $input.attr('data-dia');
      var $field_type = $(this).attr('data-field');
  
      $repetit = $('.added_'+$dia).filter(function(i, data) {
        return $datetime.val() == $(data).find('.hora').val();
      });
  
      if ($repetit.length > 0 && !rep) {
          $repetit.each(function(i, e) {
            $(e).addClass('repetit');
            $(e).css('background-color', '#ff3160');
          });
  
          $('#dies'+$dia+' .adding-input .input-group-prepend').after(`<p class="programa text-muted">No se pueden repetir horas en un mismo dia</p>`);
          rep = true;
      }
  
      // Faltaria marcar que falta la data
      if ($input.val() && $input.val().length > 10 && $datetime.val() !== '' && $repetit.length == 0) {
          $('#dies'+$dia+' .adding-input .programa.text-muted').remove();
          $('.added_'+$dia+'.repetit').css('background-color', 'inherit');
  
          $contenidor.prepend(`
            <div class="added_${$dia} input-group mb-1">
              <div class="input-group-prepend">
                <input type="hidden" class="hora" name="${$dia}_${$field_type}_hora_${n[$dia]}" value="${$datetime.val()}">
                <span class="input-group-addon" style="display: inline-block;border-right: 0px;">${$datetime.val()}</span>
              </div>
              <input
                      type="text"
                      class="form-control"
                      id="basic-url"
                      name="${$dia}_${$field_type}_activitat_${n[$dia]}"
                      value="${$input.val()}"
                      readonly />
  
              <button class="remove_field btn btn-sm btn-danger" style="padding: 0 15px;">
                  <i class="fa fa-minus" aria-hidden="true"></i>
              </button>
            </div>
          `);
  
          auxHora = parseInt($datetime.val().split(':')[0]);
          var hora =  ((auxHora < 23) ? (auxHora+1) : 0);
          hora = hora.pad(2).toString().concat(':00');
  
          var added = $('.added_'+$dia).sort(function(a, b) {
              var elA = $(a).find('.input-group-addon').text();
              var elB = $(b).find('.input-group-addon').text();
              return ((Date.parse('01/01/2000 '+elA+':00') < Date.parse('01/01/2000 '+elB+':00')) ? 1 : -1);
          }).clone();
  
          $('.added_'+$dia).remove();
          $contenidor.prepend(added);
  
          $(this).parents('.adding-input').find('.data').val(hora);
          $(this).prev().val('');
          $(this).prev().focus();
          n[$dia]++;
      }
  }
  
  function afegirDia(e) {
      e.preventDefault();
      $('.tab-content').append(`
        <div id="dies${dies}" class="pestanya tab-pane fade">
          <div class="adding-input input-group mb-3" style="margin-bottom: 10px;">
            <div class="input-group-prepend">
              <input
                  type="text"
                  class="data form-control"
                  placeholder="12:00"
                  data-locale="{{ field_type.locale ?: config('app.locale') }}"
                  data-provides="anomaly.field_type.datetime"
                  data-step="1"
                  data-input-mode="time"
                  data-datetime-format="H:i"
                  data-alt-format="H:i"
                  value="" />
            </div>
  
            <input
                  type="text"
                  class="activitat form-control"
                  placeholder="Actividad"
                  data-dia="${dies}"/>
            <button class="add_field btn btn-sm btn-info">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
          </div>
          <div class="controls">
            <div class="buttons">
              <!-- <button class="remove_day btn btn-sm btn-danger" disabled>Eliminar Dies</button> -->
            </div>
          </div>
        </div>
      `);
      $(`<li class="nav-item links"><a data-toggle="tab" class="nav-link" href="#dies${dies}">Dia ${dies}</a></li>`).insertBefore($(this).parent());
  
      picker(window, document);
      n[dies] = 1;
      $('a[href="#dies' + dies + '"]').trigger('click');
      dies++;
  }
  
  function enterKey(e) {
    if(e.which == 13) {
      e.preventDefault();
      $(e.target).parent('.input-group')
                 .find('.add_field')
                 .trigger('click');
    }
  }
  
  
  function eliminarDia(e) {
    e.preventDefault();
    $pestanya = $(this).parents('.pestanya').remove();
    $('a[href="#' + $pestanya.attr('id') + '"]').parent().remove();
  
    var idx = parseInt($pestanya.attr('id').split('dies')[1]) - 1;
  
    $('a[href="#dies' + idx + '"]').trigger('click');
  }
  
  $('.nav.nav-sections ').on('show.bs.tab', function (e) {
    // Variable emplenada a la pestana de Fechas y Disponibilidad
    var numDies = parseInt(duracio);
    var pestanyesActuals = $('.pestanya').length + 1;
    
    for (var i = pestanyesActuals; i < numDies + 1; i++) {
      $(".programa .nav.nav-sections").append(`
        <li class="nav-item links">
          <a data-toggle="tab" class="nav-link" href="#dies${i}">Dia ${i}</a>
        </li>
      `);
  
      $(".programa .tab-content").append(`
        <div id="dies${i}" class="pestanya tab-pane fade in">
          <div class="adding-input input-group mb-3" style="margin-bottom: 10px;">
            <div class="input-group-prepend">
              <input
                  type="text"
                  class="data form-control"
                  placeholder="12:00"
                  data-locale="${field_type.locale ? config('app.locale'):'es'}"
                  data-provides="anomaly.field_type.datetime"
                  data-step="1"
                  data-input-mode="time"
                  data-datetime-format="H:i"
                  data-alt-format="H:i"
                  value="12:00" />
            </div>
  
            <input
                type="text"
                class="activitat form-control"
                placeholder="Actividad"
                data-dia="${i}" />
  
            <button data-field="${field_type.input_name}" class="add_field btn btn-sm btn-info">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
          </div>
          <div class="controls">
            <div class="buttons">
              <!-- <button class="remove_day btn btn-sm btn-danger" disabled>Eliminar Dies</button> -->
            </div>
          </div>
        </div>
      `);
    }
  
    /**
     * Eliminem els dies que sobren
     */
    if (pestanyesActuals > numDies) {
      for(var i = pestanyesActuals; i > numDies; i--) {
        $('a[href="#dies'+ i + '"]').parent().remove();
        $("#dies" + i).remove();
      }
    }
  });