@extends('Admin::layouts.admin')

@section('section-title')
    @Lang('Modules')
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card ">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">extension</i>
                </div>
                <h4 class="card-title">@Lang('Edit')</h4>
            </div>
            <form id="form" class="form-horizontal" action="{{ route('admin.modules.update', $item->id) }}" method="post">
                <div class="card-body ">
                    <input type="hidden" name="_method" value="PUT">    
                    {{ csrf_field() }}
                    <div class="row">
                        <label class="col-md-3 col-form-label">@Lang('Name')</label>
                        <div class="col-md-9">
                            <div class="form-group has-default">
                                <input name="name" type="text" class="form-control" value="{{ $item->name }}" required tabindex=1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="mr-auto">
                        <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" name="previous" value="@Lang('Tornar')" onclick="window.location.href='{{ route('admin.modules.index') }}'">
                    </div>
                    <div class="ml-auto">
                        <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="@Lang('Update')">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">g_translate</i>
                </div>
                <h4 class="card-title">
                    @Lang('Translations')
                </h4>
            </div>
            <div class="card-body">
                <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                    <form class="form-inline" method="POST" action="{{ route('admin.modules.translations.create', $item->id) }}">
                        {!! csrf_field() !!}
                        <div class="form-group mr-4">
                            <label for="key" class="bmd-label-floating">Key:</label>
                            <input type="text" name="key" class="form-control" placeholder="">
                        </div>
                        <div class="form-group mr-4">
                            <label for="value" class="bmd-label-floating">Value</label>
                            <input type="text" name="value" class="form-control" placeholder="">
                        </div>
                        <span class="form-group bmd-form-group"> <!-- needed to match padding for floating labels -->
                            <button type="submit" class="btn btn-default btn-raised">Add</button>
                        </span>
                    </form>
                    <ul class="nav nav-pills nav-pills-primary" style="margin: 20px auto;">
                        <li class="nav-item active">
                            <a class="nav-link active" data-toggle="tab" href="#traducciones">@Lang('Translations')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#rutas">@Lang('Routes')</a>
                        </li>
                    </ul>
                </div>
                <div class="material-datatables">
                    <div class="tab-content">
                        <div id="traducciones" class="tab-pane active show">
                            <table id="tabla-traducciones" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Key</th>
                                    @if($languages->count() > 0)
                                        @foreach($languages as $language)
                                            <th>{{ __($language->name) }}({{ $language->code }})</th>
                                        @endforeach
                                    @endif
                                    <th width="80px;">@Lang('Action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if($columnsCount > 0)
                                        @foreach($columns[0] as $columnKey => $columnValue)
                                            @if ( substr( $columnKey, 0, 1 ) !== "/" )
                                            <tr>
                                                <td><a href="#" class="translate-key" data-title="Enter Key" data-type="text" data-pk="{{ $columnKey }}" data-url="{{ route('admin.modules.translations.update.json.key', $item->id) }}">{{ $columnKey }}</a></td>
                                                @for($i=1; $i<=$columnsCount; ++$i)
                                                <td><a href="#" data-title="@Lang('Enter Translate')" class="translate" data-code="{{ $columns[$i]['lang'] }}" data-type="textarea" data-pk="{{ $columnKey }}" data-url="{{ route('admin.modules.translations.update.json', $item->id) }}">{{ isset($columns[$i]['data'][$columnKey]) ? $columns[$i]['data'][$columnKey] : '' }}</a></td>
                                                @endfor
                                                <td>
                                                    <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger" data-action="{{ route('admin.modules.translations.destroy', [$item->id, base64_encode($columnKey)]) }}">
                                                        <i class="material-icons">close</i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div id="rutas" class="tab-pane">
                            <table id="tabla-rutas" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Key</th>
                                    @if($languages->count() > 0)
                                        @foreach($languages as $language)
                                            <th>{{ __($language->name) }}({{ $language->code }})</th>
                                        @endforeach
                                    @endif
                                    <th width="80px;">@Lang('Action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if($columnsCount > 0)
                                        @foreach($columns[0] as $columnKey => $columnValue)
                                            @if ( substr( $columnKey, 0, 1 ) === "/" )
                                            <tr>
                                                <td><a href="#" class="translate-key" data-title="Enter Key" data-type="text" data-pk="{{ $columnKey }}" data-url="{{ route('admin.modules.translations.update.json.key', $item->id) }}">{{ $columnKey }}</a></td>
                                                @for($i=1; $i<=$columnsCount; ++$i)
                                                <td><a href="#" data-title="@Lang('Enter Translate')" class="translate" data-code="{{ $columns[$i]['lang'] }}" data-type="textarea" data-pk="{{ $columnKey }}" data-url="{{ route('admin.modules.translations.update.json', $item->id) }}">{{ isset($columns[$i]['data'][$columnKey]) ? $columns[$i]['data'][$columnKey] : '' }}</a></td>
                                                @endfor
                                                <td>
                                                    <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger" data-action="{{ route('admin.modules.translations.destroy', [$item->id, base64_encode($columnKey)]) }}">
                                                        <i class="material-icons">close</i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link href="{{asset($_admin . 'css/bootstrap-editable.css')}}" rel="stylesheet" />
@endsection

@section('scripts')
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-editable.min.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.translate').editable({
        params: function(params) {
            params.code = $(this).editable().data('code');
            return params;
        }
    });


    $('.translate-key').editable({
        validate: function(value) {
            if($.trim(value) == '') {
                return 'Key is required';
            }
        }
    });


    $('body').on('click', '.remove-key', function(){
        var cObj = $(this);


        if ( confirm("@Lang('Are you sure to delete it?')") ) {
            $.ajax({
                url: cObj.data('action'),
                method: 'DELETE',
                success: function(data) {
                    cObj.parents("tr").remove();
                    $.snackbar({content: "@Lang('Translation deleted')", timeout: 3000});
                }
            });
        }
    });
</script>
<script>
$(document).ready(function() {
    $('.table').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "@Lang('All')"]
        ],
        responsive: true,
        language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" }
    });

    var table = $('.table').DataTable();
});
</script>
@endsection