<link rel='stylesheet' id='dashicons-css'  href="{{asset($_admin . 'css/dashicons.min.css')}}" type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href="{{asset($_admin . 'css/admin-bar.min.css')}}" type='text/css' media='all' />
<style>
#wpadminbar .menupop:hover .ab-sub-wrapper { display:block; }
#wpadminbar #wp-admin-bar-my-account.with-avatar>.ab-empty-item img, #wpadminbar #wp-admin-bar-my-account.with-avatar>a img {
    border: none;
    background: none;
}
</style>

<div id="wpadminbar" class="">
	<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">
        Admin
    </a>
	<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Barra d'eines">
		<ul id="wp-admin-bar-root-default" class="ab-top-menu">
			<li id="wp-admin-bar-site-name" class="menupop">
				<a class="ab-item" aria-haspopup="true" href="{{ route('admin') }}">
                @if ( empty($_config['app_name']['value']) )
                    @php( $app_name = config('app.name', 'Endo') )
                @else
                    @php( $app_name = $_config['app_name']['value'] )
                @endif
                {{ $app_name }}
                </a>
            </li>
            @if ( $_te_post )
                <li id="wp-admin-bar-edit">
                    <a class="ab-item" href="{{ route('admin.posts.edit', $_item->id) . '?post_type=' . $_item->type }}">
                        @Lang('Edit') @Lang('Page')
                    </a>
                </li>
            @endif
            <li id="wp-admin-bar-cache">
                <a class="ab-item" href="{{ route('admin.cache.clear') }}">
                    @Lang("Clear Cache")
                </a>
            </li>
		</ul>
		<ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
			<li id="wp-admin-bar-search" class="admin-bar-search">
				<div class="ab-item ab-empty-item" tabindex="-1">
					<form action="{{ route('admin.search.index') }}" method="get" id="adminbarsearch">
                        <input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150">
                        <label for="adminbar-search" class="screen-reader-text">@Lang('Search')</label>
                        <input type="submit" class="adminbar-button" value="@Lang('Search')">
                    </form>
				</div>
			</li>
			<li id="wp-admin-bar-my-account" class="menupop with-avatar">
				<a class="ab-item" aria-haspopup="true" href="{{ route('admin.users.edit', $_current_user->id) }}">
                    @Lang('Hello'), <span class="display-name">{{ $_current_user->name }}</span>
                    <img alt="" src="{{ $_current_user->get_avatar_url() }}" srcset="{{ $_current_user->get_avatar_url() }}" class="avatar avatar-26 photo" height="26" width="26">
                </a>
				<div class="ab-sub-wrapper">
					<ul id="wp-admin-bar-user-actions" class="ab-submenu">
						<li id="wp-admin-bar-user-info">
                            <a class="ab-item" tabindex="-1" href="{{ route('admin.users.edit', $_current_user->id) }}">
                                <img alt="" src="{{ $_current_user->get_avatar_url() }}" class="avatar avatar-64 photo" height="64" width="64">
                                <span class="display-name">

                                </span>
                            </a>
                        </li>
						<li id="wp-admin-bar-edit-profile">
                            <a class="ab-item" href="{{ route('admin.users.edit', $_current_user->id) }}">
                                @Lang('Edit Profile')
                            </a>
                        </li>
						<li id="wp-admin-bar-logout">
                            <a class="ab-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                @Lang('Logout')
                            </a>
                        </li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
	<a class="screen-reader-shortcut" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        @Lang('Logout')
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</div>
<div class="margin"></div>
<style>
.margin,
.filter-menu {margin-top: 32px}

@media screen and (max-width: 782px) {
    .margin,
    .filter-menu {margin-top: 46px}
}
</style>