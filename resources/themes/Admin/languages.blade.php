@extends('Admin::layouts.admin')

@section('section-title')
    @Lang('Translations')
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">g_translate</i>
                </div>
                <h4 class="card-title">
                    @Lang('Translations')
                </h4>
            </div>
            <div class="card-body">
                <div class="toolbar">
                    <form class="form-inline" method="POST" action="{{ route('translations.create') }}">
                        {!! csrf_field() !!}
                        <div class="form-group mr-4">
                            <label for="key" class="bmd-label-floating">Key:</label>
                            <input type="text" name="key" class="form-control" placeholder="">
                        </div>
                        <div class="form-group mr-4">
                            <label for="value" class="bmd-label-floating">Value</label>
                            <input type="text" name="value" class="form-control" placeholder="">
                        </div>
                        <span class="form-group bmd-form-group"> <!-- needed to match padding for floating labels -->
                            <button type="submit" class="btn btn-default btn-raised">Add</button>
                        </span>
                    </form>

                    <ul class="nav nav-pills nav-pills-primary" style="margin: 20px auto;">
                        <li class="nav-item active">
                            <a class="nav-link active" data-toggle="tab" href="#traducciones">@Lang('Translations')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#rutas">@Lang('Routes')</a>
                        </li>
                    </ul>
                </div>
                <div class="material-datatables">
                <div class="tab-content">
                    <div id="traducciones" class="tab-pane active show">
                        <table id="tabla-traducciones" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                            <tr>
                                <th>Key</th>
                                @if($languages->count() > 0)
                                    @foreach($languages as $language)
                                        <th>{{ __($language->name) }}({{ $language->code }})</th>
                                    @endforeach
                                @endif
                                <th width="80px;">@Lang('Action')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($columnsCount > 0)
                                    @foreach($columns[0] as $columnKey => $columnValue)
                                        @if ( substr( $columnKey, 0, 1 ) !== "/" )
                                        <tr>
                                            <td><a href="#" class="translate-key" data-title="Enter Key" data-type="text" data-pk="{{ $columnKey }}" data-url="{{ route('translation.update.json.key') }}">{{ $columnKey }}</a></td>
                                            @for($i=1; $i<=$columnsCount; ++$i)
                                            <td><a href="#" data-title="@Lang('Enter Translate')" class="translate" data-code="{{ $columns[$i]['lang'] }}" data-type="textarea" data-pk="{{ $columnKey }}" data-url="{{ route('translation.update.json') }}">{{ isset($columns[$i]['data'][$columnKey]) ? $columns[$i]['data'][$columnKey] : '' }}</a></td>
                                            @endfor
                                            <td>
                                                <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger" data-action="{{ route('translations.destroy', base64_encode($columnKey)) }}">
                                                    <i class="material-icons">close</i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endif 
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div id="rutas" class="tab-pane fade">
                        <table id="tabla-rutas" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                            <tr>
                                <th>Key</th>
                                @if($languages->count() > 0)
                                    @foreach($languages as $language)
                                        <th>{{ __($language->name) }}({{ $language->code }})</th>
                                    @endforeach
                                @endif
                                <th width="80px;">@Lang('Action')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($columnsCount > 0)
                                    @foreach($columns[0] as $columnKey => $columnValue)
                                        @if ( substr( $columnKey, 0, 1 ) === "/" )
                                        <tr>
                                            <td><a href="#" class="translate-key" data-title="Enter Key" data-type="text" data-pk="{{ $columnKey }}" data-url="{{ route('translation.update.json.key') }}">{{ $columnKey }}</a></td>
                                            @for($i=1; $i<=$columnsCount; ++$i)
                                            <td><a href="#" data-title="@Lang('Enter Translate')" class="translate" data-code="{{ $columns[$i]['lang'] }}" data-type="textarea" data-pk="{{ $columnKey }}" data-url="{{ route('translation.update.json') }}">{{ isset($columns[$i]['data'][$columnKey]) ? $columns[$i]['data'][$columnKey] : '' }}</a></td>
                                            @endfor
                                            <td>
                                                <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger" data-action="{{ route('translations.destroy', base64_encode($columnKey)) }}">
                                                    <i class="material-icons">close</i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link href="{{asset($_admin . 'css/bootstrap-editable.css')}}" rel="stylesheet" />
@endsection

@section('scripts')
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-editable.min.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.translate').editable({
        params: function(params) {
            params.code = $(this).editable().data('code');
            return params;
        }
    });


    $('.translate-key').editable({
        validate: function(value) {
            if($.trim(value) == '') {
                return 'Key is required';
            }
        }
    });


    $('body').on('click', '.remove-key', function(){
        var cObj = $(this);


        if ( confirm("@Lang('Are you sure to delete it?')") ) {
            $.ajax({
                url: cObj.data('action'),
                method: 'DELETE',
                success: function(data) {
                    cObj.parents("tr").remove();
                    $.snackbar({content: "@Lang('Translation deleted')", timeout: 3000});
                }
            });
        }
    });
</script>
<script>
$(document).ready(function() {
    $('.table').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "@Lang('All')"]
        ],
        responsive: true,
        language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" }
    });

    var table = $('.table').DataTable();
});
</script>
@endsection