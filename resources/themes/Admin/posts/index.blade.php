@extends('Admin::layouts.admin')

@section('section-title')
    @Lang(ucfirst($post_type_plural))
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">library_books</i>
                </div>
                <h4 class="card-title">
                    @Lang(ucfirst($post_type_plural))
                    @if ( $status != 'trash')
                    <a href='{{ route($section_route . ".create", ["post_type" => $post_type]) }}' class="btn btn-primary btn-sm">@Lang('Add')</a>
                    @endif
                </h4>
            </div>
            <div class="card-body">
                <div class="toolbar">
                    <ul class="nav nav-pills nav-pills-primary" style="margin: 20px auto;">
                        <li class="nav-item {{ !in_array($status, array('trash', 'pending', 'draft')) ? 'active' : '' }}">
                            <a class="nav-link {{ !in_array($status, array('trash', 'pending', 'draft')) ? 'active' : '' }}" href='{{ route($section_route . ".index", ["post_type" => $post_type]) }}'>
                                @Lang(ucfirst($post_type_plural))
                            </a>
                        </li>
                        <li class="nav-item {{ $status == 'trash' ? 'active' : '' }}">
                            <a class="nav-link {{ $status == 'trash' ? 'active' : '' }}" href='{{ route($section_route . ".index", ["post_type" => $post_type, "status" => "trash"]) }}'>
                                @Lang('Trash') @if ( $num_trashed > 0 ) ({{ $num_trashed }}) @endif
                            </a>
                        </li>
                        <li class="nav-item {{ $status == 'pending' ? 'active' : '' }}">
                            <a class="nav-link {{ $status == 'pending' ? 'active' : '' }}" href='{{ route($section_route . ".index", ["post_type" => $post_type, "status" => "pending"]) }}'>
                                @Lang('Pending') @if ( $num_pending > 0 ) ({{ $num_pending }}) @endif
                            </a>
                        </li>
                        <li class="nav-item {{ $status == 'draft' ? 'active' : '' }}">
                            <a class="nav-link {{ $status == 'draft' ? 'active' : '' }}" href='{{ route($section_route . ".index", ["post_type" => $post_type, "status" => "draft"]) }}'>
                                @Lang('Draft') @if ( $num_draft > 0 ) ({{ $num_draft }}) @endif
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                @if ($items->first() && !is_null($items->first()->order))
                                    <th></th>
                                    <th></th>
                                @endif
                                @if ( count($items) > 0  && $status != 'trash')
                                    <th width="5%" class="disabled-sorting text-right">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input checkbox_all" type="checkbox">
                                                <span class="form-check-sign">
                                                <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </th>
                                @endif
                                <th>@Lang('Title')</th>
                                <th width="15%">@Lang('URL')</th>
                                @if ($post_type == "page")
                                    <th width="15%">@Lang('Parent')</th>
                                    <th width="15%">@Lang('Template')</th>
                                @endif
                                <th width="15%">@Lang('Updated')</th>
                                <th width="15%" class="disabled-sorting text-right">@Lang('Actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $key => $item)
                            <tr data-post_id="{{ $item->id }}">
                                @if (!is_null($item->order))
                                    <td>{{ $item->order }}</td>
                                    <td class="reorder"><i style="color: #bababa;" class="material-icons">drag_handle</i></td>
                                @endif
                                @if ( $status != 'trash')
                                <td class="text-right">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input checkbox_delete" type="checkbox" name="entries_to_delete[]" value="{{ $item->id }}">
                                            <span class="form-check-sign">
                                            <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </td>
                                @endif
                                
                                <td>
                                    <a @if (view()->exists("Front::posts.single-{$item->type}") || in_array($post_type, ['post', 'page']))href="{{ $item->get_url() }}" target="_blank"@else href="{{ route($section_route . '.edit', $item->id) . "?post_type=$post_type" }}"@endif>
                                        {{ $item->title }}
                                    </a>
                                    @if ( $item->id == $home_page_id )
                                        <i style="font-size: 11px;"> - @Lang('Front')</i>
                                    @endif
                                </td>
                                <td>{{ $item->post_name }}</td>
                                
                                @if ($post_type == "page")
                                <td>
                                    @if ($parent = $item->parent)
                                    <a href="{{ route('admin.posts.index', ['post_type' => $post_type, 'parent' => $parent->id]) }}">
                                        {{ $parent->title }}
                                    </a>
                                    @endif
                                </td>

                                <td>{{ $item->template }}</td>
                                @endif
                                <td data-order="{{ $item->updated_at->timestamp }}">@if ($item->updated_at) {{ $item->updated_at->format('d/m/Y') . ' ' . $item->updated_at->format('H:i') }} @endif</td>

                                <td class="td-actions text-right">
                                    <a href="{{ route($section_route . '.edit', $item->id) }}?post_type={{$post_type}}" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-success" title="@Lang('Edit')">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    @if ( $status != 'trash')
                                    <button data-url="{{ route('admin.posts.duplicate', $item->id) }}?post_type={{$post_type}}" data-name="@Lang('Are you sure to duplicate :name?', ['name' => $item->title])" type="button" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-info duplicate" title="@Lang('Duplicate')">
                                        <i class="material-icons">file_copy</i>
                                    </button>
                                    <button data-url="{{ route('admin.posts.destroy', $item->id) }}?post_type={{$post_type}}" data-name="@Lang('Are you sure to delete :name?', ['name' => $item->title])" type="button" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                                        <i class="material-icons">close</i>
                                    </button>
                                    @else
                                    <button data-url="{{ route('admin.posts.restore', [$item->id, 'post_type' => $post_type]) }}" data-name="@Lang('Are you sure to restore it?')" type="button" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-warning restore" title="@Lang('Restore')">
                                        <i class="material-icons">restore</i>
                                    </button>
                                    <button data-url="{{ route('admin.posts.destroy_permanent', [$item->id, 'post_type' => $post_type]) }}" data-name="@Lang('Are you sure to delete :name?', ['name' => $item->title])" type="button" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                                        <i class="material-icons">close</i>
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            <div class="loader">
                                <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                    <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                                </svg>
                            </div>
                        </tbody>
                    </table>
                    @if ( count($items) > 0 && $status != 'trash')
                        <form class="mt-5" action='{{ route("admin.posts.mass_destroy", ["post_type" => $post_type]) }}' method="post" onsubmit="return confirm('Are you sure?');">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="ids" id="ids" value="" />
                            <input type="submit" value="@Lang('Delete selected')" class="btn btn-danger" />
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
function getIDs() {
    var ids = [];
    $('.checkbox_delete').each(function () {
        if($(this).is(":checked")) {
            ids.push($(this).val());
        }
    });
    $('#ids').val(ids.join());
}

$(".checkbox_all").click(function(){
    $('input.checkbox_delete').prop('checked', this.checked);
    getIDs();
});

$('.checkbox_delete').change(function() {
    getIDs();
});
</script>

<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<script>
$(document).ready(function() {
    var table = $('.table').DataTable({
        "pagingType": "full_numbers",
        @if ($items->first() && !is_null($items->first()->order))
        "rowReorder": true,
        "columnDefs": [
            { targets: 0, visible: false }
        ],
        @endif
        "lengthMenu": [
            [25, 50, -1],
            [25, 50, "@Lang('All')"]
        ],
        responsive: true,
        /*language: {
            search: "_INPUT_",
            searchPlaceholder: "@Lang('Search')",
        }*/
        language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" }
    });

    table.on( 'row-reorder', function ( e, diff, edit ) {
        var order = [];
        $('.table tbody tr').each(function (index, el) {
            order.push({post_id:$(this).data('post_id'), newPosition:index});
        });

        $.ajax({
            url: "{{ route('ajax') }}",
            method: 'POST',
            data: {
                parameters: JSON.stringify(order),
                action: 'reorderRow',
            },
            success: function(data) {
                $('.loader').fadeOut();
                e.preventDefault();
            },
            error: function(data) {
                swal({
                    title: "@Lang('Error')",
                    text: "@Lang('Error saving data')",
                    type: 'error',
                    confirmButtonClass: "btn",
                    buttonsStyling: false
                });
            }
        });
    });

    //var table = $('#datatable').DataTable();

    // Delete a record
    table.on('click', '.remove', function(e) {
        var $tr = $(this).closest('tr');
        var url = $(this).data('url');
        var title = $(this).data('name');
        swal({
            text: title,
            //title: 'Are you sure?',
            //text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: '@Lang('Delete')',
            cancelButtonText: '@Lang('Cancel')',
            buttonsStyling: false
        }).then(function(result) {
            if ( result.value ) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {'_token': '{{csrf_token()}}', '_method': 'DELETE'},
                    success: function(data) {
                        swal({
                            title: "@Lang('Deleted')",
                            text: "@Lang('Item deleted succesfully.')",
                            type: 'success',
                            confirmButtonClass: "btn btn-success",
                            buttonsStyling: false
                        });
                        table.row($tr).remove().draw();
                        e.preventDefault();
                    },
                    error: function(data) {
                        swal({
                            title: "@Lang('Error')",
                            text: "@Lang('Error saving data')",
                            type: 'error',
                            confirmButtonClass: "btn",
                            buttonsStyling: false
                        });
                    }
                });
            }
        });
    });

    // RESTORE
    table.on('click', '.restore', function(e) {
        var $tr = $(this).closest('tr');
        var url = $(this).data('url');
        var title = $(this).data('name');
        swal({
            text: title,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: '@Lang('Restore')',
            cancelButtonText: '@Lang('Cancel')',
            buttonsStyling: false
        }).then(function(result) {
            if ( result.value ) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {'_token': '{{csrf_token()}}', '_method': 'PUT'},
                    success: function(data) {
                        swal({
                            title: "@Lang('Restored')",
                            text: "@Lang('Item restored succesfully.')",
                            type: 'success',
                            confirmButtonClass: "btn btn-success",
                            buttonsStyling: false
                        });
                        table.row($tr).remove().draw();
                        e.preventDefault();
                    },
                    error: function(data) {
                        swal({
                            title: "@Lang('Error')",
                            text: "@Lang('Error saving data')",
                            type: 'error',
                            confirmButtonClass: "btn",
                            buttonsStyling: false
                        });
                    }
                });
            }
        });
    });

    table.on('click', '.switch_active', function(e) {
        var boto = $(this);
        var url = $(this).data('url');
        var title = $(this).data('name');
        var text = "@Lang('actived')";
        var id = $(this).data('id');
        var onlyone = $(this).data('onlyone');
        $.ajax({
            url: url,
            method: 'POST',
            data: {'_token': '{{csrf_token()}}', 'id': id},
            success: function(data) {
                if ( data.active ) {
                    swal({
                        title: "{{ucfirst(__('actived'))}}",
                        text: title + " @Lang('actived')",
                        type: 'success',
                        confirmButtonClass: "btn btn-success",
                        buttonsStyling: false
                    });
                    if ( onlyone ) {
                        $('.switch_active').removeClass('btn-success');
                        $('.switch_active').addClass('btn-danger');
                        $('.switch_active').find('i').html('clear');
                    }
                    boto.removeClass('btn-danger');
                    boto.addClass('btn-success');
                    boto.find('i').html('check');
                } else {
                    swal({
                        title: "{{ucfirst(__('deactived'))}}",
                        text: title + " @Lang('deactived')",
                        type: 'success',
                        confirmButtonClass: "btn btn-success",
                        buttonsStyling: false
                    });
                    boto.addClass('btn-danger');
                    boto.removeClass('btn-success');
                    boto.find('i').html('clear');
                }
                e.preventDefault();
            },
            error: function(data) {
                swal({
                    title: "@Lang('Error')",
                    text: "@Lang('Error saving data')",
                    type: 'error',
                    confirmButtonClass: "btn",
                    buttonsStyling: false
                });
            }
        });
    });

    $('.table').on('click', '.show_img', function(e) {
        var boto = $(this);
        var img = $(this).data('img');
        var title = $(this).data('name');
        swal({
            title: title,
            width: '80%',
            showCloseButton: true,
            showCancelButton: false,
            showConfirmButton: false,
            html: '<img src="'+img+'" style="width: 100%;">'
        });
    });

    // DUPLICATE
    table.on('click', '.duplicate', function(e) {
        var $tr = $(this).closest('tr');
        var url = $(this).data('url');
        var title = $(this).data('name');
        swal({
            text: title,
            type: 'info',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: '@Lang('Duplicate')',
            cancelButtonText: '@Lang('Cancel')',
            buttonsStyling: false
        }).then(function(result) {
            if ( result.value ) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {'_token': '{{csrf_token()}}', '_method': 'PUT'},
                    success: function(data) {
                        swal({
                            title: "@Lang('Duplicated')",
                            text: "@Lang('Item duplicated succesfully.')",
                            type: 'success',
                            confirmButtonClass: "btn btn-success",
                            buttonsStyling: false
                        }).then(function(result) {
                            location.reload();
                        });
                    },
                    error: function(data) {
                        swal({
                            title: "@Lang('Error')",
                            text: "@Lang('Error saving data')",
                            type: 'error',
                            confirmButtonClass: "btn",
                            buttonsStyling: false
                        });
                    }
                });
            }
        });
    });
});
</script>
@endsection