<!-- Modal -->
<div class="modal fade" id="media_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" style="width: 80%;max-width: 80%;">
		<div class="modal-content">
			<div class="modal-header">
                <h5 class="modal-title ml-4" id="myModalLabel" style="text-align: left;display:flex;">
                    <span class="pt-2">@Lang('Select a Media')</span>
                    <div class="col-md-2 text-right">
                        <input type="text" placeholder="@Lang('Search')" class="form-control search">
                    </div>
                    <div class="col-md-1 text-right">
                        <label class="col-form-label" style="font-size: 14px;">@Lang('Order'):</label>
                    </div>
                    <div class="col-md-3">
                        <select class="selectpicker sortby" data-size="7" data-style="btn btn-primary btn-round btn-sm" title="@Lang('Sortby')">
                            <option value="id">@Lang('Id')</option>
                            <option value="title">@Lang('Title')</option>
                            <option value="legend">@Lang('Legend')</option>
                            <option value="alt">@Lang('Alternative text')</option>
                            <option value="created_at">@Lang('Created at')</option>
                            <option value="updated_at" selected>@Lang('Updated at')</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="selectpicker order" data-size="7" data-style="btn btn-primary btn-round btn-sm" title="@Lang('Order')">
                            <option value="asc">@Lang('Ascending')</option>
                            <option value="desc" selected>@Lang('Descending')</option>
                        </select>
                    </div>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="lds-dual-ring"></div>
                <form action="{{ route('admin.media.upload') }}" id="dropzone" enctype="multipart/form-data">
                    <div class="dz-message"><i style="font-size: 10em;color: lightgray;" class="material-icons">cloud_upload</i></div>
                    <div class="image-grid"></div>
                    <div class="image-info">
                        <input id="media-id" type="hidden" />
                        <div class="flex-row">
                            <div class="form-group col-md-3">
                                <label class="col-form-label">@Lang('Title'):</label>
                            </div>
                            <div class="form-group col-md-9">
                                <input id="modal-title" type="text" class="form-control info-title col-md-12" value="">
                            </div>
                        </div>
                        <div class="flex-row">
                            <div class="form-group col-md-3">
                                <label class="col-form-label">@Lang('Legend'):</label>
                            </div>
                            <div class="form-group col-md-9">
                                <input id="modal-legend" type="text" class="form-control info-legend col-md-12" value="">
                            </div>
                        </div>
                        <div class="flex-row">
                            <div class="form-group col-md-3">
                                <label class="col-form-label">@Lang('Alternative text'):</label>
                            </div>
                            <div class="form-group col-md-9">
                                <input id="modal-alt" type="text" class="form-control info-alt col-md-12" value="">
                            </div>
                        </div>
                        <div class="flex-row">
                            <div class="form-group col-md-2">
                                <label class="col-form-label">@Lang('URL'):</label>
                            </div>
                            <div class="form-group">
                                <a class="info-url col-md-12" target="_blank" href="javascript:void(0);"></a>
                            </div>
                        </div>
                        {{--
                        <div class="flex-row">
                            <button type="button" id="update" class="btn btn-previous btn-fill btn-default btn-wd">@Lang('Update')</button>        
                        </div>
                        --}}
                    </div>
                </form>
            </div>
                
            <div class="modal-footer">
                <div class="mr-auto">
                    <button type="button" class="btn btn-previous btn-fill btn-default btn-wd" data-dismiss="modal">@Lang('Cancel')</button>
                </div>
                <div class="ml-auto">
                    <button type="button" class="btn btn-next btn-fill btn-primary btn-wd media_modal_accept">@Lang('Accept')</button>
                </div>
                <div class="clearfix"></div>
            </div>
		</div>
	</div>
</div>

<div id="poup-txt" class="popup">
    <div class="pro-lft-pad cropper-bg">
        <img src="" />
        <div class="controls">
            <button id="crop" class="btn btn-sm btn-info">Crop</button>
            <button id="remove" class="btn btn-sm btn-danger">Clear</button>
        </div>
    </div>
    <div class="pro-rgt-pad">
        <div class="prod-txt">
            <div class="image_info">
                <div class="inline">
                    <label>Size</label>
                    <div id="img_size"></div>
                </div>
                <label>Title</label>
                <input id="img_title" class="form-control" type="text" name="img[title]">
                <label>Level</label>
                <input id="img_level" class="form-control" type="number" name="img[level]">
                <div class="inline">
                    <label>URL</label>
                    <button id="img_url" class="btn btn-sm btn-info" data-clipboard-text="Just because you can doesn't mean you should — clipboard.js">Copy</button>
                </div>

                <div class="cropper-modal-buttons">
                    <button id="save_img" class="btn btn-sm btn-success pull-right">@lang('Save')</button>
                    <button class="btn btn-sm btn-default pull-right cancel_crop">@lang('Cancel')</button>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="{{ asset($_admin.'css/jquery.fancybox.css') }}" />
<link rel="stylesheet" href="{{ asset($_admin.'css/cropper.min.css') }}" />
<script src="{{ asset($_admin.'js/plugins/dropzone.js') }}"></script>
<script src="{{ asset($_admin.'js/plugins/cropper.min.js') }}"></script>
<script src="{{ asset($_admin.'js/plugins/jquery.fancybox.js') }}"></script>

<script>
var imageFormats = {!! \App\Models\Media::getFileTypesArray('image') !!};
var videoFormats = {!! \App\Models\Media::getFileTypesArray('videos') !!};

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var dropzone = new Dropzone('#dropzone', {
    previewsContainer: '.image-grid',
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    parallelUploads: 2,
    thumbnailHeight: 120,
    thumbnailWidth: 120,
    uploadMultiple: true,
    queuecomplete: function(file, response) {
        get_medias($current);
    }
});

function addfile(event) {
    var files = event.target.files;
    var data = new FormData();
    jQuery.each(files, function(i, file) {
        data.append('file[]', file);
    });

    $('.lds-dual-ring').css('display', 'flex');

    $.ajax({
        url: "{{ route('admin.media.upload') }}",
        method: 'POST',
        data: data,
        contentType: false,
        processData: false,
        success: function(data) {
            get_medias($current);
            $('.lds-dual-ring').css('display', 'none');
        }
    })
}

var $current, nextURL, multiple, method;

$(document).on('click', '#remove', function(e) {
    var img = $('#poup-txt').find('img.cropper-hidden').get(0);

    if (typeof $(img).data('croppable') === 'undefined' || (typeof $(img).data('croppable') !== 'undefined' && $(img).data('croppable') == 0)) {
        return;
    }
    cropper.destroy();
    crop = false;
});


var cropper, crop = false;
$(document).on('click', '#crop', function(e) {
    e.preventDefault();
    var img = $('#poup-txt').find('img').get(0);

    if (typeof $(img).data('croppable') === 'undefined' || (typeof $(img).data('croppable') !== 'undefined' && $(img).data('croppable') == 0)) {
        return;
    }

    var id = $(img).data('id');
    var type = $(img).data('type');
    var filename = $(img).data('filename');

    var height = img.naturalHeight;
    var width = img.naturalWidth;

    var currentInput = $($current).find('input.media_input');
    var toggleCropMode = true;

    var cropHeight = 200;
    var cropWidth = 300;

    if (typeof currentInput.data('height') !== 'undefined') {
        cropHeight = currentInput.data('height');
        cropWidth = currentInput.data('width');
        toggleCropMode = false;
    }

    var cropperOptions = {
        multiple: false,
        dragMode: 'move',
        viewMode: 2,
        autoCrop: false,
        zoomOnWheel: true,
        zoomOnTouch: true,
        zoomable: true,
        cropBoxMovable: true,
        cropBoxResizable: toggleCropMode,
        scalable: false,
        rotatable: false,
        guides: false,
        toggleDragModeOnDblclick: toggleCropMode,
        dragCrop: true,
        ready() {
            this.cropper.crop();
            cropper.setCropBoxData({
                width: cropWidth,
                height: cropHeight,
            });
            /*cropper.setCanvasData({
                width: width,
                height: height
            });*/
        }
    };

    if (crop) {
        img.cropper('destroy').cropper(cropperOptions);
    } else {
        cropper = new Cropper(img, cropperOptions);
    }

    crop = true;
});

$(document).on('click', '#save_img', function () {
    if (typeof cropper !== 'undefined') {
        var cropData = cropper.getData(true);

        $.ajax({
            url: "{{ route('admin.media.crop') }}",
            method: 'POST',
            data: {
                cropData: cropData,
                id: $('#poup-txt').find('img').data('id')
            },
            success: function (data) {
                get_medias($current);
                /*console.log('crop success');*/
            },
            error: function (data) {
                /*console.log('crop error');*/
            }
        });
    }
});

$(document).on('click', 'a.fileinput-exists[data-dismiss=fileinput]', function() {
    $current = $(this).closest('.fileinput[data-provides=fileinput]');
    $current.find('input.media_input').val('');
    $current.find('.fileinput-preview').html('');

    if (!$current.parent('.gallery').length) {
        $current.removeClass('fileinput-exists');
        if (!$current.hasClass('fileinput-new')) {
            $current.addClass('fileinput-new');
        }
    } else {
        $(this).closest('.fileinput[data-provides=fileinput]').remove();
    }
});


function mostrarGaleria() {
    var info = $('#media_modal .image-info');
    var grid = $('#media_modal .image-grid');

    $('#media_modal').modal('toggle');
    grid.css('width', '100%');
    info.css('width', '0%');
    //$('#media_modal .image-grid').perfectScrollbar();
}

function show_media_modal(custom_field) {
    get_medias(custom_field);
    mostrarGaleria();
}

$('#media_modal .modal-header .search').focusout(function(){ get_medias($current); });
$('#media_modal .modal-header .order').change(function(){ get_medias($current); });
$('#media_modal .modal-header .sortby').change(function(){ get_medias($current); });


$(document).on('click', '#load_more', function() {
    get_medias($current, nextURL, true);
});

$(document).on('click', '.see', function(e) {
    if ($(this).hasClass('fancybox')) {
        e.stopPropagation();
        e.preventDefault();

        var height = $(this).closest('.image').find('img').data('height');
        var width = $(this).closest('.image').find('img').data('width');
        var title = $(this).closest('.image').find('img').data('filename');
        var url = $(this).closest('.image').find('img').data('url');
        var level = $(this).closest('.image').find('img').data('level');


        $('#poup-txt').find('img').attr('src', url);
        $('#poup-txt').find('img').attr('data-id', $(this).closest('.image').find('img').data('id'));
        $('#poup-txt').find('img').attr('data-title', title);
        $('#poup-txt').find('img').attr('data-type', $(this).closest('.image').find('img').data('type'));
        $('#poup-txt').find('img').attr('data-height', height);
        $('#poup-txt').find('img').attr('data-width', width);
        $('#poup-txt').find('img').attr('data-croppable', $(this).closest('.image').find('img').data('croppable'));

        $('#img_size').text(width + 'x' + height);
        $('#img_title').val(title);
        $('#img_url').attr('data-clipboard-text', url);
        $('#img_level').val(level);
    }
});


$('.crop-media').on('click', function () {
    multiple = $(this).closest('.gallery-field').length > 0;
    var element = $(this).closest('.custom-field');
    method = 'update';

    if (method === 'update') {
        element = $(this).closest('.fileinput[data-provides=fileinput]');
    }

    if ( element.length == 0) element = $(this).closest('.miniatura').parent();
    if ( element.length == 0) element = $(this).closest('.fileinput[data-provides=fileinput]');

    $current = element;
    mostrarGaleria();

    var me = this;

    setTimeout(function() {
        var imgElement = $(me).parent().parent().find('img.media');

        var height = imgElement.data('height');
        var width = imgElement.data('width');
        var title = imgElement.data('filename');
        var url = imgElement.data('url');
        var level = imgElement.data('level');

        var poupImgElement = $('#poup-txt').find('img');

        poupImgElement.attr('src', url);
        poupImgElement.attr('data-id', imgElement.data('id'));
        poupImgElement.attr('data-title', title);
        poupImgElement.attr('data-type', imgElement.data('type'));
        poupImgElement.attr('data-height', height);
        poupImgElement.attr('data-width', width);
        poupImgElement.attr('data-croppable', 1);

        $('#img_size').text(width + 'x' + height);
        $('#img_title').val(title);
        $('#img_url').attr('data-clipboard-text', url);
        $('#img_level').val(level);

        $.fancybox({
            href: '#poup-txt',
            modal: true
        });
    }, 200);
});

$('.cancel_crop').on('click', function () {
    $.fancybox.close(true);
    get_medias($current);
});

$(document).on('click', '.edit-media, .btn-file, .add_image_to_gallery', function(e) {
    multiple = $(this).closest('.gallery-field').length > 0;
    var element = $(this).closest('.custom-field');
    method = $(this).hasClass('edit-media') ? 'update' : 'insert';

    if (method === 'update') {
        element = $(this).closest('.fileinput[data-provides=fileinput]');
    }

    if ( element.length == 0) element = $(this).closest('.miniatura').parent();
    if ( element.length == 0) element = $(this).closest('.fileinput[data-provides=fileinput]');
    show_media_modal(element);
});

function get_medias(targetElement, url, append) {
    $current = targetElement;

    var search = $('#media_modal .modal-header input.search').val();
    var sortby = $('#media_modal .modal-header select.sortby').val();
    var order = $('#media_modal .modal-header select.order').val();
    var filter = {};

    /*var currentInput = $($current).find('input.media_input');

    if (typeof currentInput.data('height') !== 'undefined') {
        filter = {
            crop_height: currentInput.data('height'),
            crop_width: currentInput.data('width')
        };
    }*/
    
    $('.lds-dual-ring').css('display', 'flex');
    $.ajax({
        url: url ? url : "{{ route('admin.media.get_all') }}",
        method: 'POST',
        data: {
            'search': search,
            'sortby': sortby,
            'order': order,
            'filter': filter
        },
        success: function (data) {
            nextURL = data.items.next_page_url;
            if (nextURL == null) $('#load_more').remove();

            var info = $('#media_modal .image-info');
            var grid = $('#media_modal .image-grid');

            grid.css('width', '100%');
            info.css('width', '0%');
            info.removeClass('open');
            $('.dz-message').removeClass('open');
            $('#media_modal .image-grid img').removeClass('active');

            var pre = `
            <div class="column">
                <label for="add_files"><i class="material-icons">cloud_upload</i>@Lang("New File")</label>
                <input id="add_files" onchange="addfile(event)" type="file" name="file" multiple />`;
            var img = '';
            for (var key in data.items.data) {
                var $item = data.items.data[key];
                var src = ($item.thumbnails.medium != '') ? $item.thumbnails.medium : $item.thumbnails.full;
                console.log(src);
                var srcAux = src;

                var isImg = true;
                if ($.inArray($item['file_type'].toLowerCase(), imageFormats) < 0) {
                    src = '{{ asset($_admin.'img/icons/doc.png') }}';
                    isImg = false;
                }

                img += `<div class="image" data-full="${$item.thumbnails.full}">
                            <a class="btn btn-info btn-round btn-sm see `+ (isImg ? 'fancybox show' : '' ) + `" href="`+ (isImg ? '#poup-txt' : srcAux) + `" ` + (!isImg ? 'target="_blank"' : '') + `"><i class="material-icons">remove_red_eye</i></a>
                            <span class="tick"><i class="material-icons">check</i></span>`;
                if ($.inArray($item['file_type'].toLowerCase(), videoFormats) !== -1) {
                    img +=  `<video id="media-${$item['id']}" src="${src}" title="${$item['title']}" data-level="${$item['level']}" data-type="${$item['file_type']}" data-filename="${$item['file_name']}" data-id="${$item['id']}" data-legend="${$item['legend']}" data-title="${$item['title']}" data-alt="${$item['alt']}" data-croppable="${$item['isCroppable']}" autoplay loop muted>
                                <source src="${src}" type="video/mp4">
                                <em>Sorry, your browser doesn't support HTML5 video.</em>
                            </video>`;
                } else {
                    img +=  `<a class="btn btn-info btn-round btn-sm see `+ (isImg ? 'fancybox show' : '' ) + `" href="`+ (isImg ? '#poup-txt' : srcAux) + `" ` + (!isImg ? 'target="_blank"' : '') + `><i class="material-icons">remove_red_eye</i></a>
                            <img id="media-${$item['id']}" src="${src}" title="${$item['title']}" data-level="${$item['level']}" data-type="${$item['file_type']}" data-filename="${$item['file_name']}" data-id="${$item['id']}" data-legend="${$item['legend']}" data-title="${$item['title']}" data-alt="${$item['alt']}" data-url="${$item['thumbnails']['full']}" data-height="${$item['height']}" data-width="${$item['width']}"  data-croppable="${$item['isCroppable']}">`;

                    if ($.inArray($item['file_type'].toLowerCase(), imageFormats) < 0) {
                        img += `<div style="text-align: center">${$item['title']}</div>`
                    }
                }
                img +=  `</div>`;
            }

            post = `
                <label id="load_more"><i class="material-icons">add</i>@Lang("Load More")</label>
            </div>`;

            if (append) {
                $('#media_modal #load_more').before(img);
            } else {
                $('#media_modal .image-grid').html(pre + img + post);
            }
            
            $.fancybox.close(true);
            $('#media_modal .image-grid').find('.fancybox').fancybox({
                height: 792,
                width: 1024,
                afterClose: function() {
                    if (typeof cropper !== 'undefined') {
                        cropper.destroy();
                    }
                    
                    crop = false;
                }
            });

            // Seleccionar una imatge de la galeria i premer el btó d'acceptar
            $('.lds-dual-ring').css('display', 'none');
        },
        error: function (data) {
            $('#media_modal').modal('toggle');
            $('#media_modal .image-grid').perfectScrollbar('destroy');
            swal({
                title: "@Lang('Error')",
                text: "@Lang('Error retrieving data')",
                type: 'error',
                confirmButtonClass: "btn",
                buttonsStyling: false
            });
        }
    });
}

/*$(document).on('click', '.remove-media', function() {
    $(this).closest('.fileinput[data-provides=fileinput]').remove();
});*/

// Seleccionar una imatge de la Galeria
$(document).on('click', '#media_modal .image-grid .image', function() {
    var same = $(this).hasClass('active');
    var info = $('#media_modal .image-info');
    var grid = $('#media_modal .image-grid');
    var title = $(this).data('title');
    var legend = $(this).data('legend');
    var id = $(this).data('id');

    var alt = $(this).data('alt');
    var url = $(this).data('url');

    if (!multiple) $('#media_modal .image-grid .image').removeClass('active');

    $(this).toggleClass('active');
    if ( !same ) {
        /*
        info.find('input.info-title').val(title);
        info.find('input.info-legend').val(legend);
        info.find('input.info-alt').val(alt);
        info.find('a.info-url').html(url);
        info.find('a.info-url').prop('href', url);
        info.find('input#media-id').val(id);

        info.css('width', '40%');
        info.addClass('open');
        */
        $('.dz-message').addClass('open');
        /*
        Evitem el nou scroll quan seleccionem una imatge
        grid.animate({
            scrollTop: $(this).offset().top - 80
        }); */
    } else {

        $(this).removeClass('active');
        /*
        grid.css('width', '100%');
        info.css('width', '0%');
        info.removeClass('open');
        */
        $('.dz-message').removeClass('open');
    }
});


$(document).on('click', '#media_modal .media_modal_accept', function() {
    var info = $('#media_modal .image-info');
    var grid = $('#media_modal .image-grid');
    var img = $('#media_modal .image-grid .image.active');

    if (img.length) {
        switch(method) {
            case 'update':
                var image = $(img).find('img, video');
                var media_id = image.attr('id').replace('media-', '');
                var miniatura = $current.find('.miniatura');

                if (!miniatura.length && $current.hasClass('miniatura')) {
                    miniatura = $current;
                }

                // Cas de modificar una imatge que ja ha estat "setejada"
                if (miniatura.length) {

                    miniatura.find('input.media_input').val( media_id );
                    miniatura.find('.fileinput-preview').html(`<img src="${image.attr('src')}">`);

                    if ($.inArray(image.data('type').toLowerCase(), imageFormats) < 0) {
                        miniatura.find('.fileinput-preview').append(`<div style="margin-top: -24px">${image.data('title')}</div>`)
                    }

                    miniatura.removeClass('fileinput-new');
                    if ( !miniatura.hasClass('fileinput-exists') ) {
                        miniatura.addClass('fileinput-exists');
                    }
                }
                break;
            case 'insert':
                if (multiple) {
                    var name = $current.find('.name').attr('name');
                    var galeria = $current.find('.gallery');

                    img.each(function(idx, el) {
                        var image = $(el).find('img');
                        var media_id = image.attr('id').replace('media-', '');
                        var aux_name = name + '[' + (galeria.children().length + 1) + ']';

                        var imageElement = renderImage(media_id, image.attr('src'), aux_name);
                        galeria.append(imageElement);
                    });
                } else {
                    var image = $(img).find('img');
                    var miniatura = $current.find('.miniatura');
                    var thumbnail = $current.find('.thumbnail');

                    if (miniatura.length == 0 && thumbnail.length == 0) $current.summernote('insertImage', image.data('url'));
                    else {
                        var media_id = image.attr('id').replace('media-', '');
                        $($current).find('input.media_input').val( media_id );
                        $($current).find('.fileinput-preview').html( '<img src="' + image.data('url') + '">' );
                        
                        $($current).removeClass('fileinput-new');
                        if ( !$($current).hasClass('fileinput-exists') ) {
                            $($current).addClass('fileinput-exists');
                        }
                    }
                }
                break;
        }
    }

    grid.css('width', '100%');
    info.css('width', '0%');

    info.removeClass('open');
    img.removeClass('active');
    $('.dz-message').removeClass('active');
    $('#media_modal').modal('toggle');
    $('#media_modal .image-grid').perfectScrollbar('destroy');
});

function renderImage(id, src, name) {
    var aux = src.split('.');
    var svg = (aux[aux.length - 1] == 'svg') ? 'svg' : '';

    return `
        <div class="miniatura fileinput fileinput-exists text-center" data-provides="fileinput">
            <div class="fileinput-new thumbnail">
                <img src="{{ asset('images/image_placeholder.jpg') }}">
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="">
                <img class="${svg}" src="${src}" />
            </div>
            <div>
                <span class="btn btn-rose btn-round btn-file btn-sm edit-media">
                <span class="fileinput-new">@Lang('Select image')</span>
                <span class="fileinput-exists"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                <input class="media_input" type="hidden" name="${name}" value="${id}">
                <div class="ripple-container"></div></span>
                <a href="javascript:void(0);" class="btn btn-danger btn-round fileinput-exists btn-sm remove-media" data-dismiss="fileinput">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                </a>
            </div>
        </div>`;
}
</script>