@extends('Admin::layouts.app')

@section('header')
	@Lang('Reset Password')
@endsection

@section('content')
<div class="page-header login-page header-filter" filter-color="black" style="background-image: url('../../assets/img/login.jpg'); background-size: cover; background-position: top center;">
	<!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
				@if (session('status'))
					<div class="alert alert-success" role="alert">
						{{ session('status') }}
					</div>
				@endif
				<form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
					{{ csrf_field() }}

					<div class="card card-login card-hidden">
						<div class="card-header card-header-rose text-center">
							<h4 class="card-title">@Lang('Reset Password')</h4>
						</div>

						<div class="card-body ">

							<!-- Email -->
							<span class="bmd-form-group {{ $errors->has('email') ? ' has-error' : '' }}">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="material-icons">email</i>
										</span>
									</div>
									
									<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@Lang('Email')" required autofocus>
									@if ($errors->has('email'))
										<div class="alert alert-warning" role="alert">
											<span class="help-block">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
										</div>
									@endif
								</div>
							</span>
							
						</div>

						<div class="card-footer justify-content-center">
							<a class="btn btn-link" href="{{ route('login') }}">
								@Lang('Login')
							</a>
							<button type="submit" class="btn btn-primary btn-raised btn-sm">
								@Lang('Send')
							</button>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<nav class="float-left">
				<ul>
					<li>
						<a href="javascript:void(0);">
							@Lang('About Us')
						</a>
					</li>
				</ul>
			</nav>
			
			<div class="copyright float-right">
				&copy;
				<script>
					document.write(new Date().getFullYear())
				</script>, powered by
				<a href="https://www.6tems.com" target="_blank">6TEMS</a>.
			</div>
		</div>
	</footer>
	
</div>
@endsection