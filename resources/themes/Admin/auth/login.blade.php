@extends('Admin::layouts.app')

@section('header')
	@Lang('Login')
@endsection

@section('content')
<div class="page-header login-page header-filter" filter-color="black" style="background-image: url('../../assets/img/login.jpg'); background-size: cover; background-position: top center;">
	<!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
				<form class="form-horizontal" method="POST" action="{{ route('login') }}">
					{{ csrf_field() }}

					<div class="card card-login card-hidden">
						<div class="card-header card-header-rose text-center">
							<h4 class="card-title">@Lang('Login')</h4>
							<!--<div class="social-line">
								<a href="#pablo" class="btn btn-just-icon btn-link btn-white">
									<i class="fa fa-facebook-square"></i>
								</a>
								<a href="#pablo" class="btn btn-just-icon btn-link btn-white">
									<i class="fa fa-twitter"></i>
								</a>
								<a href="#pablo" class="btn btn-just-icon btn-link btn-white">
									<i class="fa fa-google-plus"></i>
								</a>
							</div>-->
						</div>

						<div class="card-body ">
							<!--<p class="card-description text-center">Or Be Classical</p>
							<span class="bmd-form-group">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="material-icons">face</i>
										</span>
									</div>
									<input type="text" class="form-control" placeholder="First Name...">
								</div>
							</span>-->

							<!-- Email -->
							<span class="bmd-form-group">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="material-icons">email</i>
										</span>
									</div>
									
									<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@Lang('Email')" required autofocus>
									@if ($errors->has('email'))
										<div class="alert alert-warning" role="alert">
											<span class="help-block">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
										</div>
									@endif
								</div>
							</span>
							
							<!-- Password -->
							<span class="bmd-form-group">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">
											<i class="material-icons">lock_outline</i>
										</span>
									</div>
									
									<input id="password" type="password" class="form-control" name="password" placeholder="@Lang('Password')" required>
									@if ($errors->has('password'))
										<div class="alert alert-warning" role="alert">
											<span class="help-block">
												<strong>{{ $errors->first('password') }}</strong>
											</span>
										</div>
									@endif
								</div>
							</span>
						</div>

						<div class="card-footer justify-content-center">
							<a class="btn btn-link" href="{{ route('password.request') }}">
								@Lang('Forgot Your Password?')
							</a>
							<button type="submit" class="btn btn-primary btn-raised btn-sm">
								@Lang('Login')
							</button>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<nav class="float-left">
				<ul>
					<li>
						<a href="javascript:void(0);">
							@Lang('About Us')
						</a>
					</li>
				</ul>
			</nav>
			
			<div class="copyright float-right">
				&copy;
				<script>
					document.write(new Date().getFullYear())
				</script>, powered by
				<a href="https://www.6tems.com" target="_blank">6TEMS</a>.
			</div>
		</div>
	</footer>
	
</div>
@endsection
