@extends('Admin::layouts.admin')

@section('section-title')
    @Lang('Configuration')
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <form id="form" class="form-horizontal" action="{{ route('admin.configuracion.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <article class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">tune</i>
                    </div>
                    <h4 class="card-title">
                        @Lang('Configuration')
                        <a id="add_config" href="javascript:void(0);" class="btn btn-primary btn-sm">@Lang('Add')</a>
                    </h4>
                </div>

                <div class="card-body">
                    <!-- TABS DE CONFIGURACIÓ -->
                    <div class="toolbar">
                        <!-- Subforms -->
                        <ul class="nav nav-pills nav-pills-primary" role="tablist">
                            @php($i = 0)
                            @foreach($items as $key => $group)
                            <li class="nav-item">
                                <a href="#{{ $key }}" data-toggle="tab" class="nav-link {{ ($i == 0) ? 'active' : '' }}">{{$key}}</a>
                            </li>
                            @php($i++)
                            @endforeach
                        </ul>
                    </div>
                    <!-- FI TABS DE CONFIGURACIÓ -->

                    <!-- Tab de General -->
                    <div class="tab-content">
                        <!-- General -->
                        @php($i = 0)
                        @foreach($items as $key => $group)
                            <div id="{{$key}}" class="tab-pane {{ ($i == 0) ? 'active' : '' }}">
                            @foreach ($group as $k => $i)
                                @if ( empty($i['value']) ) @php( $i['value'] = null ) @endif
                                @if ( empty($i['file']) ) @php( $i['file'] = null ) @endif
                                @if ( $i['type'] == 'text' )
                                    <div class="row">
                                        <label class="ml-4 col-md-2 col-form-label">{{ __($i['title']) }}</label>
                                        <div class="col-md-9">
                                            <div class="form-group has-default">
                                                <input name="{{$key}}[{{ $i['name'] }}]" type="text" class="form-control" value="{{ $i['value'] }}" @if (array_key_exists('required', $i) && $i['required'])required @endif tabindex={{ $k }}>
                                            </div>
                                        </div>
                                    </div>
                                @elseif ( $i['type'] == 'img' )
                                    @php ( $has_thumbnail = false )
                                    @php ( $img_src = '' )
                                    @if ( !empty($i['file']) )
                                        @php ( $has_thumbnail = true )
                                        @php ( $img_src = $i['obj']->get_file_url() )
                                    @endif
                                    <div class="row">
                                        <div class="form-group col-md-4 col-sm-4 ml-5">
                                        <h4 class="title">{{ __($i['title']) }}</h4>
                                        <div class="fileinput {{ $has_thumbnail ? 'fileinput-exists' : 'fileinput-new' }} text-center" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail">
                                                <img src="{{ asset('images/image_placeholder.jpg') }}">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="">
                                                <img src="{{ $img_src }}" >
                                            </div>
                                            <div>
                                                <span class="btn btn-rose btn-round btn-file">
                                                    <span class="fileinput-new">@Lang('Select image')</span>
                                                    <span class="fileinput-exists">@Lang('Change')</span>
                                                    <input type="hidden"><input type="file" name="{{$key}}[{{ $i['name'] }}]">
                                                    <div class="ripple-container"></div>
                                                </span>
                                                <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                                                    <i class="fa fa-times"></i> @Lang('Delete')
                                                </a>
                                            </div>
                                        </div>
                                        </div>
                                    </div>      
                                @elseif ( $i['type'] == 'color' )
                                    <div class="row">
                                        <label class="ml-4 col-md-2 col-form-label">{{ __($i['title']) }}</label>
                                        <div class="col-md-9">
                                            <div class="form-group has-default">
                                                {{--<input name="{{ $i['name'] }}" type="text" class="form-control" value="{{ $i['value'] }}" required tabindex={{ $k }}>--}}
                                                <div class="fixed-plugin" style="position: relative;top: 0;left: 0;width: auto;background: none;text-align: left;z-index:0;">
                                                    <div data-name="{{ $i['name'] }}" class="badge-colors ml-auto mr-auto">
                                                        <span class="badge filter badge-azure {{ ($i['value'] == 205 ? 'active' : '') }}" data-color=205></span>
                                                        <span class="badge filter badge-green {{ ($i['value'] == 131 ? 'active' : '') }}" data-color=131></span>
                                                        <span class="badge filter badge-orange {{ ($i['value'] == 380 ? 'active' : '') }}" data-color=380></span>
                                                        <span class="badge filter badge-yellow {{ ($i['value'] == 50 ? 'active' : '') }}" data-color=50></span>
                                                        <span class="badge filter badge-red {{ ($i['value'] == 350 ? 'active' : '') }}" data-color=350></span>
                                                        <span class="badge filter badge-purple {{ ($i['value'] == 287 ? 'active' : '') }}" data-color=287></span>
                                                        <span class="badge filter badge-rose {{ ($i['value'] == 320 ? 'active' : '') }}" data-color=320></span>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="{{$key}}[{{ $i['name'] }}]" value="{{ $i['value'] }}">
                                            </div>
                                        </div>
                                    </div>
                                @elseif ( $i['type'] == 'page' )
                                    <div class="row">
                                        <label class="ml-4 col-md-2 col-form-label">{{ __($i['title']) }}</label>
                                        <div class="col-md-9">
                                            <div class="form-group has-default">
                                                <select class="selectpicker" data-size="7" name="{{$key}}[{{ $i['name'] }}]" data-style="btn btn-primary" title="{{ __($i['title']) }}" data-live-search="true" tabindex={{ $k }}>
                                                    <option value="0">@Lang('None')</option>
                                                    @foreach ( $pages as $page )
                                                        <option value="{{ $page->id }}" {{ isset($i['value']) && $page->id == $i['value'] ? 'selected' : '' }}>{{ $page->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>                          
                                @elseif ( $i['type'] == 'true_false' )
                                    <div class="row">
                                        <label class="ml-4 col-md-2 col-form-label">{{ __($i['title']) }}</label>
                                        <div class="col-md-9">
                                            <div class="form-group has-default">
                                                <div class="togglebutton">
                                                    <label>
                                                        <input name="{{$key}}[{{ $i['name'] }}]" type="checkbox" {{ $i['value'] ? 'checked' : '' }}>
                                                        <span class="toggle"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                          
                                @endif
                            @endforeach
                        </div>
                        @php($i++)
                        @endforeach
                    </div>
                </div>
                <div class="card-footer">
                    <div class="mr-auto">
                        <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" value="@Lang('Cancel')" onclick="window.location.href='{{ route('admin.configuracion.index') }}'">
                    </div>
                    <div class="ml-auto">
                        <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd" value="@Lang('Save')">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </article>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.badge-colors .badge').click(function() {
        $('.badge-colors .badge').removeClass('active');
        $(this).addClass('active');
        var color = $(this).data('color');
        $(this).parent().parent().parent().find('input[type=hidden]').val(color);
    });
</script>
@endsection