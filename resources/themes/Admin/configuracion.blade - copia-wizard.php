@extends('Admin::layouts.admin')

@section('section-title')
    @Lang('Configuraction')
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">tune</i>
                </div>
                <h4 class="card-title">
                    @Lang('Configuracion')
                    <a id="add_config" href="javascript:void(0);" class="btn btn-primary btn-sm">@Lang('Add')</a>
                </h4>
            </div>
            <form id="form" class="form-horizontal" action="{{ route('admin.configuracion.store') }}" method="post" enctype="multipart/form-data">
            <div class="card-body ">
                    {{ csrf_field() }}
                    <div class="row">
                        <label class="ml-4 col-md-1 col-form-label">@Lang('Name')</label>
                        <div class="col-md-9">
                            <div class="form-group has-default">
                                <input name="name" type="text" class="form-control" value="{{ old('name') }}" required tabindex=1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4 col-sm-4 ml-5">
                        <h4 class="title">@Lang('Thumbnail')</h4>
                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                            <div class="fileinput-new thumbnail">
                                <img src="{{ asset('images/image_placeholder.jpg') }}">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="">
                            </div>
                            <div>
                                <span class="btn btn-rose btn-round btn-file">
                                <span class="fileinput-new">@Lang('Select image')</span>
                                <span class="fileinput-exists">@Lang('Change')</span>
                                <input type="hidden"><input type="file" name="file">
                                <div class="ripple-container"></div></span>
                                <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                                    <i class="fa fa-times"></i> @Lang('Delete')
                                </a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="mr-auto">
                        <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" name="previous" value="@Lang('Cancel')" onclick="window.location.href='{{ route('admin.configuracion.index') }}'">
                    </div>
                    <div class="ml-auto">
                        <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="@Lang('Save')">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalWizard" tabindex="-1" role="dialog" aria-labelledby="modalWizard" aria-hidden="true">
<div class="col-md-8 col-12 mr-auto ml-auto">
<!--      Wizard container        -->
<div class="wizard-container">
    <div class="card card-wizard" data-color="rose" id="wizardProfile">
        <div class="modal-dialog" role="document" style="width: 100%; margin: 0; max-width: 100%;">
            <div class="modal-content" style="box-shadow: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        <form action="" method="">
            <!--        You can switch " data-color="primary" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
            <div class="card-header text-center">
                <h3 class="card-title">
                    @Lang('Add new configuration')
                </h3>
            </div>
            <div class="wizard-navigation">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a class="nav-link active" href="#about" data-toggle="tab" role="tab">
                            @Lang('Name')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#account" data-toggle="tab" role="tab">
                            @Lang('Type')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#address" data-toggle="tab" role="tab">
    Address
</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="about">
                        <h5 class="info-text"> Let's start with the basic information (with validation)</h5>
                        <div class="row justify-content-center">
                            <div class="col-sm-4">
                                <div class="picture-container">
                                    <div class="picture">
                                        <img src="../../assets/img/default-avatar.png" class="picture-src" id="wizardPicturePreview" title="" />
                                        <input type="file" id="wizard-picture">
                                    </div>
                                    <h6 class="description">Choose Picture</h6>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group form-control-lg">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
            <i class="material-icons">face</i>
        </span>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput1" class="bmd-label-floating">First Name (required)</label>
                                        <input type="text" class="form-control" id="exampleInput1" name="firstname" required>
                                    </div>
                                </div>
                                <div class="input-group form-control-lg">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
            <i class="material-icons">record_voice_over</i>
        </span>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput11" class="bmd-label-floating">Second Name</label>
                                        <input type="text" class="form-control" id="exampleInput11" name="lastname" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-10 mt-3">
                                <div class="input-group form-control-lg">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
            <i class="material-icons">email</i>
        </span>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput1" class="bmd-label-floating">Email (required)</label>
                                        <input type="email" class="form-control" id="exampleemalil" name="email" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="account">
                        <h5 class="info-text"> What are you doing? (checkboxes) </h5>
                        <div class="row justify-content-center">
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="choice" data-toggle="wizard-checkbox">
                                            <input type="checkbox" name="jobb" value="Design">
                                            <div class="icon">
                                                <i class="fa fa-pencil"></i>
                                            </div>
                                            <h6>Design</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="choice" data-toggle="wizard-checkbox">
                                            <input type="checkbox" name="jobb" value="Code">
                                            <div class="icon">
                                                <i class="fa fa-terminal"></i>
                                            </div>
                                            <h6>Code</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="choice" data-toggle="wizard-checkbox">
                                            <input type="checkbox" name="jobb" value="Develop">
                                            <div class="icon">
                                                <i class="fa fa-laptop"></i>
                                            </div>
                                            <h6>Develop</h6>
                                        </div>
                                        <select class="selectpicker" data-style="btn btn-primary btn-round" title="Single Select" data-size="7">
            <option disabled selected>Choose city</option>
            <option value="2">Foobar</option>
            <option value="3">Is great</option>
        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="address">
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <h5 class="info-text"> Are you living in a nice area? </h5>
                            </div>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label>Street Name</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Street No.</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group select-wizard">
                                    <label>Country</label>
                                    <select class="selectpicker" data-size="7" data-style="select-with-transition" title="Single Select">
        <option value="Afghanistan"> Afghanistan </option>
        <option value="Albania"> Albania </option>
        <option value="Algeria"> Algeria </option>
        <option value="American Samoa"> American Samoa </option>
        <option value="Andorra"> Andorra </option>
        <option value="Angola"> Angola </option>
        <option value="Anguilla"> Anguilla </option>
        <option value="Antarctica"> Antarctica </option>
        </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="mr-auto">
                    <input type="button" class="btn btn-previous btn-fill btn-default btn-wd disabled" name="previous" value="Previous">
                </div>
                <div class="ml-auto">
                    <input type="button" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="Next">
                    <input type="button" class="btn btn-finish btn-fill btn-primary btn-wd" name="finish" value="Finish" style="display: none;">
                </div>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>
<!-- wizard container -->

</div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {

        $('#add_config').click(function() {
            $('#modalWizard').modal('toggle');
            initWizard();
            //$('.card.card-wizard').addClass('active');
            setTimeout(function() {
                $('.card.card-wizard').addClass('active');
            }, 600);
            $('.wizard-container .moving-tab').css('width', '274.443px');
            $('.wizard-container .moving-tab').css('transform', 'translate3d(-8px, 0px, 0px)');
            $('.wizard-container .moving-tab').css('transition', 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1) 0s');
        });

        function initWizard() {
            // Code for the Validator
            var $validator = $('.card-wizard form').validate({
                rules: {
                    firstname: {
                        required: true,
                        minlength: 3
                    },
                    lastname: {
                        required: true,
                        minlength: 3
                    },
                    email: {
                        required: true,
                        minlength: 3,
                    }
                },

                highlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
                },
                success: function(element) {
                    $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
                },
                errorPlacement: function(error, element) {
                    $(element).append(error);
                }
            });

            // Wizard Initialization
            $('.card-wizard').bootstrapWizard({
                'tabClass': 'nav nav-pills',
                'nextSelector': '.btn-next',
                'previousSelector': '.btn-previous',

                onNext: function(tab, navigation, index) {
                    var $valid = $('.card-wizard form').valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    }
                },

                onInit: function(tab, navigation, index) {
                    //check number of tabs and fill the entire row
                    var $total = navigation.find('li').length;
                    var $wizard = navigation.closest('.card-wizard');

                    $first_li = navigation.find('li:first-child a').html();
                    $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
                    $('.card-wizard .wizard-navigation').append($moving_div);

                    refreshAnimation($wizard, index);

                    $('.moving-tab').css('transition', 'transform 0s');
                },

                onTabClick: function(tab, navigation, index) {
                    var $valid = $('.card-wizard form').valid();

                    if (!$valid) {
                        return false;
                    } else {
                        return true;
                    }
                },

                onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;

                    var $wizard = navigation.closest('.card-wizard');

                    // If it's the last tab then hide the last button and show the finish instead
                    if ($current >= $total) {
                        $($wizard).find('.btn-next').hide();
                        $($wizard).find('.btn-finish').show();
                    } else {
                        $($wizard).find('.btn-next').show();
                        $($wizard).find('.btn-finish').hide();
                    }

                    button_text = navigation.find('li:nth-child(' + $current + ') a').html();

                    setTimeout(function() {
                        $('.moving-tab').text(button_text);
                    }, 150);

                    var checkbox = $('.footer-checkbox');

                    if (!index == 0) {
                        $(checkbox).css({
                            'opacity': '0',
                            'visibility': 'hidden',
                            'position': 'absolute'
                        });
                    } else {
                        $(checkbox).css({
                            'opacity': '1',
                            'visibility': 'visible'
                        });
                    }

                    refreshAnimation($wizard, index);
                }
            });


            // Prepare the preview for profile picture
            $("#wizard-picture").change(function() {
                readURL(this);
            });

            $('[data-toggle="wizard-radio"]').click(function() {
                wizard = $(this).closest('.card-wizard');
                wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
                $(this).addClass('active');
                $(wizard).find('[type="radio"]').removeAttr('checked');
                $(this).find('[type="radio"]').attr('checked', 'true');
            });

            $('[data-toggle="wizard-checkbox"]').click(function() {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).find('[type="checkbox"]').removeAttr('checked');
                } else {
                    $(this).addClass('active');
                    $(this).find('[type="checkbox"]').attr('checked', 'true');
                }
            });

            $('.set-full-height').css('height', 'auto');

            //Function to show image before upload

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(window).resize(function() {
                $('.card-wizard').each(function() {
                    $wizard = $(this);

                    index = $wizard.bootstrapWizard('currentIndex');
                    refreshAnimation($wizard, index);

                    $('.moving-tab').css({
                        'transition': 'transform 0s'
                    });
                });
            });

            function refreshAnimation($wizard, index) {
                $total = $wizard.find('.nav li').length;
                $li_width = 100 / $total;

                total_steps = $wizard.find('.nav li').length;
                move_distance = $wizard.width() / total_steps;
                index_temp = index;
                vertical_level = 0;

                mobile_device = $(document).width() < 600 && $total > 3;

                if (mobile_device) {
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width', $li_width + '%');

                step_width = move_distance;
                move_distance = move_distance * index_temp;

                $current = index + 1;

                if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
                    move_distance -= 8;
                } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
                    move_distance += 8;
                }

                if (mobile_device) {
                    vertical_level = parseInt(index / 2);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
            }
        }

    });
</script>
@yield('scripts2')
@endsection