@extends('Admin::layouts.admin')

@section('section-title')
    @lang($section_title)
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <article class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">library_books</i>
                    </div>
                    <h4 class="card-title">
                        {{ isset($coupon) ? __('Edit') : __('Create') }}
                    </h4>
                </div>

                <div class="card-body">
                    <form id="PostForm" class="form-horizontal" action="@if (isset($coupon)){{ route($section_route . ".update", ['id' => $coupon->id]) }}@else{{ route($section_route . ".store") }}@endif" method="POST">
                        {{ csrf_field() }}

                        @if (isset($coupon))
                            <input type="hidden" name="_method" value="PUT">
                        @endif

                        <div class="flex-row pt-3">
                            <label class="col-md-12">@lang('Código')</label>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="code" type="text" class="form-control" required value="{{ isset($coupon) ? $coupon->code : old('code', '') }}"/>
                                </div>
                            </div>
                        </div>

                        <div class="flex-row pt-3">
                            <label class="col-md-12">@lang('Status')</label>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select class="selectpicker" data-style="btn btn-primary" name="status">
                                        @foreach($statuses as $key => $status)
                                            <option value="{{ $key }}" {{ ((isset($coupon) && $key == $coupon->status) || old('status') == $key) ? 'selected' : '' }}>@lang($status)</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="flex-row pt-3">
                            <label class="col-md-12">@lang('Value')</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input name="value" type="number" class="form-control" required step="0.01" min="0" value="{{ isset($coupon) ? $coupon->value : old('value', '') }}"/>
                                    <span class="input-group-addon">€</span>
                                </div>
                            </div>
                        </div>

                        <div class="flex-row pt-3">
                            <label class="col-md-12">@lang('Máx. usos')</label>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="total_uses" type="number" class="form-control" step="1" min="1" value="{{ isset($coupon) && $coupon->total_uses ? $coupon->total_uses : old('total_uses', '') }}"/>
                                </div>
                            </div>
                        </div>

                        <div class="flex-row pt-3">
                            <label class="col-md-12">@lang('Máx. usos por usuario')</label>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="uses_per_user" type="number" class="form-control" step="1" min="1" value="{{ isset($coupon) && $coupon->uses_per_user ? $coupon->uses_per_user : old('uses_per_user', '') }}"/>
                                </div>
                            </div>
                        </div>

                        <div class="flex-row pt-3">
                            <label class="col-md-12">@lang('Mín. valor compra')</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input name="min_purchase_amount" type="number" class="form-control" step="1" min="1" value="{{ isset($coupon) && $coupon->min_purchase_amount ? $coupon->min_purchase_amount : old('min_purchase_amount', '') }}"/>
                                    <span class="input-group-addon">€</span>
                                </div>
                            </div>
                        </div>

                        <div class="flex-row pt-3">
                            <label class="col-md-12">@lang('Fecha mínima')</label>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="min_date" type="date" class="form-control" step="1" min="1" value="{{ isset($coupon) && $coupon->min_date ? $coupon->min_date->toDateString() : old('min_date', '') }}"/>
                                </div>
                            </div>
                        </div>

                        <div class="flex-row pt-3">
                            <label class="col-md-12">@lang('Fecha máxima')</label>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="max_date" type="date" class="form-control" step="1" min="1" value="{{ isset($coupon) && $coupon->max_date ? $coupon->max_date->toDateString() : old('max_date', '') }}"/>
                                </div>
                            </div>
                        </div>

                        <div class="flex-row pt-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <a class="btn btn-default" href="{{ route($section_route . '.index') }}">@lang('Cancel')</a>
                                    <button class="btn btn-primary" type="submit">{{ isset($coupon) ? __('Save') : __('Create') }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </article>
        </div>
    </div>

@endsection