@extends('Admin::layouts.admin')

@section('section-title')
    @lang($section_title)
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">library_books</i>
                    </div>
                    <h4 class="card-title">
                        @lang(ucfirst($post_type_plural))

                        <a href='{{ route($section_route . ".create") }}' class="btn btn-primary btn-sm">@lang('Add')</a>
                    </h4>
                </div>

                <div class="card-body">

                    <div class="material-datatables">
                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                            <tr>
                                <th>@lang('Código')</th>
                                <th>@lang('Status')</th>
                                <th>@lang('Value')</th>
                                <th>@lang('Máx. usos')</th>
                                <th>@lang('Usado')</th>
                                <th>@lang('Caduca')</th>
                                <th class="disabled-sorting text-right">@lang('Actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($coupons as $coupon)
                                <tr>
                                    <td><strong>{{ $coupon->code }}</strong></td>
                                    <td class="@if ($coupon->status == 1)text-success @else text-warning @endif">{{ $coupon->present()->statusString }}</td>
                                    <td>{{ $coupon->present()->valueCurrency }}</td>
                                    <td>{{ $coupon->present()->maxUses }}</td>
                                    <td>{{ $coupon->present()->timesUsed }}</td>
                                    <td>{{ $coupon->max_date ? $coupon->max_date->toDatetimeString() : __('Sin caducidad') }}</td>
                                    <td class="td-actions text-right">
                                        <a href="{{ route($section_route . '.edit', $coupon->id) }}" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-success" title="@lang('Edit')">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        @if (!$coupon->purchases->count())
                                            <button data-url="{{ route($section_route . '.destroy', $coupon->id) }}" data-name="@lang('Are you sure to delete :name?', ['name' => $coupon->code])" type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove" title="@lang('Delete')">
                                                <i class="material-icons">close</i>
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            <div class="loader">
                                <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                    <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                                </svg>
                            </div>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var table = $('.table').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [25, 50, -1],
                    [25, 50, "@lang('All')"]
                ],
                responsive: true,
                language: { "url": "{{ asset($datatableLangFile ?: 'js/datatables/Spanish.json') }}" }
            });

            // Delete a record
            table.on('click', '.remove', function(e) {
                var $tr = $(this).closest('tr');
                var url = $(this).data('url');
                var title = $(this).data('name');
                swal({
                    text: title,
                    //title: 'Are you sure?',
                    //text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: '@lang('Delete')',
                    cancelButtonText: '@lang('Cancel')',
                    buttonsStyling: false
                }).then(function(result) {
                    if ( result.value ) {
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: {'_token': '{{csrf_token()}}', '_method': 'DELETE'},
                            success: function(data) {
                                swal({
                                    title: "@lang('Deleted')",
                                    text: "@lang('Item deleted succesfully.')",
                                    type: 'success',
                                    confirmButtonClass: "btn btn-success",
                                    buttonsStyling: false
                                });
                                table.row($tr).remove().draw();
                                e.preventDefault();
                            },
                            error: function(data) {
                                swal({
                                    title: "@lang('Error')",
                                    text: "@lang('Error saving data')",
                                    type: 'error',
                                    confirmButtonClass: "btn",
                                    buttonsStyling: false
                                });
                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection