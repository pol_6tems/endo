<div class="sidebar" data-color="rose" data-background-color="black" data-image="{{asset($_admin.'img/sidebar-1.jpg')}}">
<!--    Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
        Tip 2: you can also add an image using data-image tag
-->
    <div class="logo">
        <a href="{{ route('index') }}" class="simple-text logo-mini">
            @if ( empty($_config['app_icon']['file']) )
                <img src="{{asset('assets/img/6tems_6_round_50.png')}}" alt=""/>
            @else
                <img src="{{ $_config['app_icon']['obj']->get_file_url() }}" alt="">
            @endif
        </a>
        <a href="{{ route('index') }}" class="simple-text logo-normal">
            {{ $app_name }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{ auth()->user()->get_avatar_url() }}" />
            </div>
            <div class="user-info">
                <a data-toggle="collapse" href="#collapseExample" class="username">
                    <span>
                        {{ cut_text(auth()->user()->fullname(), 15) }}
                        <b class="caret"></b>
                    </span>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        {{--<li class="nav-item">
                            <a class="nav-link" href="#">
                                <span class="sidebar-mini">
                                    {{ obtenirInicials(__('My Profile')) }}
                                </span>
                                <span class="sidebar-normal"> @Lang('My Profile') </span>
                            </a>
                        </li>--}}
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.users.edit', ['id' => auth()->user()->id]) }}">
                                <span class="sidebar-mini">
                                    {{ obtenirInicials(__('Edit Profile')) }}
                                </span>
                                <span class="sidebar-normal"> @Lang('Edit Profile') </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.configuracion.index') }}">
                                <span class="sidebar-mini">
                                    {{ obtenirInicials(__('Settings')) }}
                                </span>
                                <span class="sidebar-normal"> @Lang('Settings') </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
        @foreach ( $menu_items as $menu )
            <li class="nav-item @if($menu->is_active) active @endif">
                <a class="nav-link" href="{{ $menu->submenus->count() > 0 ? '#' . $menu->name : $menu->href }}" @if ( $menu->submenus->count() > 0 ) data-toggle="collapse" aria-expanded="{{$menu->is_active?'true':false}}" @endif>
                    <i class="material-icons">{{$menu->icon}}</i>
                    <p> {{__($menu->name)}}
                        @if ( $menu->submenus->count() > 0 )<b class="caret"></b>@endif
                    </p>
                </a>
                @if ( $menu->submenus->count() > 0 )
                <div class="collapse @if($menu->is_active) show @endif" id="{{$menu->name}}">
                    <ul class="nav">
                    @foreach ( $menu->submenus as $sub )
                        <li class="nav-item @if ( $sub->is_active ) active @endif">
                            <a class="nav-link" href="{{ $sub->href }}">
                                @if ( !empty($sub->icon) )
                                    <i class="material-icons">{{$sub->icon}}</i>
                                @else
                                    <span class="sidebar-mini"> {{ obtenirInicials(__($sub->name)) }}</span>
                                @endif
                                <span class="sidebar-normal"> @Lang($sub->name) </span>
                            </a>
                        </li>
                    @endforeach
                    </ul>
                </div>
                @endif
            </li>
        @endforeach
        </ul>
    </div>
</div>