@if (count($idiomes) > 1)
<div class="row">
    <div class="col-lg-12">
        <div class="toolbar" style="z-index: 99;">
            <ul class="nav nav-pills nav-pills-primary">
            @foreach($idiomes as $language_item)
                    <li class="nav-item {{ ($language_item->code == $currentLang) ? 'active' : '' }}">
                        <a class="nav-link {{ ($language_item->code == $currentLang) ? 'active' : '' }} nav-lang-{{ $language_item->code }}" data-code="{{ $language_item->code }}" data-lang="{{ $language_item->name }}" data-toggle="tab" href="#{{ $prepend . $language_item->code }}">{{ $language_item->code }}</a>
                    </li>
            @endforeach
            </ul>
        </div>
    </div>
</div>
@endif