<footer class="footer" style="display:none!important;">
    <div class="container-fluid">
        <nav class="float-left">
            <ul>
                <li>
                    <a href="http://www.6tems.com/es/index.html" target="_blank">
                        6TEMS
                    </a>
                </li>
                <li>
                    <a href="http://www.6tems.com/es/index.html" target="_blank">
                        @Lang('About Us')
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright float-right">
            © 2019, @Lang('made by')&nbsp;
            <a href="http://www.6tems.com/es/index.html" target="_blank"><img style="width: 30px;" src="{{asset('images/6tems_round_50.png')}}"></a>.
        </div>
    </div>
</footer>