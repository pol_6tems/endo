<!-- Titol -->
<div class="pt-3 flex-row ">
    <label class="col-md-12">@Lang('Title')</label>
    <div class="col-md-12">
        <div class="form-group has-default">
            <input name="{{$language_item->code}}[title]" type="text" class="form-control" value="{{ $post->translate($language_item->code, true)->title }}" tabindex=1 {{ ($language_item->code == $language_code) ? 'required' : '' }}>
        </div>
    </div>
</div>

<!-- URL -->
<div class="pt-3 flex-row ">
    <label class="col-md-12">@Lang('URL')</label>
    <div class="col-md-12">
        <div class="form-group has-default">
            <input name="{{$language_item->code}}[post_name]" type="text" class="form-control" value="{{ $post->translate($language_item->code, true)->post_name }}" tabindex=2 {{ ($language_item->code == $language_code) ? 'required' : '' }}>
        </div>
    </div>
</div>

@if( in_array($post->type, ['post', 'page']) || ($params && $params->content) )
<!-- WYSIWYG EDITOR -->
<div class="pt-3 flex-row  {{ ($language_item->code != $language_code) ? 'summernote-not-first' : '' }}">
    <label class="col-md-12">@Lang('Description')</label>
    <div class="col-md-12">
        <div class="form-group has-default">
        <textarea id="description_{{$language_item->code}}" data-lang="{{$language_item->code}}" name="{{$language_item->code}}[description]" class="form-control summernote" tabindex=3>{!! $post->translate($language_item->code, true)->description !!}</textarea>
        <div id="descripcio_count_{{$language_item->code}}"></div>
        </div>
    </div>
</div>
@endif

@foreach ($cf_groups as $key => $cf_group)
    @if($cf_group->position == 'main' && !$cf_group->fields->isEmpty())
        
        @if (Auth::user()->isAdmin())
        <div class="flex-row mt-4" style="justify-content: flex-end;">
            <div class="header-right">
                <a href="{{ route('admin.custom_fields.edit', $cf_group->id) }}?post_type={{$post_type}}" target="_blank" rel="tooltip">
                    <i class="material-icons">settings</i>
                </a>
            </div>
        </div>
        @endif

        <div id="cf_group{{ $cf_group->id }}">
            {{ mostrarIdiomes($_languages, $language_code, 'panel'.$cf_group->id) }}
            <!-- Tab de General -->
            <div class="tab-content">
                @foreach($_languages as $key => $language_item)
                <div id="panel{{ $cf_group->id }}{{ $language_item->code }}" class="tab-pane {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                    <div class="custom_field_group_fields">
                        @foreach ($cf_group->fields as $k => $custom_field)
                            @php($viewParams = [
                                'title' => $custom_field->title,
                                'name' => "custom_fields[".$language_item->code."][".$custom_field->id . "]",
                                'value' => $post->get_field( $custom_field->name, $language_item->code ),
                                'params' => json_decode($custom_field->params),
                                'position' => $cf_group->position,
                                'custom_field' => $custom_field,
                                'order' => $k,
                                'lang' => $language_item->code,
                            ])

                            @if (View::exists('Front::admin.custom_fields.' . $custom_field->type))
                                @includeIf('Front::admin.custom_fields.' . $custom_field->type, $viewParams)
                            @elseif (View::exists('Admin::default.custom_fields.' . $custom_field->type))
                                <!-- General -->
                                @includeIf('Admin::default.custom_fields.' . $custom_field->type, $viewParams)
                            @else
                                @php($module = explode(".", $custom_field->type)[0])
                                @php($field = explode(".", $custom_field->type)[1])
                                @includeIf('Modules::'.$module.'.resources.views.custom_fields.'.$field, $viewParams)
                            @endif
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    @endif
@endforeach