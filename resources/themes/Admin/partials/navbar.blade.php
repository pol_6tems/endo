<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-minimize">
                <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                    <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                    <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                </button>
            </div>
            <a class="navbar-brand" href="#">@yield('section-title')</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form" method="GET" action="{{ route('admin.search.index') }}">
                <div class="input-group no-border">
                    <input type="text" name="s" value="{{!empty($search_key)?$search_key:''}}" class="form-control" placeholder="@Lang('Search')...">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin') }}">
                        <i class="material-icons">dashboard</i>
                        <p class="d-lg-none d-md-block">
                            Stats
                        </p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">language</i>
                        <span class="notification">{{app()->getLocale()}}</span>
                        <p class="d-lg-none d-md-block">
                            @Lang('Language')
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownLanguage">
                    @foreach ($_languages as $language)
                        <a class="dropdown-item" href="{{ $language->url . (request()->getQueryString() ? ('?' . request()->getQueryString()) : '') }}">@Lang($language->name)</a>
                    @endforeach
                    </div>
                </li>
                <li class="nav-item dropdown">
                    @php ( $notifications = get_notifications() )
                    <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">notifications</i>
                        @if ( count($notifications) > 0 )
                            <span class="notification">
                                {{ count($notifications) }}
                            </span>
                        @endif
                        <p class="d-lg-none d-md-block">
                            @Lang('Notifications')
                        </p>
                    </a>
                    @if ( count($notifications) > 0 )
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            @foreach ( $notifications as $notif )
                                <a class="dropdown-item" href="{{ $notif['href'] }}">{{ $notif['msg'] }}</a>
                            @endforeach
                        </div>
                    @endif
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">person</i>
                        <p class="d-lg-none d-md-block">
                            @Lang('Account')
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                        <a class="dropdown-item" href="{{ route('admin.users.edit', ['id' => auth()->user()->id]) }}">@Lang('Profile')</a>
                        <a class="dropdown-item" href="{{ route('admin.configuracion.index') }}">@Lang('Settings')</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            @Lang('Logout')
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->