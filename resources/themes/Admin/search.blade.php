@extends('Admin::layouts.admin')

@section('section-title')
    @Lang('Search'): {{ $search_key }}
@endsection

@section('content')
@if (!empty($widgets))
    @foreach ($widgets as $w)
    
    <div class="card mt-5">
        <div class="card-header card-header-{{ $w['icon_class'] }} card-header-icon">
            <div class="card-icon">
                <i class="material-icons">{{ $w['icon'] }}</i>
            </div>
            <h4 class="card-title">
                {{ $w['title'] }}
                @if ( !empty($w['list_url']) )
                    <a href="{{ $w['list_url'] }}" rel="tooltip" class="btn btn-just-icon btn-sm btn-info ml-2" title="@Lang('List')">
                        <i class="material-icons">list</i>
                    </a>
                @endif
            </h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <div class="table-responsive table-sales">
                    <table class="table search">
                        <thead>
                            <tr>
                            @foreach( $w['headers'] as $h )
                                <th><strong>{{ $h }}</strong></th>
                            @endforeach
                                <th style="min-width: 50px;" colspan=4></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse( $w['rows'] as $r )
                                <tr>
                                @foreach( $r as $k => $f )
                                    @if( !is_int($k) && !empty($f) )
                                        @if ( $k == 'edit' )
                                            <td class="td-actions text-right">
                                                <a href="{{ $f }}" rel="tooltip" class="btn btn-just-icon btn-sm btn-success" title="@Lang('Edit')">
                                                    <i class="material-icons">edit</i>
                                                </a>
                                            </td>
                                        @else
                                            <td class="td-actions text-right">
                                                <a href="{{ $f }}" rel="tooltip" class="btn btn-just-icon btn-sm btn-warning" title="@Lang('Show')">
                                                    <i class="material-icons">personal_video</i>
                                                </a>
                                            </td>
                                        @endif
                                    @else
                                        <td>{!! $f !!}</td>
                                    @endif
                                @endforeach
                                </tr>
                            @empty
                                <tr><td colspan="{{ count($w['headers']) }}">@Lang('No entries found.')</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@endif
@endsection