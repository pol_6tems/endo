@extends('Admin::layouts.admin')

@section('section-title')
    @Lang('Options')
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">ballot</i>
                </div>
                <h4 class="card-title">
                    @Lang('Options')
                </h4>
            </div>
            <div class="card-body">
                <div class="toolbar">
                    <form class="form-inline" method="POST" action="{{ route('admin.opciones.store') }}">
                        {!! csrf_field() !!}
                        <div class="form-group mr-4">
                            <label for="key" class="bmd-label-floating">@Lang('Key')</label>
                            <input type="text" name="key" class="form-control" placeholder="">
                        </div>
                        <div class="form-group mr-4">
                            <label for="value" class="bmd-label-floating">@Lang('Value')</label>
                            <input type="text" name="value" class="form-control" placeholder="">
                        </div>
                        <div class="form-group mr-4">
                            <label for="descripcion" class="bmd-label-floating">@Lang('Description')</label>
                            <input type="text" name="descripcion" class="form-control" placeholder="">
                        </div>
                        <span class="form-group bmd-form-group"> <!-- needed to match padding for floating labels -->
                            <button type="submit" class="btn btn-primary btn-sm">@Lang('Add')</button>
                        </span>
                    </form>
                </div>
                <div class="material-datatables">
                    <table id="tabla-traducciones" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>@Lang('Key')</th>
                            <th>@Lang('Value')</th>
                            <th>@Lang('Description')</th>
                            <th width="80px;">@Lang('Action')</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td><a href="#" class="translate-key" data-title="" data-type="text" data-pk="{{ $item->key }}" data-url="{{ route('admin.opciones.update_key') }}">{{ $item->key }}</a></td>
                                    <td><a href="#" data-title="" class="translate" data-code="{{ $item->value }}" data-type="textarea" data-pk="{{ $item->key }}" data-url="{{ route('admin.opciones.update_value') }}">{{ $item->value }}</a></td>
                                    <td><a href="#" data-title="" class="translate" data-code="{{ $item->descripcion }}" data-type="textarea" data-pk="{{ $item->key }}" data-url="{{ route('admin.opciones.update_desc') }}">{{ $item->descripcion }}</a></td>
                                    <td>
                                        <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger" data-action="{{ route('admin.opciones.destroy', base64_encode($item->key)) }}">
                                            <i class="material-icons">close</i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link href="{{asset($_admin . 'css/bootstrap-editable.css')}}" rel="stylesheet" />
@endsection

@section('scripts')
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-editable.min.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.translate').editable({
        params: function(params) {
            params.code = $(this).editable().data('code');
            return params;
        }
    });


    $('.translate-key').editable({
        validate: function(value) {
            if($.trim(value) == '') {
                return 'Key is required';
            }
        }
    });


    $('body').on('click', '.remove-key', function(){
        var cObj = $(this);


        if ( confirm("@Lang('Are you sure to delete it?')") ) {
            $.ajax({
                url: cObj.data('action'),
                method: 'DELETE',
                success: function(data) {
                    cObj.parents("tr").remove();
                    $.snackbar({content: "@Lang('Translation deleted')", timeout: 3000});
                }
            });
        }
    });
</script>
<script>
$(document).ready(function() {
    $('.table').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "@Lang('All')"]
        ],
        responsive: true,
        language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" }
    });

    var table = $('.table').DataTable();
});
</script>
@endsection