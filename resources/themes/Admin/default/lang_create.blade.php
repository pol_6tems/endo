@extends('Admin::layouts.admin')

@section('section-title')
    {{ $section_title }}
@endsection

@section('content')
<div class="row">
    <div class="col-md-{{ $width or '12' }}">
        <form id="form" class="form-horizontal" action="{{ route($route . '.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <article class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">{{ $section_icon }}</i>
                    </div>
                    <h4 class="card-title">@Lang('Add new')</h4>
                </div>
                <div class="card-body">
                    @if ($errors->count() > 0)
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {{ mostrarIdiomes($_languages, $language_code) }}

                    <div class="tab-content">
                        @foreach($_languages as $language_item)
                        <div id="{{$language_item->code}}" class="tab-pane {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                            @foreach ( $rows as $key => $params )
                                @if ( empty($params['type']) )
                                    @includeIf('Admin::default.subforms.text', ['is_create' => true, 'params' => $params, 'key' => $key, 'lang' => $language_item->code])
                                @elseif ( $params['type'] == 'subform' )
                                    @includeIf($params['value'], ['is_create' => true, 'params' => $params, 'key' => $key, 'lang' => $language_item->code])
                                @else
                                    @includeIf('Admin::default.subforms.' . $params['type'], ['is_create' => true, 'params' => $params, 'key' => $key, 'lang' => $language_item->code])
                                @endif
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                </div>
            </article>
            <article class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">tune</i>
                    </div>
                    <h4 class="card-title">@Lang('Opcions')</h4>
                </div>
                <div class="card-body" style="padding: 20px 80px;">
                    {{-- Automatitzar --}}
                    <div class="row">
                        <div class="form-check col-md-6 mt-4">
                            <label class="form-check-label">
                                <input type="hidden" name="params[imatge]" value="0" />
                                <input class="form-check-input" type="checkbox" name="params[imatge]" value="1" {{ (isset($parameters->imatge) && $parameters->imatge == '1') ? 'checked' : '' }} />
                                @Lang('Imatge')
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check col-md-6 mt-4">
                            <label class="form-check-label">
                                <input type="hidden" name="params[content]" value="0" />
                                <input class="form-check-input" type="checkbox" name="params[content]" value="1" {{ (isset($parameters->content) && $parameters->content == '1') ? 'checked' : '' }} />
                                @Lang('Content')
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check col-md-6 mt-4">
                            <label class="form-check-label">
                                <input type="hidden" name="params[author]" value="0" />
                                <input class="form-check-input" type="checkbox" name="params[author]" value="1" {{ (isset($parameters->author) && $parameters->author == '1') ? 'checked' : '' }} />
                                @Lang('Author')
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check col-md-6 mt-4">
                            <label class="form-check-label">
                                <input type="hidden" name="params[parent]" value="0" />
                                <input class="form-check-input" type="checkbox" name="params[parent]" value="1" {{ (isset($parameters->parent) && $parameters->parent == '1') ? 'checked' : '' }} />
                                @Lang('Parent')
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check col-md-6 mt-4">
                            <label class="form-check-label">
                                <input type="hidden" name="params[published]" value="0" />
                                <input class="form-check-input" type="checkbox" name="params[published]" value="1" {{ (isset($parameters->published) && $parameters->published == '1') ? 'checked' : '' }} />
                                @Lang('Published')
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </article>
            <article class="card">
                <div class="card-footer">
                    <div class="mr-auto">
                        <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" name="previous" value="@Lang('Tornar')" onclick="window.location.href='{{ route($route . '.index') }}'">
                    </div>
                    <div class="ml-auto">
                        <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="@Lang('Create')">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </article>
        </form>
    </div>
</div>
@endsection

@section('scripts')
@yield('scripts2')
@endsection