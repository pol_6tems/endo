@extends('Admin::layouts.admin')

@section('section-title')
    {{ $section_title }}
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">{{ $section_icon }}</i>
                </div>
                <h4 class="card-title">
                    {{ $section_title }}

                    @if ( isset($toolbar_header) && !empty($toolbar_header) )
                        @foreach ( $toolbar_header as $tool )
                            {!! $tool !!}
                        @endforeach
                    @endif
                </h4>
            </div>
            <div class="card-body">

            <div class="inbox_msg">

                <!-- CONTACTES -->
                <div class="inbox_people">
                    <div class="headind_srch">
                        
                        <!-- Title -->
                        <div class="recent_heading">
                            <h4>@Lang('Chats')</h4>
                        </div>
                        
                        <!-- Search -->
                        <div class="srch_bar">
                            <div class="stylish-input-group">
                                <input type="text" class="search-bar"  placeholder="@Lang('Search')..." >
                                <button class="search-btn" type="button">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>

                    </div>
                

                    <div class="inbox_chat">

                    @php( $active_chat = null )
                    @foreach ( $items as $key => $item )
                        @if ( $key == 0 ) @php( $active_chat = $item->chat ) @endif
                        <div class="chat_list chat_id-{{$item->chat}} {{$key == 0 ? 'active_chat' : ''}}" onclick="seleccionar_chat( '{{$item->chat}}' );">
                            <div class="chat_people">
                                <div class="chat_img">
                                    <img class="avatar-icon" src="{{ $item->user_avatar }}">
                                </div>
                                @if ( !empty($item->last_mensaje) )
                                    <div class="chat_ib {{ !$item->last_mensaje->visto ? 'nuevo_mensaje' : '' }}">
                                        <h5>
                                            {{ $item->user_name }}
                                            <span class="chat_date">
                                                {{$item->last_mensaje->created_at->format('G:i · d/m/Y')}}
                                            </span>
                                            <br>
                                            {{-- <i>{{ $item->user_email }}</i> --}}
                                        </h5>
                                        <p>{{ get_excerpt(nl2br($item->last_mensaje->show_message(true)), 9) }}</p>
                                    </div>
                                @else
                                    <div class="chat_ib">
                                        <h5>
                                            {{ $item->user_name }}
                                            <i>{{ $item->user_email }}</i>
                                        </h5>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                        
                    </div>
                </div>
                <!-- end CONTACTES -->

                <!-- MISSATGES -->
                <div class="mesgs">
                
                @foreach ( $items as $key => $item )
                    <div class="msg_history" id="chat_id-{{$item->chat}}" data-chat_id="{{ $item->chat_noencode }}" data-chat_type="{{ $item->last_mensaje->type }}" style="display:none;">
                    
                        @foreach ( $item->mensajes as $mensaje )
                            @if ( $mensaje->user_id != Auth::user()->id )
                                <div class="incoming_msg">
                                    <div class="incoming_msg_img">
                                        @php ( $user_avatar = $user_avatar_default )
                                        @php ( $user_name = $mensaje->name )
                                        @if ( !empty($mensaje->user_id) && !empty($mensaje->user) )
                                            @php ( $avatar = asset('storage/avatars/' . $mensaje->user->avatar) )
                                            @if ( file_exists(public_path('storage/avatars/' . $mensaje->user->avatar)) ) @php ( $user_avatar = $avatar ) @endif
                                            @php ( $user_name = $mensaje->user->fullname() )
                                        @endif
                                        <img class="avatar-icon" src="{{ $user_avatar }}">
                                    </div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">
                                            <span style="font-size: 15px;line-height: 30px;">
                                                {{ $user_name }}
                                            </span>
                                            <p>{!! nl2br($mensaje->show_message()) !!}</p>
                                            @if ( !empty($mensaje->fitxer) )
                                                <span class="fitxer">
                                                    <a target="_blank" title="{{ $mensaje->fitxer }}" href="{{ asset('storage/' . $mensaje->fitxer) }}">
                                                        <i class="material-icons">description</i>
                                                    </a>
                                                </span>
                                            @endif
                                            <span class="time_date">
                                                {{ $mensaje->created_at->format('G:i · d/m/Y') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="outgoing_msg">
                                    <div class="sent_msg">
                                    <p>{!! nl2br($mensaje->show_message()) !!}</p>
                                        <span class="time_date">
                                            {{ $mensaje->created_at->format('G:i · d/m/Y') }}
                                        </span>
                                    </div>
                                </div>
                            @endif

                        
                        @endforeach

                    </div>

                @endforeach
                    
                    <!-- TYPE A MESSAGE -->
                    <div class="type_msg">

                        <!-- Botones del chat -->
                        @foreach ( $items as $key => $item )
                            @if ( !empty($item->buttons) )
                                <ul class="input_msg_buttons" id="chat_btns_id-{{$item->chat}}">
                                @foreach ( $item->buttons as $chat_btn )
                                    <li>
                                        <button @if (filter_var($chat_btn['url'], FILTER_VALIDATE_URL))data-href="{{ $chat_btn['url'] }}" data-text="{{ $chat_btn['text'] }}"@else data-text="{{ $chat_btn['url'] }}"@endif  @if (isset($chat_btn['action']) && $chat_btn['action'])data-action="{{ $chat_btn['action'] }}"@endif class="btn btn-default btn-sm">
                                            {{ $chat_btn['text-btn'] }}
                                        </button>
                                    </li>
                                @endforeach
                                </ul>
                            @endif
                        @endforeach
                        <!-- end Botones del chat -->

                        <div class="input_msg_write">
                            {{--<input id="mensaje-box" type="text" class="write_msg" placeholder="@Lang('Type a message')" />--}}
                            <textarea id="mensaje-box" type="text" class="write_msg" placeholder="@Lang('Type a message')"></textarea>
                            <button id="enviar_missatge_btn" title="@Lang('Send')" class="msg_send_btn btn btn-primary btn-round btn-just-icon" type="button" style="position: absolute;"><i class="material-icons" aria-hidden="true">send</i></button>
                        </div>
                    </div>
                    
                </div>
                <!-- end MISSATGES -->

            </div>

            </div>
        </div>
    </div>
</div>
@endsection	

@section('styles')
<link href="{{asset('css/endo-admin-messaging.css')}}" rel="stylesheet" type="text/css">
<style>
.inbox_chat { overflow-y: auto; }
.msg_history { min-height: 600px;margin-bottom: 40px; }
.inbox_people { min-height: 700px; }
</style>
@endsection

@section('scripts')
<script>
var $active_chat = "{{$active_chat}}";
var $last_update_chat = new Date();

$(document).ready(function(){
    seleccionar_chat( '{{ $active_chat }}' );
    //$('.msg_history, .inbox_chat').perfectScrollbar();
    if ( $active_chat == '' ) $('.mesgs').hide();
});

function seleccionar_chat( chat_id ) {
    $('.msg_history').hide();
    $('.chat_list').removeClass('active_chat');
    $('.chat_list.chat_id-' + chat_id).addClass('active_chat');
    $('#chat_id-' + chat_id).show();
    
    // Buttons
    $('.input_msg_buttons').hide();
    $('#chat_btns_id-' + chat_id).show();
    // end Buttons

    $('#chat_id-' + chat_id).animate({ scrollTop: $('#chat_id-' + chat_id).prop("scrollHeight")}, 0);
    $active_chat = chat_id;
    var char_id_noencode = $('#chat_id-' + chat_id).data('chat_id');

    // Mensaje visto
    $.ajax({
        url: '{{ route($route . '.visto') }}',
        method: 'POST',
        data: {'_token': '{{csrf_token()}}', 'chat_id': char_id_noencode},
        success: function(data) {
            $('.chat_list.chat_id-' + chat_id + ' .chat_ib.nuevo_mensaje').removeClass('nuevo_mensaje');
        }
    });
}

function date_to_str(d) {
    var d_aux = d.getDate();
    var m_aux = d.getMonth() + 1;
    //return d.getFullYear() + '-' + (m_aux > 9 ? m_aux : '0' + m_aux) + '-' + (d_aux > 9 ? d_aux : '0' + d_aux);
    return (d_aux > 9 ? d_aux : '0' + d_aux) + '/' + (m_aux > 9 ? m_aux : '0' + m_aux) + '/' + d.getFullYear();
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function enviar_mensaje(mensaje) {
    var chat_id = $active_chat;
    var chat_type = $('#chat_id-' + chat_id).data('chat_type');
    var chat_id_noencode = $('#chat_id-' + chat_id).data('chat_id');
    
    $.ajax({
        url: '{{ route($route . '.store') }}',
        method: 'POST',
        data: {'_token': '{{csrf_token()}}', 'mensaje': mensaje, 'chat_id': chat_id_noencode, 'type': chat_type},
        success: function(data) {
            if ( data == 'KO' ) alert('Error al enviar el missatge.')
            var ahora = new Date();
            var hora = ahora.toLocaleString('es-ES', { hour: '2-digit', minute: '2-digit', hour12: false });
            var fecha = date_to_str(ahora);
            $('#chat_id-' + chat_id).append(`
                <div class="outgoing_msg">
                    <div class="sent_msg">
                    <p>` + mensaje.replace(/\r?\n/g, '<br />') + `</p>
                        <span class="time_date">` + hora + ` · ` + fecha + `</span>
                    </div>
                </div>
            `);
            $('#chat_id-' + chat_id).animate({ scrollTop: $('#chat_id-' + chat_id).prop("scrollHeight")}, 0);
            $('#mensaje-box').val('');
        }
    });
}

$('#enviar_missatge_btn').click(function(){
    var mensaje = $('#mensaje-box').val();
    enviar_mensaje(mensaje);
});

$(document).on('click', '.input_msg_buttons button', function(){
    var text = $(this).data('text');
    var url = $(this).data('href');

    var mensaje = text;

    if (typeof url !== 'undefined') {
        mensaje = `<a target="_blank" href="${url}">${text}</a>`;
    }

    enviar_mensaje(mensaje);

    var action = $(this).data('action');

    if (typeof action !== 'undefined') {
        eval(action);
    }
});
</script>
@endsection