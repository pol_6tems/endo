<div class="table-roles">
    <div class="table-header">
        <div style="flex: 1">
            Chats
            @if ($item->name == 'admin')
                (En cas de ser ADMIN els canvis no tindran cap efecte)
            @endif
        </div>
        <ul>
            <li></li>
            <li>{{ ucfirst($item->name) }}</li>
        </ul>
    </div>
    <div class="table-content">
        @foreach($params['chats'] as $chat)
            <div class="table-custom_post custom-post">
                <h6>
                    <span>{{ $chat->name }}</span>
                    <div class="togglebutton">
                        <label>
                            <input type="checkbox" data-url="{{ route('chats.change-permission', ['id' => $chat->id, 'role_id' => $item->id]) }}" {{ $chat->permissions && isset($chat->permissions[$item->name]) && isset($chat->permissions[$item->name]) ? 'checked' : '' }} class="permisos js-dynamic-permission">
                            <span class="toggle"></span>
                        </label>
                    </div>
                </h6>
            </div>
        @endforeach
    </div>
</div>