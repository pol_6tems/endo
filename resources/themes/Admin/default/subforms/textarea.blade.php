@php ( $is_required = !empty($params['required']) && $params['required'] ? 'required' : '' )

@if ( $is_create )
    @php ( $value_old = old($params['value']) )
@else
    @php ( $field = $params['value'] )
    @php ( $value = $item->{$field} )
    @php ( $value_old = old($params['value']) )
@endif

@php ( $input_name = $params['value'] )
@if ( isset($lang) && !empty($lang) )
    @php ( $input_name = $lang . "[" . $params['value'] . "]" )
    @if ( !$is_create )
        @php ( $field = $params['value'] )
        @php ( $value = $item->translate($lang)->{$field} )
    @endif
    @if ( $is_required )
        @php ( $is_required = $lang == \App::getLocale() ? 'required' : '' )
    @endif
@endif

<div class="flex-row">
    <label class="ml-4 col-md-12 mt-4">{{ $params['title'] }}</label>
</div>
<div class="flex-row">
    <div class="ml-2 col-md-12">
        <div class="form-group has-default">
            <textarea {{ isset($params['rows']) ? 'rows=' . $params['rows'] : '' }} name="{{ $input_name }}" class="form-control {{ isset($params['class']) ? $params['class'] : '' }}" {{ $is_required }} tabindex={{ $key + 1 }}>{{ $value or $value_old }}</textarea>
        </div>
    </div>
</div>