@if ( $is_create )
    @php ( $value = $params['content'] )
@else
    @php ( $field = $params['value'] )
    @php ( $value = $item->{$field} )
@endif

<input name="{{ $params['value'] }}" type="hidden" value="{{ $value }}">