<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">menu</i>
                </div>
                <h4 class="card-title">
                    @Lang('Submenus')
                    <a href="{{ route('admin.menus.subs.create', $item->id) }}" class="btn btn-primary btn-sm">@Lang('Add new')</a>
                </h4>
            </div>
            <div class="card-body">
                <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                </div>
                <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th width="10%">@Lang('Order')</th>
                                <th>@Lang('Title')</th>
                                <th>@Lang('URL')</th>
                                <th width="10%" class="disabled-sorting text-right">@Lang('Actions')</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th width="10%">@Lang('Order')</th>
                                <th>@Lang('Title')</th>
                                <th>@Lang('URL')</th>
                                <th width="10%" class="disabled-sorting text-right">@Lang('Actions')</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        @forelse($subs as $sub)
                            <tr>
                                <td class="text-right">{{ $sub->order }}</td>
                                <td>{{ __($sub->name) }}</td>
                                <td>{{ $sub->url }}</td>
                                <td class="td-actions text-right">
                                    <button onclick="window.location='{{ route('admin.menus.subs.edit', $sub->id) }}';" type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-success">
                                        <i class="material-icons">edit</i>
                                    </button>
                                    <button data-url="{{ route('admin.menus.subs.destroy', $sub->id) }}" data-name="@Lang('Are you sure to delete :name?', ['name' => $sub->name])" type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove">
                                        <i class="material-icons">close</i>
                                    </button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">@Lang('No entries found.')</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>