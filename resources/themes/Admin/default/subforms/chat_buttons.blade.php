@php
    $key += 1;
    $j = 0;
    $buttons = array();
    if ( !$is_create ) $buttons = $item->get_buttons();
@endphp

<div class="flex-row">
    <label class="col-sm-2 col-form-label">
        @Lang('Buttons')
        <button type="button" class="add-button btn btn-primary btn-sm" style="padding: 3px 10px;font-size: 10px;margin-left: 10px;">
            @Lang('Add')
        </button>
        
        @if ( !empty($params['help']) )
            <i class="sub">{!! nl2br($params['help']) !!}</i>
        @endif
    </label>


    <div class="col-sm-10 buttons">
        @forelse ( $buttons as $j => $line )
            <div class="flex-row">
                <div class="col-sm-11">
                    <input name="buttons[{{ $j }}]" type="text" class="form-control text-console" value="{{ $line }}" tabindex="{{ $key + $j }}">
                </div>
                <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                    <i class="material-icons">close</i>
                </button>
            </div>
        @empty
            <div class="flex-row">
                <div class="col-sm-11">
                    <input name="buttons[{{ $j }}]" type="text" class="form-control text-console" value="" tabindex="{{ $key }}">
                </div>
                <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                    <i class="material-icons">close</i>
                </button>
            </div>
        @endforelse
    </div>
</div>
@php( $key = $key + count($buttons) )

@section('scripts-chat-buttons')
<script>
var button_line_num = {{ $j }};
$(document).ready(function() {

    $( document ).on('click', '.remove', function() {
        var aria_describedby = '#' + $(this).attr('aria-describedby');
        $(this).closest('.flex-row').remove();
        $(aria_describedby).remove();
    });

    $( '.add-button' ).on('click', function() {
        console.log('add');
        button_line_num += 1;

        var html = `
        <div class="flex-row">
            <div class="col-sm-11">
                <input name="buttons[${button_line_num}]" type="text" class="form-control text-console" value="" tabindex="{{ $key }}">
            </div>
            <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                <i class="material-icons">close</i>
            </button>
        </div>
        `;
        $('.buttons').append(html);
    });
});
</script>
@endsection