<div class="row">
    <div class="col-sm-3 col-form-label">
        <div class="togglebutton">
            <label>
                <input name="has_url" type="checkbox" onclick="show_url_form();" tabindex=4 @if( !$is_create && $item->has_url) checked @endif>
                <span class="toggle"></span>
                @Lang('Has link?')
            </label>
        </div>
    </div>
</div>

<div class="te_enllac">
    <div class="row">
        <label class="col-sm-2 col-form-label">@Lang('Type')</label>
        <div class="col-lg-5 col-md-6 col-sm-3">
            <div class="form-group">
                <select class="selectpicker" data-size="7" name="url_type" data-style="btn btn-primary btn-round" title="@Lang('Type')" tabindex=5>
                    <option value="route" {{ !$is_create && $item->url_type == 'route' ? 'selected' : '' }}>ROUTE</option>
                    <option value="url" {{ !$is_create && $item->url_type == 'url' ? 'selected' : '' }}>URL</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">@Lang('Url')</label>
        <div class="col-sm-10">
            <div class="form-group">
                <input type="text" class="form-control" id="url" name="url" value="{{ !$is_create ? $item->url : old('url') }}" tabindex=6>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">@Lang('Controller')</label>
        <div class="col-sm-10">
            <div class="form-group">
                <input type="text" class="form-control" id="controller" name="controller" value="{{ !$is_create ? $item->controller : old('controller') }}" tabindex=7>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2 col-form-label">@Lang('Parameters')</label>
        <div class="col-sm-10">
            <div class="form-group">
                <input type="text" class="form-control" id="parameters" name="parameters" value="{{ !$is_create ? $item->parameters : old('parameters') }}" tabindex=8>
            </div>
        </div>
    </div>
</div>

<style>
    .te_enllac { display: none; }
</style>

@section('scripts2')
<script>
    function show_url_form() {
        $('.te_enllac').toggle("slow");
    }
    $(document).ready(function() {
        @if ( !$is_create && $item->has_url ) show_url_form(); @endif
    });
</script>
@endsection