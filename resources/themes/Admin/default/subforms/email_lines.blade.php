@php
    $key += 1;
    $j = 0;
    $lines = array();
    if ( !$is_create ) $lines = $item->get_lines();
@endphp

<div class="flex-row">
    <label class="col-sm-2 col-form-label">
        @Lang('Lines')
        <button type="button" class="add-line btn btn-primary btn-sm" style="padding: 3px 10px;font-size: 10px;margin-left: 10px;">
            @Lang('Add')
        </button>
        
        @if ( !empty($params['help']) )
            <i class="sub">{!! nl2br($params['help']) !!}</i>
        @endif
    </label>


    <div id="sortable" class="col-sm-10 lines">
        @forelse ( $lines as $j => $line )
            <div class="flex-row">
                <label class="col-sm-3 col-form-label">
                    <div class="togglebutton">
                        <label>
                            <input type="checkbox" name="lines[{{ $j }}][is_action]" {{ !empty($line['is_action']) ? 'checked' : '' }}>
                            @Lang('Line')
                            <span class="toggle" style="margin-left: 10px; margin-right: 5px;"></span>
                            @Lang('Action')
                        </label>
                    </div>
                </label>
                <div class="col-sm-8">
                    <input name="lines[{{ $j }}][line]" type="text" class="form-control text-console" value="{{ $line['line'] }}" tabindex="{{ $key + $j }}">
                </div>
                <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                    <i class="material-icons">close</i>
                </button>
            </div>
        @empty
            <div class="flex-row">
                <label class="col-sm-3 col-form-label">
                    <div class="togglebutton">
                        <label>
                            <input type="checkbox" name="lines[{{ $j }}][is_action]">
                            @Lang('Line')
                            <span class="toggle" style="margin-left: 10px; margin-right: 5px;"></span>
                            @Lang('Action')
                        </label>
                    </div>
                </label>
                <div class="col-sm-8">
                    <input name="lines[{{ $j }}][line]" type="text" class="form-control text-console" value="" tabindex="{{ $key }}">
                </div>
                <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                    <i class="material-icons">close</i>
                </button>
            </div>
        @endforelse
    </div>
</div>
@php( $key = $key + count($lines) )

@section('scripts2')
<script>
var email_line_num = {{ $j }};
$(document).ready(function() {
    $("#sortable").sortable({
        stop: function(event, ui) {
            $(this).children().each(function(idx, el) {
                $(el).find('input').map(function(i, elem) {
                    var name = $(elem).attr('name');
                    name = name.split('[');
                    name[1] = idx + ']';
                    $(elem).attr('name', name.join('['));
                });
            });
        }
    });
    $( document ).on('click', '.remove', function() {
        var aria_describedby = '#' + $(this).attr('aria-describedby');
        $(this).closest('.flex-row').remove();
        $(aria_describedby).remove();
    });

    $( '.add-line' ).on('click', function() {
        email_line_num += 1;
        
        var html = `
        <div class="flex-row">
            <label class="col-sm-3 col-form-label">
                <div class="togglebutton">
                    <label>
                        <input type="checkbox" name="lines[${email_line_num}][is_action]">
                        @Lang('Line')
                        <span class="toggle" style="margin-left: 10px; margin-right: 5px;"></span>
                        @Lang('Action')
                    </label>
                </div>
            </label>
            <div class="col-sm-8">
                <input name="lines[${email_line_num}][line]" type="text" class="form-control text-console" value="" tabindex="{{ $key }}">
            </div>
            <button type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                <i class="material-icons">close</i>
            </button>
        </div>
        `;
        $('.lines').append(html);
    });
});
</script>
@endsection