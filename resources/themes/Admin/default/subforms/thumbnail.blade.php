@php ( $is_required = !empty($params['required']) && $params['required'] ? 'required' : '' )

@php ( $has_thumbnail = false )
@php ( $img_src = '' )

@if ( $is_create )
    @php ( $value_old = old($params['value']) )
@else
    @php ( $field = $params['value'] )
    @php ( $value = $item->{$field} )
    @php ( $value_old = old($params['value']) )
    @if ( method_exists($item, 'has_thumbnail') )
        @if ( $item->has_thumbnail() )
            @if ($item->is_image())
                @php ( $img_src = $item->get_thumbnail_url('medium') )
            @else
                @php ( $img_src = asset($_admin.'img/icons/doc.png') )
            @endif
            @php ( $has_thumbnail = true )
        @endif
    @elseif ( !empty($value) )
        @php ( $img_src = "data:image/jpeg;base64," . base64_encode($value) )
        @php ( $has_thumbnail = true )
    @endif
@endif

@php ( $input_name = $params['value'] )
@if ( isset($lang) && !empty($lang) )
    @php ( $input_name = $lang . "[" . $params['value'] . "]" )
    @if ( !$is_create )
        @php ( $field = $params['value'] )
        @php ( $value = $item->translate($lang)->{$field} )
    @endif
    @if ( $is_required )
        @php ( $is_required = $lang == \App::getLocale() ? 'required' : '' )
    @endif
@endif

<div class="row">
    <label class="col-sm-1 col-form-label"></label>
    <div class="col-sm-10">
        <div class="form-group">
            <h4 class="title">{{ $params['title'] }}</h4>
            <div class="fileinput {{ $has_thumbnail ? 'fileinput-exists' : 'fileinput-new' }} text-center" data-provides="fileinput">
                <div class="fileinput-new thumbnail">
                    <img src="{{ asset('images/image_placeholder.jpg') }}">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="">
                    <img src="{{ $img_src }}" >
                </div>  
                <div>
                    <span class="btn btn-rose btn-round btn-file">
                    <span class="fileinput-new">@Lang('Select image')</span>
                    <span class="fileinput-exists">@Lang('Change')</span>
                    <input type="hidden"><input type="file" name="{{ $input_name }}" required>
                    <div class="ripple-container"></div></span>
                    <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                        <i class="fa fa-times"></i> @Lang('Delete')
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
window.onload = function () {
    $('.fileinput[data-provides=fileinput] .fileinput-new').click(function(){
        var input = $(this).parent().find('input[type=file]');
        input.on('change', function () {
            $('.modal .close').click();
        });

        document.body.onfocus = function(){
            $('.modal .close').click();
        };
        input.click();
    });
}
</script>