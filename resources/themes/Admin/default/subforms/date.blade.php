@php ( $date_format = !empty($params['date_format']) ? $params['date_format'] : 'd/m/Y' )

@if ( $is_create )
    @php ( $value_old = old($params['value']) )
@else
    @php ( $field = $params['value'] )
    @php ( $value = $item->{$field} )
    @php ( $value_old = old($params['value']) )
    @if ( !empty($value) )
        @php ( $value = Carbon\Carbon::parse($value)->format( $date_format ) )
    @endif
@endif

@php( $is_disabled = isset($params['disabled']) && $params['disabled'] ? 'disabled' : '' )


<div class="flex-row">
    <label class="col-sm-12 col-form-label">{{ $params['title'] }}</label>
    <div class="col-sm-12">
        <div class="form-group">
            <input name="{{ $params['value'] }}" type="text" class="form-control datepicker" value="{{ $value or $value_old }}" {{ !empty($params['required']) && $params['required'] ? 'required' : '' }} tabindex={{ $key }} {{ $is_disabled }}>
            @if ( !empty($params['help']) )
                <span class="bmd-help">{!! $params['help'] !!}</span>
            @endif
        </div>
    </div>
</div>