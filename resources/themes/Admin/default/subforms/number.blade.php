@if ( $is_create )
    @php ( $value_old = old($params['value']) )
@else
    @php ( $field = $params['value'] )
    @php ( $value = $item->{$field} )
    @php ( $value_old = old($params['value']) )
@endif

<div class="flex-row">
    <label class="col-sm-12 col-form-label">{{ $params['title'] }}</label>
    <div class="col-sm-12">
        <div class="form-group">
            <input name="{{ $params['value'] }}" type="number" class="form-control" value="{{ $value or $value_old }}" {{ !empty($params['required']) && $params['required'] ? 'required' : '' }} tabindex={{ $key }}>
            @if ( !empty($params['help']) )
                <span class="bmd-help">{!! $params['help'] !!}</span>
            @endif
        </div>
    </div>
</div>