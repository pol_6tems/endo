@php ( $is_required = !empty($params['required']) && $params['required'] ? 'required' : '' )

@if ( $is_create )
    @php ( $value_old = old($params['value']) )
@else
    @php ( $field = $params['value'] )
    @php ( $value = $item->{$field} )
    @php ( $value_old = old($params['value']) )
@endif

@php ( $input_name = $params['value'] )
@if ( isset($lang) && !empty($lang) )
    @php ( $input_name = $lang . "[" . $params['value'] . "]" )
    @if ( !$is_create )
        @php ( $field = $params['value'] )
        @if ( method_exists($item, 'translate') )
            @php ( $value = ($item->translate($lang)) ? $item->translate($lang)->{$field} : '' )
        @else
            @php ( $value = ( $item ) ? $item->{$field} : '' )
        @endif
    @endif
    @if ( $is_required )
        @php ( $is_required = $lang == \App::getLocale() ? 'required' : '' )
    @endif
@endif

@php( $is_disabled = isset($params['disabled']) && $params['disabled'] ? 'disabled' : '' )

<div class="flex-row">
    <label class="col-sm-12 col-form-label">{{ $params['title'] }}</label>
    <div class="col-sm-12">
        <div class="form-group">
            <input id="{{ $input_name }}" name="{{ $input_name }}" type="password" class="form-control" value="{{ $value_old }}" {{ $is_required }} tabindex={{ $key + 1 }} {{ $is_disabled }}>
            @if ( !empty($params['help']) )
                <span class="bmd-help">{!! $params['help'] !!}</span>
            @endif
        </div>
    </div>
</div>