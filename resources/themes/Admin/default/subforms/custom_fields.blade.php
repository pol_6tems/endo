<div class="table">
    <ul class="thead">
        <li class="li-field-order">@Lang('Order')</li>
        <li class="li-field-label">@Lang('Title')</li>
        <li class="li-field-name">@Lang('Name')</li>
        <li class="li-field-type">@Lang('Type')</li>
        <li class="li-field-params"></li>
    </ul>
    <ul class="sortable tbody sortable-parent">
        @php($k = 0)
        @php($activeModules = \App\Module::where('active', '1')->get())
        @foreach ( $item->fields as $j => $f )
        <div class="cf-item" data-order="{{ $f->order }}">
            <div class="cf-row" aria-expanded="false">
                <input class="id" type="hidden" name="fields[{{ $f->id }}][field_id]" value="{{ $f->id }}">
                <input class="order" type="hidden" name="fields[{{ $f->id }}][field_order]" value="{{ $f->order }}">
                <li class="li-field-order"><span>{{ $j }}</span></li>
                <li class="li-field-label"><input type="text" class="form-control" name="fields[{{ $f->id }}][field_title]" placeholder="@Lang('Title')" value="{{ $f->title }}"></li>
                <li class="li-field-name"><input type="text" class="form-control" name="fields[{{ $f->id }}][field_name]" placeholder="@Lang('Name')"  value="{{ $f->name }}"></li>
                <li class="li-field-type">
                    <!-- Ajax per obtenir els tipus de dades?  -->
                    <select class="selectpicker" data-size="7" name="fields[{{ $f->id }}][field_type]" data-style="btn btn-primary" title="@Lang('Type')" data-live-search="true">
                        <optgroup label="Core">
                            <option value="text" {{ $f->type == 'text' ? 'selected' : '' }}>@Lang('Text')</option>
                            <option value="textarea" {{ $f->type == 'textarea' ? 'selected' : '' }}>@Lang('Textarea')</option>
                            <option value="date" {{ $f->type == 'date' ? 'selected' : '' }}>@Lang('Date')</option>
                            <option value="number" {{ $f->type == 'number' ? 'selected' : '' }}>@Lang('Number')</option>
                            <option value="media" {{ $f->type == 'media' ? 'selected' : '' }}>@Lang('Media')</option>
                            <option value="form" {{ $f->type == 'form' ? 'selected' : '' }}>@Lang('Form')</option>
                            {{-- <option value="multiple_switch" {{ $f->type == 'multiple_switch' ? 'selected' : '' }}>@Lang('Multiple Selection')</option> --}}
                            <option value="gallery" {{ $f->type == 'gallery' ? 'selected' : '' }}>@Lang('Gallery')</option>
                            <option value="selection" {{ $f->type == 'selection' ? 'selected' : '' }}>@Lang('Selection')</option>
                            <option value="gmaps" {{ $f->type == 'gmaps' ? 'selected' : '' }}>@Lang('Google Maps')</option>
                            <option value="boolean" {{ $f->type == 'boolean' ? 'selected' : '' }}>@Lang('Boolean')</option>
                            <option value="repeater" {{ $f->type == 'repeater' ? 'selected' : '' }}>@Lang('Repeater')</option>
                        </optgroup>
                        @foreach($activeModules as $modul)
                        <optgroup label="{{ $modul->name }}">
                            @foreach(rglob( app_path('Modules/'.$modul->name.'/resources/views/custom_fields/*.php')) as $custom)
                                @php( $aux = explode('.', basename($custom))[0] )
                                <option value="{{ $modul->name . '.' . $aux }}" {{ $f->type == $modul->name . '.' . $aux ? 'selected' : '' }}>{{ $aux }}</option>
                            @endforeach
                        </optgroup>
                        @endforeach
                    </select>
                </li>
                <li class="li-field-params" data-toggle="collapse" data-target="#{{ $f->id }}" aria-expanded="false" aria-controls="{{ $f->id }}"><i class="material-icons">keyboard_arrow_down</i></li>
            </div>

            <div id="{{ $f->id }}" class="params-content collapse">
                {{-- Renderitzem els parametres --}}

                @if ( $f->type == 'repeater' )
                    @includeIf('Admin::default.custom_fields_params.repeater', ['item' => $item, 'k' => $k, 'f' => $f])
                @endif

                @foreach($f->getParams() as $param)
                    @if (!is_array($param) && class_exists($param))
                        @php($parametre = new $param($f))
                        @php($parametre->setName('fields['.$f->id.'][params]['. $parametre->getId() .']'))

                        {!! $parametre->getField() !!}
                    @endif
                @endforeach
                <div class="row2">
                    <label class="col-sm-2 col-form-label"></label>
                    <div class="col-lg-8">
                        <button onclick="remove_row(this);" type="button" rel="tooltip" class="btn btn-sm btn-primary">
                            @Lang('Delete')
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @php($k += count($f->fields) + 1)
        @endforeach
    </ul>
    <div class="hidden" style="display:none;">
        
    </div>
    {{--
        </div>
    </div> --}}
    <div id="status"></div>
</div>

@section('scripts2')
<script src="{{ asset($_admin.'js/speakingurl.min.js') }}"></script>
<script src="{{ asset($_admin.'js/slugify.min.js') }}"></script>
@php ($l = 0)
@foreach ( $item->fields as $l => $f )
<script>
    $('input[name="fields[{{$l}}][field_name]"]').slugify('input[name="fields[{{$l}}][field_title]"]'); // Type as you slug
</script>
@endforeach
<script>
(function($) {
    $.fn.closest_descendent = function(filter) {
        var $found = $(),
            $currentSet = this; // Current place
        while ($currentSet.length) {
            $found = $currentSet.filter(filter);
            if ($found.length) break;  // At least one match: break loop
            // Get all children of the current set
            $currentSet = $currentSet.children();
        }
        return $found.first(); // Return first match of the collection
    }    
})(jQuery);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

delete($.ui.accordion.prototype._keydown);
function updateFieldOrder(element, order) {
    $(element).attr('id', 'field-'+order);
    $(element).attr('data-order', order);
    $(element).find('.cf-row .li-field-order span').first().text(order);
    $(element).find('.cf-row .order').val(order);
}

function recalcularGrid(parent) {
    parent.find('.sortable').sortable({
        axis: "y",
        opacity: 0.5,
        update: function( event, ui ) {
            $(event.target).children().each(function(idx, element) {
                updateFieldOrder(element, idx);
            });
        }
    });
}

$(document).ready(function() {
    recalcularGrid($('.table').first());
});

@php($max = 0)
@foreach($item->fields as $field)
    @if ($field->id > $max) @php( $max = $field->id ) @endif
    @foreach($field->fields as $field2)
        @if ($field2->id > $max) @php( $max = $field2->id ) @endif
    @endforeach
@endforeach

var num_field = {{ $max + 1 }};
$('.add_field').click(function() {
    var $parent = $(this).closest('.repeater-param');
    if ($parent.length == 0) $parent = $(this).closest('.card');

    var is_child = $(this).data('is_child');
    var parent_id = $(this).data('parent_id');

    var sortable = '.sortable-parent';
    var field_name = 'fields['+num_field+']';
    var parent_id_input = '';

    if ( is_child ) {
        var sortable = '.sortable-child';
        var parent_id_input = '<input type="hidden" name="'+field_name+'[parent_id]" value="'+parent_id+'">';
    }

    var ordre = $parent.find(sortable + '>.cf-item').length;

    var $element = $(`
        <div class="cf-item" data-order="${ordre}">
            <div class="cf-row">
                ${parent_id_input}
                <input class="id" type="hidden" name="${field_name}[field_id]" value="">
                <input class="order" type="hidden" name="${field_name}[field_order]" value="${ordre}">
                <li class="li-field-order"><span>${ordre}</span></li>
                <li class="li-field-label"><input id="field_title" type="text" class="form-control" name="${field_name}[field_title]" value=""></li>
                <li class="li-field-name"><input id="field_name" type="text" class="form-control" name="${field_name}[field_name]"  value=""></li>
                <li class="li-field-type">
                    <!-- Ajax per obtenir els tipus de dades?  -->
                    <select class="selectpicker" data-size="7" name="${field_name}[field_type]" data-style="btn btn-primary" title="@Lang('Type')" data-live-search="true">
                        <optgroup label="Core">
                            <option value="text" selected>@Lang('Text')</option>
                            <option value="textarea">@Lang('Textarea')</option>
                            <option value="date">@Lang('Date')</option>
                            <option value="number">@Lang('Number')</option>
                            <option value="media">@Lang('Media')</option>
                            <option value="form">@Lang('Form')</option>
                            {{-- <option value="multiple_switch">@Lang('Multiple Selection')</option> --}}
                            <option value="gallery">@Lang('Gallery')</option>
                            <option value="selection">@Lang('Selection')</option>
                            <option value="gmaps">@Lang('Google Maps')</option>
                            <option value="boolean">@Lang('Boolean')</option>
                            <option value="repeater">@Lang('Repeater')</option>
                        </optgroup>
                        <optgroup label="Modules">
                        @foreach($activeModules as $modul)
                            @foreach(rglob( app_path('Modules/'.$modul->name.'/resources/views/subforms/*.php')) as $custom)
                                @php( $aux = explode('.', basename($custom))[0] )
                                <option value="{{ $aux }}">{{ $aux }}</option>
                            @endforeach
                        @endforeach
                        </optgroup>
                    </select>
                </li>
                <li class="li-field-params"><i class="material-icons">keyboard_arrow_down</i></li>
            </div>
            <div class="params-content">
                <div class="row2">
                    <div class="ml-auto mr-3">
                        <button onclick="remove_row(this);" type="button" rel="tooltip" class="btn btn-sm btn-primary">
                            @Lang('Delete')
                        </button>
                    </div>
                </div>
            </div>
        </div>
    `);

    if ( is_child ) {
        $parent.find('.sortable-child').append($element);
    } else {
        $parent.find('.sortable-parent').append($element);
    }

    $parent.find('input[name="'+field_name+'[field_name]"]').slugify('input[name="'+field_name+'[field_title]"]'); // Type as you slug
    recalcularGrid( $parent );
    $element.find('.selectpicker').selectpicker('refresh');
    num_field++;
});

function remove_row(e) {
    var is_child = $(e).closest('.sortable').data('is_child');
    var sortable = is_child ? '.sortable-child' : '.sortable-parent';

    $(e).closest(sortable + ' .cf-item').slideUp(function(){
        $(e).closest(sortable + ' .cf-item').remove();
        $(sortable + " .cf-item").each(function(idx, element) {
            updateFieldOrder(element, idx);
        });
    });
}

$('input.field_title').blur(function(){
    var field_name = $(this).closest('.field').find('.field_name');
    if ( field_name.val() == '' ) {
        console.log(2);
        field_name.val( slug($(this).val(), '-') );
    }
});

function slug(title, separator) {
	if(typeof separator == 'undefined') separator = '-';

	// Convert all dashes/underscores into separator
	var flip = separator == '-' ? '_' : '-';
	title = title.replace(flip, separator);

	// Remove all characters that are not the separator, letters, numbers, or whitespace.
	title = title.toLowerCase()
			.replace(new RegExp('[^a-z0-9' + separator + '\\s]', 'g'), '');

	// Replace all separator characters and whitespace by a single separator
	title = title.replace(new RegExp('[' + separator + '\\s]+', 'g'), separator);
	return title.replace(new RegExp('^[' + separator + '\\s]+|[' + separator + '\\s]+$', 'g'),'');
}
</script>
@endsection

@section('cf')
    @foreach ( $item->fields as $k => $f )
        <!-- Scripts Field [{{ $f->id }}] -->
        @yield("field$f->id")
        
        @foreach($f->getParams() as $param)
            @if (is_array($param))
                @foreach($param as $p)
                    @php($paramName = explode("\\", $p))
                    <!-- Scripts Field Params [{{ $f->id }}] [{{ $paramName[count($paramName) - 1] }}] -->
                    @yield("param" . $paramName[count($paramName) - 1])
                @endforeach
            @else
                @php($paramName = explode("\\", $param))
                <!-- Scripts Field Params [{{ $f->id }}] [{{ $paramName[count($paramName) - 1] }}] -->
                @yield("param" . $paramName[count($paramName) - 1])
            @endif
        @endforeach

    @endforeach
@endsection

@section('styles')
<style>
#field_add_form {
    display: none;
    -webkit-transition: all .5s linear;
    -moz-transition: all .5s linear;
    -o-transition: all .5s linear;
    transition: all .5s linear;
}
</style>
@endsection