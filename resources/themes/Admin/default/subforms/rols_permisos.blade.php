<div class="row">
    <label class="col-sm-2 col-form-label">@Lang('Permissions')</label>
    <div class="col-lg-5 col-md-6 col-sm-3">
        <div class="form-group">
            <select class="selectpicker grups col-12" data-size="7" data-style="btn btn-primary" data-live-search="true" title="@Lang('Permissions')" tabindex={{ $key }}>
            @foreach ($params['controllers'] as $grup => $controllers)
                <option value="{{ str_slug($grup, '-')  }}" {{ $grup == 'General' ? 'selected' : '' }}>{{ $grup }}</option>
            @endforeach
            </select>
        </div>
    </div>
</div>

@foreach ($params['controllers'] as $grup => $controllers)
    <div id="grup-{{ str_slug($grup, '-') }}" class="row grups">
        <label class="col-sm-2 col-form-label">@Lang('Controllers')</label>
        <div class="col-lg-5 col-md-6 col-sm-3">
            <div class="form-group">
                <select class="selectpicker controllers col-12" data-size="7" data-style="btn btn-primary" title="@Lang('Controllers')" data-live-search="true" tabindex={{ $key + 1 }}>
                    @foreach ( $controllers as $controller_name => $methods )
                        <option value="{{ str_slug($controller_name, '-') }}">{{ $controller_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-3 pt-3">
            <div class="togglebutton">
                <label>
                    <input type="checkbox" id="{{ str_slug($grup, '-') }}" class="all-permisos-grup">
                    <span class="toggle"></span>
                    <strong>@Lang('All Controllers')</strong>
                </label>
            </div>
        </div>
    </div>

    @foreach ( $controllers as $controller_name => $methods )
    <div id="controller-{{ str_slug($controller_name, '-') }}" class="row controllers">
        <div class="col-12">
            <h4 class="card-title"><strong>{{ $controller_name }}</strong>
                <div class="togglebutton">
                    <label>
                        <input type="checkbox" id="{{ str_slug($controller_name, '-') }}-{{ str_slug($grup, '-') }}" data-controller="{{ str_slug($controller_name, '-') }}" data-group="{{ str_slug($grup, '-') }}" class="single-permisos-grup">
                        <span class="toggle"></span>
                        <strong>@Lang('All')</strong>
                    </label>
                </div>
            </h4>
            @foreach ( $methods as $key => $method )
            @php ( $permiso_name = "permisos[".str_slug($grup, '-')."][".str_slug($controller_name, '-')."][".$method."]")
            <div class="col-sm-3">
                <div class="togglebutton">
                    <label>
                        <input type="checkbox" name="{{ $permiso_name }}" data-grup="{{ str_slug($grup, '-') }}" data-controller="{{ str_slug($controller_name, '-') }}" class="grup-{{ str_slug($grup, '-') }} controller-{{ str_slug($controller_name, '-') }}"
                        @if ( !empty($item->get_permisos()[ str_slug($grup, '-') ][ str_slug($controller_name, '-') ][ $method ]) && $item->get_permisos()[ str_slug($grup, '-') ][ str_slug($controller_name, '-') ][ $method ] )
                            checked
                        @endif
                        >
                        <span class="toggle"></span>
                        {{ $method }}
                    </label>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endforeach
@endforeach

@section('scripts2')
<script>
$('.all-permisos-grup').on('change', function(e) {
    $parent = $(this).closest('.custom-post');
    $parent.find('.form-check-input').attr('checked', this.checked);
});


$(document).ready(function() {
    $('.single-permisos-grup').each(function(i, el) {
        if ($('.role-' + $(el).data('role') + ':checked').length == $('.role-' + $(el).data('role')).length) {
            $(el).attr('checked', true);
        }
    });

    $('.all-permisos-grup').each(function(i, el) {
        var custom_post = $(this).closest('.custom-post');
        if (custom_post.find('.form-check-input').length == custom_post.find('.form-check-input:checked').length) {
            $(el).attr('checked', true);
        }
    });

    var $id = $('.selectpicker.grups').val();
    $('.row.grups').slideUp();
    $('#grup-' + $id).slideDown();

    /* COMPROVAR TOTS ELS CHECKBOX PER SI TOTS ESTAN CHECKED */
    $('input[type=checkbox].all-permisos-grup').each(function() {
        var grup_id = this.id;
        if (grup_id) {
            if (check_all_methods_check_by_grup(grup_id, true)) {
                $('input[type=checkbox]#' + grup_id).attr('checked', true);
            } else {
                $('input[type=checkbox]#' + grup_id).attr('checked', false);
            }
        }
    });
    /* end COMPROVAR TOTS ELS CHECKBOX PER SI TOTS ESTAN CHECKED */

    $('.selectpicker.grups').on('change', function() {
        var $id = $(this).val();
        $('.row.grups').slideUp();
        $('#grup-' + $id).slideDown();

        var $id_controller = $('#grup-' + $id).find('.selectpicker.controllers').val();
        $('.row.controllers').slideUp();
        $('#controller-' + $id_controller).slideDown();
    });

    $('.selectpicker.controllers').on('change', function() {
        var $id = $(this).val();
        $('.row.controllers').slideUp();
        $('#controller-' + $id).slideDown();
    });

    $('.all-permisos-grup').on('change', function() {
        var id = this.id;
        $('input[type=checkbox].grup-' + id).attr('checked', this.checked);
    });

    $('.single-permisos-grup').on('change', function() {
        var controller = $(this).data('controller');
        var group = $(this).data('group');

        $(`.grup-${group}.controller-${controller}`).attr('checked', this.checked);
    });

    $( ".row.controllers input[type=checkbox]" ).on('change', function() {
        var grup_id = $(this).data('grup');
        if ( check_all_methods_check_by_grup(grup_id, true) ) {
            $('input[type=checkbox]#' + grup_id).attr('checked', true);
        } else {
            $('input[type=checkbox]#' + grup_id).attr('checked', false);
        }
    });

    function check_all_methods_check_by_grup(grup_id, is_checked) {
        var result = true;
        $('input[type=checkbox].grup-' + grup_id).each(function() {
            if ( result ) result = this.checked == is_checked;
        });
        return result;
    }
});
</script>
@endsection