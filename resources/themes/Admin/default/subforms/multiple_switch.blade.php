@php ( $is_required = !empty($params['required']) && $params['required'] ? 'required' : '' )
@php ( $is_multiple = !empty($params['multiple']) && $params['multiple'] ? 'multiple' : '' )

@if ( $is_create )
    @php ( $value_old = old($params['value']) )
@else
    @php ( $field = $params['value'] )
    @php ( $value = $item->{$field} )
@endif

@php ( $source = !empty($params['datasource']) ? 'datasource' : 'choices' )
@php ( $can_search = !empty($params['sel_search']) && $params['sel_search'] ? 'true' : 'false' )

@php ( $input_name = $params['value'] )
@if ( isset($lang) && !empty($lang) )
    @php ( $input_name = $lang . "[" . $params['value'] . "]" )
    @if ( !$is_create )
        @php ( $field = $params['value'] )
        @php ( $value = $item->translate($lang)->{$field} )
    @endif
    @if ( $is_required )
        @php ( $is_required = $lang == \App::getLocale() ? 'required' : '' )
    @endif
@endif

@if ( isset($value) )
    @php ( $value = explode('#', $value) )
@endif

<div class="flex-row">
    <label class="col-sm-1 col-form-label"></label>
    <div class="col-sm-10">
        <h4 class="title">{{ $params['title'] }}</h4>
        <div class="col-lg-5 col-md-6 col-sm-3">
            <div class="form-group">

                @foreach ( $params[$source] as $k_multi => $choice )

                    @if ( isset($params['sel_translable']) && $params['sel_translable'] )
                        @php ( $choice_aux = $choice->translate( \App::getLocale() ) )
                    @else
                        @php ( $choice_aux = $choice )
                    @endif

                    <div class="togglebutton">
                        <label>
                            
                            @if ( $source == 'choices' )
                                <input type="checkbox" name="{{ $input_name }}[{{$k_multi}}]" value="{{ $choice['value'] }}" {{ isset($value) && in_array($choice_value, $value) ? 'checked' : '' }}>
                                <span class="toggle"></span>
                                {{ $choice['title'] }}
                            @else
                                @php( $sel_value = $params['sel_value'] )
                                @php ( $choice_value = $choice_aux->{$sel_value} )

                                @php( $choice_title = [ $params['sel_title'] ] )
                                @if ( is_array($params['sel_title']) )
                                    @php( $choice_title = [] )
                                    @foreach ( $params['sel_title'] as $v )
                                        @php ( $choice_title[] = strpos($v, '()') !== false ? $choice_aux->{str_replace('()','',$v)}() : $choice_aux->{$v} )
                                    @endforeach
                                @endif
                                <input type="checkbox" name="{{ $input_name }}[{{$k_multi}}]" value="{{ $choice_value }}" {{ isset($value) && in_array($choice_value, $value) ? 'checked' : '' }}>
                                <span class="toggle"></span>
                                {{ implode(' - ', $choice_title) }}
                            @endif

                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>