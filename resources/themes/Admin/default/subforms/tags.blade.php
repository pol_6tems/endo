@php
    $is_required = !empty($params['required']) && $params['required'] ? 'required' : '';
    $is_multiple = !empty($params['multiple']) && $params['multiple'] ? 'multiple' : '';

    if ( $is_create ) {
        $value_old = old($params['value']);
    } else {
        $field = $params['value'];
        $value = $item->{$field};
    }

    $source = !empty($params['datasource']) ? 'datasource' : 'choices';
    $can_search = !empty($params['sel_search']) && $params['sel_search'] ? 'true' : 'false';
    $input_name = $params['value'];

    if ( isset($lang) && !empty($lang) ) {
        $input_name = $lang . "[" . $params['value'] . "]";
        if ( !$is_create ) {
            $field = $params['value'];
            
            if ( method_exists($item, 'translate') ) {
                $value = $item->translate($lang)->{$field};
            } else {
                $value = $item->{$field};
            }
        }
        if ( $is_required ) $is_required = $lang == \App::getLocale() ? 'required' : '';
    }

    $values = '';
    $aux = array();
    foreach ( $params[$source] as $key => $choice ) {
        $values .= $choice['title'];
        if ( count($params[$source])-1 > $key) $values .= ', ';
    }
@endphp

<div class="flex-row">
    <label class="col-sm-12 col-form-label">{{ $params['title'] }}</label>
    <div class="col-md-12 col-sm-3">
        <div class="form-group">
            <input type="text" value="{{ $values }}" class="tagsinput form-control" data-color="danger">
            <br>
            <small>{{ $values }}</small>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    var tags = $('input');
    tags.tagsinput({
        itemValue: 'title',
        itemText: 'value',
        typeahead: {
            source: {{ json_encode($aux) }}
        }
    });

    {!! json_encode($params[$source]) !!}.forEach(function( el ) {
        tags.tagsinput('add', { "value": el.title , "text": el.title });
    })
});
</script>