@php( $statics = ['post' => 'Post', 'page' => 'Page'])

<div class="table-roles">
    <div class="table-header">
        <div style="flex: 1">
            @if ($item->name == 'admin')
                En cas de ser ADMIN els canvis no tindran cap efecte
            @endif
        </div>
        <ul>
            <li></li>
            <li>{{ ucfirst($item->name) }}</li>
        </ul>
    </div>
    <div class="table-content">
        @foreach($statics as $type => $title)
        @php( $permisos = (array) json_decode($item->permisos) )
        @php( $name = $item->name )
        <div class="table-custom_post custom-post" data-cp="{{ $type }}">
            <h6>
                <span>@Lang($title)</span>
                <div class="togglebutton">
                    <label>
                        <input type="hidden" name="permissions[{{ $type }}][{{ $item->name }}]" value="0">
                        <input type="checkbox" name="permissions[{{ $type }}][{{ $item->name }}]" {{ ( isset($permisos['roles']) && isset($permisos['roles']->$type->$name) && $permisos['roles']->$type->$name == '1' ) ? 'checked' : '' }} value="1" class="permisos">
                        <span class="toggle"></span>
                    </label>
                </div>
            </h6>
        </div>
        @endforeach
        @foreach($params['custom_posts'] as $cp)
        @php($permission = (array) $cp->permissions)
        @php($permission = (isset($permission[$item->name])) ? (array) $permission[$item->name] : null)
        <div class="table-custom_post custom-post" data-cp="{{ $cp->id }}">
            <h6>
                <span>{{ $cp->title }} - ({{ $cp->post_type }})</span>
                <div class="togglebutton">
                    <label>
                        <input type="checkbox" class="all-permisos-grup">
                        <span class="toggle"></span>
                    </label>
                </div>
                <a data-toggle="collapse" href="#cp_{{ $cp->id }}"><i class="material-icons">keyboard_arrow_down</i></a>
            </h6>
            <div id="cp_{{ $cp->id }}" class="collapse">
                <div class="custom-post-content">
                    @foreach(getPostsControllerMethods() as $method)
                        @if( isset($method) && substr($method, 0, 2) != '__' )
                            <ul>
                                <li>{{ $method }}</li>
                                <li>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="hidden" value="0" name="permissions[{{ $cp->id }}][{{ $item->name }}][{{ $method }}]" class="col-md-12">
                                            <input class="form-check-input" type="checkbox" value="1" name="permissions[{{ $cp->id }}][{{ $item->name }}][{{ $method }}]" {{ ((isset($permission) && isset($permission[$method]) && $permission[$method] == '1') || $item->name == 'admin') ? 'checked' : '' }} class="col-md-12 role-method role-{{ $item->id }}">
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
