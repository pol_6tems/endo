@php
    $is_required = !empty($params['required']) && $params['required'] ? 'required' : '';
    $is_multiple = !empty($params['multiple']) && $params['multiple'] ? 'multiple' : '';

    if ( $is_create ) {
        $value_old = old($params['value']);
    } else {
        $field = $params['value'];

        $explodedFields = explode('->', $field);

        $value = $item;

        foreach ($explodedFields as $explodedField) {
            $value = $value->{$explodedField};
        }

        if ($value === $item) {
            $value = null;
        }
    }

    $source = !empty($params['datasource']) ? 'datasource' : 'choices';
    $can_search = !empty($params['sel_search']) && $params['sel_search'] ? 'true' : 'false';
    $input_name = $params['value'];

    if ( isset($lang) && !empty($lang) ) {
        $input_name = $lang . "[" . $params['value'] . "]";
        if ( !$is_create ) {
            $field = $params['value'];
            
            if ( method_exists($item, 'translate') ) {
                $value = $item->translate($lang)->{$field};
            } else {
                $value = $item->{$field};
            }
        }
        if ( $is_required ) $is_required = $lang == \App::getLocale() ? 'required' : '';
    }
@endphp
<div class="flex-row">
    <label class="col-sm-12 col-form-label">{{ $params['title'] }}</label>
    <div class="col-lg-12 col-md-12 col-sm-3">
        <div class="form-group">
            <select class="selectpicker" data-size="7" name="{{ $input_name }}" data-style="btn btn-primary" title="{{ $params['title'] }}" data-live-search="{{ $can_search }}" {{ $is_required }} {{ $is_multiple }} tabindex={{ $key + 1 }}>
            @foreach ( $params[$source] as $choice )
                @if ( $source == 'choices' )
                    <option value="{{ $choice['value'] }}" {{ isset($value) && $choice['value'] == $value ? 'selected' : '' }}>{{ $choice['title'] }}</option>
                @else
                    @php
                        $choice_aux = $choice;
                        $sel_value = $params['sel_value'];
                        $choice_value = $choice->{$sel_value};
                        $choice_title = [ $params['sel_title'] ];

                        if ( is_array($params['sel_title']) ) {
                            $choice_title = [];
                            foreach ( $params['sel_title'] as $v ) {
                                $choice_title[] = strpos($v, '()') !== false ? $choice_aux->{str_replace('()','',$v)}() : $choice_aux->{$v};
                            }
                        }
                    @endphp
                    <option value="{{ $choice_value }}" {{ isset($value) && $choice_value == $value ? 'selected' : '' }}>
                        @Lang( implode(' - ', $choice_title) )
                        @if ( !isset($params['with_post_type']) || ( !empty($params['with_post_type']) && $params['with_post_type'] ) )
                            {{ !empty($choice->post_type) ? ' (' . $choice->post_type . ')' : '' }}
                        @endif
                    </option>
                @endif
            @endforeach
            </select>
        </div>
    </div>
</div>