@extends('Admin::layouts.admin')

@section('section-title')
    {{ $section_title }}
@endsection

@section('content')
<div class="row">
    <div class="col-md-{{ $width or '12' }}">
        <article class="card ">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">{{ $section_icon }}</i>
                </div>
                <h4 class="card-title">@Lang('Add new')</h4>
            </div>
            <form id="form" class="form-horizontal" action="{{ route($route . '.update', $item->id) }}" method="post" enctype="multipart/form-data">
            <div class="card-body ">
                    {{ csrf_field() }}
                    {{--<input type="hidden" name="_method" value="PUT">--}}

                    @foreach ( $rows as $key => $params )
                        @if ( empty($params['type']) )
                            @includeIf('Admin::default.subforms.text', ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item])
                        @elseif ( $params['type'] == 'subform' )
                            @includeIf($params['value'], ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item])
                        @else
                            @includeIf('Admin::default.subforms.' . $params['type'], ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item])
                        @endif
                    @endforeach

                </div>
                <div class="card-footer">
                    <div class="mr-auto">
                        <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" name="previous" value="@Lang('Tornar')" onclick="window.location.href='{{ route($route_parent . '.edit', $id_parent) }}'">
                    </div>
                    <div class="ml-auto">
                        <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="@Lang('Update')">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </article>
    </div>
</div>

@isset ( $subs )
@foreach ( $subs as $sub )
<div class="row">
    <div class="col-md-12">
        <article class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">{{ $sub['section_icon'] }}</i>
                </div>
                <h4 class="card-title">
                    {{ $sub['section_title'] }}
                    <a href="{{ route($sub['route'] . '.create', $item->id) }}" class="btn btn-primary btn-sm">@Lang('Add new')</a>
                </h4>
            </div>
            <div class="card-body">
                <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                </div>
                <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                @foreach ($sub['headers'] as $header => $params)
                                    <th
                                    @if ( array_key_exists('width', $params) )
                                        width="{{ $params['width'] }}"
                                    @endif
                                    @if ( array_key_exists('class', $params) )
                                        class="{{ $params['class'] }}"
                                    @endif
                                    >
                                        {{ $header }}
                                    </th>
                                @endforeach
                                <th width="10%" class="disabled-sorting text-right">@Lang('Actions')</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                @foreach ($sub['headers'] as $header => $params)
                                    <th
                                    @if ( array_key_exists('width', $params) )
                                        width="{{ $params['width'] }}"
                                    @endif
                                    @if ( array_key_exists('class', $params) )
                                        class="{{ $params['class'] }}"
                                    @endif
                                    >
                                        {{ $header }}
                                    </th>
                                @endforeach
                                <th width="10%" class="disabled-sorting text-right">@Lang('Actions')</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        @forelse ($sub['items'] as $sub)
                            <tr>
                                @foreach ( $sub['rows'] as $params )
                                    @php ( $field = $params['value'] )
                                    @php ( $show_value = true )
                                    @php ( $name = $sub->{$sub['field_name']} )
                                    @php ( $field_value = $sub->{$field} )
                                    <td
                                    @if ( array_key_exists('width', $params) )
                                        width="{{ $params['width'] }}"
                                    @endif
                                    @if ( array_key_exists('class', $params) )
                                        class="{{ $params['class'] }}"
                                    @endif
                                    >
                                        @if ( array_key_exists('type', $params) )
                                            @if ( $params['type'] == 'icon' )
                                                <i class="material-icons pr-2 admin-menu-icona" style="font-size: 2rem;">
                                            @elseif ( $params['type'] == 'img' )
                                                @php ( $show_value = false )
                                                <img class="show_img col-imatge col-10" src="data:image/jpeg;base64,{{ base64_encode( $field_value ) }}" 
                                                    data-img="data:image/jpeg;base64,{{ base64_encode( $field_value ) }}" 
                                                    data-name="{{ $name }}"
                                                />
                                            @elseif ( $params['type'] == 'thumbnail' )
                                                @php ( $show_value = false )
                                                <img class="show_img col-imatge col-10" src="{{ $sub->get_thumbnail_url('thumbnail') }}"
                                                    data-img="{{ $sub->get_thumbnail_url('large') }}" 
                                                    data-name="{{ $name }}"
                                                    {{--style="width: 50px;object-fit: scale-down;"--}}
                                                />
                                            @elseif ( $params['type'] == 'switch_active' )
                                                @php ( $show_value = false )
                                                <button data-url="{{ route($params['route']) }}" data-id="{{$sub->id}}" data-name="{{$name}}" data-onlyone="{{$params['onlyone']}}" type="button" rel="tooltip" class="btn btn-link btn-just-icon switch_active btn-{{ $sub->active ? 'success' : 'danger' }}">
                                                    <i class="material-icons">{{ $sub->active ? 'check' : 'clear' }}</i>
                                                </button>
                                            @elseif ( $params['type'] == 'count' )
                                                @php ( $show_value = false )
                                                {{ count($field_value) }}
                                            @elseif ( $params['type'] == 'date' )
                                                @php ( $show_value = false )
                                                @php ( $date_format = !empty($params['format']) ? $params['format'] : 'd/m/Y h:i' )
                                                {{ $field_value->format( $date_format ) }}
                                            @endif
                                        @endif

                                        @if ( array_key_exists('type', $params) && $params['type'] == 'icon' )
                                            <i class="material-icons pr-2 admin-menu-icona" style="font-size: 2rem;">
                                        @endif
                                        
                                        @if ( $show_value )
                                            @if ( array_key_exists('translate', $params) && $params['translate'] )
                                                {{ __($field_value) }}
                                            @else
                                                {{ $field_value }}
                                            @endif
                                        @endif
                                        
                                        @if ( array_key_exists('type', $params) && $params['type'] == 'icon' )
                                            </i>
                                        @endif
                                    </td>
                                @endforeach
                                <td class="td-actions text-right">
                                    <button onclick="window.location='{{ route($sub['route'] . '.edit', $sub->id) }}';" type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-success">
                                        <i class="material-icons">edit</i>
                                    </button>
                                    <button data-url="{{ route($sub['route'] . '.destroy', $sub->id) }}" data-name="@Lang('Are you sure to delete :name?', ['name' => __($name)])" type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove">
                                        <i class="material-icons">close</i>
                                    </button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="{{ count($sub['headers']) }}">@Lang('No entries found.')</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </article>
    </div>
</div>
@endforeach
@endisset

@endsection

@section('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<script>
$(document).ready(function() {
    var table = $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "@Lang('All')"]
        ],
        responsive: true,
        /*language: {
            search: "_INPUT_",
            searchPlaceholder: "@Lang('Search')",
        }*/
        language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" }
    });

    //var table = $('#datatable').DataTable();

    // Delete a record
    table.on('click', '.remove', function(e) {
        var $tr = $(this).closest('tr');
        var url = $(this).data('url');
        var title = $(this).data('name');
        swal({
            text: title,
            //title: 'Are you sure?',
            //text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: '@Lang('Delete')',
            cancelButtonText: '@Lang('Cancel')',
            buttonsStyling: false
        }).then(function(result) {
            if ( result.value ) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {'_token': '{{csrf_token()}}', '_method': 'DELETE'},
                    success: function(data) {
                        swal({
                            title: "@Lang('Deleted')",
                            text: "@Lang('Item deleted succesfully.')",
                            type: 'success',
                            confirmButtonClass: "btn btn-success",
                            buttonsStyling: false
                        });
                        table.row($tr).remove().draw();
                        e.preventDefault();
                    },
                    error: function(data) {
                        swal({
                            title: "@Lang('Error')",
                            text: "@Lang('Error saving data')",
                            type: 'error',
                            confirmButtonClass: "btn",
                            buttonsStyling: false
                        });
                    }
                });
            }
        });
    });

    table.on('click', '.switch_active', function(e) {
        var boto = $(this);
        var url = $(this).data('url');
        var title = $(this).data('name');
        var text = "@Lang('actived')";
        var id = $(this).data('id');
        var onlyone = $(this).data('onlyone');
        $.ajax({
            url: url,
            method: 'POST',
            data: {'_token': '{{csrf_token()}}', 'id': id},
            success: function(data) {
                if ( data.active ) {
                    swal({
                        title: "{{ucfirst(__('actived'))}}",
                        text: title + " @Lang('actived')",
                        type: 'success',
                        confirmButtonClass: "btn btn-success",
                        buttonsStyling: false
                    });
                    if ( onlyone ) {
                        $('.switch_active').removeClass('btn-success');
                        $('.switch_active').addClass('btn-danger');
                        $('.switch_active').find('i').html('clear');
                    }
                    boto.removeClass('btn-danger');
                    boto.addClass('btn-success');
                    boto.find('i').html('check');
                } else {
                    swal({
                        title: "{{ucfirst(__('deactived'))}}",
                        text: title + " @Lang('deactived')",
                        type: 'success',
                        confirmButtonClass: "btn btn-success",
                        buttonsStyling: false
                    });
                    boto.addClass('btn-danger');
                    boto.removeClass('btn-success');
                    boto.find('i').html('clear');
                }
                e.preventDefault();
            },
            error: function(data) {
                swal({
                    title: "@Lang('Error')",
                    text: "@Lang('Error saving data')",
                    type: 'error',
                    confirmButtonClass: "btn",
                    buttonsStyling: false
                });
            }
        });
    });

    table.on('click', '.show_img', function(e) {
        var boto = $(this);
        var img = $(this).data('img');
        var title = $(this).data('name');
        swal({
            title: title,
            width: '80%',
            showCloseButton: true,
            showCancelButton: false,
            showConfirmButton: false,
            html: '<img src="'+img+'" style="width: 100%;">'
        });
    });
});
</script>
@yield('scripts2')
@endsection