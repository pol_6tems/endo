<?php
/**
 * @title: Media
 */
?>

@php
    $has_thumbnail = false;
    $media = $value;

    if ( !empty($media) && $media->has_thumbnail() ) {
        $has_thumbnail = true;
        $value = $media->id;
    }

    $required_title = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '';
    $required = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? 'required' : '';

    $height = (isset($params->height_param) && $params->height_param) ? $params->height_param : null;
    $width = (isset($params->width_param) && $params->width_param) ? $params->width_param : null;
@endphp

<div id="cf{{ $custom_field->id }}" data-id="{{ $custom_field->id }}" class="pb-2 pr-5 m-auto custom-field media-field">
    <div class="flex-row pt-3">
        <label class="col-md-12 mt-4">
            {{ __($title) }} {{ $required_title }}
            @if (Auth::user()->role == 'admin')
                (<button class="copy" type="button">{{ $custom_field->name }}</button>)
            @endif
        </label>
        <div class="form-group col-md-12 mt-4">
            <div class="fileinput miniatura {{ $has_thumbnail ? 'fileinput-exists' : 'fileinput-new' }} text-center" data-provides="fileinput">
                <div class="fileinput-new thumbnail">
                    <img src="{{ asset('images/image_placeholder.jpg') }}" >
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="">
                    @if ( !empty($media) && $media->has_thumbnail() )
                        @if ( $media->is_image() )
                            <img src="{{ $media->get_thumbnail_url('medium') }}" data-level="{{ $media->level }}" data-type="{{ $media->file_type }}" data-filename="{{ $media->file_name }}" data-id="{{ $media->id }}" data-legend="{{ $media->legend }}" data-title="{{ $media->title }}" data-alt="{{ $media->alt }}" data-url="{{ $media->get_thumbnail_url() }}" data-height="{{ $media->height }}" data-width="{{ $media->width }}">
                        @elseif($media->is_video())
                            <video controls autoplay loop>
                                <source src="{{ $media->get_thumbnail_url() }}" type="video/mp4">
                                <em>Sorry, your browser doesn't support HTML5 video.</em>
                            </video>
                        @else
                            <img src="{{ asset($_admin . 'img/icons/doc.png') }}" >
                            <div>{{ $media->file_name }}</div>
                        @endif
                    @endif
                </div>
                <div>
                    <span class="btn btn-rose btn-round edit-media btn-sm ">
                        <span class="fileinput-new"><i class="material-icons">cloud_upload</i></span>
                        <span class="fileinput-exists"><i class="material-icons">edit</i></span>
                        <input type="hidden"><input class="media_input" type="hidden" name="{{ $name }}" @if ($height && $width)data-height="{{ $height }}" data-width="{{ $width }}"@endif value="{{ $value }}" />
                        <div class="ripple-container"></div>
                    </span>
                    <a href="#" class="btn btn-danger btn-round fileinput-exists btn-sm remove-media" data-dismiss="fileinput">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </a>
                    @if (!empty($media) && $media->has_thumbnail() && $media->is_image() && $media->isCroppable())
                        <span class="btn btn-info btn-round btn-sm crop-media"><i class="material-icons">crop</i></span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>