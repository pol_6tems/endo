<?php
/**
 * @title: Multiple Switch
 */
?>
{{--
@php ( $choices = \App\Post::where('type', $params->post_type)->get() )
@php ( $post_field = $params->post_field )

@if ( isset($value) )
    @php ( $value = json_decode($value, TRUE) )
@endif

<div id="cf{{ $custom_field->id }}" class="pb-2 col-lg-12 pr-5 m-auto">
    <div class="row">
        <label class="col-md-12 mt-4">{{ __($title) }} {{ (!isset($is_param) && $params->required) ? '*' : '' }}</label>
    </div>
    <div class="row">
        <div class="ml-2 col-md-12">
            <div class="form-group">
                @foreach ( $choices as $k_multi => $choice )
                    @php ( $choice_aux = $choice->translate( \App::getLocale() ) )
                    <div class="togglebutton">
                        <label>
                            @php ( $choice_title = $choice_aux->{$post_field} )
                            @php ( $choice_value = $choice_aux->id )
                            <input type="checkbox" name="{{ $name }}[{{$k_multi}}]" value="{{ $choice_value }}" {{ isset($value) && in_array($choice_value, $value) ? 'checked' : '' }}>
                            <span class="toggle"></span>
                            {{ $choice_title }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
--}}