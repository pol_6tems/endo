<?php
/**
 * @title: Form
 */
?>

@php
$value = '';

if ( !empty($post) ) $value = $post->get_field( $custom_field->name);

$preguntes = $value;
if ( !is_array($preguntes) ) $preguntes = (array)json_decode($value);

$k = 0;
@endphp

<div id="cf{{ $custom_field->id }}" class="pb-5 pr-5 m-auto form-field" style="border: dashed 1px #999;">
    <div class="flex-row">
        <div class="form-group col-md-4 col-sm-4 ml-5 mr-auto">
            <h4 class="title">
                <b>{{ __($custom_field->title) }}</b>
                <button type="button" class="add_field-{{$custom_field->id}} btn btn-sm btn-primary" data-lang="{{$lang}}" data-name="{{$name}}">
                    @Lang('Add')
                </button>
                <div class="dropdown bootstrap-select form-group col-lg-5">
                    <select id="tipus_field-{{$custom_field->id}}-{{$lang}}" class="selectpicker" data-size="7" data-style="btn btn-primary btn-round" title="@Lang('Type')" data-live-search="true">
                        <option value="text" selected>@Lang('Text')</option>
                        <option value="textarea">@Lang('Textarea')</option>
                        <option value="checkbox">@Lang('Checkbox')</option>
                    </select>
                </div>
            </h4>
        </div>
    </div>
    
    @foreach ( $preguntes as $k => $pregunta )
    <div class="row pregunta-{{$lang}}">
        <label class="col-sm-2 col-form-label">@Lang('Question') {{ $k + 1 }}</label>
        <div class="col-sm-10">
            <div class="form-group">
                <input name="{{$name}}[{{$k}}][question]" type="text" class="form-control" value="{{$pregunta->question}}">
                <input name="{{$name}}[{{$k}}][answer]" type="hidden" value="{{$pregunta->answer}}">
            </div>
        </div>
    </div>
        @if ( $pregunta->answer == 'checkbox' )
            <div class="row">
                <label class="col-sm-2 col-form-label">@Lang('Choices') {{ $k + 1 }}</label>
                <div class="col-sm-10">
                    <div class="form-group">
                        <textarea name="{{$name}}[{{$k}}][choices]" class="form-control">{{$pregunta->choices}}</textarea>
                        <span class="bmd-help">ex. manzana : Manzana</span>
                    </div>
                </div>
            </div>
        @endif
    @endforeach

    <div id="preguntes-{{$custom_field->id}}-{{$lang}}"></div>
</div>

@section("scripts-cf$custom_field->id")
<script>
$('.add_field-{{$custom_field->id}}').click(function() {
    var lang = $(this).data('lang');
    var name = $(this).data('name');
    var tipus = $('#tipus_field-{{$custom_field->id}}-'+lang).val();
    var num_field = $('.pregunta-'+lang).length;

    var html = '<div class="row pregunta-'+lang+'">';
    html += '<label class="col-sm-2 col-form-label">@Lang('Question') '+ (num_field + 1) +'</label>';
    html += '<div class="col-sm-10">';
    html += '<div class="form-group">';
    html += '<input name="'+name+'['+num_field+'][question]" type="text" class="form-control">';
    html += '<input name="'+name+'['+num_field+'][answer]" type="hidden" value="'+tipus+'">';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    if ( tipus == 'checkbox' ) {
        html += '<div class="row">';
        html += '<label class="col-sm-2 col-form-label">@Lang('Choices') '+(num_field + 1)+'</label>';
        html += '<div class="col-sm-10">';
        html += '<div class="form-group">';
        html += '<textarea name="'+name+'['+num_field+'][choices]" class="form-control summernote"></textarea>';
        html += '<span class="bmd-help">ex. manzana : Manzana</span>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
    }
    $(html).insertBefore('#preguntes-{{$custom_field->id}}-' + lang);
});
</script>
@endsection