<?php
/**
 * @title: Number
 */
?>

@php
    $required_title = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '';
    $required = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? 'required' : '';
    $max = (isset($params->max_length) && $params->max_length) ? $params->max_length : '';
    $min = (isset($params->min_length) && $params->min_length) ? $params->min_length : '';
    if ( !is_numeric($value) ) $value = (isset($params->default_value) && $params->default_value) ? $params->default_value : '';
@endphp

<div id="cf{{ $custom_field->id }}" data-id="{{ $custom_field->id }}" class="m-auto custom-field number-field">
    <div class="flex-row pt-3">
        <label class="col-md-12">
            {{ __($title) }} {{ $required_title }}
            @if (isset($min) && $min != '') - Min: {{ $min }}@endif
            @if (isset($max) && $max != '') - Max: {{ $max }}@endif
            @if (Auth::user()->role == 'admin')
                (<button class="copy" type="button">{{ $custom_field->name }}</button>)
            @endif
        </label>
        <div class="col-md-12">
            <div class="form-group">
                <input
                    name="{{ $name }}"
                    type="number"
                    class="form-control"
                    value="{{ $value }}"
                    {{ ($max != '') ? "max=". $max : '' }}
                    {{ ($min != '') ? "min=". $min : '' }}
                    {{ $required }}>
                @if ( !empty($instructions) )
                    <span class="bmd-help">{!! $instructions !!}</span>
                @endif
            </div>
        </div>
    </div>
</div>