<?php
/**
 * @title: Date
 */
?>
@php($required_title = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '')
@php($required = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? 'required' : '')

<div id="cf{{ $custom_field->id }}" data-id="{{ $custom_field->id }}" class="m-auto custom-field date-field">
    <div class="flex-row pt-3">
        <label class="col-md-12">
            {{ __($title) }} {{ $required_title }}
            @if (Auth::user()->role == 'admin')
                (<button class="copy" type="button">{{ $custom_field->name }}</button>)
            @endif
        </label>
        <div class="col-md-12">
            <div class="form-group">
                <input
                    name="{{ $name }}"
                    type="text"
                    class="form-control datepicker"
                    value="{{ $value }}"
                    {{ $required }}/>

                @if ( !empty($instructions) )
                    <span class="bmd-help">{!! $instructions !!}</span>
                @endif
            </div>
        </div>
    </div>
</div>