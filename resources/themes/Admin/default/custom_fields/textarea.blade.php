<?php
/**
 * @title: Textarea
 */
?>

@php
    $max = (isset($params->max_length) && $params->max_length) ? $params->max_length : '';
    $min = (isset($params->min_length) && $params->min_length) ? $params->min_length : '';
    $required_title = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '';
    $required = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? 'required' : '';
    $wysiwyg = (isset($params->wysiwyg) && isset($params->wysiwyg) && $params->wysiwyg) ? 1 : 0;
    $placeholder = (isset($params->placeholder) && $params->placeholder) ? preg_replace( "/\r|\n/", "", $params->placeholder ) : '';
    $rows = (isset($params->num_rows) && $params->num_rows) ? $params->num_rows : 3;
@endphp

<div id="cf{{ $custom_field->id }}" data-id="{{ $custom_field->id }}" class="m-auto custom-field textarea-field">
    <div class="flex-row pt-3">
        <label class="col-md-12">
            {{ __($title) }} {{ $required_title }}
            @if (Auth::user()->role == 'admin')
                (<button class="copy" type="button">{{ $custom_field->name }}</button>)
            @endif
            - @Lang('Max:') {{ $max }} @Lang('caracteres')
        </label>
        <div class="col-md-12">
            <div class="has-default">
                <textarea
                    name="{{ $name }}"
                    rows="{{ $rows }}"
                    class="form-control {{ ($wysiwyg) ? 'wysiwyg' : 'text' }}"
                    {{ $required }}
                    maxlength="{{ $max }}"
                    minlength="{{ $min }}"
                    placeholder="{{ $placeholder }}"
                    @if (!$wysiwyg)
                    onKeydown="textareaKeyDown(event, {{ $max }})"
                    onKeyup="textareaKeyup(event, {{ $max }}, $('#cf{{ $custom_field->id }}-max'))"
                    onPaste="textareaPaste(event, {{ $max }}, $('#cf{{ $custom_field->id }}-max'))"
                    @endif
                    >{{ ($wysiwyg) ? $value : strip_tags($value) }}</textarea>
                <div class="progress-container">
                    <div class="progress">
                        <div id="cf{{ $custom_field->id }}-max" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ strlen(strip_tags($value)) }}" aria-valuemin="0" aria-valuemax="{{ $max }}" style="width: {{ strlen(strip_tags($value))*100/400}}%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section("scripts-cf$custom_field->id")
<script>
var placeholder = "{{ $placeholder }}";
var max = "{{ $max }}";

function comptarParaules(max) {
    var current = ((parseInt("{{ $max }}")) - max)*100/(parseInt("{{ $max }}"));

    $('#cf{{ $custom_field->id }}-max').attr('aria-valuenow', (400 - max))
    $('#cf{{ $custom_field->id }}-max').css('width', current+'%')
}

registerSummernote("#cf{{ $custom_field->id }} .wysiwyg", placeholder, max, comptarParaules);
</script>
@endsection