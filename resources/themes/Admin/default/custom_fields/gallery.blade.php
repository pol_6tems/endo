<?php
/**
 * @title: Gallery
 */
?>

@php
    $required_title = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '';
    $required = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '';
    $min = (isset($params->min_length) && $params->min_length) ? $params->min_length : '';

    $height = (isset($params->height_param) && $params->height_param) ? $params->height_param : null;
    $width = (isset($params->width_param) && $params->width_param) ? $params->width_param : null;

@endphp

<div id="cf{{ $custom_field->id }}" data-id="{{ $custom_field->id }}" class="pb-2 m-auto custom_field_gallery custom-field gallery-field">
    <input class="name" type="hidden" name="{{ $name }}" value="0">
    <div class="flex-row pt-3">
        <label class="col-md-4">
            {{ __($title) }} {{ $required_title }}
            @if (Auth::user()->role == 'admin')
                (<button class="copy" type="button">{{ $custom_field->name }}</button>)
            @endif

            @if (isset($min) && $min != '') - Min: {{ $min }}@endif
        </label>
        <button id="{{ $name }}" type="button" rel="tooltip" class="btn btn-sm btn-primary add_image_to_gallery" style="margin-left: auto;">
            @Lang('Add')
        </button>
        <div class="gallery form-group col-md-12">
            @php ( $k_gal = 1 )
            @if ($value && !empty($value))
                @foreach ( $value as $media )
                    @if ( !empty($media) && $media->has_thumbnail() )
                        @php ( $has_thumbnail = true )
                        <div class="miniatura fileinput {{ $has_thumbnail ? 'fileinput-exists' : 'fileinput-new' }} text-center" data-provides="fileinput">
                            <div class="fileinput-new thumbnail">
                                <img src="{{ asset('images/image_placeholder.jpg') }}">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="">
                                @if ( !empty($media) && $media->has_thumbnail() )
                                    @if ( $media->is_image() )
                                        <img class="media {{ $media->getFileType() }}" src="{{ $media->get_thumbnail_url('medium') }}" data-level="{{ $media->level }}" data-type="{{ $media->file_type }}" data-filename="{{ $media->file_name }}" data-id="{{ $media->id }}" data-legend="{{ $media->legend }}" data-title="{{ $media->title }}" data-alt="{{ $media->alt }}" data-url="{{ $media->get_thumbnail_url() }}" data-height="{{ $media->height }}" data-width="{{ $media->width }}">
                                    @else
                                        <img src="{{ asset($_admin . 'img/icons/doc.png') }}" >
                                    @endif
                                @endif
                            </div>
                            <div>
                                <span class="btn btn-rose btn-round btn-file btn-sm edit-media">
                                    <span class="fileinput-new"><i class="material-icons">cloud_upload</i></span>
                                    <span class="fileinput-exists"><i class="material-icons">edit</i></span>
                                    <input type="hidden"><input class="media_input" type="hidden" name="{{ $name }}[{{ $k_gal }}]" @if ($height && $width)data-height="{{ $height }}" data-width="{{ $width }}"@endif value="{{ $media->id }}" />
                                    <div class="ripple-container"></div>
                                </span>
                                <a href="javascript:void(0);" class="btn btn-danger btn-round fileinput-exists btn-sm remove-media" data-dismiss="fileinput" onclick="delete_gallery_element(this);">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                                @if (!empty($media) && $media->has_thumbnail() && $media->is_image() && $media->isCroppable())
                                    <span class="btn btn-info btn-round btn-sm crop-media"><i class="material-icons">crop</i></span>
                                @endif
                            </div>
                        </div>
                        @php ( $k_gal += 1 )
                    @endif
                @endforeach
            @endif
        </div>
        @if (isset($params->instructions_param) && $params->instructions_param)
            <div class="col-md-12">
                <span>{{ $params->instructions_param }}</span>
            </div>
        @endif
    </div>
</div>

@section("scripts-cf$custom_field->id")
<script src="{{asset($_admin.'js/plugins/Sortable.min.js')}}"></script>
<script>
Sortable.create(document.querySelector('#cf{{ $custom_field->id }} .gallery'), {
    animation: 150,
    ghostClass: 'blue-background-class',
    onEnd: function(evt) {
        $('#cf{{ $custom_field->id }} .miniatura').each(function(idx, element) {
            $(element).find('.media_input').attr('name', '{{ $name }}[' + (idx+1) + ']')
        });
    }
});
</script>

    @if (isset($min) && is_numeric($min))
        <script>
            var cf{{ $custom_field->id }} = $('#cf{{ $custom_field->id }}');
            cf{{ $custom_field->id }}.closest('form').on('submit', function (e) {

                if ( $(this).find('input[type=hidden][name=status]').val() != 'draft' ) {

                    if ($('#cf{{ $custom_field->id }} .media_input').length < {{ $min }}) {
                        e.preventDefault();
                        cf{{ $custom_field->id }}.closest('.collapse').removeClass('collapse');
                        $("html, body").animate({ scrollTop: $('#cf{{ $custom_field->id }}').offset().top }, 300);
                        $('#cf{{ $custom_field->id }} label:first').css('color', 'red');
                    }

                }
            });
        </script>
    @endif
@endsection