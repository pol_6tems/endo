<?php
/**
 * @title: Google Maps
 */
?>

@php
    $required_title = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '';
    $required = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? 'required' : '';
    $id_element = 'cf' . $custom_field->id;

    if ($custom_field->parent_id) {
        $id_element .= '_k' . $k;
    }
@endphp

<div id="{{ $id_element }}" data-id="{{ $custom_field->id }}" class="pb-2 m-auto custom-field gmaps-field">
    <div class="flex-row pt-3">
        <label class="col-md-12">
            {{ __($title) }} {{ $required_title }}
            @if (Auth::user()->role == 'admin')
                (<button class="copy" type="button">{{ $custom_field->name }}</button>)
            @endif
        </label>
        <div class="form-group col-md-12">
            <input class="gmaps_obj" type="hidden" name="{{ $name }}[object]"/>
            <input
                class="coordenades"
                type="hidden"
                name="{{ $name }}[coordenades]"
                value="{{ $value ? $value->coordenades : '' }}"/>
            
            <input
                class="zoom"
                type="hidden"
                name="{{ $name }}[zoom]"
                value="{{ $value ? $value->zoom : '' }}"/>
            
            <input
                class="form-control searchTextField"
                type="text"
                name="{{ $name }}[adreca]"
                value="{{ $value ? $value->adreca : '' }}"
                {{ $required }} />
        </div>
        <div class="col-md-12">
            <div class="map map-big"></div>
        </div>
    </div>
</div>

@if ($value && $value != '' && $value->coordenades)
    @php($lat = explode(',', $value->coordenades)[0])
    @php($lng = explode(',', $value->coordenades)[1])
@endif

@section("scripts-$id_element")
<script>
var map{{$id_element}}, autocomplete{{$id_element}}, marker{{$id_element}};
function {{ $id_element }}_initMap() {
    @if ($value && $value != '' && $value->coordenades)
    var myLatlng = new google.maps.LatLng({{$lat}},{{$lng}});
    @else
    var myLatlng = new google.maps.LatLng(41.8862532,2.8751690999999937);
    @endif
    var mapOptions = {
        zoom: {{ isset($value) && $value != '' && $value->zoom ? $value->zoom : 14 }},
        center: myLatlng,
        scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
        disableDoubleClickZoom: true,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            mapTypeIds: ['roadmap', 'terrain']
        }
    };
    map{{$id_element}} = new google.maps.Map(document.getElementById("{{ $id_element }}").querySelector('.map'), mapOptions);

    map{{$id_element}}.addListener('dblclick', function(event) {
        marker{{$id_element}}.setVisible(false);
        marker{{$id_element}}.setPosition(event.latLng);
        marker{{$id_element}}.setVisible(true);

        document.getElementById("{{ $id_element }}").querySelector('.coordenades').value = event.latLng.lat() + ',' + event.latLng.lng();
        document.getElementById("{{ $id_element }}").querySelector('.zoom').value = map{{$id_element}}.getZoom();
    });

    marker{{$id_element}} = new google.maps.Marker({
        position: myLatlng,
        map: map{{$id_element}}
    });
    marker{{$id_element}}.setMap(map{{$id_element}});

}


function {{ $id_element }}_autocomplete() {
    var input = document.getElementById("{{ $id_element }}").querySelector('.searchTextField');
    var options = {
        types: ['geocode', 'establishment']
    };
    autocomplete{{$id_element}} = new google.maps.places.Autocomplete(input, options);
    autocomplete{{$id_element}}.bindTo('bounds', map{{$id_element}});
    autocomplete{{$id_element}}.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

    autocomplete{{$id_element}}.addListener('place_changed', function() {
        marker{{$id_element}}.setVisible(false);
        var place = autocomplete{{$id_element}}.getPlace();

        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }
        
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map{{$id_element}}.fitBounds(place.geometry.viewport);
        } else {
            map{{$id_element}}.setCenter(place.geometry.location);
            map{{$id_element}}.setZoom(17);  // Why 17? Because it looks good.
        }

        marker{{$id_element}}.setPosition(place.geometry.location);

        marker{{$id_element}}.setVisible(true);
        document.getElementById("{{ $id_element }}").querySelector('.gmaps_obj').value = JSON.stringify(place);
        document.getElementById("{{ $id_element }}").querySelector('.coordenades').value = place.geometry.location.lat() + ',' + place.geometry.location.lng();
        document.getElementById("{{ $id_element }}").querySelector('.zoom').value = map{{$id_element}}.getZoom();
    });
}

{{ $id_element }}_initMap();
{{ $id_element }}_autocomplete();
</script>
@endsection