<?php
/**
 * @title: Boolean
 * @author: Oriol Testart
 */
?>
@php
    $required = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '';
    $boolean_rule = (isset($params->boolean_rule) && $params->boolean_rule) ? $params->boolean_rule : null;
@endphp

<div id="cf{{ $custom_field->id }}" data-id="{{ $custom_field->id }}" class="m-auto custom-field boolean-field">
    <div class="flex-row pt-3">
        <label class="col-md-10">
            {{ __($title) }} {{ $required }}
            @if (Auth::user()->role == 'admin')
                (<button class="copy" type="button">{{ $custom_field->name }}</button>)
            @endif
        </label>
        <div class="col-md-2" style="display: flex;align-items: center;">
            <div class="form-check">
                <label class="form-check-label">
                    <input type='hidden' value="0" name="{{ $name }}">
                    <input
                        class="form-check-input"
                        name="{{ $name }}"
                        type="checkbox"
                        value="1"
                        onchange="$('#cf{{ $boolean_rule }}').fadeToggle();"
                        {{ $required }}
                        {{ $value ? 'checked' : '' }} />
                        
                    <span class="form-check-sign">
                        <span class="check"></span>
                    </span>
                </label>
            </div>
        </div>
    </div>
</div>

@if ($boolean_rule)
<script>
@if ( !$value )
document.addEventListener('DOMContentLoaded', function() {
    var field = document.getElementById('cf{{ $boolean_rule }}');
    field.style.display = 'none';
}, false);
@endif
</script>
@endif