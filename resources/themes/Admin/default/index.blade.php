@extends('Admin::layouts.admin')

@section('section-title')
    {{ $section_title }}
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">{{ $section_icon }}</i>
                </div>
                <h4 class="card-title">
                    {{ $section_title }}
                    @if ( empty($readonly) || (!empty($readonly) && !$readonly) )
                        <a href="{{ route($route . '.create') }}" class="btn btn-primary btn-sm">@Lang('Add new')</a>
                    @endif
                    @if ( !empty($toolbar_header) )
                        @foreach ( $toolbar_header as $tool )
                            {!! $tool !!}
                        @endforeach
                    @endif
                </h4>
            </div>
            <div class="card-body">
                <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                    @if ( !empty($toolbar) )
                        @foreach ( $toolbar as $tool )
                            {!! $tool !!}
                        @endforeach
                    @endif
                </div>
                <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                @foreach ($headers as $header => $params)
                                    <th
                                    @if ( array_key_exists('width', $params) )
                                        width="{{ $params['width'] }}"
                                    @endif
                                    @if ( array_key_exists('class', $params) )
                                        class="{{ $params['class'] }}"
                                    @endif
                                    >
                                        {{ $header }}
                                    </th>
                                @endforeach
                                <th width="10%" class="disabled-sorting text-right">@Lang('Actions')</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                @foreach ($headers as $header => $params)
                                    <th
                                    @if ( array_key_exists('width', $params) )
                                        width="{{ $params['width'] }}"
                                    @endif
                                    @if ( array_key_exists('class', $params) )
                                        class="{{ $params['class'] }}"
                                    @endif
                                    >
                                        {{ $header }}
                                    </th>
                                @endforeach
                                <th width="15%" class="disabled-sorting text-right">@Lang('Actions')</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @forelse ($items as $item)
                            <tr>
                                {{-- En cas de ser traduible obtenir el post traduit --}}
                                @if ( isset($translable) && $translable )
                                    @php ( $item_aux = $item->translate( \App::getLocale(), true) )
                                @else
                                    @php ( $item_aux = $item )
                                @endif

                                @if ( is_array($field_name) )
                                    @php ( $field_name_aux = $item_aux )
                                    @foreach ( $field_name as $f_key => $f )
                                        @if ( $field_name_aux == null )
                                            @php ( $field_name_aux = '-' )
                                            @break
                                        @else
                                            @if ( $f == 'translate()' )
                                                @php ( $field_name_aux = strpos($f, '()') !== false ? $field_name_aux->{str_replace('()','',$f)}(true) : $field_name_aux->{$f} )
                                            @else
                                                @php ( $field_name_aux = strpos($f, '()') !== false ? $field_name_aux->{str_replace('()','',$f)}() : $field_name_aux->{$f} )
                                            @endif
                                        @endif
                                    @endforeach
                                    @php ( $field_name = $field_name_aux )
                                @else
                                    {{-- Obtenim del objecte la variable especificada --}}
                                    @php ( $field_name = $item_aux->{$field_name} )
                                @endif

                                @php ( $name = $field_name )
                                @foreach ( $rows as $params )
                                    @php ( $show_value = true )
                                    @php ( $field = $params['value'] )

                                    @if ( !is_array($field) )
                                        @php ( $show_value = true )
                                        @if ( $field == 'translate()' )
                                            @php ( $field_value = strpos($field, '()') !== false ? $item_aux->{str_replace('()','',$field)}(true) : $item_aux->{$field} )
                                        @elseif (strpos($field, 'get_field') !== false)
                                            @php($field_value = $item_aux->get_field(str_replace('"', '', str_replace('\'', '', trim(trim(str_replace('get_field', '', $field), ')'), '(')))) ?: '')
                                        @elseif (strpos($field, 'present()') !== false)
                                            @php ($field_value = $item_aux->present()->{str_replace('present()->','',$field)})
                                        @else
                                            @php ( $field_value = strpos($field, '()') !== false ? $item_aux->{str_replace('()','',$field)}() : $item_aux->{$field} )
                                        @endif
                                    @else
                                        @php ( $field_value = $item_aux )

                                        @foreach ( $field as $f_key => $f )
                                            @if ( $field_value == null )
                                                @php ( $field_value = '-' )
                                                @break
                                            @else
                                                @if ( $f == 'translate()' )
                                                    @php ( $field_value = strpos($f, '()') !== false ? $field_value->{str_replace('()','',$f)}(true) : $field_value->{$f} )
                                                @elseif (strpos($f, 'present()') !== false)
                                                    @php ($field_value = $field_value->present()->{str_replace('present()->','',$f)})
                                                @else
                                                    @php ( $field_value = strpos($f, '()') !== false ? $field_value->{str_replace('()','',$f)}() : $field_value->{$f} )
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif

                                    <td
                                        @if ( array_key_exists('width', $params) )
                                            width="{{ $params['width'] }}"
                                        @endif
                                        @if ( array_key_exists('class', $params) )
                                            class="{{ $params['class'] }}"
                                        @endif
                                        @if (isset($params['type']) && $params['type'] == 'date')
                                            data-order="{{ $field_value ? $field_value->timestamp : '' }}"
                                        @endif
                                    >
                                        @if ( array_key_exists('type', $params) )
                                            @if ( $params['type'] == 'icon' )
                                                <i class="material-icons pr-2 admin-menu-icona" style="font-size: 2rem;">
                                            @elseif ( $params['type'] == 'img' )
                                                @php ( $show_value = false )
                                                <img class="show_img col-imatge col-10" src="data:image/jpeg;base64,{{ base64_encode( $field_value ) }}" 
                                                    data-img="data:image/jpeg;base64,{{ base64_encode( $field_value ) }}" 
                                                    data-name="{{ $name }}"
                                                />
                                            @elseif ( $params['type'] == 'thumbnail' )
                                                @php ( $show_value = false )

                                                @if ($item->is_image())
                                                    <img class="show_img col-imatge col-10" src="{{ $item->get_thumbnail_url('thumbnail') }}"
                                                        data-img="{{ $item->get_thumbnail_url('large') }}" 
                                                        data-name="{{ $name }}"
                                                        {{--style="width: 50px;object-fit: scale-down;"--}}
                                                        data-action="zoom"
                                                        data-original="{{ $item->get_thumbnail_url('large') }}" 
                                                    />
                                                @else
                                                    <a href="{{ $item->get_thumbnail_url() }}" target="_blank">
                                                        <img width="36" height="36" class="col-imatge" src="{{ asset($_admin.'img/icons/doc.png') }}"
                                                            data-name="{{ $name }}"
                                                            {{--style="width: 50px;object-fit: scale-down;"--}}
                                                        />
                                                    </a>
                                                @endif
                                            @elseif ( $params['type'] == 'media' )
                                                @php ( $show_value = false )
                                                @php ( $media_field = $params['media'] )

                                                @if ( !empty($item_aux->{$media_field}) )
                                                    @if ($item_aux->{$media_field}->is_image())
                                                        <img class="show_img col-imatge col-10" src="{{ $item_aux->{$media_field}->get_thumbnail_url('thumbnail') }}"
                                                            data-img="{{ $item_aux->{$media_field}->get_thumbnail_url('large') }}" 
                                                            data-name="{{ $name }}"
                                                            {{--style="width: 50px;object-fit: scale-down;"--}}
                                                            data-action="zoom"
                                                            data-original="{{ $item_aux->{$media_field}->get_thumbnail_url('large') }}" 
                                                        />
                                                    @else
                                                        <a href="{{ $item_aux->{$media_field}->get_thumbnail_url() }}" target="_blank">
                                                            <img width="36" height="36" class="col-imatge" src="{{ asset($_admin.'img/icons/doc.png') }}"
                                                                data-name="{{ $name }}"
                                                                {{--style="width: 50px;object-fit: scale-down;"--}}
                                                            />
                                                        </a>
                                                    @endif
                                                @endif
                                            @elseif ( $params['type'] == 'switch_active' )
                                                @php ( $show_value = false )
                                                <button data-url="{{ route($params['route']) }}" data-id="{{$item->id}}" data-name="{{$field_name}}" data-onlyone="{{$params['onlyone']}}" data-column="{{ $params['value'] }}" type="button" rel="tooltip" class="btn btn-link btn-just-icon switch_active btn-{{ $field_value ? 'success' : 'danger' }}">
                                                    <i class="material-icons">{{ $field_value ? 'check' : 'clear' }}</i>
                                                </button>
                                            @elseif ( $params['type'] == 'count' )
                                                @php ( $show_value = false )
                                                {{ count($field_value) }}
                                            @elseif ( $params['type'] == 'yes_no' )
                                                @php ( $show_value = false )
                                                {{ $field_value == 1 ? __('Yes') : __('No') }}
                                            @elseif ( $params['type'] == 'date' )
                                                @php ( $show_value = false )
                                                @php ( $date_format = !empty($params['format']) ? $params['format'] : 'd/m/Y H:i' )
                                                @if ( !empty($field_value) )
                                                    {{ $field_value->format( $date_format ) }}
                                                @endif
                                            @endif
                                        @endif

                                        @if ( array_key_exists('type', $params) && $params['type'] == 'icon' )
                                            <i class="material-icons pr-2 admin-menu-icona" style="font-size: 2rem;">
                                        @endif
                                        
                                        @if ( $show_value )
                                            @if ( array_key_exists('translate', $params) && $params['translate'] )
                                                {{ cut_text(__($field_value), 40) }}
                                            @else
                                                {{ cut_text($field_value, 40) }}
                                            @endif
                                        @endif
                                        
                                        @if ( array_key_exists('type', $params) && $params['type'] == 'icon' )
                                            </i>
                                        @endif
                                    </td>
                                @endforeach
                                <td class="td-actions text-right">
                                    @if ( !empty($readonly) && $readonly )
                                        <a href="{{ route($route . '.show', $item->id) }}"rel="tooltip" class="btn btn-just-icon btn-sm btn-warning" title="@Lang('Show')">
                                            <i class="material-icons">personal_video</i>
                                        </a>
                                    @else
                                        <a href="{{ route($route . '.edit', $item->id) }}" rel="tooltip" class="btn btn-just-icon btn-sm btn-success" title="@Lang('Edit')">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        @if ( !empty($has_duplicate) && $has_duplicate )
                                            <button data-url="{{ route($route . '.duplicate', $item->id) }}" data-name="@Lang('Are you sure to duplicate :name?', ['name' => $name])" type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-info duplicate" title="@Lang('Duplicate')">
                                                <i class="material-icons">file_copy</i>
                                            </button>
                                        @endif
                                        @if ( !empty($has_reset) && $has_reset )
                                            <button data-url="{{ route($route . '.reset', $item->id) }}" data-id="{{ $item->id }}" data-name="@Lang('Are you sure to reset :name?', ['name' => $name])" type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-warning reset" title="@Lang('Reset')">
                                                <i class="material-icons">settings_backup_restore</i>
                                            </button>
                                        @endif
                                        <button data-url="{{ route($route . '.destroy', $item->id) }}" data-name="@Lang('Are you sure to delete :name?', ['name' => $name])" type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                                            <i class="material-icons">close</i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="{{ count($headers) }}">@Lang('No entries found.')</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection	

@section('scripts')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<script>
$(document).ready(function() {
    var table = $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
            [25, 50, -1],
            [25, 50, "@Lang('All')"]
        ],
        responsive: true,
        /*language: {
            search: "_INPUT_",
            searchPlaceholder: "@Lang('Search')",
        }*/
        language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" },
        @if (isset($order_col) && isset($order_dir))
            order: [[{{ $order_col }}, '{{ $order_dir }}']],
        @endif
    });

    //var table = $('#datatable').DataTable();

    // Delete a record
    table.on('click', '.remove', function(e) {
        var $tr = $(this).closest('tr');
        var url = $(this).data('url');
        var title = $(this).data('name');
        swal({
            text: title,
            //title: 'Are you sure?',
            //text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: '@Lang('Delete')',
            cancelButtonText: '@Lang('Cancel')',
            buttonsStyling: false
        }).then(function(result) {
            if ( result.value ) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {'_token': '{{csrf_token()}}', '_method': 'DELETE'},
                    success: function(data) {
                        swal({
                            title: "@Lang('Deleted')",
                            text: "@Lang('Item deleted succesfully.')",
                            type: 'success',
                            confirmButtonClass: "btn btn-success",
                            buttonsStyling: false
                        });
                        table.row($tr).remove().draw();
                        e.preventDefault();
                    },
                    error: function(data) {
                        swal({
                            title: "@Lang('Error')",
                            text: "@Lang('Error saving data')",
                            type: 'error',
                            confirmButtonClass: "btn",
                            buttonsStyling: false
                        });
                    }
                });
            }
        });
    });

    table.on('click', '.switch_active', function(e) {
        var boto = $(this);
        var url = $(this).data('url');
        var title = $(this).data('name');
        var column = $(this).data('column');
        var text = "@Lang('actived')";
        var id = $(this).data('id');
        var onlyone = $(this).data('onlyone');
        $.ajax({
            url: url,
            method: 'POST',
            data: {'_token': '{{csrf_token()}}', 'id': id},
            success: function(data) {
                if ( data.active ) {
                    swal({
                        title: "{{ucfirst(__('actived'))}}",
                        text: title + " @Lang('actived')",
                        type: 'success',
                        confirmButtonClass: "btn btn-success",
                        buttonsStyling: false
                    });
                    if ( onlyone ) {
                        $('.switch_active[data-column='+column+']').removeClass('btn-success');
                        $('.switch_active[data-column='+column+']').addClass('btn-danger');
                        $('.switch_active[data-column='+column+']').find('i').html('clear');
                    }
                    boto.removeClass('btn-danger');
                    boto.addClass('btn-success');
                    boto.find('i').html('check');
                } else {
                    swal({
                        title: "{{ucfirst(__('deactived'))}}",
                        text: title + " @Lang('deactived')",
                        type: 'success',
                        confirmButtonClass: "btn btn-success",
                        buttonsStyling: false
                    });
                    boto.addClass('btn-danger');
                    boto.removeClass('btn-success');
                    boto.find('i').html('clear');
                }
                e.preventDefault();
            },
            error: function(data) {
                swal({
                    title: "@Lang('Error')",
                    text: "@Lang('Error saving data')",
                    type: 'error',
                    confirmButtonClass: "btn",
                    buttonsStyling: false
                });
            }
        });
    });

    // DUPLICATE
    table.on('click', '.duplicate', function(e) {
        var $tr = $(this).closest('tr');
        var url = $(this).data('url');
        var title = $(this).data('name');
        swal({
            text: title,
            type: 'info',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: '@Lang('Duplicate')',
            cancelButtonText: '@Lang('Cancel')',
            buttonsStyling: false
        }).then(function(result) {
            if ( result.value ) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {'_token': '{{csrf_token()}}', '_method': 'PUT'},
                    success: function(data) {
                        swal({
                            title: "@Lang('Duplicated')",
                            text: "@Lang('Item duplicated succesfully.')",
                            type: 'success',
                            confirmButtonClass: "btn btn-success",
                            buttonsStyling: false
                        }).then(function(result) {
                            location.reload();
                        });
                    },
                    error: function(data) {
                        swal({
                            title: "@Lang('Error')",
                            text: "@Lang('Error saving data')",
                            type: 'error',
                            confirmButtonClass: "btn",
                            buttonsStyling: false
                        });
                    }
                });
            }
        });
    });

    // Delete a record
    table.on('click', '.reset', function(e) {
        var $tr = $(this).closest('tr');
        var url = $(this).data('url');
        var title = $(this).data('name');
        var id = $(this).data('id');
        swal({
            text: title,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: '@Lang('Reset')',
            cancelButtonText: '@Lang('Cancel')',
            buttonsStyling: false
        }).then(function(result) {
            if ( result.value ) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {'_token': '{{csrf_token()}}', 'id': id},
                    success: function(data) {
                        swal({
                            title: "@Lang('Reset')",
                            text: "@Lang('Item reset succesfully.')",
                            type: 'success',
                            confirmButtonClass: "btn btn-success",
                            buttonsStyling: false
                        });
                        e.preventDefault();
                    },
                    error: function(data) {
                        swal({
                            title: "@Lang('Error')",
                            text: "@Lang('Error saving data')",
                            type: 'error',
                            confirmButtonClass: "btn",
                            buttonsStyling: false
                        });
                        e.preventDefault();
                    }
                });
            }
        });
    });

    /* Image Zooming */
    new Zooming({
        bgColor: '#000',
        bgOpacity: 0.8,
        zIndex: 99999,
    }).listen('.show_img');

    /*table.on('click', '.show_img', function(e) {
        var boto = $(this);
        var img = $(this).data('img');
        var title = $(this).data('name');
        swal({
            title: title,
            width: '80%',
            showCloseButton: true,
            showCancelButton: false,
            showConfirmButton: false,
            html: '<img src="'+img+'" style="width: 100%;">'
        });
    });*/
});
</script>
@endsection