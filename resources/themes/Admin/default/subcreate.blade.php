@extends('Admin::layouts.admin')

@section('section-title')
    {{ $section_title }}
@endsection

@section('content')
<div class="row">
    <div class="col-md-{{ $width or '12' }}">
        <article class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">{{ $section_icon }}</i>
                </div>
                <h4 class="card-title">@Lang('Add new')</h4>
            </div>
            <form id="form" class="form-horizontal" action="{{ route($route . '.store') }}" method="post" enctype="multipart/form-data">
            <div class="card-body ">
                    {{ csrf_field() }}

                    @foreach ( $rows as $key => $params )
                        @if ( empty($params['type']) )
                            @includeIf('Admin::default.subforms.text', ['is_create' => true, 'params' => $params, 'key' => $key])
                        @elseif ( $params['type'] == 'subform' )
                            @includeIf($params['value'])
                        @else
                            @includeIf('Admin::default.subforms.' . $params['type'], ['is_create' => true, 'params' => $params, 'key' => $key])
                        @endif
                    @endforeach

                    {{--
                    <div class="row">
                        <label class="ml-4 col-md-1 col-form-label">@Lang('Name')</label>
                        <div class="col-md-9">
                            <div class="form-group has-default">
                                <input name="name" type="text" class="form-control" value="{{ old('name') }}" required tabindex=1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="ml-4 col-md-1 col-form-label">@Lang('Path')</label>
                        <div class="col-md-9">
                            <div class="form-group has-default">
                                <input name="path" type="text" class="form-control" value="{{ old('path') }}" required tabindex=2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4 col-sm-4 ml-5">
                        <h4 class="title">@Lang('Thumbnail')</h4>
                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                            <div class="fileinput-new thumbnail">
                                <img src="{{ asset('images/image_placeholder.jpg') }}">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="">
                            </div>
                            <div>
                                <span class="btn btn-rose btn-round btn-file">
                                <span class="fileinput-new">@Lang('Select image')</span>
                                <span class="fileinput-exists">@Lang('Change')</span>
                                <input type="hidden"><input type="file" name="file">
                                <div class="ripple-container"></div></span>
                                <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                                    <i class="fa fa-times"></i> @Lang('Delete')
                                </a>
                            </div>
                        </div>
                        </div>
                    </div>
                    --}}
                </div>
                <div class="card-footer">
                    <div class="mr-auto">
                        <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" name="previous" value="@Lang('Tornar')" onclick="window.location.href='{{ route($route_parent . '.index') }}'">
                    </div>
                    <div class="ml-auto">
                        <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="@Lang('Create')">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </article>
    </div>
</div>
@endsection