<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    @if ( !empty($_config['app_icon']['file']) )
        <link rel="apple-touch-icon" sizes="76x76" href="{{ $_config['app_icon']['obj']->get_file_url() }}">
        <link rel="icon" type="image/png" href="{{ $_config['app_icon']['obj']->get_file_url() }}">
    @endif

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        @if ( empty($_config['app_name']['value']) )
            @php( $app_name = config('app.name', 'Endo') )
        @else
            @php( $app_name = $_config['app_name']['value'] )
        @endif
        @yield("title", $app_name)
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Extra details for Live View on GitHub Pages -->
    <!-- Canonical SEO -->
    <link rel="canonical" href="{{ route('index') }}" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{asset($_admin.'css/material-dashboard.min.css?v=2.1.0')}}" rel="stylesheet" />
    <link href="{{asset($_admin.'css/dropzone.css')}}" rel="stylesheet" />
    <style>
        * { --color: {{ $_config['color']['value'] }}; --color-hsl: hsl(350, 87%, 43%); }
    </style>

    <!-- Imatge Fons Sidebar -->
    @if ( !empty($_config['img_sidebar']['file']) )
        <style>
            .sidebar .sidebar-background { background-image: url("{{ asset('storage/configuracion/'.$_config['img_sidebar']['file']) }}") !important; }
        </style>
    @endif

    <link href="{{asset($_admin.'css/override.css')}}" rel="stylesheet" />
    @yield('styles')
</head>

<body>
    <div id="page">
        <div class="wrapper">
            <!-- Sidebar -->
            @includeIf('Admin::partials.sidebar')
            <div class="main-panel">
                <!-- Navbar Template -->
                @includeIf('Admin::partials.navbar')
                <div class="content">
                    <div class="container-fluid">
                        @if ($errors->count() > 0)
                            <!-- Errors Template -->
                            @includeIf('Admin::partials.errors')
                        @endif
                        @yield('content')
                    </div>
                </div>
                @includeIf('Admin::partials.footer')
            </div>
        </div>
        <div id="scrollDown"><i class="material-icons">arrow_downward</i></div>
    </div>
    
    <!--   Core JS Files   -->
    <script src="{{asset($_admin.'js/core/jquery.min.js')}}"></script>
    <script src="{{asset($_admin.'js/core/jquery-ui.min.js')}}"></script>
    <script src="{{asset($_admin.'js/core/popper.min.js')}}"></script>
    <script src="{{asset($_admin.'js/core/bootstrap-material-design.min.js')}}"></script>
    <script src="{{asset($_admin.'js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>

    <!-- Plugin for the Zooming  -->
    <script src="{{asset($_admin.'js/zooming/zooming.min.js')}}"></script>
    <link href="{{asset($_admin.'js/zooming/image-zoom.css')}}" rel="stylesheet" />
    <!-- Plugin for the momentJs  -->
    <script src="{{asset($_admin.'js/plugins/moment.min.js')}}"></script>
    <!--  Plugin for Sweet Alert -->
    <script src="{{asset($_admin.'js/plugins/sweetalert2.js')}}"></script>
    <!-- Forms Validations Plugin -->
    <script src="{{asset($_admin.'js/plugins/jquery.validate.min.js')}}"></script>
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="{{asset($_admin.'js/plugins/jquery.bootstrap-wizard.js')}}"></script>
    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="{{asset($_admin.'js/plugins/bootstrap-selectpicker.js')}}"></script>
    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="{{asset($_admin.'js/plugins/bootstrap-datetimepicker.min.js')}}"></script>
    
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
    <script src="{{asset($_admin.'js/plugins/jquery.dataTables.min.js')}}"></script>
    
    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="{{asset($_admin.'js/plugins/bootstrap-tagsinput.js')}}"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="{{asset($_admin.'js/plugins/jasny-bootstrap.min.js')}}"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="{{asset($_admin.'js/plugins/fullcalendar.min.js')}}"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="{{asset($_admin.'js/plugins/jquery-jvectormap.js')}}"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{asset($_admin.'js/plugins/nouislider.min.js')}}"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <!-- Library for adding dinamically elements -->
    <script src="{{asset($_admin.'js/plugins/arrive.min.js')}}"></script>

    {{-- @if (isset($_config['google_maps']) && $_config['google_maps']['value']) --}}
        <!--  Google Maps Plugin    -->
        <script src="https://maps.googleapis.com/maps/api/js?key={{ isset($_config['google_maps']) ? $_config['google_maps']['value'] : '' }}&language=es&libraries=places"></script>
    {{-- @endif --}}

    <!-- Chartist JS -->
    <script src="{{asset($_admin.'js/plugins/chartist.min.js')}}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{asset($_admin.'js/plugins/bootstrap-notify.js')}}"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{asset($_admin.'js/material-dashboard.min.js?v=2.1.0')}}" type="text/javascript"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
        
    <script>
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                $('#scrollDown').fadeOut();
            } else {
                if ($('#scrollDown').is(':hidden')) {
                    $('#scrollDown').fadeIn();
                }
            }
        });

        $(document).ready(function() {
            $('#scrollDown').on('click', function() {
                $("html, body").animate({ scrollTop: $(document).height() }, 1000, 'swing', function() {
                    $('#scrollDown').fadeOut();
                });
            });

            $().ready(function() {
                $(window).scroll();
                
                $sidebar = $('.sidebar');
                $sidebar_img_container = $sidebar.find('.sidebar-background');
                $full_page = $('.full-page');
                $sidebar_responsive = $('body > .navbar-collapse');
                window_width = $(window).width();
                fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();
                
                if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                    if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                        $('.fixed-plugin .dropdown').addClass('open');
                    }
                }

                $('.fixed-plugin a').click(function(event) {
                    // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                    if ($(this).hasClass('switch-trigger')) {
                        if (event.stopPropagation) {
                            event.stopPropagation();
                        } else if (window.event) {
                            window.event.cancelBubble = true;
                        }
                    }
                });

                $('.fixed-plugin .active-color span').click(function() {
                    $full_page_background = $('.full-page-background');

                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data-color', new_color);
                    }

                    if ($full_page.length != 0) {
                        $full_page.attr('filter-color', new_color);
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.attr('data-color', new_color);
                    }
                });

                $('.fixed-plugin .background-color .badge').click(function() {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('background-color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data-background-color', new_color);
                    }
                });

                $('.fixed-plugin .img-holder').click(function() {
                    $full_page_background = $('.full-page-background');

                    $(this).parent('li').siblings().removeClass('active');
                    $(this).parent('li').addClass('active');


                    var new_image = $(this).find("img").attr('src');

                    if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                        $sidebar_img_container.fadeOut('fast', function() {
                            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                            $sidebar_img_container.fadeIn('fast');
                        });
                    }

                    if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                        var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                        $full_page_background.fadeOut('fast', function() {
                            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                            $full_page_background.fadeIn('fast');
                        });
                    }

                    if ($('.switch-sidebar-image input:checked').length == 0) {
                        var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                        var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                    }
                });

                $('.switch-sidebar-image input').change(function() {
                    $full_page_background = $('.full-page-background');

                    $input = $(this);

                    if ($input.is(':checked')) {
                        if ($sidebar_img_container.length != 0) {
                            $sidebar_img_container.fadeIn('fast');
                            $sidebar.attr('data-image', '#');
                        }

                        if ($full_page_background.length != 0) {
                            $full_page_background.fadeIn('fast');
                            $full_page.attr('data-image', '#');
                        }

                        background_image = true;
                    } else {
                        if ($sidebar_img_container.length != 0) {
                            $sidebar.removeAttr('data-image');
                            $sidebar_img_container.fadeOut('fast');
                        }

                        if ($full_page_background.length != 0) {
                            $full_page.removeAttr('data-image', '#');
                            $full_page_background.fadeOut('fast');
                        }

                        background_image = false;
                    }
                });

                $('.switch-sidebar-mini input').change(function() {
                    $body = $('body');

                    $input = $(this);

                    if (md.misc.sidebar_mini_active == true) {
                        $('body').removeClass('sidebar-mini');
                        md.misc.sidebar_mini_active = false;

                        //$('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                    } else {

                        //$('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                        setTimeout(function() {
                            $('body').addClass('sidebar-mini');

                            md.misc.sidebar_mini_active = true;
                        }, 300);
                    }

                    // we simulate the window Resize so the charts will get updated in realtime.
                    var simulateWindowResize = setInterval(function() {
                        window.dispatchEvent(new Event('resize'));
                    }, 180);

                    // we stop the simulation of Window Resize after the animations are completed
                    setTimeout(function() {
                        clearInterval(simulateWindowResize);
                    }, 1000);
                });

                // initialise Datetimepicker and Sliders
                md.initFormExtendedDatetimepickers();
                if ($('.slider').length != 0) {
                    md.initSliders();
                }

            });

            @if (Session::has('message'))
                md.showNotification('top', 'center', "@Lang(Session::get('message'))", 'success');
            @endif
            @if (Session::has('error'))
                md.showNotification('top', 'center', "@Lang(Session::get('error'))", 'danger');
            @endif

            /* COOKIES */
            function createCookie(name, value, days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    var expires = "; expires=" + date.toGMTString();
                }
                else var expires = "";               

                document.cookie = name + "=" + value + expires + "; path=/";
            }

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            }

            function eraseCookie(name) {
                createCookie(name, "", -1);
            }
            /* end COOKIES */

            //createCookie("minimizeSidebar", true);
            if ( readCookie("minimizeSidebar") == 'true' ) {
                $('#minimizeSidebar').trigger('click');
            }
            $('#minimizeSidebar').on('click', function() {
                var is_minimized = $('body').hasClass('sidebar-mini');
                createCookie("minimizeSidebar", is_minimized, 90);
            });

            /* SIDEBAR SCROLL TO ACTIVE MENU */
            if ( $(".nav-item.active").length > 0 ) {
                $('.sidebar-wrapper').animate({
                    scrollTop: $(".nav-item.active").offset().top
                }, 1000);
            }

        });

        $('.js-dynamic-permission').on('change', function () {
            var url = $(this).data('url');
            var status = $(this).is(':checked') ? 1 : 0;

            $.ajax({
                url: url,
                method: 'POST',
                data: {'_token': '{{ csrf_token() }}', status: status},
                success: function (data) {
                    md.showNotification('top', 'center', "@lang('Permiso actualizado correctamente')", 'success');
                }
            });
        });
    </script>
    @yield('scripts')
</body>

</html>