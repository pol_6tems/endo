@extends('Admin::layouts.admin')

@section('section-title')
    @Lang('Dashboard')
@endsection

@section('content')
@if (!empty($widgets))
    @foreach ($widgets as $w)
    <div class="card mt-5">
        <div class="card-header card-header-{{ $w->icon_class }} card-header-icon">
            <div class="card-icon">
                <i class="material-icons">{{ $w->icon }}</i>
            </div>
            <h4 class="card-title">{{ $w->title }}</h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <div class="table-responsive table-sales">
                    <table class="table">
                        <thead>
                            <tr>
                            @foreach( $w->headers as $h )
                                <td><strong>{{ $h }}</strong></td>
                            @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @forelse( $w->rows as $r )
                                <tr>
                                @foreach( $r as $f )
                                    <td>{{ $f }}</td>
                                @endforeach
                                </tr>
                            @empty
                                <tr><td colspan="{{ count($w->headers) }}">@Lang('No entries found.')</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@endif
@endsection