<header>
    <div class="row">
        <div class="side-menu">
            <div class="slide-menu-control"> <span></span> <span></span> <span></span></div>
            <div class="side-bar">
                <div class="btn slide-menu-control" data-action="close"><img src="{{ asset($_front . 'images/salir-aero-close.svg') }}" alt=""></div>
                <div class="profile-det mob-profile"> <img class="avatar-img-big" src="{{ $user->get_avatar_url() }}" alt="">
                    <h1>{{ $user->name }} @if($user->lastname){{ $user->lastname[0] . '.' }}@endif</h1>
                    <p>@if ($isPartner && $user->get_field('num-socio'))@lang('Socio no :num', ['num' => $user->get_field('num-socio')])@endif</p>
                </div>
                <div class="logo white"><a href="{{ route('index') }}"><img src="{{ asset($_front . 'images/logo-aravell-white.svg') }}" alt=""></a></div>
                <div class="nav">
                    <ul>
                        <li class="green-fee"><a href="{{ route('green-fee.create') }}">@lang('Green Fee')</a></li>
                        {{--<li class="camp-de"><a href="{{ route('practicas.create') }}">@lang('Campo de prácticas')</a></li>--}}
                        <li class="restaurante"><a href="{{ route('restaurant.create') }}">@lang('Restaurante')</a></li>
                        @if ($isPartner)<li class="wallet"><a href="{{ route('user.wallet') }}">@lang('Mi wallet')</a></li>@endif
                        <li class="mi-rese"><a href="{{ route('reserves.index') }}">@lang('Mis reservas')</a></li>
                        <li class="noti"><a href="{{ route('communication') }}">@lang('Notificaciones')</a></li>
                        <li class="comuni"><a href="{{ route('communication.chat') }}">@lang('Comunicación')</a></li>
                        <li class="mi-dato">
                            <div class="accordion_example1">
                                <!-- Section 1 -->
                                <div class="accordion_in">
                                    <div class="acc_head">@lang('Mis datos')</div>
                                    <div class="acc_content">
                                        <ul>
                                            <li class="dato-de"><a href="{{ route('user.edit') }}">@lang('Datos de usuario')</a></li>
                                            <li class="privacy"><a href="{{ route('user.privacity') }}">@lang('Privacidad')</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="Salir"> <a href="javascript:void(0);" onclick="$('#logout-form').submit();">@lang('Salir')</a> </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="logo black"> <a href="{{ route('index') }}"><img src="{{ asset($_front . 'images/logo-aravell-black.svg') }}" alt=""></a> </div>
        <div class="profile">
            <div class="profile-det"> <img class="avatar-img" src="{{ $user->get_avatar_url() }}" alt="">
                <h1>{{ $user->name }} @if($user->lastname){{ $user->lastname[0] . '.' }}@endif</h1>
                <p>@if ($isPartner && $user->get_field('num-socio'))@lang('Socio no :num', ['num' => $user->get_field('num-socio')])@endif</p>
            </div>
            <div class="profile-not-mob"> <a href="{{ route('communication') }}"><img src="{{ asset($_front . 'images/header-notification-mob.svg') }}" alt=""></a> </div>
            <div class="profile-not"> <a href="{{ route('communication') }}"><img src="{{ asset($_front . 'images/header-notification-active.svg') }}" alt=""></a> </div>
        </div>
    </div>
</header>

<form id="logout-form" action="{{ route('logout') }}" method="POST">
    @csrf
</form>