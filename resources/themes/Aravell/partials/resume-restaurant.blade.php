<li>@lang('Fecha') <span>{{ ucfirst(\Jenssegers\Date\Date::createFromFormat('d/m/Y', $reserve->get_field('data'))->format('l d/m/Y')) }}</span></li>
<li>@lang('Hora') <span>{{ $reserve->get_field('hora') }}</span></li>
<li>@lang('Núm. de adultos')<span>{{ $reserve->get_field('adults') }}</span></li>
@if ($reserve->get_field('nens'))
    <li>@lang('Núm. de niños')<span>{{ $reserve->get_field('nens') }}</span></li>
@endif
@if ($reserve->get_field('cadiretes-nens'))
    <li>@lang('Núm. de sillitas para niños')<span>{{ $reserve->get_field('cadiretes-nens') }}</span></li>
@endif

<li>@lang('Algun acompañante tiene intolerancias a<br> determinados alimentos')<span>{{ $reserve->get_field('intolerancies') ? __('Sí') : __('No') }}</span></li>
<li>@lang('Algun acompañante tiene movilidad<br> reducida')<span>{{ $reserve->get_field('mobilitat-reduida') ? __('Sí') : __('No') }}</span></li>