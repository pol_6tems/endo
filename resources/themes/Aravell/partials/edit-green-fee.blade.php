<ul>
    <li>
        <label>@lang('Fecha')</label>
        <div class="frm-input">
            <div class="date-picker">
                <div class="input">
                    <div class="result"><span>{{ $reserve ? $reserve->get_field('data') : old('data', \Carbon\Carbon::tomorrow()->format('d/m/Y')) }}</span></div>
                </div>
                <div class="calendar"></div>
            </div>
        </div>
    </li>
    <li>
        <label>@lang('Hora de salida')</label>
        <div class="frm-input-ctrl">
            <div class="frm-select">
                <select class="select_box demo-y" name="hora-sortida">
                    @foreach($horas_salida as $hora_salida)
                        <option value="{{ $hora_salida->get_field('hora') }}" @if (($reserve && $reserve->get_field('hora-sortida') == $hora_salida->get_field('hora')) || $hora_salida->get_field('hora') == old('hora-sortida')) SELECTED @endif>{{ $hora_salida->get_field('hora') }}</option>
                    @endforeach
                </select>
                <span class="calendar-advice js-calendar-advice" style="display: none">
                    @lang('Fecha/Hora de salida no disponible')
                </span>
            </div>
        </div>
    </li>
    <li>
        <label>@lang('Número jugadores')</label>
        <div class="numero"> <span>@lang('Jugadores')</span>
            <div class="qty-available">
                <div id='myform'>
                    <input type='button' value='-' class='qtyminuss' data-min="1" field='quantity' />
                    <input type='text' name='quantity' value='{{ $reserve ? count($reserve->get_field('jugadors')) : 1 }}' class='qty' />
                    <input type='button' value='+' class='qtypluss' data-min="1" field='quantity' />
                </div>
            </div>
        </div>
    </li>
    <li class="js-players">
        <label>@lang('Datos de los jugadores')</label>
        <div class="datos player">
            <input class="js-input-player-price" type="hidden" name="jugadors[0][preu]" data-type="{{ $reserve && count($reserve->get_field('jugadors')) ? $reserve->get_field('jugadors')[0]['type']['value'] : $type_logged_player }}" value="{{ $reserve && count($reserve->get_field('jugadors')) ? $reserve->get_field('jugadors')[0]['preu']['value'] : $price_logged_player }}">
            <h3>@lang('JUGADOR') <span class="js-player-pos player-pos">{{ $reserve ? count($reserve->get_field('jugadors')) : 1 }}</span></h3>
            <ul>
                <li>@lang('Nombre')<input class="js-input-name" type="text" name="jugadors[0][nom]" placeholder="@lang('Nombre del jugador')" value="{{ $reserve && count($reserve->get_field('jugadors')) ? $reserve->get_field('jugadors')[0]['nom']['value'] : auth()->user()->name }}"></li>
                <li>@lang('Apellido')<input class="js-input-lastname" type="text" name="jugadors[0][cognom]" placeholder="@lang('Apellido del jugador')" value="{{ $reserve && count($reserve->get_field('jugadors')) ? $reserve->get_field('jugadors')[0]['cognom']['value'] : auth()->user()->lastname }}"></li>
                <li>@lang('Licencia')<input class="js-input-licence" type="text" name="jugadors[0][llicencia]" placeholder="@lang('Licencia del jugador')" value="{{ $reserve  && count($reserve->get_field('jugadors')) ? $reserve->get_field('jugadors')[0]['llicencia']['value'] : auth()->user()->get_field('llicencia') }}"></li>
            </ul>
        </div>
        @if ($reserve)
            @foreach($reserve->get_field('jugadors') as $key => $jugador)
                @if ($key > 0)
                    <div class="datos player">
                        <input class="js-input-player-price" type="hidden" name="jugadors[{{ $key }}][preu]" data-type="{{ $jugador['type']['value'] }}" value="{{ $jugador['preu']['value'] }}">
                        <h3>@lang('JUGADOR') <span class="js-player-pos player-pos">{{ $key + 1 }}</span> <span class="close js-remove-player"><img src="{{ asset($_front . 'images/close.svg') }}" alt=""></span></h3>
                        <ul>
                            <li>@lang('Nombre')<input class="js-input-name" type="text" name="jugadors[{{ $key }}][nom]" placeholder="@lang('Nombre del jugador')" value="{{ $jugador['nom']['value'] }}"></li>
                            <li>@lang('Apellido')<input class="js-input-lastname" type="text" name="jugadors[{{ $key }}][cognom]" placeholder="@lang('Apellido del jugador')" value="{{ $jugador['cognom']['value'] }}"></li>
                            <li>@lang('Licencia')<input class="js-input-licence" type="text" name="jugadors[{{ $key }}][llicencia]" placeholder="@lang('Licencia del jugador')" value="{{ $jugador['llicencia']['value'] }}"></li>
                        </ul>
                    </div>
                @endif
            @endforeach
        @endif
    </li>
    <li>
        <h4>@lang('PRECIO SALIDA') <span>&nbsp;€</span><strong class="js-preu-price">{{ $reserve ? $reserve->get_field('preu') : old('preu', $price_logged_player) }}</strong></h4>
    </li>
    <li>
        <label>@lang('Extras')</label>
        @foreach($extras as $key => $extra)
            <div class="numero"> <span>{{ $extra->title }} ({{ format_money($extra->get_field('preu')) }}€)</span>
                <div class="qty-available">
                    <div id='myform'>
                        <input type='button' value='-' class='qtyminuss' data-min="0" field='extra{{ $extra->id }}' />
                        <input type='text' name='extras[{{ $extra->id }}]' value='{{ $reserve && isset($reserve_extras[$extra->id]) ? $reserve_extras[$extra->id] : 0 }}' class='qty js-extra-input extra{{ $extra->id }}' data-unit-price="{{ $extra->get_field('preu') }}" />
                        <input type='button' value='+' class='qtypluss' data-min="0" field='extra{{ $extra->id }}' />
                    </div>
                </div>
            </div>
        @endforeach
    </li>
    <li>
        <h4>@lang('TOTAL') <span>&nbsp;€</span><strong class="js-total-price">{{ $reserve ? $reserve_total : old('preu', $price_logged_player) }}</strong></h4>
    </li>
    <li>
        <input type="hidden" name="data" value="{{ $reserve ? $reserve->get_field('data') : old('data', \Carbon\Carbon::tomorrow()->format('d/m/Y')) }}">
        <input type="hidden" name="preu" value="{{ $reserve ? $reserve->get_field('preu') : old('preu', $price_logged_player) }}">
        <input type="hidden" name="total" value="{{ $reserve ? $reserve_total : old('preu', $price_logged_player) }}">


        <input type="submit" value="@lang('RESERVAR')">
    </li>
</ul>