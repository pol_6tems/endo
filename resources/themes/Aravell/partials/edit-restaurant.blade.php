<div id="tab1" class="inner-form nom green-fee-cont tab-content tab-open">
    <h2>@lang('¿Cuándo quieres venir?')</h2>
    <ul>
        <li>
            <label>@lang('Fecha')</label>
            <div class="frm-input">
                <div class="date-picker">
                    <div class="input">
                        <div class="result"><span>{{ $reserve ? $reserve->get_field('data') : old('data', \Carbon\Carbon::tomorrow()->format('d/m/Y')) }}</span></div>
                    </div>
                    <div class="calendar"></div>
                </div>
            </div>
        </li>
        <li>
            <label>@lang('Hora')</label>
            <div class="frm-input-ctrl">
                <div class="frm-select">
                    <select class="select_box" name="hora">
                        @foreach($horas_restaurant as $hora_restaurant)
                            <option value="{{ $hora_restaurant->get_field('hora') }}" @if (($reserve && $reserve->get_field('hora') == $hora_restaurant->get_field('hora')) || $hora_restaurant->get_field('hora') == old('hora-sortida')) SELECTED @endif>{{ $hora_restaurant->get_field('hora') }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </li>
        <li>
            <label>@lang('Comensales')</label>
            <div class="numero"> <span>@lang('Adultos')</span>
                <div class="qty-available">
                    <div id='myform'>
                        <input type='button' value='-' class='qtyminuss' data-min="1" field='adults' />
                        <input type='text' name='adults' value='{{ $reserve ? $reserve->get_field('adults') : 1 }}' class='qty' />
                        <input type='button' value='+' class='qtypluss' data-min="1" field='adults' />
                    </div>
                </div>
            </div>
            <div class="numero"> <span>@lang('Niños')</span>
                <div class="qty-available">
                    <div id='myform'>
                        <input type='button' value='-' class='qtyminuss' data-min="0" field='nens' />
                        <input type='text' name='nens' value='{{ $reserve ? $reserve->get_field('nens') : 0 }}' class='qty' />
                        <input type='button' value='+' class='qtypluss' data-min="0" field='nens' />
                    </div>
                </div>
            </div>
            <div class="numero"> <span>@lang('Sillitas para niños')</span>
                <div class="qty-available">
                    <div id='myform'>
                        <input type='button' value='-' class='qtyminuss' data-min="0" field='cadiretes-nens' />
                        <input type='text' name='cadiretes-nens' value='{{ $reserve ? $reserve->get_field('cadiretes-nens') : 0 }}' class='qty' />
                        <input type='button' value='+' class='qtypluss' data-min="0" field='cadiretes-nens' />
                    </div>
                </div>
            </div>
        </li>
        <li class="chk">
            <input id="options" type="checkbox" name="intolerancies" value="options" {{ $reserve && $reserve->get_field('intolerancies') ? 'CHECKED' : '' }}>
            <label for="options"><span><span></span></span><strong>@lang('Algun acompañante tiene intolerancias a<br> determinados alimentos')</strong></label>
        </li>
        <li class="chk">
            <input id="options1" type="checkbox" name="mobilitat-reduida" value="options" {{ $reserve && $reserve->get_field('mobilitat-reduida') ? 'CHECKED' : '' }}>
            <label for="options1"><span><span></span></span><strong>@lang('Algun acompañante tiene movilidad reducida')</strong></label>
        </li>
        <li>
            <input type="hidden" name="data" value="{{ $reserve ? $reserve->get_field('data') : old('data', \Carbon\Carbon::tomorrow()->format('d/m/Y')) }}">

            <input type="submit" name="" value="@lang(isset($submitBtn) ? $submitBtn : 'PRERESERVAR')" onClick="fnTab('1');">
        </li>
    </ul>
</div>