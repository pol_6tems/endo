<li>@lang('Fecha') <span>{{ ucfirst(\Jenssegers\Date\Date::createFromFormat('d/m/Y', $reserve->get_field('data'))->format('l d/m/Y')) }}</span></li>
<li>@lang('Hora de salida') <span>{{ $reserve->get_field('hora-sortida') }}</span></li>
<li>@lang('Núm. de jugadores')<span>{{ count($reserve->get_field('jugadors')) }}</span></li>
@foreach($reserve->get_field('jugadors') as $key => $jugador)
    <li class="full-txt">@lang('Jugador') {{ $key + 1 }}</li>
    <li class="lft">@lang('Nombre')<span>{{ $jugador['nom']['value'] }}</span></li>
    <li class="lft">@lang('Apellido')<span>{{ $jugador['cognom']['value'] }}</span></li>
    <li class="lft">@lang('Licencia')<span>{{ $jugador['llicencia']['value'] }}</span></li>
@endforeach
@php ($extras = $reserve->get_field('extres'))
@if ($extras && count($extras))
    <li class="full-txt">@lang('Extras')</li>
    @foreach($extras as $extra)
        <li class="lft">{{ $extra['extra']['value']->title }}<span>{{ $extra['quantitat']['value'] }}</span></li>
    @endforeach
@endif
<li class="tot">@lang('TOTAL') <strong>{{ $reserve_total }} €</strong></li>