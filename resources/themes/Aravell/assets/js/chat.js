(function(window, $) {
    var AravellChat = function () {
        this.init();
    };

    AravellChat.prototype = {
        init: function () {
            this.bindings();

            $.mCustomScrollbar.defaults.theme = "light-2"; //set "light-2" as the default theme

            var demoY = $(".demo-y");
            demoY.mCustomScrollbar();
            demoY.mCustomScrollbar("scrollTo","bottom", {
                scrollInertia: 0
            });
        },

        bindings: function () {
            var me = this;

            $('.js-chat-send').on('click', function () {
                var message = $('.js-chat-message').val();

                if (message === '') {
                    toastr.warning($(this).data('empty-msg'));
                    return;
                }

                var chatId = $(this).data('chat-id');
                var chatType = $(this).data('chat-type');
                var url = $(this).data('url');

                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    data: {chat_id: chatId, chat_type: chatType, message:message},
                    success: me.messageSent,
                    error: function () {
                        toastr.error($('.js-chat-send').data('error-msg'));
                    }
                });
            });

            $('.js-chat-message').keyup(function(e) {
                if (e.keyCode === 13 && $(this).val() !== '') {
                    $('.js-chat-send').click();
                }
            });
        },

        messageSent: function (data) {
            var sendButton = $('.js-chat-send');

            if (data.status !== 'OK') {
                toastr.error(sendButton.data('error-msg'));
                return;
            }

            var chatInput = $('.js-chat-message');
            var chatList = $('.js-chat-list');
            var message = chatInput.val();

            $('.js-chat-today').show();
            var now = new Date();
            var time = now.toLocaleString('es-ES', { hour: '2-digit', minute: '2-digit', hour12: false });

            var missatge = `<li class="sender">\n` +
                            `<p>${message}</p>\n` +
                            `<span>${time}</span>\n` +
                        `</li>`;

            chatList.append(missatge);
            chatList.animate({ scrollTop: chatList.prop("scrollHeight") }, 300);
            chatInput.val('');
            $(".demo-y").mCustomScrollbar("scrollTo", "bottom", {
                scrollInertia: 60
            });
        }
    };

    if (typeof self !== 'undefined') {
        self.AravellChat = AravellChat;
    }

    // Expose as a CJS module
    if (typeof exports === 'object') {
        module.exports = AravellChat;
    }
})(window, $);

var chatButton =  $('.js-chat-send').first();
if (typeof chatButton !== 'undefined' && chatButton !== null && chatButton.length > 0) {
    new AravellChat();
}