$(document).ready(function () {
	// Sticky Header
	/*$(".accordion_example1").smk_Accordion({
		closeAble: true, //boolean
	});*/

	if (typeof availabilityUrl !== 'undefined') {
		checkCalendarAvailability();
	}

	if (typeof allAvailabilityUrl !== 'undefined') {
		getAllHoursAvailability();
	}
});

// Mmenu - Mobile Menu
/*$('#mobNav').mmenu();*/

$(".select_box").selectbox();

var login = $('.login-bg');

if (typeof login !== "undefined") {
	function setHeight() {
		var windowHeight = $(window).innerHeight();
		login.css('min-height', windowHeight);
	}

	$(document).ready(function() {
		setHeight();
		$(window).resize(function() {
			setHeight();
		});
	});

	$('#horizontalTab').easyResponsiveTabs({
		type: 'default', //Types: default, vertical, accordion
		width: 'auto', //auto or any width like 600px
		fit: true,   // 100% fit in a container
		closed: 'accordion', // Start closed if in accordion view
		activate: function() { // Callback function if tab is switched
			var $tab = $(this);
			var $info = $('#tabInfo');
			var $name = $('span', $info);
			$name.text($tab.text());
			$info.show();
		}
	});

	$(".close").click(function(){
		$(this).closest(".lg-error").hide();
	});
} else {
	$(".close").click(function(){
		$(this).parent().parent().hide();
	});
}

$(".accordion_example1").smk_Accordion({
	closeAble: true, //boolean
});


$('.side-menu').removeClass("openmenu");
$(".slide-menu-control").click(function() {
	var sideMenu = $('.side-menu');

	if(sideMenu.hasClass("openmenu")) {
		sideMenu.removeClass("openmenu");
	} else {
		sideMenu.addClass("openmenu");
	}
});

$('.qtyplus, .qtypluss').click(function(e){
	// Stop acting like a button
	e.preventDefault();
	// Get the field name
	var fieldName = $(this).attr('field');
	var min = $(this).data('min');
	var fnInput = $('input[name='+fieldName+']');

	if (!fnInput.length) {
		fnInput = $('.' + fieldName);
	}

	// Get its current value
	var currentVal = parseInt(fnInput.val());
	// If is not undefined
	if (!isNaN(currentVal)) {
		// Increment
		fnInput.val(currentVal + 1);
	} else {
		// Otherwise put a 0 there
		fnInput.val(min);
	}

	if ($(this).hasClass('qtypluss')) {
		if (fieldName === 'quantity') {
			addPlayer();
		}
		updatePlayerNumbers();
		updateTotal();
	}
});
// This button will decrement the value till 0
$(".qtyminus, .qtyminuss").click(function(e) {
	// Stop acting like a button
	e.preventDefault();
	// Get the field name
	var fieldName = $(this).attr('field');
	var min = $(this).data('min');
	var fnInput = $('input[name='+fieldName+']');

	if (!fnInput.length) {
		fnInput = $('.' + fieldName);
	}

	// Get its current value
	var currentVal = parseInt(fnInput.val());
	// If it isn't undefined or its greater than 0
	if (!isNaN(currentVal) && currentVal > min) {
		// Decrement one
		fnInput.val(currentVal - 1);
	} else {
		// Otherwise put a 0 there
		fnInput.val(min);
	}

	if ($(this).hasClass('qtyminuss') && currentVal > min) {
		if (fieldName === 'quantity') {
			removePlayer();
		}
		updatePlayerNumbers();
		updateTotal();
	}
});

$(document).on('click', '.js-remove-player', function () {
	var quantityInput = $('input[name="quantity"]');
	quantityInput.val(parseInt(quantityInput.val()) - 1);

	$(this).closest('.player').remove();
	updatePlayerNumbers();
	updateTotal();
});

$("#anadir").click(function(){
	$(".anadir").slideToggle();
});

$('.js-wallet-add-input').on('change', function () {
	$('.js-wallet-add-title').html($(this).val());
});

$(function() {
	var cal = $(".calendar");
	cal.datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		minDate: 0,
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		dayNamesMin: ['D .', 'L .', 'M .', 'MI .', 'J .', 'V .', 'S .'],
		defaultDate: cal.parents('.date-picker').find('.result').children('span').html()
	}).on('change', function () {
		$('.date-picker').removeClass('open');
	});

	$(document).on('click', '.date-picker .input', function () {
		var $me = $(this),
			$parent = $me.parents('.date-picker');
		$parent.toggleClass('open');
	});

	cal.on("change", function () {
		var $me = $(this),
			$selected = $me.val(),
			$parent = $me.parents('.date-picker');
		$parent.find('.result').children('span').html($selected);
		$('input[name="data"]').val($selected);

		if (typeof availabilityUrl !== 'undefined') {
			checkCalendarAvailability();
		}

		if (typeof allAvailabilityUrl !== 'undefined') {
			getAllHoursAvailability();
		}
	});
});

function updateTotal() {
	var total = parseFloat($('input[name="preu"]').val());

	$('.js-extra-input').each(function () {
		total = total + (parseFloat($(this).val()) * parseFloat($(this).data('unit-price')));
	});

	$('.js-total-price').html(total);
	$('input[name="total"]').val(total);
}


function addPlayer() {
	var position = $('.player').length;

	var html = '<div class="datos player">\n' +
		'            <input class="js-input-player-price" type="hidden" data-type="' + normalType +'" name="jugadors[' + position + '][preu]" value="' + normalPrice + '">\n' +
		'            <h3>' + playerTitle + ' <span class="js-player-pos player-pos">' + (position + 1) + '</span> <span class="close js-remove-player"><img src="' + closeAsset + '" alt=""></span></h3>\n' +
		'            <ul>\n' +
		'                <li>' + playerName + '<input class="js-input-name" type="text" name="jugadors[' + position + '][nom]" placeholder="' + playerNamePlaceholder + '"></li>\n' +
		'                <li>' + playerLastname + '<input class="js-input-lastname" type="text" name="jugadors[' + position + '][cognom]" placeholder="' + playerLastnamePlaceholder + '" value=""></li>\n' +
		'                <li>' + playerLicence + '<input class="js-input-licence" type="text" name="jugadors[' + position + '][llicencia]" placeholder="' + playerLicencePlaceholder + '" value=""></li>\n' +
		'            </ul>\n' +
		'        </div>';

	$('.js-players').append(html);
}


function removePlayer() {
	$('.js-players .player:last').remove();
}


function updatePlayerNumbers() {
	var players = $('.player');
	var preu = 0;
	players.each(function (index) {
		preu = preu + parseFloat($(this).find('.js-input-player-price').val());
		$(this).find('.js-player-pos').html(index + 1);
		$(this).find('.js-input-name').attr('name', 'jugadors[' + index + '][nom]');
		$(this).find('.js-input-lastname').attr('name', 'jugadors[' + index + '][cognom]');
		$(this).find('.js-input-licence').attr('name', 'jugadors[' + index + '][llicencia]');
	});

	$('input[name="preu"]').val(preu);
	$('.js-preu-price').html(preu);
}

$(document).on('change', '.js-input-licence', function () {
	var me = $(this);
	var license = $(this).val();

	$.ajax({
		url: licenceUrl,
		method: 'GET',
		dataType: 'json',
		data: {license: license},
		success: function (data) {
			var playerInput = me.closest('.player').find('.js-input-player-price');
			playerInput.val(data.price);
			playerInput.attr('data-type', data.type);

			checkCalendarAvailability();

			getAllHoursAvailability();
		}
	});
});

var horaInput = $('select[name="hora-sortida"]');

horaInput.on('change', function () {
	if (typeof availabilityUrl !== 'undefined') {
		checkCalendarAvailability();
	}
});

function checkCalendarAvailability() {
	$('.js-calendar-advice').hide();

	var hour = horaInput.val();
	var date = $('input[name="data"]').val();

	$.ajax({
		url: availabilityUrl,
		method: 'GET',
		dataType: 'json',
		data: {hour: hour, date: date, reserve_id: reserveId},
		success: function (data) {
			if (!data.available) {
				$('.js-calendar-advice').show();
			} else {
				$.each($.parseJSON(data.prices), function (index, price) {
					$('.js-input-player-price[data-type="' + price.post_id +'"').val(price.price);

					if (typeof price.category_name !== 'undefined' && price.category_name == 'publico') {
						normalPrice = price.price;
					}
				});

				updatePlayerNumbers();
				updateTotal();
			}
		}
	});
}


function getAllHoursAvailability() {
	var date = $('input[name="data"]').val();

	$.ajax({
		url: allAvailabilityUrl,
		method: 'GET',
		dataType: 'json',
		data: {date: date, reserve_id: reserveId},
		success: function (data) {
			if (data.availabilities) {
				$.each(data.availabilities, function (index, availability) {

					if (availability.available) {
						$('a[rel="' + availability.hour + '"]').removeClass('unavailable');
					} else {
						$('a[rel="' + availability.hour + '"]').addClass('unavailable');
					}
				});
			}
		}
	});
}

$(document).on('click','.reserva li', function(e) {
	//e.preventDefault();
	$(".ver-buton").hide();
	$(this).find(".ver-buton").show();
});