@extends('Front::layouts.main')

@section('page_header', __('Green Fee'))

@section('content')
    <div class="inner-form">
        <h2>@lang('Modifica tu reserva')</h2>

        <form method="POST" action="{{ route($type . '.update', ['id' => $reserve->id]) }}" >
            @csrf
            <input type="hidden" name="_method" value="PUT">

            <h3 style="color: red">TODO: Update amb preu diferent</h3>
            @include('Front::partials.edit-green-fee', ['reserve' => $reserve])
        </form>
    </div>
@endsection

@section('bottom_foot_scripts')
    <script>
        var playerTitle = '@lang('JUGADOR')';
        var playerName = '@lang('Nombre')';
        var playerNamePlaceholder = '@lang('Nombre del jugador')';
        var playerLastname = '@lang('Apellido')';
        var playerLastnamePlaceholder = '@lang('Apellido del jugador')';
        var playerLicence = '@lang('Licencia')';
        var playerLicencePlaceholder = '@lang('Licencia del jugador')';
        var closeAsset= '{{ asset($_front . 'images/close.svg') }}';
        var normalPrice = {{ $normal_price }};
        var normalType = {{ $type_normal_price }};
        var licenceUrl = '{{ route('reserves.player-price') }}';
        var availabilityUrl = '{{ route('green-fee.availability') }}';
        var allAvailabilityUrl = '{{ route('green-fee.all-availability') }}';
        var reserveId = {{ $reserve->id }};
    </script>
@endsection