@extends('Front::layouts.main')

@section('page_header', __('Green Fee'))

@section('content')
    <div class="green-fee">
        <ul>
            <li class="active" id="green1"><span>1</span>
                <h3>@lang('Seleccionar salida')</h3>
            </li>
            <li id="green2"><span>2</span>
                <h3>@lang('Resumen salida')</h3>
            </li>
            <li id="green3"><span>3</span>
                <h3>@lang('Pago')</h3>
            </li>
            <li id="green4"><span>4</span>
                <h3>@lang('Reserva Realizada')</h3>
            </li>
        </ul>
    </div>
    <!--tab-1 start-->
    <form method="POST" action="{{ route($type . '.store') }}" >
        @csrf

        <div id="tab1" class="inner-form nom green-fee-cont tab-content tab-open">
            <h2>@lang('Selecciona tu salida')</h2>
            @include('Front::partials.edit-green-fee', ['reserve' => null])
        </div>
    </form>
    <!--tab-1 end-->

@endsection

@section('bottom_foot_scripts')
    <script>
        var playerTitle = '@lang('JUGADOR')';
        var playerName = '@lang('Nombre')';
        var playerNamePlaceholder = '@lang('Nombre del jugador')';
        var playerLastname = '@lang('Apellido')';
        var playerLastnamePlaceholder = '@lang('Apellido del jugador')';
        var playerLicence = '@lang('Licencia')';
        var playerLicencePlaceholder = '@lang('Licencia del jugador')';
        var closeAsset = '{{ asset($_front . 'images/close.svg') }}';
        var normalPrice = {{ $normal_price }};
        var normalType = {{ $type_normal_price }};
        var licenceUrl = '{{ route('reserves.player-price') }}';
        var availabilityUrl = '{{ route('green-fee.availability') }}';
        var allAvailabilityUrl = '{{ route('green-fee.all-availability') }}';
        var reserveId = 0;
    </script>
@endsection