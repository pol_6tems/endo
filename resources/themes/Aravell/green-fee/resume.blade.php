@extends('Front::layouts.main')

@section('page_header', __('Green Fee'))

@section('content')
    <div class="green-fee">
        <ul>
            <li class="active" id="green1" onclick="window.location = '{{ route('green-fee.create') }}'"><span>1</span>
                <h3>@lang('Seleccionar salida')</h3>
            </li>
            <li class="active" id="green2"><span>2</span>
                <h3>@lang('Resumen salida')</h3>
            </li>
            <li id="green3"><span>3</span>
                <h3>@lang('Pago')</h3>
            </li>
            <li id="green4"><span>4</span>
                <h3>@lang('Reserva Realizada')</h3>
            </li>
        </ul>
    </div>

    <!--tab-2 start-->
    <div class="inner-form tab-content tab-open">
        <h2>@lang('Resumen Green Fee')</h2>
        <ul class="fecha resumen">
            @include('Front::partials.resume-green-fee', ['reserve' => $reserve])

            <li class="reser">
                <form method="POST" action="{{ route($type . '.pay') }}" >
                    @csrf
                    <input type="hidden" name="reserve_id" value="{{ $reserve->id }}">
                    <input type="submit" value="@lang('RESERVAR')">
                </form>
            </li>
        </ul>
    </div>
    <!--tab-2 end-->
@endsection