@extends('Front::layouts.main')

@section('page_header', __('Restaurante'))

@section('content')
    <div class="inner-form">
        <h2>@lang('Modificación realizada')</h2>
        <ul class="fecha resumen">
            @include('Front::partials.resume-green-fee', ['reserve' => $reserve])

            <div class="full-sec">
                <p>@lang('Para contactar directamente con Aravell Golf llame al (+34) <span>973 36 00 66</span>')</p>
            </div>
            <div class="inner-form volver">
                <ul class="fecha resumen">
                    <li class="reser">
                        <input type="button" onclick="window.location = '{{ route('index') }}'" value="@lang('VOLVER')">
                    </li>
                </ul>
            </div>
        </ul>
    </div>
@endsection