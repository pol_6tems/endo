@extends('Front::layouts.main')

@section('page_header', __('Green Fee'))

@section('content')
    <div class="green-fee">
        <form id="form-back-resume" method="post" action="{{ route('green-fee.store') }}">
            @csrf
            <input type="hidden" name="reserve_id" value="{{ $reserve->id }}">
        </form>
        <ul>
            <li class="active" id="green1" onclick="window.location = '{{ route('green-fee.create') }}'"><span>1</span>
                <h3>@lang('Seleccionar salida')</h3>
            </li>
            <li class="active" id="green2"  onclick="$('#form-back-resume').submit();"><span>2</span>
                <h3>@lang('Resumen salida')</h3>
            </li>
            <li class="active" id="green3"><span>3</span>
                <h3>@lang('Pago')</h3>
            </li>
            <li id="green4"><span>4</span>
                <h3>@lang('Reserva Realizada')</h3>
            </li>
        </ul>
    </div>

    <!--tab-3 start-->
    <div id="tab3" class="inner-form pago tab-content tab-open">
        <h3 style="color: red">TODO: Integració redsys + tarjetes guardades</h3>
        @if (auth()->user()->role != 'No soci')
            <h2>@lang('Selecciona la opción de pago')</h2>
            <h5><span>@lang('Pago con Wallet')</span></h5>
            <h1>@lang('Pagar :total con Wallet', [ 'total' => $reserve_total . '€'])</h1>
            <p>@lang('Saldo actual'): {{ $user->get_field('saldo') }}€</p>
            <ul class="fecha resumen">
                <li class="reser">
                    <form method="post" action="{{ route('green-fee.payPost') }}">
                        @csrf
                        <input type="hidden" name="reserve_id" value="{{ $reserve->id }}">
                        <input type="submit" value="@lang('PAGAR CON WALLET')">
                    </form>
                </li>
            </ul>
        @endif
        <h5><span>@lang('Pago con Targeta')</span></h5>
        <div class="credit">
            <ul>
                <li>
                    <input id="options1" type="checkbox" name="field1" value="options1">
                    <label for="options1"><span><span></span></span></label>
                </li>
                <li><img src="{{ asset($_front . 'images/mastercard.svg') }}" alt="" class="dk-tp"> <img src="{{ asset($_front . 'images/mastercard-mbl.svg') }}" alt="" class="mb-vw"> </li>
                <li>Mastercard</li>
                <li><span>****</span><span>****</span> <span>****</span><span>****</span> <span>2012</span></li>
            </ul>
        </div>
        <div class="clear"></div>
        <div class="credit">
            <ul>
                <li>
                    <input id="options2" type="checkbox" name="field2" value="options2">
                    <label for="options2"><span><span></span></span></label>
                </li>
                <li><img src="{{ asset($_front . 'images/visa.svg') }}" alt="" class="dk-tp"> <img src="{{ asset($_front . 'images/visa-mbl.svg') }}" alt="" class="mb-vw"> </li>
                <li>Visa</li>
                <li><span>****</span><span>****</span> <span>****</span><span>****</span> <span>5025</span></li>
            </ul>
        </div>
        <div class="clear"></div>
        <div class="credit">
            <ul>
                <li>
                    <input id="options3" type="checkbox" name="field3" value="options3">
                    <label for="options3"><span><span></span></span></label>
                </li>
                <li class="nueva">@lang('Nueva targeta')</li>
            </ul>
        </div>
        <div class="clear"></div>
        <ul class="fecha resumen">
            <li class="reser con">
                <input type="submit" value="@lang('PAGAR CON TARJETA')">
            </li>
        </ul>
    </div>
    <!--tab-3 end-->
@endsection 