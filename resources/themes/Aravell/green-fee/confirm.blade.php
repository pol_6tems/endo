@extends('Front::layouts.main')

@section('page_header', __('Green Fee'))

@section('content')
    <div class="green-fee">
        <ul>
            <li class="active" id="green1" onclick="window.location = '{{ route('green-fee.create') }}'"><span>1</span>
                <h3>@lang('Seleccionar salida')</h3>
            </li>
            <li class="active" id="green2"><span>2</span>
                <h3>@lang('Resumen salida')</h3>
            </li>
            <li class="active" id="green3"><span>3</span>
                <h3>@lang('Pago')</h3>
            </li>
            <li class="active" id="green4"><span>4</span>
                <h3>@lang('Reserva Realizada')</h3>
            </li>
        </ul>
    </div>

    <!--tab-4 start-->
    <div id="tab4" class="inner-form pago tab-content tab-open">
        <h2>@lang('Reserva realizada con éxito')</h2>
        <p class="horas">@lang('Se ha reservado su salida Green Fee<br> para el :date a las :hour horas', ['date' => $reserve->get_field('data'), 'hour' => $reserve->get_field('hora-sortida')])</p>
        <ul class="fecha resumen">
            <li class="reser">
                <input type="button" onclick="window.location = '{{ route('green-fee.show', ['id' => $reserve->id]) }}'" value="@lang('VER RESERVA')">
            </li>
        </ul>
    </div>
    <!--tab-4 end-->
@endsection