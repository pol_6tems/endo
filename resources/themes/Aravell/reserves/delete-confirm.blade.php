@extends('Front::layouts.main')

@section('page_header', __('Reservas'))

@section('content')
    <div class="soci-dades ana">
        <h2>@lang('Anulación realizada')</h2>
        <p>@lang('Su reserva #:reserve_id ha sido anulada correctamente.', ['reserve_id' => $reserve->title])
            @if ($reserve->get_field('preu-total') && !$hasWallet)
                <br> @lang('En breve realizaremos el abono de :amount<i class="fa fa-eur" aria-hidden="true"></i> a su cuenta.', ['amount' => $reserve->get_field('preu-total')])
            @endif
        </p>
    </div>
@endsection