@extends('Front::layouts.main')

@section('page_header', __('Reservas'))

@section('content')
    <div class="inner-form res">
        @if ($futureReserves->count())
            <ul class="reservas-lst">
                @foreach($futureReserves as $reserve)
                <li>
                    <a href="{{ route($reserve->route, ['id' => $reserve->id]) }}">
                        <div class="noti-img"> <img src="{{ asset($_front . 'images/' . $reserve->listImg) }}" alt=""> </div>
                        <div class="noti-cnt">
                            <h1>{{ $reserve->type_name }}</h1>
                            @php($reserveDate = \Carbon\Carbon::createFromFormat('d/m/Y', $reserve->get_field('data')))
                            <p>@lang('Reserva para :date a las :hour', ['date' => $reserveDate->format('d/m/y'), 'hour' => $reserve->get_field('hora')])</p>
                            <span>{{ $reserve->created_at->format('H:i') }}</span> </div>
                    </a> </li>
                @endforeach
            </ul>
        @endif

        @if ($pastReserves->count())
            <h3><span>@lang('Histórico')</span></h3>

            <ul class="reservas-lst">
                @foreach($pastReserves as $reserve)
                    <li>
                        <a href="{{ route($reserve->route, ['id' => $reserve->id]) }}">
                            <div class="noti-img"> <img src="{{ asset($_front . 'images/' . $reserve->listImg) }}" alt=""> </div>
                            <div class="noti-cnt">
                                <h1>{{ $reserve->type_name }}</h1>
                                @php($reserveDate = \Carbon\Carbon::createFromFormat('d/m/Y', $reserve->get_field('data')))
                                <p>@lang('Reserva para :date a las :hour', ['date' => $reserveDate->format('d/m/y'), 'hour' => $reserve->get_field('hora') ?: $reserve->get_field('hora-sortida')])</p>
                                <span>{{ $reserve->created_at->format('H:i') }}</span> </div>
                        </a> </li>
                @endforeach
            </ul>
        @endif

    </div>
@endsection