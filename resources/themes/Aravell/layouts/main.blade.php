@extends('Front::layouts.html')

@section('body')
    @include('Front::partials.header')

    <div class="container">
        <div class="row">
            @if (!route_name('index'))
                <div class="salir-aero"><a href="{{ route('index') }}"></a></div>
            @endif

            @hasSection('page_header')
                <h1 class="m-tit">@yield('page_header')</h1>
            @endif

            @yield('content')
        </div>
    </div>

    <div class="green-bottom">
        <div class="row">
            <ul>
                <li class="green-tea-ico"><a href="{{ route('green-fee.create') }}"><span></span>
                        <h4>@lang('Green Fee')</h4>
                    </a></li>
                <li class="restaurant-ico"><a href="{{ route('restaurant.create') }}"><span></span>
                        <h4>@lang('Restaurante')</h4>
                    </a></li>
                <li class="wallet-ico"><a href="{{ route('user.wallet') }}"><span></span>
                        <h4>@lang('Wallet')</h4>
                    </a></li>
                <li class="reservas-ico"><a href="{{ route('reserves.index') }}"><span></span>
                        <h4>@lang('Reservas')</h4>
                    </a></li>
            </ul>
        </div>
    </div>
@endsection