@extends('Front::layouts.html')

@section('body')
    <section class="login-bg">
        <div class="login-container">
            <div class="salir"><a href="javascript:void(0);">Salir</a></div>
            <h1><a href="{{ route('index') }}"><img src="{{ asset($_front.'images/logo-aravell-white.svg') }}" alt="" title=""></a></h1>

            @yield('content')
        </div>
    </section>
@endsection