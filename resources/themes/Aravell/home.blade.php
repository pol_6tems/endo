@extends('Front::layouts.main')

@section('page_header', __('Bienvenido'))

@section('content')

    <ul class="das-ico">
        <li> <a href="{{ route('green-fee.create') }}"> <img src="{{ asset($_front . 'images/home-icon-greenfee.svg') }}" alt="">
                <p>@lang('Green Fee')</p>
            </a> </li>
        <li> <a href="{{ route('restaurant.create') }}"> <img src="{{ asset($_front . 'images/home-icon-restaurant.svg') }}" alt="">
                <p>@lang('Restaurante')</p>
            </a> </li>
        @if ($walletTransactions)<li> <a href="{{ route('user.wallet') }}"> <img src="{{ asset($_front . 'images/home-icon-wallet.svg') }}" alt="">
                <p>@lang('Wallet')</p>
            </a> </li>@endif
        <li> <a href="{{ route('reserves.index') }}"> <img src="{{ asset($_front . 'images/home-icon-reservas.svg') }}" alt="">
                <p>@lang('Reservas')</p>
            </a> </li>
    </ul>
    <div class="notifi">
        @if ($newNotifications->count() || $historyNotifications->count())
            <div class="notifi-tit">
                <h1 class="m-tit">@lang('Notificaciones')</h1>
                <span>{{ $newNotifications->count() }}</span> </div>
            <ul>
                @foreach($newNotifications as $notification)
                    <li>
                        <a href="{{ route($notification->route_name, $notification->post_id ? ['id' => $notification->post_id] : []) }}">
                            <div class="noti-img"> <img src="{{ asset($_front . 'images/' . $notification->image_url) }}" alt=""> </div>
                            <div class="noti-cnt">
                                <h1>@lang($notification->title)</h1>
                                @if ($notification->subtitle)
                                    <p>@lang($notification->subtitle, $notification->post_id ? ['date' => \Carbon\Carbon::createFromFormat('d/m/Y', $notification->post->get_field('data'))->format('d/m/y'), 'hour' => $notification->post->get_field('hora') ? $notification->post->get_field('hora') : $notification->post->get_field('hora-sortida')] : [])</p>
                                @endif
                                <span>{{ $notification->created_at->format('H:i') }}</span> </div>
                        </a> </li>
                @endforeach
            </ul>
            <div class="histo">
                @if ($historyNotifications->count())
                    <div class="histo-tit">
                        <h1 class="m-tit">@lang('Histórico')</h1>
                    </div>
                    <ul>
                        @foreach($historyNotifications as $notification)
                            <li> <a href="{{ route($notification->route_name, $notification->post_id ? ['id' => $notification->post_id] : []) }}">
                                    <div class="histo-img"> <img src="{{ asset($_front . 'images/' . $notification->small_image_url) }}" alt=""> </div>
                                    <div class="histo-cnt">
                                        <h2>@lang($notification->title)</h2>
                                        <p>{{ $notification->created_at->format('d/m/y') }}</p>
                                    </div>
                                </a> </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        @endif
    </div>
    <div class="histo mb">
        <div class="histo-tit">
            <a href="{{ route('communication') }}"><h1 class="m-tit">@lang('Ver todas')</h1></a>
        </div>
    </div>
    @if ($walletTransactions)
        <div class="wallet">
            <h1 class="m-tit">@lang('Wallet')</h1>
            <div class="estado-wall">
                <div>
                    <div class="estado-wall-lft">
                        <p>@lang('Estado de su Wallet')</p>
                        <h2>{{ format_money($user->get_field('saldo')) }}€</h2>
                    </div><div class="estado-wall-rgt"> <a href="{{ route('user.wallet') }}">@lang('AÑADIR')</a> </div>
                </div>
                @if ($user->get_field('saldo') <= 0)<h4>@lang('Recuerde recargar su wallet')</h4>@endif
            </div>
            <ul>
                @foreach($walletTransactions as $walletTransaction)
                    <li>
                        {{--<a href="javascript:void(0);">--}}
                            <div class="walle-lft">
                                <h2>{{ $walletTransaction->comments }}</h2>
                                <p>{{ $walletTransaction->created_at->format('d/m/Y') }}</p>
                            </div>
                            <div class="walle-rgt">
                                @if ($walletTransaction->amount >= 0)
                                    <p>+{{ $walletTransaction->amount }} €</p>
                                @else
                                    <p class="red">{{ $walletTransaction->amount }} €</p>
                                @endif

                            </div>
                        {{--</a>--}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

@endsection