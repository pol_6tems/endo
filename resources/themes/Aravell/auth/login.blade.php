@extends('Front::layouts.login_main')

@section('content')
    <div class="login-white-bg">
        <h2>@lang('Área privada')</h2>
        <!--tab-->
        <div id="horizontalTab">
            <ul class="resp-tabs-list">
                <li>@lang('Socios')</li>
                <li>@lang('Visitantes')</li>
            </ul>
            <div class="resp-tabs-container">
                <div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="login-form">
                            <ul>
                                <li>
                                    <label>@lang('Email')</label>
                                    <input type="email" name="email" placeholder="@lang('Email usuario')" class="user-ico" value="{{ old('email', '') }}">
                                </li>
                                <li>
                                    <label>@lang('Password')</label>
                                    <input type="password" name="password" placeholder="@lang('Password')" class="pwd-ico">
                                </li>
                                <li class="chek">
                                    <input id="option" type="checkbox" name="remember" value="option" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="option"><span><span></span></span>@lang('Mantener iniciada la sesión')</label>
                                </li>
                                <li>
                                    <input type="submit" value="@lang('Acceder')">
                                </li>
                                <li>
                                    <a href="{{ route('password.request') }}"><h5><span></span> @lang('No recuerdo mi contraseña')</h5></a>
                                </li>

                                @if (count($errors->all()))
                                    @foreach($errors->all() as $error)
                                        <li class="lg-error">{{ $error }}<span class="close"><img src="{{ asset($_front.'images/close-icons.svg') }}" alt="" title=""></span></li>
                                        <li class="lg-error mbl">{{ $error }}<span class="close"><img src="{{ asset($_front.'images/cl-icon.svg') }}" alt="" title=""></span></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </form>
                </div>
                <div>
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="login-form invitados">
                            <p>@lang('Para registrarse en Aravell rellene los siguientes campos'):</p>
                            <ul>
                                <li>
                                    <label>@lang('Nombre')</label>
                                    <input type="text" name="name" placeholder="@lang('Nombre')" class="user-ico" value="{{ old('name', '') }}">
                                </li>
                                <li>
                                    <label>@lang('Apellido')</label>
                                    <input type="text" name="lastname" placeholder="@lang('Apellido')" class="pwd-ico" value="{{ old('lastname', '') }}">
                                </li>
                                <li>
                                    <label>@lang('Email')</label>
                                    <input type="text" name="email" placeholder="@lang('Email')" class="pwd-ico" value="{{ old('email', '') }}">
                                </li>
                                <li>
                                    <label>@lang('Teléfono')</label>
                                    <input type="text" name="telefon-1" placeholder="@lang('Num. de teléfono')" class="pwd-ico" value="{{ old('telefon-1', '') }}">
                                </li>
                                <li>
                                    <label>@lang('País')</label>
                                    <input type="text" name="pais" placeholder="@lang('País')" class="pwd-ico" value="{{ old('pais', '') }}">
                                </li>
                                <p class="crea">@lang('Crea una contraseña de acceso')</p>
                                <li>
                                    <label>@lang('Password')</label>
                                    <input type="password" name="password" placeholder="@lang('Password')" class="pwd-ico">
                                </li>
                                <li>
                                    <label>@lang('Repetir contraseña')</label>
                                    <input type="password" name="password_confirmation" placeholder="@lang('Password')" class="pwd-ico">
                                </li>
                                <li class="chek">
                                    <input id="options" type="checkbox" name="remember" value="option" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="options"><span><span></span></span>@lang('Mantener iniciada la sesión')</label>
                                </li>
                                <li>
                                    <input type="submit" value="@lang('Acceder')">
                                </li>
                                <li>
                                    <a href="{{ route('password.request') }}"><h5><span></span> @lang('No recuerdo mi contraseña')</h5></a>
                                </li>

                                @if (count($errors->all()))
                                    @foreach($errors->all() as $error)
                                        <li class="lg-error">{{ $error }}<span class="close"><img src="{{ asset($_front.'images/close-icons.svg') }}" alt="" title=""></span></li>
                                        <li class="lg-error mbl">{{ $error }}<span class="close"><img src="{{ asset($_front.'images/cl-icon.svg') }}" alt="" title=""></span></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--tab-end-->
    </div>
@endsection