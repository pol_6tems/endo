@extends('Front::layouts.login_main')

@section('content')
	<div class="login-white-bg rec">
		@if (session('status'))
			<h2>@lang('Email enviado')</h2>
			<p>@lang('Compruebe su Email. Le hemos enviado un <br>nuevo link para modificar la contraseña')</p>
			<div class="login-form r-mail">
				<ul>
					<li> <img src="{{ asset($_front . 'images/mail-icon-white.svg') }}" alt="" title=""> </li>
					<li>
						<input type="submit" onclick="{{ route('index') }}" value="@lang('VOLVER')'">
					</li>
				</ul>
			</div>
		@else

			<h2>@lang('Recuperar contraseña')</h2>
			<p>@lang('Introduzca su dirección de correo<br> electrónico para recuperar su contraseña')</p>

			<form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
				@csrf

				<div class="login-form r-mail">
					<ul>
						<li>
							<label>@lang('Email')</label>
							<input type="text" name="email" placeholder="@lang('Email')" class="user-ico">
						</li>
						<li class="desktop">
							<input type="submit" value="@lang('ENVIAR')">
						</li>
						@if (count($errors->all()))
							@foreach($errors->all() as $error)
								<li class="lg-error">{{ $error }}<span class="close"><img src="{{ asset($_front.'images/close-icons.svg') }}" alt="" title=""></span></li>
								<li class="lg-error mbl">{{ $error }}<span class="close"><img src="{{ asset($_front.'images/cl-icon.svg') }}" alt="" title=""></span></li>
							@endforeach
						@endif

						<li class="mbl-view">
							<input type="submit" value="@lang('ENVIAR')">
						</li>
					</ul>
				</div>
			</form>
		@endif
	</div>
@endsection
