@extends('Front::layouts.login_main')

@section('content')
	<form method="POST" action="{{ route('password.request') }}">
		@csrf

		<input type="hidden" name="token" value="{{ $token }}">

		<div class="login-white-bg nueva">
			<h2>@lang('Actualización de contraseña')</h2>
			<p>@lang('Introduzca su nueva contraseña')</p>
			<div class="login-form repetir nue">
				<ul>
					<li>
						<label>@lang('Email')</label>
						<input type="email" name="email" placeholder="@lang('Email')" class="user-ico" value="{{ $email or old('email') }}">
					</li>
					<li>
						<label>@lang('Nueva contraseña')</label>
						<input type="password" name="password" placeholder="@lang('Password')" class="user-ico">
					</li>
					<li>
						<label>@lang('Repetir nueva contraseña')</label>
						<input type="password" name="password_confirmation" placeholder="@lang('Password')" class="user-ico">
					</li>

					<li>
						<input type="submit" value="@lang('GUARDAR')">
					</li>

					@if (count($errors->all()))
						@foreach($errors->all() as $error)
							<li class="lg-error">{{ $error }}<span class="close"><img src="{{ asset($_front.'images/close-icons.svg') }}" alt="" title=""></span></li>
							<li class="lg-error mbl">{{ $error }}<span class="close"><img src="{{ asset($_front.'images/cl-icon.svg') }}" alt="" title=""></span></li>
						@endforeach
					@endif
				</ul>
			</div>
		</div>
	</form>
@endsection
