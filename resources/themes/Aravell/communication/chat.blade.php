@extends('Front::layouts.main')

@section('page_header', __('Comunicación'))

@section('content')
    <div class="comm-nav">
        <ul class="tabs-list">
            <li onclick="window.location = '{{ route('communication') }}';">@lang('Notificaciones')</li>
            <li class="tab-active">@lang('Chat')</li>
        </ul>
    </div>

    <div class="chat">
        <div class="chat-tit">
            <p><span>{{--Sergio--}}{{ $orgUser->name }}</span> @lang('Organizador')</p>
            <p class="rg-p">@lang('En línea')</p>
        </div>
        <div class="chat-area demo-y">
            <ul class="chat-list js-chat-list">
                @php($currentDate = null)
                @foreach($chat->mensajes as $mensaje)
                    @if ($currentDate != $mensaje->created_at->startOfDay())
                        @php($currentDate = $mensaje->created_at)
                        <li>
                            <div class="chat-date">
                                <h1>{{ print_chat_date($currentDate) }}</h1>
                            </div>
                        </li>
                    @endif

                    <li class="{{ $mensaje->to == $user->id ? 'reciever' : 'sender' }}">
                        <p>{!! nl2br($mensaje->show_message()) !!}</p>
                        <span>{{ $mensaje->created_at->format('G:i') }}</span>
                    </li>
                @endforeach
                @if ($currentDate->startOfDay() != \Carbon\Carbon::today())
                    <li class="js-chat-today" style="display: none">
                        <div class="chat-date">
                            <h1>@lang('Hoy')</h1>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
        <div class="chat-send">
            <input type="text" class="js-chat-message" placeholder="@lang('Escribe tu mensaje…')">
            <input type="submit" class="js-chat-send" data-chat-type="{{$chat->type}}" data-chat-id="{{ $chat->chat }}" data-empty-msg="@lang('Mensaje vacío')" data-error-msg="@lang('Error al enviar el mensaje')" data-url="{{ route('communication.send-message') }}" value="">
        </div>
    </div>
@endsection