@extends('Front::layouts.main')

@section('page_header', __('Comunicación'))

@section('content')
    <div class="comm-nav">
        <ul class="tabs-list">
            <li class="tab-active">@lang('Notificaciones')</li>
            <li onclick="window.location = '{{ route('communication.chat') }}';">@lang('Chat')</li>
        </ul>
    </div>

    <div class="notifi reserva">
        <ul>
            @foreach($newNotifications as $notification)
                <li><a href="javascript:void(0);">
                        <div class="noti-img"> <img src="{{ asset($_front . 'images/' . $notification->image_url) }}" alt=""> </div>
                        <div class="noti-cnt">
                            <h1>{{ $notification->title }}</h1>
                            @if ($notification->subtitle)
                                <p>@lang($notification->subtitle, $notification->post_id ? ['date' => \Carbon\Carbon::createFromFormat('d/m/Y', $notification->post->get_field('data'))->format('d/m/y'), 'hour' => $notification->post->get_field('hora') ? $notification->post->get_field('hora') : $notification->post->get_field('hora-sortida')] : [])</p>
                            @endif
                            <span>{{ $notification->created_at->format('H:i') }}</span> </div>
                    </a>
                    <div class="ver-buton">
                        <input type="submit" onclick="window.location = '{{ route($notification->route_name, $notification->post_id ? ['id' => $notification->post_id] : []) }}';" value="{{ $notification->post_id ? __('VER RESERVA') : __('VER') }}">
                    </div>
                </li>
            @endforeach
        </ul>
        <div class="notifi history">
            @if ($historyNotifications->count())
                <div class="histo hisy">
                    <div class="histo-tit">
                        <h1 class="m-tit">@lang('Histórico')</h1>
                    </div>
                </div>
                <ul>
                    @foreach($historyNotifications as $notification)
                        <li><a href="javascript:void(0);">
                                <div class="noti-img"> <img src="{{ asset($_front . 'images/' . $notification->image_url) }}" alt=""> </div>
                                <div class="noti-cnt">
                                    <h1>{{ $notification->title }}</h1>
                                    @if ($notification->subtitle)
                                        <p>@lang($notification->subtitle, $notification->post_id ? ['date' => \Carbon\Carbon::createFromFormat('d/m/Y', $notification->post->get_field('data'))->format('d/m/y'), 'hour' => $notification->post->get_field('hora') ? $notification->post->get_field('hora') : $notification->post->get_field('hora-sortida')] : [])</p>
                                    @endif
                                    <span>{{ $notification->created_at->format('d/m/y H:i') }}</span> </div>
                            </a>
                            <div class="ver-buton">
                                <input type="submit" onclick="window.location = '{{ route($notification->route_name, $notification->post_id ? ['id' => $notification->post_id] : []) }}';" value="{{ $notification->post_id ? __('VER RESERVA') : __('VER') }}">
                            </div>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
@endsection