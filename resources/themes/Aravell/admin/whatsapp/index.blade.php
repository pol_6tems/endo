@extends('Admin::layouts.admin')

@section('section-title')
    @lang('Mensajería')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">textsms</i>
                    </div>
                    <h4 class="card-title">
                        @lang('Mensajería')
                    </h4>
                </div>
                <div class="card-body">
                    <div class="material-datatables">
                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                            <tr>
                                <th>@lang('Name')</th>
                                <th width="15%">@lang('Email')</th>
                                <th width="15%" class="disabled-sorting text-right">@lang('Actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>
                                        {{ $user->fullname() }}
                                    </td>
                                    <td>{{ $user->email }}</td>

                                    <td class="td-actions text-right">
                                        <a href="{{ route('admin.messaging.edit', ['id' => $user->id]) }}" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-success" title="@lang('Enviar mensaje')">
                                            <i class="material-icons">textsms</i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            <div class="loader">
                                <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                    <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                                </svg>
                            </div>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.table').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [25, 50, -1],
                    [25, 50, "@lang('All')"]
                ],
                responsive: true,
                language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" }
            });
        });
    </script>
@endsection