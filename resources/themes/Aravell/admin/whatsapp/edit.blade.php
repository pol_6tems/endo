@extends('Admin::layouts.admin')

@section('section-title')
    @lang('Mensajeria')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">textsms</i>
                    </div>
                    <h4 class="card-title">
                        @lang('Enviar mensaje')
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>@lang('Usuario')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Name'):</dt><dd>{{ $user->fullname() }}</dd>
                                <dt>@lang('Email'):</dt><dd>{{ $user->email }}</dd>
                                <dt>@lang('Licencia'):</dt><dd>{!! $user->get_field('llicencia') ?: '&nbsp;' !!}</dd>
                                <dt>@lang('Teléfono 1'):</dt><dd>{!! $user->get_field('telefon-1') ?: '&nbsp;' !!}</dd>
                                <dt>@lang('Teléfono 2'):</dt><dd>{!! $user->get_field('telefon-2') ?: '&nbsp;' !!}</dd>
                            </dl>
                        </div>
                    </div>
                    <form id="form" class="form-horizontal" action="{{ route('admin.messaging.update', ['id' => $user->id]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="dharmas-form">
                            <label for="message">@lang('Mensaje') (Your reserve code is 1234567)</label><br>
                            <textarea type="text" rows="4" class="form-control" name="message" id="message" placeholder="@lang('Mensaje a enviar')" required></textarea>
                        </div>

                        <div class="actions" style="margin-top: 16px">
                            <input type="submit" class="btn btn-sm btn-primary pull-right" value="@lang('Enviar')"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection