@extends('Admin::layouts.admin')

@section('section-title')
    @lang(__('Calendar'))
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon" onclick="window.location = '{{ route('admin.green-fee-calendar', ['start_date' => $date->format('d/m/Y')]) }}'" style="cursor: pointer">
                        <i class="material-icons">calendar_today</i>
                    </div>

                    <h4 class="card-title">{{ ucfirst(\Jenssegers\Date\Date::parse($date)->format('l d/m')) }}</h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="material-datatables">
                            <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>@lang('Horas')</th>
                                    <th>@lang('Tarifa')</th>
                                    @foreach($categories as $category)
                                        <th style="text-align: center">{{ $category->title }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(range(8, 19) as $hour)
                                    @php ($dayHour = null)
                                    @if ($day)
                                        @php ($dayHour = $day->hours->filter(function ($dayHour) use ($hour) {
                                            return $dayHour->hour == sprintf("%02s", $hour) . ':00:00';
                                        })->first())
                                    @endif

                                    <tr>
                                        <td>{{ sprintf("%02s", $hour) }}:00</td>
                                        <td>
                                            <select class="js-tariff-select" data-date="{{ $date->format('d/m/Y') }}" data-hour="{{ $hour }}" style="-webkit-appearance: menulist; -moz-appearance: menulist" disabled>
                                                <option value="0">@lang('Cerrado')</option>
                                                @foreach($tariffs as $tariff)
                                                    <option value="{{ $tariff->id }}" @if ($dayHour && $dayHour->tariff_id == $tariff->id)selected @endif>{{ $tariff->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        @foreach($categories as $category)
                                                <td>
                                                    @php ($exceptionPrice = $dayHour ? $dayHour->prices->where('post_id', $category->id)->first() : null)

                                                    <select class="js-exception-select" data-date="{{ $date->format('d/m/Y') }}" data-hour="{{ $hour }}" data-category="{{ $category->id }}" style="-webkit-appearance: menulist; -moz-appearance: menulist" disabled>
                                                        <option value="0">@lang('Sin excepción')</option>
                                                        @foreach($tariffs as $tariff)
                                                            <option value="{{ $tariff->id }}" @if ($exceptionPrice && $exceptionPrice->tariff_id == $tariff->id)selected @endif>{{ $tariff->name }}</option>
                                                        @endforeach
                                                    </select>

                                                    {{--<input type="number" class="js-exception-select" data-hour="{{ $hour }}" data-category="{{ $category->id }}" value="{{ $catPrice }}" min="0" disabled>--}}
                                                </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <h3 class="card-title">@lang('Opciones')</h3>
                </div>

                <div class="card-body">
                    <div class="togglebutton">
                        <label style="width: 100%; text-align: center">
                            <input
                                    type="checkbox"
                                    class="js-calendar-open"
                                    value="1"
                                    {{ $day && !$day->is_open ? '' : 'checked' }} disabled />

                            <span class="toggle"></span>
                            @lang('Abierto')
                        </label>
                    </div>

                    <br>
                    <h4><strong>@lang('Clonador')</strong></h4>

                    <div class="row">

                        <form action="{{ route('admin.green-fee-calendar.clone') }}" method="POST">
                            @csrf
                            <input type="hidden" name="type" value="day">
                            <input type="hidden" name="id" value="{{ $day ? $day->id : 0 }}">
                            <input type="hidden" name="date" value="{{ $date->format('d/m/Y') }}">

                            <div class="flex-row pt-3">
                                <label class="col-md-12">
                                    @lang('Desde')
                                </label>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input
                                            type="text"
                                            class="form-control datepicker"
                                            name="start_date"
                                            required />
                                    </div>
                                </div>
                            </div>

                            <div class="flex-row pt-3">
                                <label class="col-md-12">
                                    @lang('Hasta')
                                </label>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input
                                                type="text"
                                                class="form-control datepicker"
                                                name="end_date" />
                                    </div>
                                </div>
                            </div>
                            <div class="flex-row pt-3">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary pull-right" value="@lang('Clonar')">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <article class="card">
        <div class="card-footer">
            <div class="mr-auto">
                <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" value="@lang('Tornar')" onclick="window.location.href='{{ route('admin.green-fee-calendar', ['start_date' => $date->format('d/m/Y')]) }}'">
            </div>
            <div class="clearfix"></div>
        </div>
    </article>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var exceptionSelect = $('.js-exception-select');
            exceptionSelect.removeAttr('disabled');

            exceptionSelect.on('change', function () {
                $.ajax({
                    url: '{{ route('admin.green-fee-calendar.price', $day ? ['id' => $day->id] : ['date' => $date->format('d/m/Y')]) }}',
                    method: 'POST',
                    data: {'_token': '{{ csrf_token() }}', 'category': $(this).data('category'), 'hour': $(this).data('hour'), 'tariff': $(this).val()},
                });
            });

            var calendarOpenCheckbox = $('.js-calendar-open');
            calendarOpenCheckbox.removeAttr('disabled');

            calendarOpenCheckbox.on('change', function () {
                $.ajax({
                    url: '{{ route('admin.green-fee-calendar.open', $day ? ['id' => $day->id] : ['date' => $date->format('d/m/Y')]) }}',
                    method: 'POST',
                    data: {'_token': '{{ csrf_token() }}', 'is_open': $(this).is(':checked') ? 1 : 0},
                });
            });

            var tariffSelect = $('.js-tariff-select');

            tariffSelect.removeAttr('disabled');

            tariffSelect.on('change', function () {
                $.ajax({
                    url: '{{ route('admin.green-fee-calendar.price', $day ? ['id' => $day->id] : ['date' => $date->format('d/m/Y')]) }}',
                    method: 'POST',
                    data: {'_token': '{{ csrf_token() }}', 'hour': $(this).data('hour'), 'tariff': $(this).val()},
                });
            });
        });
    </script>
@endsection