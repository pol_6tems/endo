@extends('Admin::layouts.admin')

@section('section-title')
    @lang(__('Transacciones'))
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <article class="card pm">
                <div class="card-header card-header-primary card-header-icon">
                    <h4 class="card-title">@lang('Detalles')</h4>
                </div>

                <div class="card-body pb-5">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>@lang('Compra')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Id'):</dt><dd>{{ $purchase->order_id ? $purchase->order_id : 'N/A' }}</dd>
                                <dt>@lang('Método'):</dt><dd>{{ ucfirst($purchase->method) }}</dd>
                                @if ($purchase->external_order_id)
                                    <dt>@lang('Id pago')</dt><dd>{{ $purchase->external_order_id }}</dd>
                                @endif

                                <dt>@lang('Tipo'):</dt><dd><a href="{{ route('admin.posts.edit', ['id' => $purchase->post_id, 'post_type' => $purchase->post->type]) }}">{!! $purchase->post->customPostTranslations->where('locale', app()->getLocale())->first()->title !!}</a></dd>
                                <dt>@lang('Created'):</dt><dd>{{ $purchase->created_at->format('d/m/Y H:m:s') }}</dd>
                                <dt>@lang('Updated'):</dt><dd>{{ $purchase->updated_at->format('d/m/Y H:m:s') }}</dd>
                            </dl>
                        </div>

                        <div class="col-md-6">
                            <h5>@lang('Comentarios')</h5>
                            <dl class="dl-horizontal">
                                <dt></dt><dd>{{ $purchase->comments }}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5>@lang('Usuario')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Name'):</dt><dd>{{ $purchase->user->fullname() }}</dd>
                                <dt>@lang('Email'):</dt><dd><a href="{{ route('admin.users.edit', ['id' => $purchase->user_id]) }}">{{ $purchase->user->email }}</a></dd>
                                <dt>@lang('Licencia'):</dt><dd>{{ $purchase->user->get_field('llicencia') }}</dd>
                            </dl>
                        </div>

                        <div class="col-md-6">
                            <h5>@lang('Pago')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Base'):</dt><dd>{{ $purchase->base_amount }} €</dd>
                                <dt>@lang('Descuento'):</dt><dd>{{ $purchase->coupon_discount_amount }} €</dd>
                                <dt>@lang('Total')</dt><dd><strong>{{ $purchase->total_amount }} €</strong></dd>
                            </dl>
                        </div>
                    </div>

                </div>
            </article>
        </div>
    </div>
@endsection