@extends('Admin::layouts.admin')

@section('section-title')
    @lang(__('Transacciones'))
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">library_books</i>
                    </div>
                    <h4 class="card-title">
                        @lang(__('Transacciones'))
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                            <form method="GET" class="form-horizontal">
                                <div class="input-group mb-3">
                                    <input type="text" name="search" class="form-control form-control-sm" placeholder="@lang('Filtrar')"  value="{{ request('search') }}">
                                    <div class="input-group-append">
                                        <button class="btn btn-sm btn-primary" type="submit">Go!</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="material-datatables">
                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                            <tr>
                                <th>@lang('Id')</th>
                                <th>@lang('Type')</th>
                                <th>@lang('Client')</th>
                                <th>@lang('Total')</th>
                                <th>@lang('Comentarios')</th>
                                <th>@lang('Created at')</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($purchases as $purchase)
                                    <tr>

                                        <td>
                                            <a href="{{ route('admin.purchases.show', ['id' => $purchase->id]) }}">
                                                {{ $purchase->order_id }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $purchase->post ? $purchase->post->customPostTranslations->where('locale', app()->getLocale())->first()->title : 'N/A' }}
                                        </td>
                                        <td>
                                            {{ $purchase->user->fullname() }}
                                        </td>
                                        <td>
                                            {{ format_money($purchase->total_amount) }} €
                                        </td>
                                        <td>
                                            {{ $purchase->comments }}
                                        </td>
                                        <td>
                                            {{ $purchase->created_at->toDateTimeString() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{ $purchases->links("Front::admin.simple-pagination") }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection