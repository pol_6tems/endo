@extends('Admin::layouts.admin')

@section('section-title')
    @lang('Tarifas')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <form id="form" class="form-horizontal" action="{{ route('admin.tariffs.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <article class="card">
                    <div class="card-header card-header-primary card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">calendar_view_day</i>
                        </div>
                        <h4 class="card-title">@Lang('Add new')</h4>
                    </div>

                    <div class="card-body ">
                        @if ($errors->count() > 0)
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <div class="mt-4 m-auto custom-field text-field">
                            <div class="flex-row pt-3">
                                <label class="col-md-12">
                                    @lang('Name')
                                </label>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input name="name" type="text" class="form-control" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="card sticky bottom">
                    <div class="card-footer">
                        <div class="mr-auto">
                            <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" value="@Lang('Tornar')" onclick="window.location.href='{{ route('admin.tariffs') }}'">
                        </div>
                        <div class="ml-auto">
                            <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd" value="@Lang('Create')">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </article>
            </form>
        </div>
    </div>
@endsection