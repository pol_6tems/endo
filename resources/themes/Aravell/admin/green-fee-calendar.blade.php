@extends('Admin::layouts.admin')

@section('section-title')
    @lang(__('Calendar'))
@endsection

@section('styles')
    <style>
        .table>tbody>tr>td {
            padding: 0;
        }

        .calendar-cell {
            border: 2px solid transparent;
            cursor: pointer;
            padding: 8px;
        }

        .calendar-cell:hover, .calendar-cell.hover {
            border: 2px solid #d7d7d7;
        }

        .calendar-cell.checked {
            border: 2px solid #0aa927;
        }

        .calendar-cell .calendar-checkbox {
            height: 25px;
            display: inline-block;
            width: 30%;
        }

        .calendar-cell .hour {
            color: #0aa927;
            font-weight: 500;
        }

        .calendar-cell .selected-tariff {
            font-weight: bold;
            display: inline-block;
            width: 70%;
        }

        /* The container */
        .ccb-container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .ccb-container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .ccb-checkmark {
            position: absolute;
            top: 0;
            right: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .ccb-containerr:hover input ~ .ccb-checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .ccb-container input:checked ~ .ccb-checkmark {
            background-color: #0aa927;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .ccb-checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .ccb-container input:checked ~ .ccb-checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .ccb-container .ccb-checkmark:after {
            left: 9px;
            top: 5px;
            width: 6px;
            height: 13px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        @media screen and (max-width:1023px) {
            .calendar-cell .calendar-checkbox, .calendar-cell .selected-tariff {
                display: block;
                width: 100%;
            }
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">calendar_today</i>
                    </div>

                    <h4 class="card-title">
                        <a href="javascript:void(0);" class="btn btn-primary btn-sm js-calendar-select-all">@lang('Select all')</a>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-3 col-lg-4 col-md-5 col-sm-6">
                            <div class="pt-3 pb-3">
                                <div class="form-group">
                                    {{--<label>
                                        @lang('Semana')
                                    </label>--}}
                                    <div class="input-group">
                                        @if ($prev_date)
                                            <a href="{{ route(route_name(),['start_date' => $prev_date->format('d/m/Y')]) }}" ><span class="input-group-addon btn btn-sm btn-primary" style="display: block; padding-left: 12px"><i class="fa fa-angle-double-left"></i> </span></a>
                                        @endif
                                        <input
                                                type="text"
                                                class="form-control datepicker js-aravell-calendar"
                                                data-url="{{ route(route_name(),['start_date' => '']) }}"
                                                value="{{ $start_date->format('d/m/Y') }}"/>
                                        @if ($next_date)
                                                <a href="{{ route(route_name(),['start_date' => $next_date->format('d/m/Y')]) }}"><span class="input-group-addon btn btn-sm btn-primary" style="display: block; padding-left: 12px; margin-left: 12px"><i class="fa fa-angle-double-right"></i></span></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="material-datatables" style="width: 100%">
                            <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    {{--<th>@lang('Horas')</th>--}}
                                    @foreach($dates as $date)
                                        <th style="text-align: center;cursor: pointer" class="js-day-header" data-date="{{ $date->format('d/m/Y') }}">{{ ucfirst(\Jenssegers\Date\Date::parse($date)->format('l d/m')) }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        {{--<td>@lang('Abierto')</td>--}}

                                        @foreach($dates as $date)
                                            @php ($day = $days->where('date', $date)->first())
                                            <td style="padding: 12px 8px">
                                                <div class="togglebutton">
                                                    <label style="width: 100%; text-align: center">
                                                        <span class="toggle-text" data-open="@lang('Abierto')" data-closed="@lang('Cerrado')">
                                                            {{ $day && !$day->is_open ? __('Cerrado') : __('Abierto') }}
                                                        </span>

                                                        <input
                                                                type="checkbox"
                                                                class="js-calendar-open"
                                                                data-date="{{ $date->format('d/m/Y') }}"
                                                                value="1"
                                                                {{ $day && !$day->is_open ? '' : 'checked' }} disabled />

                                                        <span class="toggle"></span>
                                                    </label>
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>

                                    @foreach(range(8, 19) as $hour)
                                        <tr>
                                            {{--<td>{{ sprintf("%02s", $hour) }}:00</td>--}}
                                            @foreach($dates as $date)
                                                @php ($day = $days->where('date', $date)->first())
                                                @php ($dayHour = null)
                                                @if ($day)
                                                    @php ($dayHour = $day->hours->filter(function ($dayHour) use ($hour) {
                                                        return $dayHour->hour == sprintf("%02s", $hour) . ':00:00';
                                                    })->first())
                                                @endif
                                                <td>
                                                    <div class="calendar-cell" data-date="{{ $date->format('d/m/Y') }}">
                                                        <div class="hour">
                                                            {{ sprintf("%02s", $hour) }}:00
                                                        </div>
                                                        <div class="selected-tariff">
                                                            {{ $dayHour && $dayHour->tariff_id ? $tariffs->where('id', $dayHour->tariff_id)->first()->name : __('Cerrado') }}

                                                            @if ($dayHour && $dayHour->prices->count())
                                                                <span title="@lang('Franja con excepciones de tarifa según categoría')" style="color: red">*</span>
                                                            @endif
                                                        </div><div class="calendar-checkbox">
                                                            <label class="ccb-container">
                                                                <input class="js-calendar-cb" type="checkbox" data-date="{{ $date->format('d/m/Y') }}" data-hour="{{ $hour }}">
                                                                <span class="ccb-checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    {{--<div style="text-align: center">
                                                        <select class="js-tariff-select" data-date="{{ $date->format('d/m/Y') }}" data-hour="{{ $hour }}" style="-webkit-appearance: menulist; -moz-appearance: menulist" disabled>
                                                            <option value="0">@lang('Cerrado')</option>
                                                            @foreach($tariffs as $tariff)
                                                                <option value="{{ $tariff->id }}" @if ($dayHour && $dayHour->tariff_id == $tariff->id)selected @endif>{{ $tariff->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>--}}
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach

                                    <tr>
                                        @foreach($dates as $date)
                                            <td style="text-align: center; padding: 12px 8px;">
                                                <a href="{{ route('admin.green-fee-calendar.day', ['date' => $date->format('d/m/Y')]) }}" style="color: inherit" title="@lang('Definir excepciones')"><i class="material-icons" style="font-size:32px;color: #7b7b7b">warning</i></a>
                                            </td>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <span><span style="color: red">*</span> @lang('Franja con excepciones de tarifa según categoría')</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <h3 class="card-title">@lang('Aplicar tarifa')</h3>
                </div>

                <div class="card-body">
                    <div class="row pt-3">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select
                                        class="selectpicker js-selected-tariff"
                                        data-style="btn btn-primary"
                                        data-live-search="true">
                                    <option value="0" selected>@lang('Cerrado')</option>
                                    @foreach($tariffs as $tariff)
                                        <option value="{{ $tariff->id }}" >{{ $tariff->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="col-md-12">
                            <input type="button" class="btn btn-primary pull-right js-apply-tariff" value="@lang('Aplicar')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <h3 class="card-title">@lang('Clonador')</h3>
                </div>

                <div class="card-body">
                    <div class="row">

                        <form action="{{ route('admin.green-fee-calendar.clone') }}" method="POST">
                            @csrf
                            <input type="hidden" name="type" value="week">
                            <input type="hidden" name="date" value="{{ $start_date->format('d/m/Y') }}">

                            <div class="row pt-3">
                                <div class="col-md-6">
                                    <label class="col-md-12">
                                        @lang('Desde')
                                    </label>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input
                                                    type="text"
                                                    class="form-control datepicker"
                                                    name="start_date"
                                                    required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-12">
                                        @lang('Hasta')
                                    </label>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input
                                                    type="text"
                                                    class="form-control datepicker"
                                                    name="end_date"
                                                    required/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row pt-3">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary pull-right" value="@lang('Clonar')">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function changeTariff(tariff, hour, date, last) {
            $.ajax({
                url: '{{ route('admin.green-fee-calendar.price') }}',
                method: 'POST',
                data: {'_token': '{{ csrf_token() }}', 'hour': hour, 'date': date, 'tariff': tariff},
                async:false,
                success: function () {
                    if (last) {
                        location.reload();
                    }
                }
            });
        }

        $(document).ready(function() {
            $('.calendar-cell').on('click', function () {
                var cb = $(this).find('.js-calendar-cb');
                cb.attr("checked", !cb.prop("checked"));

                $(this).closest('.calendar-cell').toggleClass('checked');
            });

            $('.js-calendar-cb').on('change', function () {
                $(this).closest('.calendar-cell').toggleClass('checked');
            });

            $('.js-aravell-calendar').on('change dp.change', function (e) {
                if (e.oldDate !== null) {
                    window.location = $(this).data('url') + $(this).val();
                }
            });

            $('.js-apply-tariff').on('click', function () {
               var tariff = $('.selectpicker.js-selected-tariff').val();
               var selectedHours = $('.js-calendar-cb:checked');

               var selectedHoursLength = selectedHours.length;
               if (!selectedHoursLength) {
                   md.showNotification('top', 'center', "@lang('Seleccione al menos una franja horaria')", 'danger');
                   return;
               }

               if ($(this).is(':disabled')) {
                   console.log('disabled');
                   return;
               }

               $(this).attr('disabled', true);

               selectedHours.each(function (index, element) {
                   changeTariff(tariff, $(this).data('hour'), $(this).data('date'), index === (selectedHoursLength - 1));
               });
            });

            var calendarOpenCheckbox = $('.js-calendar-open');

            calendarOpenCheckbox.removeAttr('disabled');

            calendarOpenCheckbox.on('change', function () {
                var me = $(this);

                $.ajax({
                    url: '{{ route('admin.green-fee-calendar.open') }}',
                    method: 'POST',
                    data: {'_token': '{{ csrf_token() }}', 'is_open': $(this).is(':checked') ? 1 : 0, 'date': $(this).data('date')},
                    success: function () {
                        var toggleSpan = me.parent().find('.toggle-text');

                        toggleSpan.html(me.is(':checked') ? toggleSpan.data('open') : toggleSpan.data('closed'));
                    }
                });
            });

            var tariffSelect = $('.js-tariff-select');

            tariffSelect.removeAttr('disabled');

            tariffSelect.on('change', function () {
                changeTariff($(this).val(), $(this).data('hour'), $(this).data('date'), false);
            });

            $('.js-calendar-select-all').on('click', function () {
                var calendarCb = $('.js-calendar-cb');

                if ($('.js-calendar-cb:checked').length !== calendarCb.length) {
                    calendarCb.each(function () {
                        if (!$(this).attr('checked')) {
                            $(this).attr("checked", true);
                            $(this).closest('.calendar-cell').addClass('checked');
                        }
                    });
                } else {
                    calendarCb.each(function () {
                        if ($(this).attr('checked')) {
                            $(this).attr("checked", false);
                            $(this).closest('.calendar-cell').removeClass('checked');
                        }
                    });
                }
            });

            var dayHeader = $('.js-day-header');

            dayHeader.on('mouseover', function () {
                $('.calendar-cell[data-date="' + $(this).data('date') +'"').addClass('hover');
            });

            dayHeader.on('mouseout', function () {
                $('.calendar-cell[data-date="' + $(this).data('date') +'"').removeClass('hover');
            });

            dayHeader.on('click', function () {
                var calendarCb = $('.js-calendar-cb[data-date="' + $(this).data('date') +'"]');

                if ($('.js-calendar-cb[data-date="' + $(this).data('date') +'"]:checked').length !== calendarCb.length) {
                    calendarCb.each(function () {
                        if (!$(this).attr('checked')) {
                            $(this).attr("checked", true);
                            $(this).closest('.calendar-cell').addClass('checked');
                        }
                    });
                } else {
                    calendarCb.each(function () {
                        if ($(this).attr('checked')) {
                            $(this).attr("checked", false);
                            $(this).closest('.calendar-cell').removeClass('checked');
                        }
                    });
                }
            });
        });
    </script>
@endsection