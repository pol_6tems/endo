@extends('Admin::layouts.admin')

@section('section-title')
    @lang('Tarifas')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">calendar_view_day</i>
                    </div>

                    <h4 class="card-title">
                        @lang('Tarifas')

                        <a href="{{ route('admin.tariffs.create') }}" class="btn btn-primary btn-sm">@Lang('Add new')</a>
                    </h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="material-datatables">
                            <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>@lang('Tarifa')</th>
                                    @foreach($categories as $category)
                                        <th style="text-align: center">{{ $category->title }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($tariffs as $tariff)
                                        <tr>
                                            <td><input type="text" class="js-tariff-name" data-id="{{ $tariff->id }}" value="{{ $tariff->name }}" disabled></td>

                                            @foreach($categories as $category)
                                                <td>
                                                    @php($tariffCategory = $tariff->tariffCategories->where('category_id', $category->id)->first())
                                                    <input type="number" class="js-tariff-price" data-id="{{ $tariff->id }}" data-category="{{ $category->id }}" value="{{ $tariffCategory ? $tariffCategory->price : '' }}" min="0" disabled>
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var tariffPrice = $('.js-tariff-price');
            tariffPrice.removeAttr('disabled');

            tariffPrice.on('change', function () {
                $.ajax({
                    url: '{{ route('admin.tariffs.price') }}',
                    method: 'POST',
                    data: {'_token': '{{ csrf_token() }}', 'category': $(this).data('category'), 'id': $(this).data('id'), 'price': $(this).val()},
                });
            });

            var tariffName = $('.js-tariff-name');
            tariffName.removeAttr('disabled');

            tariffName.on('change', function () {
                $.ajax({
                    url: '{{ route('admin.tariffs.update') }}',
                    method: 'POST',
                    data: {'_token': '{{ csrf_token() }}', 'id': $(this).data('id'), 'name': $(this).val()},
                });
            });
        });
    </script>
@endsection