@extends('Admin::layouts.admin')

@section('section-title')
    @lang('Cobrar mesa')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">account_balance_wallet</i>
                    </div>
                    <h4 class="card-title">
                        @lang('Cobrar mesa')
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>@lang('Usuario')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Name'):</dt><dd>{{ $user->fullname() }}</dd>
                                <dt>@lang('Email'):</dt><dd>{{ $user->email }}</dd>
                                <dt>@lang('Licencia'):</dt><dd>{{ $user->get_field('llicencia') }}</dd>
                            </dl>
                        </div>
                    </div>
                    <div class="charge-total text-center">
                        <h3>@lang('Saldo')</h3>
                        <h2>{{ $user->get_field('saldo') }}</h2>
                    </div>
                    <form id="form" class="form-horizontal" action="{{ route('admin.tables.update', ['id' => $user->id]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="dharmas-form">
                            <label for="charge-explanation">@lang('Concepto')</label><br>
                            <input type="text" class="form-control" name="concept" id="charge-explanation">

                            <label for="dharmas-ammount">@lang('Cantidad')</label><br>
                            <input type="number" class="form-control" name="amount" id="dharmas-ammount">
                        </div>

                        <div class="actions" style="margin-top: 16px">
                            <input type="submit" class="btn btn-sm btn-primary pull-right" value="@lang('Cobrar')"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection