@extends('Front::layouts.main')

@section('page_header', __('Restaurante'))

@section('content')
    <div class="green-fee res-pge">
        <ul>
            <li class="active" id="green1" onclick="window.location = '{{ route('restaurant.create') }}'"><span>1</span>
                <h3>@lang('Seleccionar reserva')</h3>
            </li>
            <li class="active" id="green2"><span>2</span>
                <h3>@lang('Resumen reserva')</h3>
            </li>
            <li id="green3"><span>3</span>
                <h3>@lang('Prereserva realizada')</h3>
            </li>
        </ul>
    </div>

    <div id="tab2" class="inner-form tab-content tab-open">
        <h2>@lang('Resumen reserva')</h2>
        <ul class="fecha resumen">
            @include('Front::partials.resume-restaurant')

            <li class="reser">
                <form method="POST" action="{{ route($type . '.pay') }}" >
                    @csrf

                    <input type="hidden" name="reserve_id" value="{{ $reserve->id }}">
                    <input type="submit" name="" value="@lang('PRERESERVAR')" onClick="fnTab('2');">
                </form>
            </li>
        </ul>
    </div>
@endsection