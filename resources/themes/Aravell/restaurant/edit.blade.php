@extends('Front::layouts.main')

@section('page_header', __('Restaurante'))

@section('content')
    <div class="inner-form">
        <h2>@lang('Modifica tu reserva')</h2>

        <form method="POST" action="{{ route($type . '.update', ['id' => $reserve->id]) }}" >
            @csrf
            <input type="hidden" name="_method" value="PUT">

            @include('Front::partials.edit-restaurant', ['reserve' => $reserve, 'submitBtn' => __('Modificar')])
        </form>
    </div>
@endsection