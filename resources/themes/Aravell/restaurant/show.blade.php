@extends('Front::layouts.main')

@section('page_header', __('Restaurante'))

@section('content')
    <div class="inner-form">
        <h2>@lang('Resumen reserva Restaurante')</h2>
        <ul class="fecha resumen">
            @include('Front::partials.resume-restaurant', ['reserve' => $reserve])
        </ul>
    </div>
    @if (\Carbon\Carbon::createFromFormat('d/m/Y H:i', $reserve->get_field('data') . ' ' . $reserve->get_field('hora')) > \Carbon\Carbon::now())
        <div class="full-sec">
            <p>@lang('Para contactar directamente con Aravell Golf llame al (+34) <span>973 36 00 66</span>')</p>
            <p>@lang('También puede modificar su reserva.')</p>
        </div>
        <div class="inner-form modificar">
            <ul>
                <li>
                    <input type="submit" onclick="window.location = '{{ route('restaurant.edit', ['id' => $reserve->id]) }}'" value="@lang('MODIFICAR')">
                    @if (!$isAfterConfirm)
                        <input type="submit" onclick="window.location = '{{ route('restaurant.pre-delete', ['id' => $reserve->id]) }}'" value="@lang('ANULAR')">
                    @endif
                </li>
            </ul>
        </div>
    @endif
@endsection