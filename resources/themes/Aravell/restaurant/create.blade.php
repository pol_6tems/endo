@extends('Front::layouts.main')

@section('page_header', __('Restaurante'))

@section('content')
    <div class="green-fee res-pge">
        <ul>
            <li class="active" id="green1"><span>1</span>
                <h3>@lang('Seleccionar reserva')</h3>
            </li>
            <li id="green2"><span>2</span>
                <h3>@lang('Resumen reserva')</h3>
            </li>
            <li id="green3"><span>3</span>
                <h3>@lang('Prereserva realizada')</h3>
            </li>
        </ul>
    </div>

    <!--tab-1 start-->
    <form method="POST" action="{{ route($type . '.store') }}" >
        @csrf

        @include('Front::partials.edit-restaurant', ['reserve' => null])
    </form>
    <!--tab-1 end-->
@endsection