@extends('Front::layouts.main')

@section('page_header', __('Restaurante'))

@section('content')
    <!--tab-3 start-->
    <div id="tab3" class="inner-form tab-content  tab-open">
        <h2>@lang('Prereserva realizada con éxito')</h2>
        <p>@lang('Aravell Restaurant le enviará una notificación para confirmar definitivamente su reserva')</p>
        <ul class="fecha exito">
            <li>@lang('Fecha') <span>{{ ucfirst(\Jenssegers\Date\Date::createFromFormat('d/m/Y', $reserve->get_field('data'))->format('l d/m/Y')) }}</span></li>
            <li>@lang('Hora') <span>{{ $reserve->get_field('hora') }}</span></li>
            <li>@lang('Adultos')<span>{{ $reserve->get_field('adults') }}</span></li>
            <li>@lang('Niños')<span>{{ $reserve->get_field('nens') }}</span></li>
            <li>@lang('Sillitas para niños')<span>{{ $reserve->get_field('cadiretes-nens') }}</span></li>
        </ul>
        <div class="full-sec">
            <p>@lang('Para contactar directamente con Aravell Golf llame al (+34) <span>973 36 00 66</span>')</p>
        </div>
        <div class="inner-form volver">
            <ul class="fecha resumen">
                <li class="reser">
                    <input type="button" onclick="window.location = '{{ route('index') }}'" value="@lang('VOLVER')">
                </li>
            </ul>
        </div>
    </div>
    <!--tab-3 end-->
@endsection