@extends('Front::layouts.main')

@section('page_header', __('Privacidad'))

@section('content')
    <div class="soci-dades ex">
        <h2>@lang('Su contraseña de acceso ha sido actualizada con éxito')</h2>

        <div class="inner-form vol">
            <ul class="fecha resumen">
                <li class="reser con">
                    <input type="submit" onclick="window.location = '{{ route('user.privacity') }}';" value="@lang('VOLVER')">
                </li>
            </ul>
        </div>
    </div>
@endsection