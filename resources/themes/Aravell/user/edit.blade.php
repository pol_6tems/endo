@extends('Front::layouts.main')

@section('page_header', __('Mis datos'))

@section('content')
    <form method="POST" enctype="multipart/form-data">
        @csrf
        <div class="inner-form">
            <ul>
                <li>
                    <label>@lang('Nombre')</label>
                    <input type="text" name="name" placeholder="@lang('Nombre')" value="{{ $user->name }}">
                </li>
                <li>
                    <label>@lang('Apellido')</label>
                    <input type="text" name="lastname" placeholder="@lang('Apellido')" value="{{ $user->lastname }}">
                </li>
                <li>
                    <label>@lang('Email')</label>
                    <input type="email" name="email" placeholder="@lang('Email')" value="{{ $user->email }}">
                </li>
                <li>
                    <label>@lang('Nº de socio')</label>
                    <input type="text" name="" placeholder="1034">
                </li>
                <li>
                    <label>@lang('Fecha de nacimiento')</label>
                    <div class="due">
                        <input type="number" min="1" max="31" name="birth_day" placeholder="@lang('Día')" @if (isset($birthday) && $birthday)value="{{ $birthday->day }}"@endif>
                        <input type="number" min="1" max="12" name="birth_month" placeholder="@lang('Mes')" @if (isset($birthday) && $birthday)value="{{ $birthday->month }}"@endif>
                        <input type="text" pattern="\d{4}" name="birth_year" placeholder="@lang('Año')" @if (isset($birthday) && $birthday)value="{{ $birthday->year }}"@endif>
                    </div>
                </li>
                <li>
                    <label>@lang('Teléfono 1')</label>
                    <input type="text" name="telefon-1" placeholder="@lang('Teléfono 1')" @if ($user->get_field('telefon-1'))value="{{ $user->get_field('telefon-1') }}"@endif>
                </li>
                <li>
                    <label>@lang('Teléfono 2')</label>
                    <input type="text" name="telefon-2" placeholder="@lang('Teléfono 2')" @if ($user->get_field('telefon-2'))value="{{ $user->get_field('telefon-2') }}"@endif>
                </li>
                <li>
                    <label>@lang('Dirección')</label>
                    <input type="text" name="adreca" placeholder="@lang('Dirección')" @if ($user->get_field('adreca'))value="{{ $user->get_field('adreca') }}"@endif>
                </li>
                <li>
                    <label>@lang('Población')</label>
                    <input type="text" name="poblacio" placeholder="@lang('Población')" @if ($user->get_field('poblacio'))value="{{ $user->get_field('poblacio') }}"@endif>
                </li>
                <li>
                    <label>@lang('País')</label>
                    <input type="text" name="pais" placeholder="@lang('País')" @if ($user->get_field('pais'))value="{{ $user->get_field('pais') }}"@endif>
                </li>
                <li>
                    <label>@lang('Handicap de juego')</label>
                    <div class="qty-available">
                        <div id='myform'>
                            <input type='button' value='' class='qtyminus edit-usr' field='handicap' />
                            <input type='text' name='handicap' value='{{ $user->get_field('handicap')?: 0  }}' class='qty' @if ($user->get_field('handicap'))value="{{ $user->get_field('handicap') }}"@endif />
                            <input type='button' value='' class='qtyplus edit-usr' field='handicap' />
                        </div>
                    </div>
                </li>
                <li>
                    <label>@lang('Imagen de perfil')</label>
                    <div class="image-upload">
                        <label for="file-input"> <img id="img_avatar" class="avatar-img" src="{{ $user->get_avatar_url() }}"/><span class="edit"><img src="{{ asset($_front . 'images/edit-icon.svg') }}"/></span> </label>
                        <input id="file-input" name="avatar" type="file" />
                    </div>
                </li>
                <li class="tp">
                    <input type="submit" value="@lang('Guardar cambios')">
                </li>
            </ul>
        </div>
    </form>
@endsection


@section('bottom_foot_scripts')
    <script>
        document.getElementById("file-input").onchange = function () {
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                document.getElementById("img_avatar").src = e.target.result;
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        };

        $(document).ready(function() {
            @if (count($errors->all()))
                @foreach($errors->all() as $error)
                    toastr.error('{{ $error }}');
                @endforeach
            @endif
        });
    </script>
@endsection