@extends('Front::layouts.main')

@section('page_header', __('Wallet'))

@section('content')
    <div class="inner-form wall">
        <h2>@lang('Últimos movimientos')</h2>

        <div class="wallet ultimos">
            <ul>
                @foreach($transactions as $transaction)
                    <li>
                        <div class="walle-lft">
                            <h2>{{ $transaction->comments }}</h2>
                            <p>{{ $transaction->created_at->format('d/m/Y') }}</p>
                        </div>
                        <div class="walle-rgt">
                            @if ($transaction->amount >= 0)
                                <p>+{{ $transaction->amount }} €</p>
                            @else
                                <p class="red">{{ $transaction->amount }} €</p>
                            @endif

                        </div>
                    </li>
                @endforeach

                <li class="volver">
                    <input type="submit" onclick="window.location = '{{ route('user.wallet') }}';" value="@lang('VOLVER A WALLET')">
                </li>
            </ul>
        </div>
    </div>
@endsection