@extends('Front::layouts.main')

@section('page_header', __('Wallet'))

@section('content')

    <div class="soci-dades compra">
        <h2>@lang('Compra realizada con éxito')</h2>
        <p>@lang('Se han añadido :value <i class="fa fa-eur" aria-hidden="true"></i> a su Wallet.', ['value' => $amount])</p>
        <input type="submit" onclick="window.location = '{{ route('user.wallet') }}';" value="@lang('VOLVER A WALLET')">
    </div>
@endsection