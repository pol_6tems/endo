@extends('Front::layouts.main')

@section('page_header', __('Mis datos'))

@section('content')

    <div class="soci-dades">
        <h2 class="sus">@lang('Sus datos han sido<br> actualizados correctamente')</h2>
        <input type="submit" onclick="window.location = '{{ route('user.edit') }}';" value="@lang('VOLVER')">
    </div>
@endsection