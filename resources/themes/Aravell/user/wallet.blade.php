@extends('Front::layouts.main')

@section('page_header', __('Wallet'))

@section('content')
    <h3 style="color: red">TODO: Integració redsys + tarjetes guardades</h3>

    <form method="POST" enctype="multipart/form-data">
        @csrf
        <div class="inner-form">
            <h2>@lang('Tu saldo actual es')</h2>
            <h1 class="tu">{{ $user->get_field('saldo') ? format_money($user->get_field('saldo')): 0 }} €</h1>
            <p class="horas">@lang('¿Quiere actualizar su saldo? Indique la cantidad')</p>
            <ul class="button button-mob">
                <li>
                    <input type="number" class="js-wallet-add-input" name="amount" step="1" min="1" placeholder="" required>
                </li>
                <li>
                    <input id="anadir" type="button" value="@lang('AÑADIR')">
                </li>
            </ul>
            <div class="anadir">
                <h2>@lang('Está a punto de cargar <span class="js-wallet-add-title">0</span>€')</h2>
                <p class="horas">Seleccione su tarjeta:</p>
                <div class="credit">
                    <ul>
                        <li>
                            <input id="options" type="checkbox" name="field" value="options">
                            <label for="options"><span><span></span></span></label>
                        </li>
                        <li><img src="{{ asset($_front . 'images/mastercard.svg') }}" alt=""> </li>
                        <li>Mastercard</li>
                        <li><span>****</span><span>****</span> <span>****</span><span>****</span> <span>2012</span></li>
                    </ul>
                </div>
                <div class="credit">
                    <ul>
                        <li>
                            <input id="options1" type="checkbox" name="field" value="options">
                            <label for="options1"><span><span></span></span></label>
                        </li>
                        <li><img src="{{ asset($_front . 'images/visa.svg') }}" alt=""> </li>
                        <li>Visa</li>
                        <li><span>****</span><span>****</span> <span>****</span><span>****</span> <span>5025</span></li>
                    </ul>
                </div>
                <div class="credit">
                    <ul>
                        <li>
                            <input id="options2" type="checkbox" name="field" value="options">
                            <label for="options2"><span><span></span></span></label>
                        </li>
                        <li class="nueva">Nueva targeta</li>
                    </ul>
                </div>
                <ul class="fecha resumen m-b">
                    <li class="reser con">
                        <input type="submit" value="@lang('PROCEDER PAGO')">
                    </li>
                </ul>
            </div>
            <ul class="fecha top-0 fecha-mob">
                <li class="reser">
                    <input type="button" onclick="window.location = '{{ route('user.wallet-history') }}';" value="@lang('VER MOVIMIENTOS')">
                </li>
            </ul>
        </div>
    </form>
@endsection