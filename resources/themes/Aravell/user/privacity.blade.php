@extends('Front::layouts.main')

@section('page_header', __('Privacidad'))

@section('content')
    <form method="POST">
        @csrf

        <div class="soci-dades">
            <h2>@lang('Cambie la contraseña de acceso')</h2>
            <div class="pass-form">
                <ul>
                    <li>
                        <label>@lang('Nueva contraseña')</label>
                        <input type="password" name="password" placeholder="******">
                    </li>
                    <li>
                        <label>@lang('Repetir contraseña')</label>
                        <input type="password" name="password_confirmation" placeholder="******">
                    </li>
                    <li>
                        <input type="submit" value="@lang('GUARDAR CAMBIOS')">
                    </li>
                </ul>
            </div>
        </div>
    </form>
@endsection