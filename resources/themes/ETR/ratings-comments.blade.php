@php($valoraciones = do_action('get_ratings', $item->id))

<!-- VALORACIONES/COMENTARIOS -->
<div class="white-box-lft white-box-tit">
    
    @if ($valoraciones["num"] > 0)
    <!-- VALORACIONES -->
    @php ( do_action('show_ratings', $item->id) )
    @endif
    
    <!-- COMENTARIOS -->
    @php ( do_action('show_comments', $item->id) )

</div>
<!-- end VALORACIONES/COMENTARIOS -->