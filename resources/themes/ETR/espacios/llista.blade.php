@extends('Front::layouts.base')

@section('styles')
<link href="{{ asset($_front.'css/smk-accordion.css') }}" rel="stylesheet" />
<link href="{{ asset($_front.'css/mb.slider.css') }}" rel="stylesheet" />
<link href="{{ asset($_front.'css/datepicker.css') }}" rel="stylesheet" />
@endsection

@section('contingut')
@includeIf('Front::partials.header')
<!-- banner starts -->
<section class="banner-inner vol"> <img src="{{ asset($_front.'images/stone-header.jpg') }}" alt="">
    <div class="ban-search">
        <div class="row1">
            <!-- <h1><span>Lorem ipsum</span> dolor sit amet</h1> -->
        </div>
    </div>
</section>

<section class="home-title">
    <div class="row1">
        <h1>Los <span>mejores espacios</span> para retiros espirituales en todo el mundo</h1>
    </div>
</section>
<section class="search-sec voluntar espais">
    <div class="row1">
        <div class="es-pad">
            <ul>
                <li class="loca"><input type="text" placeholder="Localizacion"></li>
                <li class="capa">
                    <select class="select_box" placheolder="Capacidad">
                        <option>@Lang('Capacidad')</option>
                        @foreach(get_posts('espacios-capacidad') as $capacidad)
                            @php($t_capacidad = $capacidad->translate())
                            <option>{{ $t_capacidad->title }}</option>
                        @endforeach
                    </select>
                </li>
                <li><input type="submit" value="Buscar"></li>
            </ul>
        </div>
    </div>
</section>

<!-- banner ends -->
<section class="mbl-filter">
    <div class="row1">
        <h1><img src="{{ asset($_front.'images/title-arrw.jpg') }}" alt=""> 94 retiros de yoga en Espana</h1>
        <ul>
            <li><a href="javascript:void(0);" class="order"><img src="{{ asset($_front.'images/mbl-order.jpg') }}"
                        alt="">ORDENAR</a></li>
            <li><a href="javascript:void(0);" class="filter" id="slide"><img
                        src="{{ asset($_front.'images/mbl-filter.jpg') }}" alt="">FILTRO</a></li>
            <li><a href="javascript:void(0);" class="mapa"><img src="{{ asset($_front.'images/mbl-mapa.jpg') }}"
                        alt="">MAPA</a></li>
        </ul>
    </div>
    <div class="filter-menu">
        <h1><a href="javascript:void(0);"><img src="{{ asset($_front.'images/title-arrw.jpg') }}" alt="">Filtros</a>
        </h1>
        <div class="accordion">
            @includeIf('Front::partials.filters-llistes-mob')
        </div>
    </div>
</section>
<section class="retiros-sec">
    <div class="row1">
        <div class="ret-lft filters">
            @php($metas = getFilters('espacio', [
                'CAPACIDAD' => [
                    'post_type' => 'espacios-capacidad',
                    'custom_field' => 'capacidad',
                ],
                'ENTORNO' => [
                    'post_type' => 'espacios-entorno',
                    'custom_field' => 'entorn',
                ],
                'RÉGIMEN' => [
                    'post_type' => 'espacios-regimen',
                    'custom_field' => 'regimen',
                ],
                'PRESTACIONES' => [
                    'post_type' => 'prestaciones-espacios',
                    'custom_field' => 'prestaciones',
                ],
            ]))
        </div>
        <div class="ret-rgt">
            <div class="tit-div">
                <h1><span>{{ count(get_posts('espacio')) }} @Lang('espacios')</span> en España</h1>
                <div class="drop"> <span>@Lang('ORDENAR POR:')</span>
                    <div class="recom">
                        <select class="select_box">
                            <option>@Lang('Recomendados')</option>
                            <option>@Lang('Recomendados')</option>
                            <option>@Lang('Recomendados')</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="lista">

            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script src="{{ asset($_front.'js/datepicker.min.js') }}"></script>
<script src="{{ asset($_front.'js/datepicker.en.js') }}"></script>
<script src="{{ asset($_front.'js/smk-accordion.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.mb.slider.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.showmore.js') }}"></script>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function removeDups(names) {
        let unique = {};
        names.forEach(function(i) {
            if(!unique[i]) {
            unique[i] = true;
            }
        });
        return Object.keys(unique);
    }

function process_url(post_name = '', checked = false) {
    var url = new URL(window.location.href);

    // If Append
    if ( !checked ) {
        entries = url.searchParams.getAll('filter[]');
        entries = entries.filter(item => item !== post_name);

        url = new URL(window.location.origin + window.location.pathname);
        entries.forEach(function(e) {
            url.searchParams.append('filter[]', e);
        });
    } else {
        url.searchParams.append('filter[]', post_name);
    }

    var metas = [];
    url.searchParams.getAll('filter[]').forEach(function(el) {
        metas.push([$('[data-post_name="' + el + '"]').data('cf'), 'like', '%' + $('[data-post_name="' + el + '"]').data('id') + '%']);
    });

    return [url, metas];
}

$('.filters input').change(function() {
    var post_name = $(this).data('post_name');
    var [url, metas] = process_url(post_name, $(this).get(0).checked);

    carregarRetiros(metas, function() {
        window.history.pushState({},"", decodeURIComponent(url.href));
    });
});

function carregarRetiros(metas = [], callback = function() {}) {
    $('.retiros-sec').addClass('loading');
    $('.lista').fadeOut();
    $.ajax({
        url: ajaxURL,
        method: "POST",
        data: {
            action: "loadEspacios",
            method: 'html',
            parameters: {
                metas: metas
            }
        },
        success: function(data) {
            $('.retiros-sec').removeClass('loading');
            setTimeout(function(){ 
				$('.lista').html(data);
				$('.lista').fadeIn() }, 400);
            callback();
        }
    });
}

$(document).ready(function () {
    var [url, metas] = process_url();
    carregarRetiros(metas);
});
</script>
<script src="{{ asset($_front.'js/pages/llista.js') }}"></script>
@endsection