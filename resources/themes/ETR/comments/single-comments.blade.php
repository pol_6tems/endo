@php ( $comments = $params['comments'] )
@php ( $post_id = $params['post_id'] )
@php ( $post_url = $params['post_url'] )

<!-- COMENTARIOS -->
@if ( !empty($comments) && count($comments) > 0 )
<ul id="@Lang('Comments')" class="date-list fixta">
    @foreach ($comments as $c)
    <li>
        <h2>
            @if ($c->user){{ $c->user->fullname() }}@else @lang('Anónimo')@endif
            <span style="font-size: 13px;">{{ $c->created_at->format('d/m/Y') }}</span>
        </h2>

        @if ( !empty($c->user->city) )
            <h3>
                {{ $c->user->city }}
                &nbsp;
                {{ !empty($c->user->country) ? '(' . $c->user->country . ')' : '' }}
            </h3>
        @endif

        <p>{!! nl2br($c->comment) !!}</p>
    </li>
    @endforeach
</ul>
@endif


@if ( Auth::check() && (!isset($is_retiro) || !$is_retiro))
    <form id="FormAddComment" method="POST" action="{{ route('comments.store') }}">
        {{ csrf_field() }}

        <input type="hidden" name="post_id" value="{{ $post_id }}">
        <input type="hidden" name="redirect_to" value="{{ $post_url }}#@Lang('Comments')">

        <div class="frm-ryt">
            <div class="frm-grp" style="padding:0;padding-bottom: 25px;margin-top:0">
                <label>@Lang('Type your comment')</label>
                <textarea name="comment" class="frm-ctrl text-area" style="max-height: 100px;" required></textarea>
            </div>

            <!--<div class="frm-grp envir-but">
                <input type="submit" class="anim" value="Enviar">
            </div>-->
        </div>

        <input type="submit" class="ana-btn" value="@Lang('Add Comment')">

    </form>
@endif
<!-- end COMENTARIOS -->