@extends('Front::layouts.base')

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')

@endsection

@section('contingut')

    @includeIf('Front::partials.header')

    <section class="retiros-sec detalles-retiro buy-regalo confirm">
        <div class="row">
            <div class="retro-lft">
                <div class="reserva-done">
                    <h1><img src="{{ asset($_front.'images/tick-yellow.svg') }}"> @lang('Compra confirmada')</h1>
                    <p>@lang('Hemos enviado un email con todos los detalles de la compra a: <span>:email</span>', ['email' => auth()->user()->email])</p>
                </div>

                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border">
                        <h3>@lang('Si necesitas cualquier cosa, contacta con nosotros, estamos para ayudarte')</h3>
                        <ul>
                            <li>+34 972 664640</li>
                            <li>info@encuentraturetiro.com</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- right side -->
            @include('Front::partials.regalo-rgt')
        </div>
    </section>

@endsection

@section('scripts1')

@endsection

@section('analytics_ec')
    gtag('event', 'purchase', {
        "transaction_id": "{{ $purchase->order_id }}",
        "value": {{ toUserCurrency('EUR', $kit->get_field('precio') + $portes, false) }},
        "currency": "{{ userCurrency(true) }}",
        "items": [
            {
                "id": "{{ $kit->id }}",
                "name": "{{ $kit->title }}",
                "category": "Regalo retiro",
                "price": "{{ toUserCurrency('EUR', $kit->get_field('precio'), false) }}"
            },
        ]
    });
@endsection