@extends('Front::layouts.base')

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')

@endsection

@section('contingut')

    @includeIf('Front::partials.header')

    <form method="POST" enctype="multipart/form-data">
        @csrf

        <section class="retiros-sec detalles-retiro buy-regalo">
            <div class="row">
                <div class="retro-lft">
                    <div class="white-box-lft white-box-tit no-pad">
                        <div class="divider no-border">
                            <h2>@lang('Regalo para'):</h2>

                            <div class="frm-ryt">
                                <ul>
                                    <li>
                                        <div class="frm-grp">
                                            <label>@Lang('Nombre')</label>
                                            <input type="text" name="name" class="frm-ctrl" required>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="frm-grp">
                                            <label>@Lang('Apellidos')</label>
                                            <input type="text" name="lastname" class="frm-ctrl" required>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="frm-grp">
                                            <label>@Lang('Teléfono')</label>
                                            <input type="text" name="phone_number" class="frm-ctrl" required>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="frm-grp">
                                            <label>@Lang('Dirección')</label>
                                            <input type="text" name="address" class="frm-ctrl" required>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="frm-grp">
                                            <label>@Lang('Código postal')</label>
                                            <input type="text" name="cp" class="frm-ctrl" required>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="frm-grp">
                                            <label>@Lang('Localidad')</label>
                                            <input type="text" name="city" class="frm-ctrl" required>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                   
                    <div class="desimiler login-form">
                        <li class="check full-wid">
                            <input id="policy" type="checkbox" name="policy" value="options" required>
                            <label for="policy"><span><span></span></span>@Lang('Acepto la ')<a href="javascript:void(0);">@lang('Política de Privacidad')</a></label>
                        </li>
                        <li class="check full-wid">
                            <input id="policy-usr" type="checkbox" name="policy-usr" value="options" required>
                            <label for="policy-usr">
                                <span><span></span></span>
                                @Lang('He leído y acepto los <a href=":link" target="_blank">Términos y Condiciones Usuarios</a>', [ 'link' => get_page_url('terminos-y-condiciones-generales-de-contratacion') ])
                            </label>
                        </li>

                        <button type="submit" class="sub-btn">@Lang('Realizar pago seguro ahora') (@Lang('PAGAR') {{ toUserCurrency('EUR', $kit->get_field('precio') + $portes) }})</button>
                    </div>
                </div>

                <!-- right side -->
                @include('Front::partials.regalo-rgt')
            </div>
        </section>
    </form>
@endsection

@section('scripts1')

@endsection

@section('analytics_ec')
    gtag('event', 'begin_checkout', {
        "items": [
            {
                "id": "{{ $kit->id }}",
                "name": "{{ $kit->title }}",
                "category": "Regalo retiro",
                "price": "{{ toUserCurrency('EUR', $kit->get_field('precio'), false) }}"
            },
        ]
    });
@endsection