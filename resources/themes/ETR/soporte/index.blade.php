@extends('Front::layouts.base')

@section('contingut')
<!-- Header Corresponent -->
@includeIf('Front::partials.header_white')
    <!-- centro-yellow starts -->
    <section class="centro-yellow">
        <div class="row1">
            <h2>{{ $title }}</h2>
            <div class="search">
                <a href="javascript:void(0);" class="srch-btn search-txt"></a>
                <input type="text" placeholder="¿Qué estás buscando?" class="search-txt">
            </div>
            <h3>+34 972 46 28 07</h3>
        </div>
    </section>
    <!-- centro-yellow ends -->
    <!-- ayuda-sec starts -->
    <section class="ayuda-sec">
        <div class="row1">
            <div class="ayuda-top">
                <ul>
                    @foreach($categories as $categoria)
                        @php($t_categoria = $categoria->getTranslation( \App::getLocale() ))
                        <li><a class="{{ ($catActual->id == $categoria->id) ? 'active' : '' }}" href="{{ route('soporte.categories', $t_categoria->post_name) }}">{{ $t_categoria->title }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="dudas-lft">
                <ul>
                    @foreach($subcategories as $subcategoria)
                        @php($t_categoria = $subcategoria->get_field('categoria')->getTranslation( \App::getLocale() ))
                        @php($t_subcategoria = $subcategoria->getTranslation( \App::getLocale() ))
                        <li class="{{ ($subcatActual && $subcatActual->id == $subcategoria->id) ? 'active' : '' }}"><a href="{{ route('soporte.categories.subcategories', [$t_categoria->post_name, $t_subcategoria->post_name]) }}">{{ $t_subcategoria->title }}</a></li>
                    @endforeach
                </ul>
            </div>
            @if($subcatActual)
            <div class="dudas-rgt">
                <h2>{{ $subcatActual->getTranslation( \App::getLocale())->title }}</h2>
                <ul class="dudas-hab">
                    <li>¿Sed ut perspiciatis unde omnis iste natus error sit voluptatem?</li>
                    <li>¿Meacenas porttitor mus nisl eros senectus, tortor dis class malesuada?</li>
                    <li>¿Donec sociis ultrices orci sem condimentum, cras integer cubilia?</li>
                    <li>¿Senectus etiam donec ullamcorper aliquet id, curae posuere ut?</li>
                    <li>¿Massa vestibulum senectus per tempor odio, fringilla dapibus rhoncus sollicitudin?</li>
                    <li>¿Sed ut perspiciatis unde omnis iste natus error sit voluptatem?</li>
                    <li>¿laculis ligula luctus nec dapibus penatibus, dictumst felis faucibus montes?</li>
                </ul>
            </div>
            @endif
        </div>
    </section>
@endsection