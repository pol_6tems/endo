@extends('Admin::layouts.admin')

@section('section-title')
    @lang($section_title)
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <article class="card pm">
                <div class="card-header card-header-primary card-header-icon">
                    <h4 class="card-title">@lang('Detalles')</h4>
                </div>

                <div class="card-body pb-5">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>@lang('Compra')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Id'):</dt><dd>{{ $item->order_id ? $item->order_id : 'N/A' }}</dd>

                                @if ($item->status == \App\Modules\EncuentraTuRetiro\Models\Purchase::RESERVATION_STATUS_CONFIRMED)
                                    <dt>@lang('Id pago')</dt><dd>{{ $item->external_order_id }}</dd>
                                @endif

                                <dt>@lang('Status'):</dt><dd>{!! $item->present()->statusColored !!}</dd>
                                <dt>@lang('Created'):</dt><dd>{{ $item->created_at->format('d/m/Y H:m:s') }}</dd>
                                <dt>@lang('Updated'):</dt><dd>{{ $item->updated_at->format('d/m/Y H:m:s') }}</dd>
                            </dl>
                        </div>

                        <div class="col-md-6">
                            <h5>@lang('Regalo')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Kit'):</dt><dd><a href="{{ route('regalos.comprar', ['locale' => app()->getLocale(), 'kit' => $item->post->post_name]) }}" target="_blank">{{ $item->post->title }}</a> </dd>
                                <dt>@lang('Para'):</dt><dd>{{ $item->present()->extraFullname }}</dd>
                                <dt>@lang('Teléfono'):</dt><dd>{{ $item->present()->extraPhone }}</dd>
                                <dt>@lang('Dirección'):</dt><dd>{{ $item->present()->extraAddress }}</dd>
                            </dl>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5>@lang('Usuario')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Name'):</dt><dd>{{ $item->user->fullname() }}</dd>
                                <dt>@lang('Email'):</dt><dd>{{ $item->user->email }}</dd>
                            </dl>
                        </div>

                        <div class="col-md-6">
                            <h5>@lang('Pago')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Total')</dt><dd>{{ $item->present()->totalCurrency }}</dd>
                            </dl>
                        </div>
                    </div>

                </div>
            </article>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

    </script>
@endsection