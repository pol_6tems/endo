@extends('Admin::layouts.admin')

@section('section-title')
    @lang($section_title)
@endsection

@section('content')
    <div class="row">
        @if ($item->status != 0)
            <div class="col-lg-12">
                <article class="card">
                    <div class="card-header card-header-primary card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">library_books</i>
                        </div>
                        <h4 class="card-title">
                            @lang('Status')
                        </h4>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" action='{{ route($section_route . '.update', $item->id) }}' method="post">
                            @csrf

                            <input type="hidden" name="_method" value="PUT">

                            <div class="form-group">
                                <select name="status"
                                        class="selectpicker complaint-status"
                                        data-style="btn btn-primary"
                                        data-live-search="true">
                                    @foreach($statuses as $key => $status)
                                        <option value="{{ $key }}" @if ($item->status == $key) selected @endif>{{ $status }}</option>
                                    @endforeach
                                </select>
                                <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd pull-right" name="next" value="@lang('Update')">
                            </div>
                        </form>
                    </div>
                </article>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <article class="card pm">
                <div class="card-header card-header-primary card-header-icon">
                    <h4 class="card-title">@lang('Detalles')</h4>
                </div>

                <div class="card-body pb-5">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>@lang('Datos reserva')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Fecha review:')</dt><dd>{{ $item->created_at->toDateTimeString() }}</dd>
                                <dt>@lang('Reserva'):</dt><dd><a href="{{ route('admin.reservas.edit', ['id' => $item->purchase->id]) }}" target="_blank">{{ $item->purchase->order_id ? $item->purchase->order_id : 'N/A' }}</a></dd>
                                <dt>@lang('Retiro'):</dt><dd><a href="{{ route('admin.posts.edit', ['id' => $item->purchase->post->id]) }}" target="_blank">{{ $item->purchase->post->title }}</a></dd>
                                <dt>@lang('Cliente'):</dt><dd><a href="{{ route('admin.users.edit', ['id' => $item->purchase->user->id]) }}" target="_blank">{{ $item->purchase->user->fullname() }}</a></dd>
                                @if ($item->status == 0)
                                    <dt>@lang('Url valorar'):</dt><dd>{{ route('retiros.valora', ['post_name' => $item->purchase->post->post_name, 'token' => $item->hash]) }}</dd>
                                @endif
                            </dl>
                        </div>
                    </div>
                    @if ($item->status != \App\Modules\EncuentraTuRetiro\Models\Review::STATUS_NOT_REVIEWED)
                        <div class="row">
                            <div class="col-md-10">
                                <h5>@lang('Ratings')</h5>
                                <dl class="dl-horizontal">
                                    @foreach($ratings['children'] as $rating)
                                        <dt>{{ $rating->name }}:</dt><dd>{{ $item->present()->rating($rating->id) }}</dd>
                                    @endforeach
                                </dl>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <h5><strong>@lang('Me ha gustado')</strong></h5>
                                <dl class="dl-horizontal">
                                    <dt></dt><dd><p>{!! $item->like !!}</p></dd>
                                </dl>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <h5><strong>@lang('Algo a mejorar')</strong></h5>
                                <dl class="dl-horizontal">
                                    <dt></dt><dd><p>{!! $item->improve !!}</p></dd>
                                </dl>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10">
                                <h5><strong>@lang('ETR')</strong></h5>
                                <dl class="dl-horizontal">
                                    <dt>@lang('Experiencia ETR:')</dt><dd>{{ $item->etr }}</dd>
                                    <dt>@lang('Comment')</dt><dd><p>{!! $item->comment !!}</p></dd>
                                </dl>
                            </div>
                        </div>
                    @endif
                </div>
            </article>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

    </script>
@endsection