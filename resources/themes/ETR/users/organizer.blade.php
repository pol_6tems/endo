@extends('Front::layouts.base')

@section('contingut')
    @includeIf('Front::partials.header')
    <div class="process-steps">
    </div>

    <form method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}

        <input type="hidden" name="porcentaje-comision" value="18.15">

        <section class="retiros-sec no-pt reg-org-how">
            <div class="row">
                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border">
                        <h1>@lang('Para terminar el Registro de Organizador, Rellena este formulario')</h1>
                        <h2>@lang('¿Cómo funcionamos?')</h2>
                        <p>
                            <strong>1. Regístrate</strong> en este formulario y crea una cuenta de ORGANIZADOR donde podrás subir la información de anuncios de Retiros, Espacios o Voluntariados
                            <br><br>
                            <strong>2.</strong> Nos ponemos en contacto con vosotros, verificamos y <strong>Publicamos gratuitamente tus anuncios</strong>
                            <br><br>
                            <strong>3. Promocionamos tus anuncios</strong> y hacemos que lleguen a personas que están buscando lo que tu ofreces
                            <br><br>
                            <strong>4.</strong> RETIROS: <strong>Las Reservas confirmadas</strong> te llegarán vía email
                            <br><br>
                            <strong>5.</strong> RETIROS: Cobramos una <strong>comisión por éxito del 15%</strong> + (IVA) por cada reserva, que deducimos del depósito pagado por el usuario al formalizar la reserva
                            <br><br>
                            <strong>6</strong>. ESPACIOS: Para los gestores de casas y espacios de retiro <strong>el servicio de publicación será gratuito durante 3 Meses</strong> y después suscripción anual
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border">
                        <h2 class="reg-organizer-titles">@Lang('Empresa')</h2>
                        <div class="frm-ryt">
                            <ul class="reg-org">
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Nombre comercial')</label>
                                        <input type="text" name="nombre-comercial" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Localidad')</label>
                                        <input type="text" name="localidad" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('País')</label>
                                        <input type="text" name="pais" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Web')</label>
                                        <input type="text" name="web" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Actividad que realizáis')</label>
                                        <input type="text" name="actividad" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp avatar">
                                        <label>@Lang('Foto organizador')</label>
                                        <label for="avatar">
                                            <img id="img_avatar" src="{{ Auth::user()->get_avatar_url() }}" alt="">
                                        </label>
                                        <input id="avatar" type="file" name="foto-organizador">
                                    </div>
                                    {{--<div class="frm-grp">
                                        <label>@Lang('Foto organizador')</label>
                                        <input type="text" name="foto-organizador" class="frm-ctrl" >
                                    </div>--}}
                                </li>

                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border">
                        <h2 class="reg-organizer-titles">@Lang('Contactos')</h2>
                        <div class="frm-ryt">
                            <ul class="reg-org">
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Nombre Persona de Contacto')</label>
                                        <input type="text" name="nombre-persona-de-contacto" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Apellidos Persona de Contacto')</label>
                                        <input type="text" name="apellidos-persona-de-contacto" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Teléfono Persona de Contacto')</label>
                                        <input type="text" name="telefono-persona-de-contacto" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Email Persona de Contacto')</label>
                                        <input type="email" name="email-persona-de-contacto" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li></li>
                            </ul>
                        </div>

                        <p class="notes">Rellenar los campos de Persona Responsable de tu Organización si es diferente de la Persona de Contacto</p>

                        <div class="frm-ryt">
                            <ul class="reg-org">
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Nombre Responsable')</label>
                                        <input type="text" name="nombre-responsable" class="frm-ctrl" >
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Apellidos Responsable')</label>
                                        <input type="text" name="apellidos-responsable" class="frm-ctrl" >
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Teléfono Responsable')</label>
                                        <input type="text" name="telefono-responsable" class="frm-ctrl" >
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Email Responsable')</label>
                                        <input type="email" name="email-responsable" class="frm-ctrl" >
                                    </div>
                                </li>

                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border">
                        <h2 class="reg-organizer-titles">@Lang('Datos de facturación')</h2>
                        <div class="frm-ryt">
                            <ul class="reg-org">
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Nombre fiscal')</label>
                                        <input type="text" name="nombre-fiscal" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Dirección')</label>
                                        <input type="text" name="direccion-fiscal" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Localidad')</label>
                                        <input type="text" name="localidad-fiscal" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('País')</label>
                                        <input type="text" name="pais-fiscal" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Número identificación fiscal')</label>
                                        <input type="text" name="numero-identificacion" class="frm-ctrl" required>
                                    </div>
                                </li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border">
                        <h2 class="reg-organizer-titles">@Lang('¿Qué tipo de contenido quieres publicar?')</h2>
                        <div class="frm-ryt">
                            <?php
                                $params = json_decode($fields->where('name', 'que-tipo-de-contenido-quieres-publicar')->first()->params);
                                $choices = explode("\r\n", $params->choices);
                            ?>
                            <ul class="reg-org login-form">
                                @foreach($choices as $key => $choice)
                                    @php($choice = explode(':', $choice))
                                    <li>
                                        <div class="frm-grp">
                                            <input @if ($choice[0] == 'retiros')class="js-org-retiros"@endif id="cb_{{ $key }}_que_tipo" name="que-tipo-de-contenido-quieres-publicar[]" value="{{ $choice[0] }}" type="checkbox" checked="checked">
                                            <label for="cb_{{ $key }}_que_tipo"><span><span></span></span>@lang($choice[1])</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row js-policy-row">
                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border">
                        <h2 class="reg-organizer-titles">@Lang('Política de cancelación, pago y depósito')</h2>
                        <div class="frm-ryt">
                            <ul class="reg-org">
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Selecciona la Moneda preferida')</label>
                                        <select class="select_box" name="moneda">
                                            @foreach($currencies as $currency)
                                                <option value="{{ $currency->id }}" @if ($currency->id == 995)selected @endif>{{ $currency->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Escoge la Política de Cancelación de Reservas de los usuarios')</label>
                                        <div class="frm-radio">
                                            <?php
                                                $params = json_decode($fields->where('name', 'politica')->first()->params);
                                                $choices = explode("\r\n", $params->choices);
                                            ?>

                                            @foreach($choices as $key => $choice)
                                                @php($choice = explode(':', $choice))
                                                <input id="radio{{ $key }}_politica" name="politica" value="{{ $choice[0] }}" type="radio" {{ $key == 0 ? 'checked' : '' }}>
                                                <label for="radio{{ $key }}_politica">@lang($choice[1])</label>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Pago del resto del Retiro')</label>
                                        <div class="frm-radio">
                                            <?php
                                                $params = json_decode($fields->where('name', 'pago')->first()->params);
                                                $choices = explode("\r\n", $params->choices);
                                            ?>

                                            @foreach($choices as $key => $choice)
                                                @php($choice = explode(':', $choice))
                                                <input id="radio{{ $key }}_pago" name="pago" value="{{ $choice[0] }}" type="radio" {{ $key == 0 ? 'checked' : '' }}>
                                                <label for="radio{{ $key }}_pago">@lang($choice[1])</label>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="frm-grp">
                                        <label>@Lang('Depósito del usuario al formalizar la reserva')</label>
                                        <div class="frm-radio">
                                            <?php
                                                $params = json_decode($fields->where('name', 'deposito')->first()->params);
                                                $choices = explode("\r\n", $params->choices);
                                            ?>

                                            @foreach($choices as $key => $choice)
                                                @php($choice = explode(':', $choice))
                                                <input id="radio{{ $key }}_deposito" name="deposito" value="{{ $choice[0] }}" type="radio" {{ $key == 0 ? 'checked' : '' }}>
                                                <label for="radio{{ $key }}_deposito">@lang($choice[1])</label>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>
                                <li></li>
                            </ul>

                        </div>

                        <p class="notes">Nota: Los depósitos cobrados al cliente del 30%, 50% o 100% se ingresarán al organizador cuando haya vencido el plazo de cancelación descontado el 15% + IVA de comisión de éxito</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border">
                        <input type="submit" class="submit" value="@lang('Registrate')">
                    </div>
                </div>
            </div>
        </section>
    </form>
@endsection

@section('scripts1')
    <script>
        document.getElementById("avatar").onchange = function () {
            var reader = new FileReader();

            reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("img_avatar").src = e.target.result;
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        };

        $('.js-org-retiros').on('change', function () {
            if ($(this).is(':checked')) {
                $('.js-policy-row').slideDown('fast');
            } else {
                $('.js-policy-row').slideUp('fast');
            }
        });
    </script>
@endsection