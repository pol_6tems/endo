@extends(auth()->check() ? 'Front::layouts.profile' : 'Front::layouts.base')

@section(auth()->check() ? 'profile_content' : 'contingut')
    @if (!auth()->check())
        @includeIf('Front::partials.header')
        <section class="profile_content">
    @endif

    <div class="favoritos ret-rgt">
        <h1>@lang('Mi Lista')</h1>
        <ul class="ret-lst">
            <div class="lds-ripple"><div></div><div></div></div>
        </ul>
    </div>

    @if (!auth()->check())
        </section>
    @endif
@endsection

@section('scripts1')
    <script>
        $(function() {
            $('.ret-lst').addClass('loading');
            $.ajax({
                url: ajaxURL,
                method: "POST",
                data: {
                    action: "loadFavourites",
                    method: 'html'
                },
                success: function(data) {
                    $('.ret-lst').removeClass('loading');
                    $('.ret-lst').empty();
                    $('.ret-lst').html(data);
                    if ($('#count').val() == 0) {
                        $('.ret-lst').html(`
                        <div class="fav-message">
                            <span style="max-width: 85%">
                                @lang('Aquí puedes ver todos los retiros que hayas seleccionado marcando en el símbolo')
                            </span>
                            <span class="pin fav">
                                <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                            </span>
                        </div>`);
                    }
                },
                error: function(data) {
                    console.log(data);
                }
            });
        });
    </script>
@endsection