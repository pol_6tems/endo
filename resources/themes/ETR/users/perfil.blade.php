@extends('Front::layouts.profile')

@section('profile_content')
<form action="{{ route('perfil.update') }}" method="POST" enctype="multipart/form-data">
    @csrf
    @if (Session::get('success'))
        <div class="success">
            @Lang('Updated Profile')
        </div>
    @endif


    <div class="row1">
        <div class="flex-row">
            <div class="column">
                <div class="frm-ryt">
                    <div class="frm-grp">
                        <label for="name">@Lang('Name')</label>
                        @php($nombre = (count($errors->getBag('default')->get('name')) > 0) ? 1 : 0)
                        <input id="name" class="frm-ctrl {{ ($nombre) ? 'error' : '' }}" type="text" name="name" value="{{ Auth::user()->name }}">
                        @if ($nombre)
                            <small>
                                @foreach($errors->getBag('default')->get('name') as $message)
                                    {{ $message }}
                                @endforeach
                            </small>
                        @endif
                    </div>
        
                    <div class="frm-grp">
                        <label for="apellido">@Lang('Lastname')</label>
                        @php($apellido = (count($errors->getBag('default')->get('lastname')) > 0) ? 1 : 0)
                        <input id="apellido" class="frm-ctrl {{ ($apellido) ? 'error' : '' }}" type="text" name="lastname" value="{{ Auth::user()->lastname }}">
                        @if ($nombre)
                            <small>
                                @foreach($errors->getBag('default')->get('lastname') as $message)
                                    {{ $message }}
                                @endforeach
                            </small>
                        @endif
                    </div>
        
                    <div class="frm-grp">
                        <label for="email">@Lang('Email')</label>
                        @php($email = (count($errors->getBag('default')->get('email')) > 0) ? 1 : 0)
                        <input id="email" class="frm-ctrl {{ ($apellido) ? 'email' : '' }}" type="email" name="email" value="{{ Auth::user()->email }}">
                        @if ($email)
                            <small>
                                @foreach($errors->getBag('default')->get('email') as $message)
                                    {{ $message }}
                                @endforeach
                            </small>
                        @endif
                    </div>
        
                    <div class="frm-grp">
                        <label for="ciudad">@Lang('City')</label>
                        @php($ciudad = (count($errors->getBag('default')->get('city')) > 0) ? 1 : 0)
                        <input id="ciudad" class="frm-ctrl {{ ($ciudad) ? 'error' : '' }}" type="text" name="city" value="{{ Auth::user()->city }}">
                        @if ($ciudad)
                            <small>
                                @foreach($errors->getBag('default')->get('city') as $message)
                                    {{ $message }}
                                @endforeach
                            </small>
                        @endif
                    </div>
                    <div class="frm-grp">
                        <label for="pais">@Lang('País')</label>
                        @php($pais = (count($errors->getBag('default')->get('country')) > 0) ? 1 : 0)
                        <input id="pais" class="frm-ctrl {{ ($pais) ? 'error' : '' }}" type="text" name="country" value="{{ Auth::user()->country }}">
                        @if ($pais)
                            <small>
                                @foreach($errors->getBag('default')->get('country') as $message)
                                    {{ $message }}
                                @endforeach
                            </small>
                        @endif
                    </div>
                    <div class="frm-grp">
                        <label for="genero">@Lang('Gender')</label>
                        @php($genero = (count($errors->getBag('default')->get('gender')) > 0) ? 1 : 0)
                        <select class="select_box {{ ($genero) ? 'error' : '' }}" name="gender" id="genero">
                            <option value="hombre" {{ (Auth::user()->gender == 'hombre') ? 'selected' : '' }}>Hombre</option>
                            <option value="mujer" {{ (Auth::user()->gender == 'mujer') ? 'selected' : '' }}>Mujer</option>
                            <option value="otro" {{ (Auth::user()->gender == 'otro') ? 'selected' : '' }}>Otro</option>
                        </select>
                        @if ($genero)
                            <small>
                                @foreach($errors->getBag('default')->get('genero') as $message)
                                    {{ $message }}
                                @endforeach
                            </small>
                        @endif
                    </div>

                    <div class="frm-grp">
                        <label for="phone-number">@lang('Teléfono')</label>
                        <input id="phone-number" class="frm-ctrl" type="text" name="phone-number" value="{{ auth()->user()->get_field('phone-number') ? auth()->user()->get_field('phone-number') : old('phone-number', '') }}" required>
                    </div>
        
                    <div class="frm-grp">
                        <label>@Lang('Fecha de Nacimiento')</label>
                        @php( $cumpleanos = (count($errors->getBag('default')->get('cumpleanos')) > 0) ? 1 : 0 )
                        @php( $date = \Carbon\Carbon::parse(Auth::user()->birthdate) )
                        
                        <div class="col-3">
                            <label>@Lang('Day')</label>
                            <select class="select_box {{ ($cumpleanos) ? 'error' : '' }}" name="dia">
                                @for($j = 1 ; $j <= 31; $j++)
                                    <option value="{{ $j }}" {{ ($date->format('d') == $j) ? 'selected' : '' }}>{{ $j }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-3">
                            <label>@Lang('Month')</label>
                            <select class="select_box {{ ($cumpleanos) ? 'error' : '' }}" name="mes">
                                @for($j = 1 ; $j <= 12; $j++)
                                    <option value="{{ $j }}" {{ ($date->format('m') == $j) ? 'selected' : '' }}>{{ $j }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-3">
                            @php($firstDate = \Carbon\Carbon::parse('01-01-1900'))
                            <label>@Lang('Year')</label>
                            <select class="select_box {{ ($cumpleanos) ? 'error' : '' }}" name="ano">
                                @for($j = \Carbon\Carbon::now(); $j > \Carbon\Carbon::parse('01-01-1900'); $j->subYear())
                                    <option value="{{ $j->format('Y') }}" {{ ($date->format('Y') == $j->format('Y')) ? 'selected' : '' }}>{{ $j->format('Y') }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="avatar">
                    <label for="avatar">
                        <img id="img_avatar" src="{{ Auth::user()->get_avatar_url() }}" alt="">
                    </label>
                    <input id="avatar" type="file" name="avatar">
                </div>
                <div class="frm-ryt">
                    <div class="frm-grp">
                        <label for="password">@Lang('Password')</label>
                        @php($password = (count($errors->getBag('default')->get('password')) > 0) ? 1 : 0)
                        <input id="password" class="frm-ctrl {{ ($password) ? 'error' : '' }}" type="password" name="password" value="" autocomplete="off" required>
                        @if ($password)
                            <small>
                                @foreach($errors->getBag('default')->get('password') as $message)
                                    {{ $message }}
                                @endforeach
                            </small>
                        @endif
                    </div>
                    <div class="frm-grp">
                        <label for="password_confirmation">@Lang('Repeat Password')</label>
                        @php($password = (count($errors->getBag('default')->get('password')) > 0) ? 1 : 0)
                        <input id="password_confirmation" class="frm-ctrl {{ ($password) ? 'error' : '' }}" type="password" name="password_confirmation" value="" autocomplete="off" required>
                        @if ($password)
                            <small>
                                @foreach($errors->getBag('default')->get('password') as $message)
                                    {{ $message }}
                                @endforeach
                            </small>
                        @endif
                    </div>
                    <div class="frm-grp">
                        <button class="sumbit">@Lang('Update')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('scripts1')
<script>
@if (Session::get('success'))
$(function() {
    setTimeout(function() { $('.profile_content .success').slideUp(); }, 500);
});
@endif

document.getElementById("avatar").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("img_avatar").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};
</script>
@endsection