@extends('Front::layouts.profile')

@section('profile_content')
@php($transactions = get_transactions())
<div class="dharmas ret-rgt">
    <h1>Dharmas</h1>
    <div class="column col-7" style="padding: 0; min-height: 465px;">
        <div class="dharmas-list">
            <div class="head">
                <ul>
                    <li>Fecha</li>
                    <li>Concepto</li>
                    <li></li>
                </ul>
            </div>
            @if ($transactions)
            <div class="list">
                @foreach($transactions as $transaction)
                <div class="item">
                    <ul>
                        <li>{{ $transaction->created_at->format('d-m-Y H:i') }}</li>
                        <li>{{ $transaction->comments }}</li>
                        <li class="plus" @if ($transaction->amount < 0)style="color: #ca0000"@endif >{{ $transaction->amount >= 0 ? '+' : '-' }} {{ abs($transaction->amount) }} @Lang('dharmas')</li>
                    </ul>
                </div>
                @endforeach
            </div>
            @endif    
        </div>
    </div>
    <div class="column col-3 dharmas-rgt">
        <div class="total">
            <h2>Total Dharmas</h2>
            <div>{{ round(Auth::user()->dharmas, 0) }}</div>
            <span>({{ round(Auth::user()->dharmas/100, 0) }} €)</span>
        </div>
        <div class="help">
            <a href="{{ get_post(507)->get_url() }}" target="_blank" class="sumbit">¿Como conseguir dharmas?</a>
        </div>
    </div>
</div>
@endsection

@section('scripts1')

@endsection