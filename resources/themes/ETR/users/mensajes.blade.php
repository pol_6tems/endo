@extends('Front::layouts.profile')

@section('profile_content')
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet" href="{{ asset($_front.'css/jquery.mCustomScrollbar.css') }}" />
<link rel="stylesheet" href="{{ asset($_front.'css/perfil_mensajes.css') }}" />

<div class="cal-cnt pad-left cate">  
<div class="row1">

    <div class="cal-lft contact-l">
        <h2>@Lang('Contacta con nosotros')</h2>
        <p>@Lang('Envíanos tu consulta, petición y / o duda y te responderemos lo antes posible.')</p>
    </div>

    <!-- CHAT -->
    <div class="chat">

    @php( $active_chat = '' )
    @php( $active_chat_type = '' )
    @php( $last_date = '' )

    @if ( !empty($items) )
        <!-- CONVERSACIONES -->
        <div class="cal-lft contact-l users">
            <h3>@Lang('Conversaciones')</h3>

            <div class="conv-mob">
                <div class="conv-mob-expand"></div>
                @lang('Conversaciones')
            </div>

            <div class="contacts">
                @foreach ( $items as $key => $item )
                    @if ( $key == 0 )
                        @php( $active_chat = $item->chat )
                        @php( $active_chat_type = $item->type )
                    @endif
                    <div class="chat_list chat_id-{{$item->chat}} chat_type-{{$item->type}} {{$key == 0 ? 'active_chat' : ''}}" onclick="seleccionar_chat( '{{$item->chat}}', '{{$item->type}}' );">
                        <div class="chat_people">
                            <div class="chat_img">
                                <img class="avatar-icon" src="{{ $item->user_avatar }}">
                            </div>
                            <div class="chat_ib {{ !$item->last_mensaje->visto ? 'nuevo_mensaje' : '' }}">
                                <h5>
                                    {{ $item->user_name }}
                                    <span class="chat_date">
                                        {{ $item->last_mensaje->created_at->format('G:i') }}&nbsp;&nbsp;<span>{{ $item->last_mensaje->created_at->format('d/m/Y') }}</span>
                                    </span>
                                    <br>
                                    {{-- <i>{{ $item->user_email }}</i> --}}
                                </h5>

                            </div>
                            <p>{{ cut_text(preg_replace("/<br>/", ' ' , preg_replace('/\s\s+/', ' ', $item->last_mensaje->show_message(true)), 65)) }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- end CONVERSACIONES -->
  
        <!-- MENSAJES -->
        <div class="cal-rht contact-r">
            <h3>@lang('Chat')</h3>

            <!-- LOOP -->
            @foreach ( $items as $k => $item )
                <div class="message chat_type-{{$item->type}} contacts" id="chat_id-{{$item->chat}}" data-chat_id="{{ $item->chat_noencode }}" data-chat_type="{{ $item->last_mensaje->type }}" style="display:none;">
                    <ul>
                        <!-- Missatge inicial benvinguda -->
                        <li>
                            <div class="msg-c">
                                <img style="width: 200px;" src="{{asset($_front.'images/logo-encuentra-tu-retiro.svg')}}">
                                <h4>{{ get_app_name() }}<span></span></h4>
                                <p>
                                    @lang('Bienvenido / a'),
                                    @lang('Este es el espacio habilitado para poderte comunicar y plantear cualquier duda o consulta que tengas. Intentaremos darte una respuesta lo antes posible.')
                                </p>
                            </div>
                        </li>
                        <!-- end Missatge inicial benvinguda -->

                        @php( $last_date = null )
                        @foreach( $item->mensajes as $key => $mensaje )
                            @if ( $key == 0 )
                                @php( $last_date = $mensaje->created_at->format('M d') )
                                <li class="line"><span>{{ $mensaje->created_at->format('M d') }}</span></li>
                            @elseif ( $last_date != $mensaje->created_at->format('M d') )
                                <li class="line"><span>{{ $mensaje->created_at->format('M d') }}</span></li>
                            @endif

                            @php ( $user_avatar = $user_avatar_default )
                            @php ( $user_name = $mensaje->name )
                            @if ( !empty($mensaje->user_id) && !empty($mensaje->user) )
                                @php ( $user_avatar = $mensaje->user->get_avatar_url() )
                                @php ( $user_name = $mensaje->user->fullname() )
                            @endif

                            <li class="message-line {{ Auth::user()->id == $mensaje->user_id ? 'outgoing-message' : '' }}">
                                <div class="msg-r">
                                    <div class="msg-usr">
                                        <img src="{{ $user_avatar }}">
                                        <h4>
                                            {{ $user_name }}
                                        </h4>
                                        <span>{{ $mensaje->created_at->format('G:i') }}</span>
                                    </div>
                                    <p>{!! nl2br($mensaje->show_message()) !!}</p>
                                    @if ($mensaje->fitxer != '')
                                        <span class="fitxer"><a href="{{ asset('storage/'.$mensaje->fitxer) }}"><i class="material-icons">description</i></a></span>
                                    @endif
                                </div>
                            </li>

                            @php( $last_date = $mensaje->created_at->format('M d') )

                        @endforeach
                    </ul>
                    <input type="hidden" name="last_date" value="{{ $last_date }}">
                </div>
            @endforeach
            <!-- end LOOP -->

            <div class="form-pad">
                <input type="text" id="mensaje-box" class="txt-box" placeholder="@lang('Escribe tu comentario...')">

                <div class="selecc" style="display:none!important;">
                    <input id="filetext" type="text" placeholder="@Lang('No se han seleccionado archivo')" readonly>
                    <label id="#bb" style="top:5px;"> @Lang('Seleccionar archivo')
                        <input type="file" name="fitxer" id="file" >
                    </label>
                </div>

                <input id="enviar_mensaje_btn" type="button" value="Enviar">
            </div>
        </div>
        <!-- end MENSAJES -->
    @else
        <div class="cal-lft contact-l">
            <h3>@Lang('No tiene conversaciones activas.')</h3>
        </div>
    @endif
    </div>
    <!-- end CHAT -->

</div>
</div>
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script>
var $active_chat = "{{ $active_chat }}";
var $active_chat_type = "{{ $active_chat_type }}";
var $last_date = '{{ $last_date }}';

$(document).ready(function(){
    seleccionar_chat( '{{ $active_chat }}', '{{ $active_chat_type }}' );
    if ( $active_chat == '' ) $('.chat .form-pad').hide();
});

function seleccionar_chat( chat_id, chat_type ) {
    var chat_ele_selector = '#chat_id-' + chat_id + '.chat_type-' + chat_type;
    $('.message').hide();
    $('.chat_list').removeClass('active_chat');
    $('.chat_list.chat_id-' + chat_id + '.chat_type-' + chat_type).addClass('active_chat');
    $(chat_ele_selector).show();

    $(chat_ele_selector).animate({ scrollTop: $(chat_ele_selector).prop("scrollHeight")}, 0);
    $active_chat = chat_id;
    $active_chat_type = chat_type;
    $last_date = $(chat_ele_selector).find('input[type=hidden][name=last_date]').val();

    var char_id_noencode = $(chat_ele_selector).data('chat_id');

    var convMob = $('.conv-mob');
    if (convMob.hasClass('conv-mob-active')) {
        convMob.next('.contacts').slideUp(400, function () {
            $('html, body').animate({ scrollTop: $('.cal-rht.contact-r').offset().top + $('.cal-rht.contact-r').prop("scrollHeight") }, 300, 'swing', function () {
                $('#mensaje-box').focus();
            });
        });
        convMob.removeClass('conv-mob-active');
    }

    window.setTimeout(function(){
        $(".mCustomScrollbar").mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});
    }, 200);

    // Mensaje visto
    $.ajax({
        url: '{{ route('admin.mensajes.visto') }}',
        method: 'POST',
        data: {'_token': '{{csrf_token()}}', 'chat_id': char_id_noencode, 'chat_type': chat_type},
        success: function(data) {
            $('.chat_list.chat_id-' + chat_id + ' .chat_ib.nuevo_mensaje').removeClass('nuevo_mensaje');
        }
    });
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#mensaje-box').keyup(function(e) {
    if (e.keyCode == 13) {
        $('#enviar_mensaje_btn').click();
    }
});

$('#file').change(function(e) {
	var fitxer = $('#file')[0].files[0];
	$('#filetext').val(fitxer.name);
});

$('#enviar_mensaje_btn').click(function(e) {
    var user_id = {{ Auth::user()->id }};
    var mensaje = $('#mensaje-box').val();

    var chat_ele_selector = '#chat_id-' + $active_chat + '.chat_type-' + $active_chat_type;
    var char_id_noencode = $(chat_ele_selector).data('chat_id');
    
    var formData = new FormData();
    var fitxer = $('#file')[0].files[0];

    formData.append("_token", '{{csrf_token()}}');
    formData.append("mensaje", mensaje);
    formData.append("user_id", user_id);
    formData.append('fitxer', fitxer);
    formData.append('chat_id', char_id_noencode);
    formData.append('type', $active_chat_type);

    $.ajax({
        url: '{{ route('perfil.mensaje.store') }}',
        method: 'POST',
        processData: false, // important
        contentType: false, // important
        data: formData,
        success: function(data) {
            if ( data == 'KO' ) alert("@Lang('Error al enviar el missatge.')");
            var ahora = new Date();
            var hora = ahora.toLocaleString('es-ES', { hour: '2-digit', minute: '2-digit', hour12: false });
            var mes = ahora.toLocaleString('es-ES', { month: 'short' }).replace('.', '').capitalize();
            var dia = ahora.getDate();
            var fecha = mes + ' ' + dia;
            var missatge = '';
            if ( fecha != $last_date ) {
                $last_date = fecha;
                missatge += `<li class="line"><span>${fecha}</span></li>`;
            }
            missatge += `
            <li class="message-line outgoing-message">
                <div class="msg-r">
                    <div class="msg-usr">
                        <img src="{{ Auth::user()->get_avatar_url() }}">
                        <h4>{{ Auth::user()->fullname() }}</h4>
                        <span>${hora}</span>
                    </div>
                    <p>${mensaje}</p>
            `;
            if ( fitxer ) {
                missatge += `<span class="fitxer"><a><i class="material-icons">description</i></a></span>`;
            }
            
            missatge +=`</div></li>`;

            var chat_ele_selector = '#chat_id-' + $active_chat + '.chat_type-' + $active_chat_type;
            $(chat_ele_selector + '.message.contacts ul').append(missatge);
            $(chat_ele_selector + '.message.contacts').animate({ scrollTop: $(chat_ele_selector + '.message.contacts').prop("scrollHeight") }, 300);
            $('#mensaje-box').val('');
        }
    });
});
String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1)
};

$('.conv-mob').on('click', function () {
    if ($(this).hasClass('conv-mob-active')) {
        $(this).next('.contacts').slideUp();
        $(this).removeClass('conv-mob-active');
    } else {
        $(this).next('.contacts').slideDown();
        $(this).addClass('conv-mob-active');
    }
});
/* end Mensajes */
</script>
@endsection