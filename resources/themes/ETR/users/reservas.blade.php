@extends('Front::layouts.profile')

@section('profile_content')

    <div class="profile-reservas">
        <div class="row1">

            <div class="pr-title">
                <h2>@lang('Reservas')</h2>
                <p>@lang('Consulta tus reservas realizadas')</p>
            </div>

            <div class="pr-list">
                <ul>
                    @foreach($reservations as $reservation)
                        <li>
                            <div class="pr-itm">
                                <div class="pr-itm-img-mob">
                                    {!! $reservation->present()->firstImg !!}
                                </div>
                                <div class="pr-itm-cnt">
                                    <h3>{{ $reservation->post->title }}</h3>

                                    <div class="pr-itm-details">
                                        <dl class="dl-horizontal">
                                            <dt>@lang('Número')</dt><dd>{{ $reservation->order_id ?: $reservation->external_order_id }}</dd>
                                            <dt>@lang('Fecha Reserva')</dt><dd>{{ $reservation->created_at->format('j M, Y') }}</dd>
                                            <dt>@lang('Estado')</dt><dd>{!! $reservation->present()->statusColored !!}</dd>
                                            <dt>@lang('Retiro')</dt><dd>{{ $reservation->post->title }}</dd>
                                            <dt>@lang('Fechas')</dt><dd>{{ $reservation->present()->dates }}</dd>
                                            <dt>@lang('Plazas')</dt><dd>{{ $reservation->present()->people }}</dd>
                                            <dt>@lang('Habitación')</dt><dd>{{ $reservation->habitacio->title }}</dd>
                                            <dt>@lang('Precio final')</dt><dd>{{ $reservation->present()->totalPrice }}</dd>
                                            <dt>@lang('Depósito')</dt><dd>{{ $reservation->present()->payedPrice }}</dd>
                                            <dt>@lang('Resto al organizador')</dt><dd>{{ $reservation->present()->leftPrice }}</dd>
                                        </dl>
                                    </div>

                                    <div>
                                        <a class="todo-btn" href="{{ $reservation->post->get_url() }}">@lang('Ver retiro')</a>
                                    </div>
                                </div><div class="pr-itm-img">
                                    {!! $reservation->present()->firstImg !!}
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('scripts1')

@endsection