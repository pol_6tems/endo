@extends('Front::layouts.base')

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style1.css') }}" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
@endsection

@section('contingut')
@includeIf('Front::partials.header')
<section class="banner-inner"> <img src="{{ asset($_front.'images/foto-perfil-usuaris.jpg') }}" alt="">
    <div class="profile">
        <h1>{{ Auth::user()->name }} <span>{{ Auth::user()->lastname}}</span></h1>
    </div>
</section>

<nav class="breadcrumbs">
    <ul class="menu">
        <li><a href="{{ route('perfil.show') }}" class="{{ (url()->current() == route('perfil.show')) ? 'active' : '' }}" >@Lang('Profile')</a><i class="material-icons">chevron_right</i></li>
        <li><a href="{{ route('perfil.mensajes') }}" class="{{ (url()->current() == route('perfil.mensajes')) ? 'active' : '' }}">@Lang('Messages')</a><i class="material-icons">chevron_right</i></li>
        <li><a href="{{ route('perfil.favoritos') }}" class="{{ (url()->current() == route('perfil.favoritos')) ? 'active' : '' }}">@Lang('Mi Lista')</a><i class="material-icons">chevron_right</i></li>
        <li><a href="{{ route('perfil.comunidad') }}" class="{{ (url()->current() == route('perfil.comunidad')) ? 'active' : '' }}">@Lang('Comunidad')</a><i class="material-icons">chevron_right</i></li>
        <li><a href="{{ route('perfil.dharmas') }}" class="{{ (url()->current() == route('perfil.dharmas')) ? 'active' : '' }}">@Lang('Dharmas')</a><i class="material-icons">chevron_right</i></li>
        <li><a href="{{ route('perfil.reservas') }}" class="{{ (url()->current() == route('perfil.reservas')) ? 'active' : '' }}">@lang('Reservas')</a></li>
    </ul>
</nav>

<section class="profile_content @if (route_name('perfil.mensajes') || route_name('perfil.reservas'))profile_gray @endif">
    @yield('profile_content')
</section>
@endsection

@section('scripts1')
<script>
@if (Session::get('success'))
$(function() {
    setTimeout(function() { $('.profile_content .success').slideUp(); }, 500);
});
@endif

document.getElementById("avatar").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("img_avatar").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};
</script>
@endsection