<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    @php( get_header() )

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="{{ env('GOOGLE_SIGNIN_PUBLIC') }}">
    <meta name="google-site-verification" content="mJxsacdYHgya-RzuHn2aBj-wtphx8Dngs8M6nLUgcjE" />

    @if (!empty($_config['app_icon']['file']))
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset($_front.'images/favicons/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($_front.'images/favicons/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset($_front.'images/favicons/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset($_front.'images/favicons/site.webmanifest') }}">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        {{--
        <link rel="apple-touch-icon" sizes="76x76" href="{{ $_config['app_icon']['obj']->get_file_url() }}">
        <link rel="icon" type="image/png" href="{{ $_config['app_icon']['obj']->get_file_url() }}"> --}}
    @else
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset($_front.'images/favicons/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($_front.'images/favicons/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset($_front.'images/favicons/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset($_front.'images/favicons/site.webmanifest') }}">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
    @endif

    {{--<title>
        @if ( empty($_config['app_name']['value']) )
            @php( $app_name = config('app.name', 'Endo') )
        @else
            @php( $app_name = $_config['app_name']['value'] )
        @endif

        @yield('title', $app_name)
    </title>--}}

    <!--CSS Start-->
    @section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset($_front.'css/style1.css') }}" />
    <link rel="stylesheet" href="{{ asset($_front.'css/style2.css') }}" />
    @stop
    @yield('styles-parent')

    <link rel="stylesheet" href="{{ asset($_front.'css/ddsmoothmenu.css') }}" />
    <link rel="stylesheet" href="{{ asset($_front.'css/flexslider.css') }}" />
    <link rel="stylesheet" href="{{ asset($_front.'css/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset($_front.'css/jquery.selectbox.css') }}" />
    <link rel="stylesheet" href="{{ asset($_front.'css/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset($_front.'css/jquery.mmenu.all.css') }}" />
    <link rel="stylesheet" href="{{ asset($_front.'css/owl.carousel.css') }}" >
    <link rel="stylesheet" href="{{ asset($_front.'css/videobackground.css') }}">
    <link rel="stylesheet" href="{{ asset($_front.'css/font-awesome.css') }}" >
    <link rel="stylesheet" href="{{ asset($_front.'css/toastr.min.css') }}" >
    
    @yield('styles')

    <!--Responsive CSS-->
    <link rel="stylesheet" href="{{ asset($_front.'css/custom.css') }}" />
    <link rel="stylesheet" href="{{ asset($_front.'css/media.css') }}" />

    <script> var ajaxURL = "{{ route('ajax') }}"; var lang = '{{ app()->getLocale() }}';</script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <!-- Start of encuentraturetiro Zendesk Widget script -->
    @if (!Browser::isMobile())
        <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=0eb9fb89-9356-487a-b848-3794493f12d8"> </script>
    @endif

    <!-- End of encuentraturetiro Zendesk Widget script -->
    <!-- Crazy Egg -->
    <script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0090/4766.js" async="async"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77542129-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-77542129-1', {
            'currency': '{{ userCurrency(true) }}',
            @if (auth()->user())
                'user_id': '{{ auth()->user()->id }}'
            @endif
        });
    </script>
</head>
<body>

    @php( admin_bar() )

    <div class="hole_div">
        <!-- header starts -->
        <div id="page">
            @yield('contingut')
            @includeIf('Front::partials.footer')
        </div>
    </div>

    <div class="whatsappme__button">
        <a href="https://api.whatsapp.com/send?phone={{ setting('whatsapp_num') }}&text={{ rawurlencode(setting('whatsapp_text')) }}&source=&data=">
            <svg class="whatsappme__button__open" viewBox="0 0 24 24"><path fill="#fff" d="M3.516 3.516c4.686-4.686 12.284-4.686 16.97 0 4.686 4.686 4.686 12.283 0 16.97a12.004 12.004 0 0 1-13.754 2.299l-5.814.735a.392.392 0 0 1-.438-.44l.748-5.788A12.002 12.002 0 0 1 3.517 3.517zm3.61 17.043l.3.158a9.846 9.846 0 0 0 11.534-1.758c3.843-3.843 3.843-10.074 0-13.918-3.843-3.843-10.075-3.843-13.918 0a9.846 9.846 0 0 0-1.747 11.554l.16.303-.51 3.942a.196.196 0 0 0 .219.22l3.961-.501zm6.534-7.003l-.933 1.164a9.843 9.843 0 0 1-3.497-3.495l1.166-.933a.792.792 0 0 0 .23-.94L9.561 6.96a.793.793 0 0 0-.924-.445 1291.6 1291.6 0 0 0-2.023.524.797.797 0 0 0-.588.88 11.754 11.754 0 0 0 10.005 10.005.797.797 0 0 0 .88-.587l.525-2.023a.793.793 0 0 0-.445-.923L14.6 13.327a.792.792 0 0 0-.94.23z"></path></svg>
        </a>
    </div>
    
    @if (is_null(Cookie::get('cookies-accepted')))
    <div class="cookies">
        <div class="container">
            <div class="text">
                @lang('Utilizamos cookies propias y de terceros para mejorar la experiencia del usuario a través de su navegación. Si continúas navegando aceptas su uso. <a href=":url">Política de cookies</a>', ['url' => get_page_url('politica-de-cookies')])
            </div>
            <a class="cookies-link" onclick="cookies(event)">@lang('Acepto la Política de Cookies')</a>
        </div>
    </div>
    @endif

    <!--Common Script-->
    <script src="{{ asset($_front.'js/respond.js') }}"></script>
    <script src="{{ asset($_front.'js/html5.js') }}"></script>
    <script src="{{ asset($_front.'js/jquery-1.9.1.min.js') }}"></script>
    <script src="{{ asset($_front.'js/jquery.sticky.js') }}"></script>

    <script src="{{ asset($_front.'js/jquery.flexslider.js') }}"></script>
    {{-- <script src="{{ asset($_front.'js/ddsmoothmenu.js') }}"></script> --}}
    <script src="{{ asset($_front.'js/jquery.selectbox-0.2.js') }}"></script>
    
    <script type="text/javascript">
        function cookies(event) {
            event.preventDefault();
            var d = new Date();
            d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
            var expires = "expires="+ d.toUTCString();

            document.cookie = "cookies-accepted=1;path=/;"+expires;
            $('.cookies, .cookies-overlay').fadeOut();
        }


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $(window).load(function () {
            // flexslider
            $('.banner-home .flexslider').flexslider({
                animation: "fade",
                slideshowSpeed: 5000,
                animationDuration: 3000,
                easing: "swing",
                directionNav: false,
                controlNav: true,
                pausePlay: false
            });
        });
    </script>
    <script src="{{ asset($_front.'js/jquery.fancybox.js') }}"></script>
    <script src="{{ asset($_front.'js/select2.min.js') }}"></script>
    <script src="{{ asset($_front.'js/toastr.min.js') }}"></script>
    <script src="https://apis.google.com/js/api:client.js"></script>
    <script type="text/javascript">
        var googleUser = {};
        var startApp = function() {
            gapi.load('auth2', function(){
                // Retrieve the singleton for the GoogleAuth library and set up the client.
                auth2 = gapi.auth2.init({
                    client_id: '{{ env('GOOGLE_SIGNIN_PUBLIC') }}',
                    cookiepolicy: 'single_host_origin'
                });
                attachSignin(document.getElementById('google-sign-btn'));
                attachSignin(document.getElementById('google-signin-btn'));
            });
        };

        function attachSignin(element) {
            auth2.attachClickHandler(element, {},
                function(googleUser) {
                    onSignIn(googleUser);
                }, function(error) {
                    console.log(error);
                });
        }

        var $ = jQuery.noConflict();
        // $j is now an alias to the jQuery function; creating the new alias is optional.
        $(document).ready(function () {
            $.ajax({
                url: ajaxURL,
                method: 'POST',
                data: {
                    action: 'checkLocation',
                },
                success: function(data) {},
                error: function(data) {}
            });

            $('.fancybox, .fancybox1').fancybox();

            @if (old('email') && ($errors->has('email') || $errors->has('password')))
                $('#login-popup-btn').click();
            @endif

            @if (old('nombre') && old('correo') && ($errors->has('nombre') || $errors->has('apellido') || $errors->has('correo') || $errors->has('contrasena')))
                @if (old('policy-terms-org'))
                    $('.login-reg-buttons a[data-type="org"]').click();
                    $('.js-reg-type[data-type="org"]').click();
                @else
                    $('.login-reg-buttons a[data-type="usr"]').click();
                    $('.js-reg-type[data-type="usr"]').click();
                @endif
            @endif
        });

        $('.js-reg-type').on('click', function () {
            $('.reg-type-active').removeClass('reg-type-active');
            $(this).parent().addClass('reg-type-active');

            $('#regOrganizer').prop('checked', $(this).data('type') === 'org');

            var usrNameLabel = $('.js-usr-name');

            if ($(this).data('type') === 'org') {
                $('.js-last-name').hide();
                $('.js-usr-policy').hide();
                $('#policy-login-usr').removeProp('required');
                $('#policy-login-org').prop('required', true);
                $('#policy-terms-org').prop('required', true);
                $('.js-policy-org').show();
                usrNameLabel.html(usrNameLabel.data('org-string'))
            } else {
                $('.js-policy-org').hide();
                $('#policy-login-usr').prop('required', true);
                $('#policy-login-org').removeProp('required');
                $('#policy-terms-org').removeProp('required');
                $('.js-last-name').show();
                $('.js-usr-policy').show();
                usrNameLabel.html(usrNameLabel.data('usr-string'))
            }
        });

        $('.toggle-password').on('click', function () {
            var pwdInput = $(this).prev('.pass-word');
            pwdInput.prop('type', pwdInput.prop('type') === 'text' ? 'password' : 'text');
        });

        $(document).on('click', '.pin', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var $that = $(this);

            var $parent = $(this).closest('.obr-pad');

            $parent.addClass('loading');

            var postId = $(this).data('post-id');
            var postType = $(this).data('post-type');

            if (typeof postId === 'undefined') {
                postId = $parent.attr('id');
                postType = $parent.data('post-type');
            }

            $.ajax({
                url: ajaxURL,
                method: 'POST',
                data: {
                    action: 'storeFavourite',
                    parameters: {
                        post: postId,
                        type: postType
                    }
                },
                success: function(data) {
                    $parent.removeClass('loading');

                    if (data.result.original.exists) {
                        $that.removeClass('clear');
                        $that.find('.tooltip').css('display', 'block');
                    } else {
                        $that.addClass('clear');
                        $that.find('.tooltip').css('display', 'none');
                    }
                },
                error: function() {

                }
            });
        });

        function onSignIn(googleUser) {
            // Useful data for your client-side scripts:
            var profile = googleUser.getBasicProfile();

            // The ID token you need to pass to your backend:
            var id_token = googleUser.getAuthResponse().id_token;

            $.ajax({
                url: ajaxURL,
                method: 'post',
                data: {
                    action: 'signInGoogle',
                    parameters: {
                        token: id_token,
                        name: profile.getGivenName(),
                        lastname: profile.getFamilyName(),
                        email: profile.getEmail(),
                    }
                },
                success: function(data) {
                    if ($('#regOrganizer').is(':checked')) {
                        window.location.href = '{{ route('perfil.register-organizer') }}';
                    } else {
                        location.reload();
                    }
                }
            });
        }

        @if (!auth()->check())
            startApp();
        @endif
    </script>

    <script>
        @yield('analytics_ec')
    </script>

    @include('Front::partials.scripts.notifications')

    @yield('scripts1')

    <script src="{{ asset($_front.'js/common.js') }}"></script>
    <script src="{{ asset($_front.'js/pages/newsletter.js') }}"></script>
    <script type="text/javascript">
        (function($){
            function loadBgLazy () {
                $('.bg-lazy-img').each(function () {
                    var elementTop = $(this).offset().top;
                    var elementBottom = elementTop + $(this).outerHeight();
                    var viewportTop = $(window).scrollTop();
                    var viewportBottom = viewportTop + $(window).height();
                    var bgImg = $(this).data('bg-img');
                    if (elementBottom > viewportTop && elementTop < viewportBottom && typeof bgImg !== 'undefined') {
                        $(this).css({'background-image': bgImg})
                        $(this).removeData('bg-img');
                    }
                });
            }

            function customLoadBgLazy () {
                $('.custom-lazy-img').each(function () {
                    var elementTop = $(this).offset().top;
                    var elementBottom = elementTop + $(this).outerHeight();
                    var viewportTop = $(window).scrollTop();
                    var viewportBottom = viewportTop + $(window).height();
                    var imgUrl = $(this).data('src');
                    if (elementBottom > viewportTop && elementTop < viewportBottom && typeof imgUrl !== 'undefined' && $(this).is(":visible")) {
                        $(this).attr('src', imgUrl)
                        $(this).removeClass('custom-lazy-img');
                    }
                });
            }

            $(window).on('resize scroll', function() {
                customLoadBgLazy();
                loadBgLazy();
            });

            $(window).load(function() {
                customLoadBgLazy();
                loadBgLazy();
            });

        }(jQuery));
    </script>
    <div id="new"></div>
</body>

</html>