@extends('Front::layouts.base')

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style1.css') }}" />
@endsection

@section('contingut')
@includeIf('Front::partials.header')
<!-- banner starts -->
<section class="banner-inner vol"> <img src="{{ asset($_front.'images/stone-header.jpg') }}" alt="">
    <div class="ban-search">
        <div class="row1">
            <!-- <h1><span>Lorem ipsum</span> dolor sit amet</h1> -->
        </div>
    </div>
</section>
<section class="search-sec voluntar">
    <div class="row1">
        <div class="es-pad">
            <ul>
                <li class="loca"><input id="buscador" type="text" placeholder="Localizacion"></li>
                <li><input type="submit" value="Buscar"></li>
            </ul>
        </div>
    </div>
</section>
<!-- banner ends -->
<section class="mbl-filter">
    <div class="row1">
        <h1><img src="{{ asset($_front.'images/title-arrw.jpg') }}" alt=""> 94 retiros de yoga en Espana</h1>
        <ul>
            <li><a href="javascript:void(0);" class="order"><img src="{{ asset($_front.'images/mbl-order.jpg') }}" alt="">ORDENAR</a></li>
            <li><a href="javascript:void(0);" class="filter" id="slide"><img src="{{ asset($_front.'images/mbl-filter.jpg') }}" alt="">FILTRO</a></li>
            <li><a href="javascript:void(0);" class="mapa"><img src="{{ asset($_front.'images/mbl-mapa.jpg') }}" alt="">MAPA</a></li>
        </ul>
    </div>
    <div class="filter-menu">
        <h1><a href="javascript:void(0);"><img src="{{ asset($_front.'images/title-arrw.jpg') }}" alt="">Filtros</a>
        </h1>
        <div class="accordion">
            <div class="accordion_example2">
                <!-- Section 1 -->
                
            </div>
        </div>
    </div>
</section>

<section class="retiros-sec">
    <div class="row1">
        <div class="ret-lft filters">
            @php($metas = getFilters('voluntariado', [
                'TRABAJO' => [
                    'post_type' => 'voluntarios-trabajos',
                    'custom_field' => 'trabajos-a-realizar',
                ],
                'DURACIÓN MÍNIMA' => [
                    'post_type' => 'voluntariado-duracion-minima',
                    'custom_field' => 'duracion-minima',
                ],
                'ENTORNO' => [
                    'post_type' => 'voluntariado-entorno',
                    'custom_field' => 'entorno',
                ],
                'IDIOMA REQUERIDO' => [
                    'post_type' => 'voluntarios-idioma',
                    'custom_field' => 'idioma',
                ],
                'PRESTACIONES' => [
                    'post_type' => 'voluntarios-prestaciones',
                    'custom_field' => 'prestaciones',
                ],
            ]))
        </div>
        <div class="ret-rgt">
            <div class="tit-div">
                <h1><span>{{ count(get_posts('voluntariado')) }} @Lang('voluntariados')</span> en Espana</h1>
                <div class="drop"> <span>@Lang('ORDENAR POR:')</span>
                    <div class="recom">
                        <select class="select_box">
                            <option>@Lang('Recomendados')</option>
                            <option>@Lang('Recomendados')</option>
                            <option>@Lang('Recomendados')</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="lista">

            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.fancybox.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.showmore.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.mb.slider.js') }}"></script>
<script src="{{ asset($_front.'js/smk-accordion.js') }}"></script>

<script type="text/javascript">
    function removeDups(names) {
        let unique = {};
        names.forEach(function(i) {
            if(!unique[i]) {
            unique[i] = true;
            }
        });
        return Object.keys(unique);
    }

    function process_url(post_name = '', checked = false) {
        var url = new URL(window.location.href);

        // If Append
        if ( !checked ) {
            entries = url.searchParams.getAll('filter[]');
            entries = entries.filter(item => item !== post_name);

            url = new URL(window.location.origin + window.location.pathname);
            entries.forEach(function(e) {
                url.searchParams.append('filter[]', e);
            });
        } else {
            url.searchParams.append('filter[]', post_name);
        }

        var metas = [];
        url.searchParams.getAll('filter[]').forEach(function(el) {
            metas.push([$('[data-post_name="' + el + '"]').data('cf'), 'like', '%' + $('[data-post_name="' + el + '"]').data('id') + '%']);
        });

        return [url, metas];
    }

    $('.filters input').change(function() {
        var post_name = $(this).data('post_name');
        var [url, metas] = process_url(post_name, $(this).get(0).checked);

        carregarRetiros(metas, function() {
            window.history.pushState({},"", decodeURIComponent(url.href));
        });
    });

    function carregarRetiros(metas = [], callback = function() {}) {
        $('.retiros-sec').addClass('loading');
        $('.lista').fadeOut();
        $.ajax({
            url: ajaxURL,
            method: "POST",
            data: {
                action: "loadVoluntariados",
                method: 'html',
                parameters: {
                    metas: metas
                }
            },
            success: function(data) {
                $('.retiros-sec').removeClass('loading');
                setTimeout(function(){ 
                    $('.lista').html(data);
                    $('.lista').fadeIn() }, 400);
                callback();
            }
        });
    }

    $(document).ready(function () {
        var [url, metas] = process_url();
        carregarRetiros(metas);
    });


    var $ = jQuery.noConflict();
    // $j is now an alias to the jQuery function; creating the new alias is optional.
    $(document).ready(function () {
        $('.fancybox1').fancybox();
        
        $('#slide, .filter-menu h1 a').click(function () {
            var hidden = $('.filter-menu');
            if (hidden.hasClass('visible')) {
                hidden.animate({
                    "left": "-1000px"
                }, "slow").removeClass('visible');
                $('body').removeClass('hide');
            } else {
                hidden.animate({
                    "left": "0px"
                }, "slow").addClass('visible');
                $('body').addClass('hide');
            }
        });

    });
</script>
@endsection