@extends('Front::layouts.base')

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
<link href="{{ asset($_front.'css/smk-accordion.css') }}" rel="stylesheet" />
<link href="{{ asset($_front.'css/mb.slider.css') }}" rel="stylesheet" />
<link href="{{ asset($_front.'css/datepicker.css') }}" rel="stylesheet" />
@endsection

@section('extra_metas')
    @php($canonical = \App\Post::get_archive_link('espacios-casas-retiro'))
    @php($pageParam = request('page'))
    @if ($pageParam && $pageParam > 1)
        @php($canonical .= '?page=' . $pageParam)
    @endif

    <link rel="canonical" href="{{ $canonical }}"/>
@endsection

@section('contingut')
    @php($filters = request('filter', []))
@includeIf('Front::partials.header')
<!-- banner starts -->
<section class="banner-inner esp"> <img src="{{ asset($_front.'images/llistat-espacios.jpg') }}" alt="">
    <div class="ban-search">
        <div class="row1">
            <!-- <h1><span>Lorem ipsum</span> dolor sit amet</h1> -->
        </div>
    </div>
</section>

<section class="home-title">
    <div class="row1">
        <h1>@Lang('Los <span>mejores espacios</span> para organizar retiros todo el mundo')</h1>
    </div>
</section>
<section class="search-sec voluntar espais">
    <div class="row1">
        <div class="es-pad">
            <form id="form_retiro" action="{{ route('buscador') }}" method="POST">
                @csrf
                <input type="hidden" id="localidad" name="localization[localidad]" value="{{ request('city', '') }}">
                <input type="hidden" id="provincia" name="localization[provincia]" value="{{ request('province', '') }}">
                <input type="hidden" id="region" name="localization[region]" value="{{ request('region', '') }}">
                <input type="hidden" id="pais" name="localization[pais]" value="{{ request('country', '') }}">
                <input type="hidden" id="coordenadas" name="localization[coordenadas]" value="{{ request('coords', '') }}">
                <input type="hidden" name="post_type" value="espacio">
                <ul>
                    <li class="loca"><input id="search" type="text" placeholder="@lang('Localización')" value="{{ request('locInput', '') }}"></li>
                    <li class="capa">
                        <select id="cercador" name="capacidad" placheolder="@lang('Capacidad')">
                            <option>@Lang('Capacidad')</option>
                            @foreach(get_posts('espacios-capacidad') as $capacidad)
                                @php($t_capacidad = $capacidad->translate())
                                <option @if(in_array($t_capacidad->post_name, $filters))selected @endif>{{ $t_capacidad->title }}</option>
                            @endforeach
                        </select>
                    </li>
                    {{-- <li><input id="submit" type="submit" value="Buscar" disabled></li> --}}
                </ul>        
            </form>
        </div>
    </div>
</section>
@php($nEspacios = count(get_posts('espacio')))
<!-- banner ends -->
<section class="mbl-filter">
    <div class="yellow-title">
        <h2>
            <span class="num_espacios">{{ $nEspacios }} @Lang('espacios')</span> @Lang('donde organizar tu Retiro')
        </h2>
    </div>

    <div class="row1">
        <ul>
            <li class="order recom">
                <img src="{{ asset($_front.'images/icons/ordenar.svg') }}" alt="">
                <select class="select_box">
                    <option value="rating_desc">Recomendado</option>
                    <option value="price_asc">Precio total de menor a mayor</option>
                    <option value="price_night_asc">Precio por noche de menor a mayor</option>
                    <option value="duration_asc">Duración</option>
                </select>
            </li>
            <li>
                <div class="filter-container">
                    <a href="javascript:void(0);" class="filter" id="slide">
                        <img src="{{ asset($_front.'images/icons/filtrar.svg') }}" >@lang('Filtro')
                        <div class="filter-arrow"></div>
                    </a>
                </div>
            </li>
            {{-- <li><a href="javascript:void(0);" class="mapa"><img src="{{ asset($_front.'images/mbl-mapa.jpg') }}">MAPA</a></li> --}}
        </ul>
    </div>
    <div class="filter-menu filters">
        <h4><a href="javascript:void(0);"><img src="{{ asset($_front.'images/title-arrw.jpg') }}" alt="">Filtros</a>
        </h4>
        <div class="accordion">
            {!! getFilters('espacio', [
                'CAPACIDAD' => [
                    'post_type' => 'espacios-capacidad',
                    'custom_field' => 'capacidad',
                    'desplegable' => true,
                ],
                'ENTORNO' => [
                    'post_type' => 'espacios-entorno',
                    'custom_field' => 'entorn',
                    'desplegable' => true,
                ],
                'RÉGIMEN' => [
                    'post_type' => 'espacios-regimen',
                    'custom_field' => 'regimen',
                    'desplegable' => true,
                ],
                'PRESTACIONES' => [
                    'post_type' => 'prestaciones-espacios',
                    'custom_field' => 'prestaciones',
                    'desplegable' => true,
                ],
            ], true) !!}
        </div>
    </div>
</section>
<section class="retiros-sec">
    <div class="lds-ripple"><div></div><div></div></div>
    <div class="row1">
        <div class="ret-lft filters">
            {!! getFilters('espacio', [
                'CAPACIDAD' => [
                    'post_type' => 'espacios-capacidad',
                    'custom_field' => 'capacidad',
                    'desplegable' => true,
                ],
                'ENTORNO' => [
                    'post_type' => 'espacios-entorno',
                    'custom_field' => 'entorn',
                    'desplegable' => true,
                ],
                'RÉGIMEN' => [
                    'post_type' => 'espacios-regimen',
                    'custom_field' => 'regimen',
                    'desplegable' => true,
                ],
                'PRESTACIONES' => [
                    'post_type' => 'prestaciones-espacios',
                    'custom_field' => 'prestaciones',
                    'desplegable' => true,
                ],
            ]) !!}
        </div>
        <div class="ret-rgt">
            <div class="tit-div">
                <h2>
                    <span class="num_espacios">{{ $nEspacios }} @Lang('espacios')</span> @Lang('donde organizar tu Retiro')
                </h2>
                <div class="drop"> <span>@Lang('ORDENAR POR:')</span>
                    <div class="recom ordenar-por">
                        <select class="select_box">
                            <option value="rating_desc">Recomendado</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="lista">
                <ul class="ret-lst">
                    @for($i = 0; $i < 10; $i++)
                    <li class="skeleton">
                        <div class="obr-pad">
                            <div class="obr-img">
                                <img width="285" height="270" src="" alt="">
                            </div>
                            <div class="obr-cnt">
                                <h3></h3>
                                <h4></h4>
                                <span></span>
                            </div>
                            <div class="price">
                                <div class="mob-price">
                                    <div class="p-rgt">
                                        <h2></h2>
                                    </div>
                                    <div class="p-lft">
                                        <div class="rating"></div>
                                        <a class="mas-btn"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endfor
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script src="{{ asset($_front.'js/datepicker.min.js') }}"></script>
<script src="{{ asset($_front.'js/datepicker.'.$language_code.'.js') }}"></script>
<script src="{{ asset($_front.'js/smk-accordion.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.mb.slider.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.showmore.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ isset($_config['google_maps']) ? $_config['google_maps']['value'] : '' }}&libraries=places"></script>
<script>
var metas_or, coords = "{{ request('coords') }}";
var $espacios_txt = '@Lang('espacios')';
var xhr = null;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function process_url(post_name = '', checked = false) {
    metas_or = [];
    var url = new URL(window.location.href);

    // If Append
    if ( !checked ) {
        entries = url.searchParams.getAll('filter[]');
        entries = entries.filter(item => item !== post_name);

        url = new URL(window.location.origin + window.location.pathname);
        entries.forEach(function(e) {
            url.searchParams.append('filter[]', e);
        });
    } else {
        url.searchParams.append('filter[]', post_name);
    }

    var metas = [];
    var filters = [];
    url.searchParams.getAll('filter[]').forEach(function(el) {
        metas.push([$('[data-post_name="' + el + '"]').data('cf'), 'like', '%' + $('[data-post_name="' + el + '"]').data('id') + '%']);
        filters.push(el);
        metas_or.push($('[data-post_name="' + el + '"]').data('cf'));
    });

    return [url, metas, filters];
}

function carregarRetiros(metas = [], callback = function() {}, filters = []) {

    var order = $('.recom select').val();
    /*console.log(xhr);*/
    if (xhr) xhr.abort();

    xhr = $.ajax({
        url: ajaxURL,
        method: "POST",
        data: {
            action: "loadVoluntariadosEspacios",
            method: 'html',
            parameters: {
                post_type: 'espacio',
                metas: metas,
                metas_or: metas_or,
                order: order,
                coords: coords,
                filter: filters
            }
        },
        success: function(data) {
            setTimeout(function(){ 
				$('.lista').html(data);
                var num_lista_items = $('#count').val();
                $('.num_espacios').html( num_lista_items + ' ' + $espacios_txt);
            }, 400);
            callback();
            xhr = null;
        }
    });
}

$(document).ready(function () {
    var [url, metas, filters] = process_url();
    carregarRetiros(metas);
    $('#cercador').select2();
});

var map, autocomplete, marker;
function autocomplete() {
    var input = document.getElementById("search");
    var options = {
        types: ['geocode', 'establishment']
    };
    autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        
        document.getElementById('coordenadas').value = place.geometry.location.lat() + "," +  place.geometry.location.lng();
    
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }
        
        changeLocation(place);
    });
}

autocomplete();

$("#cercador").change(function() {
    $("#form_retiro").submit();
});
</script>
<script src="{{ asset($_front.'js/pages/llista.js') }}"></script>
@endsection