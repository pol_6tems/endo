@extends('Front::layouts.base')

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
    <link type="text/css" rel="stylesheet" href="{{ asset($_front.'css/org-easy-responsive-tabs.css') }}" />
@endsection

@section('contingut')
@php($galeria = $item->get_field('galeria'))
@php($localitzacio = $item->get_field('localizacion'))
<!-- Header Corresponent -->
@includeIf('Front::partials.header')

<section class="retiros-sec centros-sec">
    <div class="row1">
        <div class="centro-rht ret-tit-lft">
            {{-- <h1>{{ $trans->title }}</h1> --}}
            <h1>@lang('Alojamiento Retiro')</h1>

            @if ($item->get_field('localizacion'))
            <ul class="date-icon">
                <li class="loc">{{ $localitzacio->adreca }}</li>
            </ul>
            @endif

            <div class="flexslider centro organizador mobile">
                <ul class="slides">
                    @if ($galeria = $item->get_field('galeria'))
                        @foreach($galeria as $img)
                        <li data-thumb="{{ $img->get_thumbnail_url() }}">
                            <img src="{{ $img->get_thumbnail_url() }}">
                        </li>
                        @endforeach
                    @endif
                </ul>
            </div>

            <p>{{ strip_tags(remove_style_tags(html_entity_decode($item->description))) }}</p>
        </div>
        <div class="centro-lft">
            <div class="flexslider centro organizador">
                <ul class="slides">
                    @if ($galeria = $item->get_field('galeria'))
                        @foreach($galeria as $img)
                        <li data-thumb="{{ $img->get_thumbnail_url() }}">
                            <img src="{{ $img->get_thumbnail_url() }}">
                        </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
</section>

@php($retiros = get_posts([
    'post_type' => 'retiro',
    'metas' => [
        ['alojamiento', $item->id]
    ]
]))
@php($facilitadores = collect())
<section class="retiros-sec centro-p">
    <div class="row1">
        <div class="retiros-top tabs">
            <ul class="resp-tabs-list links">
                <li class="active"><a href="javascript:void(0);">@Lang('Retiros')</a></li>
                <li><a href="javascript:void(0);">@Lang('Facilitadores')</a></li>
            </ul>
            <div class="obres retrios-lis resp-tabs-container">
                <div>
                    <ul class="list">
                        @foreach($retiros as $retiro)
                            @php($aux = $retiro->get_field('facilitadores'))
                            @if ($aux)
                                @foreach($aux as $item)
                                    @php($facilitadores->push($item['facilitador']['value']))
                                @endforeach
                            @endif

                            @includeIf('Front::partials.single-retiro', ['retiros' => $retiros])
                        @endforeach
                    </ul>
                </div>
                
                <!-- FACILITADORES -->
                <div>
                    <ul class="facil-lst retiro">
                        @foreach($facilitadores->unique() as $fac)
                            @if ( !is_object($fac) ) @continue @endif
                            <li style="width: 100%;">
                                <div class="ret-tit-lft">
                                    <div class="lft-img">
                                        @if ($fac->media())
                                            <a href="{{ $fac->get_url() }}">
                                                <img src="{{ $fac->media()->get_thumbnail_url() }}" alt="">
                                            </a>
                                        @endif
                                    </div>
                                    <div class="cont-div">
                                        @php($name = explode(" ", $fac->title)[0])
                                        <h3><span>{{ $name }}</span></h3>
                                        <p>{{ get_excerpt($fac->description, 45) }} <a href="{{ $fac->get_url() }}">@Lang('Leer más')</a></p>
                                    </div>
                                </div>
                                <div class="ret-tit-rgt"> <a href="{{ $fac->get_url() }}" target="_blank" class="en-btn">@Lang('Ver Ficha')</a></div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <!-- end FACILITADORES -->

            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.showmore.js') }}"></script>
<script src="{{ asset($_front.'js/easyResponsiveTabs.js') }}"></script>
<script src="{{ asset($_front.'js/smk-accordion.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.mb.slider.js') }}"></script>
<script src="{{ asset($_front.'js/pages/organizadores.js') }}"></script>
<script>


$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "fade",
    controlNav: "thumbnails",
    slideshowSpeed:5000,
    animationDuration: 3000,
    directionNav: false,
  });
});

$('.tabs').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion           
    width: 'auto', //auto or any width like 600px
    fit: true,   // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    activate: function(event) { // Callback function if tab is switched
        var $tab = $(this);
        var $info = $('#tabInfo');
        var $name = $('span', $info);

        $name.text($tab.text());

        $info.show();
    }
});
</script>
@endsection