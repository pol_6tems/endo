@extends('Front::layouts.base')

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
    <link type="text/css" rel="stylesheet" href="{{ asset($_front.'css/org-easy-responsive-tabs.css') }}" />
@endsection

@section('contingut')
@php($description = $item->get_field('descripcion'))
@php($idiomas = $item->get_field('idioma'))
@php($galeria = $item->get_field('galeria'))
@php($localitzacio = $item->get_field('ubicacion'))
<!-- Header Corresponent -->
@includeIf('Front::partials.header')

<section class="retiros-sec centros-sec">
    <div class="row1">
        <div class="centro-rht ret-tit-lft">
            <h1>{{ $trans->title }}</h1>
            
            @if ($item->get_field('ubicacion'))
            <ul class="date-icon">
                <li class="loc">{{ $localitzacio->adreca }}</li>
            </ul>
            @endif

            <p>{{ strip_tags($description) }}</a></p>

            <div class="centros-list centros-list-img">
                <ul>
                    <li>Tasa de respuesta: <span>100%</span></li>
                    <li>Tiempo de respuesta: <span>En unas horas</span></li>
                    <li>Idioma:
                        <span>
                        @if ( !empty($idiomas) )
                            @foreach($idiomas as $key => $idioma)
                                @php($t_idioma = $idioma->translate())
                                {{ $t_idioma->title }}{{ ($key < count($idiomas)-1) ? ',' : ''}}
                            @endforeach
                        @endif
                        </span>
                    </li>
                </ul>
            </div>
            <div class="escrib"><img src="{{ asset($_front.'images/escribme-logo.png') }}"></div>
        </div>
        <div class="centro-lft">
            @if ( !empty($galeria) )
            <div class="flexslider centro organizador">
                <ul class="slides">
                    @if($trans->get_thumbnail_url())
                    <li data-thumb="{{ $trans->get_thumbnail_url() }}">
                        <img src="{{ $trans->get_thumbnail_url() }}" />
                    </li>
                    @endif
                    @foreach($galeria as $imatge)
                        @if ($imatge)
                            <li data-thumb="{{ $imatge->get_thumbnail_url() }}">
                                <img src="{{ $imatge->get_thumbnail_url() }}" />
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
</section>


<section class="retiros-sec centro-p">
    <div class="row1">
        <div class="retiros-top tabs">
            <ul class="resp-tabs-list links">
                <li class="active"><a href="javascript:void(0);">Retrios</a></li>
                <li><a href="javascript:void(0);">Facilitadores</a></li>
                <li><a href="javascript:void(0);">Publicaciones</a></li>
                <li><a href="javascript:void(0);">Valoraciones</a></li>
            </ul>
            <div class="obres retrios-lis resp-tabs-container">
                <div>
                    <ul>
                        <li class="item">
                            <div class="obr-pad"> <a href="javascript:void(0);">
                                    <div class="obr-img"> <img src="{{ asset($_front.'images/woods.jpg') }}" alt="">
                                        <span class="pin"><img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                                            <div class="tooltip"><span class="tooltiptext">ANADIR A LA LISTA</span> </div>
                                        </span>
                                    </div>
                                    <div class="obr-cnt">
                                        <h1>7 dias de retiro de yoga y meditacion con ceremonia</h1>
                                        <ul>
                                            <li class="date">28 oct. al 4 nov.2018</li>
                                            <li class="loc">Limon, Cahuita, Costa Rica</li>
                                        </ul>
                                        <p>Dedicado completamente a retiros que alimentan el
                                            espiritu, y que permiten descubrir un poco mas de
                                            este mundo natural tan maravilloso.</p>
                                    </div>
                                    <div class="price">
                                        <div class="p-lft"> <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-full.svg') }}"
                                                    alt=""></a> <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-full.svg') }}"
                                                    alt=""></a> <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-full.svg') }}"
                                                    alt=""></a> <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-full.svg') }}"
                                                    alt=""></a> <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-empty.svg') }}"
                                                    alt=""></a> <span>4/5</span> </div>
                                        <div class="p-rgt">
                                            <h2>1350 <i class="fa fa-eur" aria-hidden="true"></i></h2>
                                        </div>
                                    </div>
                                </a> </div>
                        </li>
                        <li class="item">
                            <div class="obr-pad"> <a href="javascript:void(0);">
                                <div class="obr-img"> <img src="{{ asset($_front.'images/exercise.jpg') }}" alt=""> <span class="exp-btn">RETIRO RECOMENDADO</span> </div>
                                <div class="obr-cnt"> <span class="red-tag">iSolo quedan 2 plazas!</span>
                                    <h1>5 dias de retiro de yoga e inteligencia emocional</h1>
                                    <ul>
                                        <li class="date">12 al 17 nov.2018</li>
                                        <li class="loc">Limon, Cahuita, Costa Rica</li>
                                    </ul>
                                    <p>Encontraras un espacio donde cuidarte, escucharte estar contigo</p>
                                    <span class="dis-txt">10% dto. reservando antes del 01 nov. 2018 189 </span>
                                </div>
                                <div class="price">
                                    <div class="p-lft"> <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-empty.svg') }}"
                                                alt=""></a> <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-empty.svg') }}"
                                                alt=""></a> <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-empty.svg') }}"
                                                alt=""></a> <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-empty.svg') }}"
                                                alt=""></a> <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-empty.svg') }}"
                                                alt=""></a> <span>0/5</span>
                                    </div>
                                    <div class="p-rgt">
                                        <h3>210 <i class="fa fa-eur" aria-hidden="true"></i></h3>
                                        <h2>189 <i class="fa fa-eur" aria-hidden="true"></i></h2>
                                    </div>
                                </div>
                            </a> </div>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul></ul>
                </div>
                <div>
                    <ul></ul>
                </div>
                <div>
                    <ul></ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.showmore.js') }}"></script>
<script src="{{ asset($_front.'js/easyResponsiveTabs.js') }}"></script>
<script src="{{ asset($_front.'js/smk-accordion.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.mb.slider.js') }}"></script>
<script src="{{ asset($_front.'js/pages/organizadores.js') }}"></script>
<script>
$('.tabs').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion           
    width: 'auto', //auto or any width like 600px
    fit: true,   // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    activate: function(event) { // Callback function if tab is switched
        var $tab = $(this);
        var $info = $('#tabInfo');
        var $name = $('span', $info);

        $name.text($tab.text());

        $info.show();
    }
});
</script>
@endsection