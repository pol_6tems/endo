@extends('Front::layouts.base')

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style1.css') }}" />
@endsection

@section('styles')
    <link href="{{ asset($_front.'css/datepicker.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset($_front.'css/flexslider.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('contingut')
@php
    $translate = $item->translate();
    $gmaps = $item->get_field('ubicacion-y-localizacion');
    $author = $item->author;

    $trabajos = $item->get_field('trabajos-a-realizar');
    $prestaciones = $item->get_field('prestaciones');
    $idiomas = $item->get_field('idioma');
    $entornos = $item->get_field('entorno');
    $duracion_minima = $item->get_field('duracion-minima');
    $disponibilitat = $item->get_field('cuando-disponible');
    $galeria = $item->get_field('galeria');
@endphp

@includeIf('Front::partials.header')
<!-- banner starts -->
<section class="banner-load retiros">
    @includeIf('Front::partials.single-gallery', ['gallery' => $galeria, 'post' => $item])
</section>
<div id="flexslider2" class="flexslider mob-slider">
    <ul class="slides">
        @if ( !empty($galeria) )
            @foreach($galeria as $imatge)
                @if ($imatge)
                <li><img src="{{ $imatge->get_thumbnail_url("medium") }}" alt="">
                    <div class="flex-ccn">
                        {{-- <h2>Lorem ipsum</h2> --}}
                    </div>
                </li>
                @endif
            @endforeach
        @endif
    </ul>
</div>
<!-- banner ends -->
<section class="retiros-sec voluntariado">
    <div class="row">
        <div class="retro-lft">
            <div class="white-box-lft">
                @if ($author)
                    <div class="ret-tit-rgt"> <img class="rounded" src="@if ($author->get_field('foto-organizador')){{ $author->get_field('foto-organizador')->get_thumbnail_url("medium") }}@else{{ $author->get_avatar_url() }}@endif" alt="">
                        <h3>{{ $author->fullname() }}</h3>
                    </div>
                @endif
                <div class="ret-tit-lft description">
                    <h1>{{ $item->title }}</h1>
                    <ul class="date-icon">
                        @if ( $gmaps )
                        <li class="loc">{{ print_location($gmaps, ['locality', 'administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'country']) }}</li>
                        @endif
                    </ul>
                    
                    <div class="descripcion-vermas">
                        <p>{!! $item->description !!}</p>
                    </div>
                    <button class="btn-org more">@Lang('Ver más')</button>
                </div>
            </div>
            <div class="white-box-lft white-box-tit">
                <h2>@Lang('Caracteristicas')</h2>
                <div class="carac-list">
                    <ul>
                        <li>
                            <div class="cara-lft">
                                <div class="ls-lft">
                                    <i class="sabad"></i>
                                    <h3>@Lang('Ubicación:')</h3>
                                </div>
                                @if ( $gmaps && $gmaps->adreca )
                                <div class="ls-rgt"> {{ $gmaps->adreca }} </div>
                                @endif
                            </div>
                            @if (isset($entornos) && count($entornos) > 0)
                            <div class="cara-rgt">
                                <div class="ls-lft">
                                    <i class="entorno"></i>
                                    <h3>@Lang('Entorno:')</h3>
                                </div>
                                <div class="ls-rgt">
                                    @php($entorn = '')
                                    @foreach($entornos as $entorno)
                                        @php($t_entorno = $entorno->getTranslation( \App::getLocale() ))
                                        @php($entorn .= $t_entorno->title.', ')
                                    @endforeach
                                    {{ rtrim($entorn, ', ') }}
                                </div>
                            </div>
                            @endif
                        </li>
                        
                        @if (isset($disponibilitat))
                        <div class="cara-full">
                            <ul>
                                <li>
                                    <div class="cara-rgt">
                                            <div class="ls-lft alt">
                                                <i class="disponibilidad"></i>
                                                <h3>@Lang('Disponibilidad:')</h3>
                                            </div>
                                        <div class="ls-rgt alt"> {{ $disponibilitat }} </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        @endif

                        @if (isset($trabajos) && count($trabajos) > 0)
                        <div class="cara-full">
                            <ul>
                                <li>
                                    <div class="cara-full-lft">
                                        <i class="trabajo"></i>
                                        <h3>@Lang('Trabajo:')</h3>
                                    </div>
                                    <div class="cara-full-rgt">
                                        <ul>
                                            @foreach($trabajos as $trabajo)
                                                @php($t_trabajo = $trabajo->getTranslation( \App::getLocale() ))
                                                <li><span class="tick-lst">{{ $t_trabajo->title }}</span> </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        @endif
                        <ul>
                            <li>
                                <div class="cara-rgt">
                                    @if($duracion_minima)
                                        <div class="ls-lft">
                                            <i class="duracion-min"></i>
                                            <h3>@Lang('Duración mínima:')</h3>
                                        </div>
                                        <div class="ls-rgt">
                                            @php($duraciones = '')
                                            @foreach($duracion_minima as $duracion)
                                                @php($t_duracion = $duracion->getTranslation( \App::getLocale() ))
                                                @php($duraciones .= $t_duracion->title.' o ')
                                            @endforeach
                                            {{ rtrim($duraciones, ' o ') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="cara-lft">
                                    <div class="ls-lft alt">
                                        <i class="horas-trabajo"></i>
                                        <h3>@Lang('Hora de trabajo semanales:')</h3>
                                    </div>
                                    <div class="ls-rgt alt"> {{ $item->get_field('horas-diarias') }} </div>
                                </div>
                            </li>
                        </ul>
                        <div class="cara-full">
                            <ul>
                                @if (isset($prestaciones) && count($prestaciones) > 0)
                                <li class="nb nbb">
                                    <div class="cara-full-lft ffl">
                                        <i class="facilidades"></i>
                                        <h3>@Lang('Prestaciones:')</h3>
                                    </div>
                                    <div class="cara-full-rgt">
                                        <ul>
                                            @foreach($prestaciones as $prestacion)
                                                @php($t_prestacion = $prestacion->getTranslation( \App::getLocale() ))
                                                <li> <span class="tick-lst">{{ $t_prestacion->title }}</span> </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                                @endif
                            </ul>
                        </div>
                        <div class="cara-full">
                            <ul>
                                @if (isset($idiomas) && count($idiomas) > 0)
                                <li>
                                    <div class="cara-lft">
                                        <div class="ls-lft">
                                            <i class="habit"></i>
                                            <h3 class="active">@Lang('Idioma:')</h3>
                                        </div>
                                        <div class="ls-rgt">
                                            @php($idiomes = '')
                                            @foreach($idiomas as $idioma)
                                                @php($t_idioma = $idioma->getTranslation( \App::getLocale() ))
                                                @php($idiomes .= $t_idioma->title.', ')
                                            @endforeach
                                            {{ rtrim($idiomes, ', ') }}
                                        </div>
                                    </div>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>


            @if ($item->get_field('espacio-y-entorno') != '')
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Espacio y Entorno')</h2>
                <p>{{ strip_tags($item->get_field('espacio-y-entorno')) }}</p>
            </div>
            @endif

            @if ($item->get_field('informacion-adicional') != '')
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Información Adicional')</h2>
                <p>{{ strip_tags($item->get_field('informacion-adicional')) }}</p>
            </div>
            @endif

            @if ( $gmaps )
                @php($coordenades = explode(',', $gmaps->coordenades))
                @if (count($coordenades) > 1)
                    <div class="white-box-lft white-box-tit white-box-para">
                        <h2>@Lang('Ubicacion')</h2>
                        <div id="gmaps" class="box-img" style="height: 500px;"></div>
                    </div>
                @endif
            @endif

            @if ($author)
            <div class="white-box-lft white-box-tit">
                <h2 class="el-mob">@Lang('El centro') <span>- {{ $author->fullname() }}</span></h2>
                <ul class="facil-lst rerr">
                    <li>
                        <div class="ret-tit-lft">

                            <div class="lft-img"> <img class="rounded" src="@if ($author->get_field('foto-organizador')){{ $author->get_field('foto-organizador')->get_thumbnail_url("medium") }}@else{{ $author->get_avatar_url() }}@endif" alt=""> </div>
                            <div class="cont-div">
                                <h2 class="cont-mob">{{ $author->fullname() }}</h2>
                                {{--<p>{{ get_excerpt(strip_tags($organizador->get_field('descripcion')), 40) }}<a href="{{ $organizador->get_url() }}"> @Lang('Leer mas')</a></p>--}}
                            </div>
                        </div>
                        {{--<div class="ret-tit-rgt full-wmob"> <a href="{{ $organizador->get_url() }}" class="en-btn">@Lang('Ver Ficha')</a>
                        </div>--}}
                    </li>
                </ul>
            </div>
            @endif

            <!-- RATINGS / COMMENTS -->
            @includeIf('Front::ratings-comments')

        </div>
        <!-- right side -->

        <form id="FormEnviarMensaje" class="messaging" style="position: relative;">
            <div class="lds-ripple"><div></div><div></div></div>
            <input type="hidden" name="type" value="voluntariado">
            <input type="hidden" name="params[Voluntariado]" value="{{ $translate->title }}">
            <input type="hidden" name="params[Organizador]" value="{{ ($author) ? $author->fullname() : 'empty' }}">
            <input type="hidden" name="params[vol_url]" value="{{ $item->get_url() }}">
            <input type="hidden" name="no_mensaje_allowed" value=true>
            <input type="hidden" name="to" value="{{ ($author) ? $author->id : 0 }}">

        <div class="retro-rgt">
            <div class="white-box-rgt">
                <div class="frm-ryt">
                    <h2 class="frm-title">@lang('¿Te interesa?')</h2>
                    
                    @if ( !Auth::check())
                        <div class="frm-grp">
                            <label>@Lang('Nombre')</label>
                            <input type="text" name="name" class="frm-ctrl" placeholder="@Lang('Nombre')" required>
                        </div>
                        <div class="frm-grp">
                            <label>@Lang('Email')</label>
                            <input type="email" name="email" class="frm-ctrl" placeholder="@Lang('Email')" required>
                        </div>
                    @else
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                    @endif

                    <div class="frm-grp">
                        <label>@lang('Teléfono')</label>
                        <input type="text" name="params[Teléfono]" class="frm-ctrl" required>
                    </div>
                    <div class="frm-grp">
                        <label>@lang('Mensaje')</label>
                        <textarea name="params[Mensaje]" class="frm-ctrl text-area" required></textarea>
                    </div>
                    <div class="message-sent" style="padding: 10px;margin: 10px;display:none;">
                        {{ __(':name sent successfully', ['name' => __('Message')]) }}
                    </div>
                    <div class="desimiler login-form">
                        <li class="check full-wid">
                            <input id="policy" type="checkbox" name="policy" value="options" required>
                            <label for="policy">
                                <span>
                                    <span></span>
                                </span>
                                <div>@Lang('He leído y acepto la <a href=":link" target="_blank">Política de Privacidad</a>', [ 'link' => get_page_url('politica-de-privacidad') ])</div>
                            </label>
                        </li>
                    </div>
                    <button type="submit" form="FormEnviarMensaje" class="sumbit anim">@Lang('Send')</button>
                </div>
            </div>
        </div>

        </form>

    </div>
</section>
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.fancybox.js') }}"></script>
<script src="{{ asset($_front.'js/easyResponsiveTabs.js') }}"></script>
<script src="{{ asset($_front.'js/pages/fitxa.js') }}"></script>

@if( $gmaps )
    @php($coordenades = explode(',', $gmaps->coordenades))

    @if (count($coordenades) > 1)
        @php($lat = $coordenades[0])
        @php($lng = $coordenades[1])

        <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ !empty($_config['google_maps']['value']) ? $_config['google_maps']['value'] : '' }}&callback=initMap"></script>
        <script>
        function initMap() {
            var myLatlng = new google.maps.LatLng({{ $lat }},{{ $lng }});
            var mapOptions = {
                zoom: 15,
                center: myLatlng,
                scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
                disableDoubleClickZoom: true,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    mapTypeIds: ['roadmap', 'terrain']
                }
            };
            map = new google.maps.Map(document.getElementById("gmaps"), mapOptions);

            marker = new google.maps.Marker({
                position: myLatlng,
                map: map
            });
            marker.setMap(map);
        }
        </script>
	@endif
@endif

<!-- ENVIAR PREGUNTA -->
<script>
$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});
$('#FormEnviarMensaje').on('submit', function(e) {
    var form_ele = $(this).addClass('loading');
    form_ele.addClass('loading');
    var form = $(this).serialize();
    $.ajax({
        url: ajaxURL,
        method: "POST",
        data: {
            action: "enviar_mensaje",
            method: "no_json",
            parameters: form
        },
        success: function(data) {
            form_ele.removeClass('loading');
            //form_ele.find('button').hide();
            //form_ele.find('textarea').hide();
            form_ele.trigger('reset');
            form_ele.find('.message-sent').show();
            toastr.success(form_ele.find('.message-sent').html());
            form_ele.find('input[type="submit"]').slideUp('fast');
        },
        error: function(data) {
            console.log(data);
        }
    });
    e.preventDefault();
    return false;
});
</script>
<!-- end ENVIAR PREGUNTA -->

<!-- DESCRIPTION VER MÁS -->
<script>
$(document).ready(function(){
    var elements = $('.description .descripcion-vermas').find('div, p');

    $('#flexslider2').flexslider({
        animation:"slide",
        slideshowSpeed:10000,
        animationDuration: 5000,
        easing: "swing",
        directionNav: true,
        controlNav: false,
        pausePlay: false,
        autoPlay:true,
    });
    
    elements.each(function(i, el) {
        $(el).replaceWith(function() {
            return $("<p />").append($(this).contents());
        });
    });

    var desvm_total_h = $('.descripcion-vermas').prop('scrollHeight');
    var desvm_h = $('.descripcion-vermas').height();
    if ( desvm_total_h > desvm_h ) {
        $('.description .btn-org.more').show();
        $('.description .btn-org.more').css('display', 'block');
    }
});
$(document).on('click', '.description .btn-org.more', function(){
    $('.descripcion-vermas').css('max-height', '2000px');
    $(this).hide();
});
</script>
<!-- end DESCRIPTION VER MÁS -->

@endsection