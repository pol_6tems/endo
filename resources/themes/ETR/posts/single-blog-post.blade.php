@extends('Front::layouts.base')

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style1.css') }}" />
@endsection

@section('styles')
    <link href="{{ asset($_front.'css/jquery.fancybox.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('contingut')
@php
    $categories = get_posts('categoria-post');
    $facilitador = $item->get_field('facilitador');
    $comunidad_url = \App\Post::get_url_by_post_name('blog-cultura-retiros');
    $publishDate = $item->get_field('fecha-publicacion');

    if ($publishDate) {
        $publishDate = \Carbon\Carbon::createFromFormat('d/m/Y', $publishDate);
    } else {
        $publishDate = $item->created_at;
    }
@endphp

@includeIf('Front::partials.header')
<div class="sharethis-inline-share-buttons" style="display:none;"></div>
<section class="retiros-sec la-experienca" style="margin-top: 50px; float: left; padding: 0;">
    <div class="row1">
        <div class="breadcrumb">
            <ul>
                <li><a href="{{ route('index') }}">@Lang('Home')</a></li>
                <li><a href="{{ $comunidad_url }}">@Lang('Cultura del Retiro')</a></li>
                <li>{{ $trans->title }}</li>
            </ul>
        </div>
    </div>
</section>

<div class="clear"></div>

<section class="retiros-sec la-experienca">
    <div class="row1">
        <div class="experienca-lft">
            <h1>{{ $trans->title }} <span> @if($item->author) por {{ $item->author->name }} @endif &nbsp;{{ $publishDate->format('d-m-Y') }}</span></h1>
            <h3>{{ $item->get_field('subtitulo') }}</h3>
            <h4>{{ $item->get_field('introduccion') }}</h4>

            
            <div class="description">
                @if ( !empty($item->media()) )
                <a href="{{ $item->media()->get_thumbnail_url() }}" rel="group" class="fancybox">
                    <img class="featured-image" src="{{ $item->media()->get_thumbnail_url("medium") }}" alt="{{ $item->media()->alt }}">
                </a>
                @endif
                {!! strip_tags(remove_style_tags($trans->description), "<span><div><p><a><strong><table><tr><h1><h2><h3><h4><h5><h6><td><tbody><ul><ol><li><b><br>") !!}
            </div>

            <div class="parecido">
                <div class="par-tit">
                    <h2>@Lang('¿Te ha parecido interesante?')</h2>
                    <div class="share-list">
                        <ul>
                            <li>
                                <!-- VALORACIONES -->
                                @php ( do_action('show_ratings', $item->id) )
                            </li>
                            <li><a href="javascript:void(0);" class="share-btn"><img src="{{ asset($_front.'images/icons/share.svg') }}"></a></li>
                        </ul>
                    </div>
                </div>

                <!-- FACILITADOR -->
                @if ( !empty($facilitador) )
                    @php($name = explode(" ", $facilitador->title)[0])
                    <div class="par-pad">
                        <ul class="facil-lst retiro">
                            <li>
                                <div class="ret-tit-lft">
                                    <div class="lft-img">
                                        @if ( !empty($facilitador->media()) )
                                            <a href="{{ $facilitador->get_url() }}">
                                                <img src="{{ $facilitador->media()->get_thumbnail_url('medium') }}" alt="{{ $facilitador->translate(true)->title }}">
                                            </a>
                                        @endif
                                    </div>
                                    <div class="cont-div">
                                        <a href="{{ $facilitador->get_url() }}">
                                            <h3>
                                                <span>{{ $name }}</span>
                                            </h3>
                                        </a>
                                        <!--<ul class="tags">
                                            <li><a href="javascript:void(0);">Yoga</a></li>
                                            <li><a href="javascript:void(0);">Meditacion</a></li>
                                            <li><a href="javascript:void(0);">Reiki</a></li>
                                            <li><a href="javascript:void(0);">Surf-yoga</a></li>
                                        </ul>-->

                                    </div>
                                    <!--<p class="crist-para">Cristina es una maestra inspiradora, y combina el Hatha yoga clasico adaptado para todos los niveles con yoga Vinyasa para hacer que... <a href="javascript:void(0);">Leer mas</a></p>-->
                                </div>
                            </li>
                        </ul>
                    </div>
                @endif
                <!-- end FACILITADOR -->

            </div>
        </div>

        <!-- CATEGORÍAS -->
        <div class="experienca-rht mob-rht">
            <div class="cat-btn">
                <a href="javascript:void(0);">@lang('Categorías') <span></span></a>
                <div class="cat-list-dp">
                    <ul>
                        @foreach($categories as $categoria)
                            @php($t_categoria = $categoria->getTranslation( \App::getLocale() ))
                            <li>
                                <a href="{{ !empty($comunidad_url) ? $comunidad_url . '?filter=' . $t_categoria->post_name : 'javascript:void(0);' }}">
                                    {{ $t_categoria->title }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!-- end CATEGORÍAS -->

    </div>       
</section>
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/jquery.fancybox.js') }}"></script>
<script type="text/javascript" src="{{ asset($_front.'js/jquery.mb.slider.js') }}"></script>
<script src="https://platform-api.sharethis.com/js/sharethis.js#property=5cd2de61d279570012cf0c1b&product=inline-share-buttons"></script>

<script type="text/javascript">   
    $("#ex0 .mb_slider").mbSlider({
        formatValue: function(val){
            return val;
        }
    });

    $(document).ready(function(){
        $('meta[property="og:image"]').attr("content", "{{ $item->media()->get_thumbnail_url("medium") }}");

        $('.fancybox').fancybox();
        var elements = $('.description').find('div, p');
        
        $('.description').find('div, p, span, img').each(function() {
            $(this).removeAttr("style");
        });
    
        $('.description p span').each(function() {
            $(this).replaceWith(function() {
                return $(this).contents();
            });
        });


        elements.each(function() {
            $(this).replaceWith(function() {
                return $("<p />").append($(this).contents());
            });
        });

        $('.cat-btn a').click(function(){
            $('.cat-btn').toggleClass('dp-open')
        });

        $('.cat-btn a').click(function(){
            $('.cat-list-dp').slideToggle('slow');
        });

        $('.share-btn').click(function(){
            $('.st-btn[data-network="sharethis"]').click();
        });
    });
</script>
@endsection