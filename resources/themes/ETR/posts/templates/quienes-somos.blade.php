@extends('Front::layouts.base')

@section('title')
    {{ strip_tags($item->title) }}
@endsection

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
<style>
/* Header */
/*.bann-cont { top: 20%; }
.bann-cont h1 { font-size: 65px; line-height: 70px; margin-bottom: 30px; }
.bann-cont h2, .bann-cont h2 span { font-size: 38px; }
.bann-cont p { font-size: 18px; line-height: 25px; font-weight: lighter; }*/
/* end Header */
/* General */
.abt-cont-in {
    width: 82%;
    padding-right: 0;
    display: block;
    margin: 0 auto;
    float: unset;
}
/* end General */
/* Equipo */
.valors {
    float: left;
    width: 100%;
    padding: 60px 0px 75px 0px;
}
.gris { background: #f5f5f5; }

.banner-home .banner {
    height: 520px;
    overflow: hidden;
    position: relative;
}

.bann-cont {
    top: 30%;
}

.bann-cont p {

}

.banner-home .banner img { width: 100%; }

.valors h1 {font-family: 'Barlow';font-weight: 800;font-size: 35px;line-height: 35px;color: #373737;letter-spacing: 0.5px;text-align: center;}
.valors .row1>ul { display: flex; flex-flow: wrap; margin: 0 auto; }
.valors .row1>ul li { width: 100%; display: flex; flex-flow: wrap; justify-content: center; background: #f5f5f5; }
.valors ul li .text-content { display: flex; flex-flow: wrap; justify-content: unset; align-items: center; flex-direction: column; padding: 30px 85px; }
.valors ul li .image, .valors ul li .text-content { width: 50%; }
.valors ul li .text-content span { width: 100%; font-size: 30px; font-weight: 600; color: #f9af02; padding-bottom: 30px; }
.valors ul li .text-content h3 { font-size: 26px; font-weight: normal; width: 100%; line-height: 40px; font-style: italic; }
.valors ul li .image { width: 30%; }
.valors ul li .image.img-mob {display: none}
.valors ul li .text-content { width: 55%; }
.valors ul li .image img { width: 100%; display: block; }
/* end Equipo */
</style>
@endsection

@section('contingut')
    @includeIf('Front::partials.header')
    <section class="banner-home banner-home-img resp-banner promociona">
        @if ( !empty($item->get_field('imagen-header')) )
            <div class="banner" style="background-image: url('{{ $item->get_field('imagen-header')->get_thumbnail_url() }}'); background-position: center; background-size: cover; background-repeat: no-repeat;">
        @else
            <div class="banner">
        @endif
            <div class="bann-cont">
                <div class="row1">
                    <h2>{!! $trans->title !!}</h2>
                    <h1>{!! $item->get_field('titulo-header') !!}</h1>
                    <p>{!! $item->get_field('descripcion-header') !!}</p>
                </div>
            </div>
        </div>
    </section>

    <section class="abt-cont">
        <div class="row1">
            <div class="abt-cont-in">
                <h2>{!! $item->get_field('titulo-general') !!}</h2>
                <p>{!! $item->get_field('descripcion-general') !!}</p>
            </div>
        </div>
    </section>

    <!-- Equipo -->
    <section class="valors gris">
        <div class="row1">
            
            @if( $item->get_field('equipo') )
            <ul>
                @foreach($item->get_field('equipo') as $key => $element)
                <li>
                    @if ($key % 2 == 0)
                        <div class="text-content">
                            <span>{!! $element['titulo']['value'] !!}</span>
                            <h3>{!! $element['descripcion']['value'] !!}</h3>
                        </div>
                        <div class="image">
                            <img src="{{ $element['imagen']['value']->get_thumbnail_url() }}" />
                        </div>
                    @else
                        <div class="image img-dsk">
                            <img src="{{ $element['imagen']['value']->get_thumbnail_url() }}" />
                        </div>
                        <div class="text-content">
                            <span>{!! $element['titulo']['value'] !!}</span>
                            <h3>{!! $element['descripcion']['value'] !!}</h3>
                        </div>
                        <div class="image img-mob">
                            <img src="{{ $element['imagen']['value']->get_thumbnail_url() }}" />
                        </div>
                    @endif
                </li>
                @endforeach
            </ul>
            @endif
        </div>
    </section>
    <!-- end Equipo -->

    <section class="nustra-blog pad-top">
        <div class="row1">
            <h2>{{ $item->get_field('conocenos-titulo') }}</h2>
            <div class="nustra-list">
                @if( $item->get_field('conocenos') )
                <ul>
                    @foreach($item->get_field('conocenos') as $bloque)
                    <li>
                        <div class="blog-img">
                            @if ( !empty($bloque['imagen']['value']) )
                                <img src="{{ $bloque['imagen']['value']->get_thumbnail_url() }}" alt="">
                            @endif
                        </div>

                        <div class="blog-cont">
                            <h2>{{ $bloque['subtitulo']['value'] }}</h2>
                            <h3 class="mob-para">{{ $bloque['titulo']['value'] }}</h3>
                            <p>{{ $bloque['descripcion']['value'] }} </p>
                            <a href="{{ $bloque['pagina']['value'] }}" class="vue-btn">@Lang('VER')</a>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('scripts1')
<script type="text/javascript" src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset($_front.'js/pages/nuestro-proyecto.js') }}"></script>
@endsection