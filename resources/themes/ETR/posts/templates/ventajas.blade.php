@extends('Front::layouts.base')

@section('contingut')
@includeIf('Front::partials.header')

<section class="banner-home banner-home-img resp-banner big-banner">
    <div class="banner">
        @if ( $trans->get_thumbnail_url() )
            <img src="{{ $trans->get_thumbnail_url() }}" alt="">
        @endif
    
        <div class="bann-cont">
            <div class="row1">
                <h2>{!! $item->get_field('subtitulo-header') !!}</h2>
                <h1>{!! $item->get_field('titulo-header') !!}</h1>
                <p>{!! $item->get_field('descripcion-header') !!}</p>
            </div>
        </div>
    </div>
</section>

<section class="passos ventajas">
    <div class="row1">
        <ul>
            @foreach($item->get_field('iconos') as $key => $icono)
            <li data-number="{{ ($key + 1) }}">
                <div>
                    @if ($icono['icono']['value'])
                        <img src="{{ $icono['icono']['value']->get_thumbnail_url() }}" />
                    @endif
                </div>
                <h2>{!! $icono['titol']['value'] !!}</h2>
            </li>
            @endforeach
        </ul>
    </div>
</section>

<section class="blog-list" style="padding: 0;">
    <div class="row1">
        <h2>{{ $item->get_field('bloque-1-titulo') }}</h2>
        <div class="contingut">
            {!! $item->get_field('bloque-1-contingut') !!}
        </div>
    </div>
</section>

<section class="funciona ventajas">
    <div class="row1">
        <h2>{!! $item->get_field('bloque-2-titulo') !!}</h2>
        <div class="funciona-l">
            <p>{!! $item->get_field('bloque-2-contingut') !!}</p>
        </div>
        <div class="funciona-r">
            <ul>
                @foreach($item->get_field('bloque-2-items') as $lista)
                    <li>{{ $lista['item']['value'] }}</li>
                @endforeach
            </ul>
        </div>       
    </div>
</section>

<section class="aun-car testmnl promo-testimonials">
    <div class="row1">
        <div class="owl-carousel promociona" id="testmnl-car">
            @foreach($item->get_field('bloque-3-recomanacions') as $recomendacion)
            <div class="item">
                <div class="obr-pad">
                    <a href="javascript:void(0);">
                        @if ($recomendacion['imagen']['value'])
                            <div class="obr-img" style="background-image: url('{{ $recomendacion['imagen']['value']->get_thumbnail_url() }}')"></div>
                        @endif
                    </a>
                    <div class="obr-cnt">
                        <div>
                            <h3>{!! $recomendacion['titulo']['value'] !!}</h3>
                            <span>{!! $recomendacion['autor']['value'] !!}</span>
                            <p>{!! $recomendacion['localizacion']['value'] !!}</p>
                            <a href="#register" class="fancybox1 show todo-btn1">@Lang('QUIERO REGISTRARME')</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<section class="queremos">
    <div class="row1">
        <h2>{!! $item->get_field('bloquen-4-titulo') !!}</h2>
        @if( $item->get_field('bloque-4-items') )
        <ul>
            @foreach($item->get_field('bloque-4-items') as $bloque)
            <li>
                <div class="que-img">
                    @if ($bloque['item-imagen']['value'])
                        <img src="{{ $bloque['item-imagen']['value']->get_thumbnail_url() }}">
                    @endif
                </div>
                <div class="que-txt">
                    <h2>{{ $bloque['item-titulo']['value'] }}</h2>
                    <p>{{ $bloque['item-descripcion']['value'] }}</p>
                    <!--<a href="javascript:void(0);">@Lang('Mes informació')</a>-->
                </div>          
            </li>
            @endforeach
        </ul>
        @endif
    </div>
</section>

@includeIf('Front::partials.banner-inferior')
@endsection

@section('scripts1')
<script type="text/javascript" src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script>
$(document).ready(function() {
    var owl = $('#testmnl-car');
    owl.owlCarousel({
        goToFirstSpeed :1000,
        loop:true,
        items:1,
        margin:1,
        autoplay:true,
        autoplayTimeout:5500,
        autoplayHoverPause:true,		
        nav : true,
        dots : false,
        responsive: {
            0:{ items:1, margin:0,},
            480:{ items:1},
            640:{ items:1},
            768:{ items:1},
            1024:{ items:1}
        },
    });
});
</script>
@endsection
<script>
function fadeNumbers() {
    $('.passos li').css('opacity', '0');
    var count = 1;
    var id = setInterval(frame, 1500);
    function frame() {
        if (count == 6) {
            clearInterval(id);
        } else {
            $('li[data-number="' + count + '"').css('opacity', '1');
            count++;
        }
    }
}
document.addEventListener("DOMContentLoaded", function(event) {
    fadeNumbers();
});
</script>