@extends('Front::layouts.base')

@section('title')
    {{ strip_tags($item->title) }}
@endsection

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
<style>
/* Header */
/*.bann-cont { top: 20%; }
.bann-cont h1 { font-size: 65px; line-height: 70px; margin-bottom: 50px; }
.bann-cont h2, .bann-cont h2 span { font-size: 38px; }
.bann-cont p { font-size: 22px; line-height: 27px; font-weight: lighter; }*/
/* end Header */
/* General */
.abt-cont { padding-bottom: 100px; }
.abt-cont-in { float: unset; width: 50%; margin: 0 auto; padding: 0; }
.abt-cont h1 { text-align: center; font-size: 36px; font-weight: bold; }
.abt-cont p { color: #000; }
/* end General */
/* Bloques */
.valors {
    float: left;
    width: 100%;
    padding: 0;
}
.gris { background: #f5f5f5; }
.valors h1 {
    font-family: 'Barlow';
    font-weight: 800;
    font-size: 35px;
    line-height: 35px;
    margin-bottom: 60px;
    color: #373737;
    letter-spacing: 0.5px;
    text-align: center;
}
.valors .row1>ul { display: flex; flex-flow: wrap; margin: 0 auto; }
.valors .row1>ul li { width: 100%; display: flex; flex-flow: wrap; justify-content: center; background: #fff; }

.valors ul li .text-content { display: flex; flex-flow: wrap; justify-content: center; align-items: center; flex-direction: column; padding: 40px 85px; }
.valors ul li .image, .valors ul li .text-content { width: 50%; }

.valors ul li .text-content span { width: 100%; font-size: 24px; font-weight: 600; color: #f9af02; padding-bottom: 20px; }
.valors ul li .text-content h3 {
    font-size: 24px;
    font-weight: lighter;
    width: 100%;
    line-height: 50px;
    text-align: center;
}
.valors ul li .text-content { width: 42%; padding: 40px 50px; }

.valors ul li .image { width: 58%; position: relative; }
.valors ul li .image img {
    width: 100%;
    display: block;
    max-height: 300px;
    object-fit: cover;
    object-position: center;
}
.valors ul li .image span {
    width: 100%;
    font-size: 30px;
    font-weight: 600;
    color: #fff;
    position: absolute;
    top: 45%;
    text-align: center;
    vertical-align: middle;
    display: inline-block;
    text-shadow: 2px 1px 5px #000;
    line-height: normal;
}
.valors ul li .image span i {
    font-size: 24px;
    width: 100%;
    display: inline-block;
    font-weight: lighter;
}
/* end Bloques */
</style>
@endsection

@section('contingut')
    @includeIf('Front::partials.header')
    <section class="banner-home banner-home-img resp-banner">
        <div class="home-video">
            <div class="bann-img">
                @if ( !empty($item->get_field('imagen-header')) )
                <img src="{{ $item->get_field('imagen-header')->get_thumbnail_url() }}" alt="">
                @endif
                <div class="bann-cont">
                    <div class="row1">
                        <h2>{!! $trans->title !!}</h2>
                        <h1>{!! $item->get_field('titulo-header') !!}</h1>
                        <p>{!! $item->get_field('descripcion-header') !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner ends -->
    <section class="abt-cont">
        <div class="row1">
            <div class="abt-cont-in">
                <h1>{!! $item->get_field('titulo-general') !!}</h1>
                <p>{!! $item->get_field('descripcion-general') !!}</p>
            </div>
        </div>
    </section>

    <!-- Bloques -->
    <section class="valors gris">
        <div class="row1">
            
            @if( $item->get_field('bloques') )
            <ul>
                @foreach($item->get_field('bloques') as $key => $element)
                <li>
                    @if ($key % 2 == 0)
                        <div class="image">
                            <img src="{{ $element['imagen']['value']->get_thumbnail_url() }}" />
                            <span>{!! $element['titulo']['value'] !!}</span>
                        </div>
                        <div class="text-content">
                            <h3>{!! $element['subtitulo']['value'] !!}</h3>
                        </div>
                    @else
                        <div class="text-content">
                            <h3>{!! $element['subtitulo']['value'] !!}</h3>
                        </div>
                        <div class="image">
                            <img src="{{ $element['imagen']['value']->get_thumbnail_url() }}" />
                            <span>{!! $element['titulo']['value'] !!}</span>
                        </div>
                    @endif
                </li>
                @endforeach
            </ul>
            @endif
        </div>
    </section>
    <!-- end Bloques -->
@endsection

@section('scripts1')
<script type="text/javascript" src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset($_front.'js/pages/nuestro-proyecto.js') }}"></script>
@endsection