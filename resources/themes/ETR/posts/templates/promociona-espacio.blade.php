@extends('Front::layouts.base')

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection


@section('title')
    {{ strip_tags($item->title) }}
@endsection

@section('styles')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
@endsection

@section('contingut')
@includeIf('Front::partials.header')

<section class="banner-home banner-home-img resp-banner promociona">
    <div class="banner">
        @if ($trans->get_thumbnail_url())
            <img src="{{ $trans->get_thumbnail_url() }}" alt="">
        @endif
        <div class="bann-cont">
            <div class="row1">
                <h2>{!! $item->title !!}</h2>
                <h1>{!! $item->get_field('titol-slider') !!}</h1>
                {!! $item->description !!}
            </div>
        </div>
    </div>
</section>

@if ($item->get_field('titulo-bloque-izquierda') != '' || $item->get_field('titulo-bloque-derecha') != '')
<section class="set-cont">
    <div class="row1">
        <ul>
            <li>
                <h1>{{ $item->get_field('titulo-bloque-izquierda') }}</h1>
                <p>{!! $item->get_field('bloque-izquierda') !!} <a class="mob-link" href="javascript:void(0);">Leer mas</a> </p>
            </li>
            <li>
                <h1>{{ $item->get_field('titulo-bloque-derecha') }}</h1>
                <p>{!! $item->get_field('bloque-derecha') !!}</p>
            </li>
        </ul>
    </div>
</section>
@endif


<section class="passos">
    <div class="row1">
        @if ($item->get_field('numeros'))
        <ul>
            @foreach($item->get_field('numeros') as $key => $numero)
            <li class="number" data-number="{{ ($key + 1) }}">
                <div class="numero">
                    <span>{{ $numero['numero-contingut']['value'] }}</span>
                </div>
                <h2>{!! $numero['numero-titol']['value'] !!}</h2>
            </li>
            @endforeach
        </ul>
        @endif
    </div>
</section>

<section class="funciona">
    <div class="row1">
        <h2>{!! $item->get_field('titulo-bloque-funciona') !!}</h2>
        <div class="funciona-l">
            <p>{!! $item->get_field('descripcion-bloque-funciona') !!}</p>
        </div>
        <div class="funciona-r">
            @if ($item->get_field('lista-bloque-funciona'))
            <ul>
                @foreach($item->get_field('lista-bloque-funciona') as $lista)
                    <li>{{ $lista['elemento-lista']['value'] }}</li>
                @endforeach
            </ul>
            @endif
        </div>       
    </div>
</section>


<section class="funciona nuestra">
    <div class="row1">
        <h2>{{ $item->get_field('titulo-ventajas') }}</h2>
        <ul>
            @foreach($item->get_field('ventajas') as $ventaja)
            <li style="background: url('{{ ($ventaja['ventaja-icono']['value']) ? $ventaja['ventaja-icono']['value']->get_thumbnail_url() : '' }}') no-repeat left 4px">
                <h3>{!! $ventaja['ventaja-titulo']['value'] !!}</h3>
                <p>{!! $ventaja['ventaja-descripcion']['value'] !!}</p>
            </li>
            @endforeach
        </ul>
    </div>
</section>

@if ($item->get_field('recomendaciones') && count($item->get_field('recomendaciones')) > 1)
<section class="aun-car testmnl promo-testimonials">
    <div class="row1">
        <div class="owl-carousel promociona" id="testmnl-car">
            @foreach($item->get_field('recomendaciones') as $recomendacion)
            <div class="item">
                <div class="obr-pad">
                    @if ($recomendacion['recomendacion-imagen']['value'])
                        <div class="obr-img" style="background-image: url('{{ $recomendacion['recomendacion-imagen']['value']->get_thumbnail_url("medium") }}')">
                            {{-- <img src="{{ $recomendacion['recomendacion-imagen']['value']->get_thumbnail_url("medium") }}" alt=""> --}}
                        </div>
                    @endif
                    <div class="obr-cnt">
                        <div>
                            <h3>{!! $recomendacion['recomendacion-titulo']['value'] !!}</h3>
                            <p>{!! $recomendacion['recomendacion-autor']['value'] !!}</p>
                            <a href="#register" class="fancybox1 show todo-btn1">@Lang('QUIERO EMPEZAR A PROMOCIONARME')</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif

<section class="queremos">
    <div class="row1">
        <h2>{!! $item->get_field('titulo-bloque-inferior') !!}</h2>
        @if( $item->get_field('bloques-inferior') )
        <ul>
            @foreach($item->get_field('bloques-inferior') as $bloque)
            <li>
                <div class="que-img">
                    @if ($bloque['imagen-bloque']['value'])
                        <img src="{{ $bloque['imagen-bloque']['value']->get_thumbnail_url() }}">
                    @endif
                </div>
                <div class="que-txt">
                    <h2>{{ $bloque['titulo-bloque']['value'] }}</h2>
                    <p>{{ $bloque['descripcion-bloque']['value'] }}</p>
                    <!--<a href="javascript:void(0);">Mes informació</a>-->
                </div>          
            </li>
            @endforeach
        </ul>
        @endif
    </div>
</section>

<section class="aun-car responde">
    <div class="row1">
        <h2>@lang('Respondemos a tus preguntas')</h2>
        @if ($item->get_field('preguntas') && $item->get_field('preguntas')->count())
            <ul>
                @foreach($item->get_field('preguntas') as $pregunta)
                    <li><a href="{{ $pregunta->get_url() }}">{{ $pregunta->title }}</a></li>
                @endforeach
            </ul>
        @endif
        <a href="{{ get_page_url('soporte') }}" class="todo-btn1">@lang('IR AL CENTRO DE AYUDA')</a>
    </div>
</section>

@if (isset($item) && $item && $item->get_field('banner-imagen'))
<section class="funciona necesit">
    <div class="row1">
        <div class="necesit-img" style="height: auto;">
            <img src="{{ $item->get_field('banner-imagen')->get_thumbnail_url() }}">
            <div class="caption-imgs">
                <h2>{!! $item->get_field('banner-titulo') !!}</h2>
                @if (Auth::check())
                    <a href="{{ $item->get_field('banner-link') }}">{{ $item->get_field('texto-boton') }}</a>
                @else
                    <a class="fancybox1 show" href="#register">{{ $item->get_field('texto-boton') }}</a>
                @endif
            </div>
        </div>
    </div>
</section>
@endif

@endsection

@section('scripts1')
<script type="text/javascript" src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script>
$(document).ready(function() {
    var owl = $('#testmnl-car');
    owl.owlCarousel({
        goToFirstSpeed :1000,
        loop:true,
        items:1,
        margin:1,
        autoHeight:true,
        autoplay:true,
        autoplayTimeout:5500,
        autoplayHoverPause:true,		
        nav : true,
        dots : false,
        responsive: {
            0:{ items:1, margin:0,},
            480:{ items:1},
            640:{ items:1},
            768:{ items:1},
            1024:{ items:1}
        },
    });
});
</script>
@endsection

<script>
document.addEventListener("DOMContentLoaded", function(event) {
    fadeNumbers();
});
</script>