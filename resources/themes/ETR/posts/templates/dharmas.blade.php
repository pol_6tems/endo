@extends('Front::layouts.base')

@section('contingut')
@includeIf('Front::partials.header')

<section class="banner-home banner-home-img resp-banner dharmas-header big-banner">
    <div class="banner">
        @if ( $trans->get_thumbnail_url() )
            <img src="{{ $trans->get_thumbnail_url() }}" alt="">
        @endif
        <div class="bann-cont">
            <div class="row1">
                <div class="caption">
                    <h2>{{ $trans->title }}</h2>
                    <span>{{ $item->get_field('subtitulo') }}</span>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog-list queretiro-bloc dharmas-content">
    <div class="row1">
        <h1>{{ $item->get_field('titulo-contenido') }}</h1>
        <div class="contingut">
            {!! $item->get_field('contenido') !!}
            <div class="list">
                @if ($item->get_field('imatge-lateral')->get_thumbnail_url())
                    <img src="{{ $item->get_field('imatge-lateral')->get_thumbnail_url() }}" />
                @endif
                <ul>
                    @foreach($item->get_field('lista-puntos') as $punto)
                        <li>{!! $punto['titulo']['value'] !!}</li>
                    @endforeach
                </ul>
            </div>
            {!! str_ireplace('1€', toUserCurrency('EUR', 1),$item->get_field('contenido-pie')) !!}
        </div>
    </div>
</section>

@includeIf('Front::partials.banner-inferior')
@endsection

@section('scripts1')
@endsection

