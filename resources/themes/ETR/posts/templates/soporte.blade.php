@extends('Front::layouts.base')

@section('contingut')
@php
    $aux = \App\Post::whereTranslation('post_name', $item->post_name)->first();

    $categories = get_posts('categoria-soporte');
    $subcategories = get_posts('subcategoria-soporte');

    $catActual = $categories->first();
    $subcatActual = $subcategories->first();
    
    switch($aux->type) {
        case 'categoria-soporte':
            $catActual = get_posts([
                'post_type' => 'categoria-soporte',
                'where' => [
                    ['post_name', '=', $item->post_name]
                ]
            ]);

            if ($catActual->isEmpty()) $catActual = $categories->first();
            else $catActual = $catActual->first();

            $subcatActual = get_posts([
                'post_type' => 'subcategoria-soporte',
                'metas' => [
                    ['categoria', $catActual->id]
                ],
            ]);
            
            $subcatActual = $subcatActual->first();
        break;
        case 'subcategoria-soporte':
            $subcatActual = \App\Post::where('type', 'subcategoria-soporte')->whereTranslation('post_name', $item->post_name)->get();

            if ($subcatActual->isEmpty()) $subcatActual = $subcategories->last();
            else $subcatActual = $subcatActual->last();

            if ( !($catActual = $subcatActual->get_field('categoria')) ) {
                $catActual = $categories->first();
            }
        break;
    }

    $subCats = collect();
    foreach($subcategories as $subcat) {
        if ($subcat->parent_id == $catActual->id) {
            $subCats->push($subcat);
        }
    }

    $subCats = $subCats->sortBy('title');
@endphp
<!-- Header Corresponent -->
@includeIf('Front::partials.header')
    <!-- centro-yellow starts -->
    <section class="centro-yellow">
        <div class="row1">
            <h2>@Lang('Centro de ayuda')</h2>
            {{--<div class="search">
                <a href="javascript:void(0);" class="srch-btn search-txt"></a>
                <input type="text" placeholder="¿Qué estás buscando?" class="search-txt">
            </div>--}}
            <h3><a href="tel:+34972664640">@Lang('+34 972 66 46 40')</a></h3>
        </div>
    </section>
    <!-- centro-yellow ends -->
    <!-- ayuda-sec starts -->
    <section class="ayuda-sec">
        <div class="row1">
            <div class="ayuda-top">
                <ul>
                    @foreach($categories as $categoria)
                        <li><a class="{{ ($catActual->id == $categoria->id) ? 'active' : '' }}" href="{{ $categoria->get_url() }}">{{ $categoria->title }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="dudas-lft">
                <ul>
                    @foreach($subCats as $subcategoria)
                        @php($t_categoria = ($subcategoria->get_parent()) ? $subcategoria->get_parent()->translate() : null)
                        @php($t_subcategoria = $subcategoria->translate())

                        <li class="{{ ($subcatActual && $subcatActual->id == $subcategoria->id) ? 'active' : '' }}"><a href="{{ $subcategoria->get_url() }}">{{ $t_subcategoria->title }}</a></li>
                    @endforeach
                </ul>
            </div>
            @if($subcatActual)
                @php($preguntes = get_posts([
                    'post_type' => 'grup-preguntes',
                    'metas' => [
                        ['subcategoria', $subcatActual->id]
                    ],
                ]))

                <div class="dudas-rgt">
                    {{--<h2>{{ $subcatActual->translate()->title }}</h2>--}}
                    <ul class="dudas-hab">
                        @foreach($preguntes as $pregunta)
                            <li><a href="{{ $pregunta->get_url() }}"><img src="">{{ $pregunta->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </section>
@endsection