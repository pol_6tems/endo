@extends('Front::layouts.base')

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style1.css') }}" />
    <link rel="stylesheet" href="{{ asset($_front.'css/style2.css') }}" />
@endsection

@section('meta_robots', 'noindex, follow')

@section('title')
    {{ $item->title }}
@endsection

@section('contingut')
    @includeIf('Front::partials.header')

    @if ($media = $item->media())
        <section class="banner-inner legal"> <img src="{{ $media->get_thumbnail_url() }}" alt="">
            <div class="ban-search">
                <div class="row1">
                    <h1>{{ $item->title }}</h1>
                </div>
            </div>
        </section>
    @endif

    <section class="blog-list queretiro-bloc legal">
        <div class="row1">
            {{-- <h1>{{ $item->title }}</h1> --}}

            <div class="contingut">
                {!! strip_tags(remove_style_tags($trans->description), "<span><div><p><a><strong><b><table><tr><td><tbody><ul><ol><li>") !!}
            </div>
        </div>
    </section>


@endsection