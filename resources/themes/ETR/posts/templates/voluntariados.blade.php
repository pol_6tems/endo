@extends('Front::layouts.base')

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('contingut')
@includeIf('Front::partials.header')
<section class="banner-home banner-home-img resp-banner">
    <div class="home-video se-vh">
        <div class="bann-img">
            @if ( !empty($trans->get_thumbnail_url()) )
            <img src="{{ $trans->get_thumbnail_url() }}" alt="">
            @endif
        </div>
    </div>
</section>
<!-- banner ends -->
<section class="home-title">
    <div class="row1">
        <h1>{!! $item->get_field('titulo') !!}</h1>
    </div>
</section>
<section class="search-sec voluntar">
    <div class="row1">
        <form id="form_retiro" action="{{ route('buscador') }}" method="POST">
            @csrf
            <input type="hidden" id="localidad" name="localization[localidad]" value="">
            <input type="hidden" id="provincia" name="localization[provincia]" value="">
            <input type="hidden" id="region" name="localization[region]" value="">
            <input type="hidden" id="pais" name="localization[pais]" value="">
            <input type="hidden" id="coordenadas" name="localization[coordenadas]" value="">
            <input type="hidden" name="lista" value="voluntariados">
            <input type="hidden" name="post_type" value="voluntariado">
            <ul>
                <li class="loca">
                    <input id="search" type="text" placeholder="Localización">
                </li>
                {{-- <li><input id="submit" type="submit" value="Buscar" disabled></li> --}}
            </ul>        
        </form>
    </div>
</section>
<section class="aun-car categ-sec voluntar">
    <div class="row1">
        <h2>{!! $item->get_field('voluntariados-destacados') !!}</h2>
        <p>{!! $item->get_field('voluntariados-descripcion') !!}</p>

        <div class="obres retrios-lis voluntariados" id="aun-car1">
            <ul class="list">
            @php($voluntariados = get_posts([
                'post_type' => 'voluntariado',
                'numberposts' => 6,
            ]))
            @foreach($voluntariados as $voluntariado)
                @if ($translated = $voluntariado->translate())
                    @php($gmaps = $voluntariado->get_field('ubicacion-y-localizacion'))
                    <li>
                        <div class="obr-pad">
                            <a href="{{ $voluntariado->get_url() }}">
                                <div class="obr-img item">
                                    @php($galeria = $voluntariado->get_field('galeria'))
                                    @if ($galeria && count($galeria) > 0)

                                        <img src="{{ reset($galeria)->get_thumbnail_url('medium') }}" alt="">
                                    @endif
                                    <span class="pin"  data-post-id="{{ $voluntariado->id }}" data-post-type="{{ $voluntariado->type }}">
                                        <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                                        <div class="tooltip"><span class="tooltiptext">@Lang('ANADIR A LA LISTA')</span></div>
                                    </span>
                                </div>
                                <div class="obr-cnt">
                                    <h3>{{ $translated->title }}</h3>
                                    <ul class="date-icon">
                                        <li class="loc">{{ print_location($gmaps, ['administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'country']) }}</li>
                                    </ul>
                                    <div class="mob-retiros-lst">
                                        <ul class="lst-icon">
                                            @if ($trabajos = $voluntariado->get_field('trabajos-a-realizar'))
                                                <li class="noc"><i class="icon trabajo"></i>{{ separatedList($trabajos, 'title', ', ', 1, '(y más)') }}</li>
                                            @endif
                                            @if ($duracion = $voluntariado->get_field('duracion-minima'))
                                                <li class="idiom"><i class="icon duracion-min"></i>{{ separatedList($duracion, 'title', ', ', 1, '(y más)') }}</li>
                                            @endif
                                        </ul>
                                    </div>
                                    {{-- <p>{{ get_excerpt(strip_tags($translated->description), 40) }}</p> --}}
                                </div>
                                    <div class="price">
                                        <div class="p-lft">
                                            <!-- VALORACIONES -->
                                            @php ( do_action('show_ratings_avg', $voluntariado->id) )
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <a href="{{ \App\Post::get_archive_link('voluntariado-retiro') }}" class="todo-btn">ver todos</a>
    </div>
</section>
@includeIf('Front::partials.blog')
@endsection

@section('scripts1')
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key={{ isset($_config['google_maps']) ? $_config['google_maps']['value'] : '' }}&libraries=places"></script>
<script>
    var map, autocomplete, marker;
    function autocomplete() {
        var input = document.getElementById("search");
        var options = {
            types: ['geocode', 'establishment']
        };
        autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            
            document.getElementById('coordenadas').value = place.geometry.location.lat() + "," +  place.geometry.location.lng();
        
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            document.getElementById('localidad').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'locality'; })[0]);
            document.getElementById('provincia').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'administrative_area_level_2'; })[0]);
            document.getElementById('region').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'administrative_area_level_1'; })[0]);
            document.getElementById('pais').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'country'; })[0]);
            
            $("#form_retiro").submit();
        });
    }

    autocomplete();

</script>
@endsection