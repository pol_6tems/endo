@extends('Front::layouts.base')

@section('title')
    {{ $item->title }}
@endsection

@section('contingut')

    @php
        $posts = get_posts(['post_type' => 'equipo']);
    @endphp

    @includeIf('Front::partials.header')

    @if ($media = $item->media())
        <section class="banner-inner"> <img src="{{ $media->get_thumbnail_url() }}" alt="">
            <div class="ban-search">
                <div class="row1">
                    <h1>@lang('Contactar con nosotros')</h1>
                </div>
            </div>
        </section>
    @endif

    <form id="contact_form" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="email_id" value="10">
        <input type="hidden" name="to" value="info@encuentraturetiro.com">

        <section class="retiros-sec detalles-retiro contact-page">
            <div class="row">
                <div class="retro-lft">
                    <h3>@lang('¿En qué estás interesad@?')</h3>

                    <div class="frm-ryt">
                        <ul>
                            <li class="w100">
                                <div class="frm-grp">
                                    <select class="select_box" name="interest">
                                        @foreach($item->get_field('intereses') as $interest)
                                            <option>{{ $interest['interes']['value'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </li>
                        </ul>

                        <ul>
                            <li>
                                <div class="frm-grp">
                                    <label>@lang('Nombre')</label>
                                    <input type="text" name="name" class="frm-ctrl" @if(auth()->user())value="{{ auth()->user()->name }}"@endif required>
                                </div>
                            </li>
                            <li>
                                <div class="frm-grp">
                                    <label>@lang('Apellidos')</label>
                                    <input type="text" name="lastname" class="frm-ctrl" @if(auth()->user())value="{{ auth()->user()->lastname }}"@endif required>
                                </div>
                            </li>
                            <li>
                                <div class="frm-grp">
                                    <label>@lang('Teléfono')</label>
                                    <input type="text" name="phone_number" class="frm-ctrl" required>
                                </div>
                            </li>
                            <li>
                                <div class="frm-grp">
                                    <label>@lang('Email')</label>
                                    <input type="email" name="email" class="frm-ctrl" @if(auth()->user())value="{{ auth()->user()->email }}"@endif required required>
                                </div>
                            </li>
                            <li>
                                <div class="frm-grp">
                                    <label>@lang('Ciudad')</label>
                                    <input type="text" name="city" class="frm-ctrl" required>
                                </div>
                            </li>
                            <li>
                                <div class="frm-grp">
                                    <label>@lang('País')</label>
                                    <input type="text" name="country" class="frm-ctrl" required>
                                </div>
                            </li>
                            <li>
                                <div class="frm-grp comanda-mensaje">
                                    <label>@lang('Mensaje')</label>
                                    <textarea class="frm-ctrl text-area-small" name="message" placeholder="@lang('Escribe un mensaje')..." required></textarea>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="desimiler login-form">
                        <li class="check full-wid">
                            <input id="policy" type="checkbox" name="policy" value="options" required>
                            <label for="policy">
                                <span><span></span></span>
                                @Lang('He leído y acepto la <a href=":link" target="_blank">Política de Privacidad</a>', [ 'link' => get_page_url('politica-de-privacidad') ])
                            </label>
                        </li>

                        <div class="message-sent" style="padding: 10px;margin: 10px;display:none;">
                            {{ __(':name sent successfully', ['name' => __('Message')]) }}
                        </div>

                        <button type="submit" class="sub-btn">@lang('Enviar')</button>
                    </div>
                </div>

                <div class="retro-rgt no-tpad">
                    <div class="white-box-rgt">
                        <div class="white-box-cnt">
                            @if ($emails = $item->get_field('emails'))
                                <div class="dat-select">
                                    <h2>@lang('Email')</h2>

                                    {!! nl2br($emails) !!}
                                </div>
                            @endif
                            @if ($telefonos = $item->get_field('telefonos'))
                                <div class="dat-select">
                                    <h2>@lang('Teléfono')</h2>

                                    {!! nl2br($telefonos) !!}
                                </div>
                            @endif
                            @if ($oficinas = $item->get_field('oficinas'))
                                <div class="dat-select">
                                    <h2>@lang('Oficina')</h2>

                                    {!! nl2br($oficinas) !!}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix-block"></div>

            <div class="contact-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2965.962440348841!2d2.809972015840709!3d41.979616179214325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12bae723f7975dc9%3A0xaef4a568de4a56cc!2sCarrer+de+Santa+Eug%C3%A8nia%2C+92%2C+17005+Girona!5e0!3m2!1ses!2ses!4v1564659772297!5m2!1ses!2ses" frameborder="0" style="border:0"></iframe>
            </div>
        </section>
    </form>
@endsection

@section('scripts1')
    <script>
        $('#contact_form').on('submit', function (e) {
            e.preventDefault();

            var form_ele = $(this);
            var form = $(this).serialize();
            $.ajax({
                url: ajaxURL,
                method: "POST",
                data: {
                    action: "enviar_email",
                    method: "no_json",
                    parameters: form
                },
                success: function(data) {
                    form_ele.trigger('reset');
                    form_ele.find('.message-sent').slideDown('fast');
                },
                error: function(data) {
                    console.log(data);
                }
            });

            return false;
        });
    </script>
@endsection