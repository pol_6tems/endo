@extends('Front::layouts.base')

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset($_front.'css/datepicker.css') }}" />
<link rel="stylesheet" href="{{ asset($_front.'css/smk-buscador.css') }}" />
@endsection

@section('contingut')
@php
    $retiroIds = collect(json_decode(request()->cookie('visited_retiros', json_encode([]))));

    $visitedRetiros = collect();

    if ( $retiroIds->count() ) {
        $visitedRetiros = \App\Post::withTranslation()->with(['metas.customField', 'author.metas.customField', 'customPostTranslations.customPost.translations'])->whereIn('id', $retiroIds)->where('status', 'publish')->get();

        $visitedRetiros = filterRetirosByDates($visitedRetiros);
    }

    $categorias_de_retiro = $item->get_field('categorias-de-retiro');

    $retirosArchiveUrl = \App\Post::get_archive_link('retiros');
    $testimonio_page = \App\Post::ofType("page")->ofName("testimonios")->first();
@endphp

@includeIf('Front::partials.header')

@includeIf('Front::partials.header-video')

<!-- banner ends -->
<section class="home-title">
    <div class="row1">
        {{-- <h1>@lang('Vive una experiencia totalmente&nbsp;<span>transformadora</span>')</h1> --}}
        @if ( $title = $item->get_field('titulo') )
            <h1>{!! $title !!}</h1>
        @endif
    </div>
</section>
<section class="search-sec search-hom">
    <div class="row1">
        @includeIf('Front::partials.buscador', ['llista' => 'retiros', 'isHome' => true])
    </div>
</section>

<section class="aun-car testimnoios">
    <div class="row1">
        <h2>@lang('Historias personales detrás de nuestros Retiros')</h2>
        @php($testimonios = $testimonio_page->get_field('testimonios'))
        <div class="mob-hidden">
            <ul>

            @foreach(array_slice($testimonios, 0, 3) as $testimoni)
                <li>
                    @if ($avatar = $testimoni['avatar']['value'])
                        <div class="avatar">
                            <img src="{{ $avatar->get_thumbnail_url() }}" alt="">
                        </div>
                    @endif
                    <div class="content">
                        <h3>{{ $testimoni['nombre']['value'] }}</h3>
                        <div class="ratings">
                            @for ($i = 1; $i <= 5; $i++)
                                @if ($i <= $testimoni['estrellas']['value'])
                                    <img src="{{ asset($_front."images/heart-full.svg") }}" alt="">
                                @else
                                    <img src="{{ asset($_front."images/heart-empty.svg") }}" alt="">
                                @endif
                            @endfor
                            <span>{{ $testimoni['estrellas']['value'] }} / 5</span>
                        </div>
                        <h6>{{ $testimoni['subtitulo']['value'] }}</h6>
                        <p>{{ $testimoni['descripcion']['value'] }}</p>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="mob-show" {{--style="padding: 0 24px"--}}>
            <div class="owl-carousel obres" id="testimonials-mob">
                @foreach(array_slice($testimonios, 0, 3) as $testimoni)
                    <div class="testimonial-container">
                        @if ($avatar = $testimoni['avatar']['value'])
                            <div class="avatar">
                                <img src="{{ $avatar->get_thumbnail_url() }}" alt="">
                            </div>
                        @endif
                        <div class="content">
                            <h3>{{ $testimoni['nombre']['value'] }}</h3>
                            <div class="ratings">
                                @for ($i = 1; $i <= 5; $i++)
                                    @if ($i <= $testimoni['estrellas']['value'])
                                        <img src="{{ asset($_front."images/heart-full.svg") }}" alt="">
                                    @else
                                        <img src="{{ asset($_front."images/heart-empty.svg") }}" alt="">
                                    @endif
                                @endfor
                                <span>{{ $testimoni['estrellas']['value'] }} / 5</span>
                            </div>
                            <h6>{{ $testimoni['subtitulo']['value'] }}</h6>
                            <p>{{ $testimoni['descripcion']['value'] }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <a href="{{ get_page_url("testimonios") }}" class="todo-btn">@lang('DESCUBRE MÁS TESTIMONIOS')</a>
    </div>
</section>

@if ($visitedRetiros->count())
    <section class="aun-car categ-sec">
        <div class="row1">
            <h2>@lang('Retiros de tu interés')</h2>
            <p>@lang('Descubre retiros, vacaciones y cursos de yoga organizados en todo el mundo.')</p>
            @if ($visitedRetiros && count($visitedRetiros))
                <div class="owl-carousel obres" id="aun-car1">
                    @foreach($visitedRetiros as $retiro)
                    <div class="item">
                        {!! renderRetiroHome($retiro, false, true) !!}
                    </div>
                    @endforeach
                </div>
                <div class="aun-car-mbl interes">
                    <ul>
                        @foreach($visitedRetiros->take(2) as $retiro)
                            <li>
                                {!! renderRetiroHome($retiro, true, true) !!}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <a href="{{ $retirosArchiveUrl }}" class="todo-btn">@lang('DESCUBRE MÁS RETIROS')</a>
        </div>
    </section>
@endif

<!-- CATEGORIAS DE RETIRO -->
@if ( !empty($categorias_de_retiro) )
<section class="aun-car categ-video">
    <div class="row1">
        <h2>@lang('Categorías de retiro')</h2>
        <p>@lang('Elige el tipo de retiro que te apetece en este momento')</p>
        <div class="mosaic-section">
            
            @foreach ( $categorias_de_retiro as $key => $cat_retiro )
                @php ( $cat_retiro_has_media = !empty($cat_retiro['imagen']['value']) ? $cat_retiro['imagen']['value']->has_thumbnail() : false )
                @php ( $cat_titulo = !empty($cat_retiro['titulo']['value']) ? $cat_retiro['titulo']['value'] : '' )
                @php ( $cat_categoria = !empty($cat_retiro['categoria']['value']) ? $cat_retiro['categoria']['value'] : '' )
                @php ( $cat_retiro_has_icon = !empty($cat_retiro['icono']['value']) ? $cat_retiro['icono']['value']->has_thumbnail() : false )

                @if ( $key == 0 )
                    <div class="mosaic-top"><ul>
                @elseif ( $key % 3 == 0 )
                    </ul></div> <!-- end mosaic-top -->
                    <div class="mosaic-top"><ul>
                @endif

                <li class="video-hover">
                    <a href="{{ route('retiros.landing', ['filter' => $cat_categoria]) }}" class="">
                        
                        @if ( $cat_retiro_has_media )
                            <img class="custom-lazy-img" data-src="{{ $cat_retiro['imagen']['value']->get_thumbnail_url('medium') }}" alt="">
                        @endif

                        <div class="list-cont">
                            <div style="width: 100%;display: flex;flex-wrap: wrap;">
                                @if ( $cat_retiro_has_icon )
                                    <img class="icon" src="{{ $cat_retiro['icono']['value']->get_thumbnail_url('thumbnail') }}" alt="">
                                @endif
                                
                                <h3 style="width: 100%; text-align: center;">
                                    {{ $cat_titulo }}
                                </h3>
                            </div>                            
                        </div>
                    </a>
                </li>

            @endforeach
                
            </ul></div> <!-- end mosaic-top -->

        </div>

        <div class="home-crem-mbl categ-mbl">
            <ul>
                @foreach ( $categorias_de_retiro as $key => $cat_retiro )
                    @php ( $cat_retiro_has_media = !empty($cat_retiro['imagen']['value']) ? $cat_retiro['imagen']['value']->has_thumbnail() : false )
                    @php ( $cat_titulo = !empty($cat_retiro['titulo']['value']) ? $cat_retiro['titulo']['value'] : '' )
                    @php ( $cat_categoria = !empty($cat_retiro['categoria']['value']) ? $cat_retiro['categoria']['value'] : '' )

                    <li>
                        <a href="{{ route('retiros.landing', ['filter' => $cat_categoria]) }}" class="">
                            
                            @if ( $cat_retiro_has_media )
                                <img class="custom-lazy-img" data-src="{{ $cat_retiro['imagen']['value']->get_thumbnail_url('medium') }}" alt="">
                            @endif

                            <div class="list-cont">
                                <h3>{{ $cat_titulo }}</h3>
                            </div>
                        </a>
                    </li>

                @endforeach
            </ul>
        </div>
        
        <a href="{{ $retirosArchiveUrl }}" class="todo-btn">@lang('DESCUBRE MÁS RETIROS')</a>

    </div>
</section>
@endif
<!-- end CATEGORIAS DE RETIRO -->

<section class="aun-car">
    <div class="row1">
        <h2>@lang('Retiros Destacados')</h2>
        <div class="owl-carousel obres alcance home-featured" id="aun-car">
            @php($retiros = \App\Post::withTranslation()->with(['metas.customField', 'author.metas.customField', 'customPostTranslations.customPost.translations'])->where(['type' => 'retiro', 'status' => 'publish'])->get())
            @php($retiros = $retiros->filter(function($value, $key) {
                return ($value->get_field('destacado') == 1);
            }))
            @php($retiros = filterRetirosByDates($retiros)->shuffle())
            @foreach($retiros as $retiro)
                <div class="item">
                    {!! renderRetiroHome($retiro, true, true) !!}
                </div>
            @endforeach
        </div>

        {{--<div class="aun-car-mbl destacados">
            <ul>
                @foreach($retiros as $retiro)
                    <li>
                        {!! renderRetiroHome($retiro, true, true) !!}
                    </li>
                @endforeach
            </ul>
        </div>--}}
        <a href="{{ $retirosArchiveUrl }}" class="todo-btn">@lang('DESCUBRE MÁS RETIROS')</a>
    </div>
</section>

<section class="aun-car testimonios-icons">
    <div class="row1">
        <h2>@lang('Nuestros usuarios confían en Encuentra tu Retiro')</h2>
        <div class="mob-hidden">
            <ul class="iconos">
                @if($iconos = $testimonio_page->get_field('iconos'))
                    @foreach(array_slice($iconos, 0, 3) as $icon)
                        <li>
                            @if ($img = $icon['icono']['value'])
                                <img src="{{ $img->get_thumbnail_url() }}" alt="">
                            @endif
                            <span>{{ $icon['descripcion']['value'] }}</span>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
        @if ($iconos)
            <div class="mob-show">
                <div class="owl-carousel obres" id="icons-mob">
                    @foreach(array_slice($iconos, 0, 3) as $icon)
                        <div class="icons-container">
                            @if ($img = $icon['icono']['value'])
                                <img src="{{ $img->get_thumbnail_url() }}" alt="">
                            @endif
                            <span>{{ $icon['descripcion']['value'] }}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        <a href="{{ get_page_url("ventajas") }}" class="todo-btn">@lang('DESCUBRE NUESTRAS VENTAJAS')</a>
    </div>
</section>

<section class="aun-list">
    <div class="row1">
        <h2>@lang('¿Qué temática de retiro te apetece?')</h2>
        <p>@lang('Puedes encontrar todos nuestros retiros clasificados por temática')</p>
        <div class="owl-carousel home-themes" id="aun-car-themes">
            @php($themes = $item->get_field('tematicas-de-retiro'))
            {{--@php($themes = false)--}}
            @if ($themes)
                @foreach($themes as $theme)
                    @php ( $themeMedia = !empty($theme['imagen']['value']) ? $theme['imagen']['value'] : false )
                    @php ( $themeTitle = !empty($theme['titulo']['value']) ? $theme['titulo']['value'] : '' )
                    @php ( $themeCategory = !empty($theme['categoria']['value']) ? $theme['categoria']['value'] : '' )
                    <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => $themeCategory]) }}">
                            <div class="img-div"> <img src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" @if ($themeMedia && $themeMedia->has_thumbnail())class="owl-lazy" data-src="{{ $themeMedia->get_thumbnail_url() }}"@endif style="min-height: 360px; min-width: 360px"  alt="{{ $themeTitle }}" >
                                <div class="list-cont">
                                    <h3>@lang($themeTitle)</h3>
                                </div>
                            </div>
                            <span class="exp-btn">@lang('EXPLORA')</span>
                        </a>
                    </div>
                @endforeach
            @else
                <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'silencio']) }}">
                        <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/06.jpg') }}" alt="">
                            <div class="list-cont">
                                <h3>@lang('Silencio')</h3>
                            </div>
                        </div>
                        <span class="exp-btn">@lang('EXPLORA')</span>
                    </a> </div>
                <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'mindfulness']) }}">
                        <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/02.jpg') }}" alt="">
                            <div class="list-cont">
                                <h3>Mindfulness</h3>
                            </div>
                        </div>
                        <span class="exp-btn">@lang('EXPLORA')</span>
                    </a> </div>
                <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'tantra']) }}">
                        <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/05.jpg') }}" alt="">
                            <div class="list-cont">
                                <h3>Tantra</h3>
                            </div>
                        </div>
                        <span class="exp-btn">@lang('EXPLORA')</span>
                    </a> </div>
                <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'ayuno']) }}">
                        <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/01.jpg') }}" alt="">
                            <div class="list-cont">
                                <h3>Ayuno</h3>
                            </div>
                        </div>
                        <span class="exp-btn">@lang('EXPLORA')</span>
                    </a> </div>
                <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'sport']) }}">
                        <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/04.jpg') }}" alt="">
                            <div class="list-cont">
                                <h3>Sport</h3>
                            </div>
                        </div>
                        <span class="exp-btn">@lang('EXPLORA')</span>
                    </a> </div>
                <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'antiestres']) }}">
                        <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/03.jpg') }}" alt="">
                            <div class="list-cont">
                                <h3>Antiestrés</h3>
                            </div>
                        </div>
                        <span class="exp-btn">@lang('EXPLORA')</span>
                    </a> </div>
            @endif
        </div>
        <div class="home-crem-mbl">
            <ul>
                @php( $tipus = get_posts(['post_type' => 'tipos-retiro', 'numberposts' => 4, 'with' => ['metas.customField']]) )
                @foreach($tipus as $t)
                    @php($t = \App\Post::find($t['id']))
                    @php($poster = $t->get_field('poster'))
                        @if ($t && $poster)
                            <li>
                                <a href="{{ $t->post_name }}"> <img src="{{ $poster->get_thumbnail_url() }}" alt="">
                                    <div class="list-cont">
                                        <div> <img class="custom-lazy-img" data-src="{{ $poster->get_thumbnail_url() }}" alt="">
                                            <h2>{{ $t->title }}</h2>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endif
                @endforeach
            </ul>
        </div>
        <a href="{{ $retirosArchiveUrl }}" class="todo-btn">@lang('DESCUBRE MÁS RETIROS')</a>
    </div>
</section>
<section class="home-blog">
    <div class="row1">
        <h2>@lang('Blog Cultura del Retiro')</h2>
        <div class="home-blog-lft">
            @php($blogPosts4 = get_posts(['post_type' => 'blog-post', 'with' => ['translations.media', 'author', 'customPostTranslations']]))
            @php($blogPosts3 = $blogPosts4->sortByDesc(function($element) {
                $publicacion = $element->get_field('fecha-publicacion');
                if ($publicacion && $publicacion != "") {
                    return Date::createFromFormat("d/m/Y", $publicacion)->timestamp;
                }
            }))
            <ul>
                @foreach($blogPosts3->slice(0, 3) as $post)
                @php($t_post = $post->translate())
                    <li>
                        <div class="blog-img">
                            @if ($t_post->media)
                            <a href="{{ $post->get_url() }}">
                                <img class="custom-lazy-img" data-src="{{ $t_post->media->get_thumbnail_url() }}" alt="{{ $t_post->media->alt }}">
                            </a>
                            @endif
                        </div>
                        <div class="blog-cont">
                            <h3>{{ $t_post->title }}</h3>
                            @php ($publishDate = $post->get_field('fecha-publicacion') ? Date::createFromFormat('d/m/Y', $post->get_field('fecha-publicacion')) : $post->created_at)
                            <h4>{{ ucfirst(Date::parse($publishDate)->format('l j \\d\\e F Y')) }} - @if ($post->author) @lang('por') {{ $post->author->name }} {{ $post->author->lastname }} @endif</h4>
                            <p>{{ get_excerpt($t_post->description, 15) }} <a href="{{ $post->get_url() }}">@lang('Leer más')</a></p>
                        </div>
                    </li>
                @endforeach
            </ul>
            <div class="button">
                <a href="{{ get_page_url('blog-cultura-retiros') }}" class="todo-btn">@lang('VER TODOS LOS ARTÍCULOS')</a>
            </div>
        </div>
        <div class="home-blog-mbl">
            <ul>
                @foreach($blogPosts3->slice(0, 4) as $post)
                    @php($t_post = $post->translate())
                    <li> <a href="{{ $post->get_url() }}"> @if ($t_post->media)<img src="{{ $t_post->media->get_thumbnail_url() }}" alt="{{ $t_post->media->alt }}">@endif
                        <div class="list-cont">
                            <div>
                                <h3>{{ $post->title }}</h3>
                                <h5><img class="custom-lazy-img" data-src="{{ asset($_front.'images/heart-white.png') }}" alt=""><span>1</span> {{ $post->author->name }} {{ $post->author->lastname }}</h5>
                            </div>
                        </div>
                    </a> </li>
                @endforeach
            </ul>

            <a href="{{ get_page_url('blog-cultura-retiros') }}" class="todo-btn">@lang('VER TODOS LOS ARTÍCULOS')</a>
        </div>
        <div class="home-blog-rgt">
            <ul>
                <li>
                    <a href="{{ get_page_url('plantamos-un-arbol') }}"> <img src="{{ asset($_front.'images/00-Plantarunarbol.jpg') }}" alt="">
                        <div class="list-cont">
                            <div class="shadow"></div>
                            <h3>@lang('We plant a tree')</h3>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="{{ get_page_url('regalo-retiro') }}"><img src="{{ asset($_front.'images/00-Regaloretiro.jpg') }}" alt="">
                        <div class="list-cont">
                            <div class="shadow"></div>
                            <h3>@lang('Gift Retreat')</h3>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="{{ get_page_url('que-es-un-retiro') }}"><img src="{{ asset($_front.'images/00-Retiro.jpg') }}" alt="">
                        <div class="list-cont">
                            <div class="shadow"></div>
                            <h3>@lang('What is a spiritual retreat?')</h3>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>
@php($facilitadores = \App\Post::with(['metas.customField', 'translations.media', 'customPostTranslations.customPost.translations'])->where('type', 'facilitador')->get())
@php($facilitadores = $facilitadores->filter(function($value, $key) {
    return intval($value->get_field('destacado')) == 1;
}))

@if ( $facilitadores && $facilitadores->count() )
{{-- Agafem els 20 primers --}}
@php( $facilitadores = $facilitadores->slice(0, 21))
@php( $facilitadores = $facilitadores->shuffle())
<section class="nues-sec">
    <div class="row1">
        <h3>@lang('Nuestros facilitadores')</h3>
        <p>@lang('Conoce mejor a las personas que hacen posible los retiros')</p>
        <div class="nues-spiking">
            <div class="bubbels-container firstTime">
                @php($i = 0)
                @foreach($facilitadores as $facilitador)
                    @php($name = explode(" ", $facilitador->title)[0])
                    @if ($i == 0)
                        <div class="bubble-center">
                            @if ($facilitador->media())
                                <img class="custom-lazy-img" data-src="{{ $facilitador->media()->get_thumbnail_url("medium") }}" alt="{{ $facilitador->media()->get_thumbnail_url("medium") }}"  class="" data-name="{{ $facilitador->title }}" data-facilitador="{{ $facilitador->id }}-{{ $i }}"/>
                            @endif
                        </div>
                    @else
                        <div class="wid-79 innerdive" id="bubble{{ ($i + 1) }}">
                            <a href="javascript:void(0);">
                                @if ($facilitador->media())
                                    <img class="custom-lazy-img" data-src="{{ $facilitador->media()->get_thumbnail_url("medium") }}" class="smallpic" data-name="{{ $name }}" data-facilitador="{{ $facilitador->id }}-{{ $i }}">
                                @endif
                                <label></label>
                            </a>
                        </div>
                    @endif
                    @php($i++)
                @endforeach
            </div>
            <div class="filter-sec">
                @php($i = 0)
                @foreach($facilitadores as $facilitador)
                    @php($name = explode(" ", $facilitador->title)[0])
                    <div id="facilitador-{{ $facilitador->id }}-{{ $i }}" class="facilitador" style="{{ ($i > 0 ) ? 'display: none' : '' }}">
                        <span>{{ $name }}</span>
                        @php( $habilidades = $facilitador->get_field('habilidades') )
                        @if ($habilidades)
                            <ul>
                                @foreach($habilidades as $key => $habilidad)
                                    @if ($key < 5)
                                        <li><a href="javascript:void(0);">{{ $habilidad->title }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                        <a href="{{ $facilitador->get_url() }}" class="ver-btn">@lang('VER PERFIL')</a>
                    </div>
                    @php($i++)
                @endforeach
            </div>
        </div>

        {{-- Mobil --}}
        <div class="mbl-nues">
            <ul>
                @php($facilitadoresMbl = $facilitadores->slice(0, 4))
                @foreach($facilitadoresMbl as $key => $facilitador)
                    @php($name = explode(" ", $facilitador->title)[0])
                    <li>
                        <div class="img-div">
                            @if ($facilitador->media())
                            <a href="{{ $facilitador->get_url() }}">
                                <img class="custom-lazy-img" data-src="{{ $facilitador->media()->get_thumbnail_url("medium") }}">
                            </a>
                            @endif
                        </div>
                        <div class="img-cont">
                            <span>{{ $name }}</span>
                            <a href="{{ $facilitador->get_url() }}">@lang('VER PERFIL')</a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</section>
@endif
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/smk-accordion.js') }}"></script>
<script src="{{ asset($_front.'js/buscador.js') }}"></script>
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script src="{{ asset($_front.'js/datepicker.min.js') }}"></script>
<script src="{{ asset($_front.'js/datepicker.'. app()->getLocale().'.js') }}"></script>
<script>
/* search drop down script */
$(".search-sec.search-hom ul li.ser .text").focus(function(){   
   $(this).next().addClass('hide');
   $('.search-sec.search-hom ul li.date').find(".search-modal").removeClass('hide');
});

$(".search-sec.search-hom ul li.date .text1").focus(function() {
   $(this).next().addClass('hide');
   $('.search-sec.search-hom ul li.ser').find(".search-modal").removeClass('hide');
});

/* search drop down script */

$(document).click(function(event) {
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if (!$(event.target).closest(".mbl-menu").length) {
        $("body").find(".lang-div").removeClass('show');
    }

    if (!$(event.target).closest("li.date").length && !isDatePickerElement(event.target.className)) {
        $('.search-sec.search-hom ul li.date').find(".search-modal").removeClass('hide');
    }
});

var $ = jQuery.noConflict();
// $j is now an alias to the jQuery function; creating the new alias is optional.
$(document).ready(function() {
    $('.fancybox, .fancybox1').fancybox();
    getLable();
    //shuffle bubbles Suppoeting
    $('.bubbels-container div').click(function() {
    //$('.bubbels-container div').removeClass('animation');
        if($(this).attr('id') != undefined) {
            $('.bubbels-container').removeClass('firstTime');
            $('.bubbels-container div').attr("style","");
            var getId = $(this).attr('id');
            var centerImg = $('.bubble-center img');

            var getMainImage = centerImg.attr('src');
            var getMainData = centerImg.attr('data-name');
            var getFacilitador = centerImg.attr('data-facilitador');
            var setIamge =$(this).find('img').attr('src');
            var dataName =$(this).find('img').attr('data-name');
            var facilitador = $(this).find('img').attr('data-facilitador');
            $('.facilitador').hide();

            var facilitador = $(this).find('img').attr('data-facilitador');
            $('#facilitador-'+ facilitador).fadeIn();

            $(this).find('img').attr('src', getMainImage);
            $(this).find('img').attr('data-name',getMainData);
            $(this).find('img').attr('data-facilitador',getFacilitador);

            centerImg.attr('src', setIamge);
            centerImg.attr('data-name', dataName);
            centerImg.attr('data-facilitador', facilitador);
            
            var Imgarray = [];
            $(".smallpic").each(function() {
                item={};
                item ["image_url"] = $(this).attr('src');
                item ["Name"] = $(this).attr('data-name');
                item ["facilitador"] = $(this).attr('data-facilitador');
                Imgarray.push(item);
            });

            shuffledarr=shuffle(Imgarray);
            
            var i=0;
            $(".smallpic").each(function() {
                $(this).attr("src",shuffledarr[i].image_url);
                $(this).attr("data-name",shuffledarr[i].Name);
                $(this).attr("data-facilitador", shuffledarr[i].facilitador);
                i++;
            });

            getLable();
        
            var d = $.Deferred().resolve();
            $(".bubbels-container .innerdive").get().forEach(function(div){
                d = d.then(function(){
                    return $(div).animate({opacity: 1 },{
                        step: function() {
                            $(this).css({'-webkit-transform':'scale(1)','-moz-transform':'scale(1)','transform':'scale(1)'}); 
                        },
                        duration:50
                    },'linear');
                });
            });
            // setTimeout(function() {$('.bubbels-container .bubble-center').removeClass('animation');},600);
        }
    });
    $('.bubbels-container .bubble-center').click(function(e){
        e.stopPropagation();
    })
});

//Get Lable Value
function getLable(){			
    $('.bubbels-container div label').each(function(){
        var getLableValue = $(this).siblings('img').attr('data-name');
        $(this).text(getLableValue);
    });
}

//shuffle jsons of bubbles
function shuffle(sourceArray) {
    for (var i = 0; i < sourceArray.length - 1; i++) {
        var j = i + Math.floor(Math.random() * (sourceArray.length - i));
        var temp = sourceArray[j];
        sourceArray[j] = sourceArray[i];
        sourceArray[i] = temp;
    }
    return sourceArray;
}

var owl = $('#aun-car1');
	owl.owlCarousel({
		goToFirstSpeed :1000,
		loop:true,
		items:3,
		margin:15,
        lazyLoad: true,
		autoplay:true,
		autoplayTimeout:5500,
		autoplayHoverPause:true,		
	    nav : true,
	    dots : false,
		responsive: {
			0:{ items:1, margin:0,},
			480:{ items:1},
			640:{ items:2},
			768:{ items:3},
			1024:{ items:3}
		},
    });

var owl = $('#aun-car');
	owl.owlCarousel({
		goToFirstSpeed :1000,
		loop:true,
		items:4,
		margin:15,
        lazyLoad: true,
		autoplay:true,
		autoplayTimeout:5500,
		autoplayHoverPause:true,		
	    nav : true,
	    dots : false,
		responsive: {
			0:{ items:1, margin:0,},
			480:{ items:1},
			640:{ items:2},
			768:{ items:3},
			1024:{ items:4}
		},
	});

var owlThemes = $('#aun-car-themes');
owlThemes.owlCarousel({
    goToFirstSpeed :1000,
    loop:true,
    items:3,
    margin:15,
    lazyLoad: true,
    autoplay:true,
    autoplayTimeout:5500,
    autoplayHoverPause:true,
    nav : true,
    dots : false,
    responsive: {
        0:{ items:1, margin:0,},
        480:{ items:1},
        640:{ items:2},
        768:{ items:3},
        1024:{ items:3}
    },
});

var owlTestimonials = $('#testimonials-mob');
owlTestimonials.owlCarousel({
    goToFirstSpeed :1000,
    loop:true,
    items:3,
    margin:15,
    lazyLoad: true,
    autoplay:true,
    autoplayTimeout:5500,
    autoplayHoverPause:true,
    nav : true,
    dots : false,
    responsive: {
        0:{ items:1, margin:0,},
        480:{ items:1},
        640:{ items:2},
        768:{ items:3},
        1024:{ items:3}
    },
});

var owlTestimonials = $('#icons-mob');
owlTestimonials.owlCarousel({
    goToFirstSpeed :1000,
    loop:true,
    items:3,
    margin:15,
    lazyLoad: true,
    autoplay:true,
    autoplayTimeout:5500,
    autoplayHoverPause:true,
    nav : false,
    dots : false,
    responsive: {
        0:{ items:1, margin:0,},
        480:{ items:1},
        640:{ items:2},
        768:{ items:3},
        1024:{ items:3}
    },
});
</script>
@endsection