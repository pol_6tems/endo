@extends('Front::layouts.base')

@section('title')
    {{ strip_tags($item->title) }}
@endsection

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
<style>
.bann-cont {top: 45%}
.valors {
    float: left;
    width: 100%;
    padding: 60px 0 75px 0;
}
.gris { background: #f5f5f5; }
.valors h1, .valors h2 {
    font-family: 'Barlow';
    font-weight: 800;
    font-size: 35px;
    line-height: 35px;
    margin-bottom: 60px;
    color: #373737;
    letter-spacing: 0.5px;
    text-align: center;
}
.valors .row1>ul { display: flex; flex-flow: wrap; margin: 0 auto; }
.valors .row1>ul li { width: 100%; display: flex; flex-flow: wrap; justify-content: center; background: #fff; }
.valors ul li .text-content { display: flex; flex-flow: wrap; justify-content: center; align-items: center; flex-direction: column; padding: 40px 85px; }
.valors ul li .image, .valors ul li .text-content { width: 50%; }
.valors ul li .text-content span { width: 100%; font-size: 24px; font-weight: 600; color: #f9af02; padding-bottom: 20px; }
.valors ul li .text-content h3 { font-size: 23px; font-weight: 600; width: 100%; }
.valors ul li .image.img-mob { display: none }
.valors ul li .image img { width: 100%; display: block; }
</style>
@endsection

@section('contingut')
    @includeIf('Front::partials.header')
    <section class="banner-home banner-home-img resp-banner">
        <div class="home-video">
            <div class="bann-img">
                @if ( !empty($item->get_field('imagen')) )
                <img src="{{ $item->get_field('imagen')->get_thumbnail_url() }}" alt="">
                @endif
                <div class="bann-cont">
                    <div class="row1">
                        <h2>{!! $trans->title !!}</h2>
                        <h1>{!! $item->get_field('titulo-slider') !!}</h1>
                        <p>{!! $item->get_field('descripcion') !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner ends -->
    <section class="abt-cont">
        <div class="row1">
            <div class="abt-cont-in">
                <h1>{!! $item->get_field('titulo-general') !!}</h1>
                <p>{!! $item->get_field('descripcion-bloque') !!}</p>
            </div>
        </div>
    </section>
    <section class="abt-car car-alg nuestro-proyecto">
        <div class="row1">
            <div class="abt-car-rgt">
                <div class="owl-carousel obres" id="abt-car">
                    @foreach($item->get_field('carousel') as $car)
                    <div class="item">
                        <div class="obr-pad">
                            <div class="obr-img">
                                @if ( !empty($car['imatge-carousel']['value']) )
                                    <img src="{{ $car['imatge-carousel']['value']->get_thumbnail_url() }}" alt=""> </div>
                                @endif
                            <div class="obr-cnt">
                                <h3>"{!! $car['text-carousel']['value'] !!}"</h3>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="abt-car-lft">
                <h3>{{ $item->get_field('texto-carousel') }}</h3>
            </div>
        </div>
    </section>

    <!-- Nosotros sumamos -->
    <section class="valors gris">
        <div class="row1">
            <h2>{!! $item->get_field('nosotros-sumamos-titulo') !!}</h2>
            @if( $item->get_field('nosotros-sumamos') )
            <ul>
                @foreach($item->get_field('nosotros-sumamos') as $key => $element)
                <li>
                    @if ($key % 2 == 0)
                        <div class="text-content">
                            <span>{{ $element['titulo']['value'] }}</span>
                            <h3>{{ $element['subtitulo']['value'] }}</h3>
                        </div>
                        <div class="image">
                            <img src="{{ $element['imagen']['value']->get_thumbnail_url() }}" />
                        </div>
                    @else
                        <div class="image img-dsk">
                            <img src="{{ $element['imagen']['value']->get_thumbnail_url() }}" />
                        </div>
                        <div class="text-content">
                            <span>{{ $element['titulo']['value'] }}</span>
                            <h3>{{ $element['subtitulo']['value'] }}</h3>
                        </div>
                        <div class="image img-mob">
                            <img src="{{ $element['imagen']['value']->get_thumbnail_url() }}" />
                        </div>
                    @endif
                </li>
                @endforeach
            </ul>
            @endif
        </div>
    </section>
    <!-- end Nosotros sumamos -->

    @if ($item->get_field('bloques-inferior'))
    <section class="nustra-blog pad-top">
        <div class="row1">
            <h2>{{ $item->get_field('titulo-bloque-inferior') }}</h2>
            <div class="nustra-list">
                <ul>
                    @foreach($item->get_field('bloques-inferior') as $bloque)
                    <li>
                        <div class="blog-img">
                            @if ( !empty($bloque['imagen-bloque']['value']) )
                                <img src="{{ $bloque['imagen-bloque']['value']->get_thumbnail_url() }}" alt="">
                            @endif
                        </div>

                        <div class="blog-cont">
                            <h2>{{ $bloque['subtitulo-bloque']['value'] }}</h2>
                            <h3 class="mob-para">{{ $bloque['titulo-bloque']['value'] }}</h3>
                            <p>{{ $bloque['descripcio-bloque']['value'] }} </p>
                            <a href="{{ $bloque['pagina']['value'] }}" class="vue-btn">@Lang('VER')</a>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
    @endif
@endsection

@section('scripts1')
<script type="text/javascript" src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset($_front.'js/pages/nuestro-proyecto.js') }}"></script>
@endsection