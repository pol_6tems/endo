@extends('Front::layouts.base')

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
<link href="{{ asset($_front.'css/select2.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('contingut')
@includeIf('Front::partials.header')
<section class="banner-home banner-home-img resp-banner">
    <div class="home-video se-vh">
        <div class="bann-img">
            @if ( !empty($trans->get_thumbnail_url()) )
                <img src="{{ $trans->get_thumbnail_url() }}" alt="">
            @endif
        </div>
    </div>
</section>
<!-- banner ends -->
<section class="home-title">
    <div class="row1">
        <h1>{!! $item->get_field('titulo2') !!}</h1>
    </div>
</section>
<section class="search-sec voluntar espais">
    <div class="row1">
        <div class="es-pad">
            <form id="form_retiro" action="{{ route('buscador') }}" method="POST">
                @csrf
                <input type="hidden" id="localidad" name="localization[localidad]" value="">
                <input type="hidden" id="provincia" name="localization[provincia]" value="">
                <input type="hidden" id="region" name="localization[region]" value="">
                <input type="hidden" id="pais" name="localization[pais]" value="">
                <input type="hidden" id="coordenadas" name="localization[coordenadas]" value="">
                <input type="hidden" name="lista" value="espacios">
                <input type="hidden" name="post_type" value="espacio">
                <ul>
                    <li class="loca"><input id="search" type="text" placeholder="@lang('Localización')"></li>
                    <li class="capa">
                        <select id="cercador" name="capacidad" placheolder="@lang('Capacidad')">
                            <option>@lang('Capacidad')</option>
                            @foreach(get_posts('espacios-capacidad') as $capacidad)
                                @php($t_capacidad = $capacidad->translate())
                                <option>{{ $t_capacidad->title }}</option>
                            @endforeach
                        </select>
                    </li>
                    {{-- <li><input id="submit" type="submit" value="Buscar" disabled></li> --}}
                </ul>        
            </form>
        </div>
    </div>
</section>
<section class="aun-car categ-sec voluntar">
    <div class="row1">
        <h2>{!! $item->get_field('espacios-destacados') !!}</h2>
        <p>{!! $item->get_field('destacados-descripcion') !!}</p>

        <div class="obres voluntariados" id="aun-car1">
            <ul>
            @foreach( get_posts(['post_type' => 'espacio', 'numberposts' => 6]) as $espacio )
                @php($translated = $espacio->translate())
                @php($selection = $espacio->get_field('selection'))
                @php($gmaps = $espacio->get_field('ubicacion'))
                <li class="vol-li">
                    <div class="espacio obr-pad">
                        <a href="{{ $espacio->get_url() }}">
                            <div class="obr-img item">
                                @php($galeria = $espacio->get_field('galeria'))
                                @if ($galeria && count($galeria) > 0)
                                    <img src="{{ reset($galeria)->get_thumbnail_url('medium') }}" alt="">
                                @endif
                                <span class="pin" data-post-id="{{ $espacio->id }}" data-post-type="{{ $espacio->type }}">
                                    <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                                    <div class="tooltip"><span class="tooltiptext">@lang('ANADIR A LA LISTA')</span></div>
                                </span>
                            </div>
                            <div class="obr-cnt">
                                <h3>{{ $translated->title }}</h3>
                                <ul class="loc-ul">
                                    <li class="loc">{{ print_location($gmaps, ['administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'country']) }}</li>
                                </ul>
                                <div class="mob-retiros-lst">
                                    <ul class="lst-icon">
                                        @if ($tipos = $espacio->get_field('tipos'))
                                            <li class="noc"><i class="icon tipo"></i>{{ separatedList($tipos, 'title', ', ', 1, '(y más)') }}</li>
                                        @endif
                                        @if ($plazas = $espacio->get_field('plazas'))
                                            <li class="idiom"><i class="icon publico"></i>{{ $plazas }} @lang('plazas')</li>
                                        @endif
                                        @if($regimen = $espacio->get_field('regimen'))
                                            <li class="menu"><i class="icon reg"></i>{{ separatedList($regimen, 'title', ', ', 1, '(y más)') }}</li>
                                        @endif
                                        @if ($alimentacion = $espacio->get_field('alimentacio'))
                                            <li class="tran"><i class="icon ali"></i>{{ separatedList($alimentacion, 'title', ', ', 1, '(y más)') }}</li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                                <div class="price">
                                    <div class="p-lft">
                                        <!-- VALORACIONES -->
                                        @php ( do_action('show_ratings_avg', $espacio->id) )
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <a href="{{ \App\Post::get_archive_link('espacios-casas-retiro') }}" class="todo-btn">@Lang('ver todos')</a>
    </div>
</section>

<!-- DESTINOS -->
@php( $espacio_cp = \App\Models\CustomPost::get_by_post_type('espacio') )
@php( $destinos_children = $item->get_field('destinos-destinos') )
@php( $destinos_archive_link = !empty($espacio_cp) ? \App\Post::get_archive_link($espacio_cp->post_type_plural) : '' )
<section class="aun-list eapais">
    <div class="row1">
        <h2>{{ $item->get_field('destinos-titulo') }}</h2>
        <p>{{ $item->get_field('destinos-subtitulo') }}</p>
        
        @if ( !empty($destinos_children) )
            <ul>
                @foreach ($destinos_children as $destinos_child)
                    @php ( $destinos_child_coords = !empty($destinos_child['destino-enlace']['value']) ? $destinos_child['destino-enlace']['value']->coordenades : '' )
                    @php ( $destinos_child_url = $destinos_archive_link . '?coords=' . $destinos_child_coords )
                    @php ( $destinos_child_media = !empty($destinos_child['destino-imagen']['value']) ? $destinos_child['destino-imagen']['value'] : '' )
                    @php ( $destinos_child_media_url = !empty($destinos_child_media) ? $destinos_child_media->get_thumbnail_url('medium') : '' )
                    <li>
                        <a href="{{ $destinos_child_url }}">
                            <div class="img-div">
                                <img src="{{ $destinos_child_media_url }}" alt="">
                                <div class="list-cont">
                                    <h3>
                                        {{ !empty($destinos_child['destino-titulo']['value']) ? $destinos_child['destino-titulo']['value'] : '' }}
                                    </h3>
                                </div>
                            </div>
                            <span class="exp-btn">
                                {{ !empty($destinos_child['destino-boton-texto']['value']) ? $destinos_child['destino-boton-texto']['value'] : '' }}
                            </span>
                        </a>
                    </li>
                @endforeach
                
            </ul>
        @endif

        <a href="{{ $destinos_archive_link }}" class="todo-btn">
            {{ $item->get_field('destinos-boton-texto') }}
        </a>

    </div>
</section>
<!-- end DESTINOS -->

@includeIf('Front::partials.banner-inferior')

@endsection


@section('scripts1')
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key={{ isset($_config['google_maps']) ? $_config['google_maps']['value'] : '' }}&libraries=places"></script>
<script>
    var map, autocomplete, marker;
    function autocomplete() {
        var input = document.getElementById("search");
        var options = {
            types: ['geocode', 'establishment']
        };
        autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            
            document.getElementById('coordenadas').value = place.geometry.location.lat() + "," +  place.geometry.location.lng();
        
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            
            document.getElementById('localidad').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'locality'; })[0]);
            document.getElementById('provincia').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'administrative_area_level_2'; })[0]);
            document.getElementById('region').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'administrative_area_level_1'; })[0]);
            document.getElementById('pais').value = JSON.stringify(place.address_components.filter(function(e) { return e.types[0] == 'country'; })[0]);
            
            $("#form_retiro").submit();

        });
    }

    autocomplete();

    $("#cercador").change(function() {
        $("#form_retiro").submit();
    });

    $(document).ready(function() {
        $('#cercador').select2({
            minimumResultsForSearch: Infinity // És per treure el input search box
        });
    });
</script>
@endsection