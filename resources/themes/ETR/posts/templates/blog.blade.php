@extends('Front::layouts.base')

@php
    $catActual = null;
    $categories = get_posts('categoria-post');
    $query = explode('&', request()->getQueryString());

    $filters = array_filter($query, function ($element) { return explode('=', $element)[0] == 'filter'; });
    if ( !empty($filters) ) {
        $filter = explode('=', $filters[0]);

        if ( !empty($filter) && $filter[0] == 'filter' ) {
            foreach( $categories as $categoria ) {
                if ( $categoria->translate()->post_name == $filter[1]) $catActual = $categoria;
            }
        }
    }

    $args = array(
        'post_type' => 'blog-post',
        'orderBy' => ['col' => 'created_at', 'dir' => 'desc']
    );

    if ($catActual) {
        $args['metas'] = [
            ['categoria', '=', $catActual->id]
        ];
    }

    $posts = get_posts($args);

    $posts = $posts->sortByDesc(function($element) {
        $publicacion = $element->get_field('fecha-publicacion');
        if ($publicacion && $publicacion != "") {
            return Date::createFromFormat("d/m/Y", $publicacion)->timestamp;
        }
    });
    $perPage = 12;
    $currentPage = Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage();

    $sliced = $posts->slice($perPage * ($currentPage - 1), $perPage);
    $posts = new Illuminate\Pagination\LengthAwarePaginator($sliced, $posts->count(), $perPage, $currentPage);
    $posts->withPath(get_page_url('blog-cultura-retiros'));

    if ( !empty($filter) && $filter[0] == 'filter' ) {
        foreach($posts as $k => $item) {
            $cat = $item->get_field('categoria');
            if ($cat) {
                if ($cat->translate()->post_name != $filter[1]) unset($posts[$k]);
            }
        }
    }
@endphp

@section('extra_metas')
    @php($canonical = get_page_url('blog-cultura-retiros'))
    @php($pageParam = request('page'))
    @if ($pageParam && $pageParam > 1)
        @php($canonical .= '?page=' . $pageParam)
    @endif

    <link rel="canonical" href="{{ $canonical }}"/>
@endsection

@section('contingut')

@includeIf('Front::partials.header')

<section class="retiros-sec la-experienca" style="margin-top: 50px; float: left; padding: 0;">
    <div class="row1">
        <div class="breadcrumb">
            <ul>
                <li><a href="{{ route('index') }}">@Lang('Home')</a></li>
                <li>@Lang('Cultura del Retiro')</li>
            </ul>
        </div>
    </div>
</section>

<section class="blog-list">
    <div class="row1">
        <h1>{{ $page->translate()->title }}</h1>
        <p>{{ $page->get_field('subtitulo') }}</p>
        <div class="experienca-ppad desk-exp">
            <div class="cat-btn">
                <!-- <a href="javascript:void(0);" class="todo-btn">@Lang('Categories')</a> -->
                <div class="cat-list-dp">
                    <ul>
                        <li><a href="{{ url()->current() }}">@Lang('Todos')</a></li>
                        @foreach($categories as $categoria)
                            <li><a class="{{ (isset($filter) && $filter[1] == $categoria->translate()->post_name) ? 'active' : '' }}" href="{{ url()->current() . '?filter=' . $categoria->translate()->post_name }}">{{ $categoria->translate()->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="blog-pp">
            <ul>
                @foreach($posts as $item)
                    @include('Front::partials.blog-item', ['item' => $item])
                @endforeach
            </ul>
        </div>
        <div class="experienca-ppad mob-exp">
            <div class="cat-btn">
                <a href="javascript:void(0);" class="todo-btn">@Lang('Categories')</a>
                <div class="cat-list-dp" style="display:none;">
                    <ul>
                        @foreach($categories as $categoria)
                            <li><a class="{{ (isset($filter) && $filter[1] == $categoria->translate()->post_name) ? 'active' : '' }}" href="{{ url()->current() . '?filter=' . $categoria->translate()->post_name }}">{{ $categoria->translate()->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!--<a href="javascript:void(0);" class="todo-btn">Categories +</a> -->
        {{ $posts->appends($_GET)->links('Front::pagination.bootstrap-4') }}
        <!-- Link -->
    </div>
</section>
@endsection

@section('scripts1')
<script type="text/javascript" src="{{ asset($_front.'js/smk-accordion.js') }}"></script>
<script type="text/javascript" src="{{ asset($_front.'js/jquery.mb.slider.js') }}"></script>
<script>
$(document).ready(function(){
    $('#slide, .filter-menu h1 a').click(function(){
    var hidden = $('.filter-menu');
    if (hidden.hasClass('visible')){
        hidden.animate({"left":"-1000px"}, "slow").removeClass('visible');
        $('body').removeClass('hide');
    } else {
        hidden.animate({"left":"0px"}, "slow").addClass('visible');
        $('body').addClass('hide');
    }
    });	

});
</script>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$(".accordion_example1, .accordion_example2").smk_Accordion();
	});
</script>
<script type="text/javascript">   
    $("#ex0 .mb_slider").mbSlider({
        formatValue: function(val){
            return val;
        }
    });

    $('.cat-btn a').click(function(){
        $('.cat-btn').toggleClass('dp-open')
    });

    $('.mob-exp .cat-btn a').click(function(){
        $('.cat-list-dp').slideToggle('slow');
    });
</script>
@endsection

