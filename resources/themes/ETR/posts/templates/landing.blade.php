@extends('Front::layouts.base')

@section('title')
    {{ strip_tags($item->title) }}
@endsection

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset($_front.'css/style2.css') }}" />
@endsection

@section('styles')
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
        .banner-home .banner {
            height: 520px;
            overflow: hidden;
            position: relative;
        }

        .bann-cont {
            top: 25%;
        }

        .bann-cont p {
            display: block !important;
            width: 80%;
        }
    </style>
@endsection

@section('contingut')
    @includeIf('Front::partials.header')

    <section class="banner-home banner-home-img resp-banner promociona">
        <div class="banner" @if ($trans->get_thumbnail_url())style="background-image: url('{{ $trans->get_thumbnail_url() }}'); background-position: center; background-size: cover; background-repeat: no-repeat;"@endif>
            <div class="bann-cont">
                <div class="row1">
                    <h1>{!! $item->title !!}</h1>

                    @if ($item->get_field('subtitulo-header'))
                        <p style="font-size: 24px;padding-top: 20px;">
                            {!! $item->get_field('subtitulo-header') !!}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix-block"></div>

    <div class="row1 landing-content">

        @if ($item->description)
            <section class="description">
                {!! str_replace('!important', '', $item->description) !!}
            </section>
        @endif

        @if ($item->get_field('link'))
            <section class="landing-button">
                <a href="{{ $item->get_field('link') }}">{{ $item->get_field('link-text') ?: __('Ver') . ' ' . $item->title }}</a>
            </section>
        @endif

        @php($posts = collect($item->get_field('posts'))->flatten(1)->map(function ($post) {
            if (array_key_exists('value', $post)) {
                return $post['value'];
            }
        }))

        @if ($posts->count())
            <section class="blog-list">
                <h2>@lang('Artículos') <span>{{ $item->title }}</span></h2>
                <div class="blog-pp">
                    <ul>
                        @foreach($posts as $post)
                            @include('Front::partials.blog-item', ['item' => $post])
                        @endforeach
                    </ul>
                </div>
            </section>
        @endif

        @php($retiros = collect($item->get_field('retiros'))->flatten(1)->map(function ($retiro) {
            if (array_key_exists('value', $retiro)) {
                return $retiro['value'];
            }
        }))

        @php($retiros = filterRetirosByDates($retiros))

        @if ($retiros->count())
            <div class="clearfix-block"></div>

            <section class="aun-car">
                <h2>@lang('Retiros Destacados')</h2>
                <div class="owl-carousel obres alcance" id="aun-car">
                    @foreach($retiros as $retiro)
                        <div class="item">
                            {!! renderRetiroHome($retiro, true) !!}
                        </div>
                    @endforeach
                </div>

                <div class="aun-car-mbl">
                    <ul>
                        @foreach($retiros as $retiro)
                            <li>
                                {!! renderRetiroHome($retiro, true) !!}
                            </li>
                        @endforeach
                    </ul>
                </div>

                <a href="{{  \App\Post::get_archive_link('retiros') }}" class="todo-btn">@lang('DESCUBRE MÁS RETIROS')</a>
            </section>
        @endif
    </div>

@endsection

@section('scripts1')
    <script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>

    <script>
        var owl = $('#aun-car');
        owl.owlCarousel({
            goToFirstSpeed :1000,
            loop:true,
            items:4,
            margin:15,
            autoplay:true,
            autoplayTimeout:5500,
            autoplayHoverPause:true,
            nav : true,
            dots : false,
            responsive: {
                0:{ items:1, margin:0,},
                480:{ items:1},
                640:{ items:2},
                768:{ items:3},
                1024:{ items:4}
            },
        });
    </script>
@endsection