@extends('Front::layouts.base')

@section('contingut')

@includeIf('Front::partials.header')
<section class="banner-home banner-home-img resp-banner que-es-retiro-header">
    @if ( $item->get_field('header-image') )
        <img src="{{ $item->get_field('header-image')->get_thumbnail_url() }}" alt="">
    @endif
    <div class="bann-cont">
        <div class="row1">
            <h2>{{ $item->get_field('header-title') }}</h2>
            <span>{!! $item->get_field('header-subtitle') !!}</span>
        </div>
    </div>
</section>


<section class="blog-list queretiro-bloc">
    <div class="row1">
        <h1>{{ $item->get_field('bloc-1-titol') }}</h1>
        <div class="contingut">
            {!! $item->get_field('bloc-1-descripcio') !!}
        </div>
    </div>
</section>


<section class="valors gris">
    <div class="row1">
        <h2>{!! $item->get_field('bloc-2-titol') !!}</h2>
        @if( $item->get_field('bloc-2-items') )
        <ul>
            @foreach($item->get_field('bloc-2-items') as $key => $element)
            <li>
                @if ($key % 2 == 0)
                    <div class="text-content">
                        <span>{{ ($key + 1) }}</span>
                        <h3>{{ $element['item-titol']['value'] }}</h3>
                    </div>
                    <div class="image">
                        <img src="{{ $element['item-imatge']['value']->get_thumbnail_url() }}" />
                    </div>
                @else
                    <div class="image img-dsk">
                        <img src="{{ $element['item-imatge']['value']->get_thumbnail_url() }}" />
                    </div>
                    <div class="text-content">
                        <span>{{ ($key + 1) }}</span>
                        <h3>{{ $element['item-titol']['value'] }}</h3>
                    </div>
                    <div class="image img-mob">
                        <img src="{{ $element['item-imatge']['value']->get_thumbnail_url() }}" />
                    </div>
                @endif
            </li>
            @endforeach
        </ul>
        @endif
    </div>
</section>

<section class="blog-list queretiro-bloc">
    <div class="row1">
        <h2>{{ $item->get_field('bloc-3-titol') }}</h2>
        <div class="contingut">
            <p>{{ $item->get_field('bloc-3-descripcio') }}</p>
        </div>
    </div>
</section>

<section class="valors gris">
    <div class="row1">
        <h2>{!! $item->get_field('bloc-4-titol') !!}</h2>
        <div class="tipos">
            @foreach($item->get_field('bloc-4-contingut') as $content)
                <div class="tipo">
                    <div class="tipo-esq">
                        <div class="title">
                            <img src="{{ $content['element-icona']['value']->get_thumbnail_url() }}">
                            <h3>{{ $content['element-titol']['value'] }}</h3>
                        </div>
                        <div class="content">
                            <p>{{ $content['element-descripcio']['value'] }}</p>    
                        </div>
                    </div>
                    <div class="tipo-dre">
                        <span>@Lang('Encontramos:')</span>
                        <ul>
                            @if ($content['element-item-1']['value'] != '')
                                <li>{{ $content['element-item-1']['value'] }}</li>
                            @endif
                            @if ($content['element-item-2']['value'] != '')
                                <li>{{ $content['element-item-2']['value'] }}</li>
                            @endif
                            @if ($content['element-item-3']['value'] != '')
                                <li>{{ $content['element-item-3']['value'] }}</li>
                            @endif
                        </ul>
                        <a href="{{ $content['element-link']['value'] }}" class="btn btn-orange">@Lang('Ver retiros') {{ $content['element-titol']['value'] }}</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@includeIf('Front::partials.banner-inferior')
@endsection

@section('scripts1')
@endsection

