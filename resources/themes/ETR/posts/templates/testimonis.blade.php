@extends('Front::layouts.base')

@section('title')
    {{ strip_tags($item->title) }}
@endsection

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
        .banner-home .banner {
            height: 520px;
            overflow: hidden;
            position: relative;
        }

        .bann-cont {
            top: 25%;
        }

        .bann-cont p {
            display: block !important;
            width: 80%;
        }
    </style>
@endsection

@section('contingut')
    @includeIf('Front::partials.header')

    <section class="banner-home banner-home-img resp-banner promociona">
        <div class="banner" @if ($trans->get_thumbnail_url())style="background-image: url('{{ $trans->get_thumbnail_url() }}'); background-position: center; background-size: cover; background-repeat: no-repeat;"@endif>
        </div>
    </section>
    
    <section class="home-title">
        <div class="row1" style="max-width:856px;">
            <h1>{!! __('Detrás de cada Retiro con nosotros hay una <span>experiencia personal</span>') !!}</h1>
        </div>
    </section>
    
    <section class="testimnoios">
        <div class="row1">
            <ul>
            @php($testimonios = $item->get_field('testimonios'))
            @foreach($testimonios as $testimoni)
                <li>
                    @if ($avatar = $testimoni['avatar']['value'])
                        <div class="avatar">
                            <img src="{{ $avatar->get_thumbnail_url() }}" alt="">
                        </div>
                     @endif
                    <div class="content">
                        <h3>{{ $testimoni['nombre']['value'] }}</h3>
                        <div class="ratings">
                            @for ($i = 1; $i <= 5; $i++)
                                @if ($i <= $testimoni['estrellas']['value'])
                                    <img src="{{ asset($_front."images/heart-full.svg") }}" alt="">
                                @else
                                    <img src="{{ asset($_front."images/heart-empty.svg") }}" alt="">
                                @endif
                            @endfor
                            <span>{{ $testimoni['estrellas']['value'] }} / 5</span>
                        </div>
                        <h6>{{ $testimoni['subtitulo']['value'] }}</h6>
                        <p>{{ $testimoni['descripcion']['value'] }}</p>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </section>

    @php($videos = $item->get_field('videos-youtube'))
    @if ($videos && count($videos))
        <section class="aun-car ytb-videos">
            <div class="row1">
                <h2>@lang('Conoce a nuestros usuari@s')</h2>

                <ul>
                    @foreach($videos as $video)
                        @php($youtubeData = getYoutubePreviewData($video['url-youtube']['value']))
                        <li>
                            <div class="ytb-video">
                                <div class="pre-container">
                                    @if ($youtubeData->embedLink)
                                        <div class="ytb-embed">
                                            <iframe src="{{ $youtubeData->embedLink }}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </div>
                                    @else
                                        <a href="{{ $youtubeData->embedLink ?: $video['url-youtube']['value'] }}" target="_blank" title="{{ isset($video['titulo']) && $video['titulo']['value'] ? $video['titulo']['value'] : $youtubeData->previewTitle }}">
                                            <div class="player">
                                                <img src="{{ $youtubeData->previewImg }}">

                                                <div class="center-img">
                                                    <img src="{{ asset($_front . 'images/yt_icon_rgb.png') }}">
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                </div><div class="video-texts">
                                    <span><a href="{{ $video['url-youtube']['value'] }}" target="_blank" title="{{ isset($video['titulo']) && $video['titulo']['value'] ? $video['titulo']['value'] : $youtubeData->previewTitle }}">{{ isset($video['titulo']) && $video['titulo']['value'] ? $video['titulo']['value'] : $youtubeData->previewTitle  }}</a></span>

                                    <p>{!! nl2br(cut_text((isset($video['descripcion']) && $video['descripcion']['value'] ? $video['descripcion']['value'] : $youtubeData->previewShortDescription), 1024)) !!}</p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    @endif

    <section class="aun-car testimonios-icons">
        <div class="row1">
            <h2>@lang('Confiáis en nosotros y esto es lo que nos da fuerza para seguir')</h2>
            <ul class="iconos">
                @if($iconos = $item->get_field('iconos'))
                    @foreach($iconos as $icon)
                    <li>
                        @if ($img = $icon['icono']['value'])
                            <img src="{{ $img->get_thumbnail_url() }}" alt="">
                        @endif
                        <span>{{ $icon['descripcion']['value'] }}</span>
                    </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </section>

    
    <section class="testimnoios-bloque-eslogan">
        <div class="testimonios-eslogan">
            @if ($img = $item->get_field('imagen'))
                <img src="{{ asset($_front."images/isotip-encuentra-tu-retiro.svg") }}">
            @endif

            <div class="ag-cont">
                {!! $item->get_field('eslogan') !!}
            </div>
        </div>
    </section>

    <section class="testimnoios-ilusionados">
        <div class="container">
            @if ($img = $item->get_field('imagen'))
                <img src="{{ $img->get_thumbnail_url() }}">
            @endif

            <div class="ag-cont">
                <span>{{ $item->get_field('bloque-texto') }}</span>
                <a href="{{ $item->get_field('bloque-link') == '' ? 'javascript:void(0)' : $item->get_field('bloque-link') }}">{{ $item->get_field('bloque-link-text') }}</a>
            </div>
        </div>
    </section>

    <section class="aun-list">
        <div class="row1">
            <h2>@lang('¿Que temática de retiro te apetece?')</h2>
            <p>@lang('Puedes encontrar todos nuestros retiros clasificados por temática')</p>
            <div class="owl-carousel home-themes" id="aun-car-themes">
                @php($page = \App\Post::where("id", 735)->first())
                @php($themes = $page->get_field('tematicas-de-retiro'))
                @if ($themes)
                    @foreach($themes as $theme)
                        @php ( $themeMedia = !empty($theme['imagen']['value']) ? $theme['imagen']['value'] : false )
                        @php ( $themeTitle = !empty($theme['titulo']['value']) ? $theme['titulo']['value'] : '' )
                        @php ( $themeCategory = !empty($theme['categoria']['value']) ? $theme['categoria']['value'] : '' )
                        <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => $themeCategory]) }}">
                                <div class="img-div"> <img src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" @if ($themeMedia && $themeMedia->has_thumbnail())class="owl-lazy" data-src="{{ $themeMedia->get_thumbnail_url() }}"@endif style="min-height: 360px; min-width: 360px"  alt="{{ $themeTitle }}" >
                                    <div class="list-cont">
                                        <h3>@lang($themeTitle)</h3>
                                    </div>
                                </div>
                                <span class="exp-btn">@lang('EXPLORA')</span>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'silencio']) }}">
                            <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/06.jpg') }}" alt="">
                                <div class="list-cont">
                                    <h3>@lang('Silencio')</h3>
                                </div>
                            </div>
                            <span class="exp-btn">@lang('EXPLORA')</span>
                        </a> </div>
                    <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'mindfulness']) }}">
                            <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/02.jpg') }}" alt="">
                                <div class="list-cont">
                                    <h3>Mindfulness</h3>
                                </div>
                            </div>
                            <span class="exp-btn">@lang('EXPLORA')</span>
                        </a> </div>
                    <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'tantra']) }}">
                            <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/05.jpg') }}" alt="">
                                <div class="list-cont">
                                    <h3>Tantra</h3>
                                </div>
                            </div>
                            <span class="exp-btn">@lang('EXPLORA')</span>
                        </a> </div>
                    <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'ayuno']) }}">
                            <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/01.jpg') }}" alt="">
                                <div class="list-cont">
                                    <h3>Ayuno</h3>
                                </div>
                            </div>
                            <span class="exp-btn">@lang('EXPLORA')</span>
                        </a> </div>
                    <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'sport']) }}">
                            <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/04.jpg') }}" alt="">
                                <div class="list-cont">
                                    <h3>Sport</h3>
                                </div>
                            </div>
                            <span class="exp-btn">@lang('EXPLORA')</span>
                        </a> </div>
                    <div class="themes-item"> <a href="{{ route('retiros.landing', ['filter' => 'antiestres']) }}">
                            <div class="img-div"> <img class="owl-lazy" data-src="{{ asset($_front.'images/03.jpg') }}" alt="">
                                <div class="list-cont">
                                    <h3>Antiestrés</h3>
                                </div>
                            </div>
                            <span class="exp-btn">@lang('EXPLORA')</span>
                        </a> </div>
                @endif
            </div>
            <div class="home-crem-mbl">
                <ul>
                    @php( $tipus = get_posts(['post_type' => 'tipos-retiro', 'numberposts' => 4, 'with' => ['metas.customField']]) )
                    @foreach($tipus as $t)
                        @php($t = \App\Post::find($t['id']))
                        @php($poster = $t->get_field('poster'))
                            @if ($t && $poster)
                                <li>
                                    <a href="{{ $t->post_name }}"> <img src="{{ $poster->get_thumbnail_url() }}" alt="">
                                        <div class="list-cont">
                                            <div> <img class="custom-lazy-img" data-src="{{ $poster->get_thumbnail_url() }}" alt="">
                                                <h2>{{ $t->title }}</h2>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                    @endforeach
                </ul>
            </div>
            <a href="{{ \App\Post::get_archive_link('retiros') }}" class="todo-btn">@lang('DESCUBRE MÁS RETIROS')</a>
        </div>
    </section>
    <div class="clearfix"></div>
    @includeIf('Front::partials.banner-inferior')
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script>
var owlThemes = $('#aun-car-themes');
owlThemes.owlCarousel({
    goToFirstSpeed :1000,
    loop:true,
    items:3,
    margin:15,
    lazyLoad: true,
    autoplay:true,
    autoplayTimeout:5500,
    autoplayHoverPause:true,
    nav : true,
    dots : false,
    responsive: {
        0:{ items:1, margin:0,},
        480:{ items:1},
        640:{ items:2},
        768:{ items:3},
        1024:{ items:3}
    },
});
</script>
@endsection