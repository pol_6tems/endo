@extends('Front::layouts.base')

@section('title')
    {{ strip_tags($item->title) }}
@endsection

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
        .banner-home .banner {
            height: 520px;
            overflow: hidden;
            position: relative;
        }

        .bann-cont {
            top: 25%;
        }

        .bann-cont p {
            display: block !important;
            width: 80%;
        }
    </style>
@endsection

@section('contingut')
    @includeIf('Front::partials.header')

    <section class="banner-home banner-home-img resp-banner promociona">
        <div class="banner" @if ($trans->get_thumbnail_url())style="background-image: url('{{ $trans->get_thumbnail_url() }}'); background-position: center; background-size: cover; background-repeat: no-repeat;"@endif>
            <div class="bann-cont">
                <div class="row1">
                    <h1>{!! $item->title !!}</h1>
                    {{--<h1>{!! $item->get_field('titol-slider') !!}</h1>--}}
                    <p style="font-size: 24px;padding-top: 20px;">
                        {!! $item->get_field('subtitulo-header') !!}
                    </p>
                </div>
            </div>
        </div>
    </section>

    <div class="regalo-content">
        @if ($item->get_field('numeros'))
            <section class="passos passos-regalo">
                <div class="row1">
                    <ul>
                        @foreach($item->get_field('numeros') as $key => $numero)
                            <li class="number" data-number="{{ ($key + 1) }}">
                                <div class="numero">
                                    <span>{{ $key + 1 }}</span>
                                </div>
                                <h2>{!! $numero['texto']['value'] !!}</h2>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </section>

            <div class="clearfix-block"></div>
        @endif

        @if ($item->get_field('titulo-recibir') && $item->get_field('kit-incluye'))
            <section class="regalo-incluye">
                <div class="row1">
                    @if ($item->get_field('fotos'))
                        <img src="{{ $item->get_field('fotos')->get_thumbnail_url() }}">
                    @endif

                    <div>
                        <div class="incluye-left">
                            <h2>{!! $item->get_field('titulo-recibir') !!}</h2>

                            <ul>
                                @foreach($item->get_field('kit-incluye') as $included)
                                    <li>
                                        {!! $included['texto']['value'] !!}
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="incluye-right">
                            <h2>{!! $item->get_field('kit-claim') !!}</h2>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        @php($kits = get_posts('retiro-regalo'))
        @if ($kits && $kits->count())
            <section class="regalo-kits">
                @php ($kitsChunked = $kits->chunk(3))
                @foreach($kitsChunked as $kitsChunk)
                    <div class="row1">
                        @foreach($kitsChunk as $kit)
                            <div class="kit">
                                @if ($kit->media())
                                    <img src="{{ $kit->media()->get_thumbnail_url() }}">
                                @endif
                                <div class="kit-content">
                                    <span>@lang('Incluye Caja Regalo con'):</span>

                                    {!! $kit->description !!}
                                    @if ( Auth::user() )
                                        <a href="{{ route('regalos.comprar', ['locale' => app()->getLocale(), 'kit' => $kit->post_name]) }}" class="kit-btn">@lang('Comprar')</a>
                                    @else
                                        <a class="fancybox1 show kit-btn" href="#register">@lang('Comprar')</a>
                                    @endif
                                </div>

                            </div>
                        @endforeach
                    </div>
                @endforeach

            </section>
        @endif

        @if ($item->get_field('condiciones'))
            <section class="regalo-condiciones">
                <div class="row1">
                    <h2>{!! $item->get_field('titulo-condiciones') !!}</h2>

                    <div>
                        @if ($item->get_field('imagen-condiciones'))
                            <div class="condiciones-img">
                                <img src="{{ $item->get_field('imagen-condiciones')->get_thumbnail_url() }}">
                            </div>
                        @endif

                        <ul>
                            @foreach($item->get_field('condiciones') as $condicion)
                                <li>
                                    {!! $condicion['texto']['value'] !!}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </section>
        @endif
    </div>

    <div class="clearfix"></div>
    @includeIf('Front::partials.banner-inferior')
@endsection

@section('scripts1')
    <script type="text/javascript" src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
    <script>
        $(document).ready(function() {
            var owl = $('#testmnl-car');
            owl.owlCarousel({
                goToFirstSpeed :1000,
                loop:true,
                items:1,
                margin:1,
                autoplay:true,
                autoplayTimeout:5500,
                autoplayHoverPause:true,
                nav : true,
                dots : false,
                responsive: {
                    0:{ items:1, margin:0,},
                    480:{ items:1},
                    640:{ items:1},
                    768:{ items:1},
                    1024:{ items:1}
                },
            });
        });

        document.addEventListener("DOMContentLoaded", function(event) {
            fadeNumbers();
        });
    </script>
@endsection

@section('analytics_ec')
    gtag('event', 'view_item_list', {
        "items": [
            @foreach($kits as $kit)
                {
                    "id": "{{ $kit->id }}",
                    "name": "{{ $kit->title }}",
                    "category": "Regalo retiro",
                    "price": "{{ toUserCurrency('EUR', $kit->get_field('precio'), false) }}"
                },
            @endforeach
        ]
    });
@endsection