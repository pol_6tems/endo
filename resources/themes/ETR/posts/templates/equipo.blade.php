@extends('Front::layouts.base')

@section('title')
    {{ $item->title }}
@endsection

@section('contingut')

@php
    $posts = get_posts(['post_type' => 'equipo']);
@endphp

@includeIf('Front::partials.header')

<section class="abt-cont">
    <div class="row1">
        <h1>{{ $item->title }}</h1>
        <p>
            {!! $item->description !!}
        </p>
    </div>
</section>

<section class="team-list">
    <div class="row1">
        <ul>
        @foreach ( $posts as $post )
            @php
                $youtube_link = $post->get_field('youtube-link');
                $img_back = $post->get_field('imagen-secundaria');
                $img_back_url = !empty($img_back) ? $img_back->get_thumbnail_url('large') : $post->translate()->get_thumbnail_url('large');
            @endphp
            <li>
                <a class="fancybox-media" href="javascript:void(0);">
                    <div class="flip-card">
                        <div class="flip-card-inner">
                            <div class="flip-card-front">
                                <div class="team-img">
                                    <img src="{{ $post->translate()->get_thumbnail_url('large') }}" alt="">
                                    {{--<div class="overlay">
                                        <img src="{{ asset($_front.'images/play-circle-white.svg') }}" alt="">
                                    </div>--}}
                                </div>
                                <div class="team-cont">
                                    <h5>{{ $post->title }}</h5>
                                    <p>{!! $post->description !!}</p>
                                </div>
                            </div>
                            <div class="flip-card-back">
                                <div class="team-img">
                                    <img src="{{ $img_back_url }}" alt="">
                                </div>
                                <div class="team-cont">
                                    <h5>{{ $post->title }}</h5>
                                    <p>{!! $post->description !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
        @endforeach
        </ul>
    </div>
</section>
@endsection

@section('scripts1')
<script type="text/javascript" src="{{ asset($_front.'js/jquery.fancybox.js') }}"></script>
<script type="text/javascript" src="{{ asset($_front.'js/jquery.fancybox-media.js') }}"></script>

<script type="text/javascript">
var $ = jQuery.noConflict();

$('.flip-card').on('touch', function() {
    $(this).toggleClass('hovered');
});


$(document).ready(function () {
    /*FancyBox*/
    $('.fancybox-media')
    .attr('rel', 'media-gallery')
    .fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        prevEffect: 'none',
        nextEffect: 'none',
        arrows: false,
        helpers: {
            media: {},
            buttons: {}
        }
    });
});
</script>
@endsection