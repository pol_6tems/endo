@extends('Front::layouts.base')

@section('title')
    {{ strip_tags($item->title) }}
@endsection

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
        .banner-home .banner {
            height: 60vh;
            overflow: hidden;
            position: relative;
        }

        .bann-cont {
            top: 30%;
        }
    </style>
@endsection

@section('contingut')
@includeIf('Front::partials.header')

<section class="banner-home banner-home-img resp-banner promociona">
    <div class="banner" @if ($trans->get_thumbnail_url())style="background-image: url('{{ $trans->get_thumbnail_url() }}'); background-position: center; background-size: cover; background-repeat: no-repeat;"@endif>
        <div class="bann-cont">
            <div class="row1">
                <h1>{!! $item->title !!}</h1>
                {{--<h1>{!! $item->get_field('titol-slider') !!}</h1>--}}
                <p style="font-size: 24px;padding-top: 20px;">
                    &ldquo;{!! $item->get_field('subtitulo-header') !!}&rdquo;
                </p>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>
<div class="arbol-content">
    @if ($item->get_field('titulo-general') && $item->get_field('texto-general'))
        <section>
            <div class="row1">
                <div class="arbol-general">
                    <img src="{{ asset($_front.'images/Grupo_1874.svg') }}" />

                    <div class="ag-cont">
                        <h2>{{ $item->get_field('titulo-general') }}</h2>
                        {!! $item->get_field('texto-general') !!}
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if ($item->get_field('titulo-proyecto') && count($item->get_field('proyecto')))
        <section>
            <div class="row1">
                <div class="arbol-proyecto">
                    <h2>{{ $item->get_field('titulo-proyecto') }}</h2>
                    <div class="proyecto-list">
                        <ul>
                            @foreach($item->get_field('proyecto') as $project)
                                <li>
                                    <div class="blog-cont">
                                        <h3 class="mob-para">{{ $project['item-titulo']['value'] }}</h3>
                                        <p>{!! $project['item-descripcion']['value'] !!}</p>
                                    </div>

                                    @if ($project['item-imagen']['value'])
                                        <div class="blog-img">
                                            <img src="{{ $project['item-imagen']['value']->get_thumbnail_url() }}" alt="">
                                        </div>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if ($item->get_field('video-proyecto'))
        <section>
            <div class="row1">
                <div class="arbol-video">
                    <iframe {{--width="560" height="315"--}} src="https://www.youtube.com/embed/{{ get_youtube_id($item->get_field('video-proyecto')) }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </section>
    @endif

    @if ($item->get_field('mapa-proyecto'))
        <section>
            <div class="arbol-map">
                <img src="{{ $item->get_field('mapa-proyecto')->get_thumbnail_url() }}">
            </div>
        </section>
    @endif


    {{-- TODO: Retiros destacados --}}


    @if (count($item->get_field('valores')))
        <section>
            <div class="row1">
                <div class="arbol-valores">
                    @foreach($item->get_field('valores') as $valor)
                        <div class="arbol-valor">
                            <div>
                                @if ($valor['item-imagen']['value'])
                                    <img src="{{ $valor['item-imagen']['value']->get_thumbnail_url() }}">
                                @endif

                                <h3>{{ $valor['item-titulo']['value'] }}</h3>
                            </div>

                            <p>{!! $valor['item-descripcion']['value'] !!}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
</div>
@includeIf('Front::partials.banner-inferior')
@endsection

@section('scripts1')
<script type="text/javascript" src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script>
$(document).ready(function() {
    var owl = $('#testmnl-car');
    owl.owlCarousel({
        goToFirstSpeed :1000,
        loop:true,
        items:1,
        margin:1,
        autoplay:true,
        autoplayTimeout:5500,
        autoplayHoverPause:true,
        nav : true,
        dots : false,
        responsive: {
            0:{ items:1, margin:0,},
            480:{ items:1},
            640:{ items:1},
            768:{ items:1},
            1024:{ items:1}
        },
    });
});
</script>
@endsection
