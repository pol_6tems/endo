@extends('Front::layouts.base')

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
    <link type="text/css" rel="stylesheet" href="{{ asset($_front.'css/mb.slider.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset($_front.'css/smk-accordion.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset($_front.'css/smk-buscador.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset($_front.'css/datepicker.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset($_front.'css/jqueryui-slider.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset($_front.'css/ion.rangeSlider.min.css') }}" />
@endsection

@section('title', isset($meta_title) ? $meta_title : __('Retiros Espirituales ▷ Encuentra Tu Retiro'))
@section('meta_desc', isset($meta_desc) ? $meta_desc : __('Encuentra todo tipo de retiros espirituales alrededor del mundo: crecimiento personal, yoga, descanso, meditación, detox y mucho más. Reserva online'))
@section('meta_robots', isset($landingRobots) ? $landingRobots : null)

@section('extra_metas')
    @if (!route_name('retiros.landing'))
        @php($canonical = \App\Post::get_archive_link('retiros'))
        @php($pageParam = request('page'))
        @if ($pageParam && $pageParam > 1)
            @php($canonical .= '?page=' . $pageParam)
        @endif

        <link rel="canonical" href="{{ $canonical }}"/>
    @endif
@endsection

@section('contingut')
@php($max = app(\App\Modules\EncuentraTuRetiro\Repositories\PostsRepository::class)->getMaxPrice($items, route_name('retiros.landing')))
@php($max = toUserCurrency("EUR", $max, false))
@php($archive_retiros_url = \App\Post::get_archive_link('retiros'))
@php($breadcrumbCurrent = null)
@if (route_name('retiros.landing'))
    @php($breadcrumbCurrent = $type_filter->title . ($locationTitle ?  ' ' . $locationTitle : ''))
@endif

@includeIf('Front::partials.header')

<div class="ab-breadgr mob-brr">
    <div class="row">
        <ul>
            <li><a href="{{ route('index') }}">@lang('Home')</a></li>
            <li><a href="{{ $archive_retiros_url }}">@lang('Retiros')</a></li>

            @if (isset($breadcrumbCurrent) && $breadcrumbCurrent)
                <li>{{ $breadcrumbCurrent }}</li>
            @endif
        </ul>
    </div>
</div>
<!-- banner starts -->
<section class="banner-inner"> <img src="{{ asset($_front.'images/foto-llistat-retiros.jpg') }}" alt="">
    <div class="ban-search">
        <div class="row1">
            <h1>{!! isset($h1Title) && $h1Title ? $h1Title : '<span> ' . __('Un viaje interior') . '</span> ' .  __('te está esperando') !!}</h1>
        </div>
    </div>
</section>


<section class="mbl-filter">
    <div class="yellow-title">
        <h2>
            @if (isset($listTitle) && $listTitle)
                {!! $listTitle !!}
            @else
                <span><span class="num">{{ (isset($totalRetiros) && $totalRetiros) ? $totalRetiros : '' }}</span> @lang('Experiencias de Retiro')</span> @lang('alrededor del mundo')
            @endif
        </h2>
    </div>
    <div class="row1">
        <ul>
            <li class="order recom">
                <img src="{{ asset($_front.'images/icons/ordenar.svg') }}" alt="">
                <select class="select_box">
                    <option value="rating_desc" @if (request('order') == 'rating_desc')selected @endif>@lang('Recomendado')</option>
                    <option value="fecha_inicio" @if (request('order') == 'fecha_inicio')selected @endif>@lang('Fecha de Inicio')</option>
                    <option value="price_asc" @if (request('order') == 'price_asc')selected @endif>@lang('Precio total de menor a mayor')</option>
                    <option value="price_night_asc" @if (request('order') == 'price_night_asc')selected @endif>@lang('Precio por noche de menor a mayor')</option>
                    <option value="duration_asc" @if (request('order') == 'duration_asc')selected @endif>@lang('Duración')</option>
                </select>
            </li>
            <li>
                <div class="filter-container">
                    <a href="javascript:void(0);" class="filter" id="slide">
                        <img src="{{ asset($_front.'images/icons/filtrar.svg') }}" >@lang('Filtro')
                        <div class="filter-arrow"></div>
                    </a>
                </div>
            </li>
        </ul>
    </div>

    <div class="filter-menu filters">
        <h4><a href="javascript:void(0);"><img src="{{ asset($_front.'images/title-arrw.jpg') }}" alt="">@lang('Filtros')</a></h4>
        {!! getFilters('retiro', [
            'TIPO' => [
                'title' => false,
                'post_type' => 'tipos-retiro',
                'custom_field' => 'tipos-de-retiro',
            ],
            'ESPECIAL' => [
                'post_type' => 'retiro-especial',
                'custom_field' => 'especiales',
                'desplegable' => true,
                'obert' => true
            ],
            'LOCALIZACIÓN' => [
                'type' => 'location',
                'desplegable' => true,
            ],
            'CATEGORIAS' => [
                'post_type' => 'retiro-categoria',
                'custom_field' => 'categoria',
                'desplegable' => true,
                'advanced' => true,
            ],
            'HABITACIONES' => [
                'post_type' => 'retiro-tipo-habitaciones',
                'custom_field' => 'tipo-habitaciones',
                'desplegable' => true,
                'advanced' => true,
            ],
            'DURACIÓN' => [
                'post_type' => 'retiro-duracion',
                'custom_field' => 'duracion',
                'desplegable' => true,
                'advanced' => true,
            ],
            'PÚBLICO' => [
                'post_type' => 'retiros-publico',
                'custom_field' => 'publico',
                'desplegable' => true,
                'advanced' => true,
            ],
            'FORMATO' => [
                'post_type' => 'retiros-formato',
                'custom_field' => 'formato',
                'desplegable' => true,
                'advanced' => true,
            ],
            'NIVEL' => [
                'post_type' => 'retiros-nivel',
                'custom_field' => 'nivel',
                'desplegable' => true,
                'advanced' => true,
            ],
            'ALIMENTACIÓN' => [
                'post_type' => 'retiros-alimentacion',
                'custom_field' => 'alimentacion',
                'desplegable' => true,
                'advanced' => true,
            ],
            'PRESTACIONES' => [
                'post_type' => 'retiro-incluye',
                'custom_field' => 'incluye2',
                'desplegable' => true,
                'advanced' => true,
            ],
            'BENEFICIOS' => [
                'post_type' => 'beneficio',
                'custom_field' => 'beneficio',
                'desplegable' => true,
                'advanced' => true,
            ],
            'YOGA' => [
                'post_type' => 'tipos-de-yoga',
                'custom_field' => 'tipos-de-yoga',
                'desplegable' => true,
                'advanced' => true,
            ],
            'MEDITACIÓN' => [
                'post_type' => 'tipo-de-meditacion',
                'custom_field' => 'tipo-de-meditacion',
                'desplegable' => true,
                'advanced' => true,
            ],
            'PRECIO' => [
                'type' => 'price',
                'post_type' => 'retiro-duracion',
                'custom_field' => 'duracion-retiros',
                'advanced' => true,
            ]
        ], true) !!}
    </div>
</section>
{{-- 
<section class="search-sec search-hom search-inn">
    <div class="row1">
        @includeIf('Front::partials.buscador', ['llista' => 'retiros'])
    </div>
</section>
 --}}
<!-- banner ends -->
<section class="retiros-sec">
    <div class="lds-ripple"><div></div><div></div></div>
    <div class="row1">
        <div class="ret-lft filters">
            {!! getFilters('retiro', [
                'TIPO' => [
                    'title' => false,
                    'post_type' => 'tipos-retiro',
                    'custom_field' => 'tipos-de-retiro',
                ],
                'ESPECIAL' => [
                    'post_type' => 'retiro-especial',
                    'custom_field' => 'especiales',
                    'desplegable' => true,
                    'obert' => true
                ],
                'LOCALIZACIÓN' => [
                    'type' => 'location',
                    'desplegable' => true,
                    'obert' => true,
                ],
                'CATEGORIAS' => [
                    'post_type' => 'retiro-categoria',
                    'custom_field' => 'categoria',
                    'desplegable' => true,
                    'advanced' => true,
                ],
                'HABITACIONES' => [
                    'post_type' => 'retiro-tipo-habitaciones',
                    'custom_field' => 'tipo-habitaciones',
                    'desplegable' => true,
                    'advanced' => true,
                ],
                'DURACIÓN' => [
                    'post_type' => 'retiro-duracion',
                    'custom_field' => 'duracion-retiros',
                    'desplegable' => true,
                    'obert' => true,
                    'advanced' => true,
                ],
                'PÚBLICO' => [
                    'post_type' => 'retiros-publico',
                    'custom_field' => 'publico',
                    'desplegable' => true,
                    'advanced' => true,
                ],
                'FORMATO' => [
                    'post_type' => 'retiros-formato',
                    'custom_field' => 'formato',
                    'desplegable' => true,
                    'advanced' => true,
                ],
                'NIVEL' => [
                    'post_type' => 'retiros-nivel',
                    'custom_field' => 'nivel',
                    'desplegable' => true,
                    'advanced' => true,
                ],
                'ALIMENTACIÓN' => [
                    'post_type' => 'retiros-alimentacion',
                    'custom_field' => 'alimentacion',
                    'desplegable' => true,
                    'advanced' => true,
                ],
                'PRESTACIONES' => [
                    'post_type' => 'prestaciones-alojamientos',
                    'custom_field' => 'prestaciones',
                    'desplegable' => true,
                    'advanced' => true,
                ],
                'BENEFICIOS' => [
                    'post_type' => 'beneficio',
                    'custom_field' => 'beneficio',
                    'desplegable' => true,
                    'advanced' => true,
                ],
                'YOGA' => [
                    'post_type' => 'tipos-de-yoga',
                    'custom_field' => 'tipos-de-yoga',
                    'desplegable' => true,
                    'advanced' => true,
                ],
                'MEDITACIÓN' => [
                    'post_type' => 'tipo-de-meditacion',
                    'custom_field' => 'tipo-de-meditacion',
                    'desplegable' => true,
                    'advanced' => true,
                ],
                'PRECIO' => [
                    'type' => 'price',
                    'post_type' => 'retiro-duracion',
                    'custom_field' => 'duracion-retiros',
                    'advanced' => true,
                ]
        ]) !!}
        </div>
        <div class="ret-rgt">
            <div class="retiros-fil filters">
                <div class="row1">
                
                </div>
            </div>
            <div class="tit-div">
                <h2>
                    @if (isset($listTitle) && $listTitle)
                        {!! $listTitle !!}
                    @else
                        <span><span class="num">{{ (isset($totalRetiros) && $totalRetiros) ? $totalRetiros : '' }}</span> @lang('Experiencias de Retiro')</span> @lang('alrededor del mundo')
                    @endif
                </h2>
                
                <div class="drop">
                    <span>@lang('ORDENAR POR'):</span>
                    <div class="recom ordenar-por">
                        <select class="select_box">
                            <option value="rating_desc" @if (request('order') == 'rating_desc')selected @endif>@lang('Recomendado')</option>
                            <option value="fecha_inicio" @if (request('order') == 'fecha_inicio')selected @endif>@lang('Fecha de Inicio')</option>
                            <option value="price_asc" @if (request('order') == 'price_asc')selected @endif>@lang('Precio total de menor a mayor')</option>
                            <option value="price_night_asc" @if (request('order') == 'price_night_asc')selected @endif>@lang('Precio por noche de menor a mayor')</option>
                            <option value="duration_asc" @if (request('order') == 'duration_asc')selected @endif>@lang('Duración')</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="lista">
                @if (isset($retiros) && $retiros->count())
                    @include('Front::ajax.retiro', ['retiros' => $retiros, 'totalRetiros' => $retiros->count()])
                @else
                    <ul class="ret-lst">
                        @for($i = 0; $i < 10; $i++)
                        <li class="skeleton">
                            <div class="obr-pad">
                                <div class="obr-img">
                                    <img width="285" height="270" src="" alt="">
                                </div>
                                <div class="obr-cnt">
                                    <h3></h3>
                                    <h2></h2>
                                    <span></span>
                                    <div class="mob-retiros-lst">
                                        <ul class="lst-icon">
                                            <li class="noc"></li>
                                            <li class="idiom"></li>
                                            <li class="menu"></li>
                                            <li class="tran"></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="price">
                                    <div class="mob-price">
                                        <div class="p-rgt">
                                            <h2></h2>
                                        </div>
                                        <div class="p-lft">
                                            <div class="rating"></div>
                                            <a class="mas-btn"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endfor
                    </ul>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/buscador.js') }}"></script>
<script src="{{ asset($_front.'js/datepicker.min.js') }}"></script>
<script src="{{ asset($_front.'js/datepicker.'.$language_code.'.js') }}"></script>
<script src="{{ asset($_front.'js/smk-accordion.js') }}"></script>
<script src="{{ asset($_front.'js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.mb.slider.js') }}"></script>
<script src="{{ asset($_front.'js/jqueryui-slider.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.showmore.js') }}"></script>
<script>
    var url, price, metas, metas_or, metas_and, loc, price;
    var isPrice = initial = isLocation = true;
    var $archive_retiros_url = "{{ $archive_retiros_url }}";
    var $archive_default_title = '{!! '<span> ' . __('Un viaje interior') . '</span> ' . __('te está esperando')  !!}';
    var $archive_default_list_title = '{!! '<span><span class="num">' . ((isset($totalRetiros) && $totalRetiros) ? $totalRetiros : '') . '</span> ' . __('Experiencias de Retiro') . '</span> ' . __('alrededor del mundo') !!}';
    var is_landing = was_landing = false;
    var landing_metas = [];
    var landing_metas_or = [];
    var landing_loc = [];
    var landing_url = '';
    var landing_type_slug = '';
    var landing_loc_slug = '';
    var range_slider;
    var ajax = null;
    var title_filter_tipo = '<span>@lang('Retiro')</span> @lang('alrededor del mundo')';

    function process_url(post_name = '', checked = false) {
        metas_or = [];
        metas = [];
        loc = [];
        metas_and = [];
        filterNodes = []

        var url = new URL(window.location.href);
        var url2 = new URL(window.location.origin + window.location.pathname);

        var date = url.searchParams.get('date');
        var range = url.searchParams.get('range');
        var min_price = url.searchParams.get('min');
        var max_price = url.searchParams.get('max');
        var pagina = url.searchParams.get('page');

        var parameters = ['loc[]', 'filter[]'];
        filters = url.searchParams.getAll('filter[]');

        if (was_landing && is_landing) {
            is_landing = false;
            was_landing = false;

            url = new URL($archive_retiros_url);
            url2 = new URL($archive_retiros_url);

            url.searchParams.append('filter[]', landing_type_slug);
            if (landing_loc_slug) {
                url.searchParams.append('loc[]', landing_loc_slug);
            }

            $('.banner-inner .row1 h1').html($archive_default_title);
            $('.ret-rgt .tit-div h2').html($archive_default_list_title);
        }

        // Refactoritzar
        if ( !checked ) {
            for ( param in parameters ) {
                let parameter = parameters[param];
                // Filtra els repetits
                // Agafa tots els parametres del tipus $parameter
                entries = url.searchParams.getAll(parameter);
                // Elimina els paràmetres que siguin el suq acabem de clicar
                entries = entries.filter(item => item !== post_name);
                entries.forEach(function(e) {
                    url2.searchParams.append(parameter, e);
                });
            }
        }

        if ( !checked ) {
            url = url2;

            // Col·loquem els parametres min/max en cas de sobreescritura
            if ( !isPrice && min_price && !url.searchParams.get('min') ) {
                url.searchParams.append('min', min_price);
                metas.push(['precio', '>=', min_price]);
            }

            if ( !isPrice && max_price && !url.searchParams.get('max') ) {
                url.searchParams.append('max', max_price);
                metas.push(['precio', '<=', max_price]);
            }
        } else {
            let parameter = (isLocation) ? 'loc[]' : 'filter[]';
            url.searchParams.append(parameter, post_name);
        }

        if ( isPrice ) {
            if (post_name == '' && min_price && max_price) {
                post_name = [min_price, max_price];
            }

            if (post_name != '') {
                url.searchParams.delete('min');
                url.searchParams.delete('max');

                url.searchParams.set('min', post_name[0]);
                url.searchParams.set('max', post_name[1]);

                metas.push(['precio', '>=', post_name[0]]);
                metas.push(['precio', '<=', post_name[1]]);
            }
        }

        if (is_landing) {
            metas = $.merge(metas, landing_metas);
            metas_or = $.merge(metas_or, landing_metas_or);
            loc = $.merge(loc, landing_loc);
        }

        url.searchParams.getAll('filter[]').forEach(function(el) {
            var element = $('[data-post_name="' + el + '"]');
            filterNodes.push(element);

            element.prop('checked', true);

            metas.push([element.first().data('cf'), 'like', '%\"' + element.first().data('id') + '\"%']);
            metas_or.push(element.first().data('cf'));
        });

        filterNodes = filterNodes.map(function(e) {
            if (e.data("cf") == "tipos-de-retiro") return e.data('title').split(" ")[0];
            return e.data('title');
        }).splice(0, 3);

        if (filterNodes.length > 0) {
            if (filterNodes.length > 1) {
                last = filterNodes.pop();
                title_filter_tipo = filterNodes.join(', ') + " y " + last;
            } else {
                title_filter_tipo = filterNodes[0];
            }
        } else {
            title_filter_tipo = '<span>@lang('Retiro')</span> @lang('alrededor del mundo')';
        }

        url.searchParams.getAll('loc[]').forEach(function(el) {
            var element = $('[data-post_name="' + el + '"]');
            element.prop('checked', true);
            loc.push(el);
        });

        loc = removeDups(loc);

        if (date) {
            if (range) {
                for(var i = 1; i <= range; i++) {
                    var currentDate = new Date(date);
                    tomorrow = currentDate.addDays(i);
                    yesterday = currentDate.addDays(-1 * i);

                    tomorrow =  tomorrow.getFullYear() + "-" + (pad(tomorrow.getMonth() + 1, 2)) + "-" + pad(tomorrow.getDate(), 2);
                    yesterday =  yesterday.getFullYear() + "-" + (pad(yesterday.getMonth() + 1, 2)) + "-" + pad(yesterday.getDate(), 2);

                    metas.push(['calendario', 'like', '%\"'+tomorrow+'\"%']);
                    metas.push(['calendario', 'like', '%\"'+yesterday+'\"%']);
                }
                if (!url.searchParams.has('range')) {
                    url.searchParams.append('range', range);
                }
            }
            if (!url.searchParams.has('date')) {
                url.searchParams.append('date', date);
            }
            metas_and.push('calendario');
            metas.push(['calendario', 'like', '%\"'+date+'\"%']);
        }

        isLocation = isPrice = false;

        if (is_landing) {
            was_landing = true;
        }

        url.searchParams.delete('page');
                
        return [url, metas];
    }

    function carregarRetiros(metas = [], callback = function() {}) {
        var order = $('.recom select').val();

        if (ajax !== null) ajax.abort();

        ajax = $.ajax({
            url: ajaxURL,
            method: "POST",
            data: {
                action: "loadRetiros",
                method: 'html',
                parameters: {
                    metas: metas,
                    metas_or: metas_or,
                    metas_and: metas_and,
                    location: loc,
                    price: price,
                    order: order
                },
            },
            success: function(data) {
                $('.lista').html(data);
                $('.retiros-sec').removeClass('loading');
                var count = $('.lista').find('#count').val();
                $('.tit-div h2 span.num').html( count );
                $('.mbl-filter h2 span.num').html( count );
                
                $('.tit-div h2').html(`<span><span class="num">${count} @lang('Experiencias de ')</span></span> ${title_filter_tipo}`);
                $('.mbl-filter h2').html(`<img src="{{ asset($_front.'images/title-arrw.jpg') }}" alt=""><span><span class="num">${count}</span> @lang('Experiencias de ')</span> ${title_filter_tipo}`);

                $(filters_container).html("");
                var url = new URL(window.location.href);
                
                filter = url.searchParams.getAll('filter[]');
                filter = filter.concat(loc);

                filter.forEach(function(element) {
                    var tipo_el = $('input[data-post_name="' + element + '"]');

                    if (!$(`.tag[data-filter="${element}"]`).length) {
                        $(filters_container).append(`<div class="tag" data-cf="${tipo_el.data('cf')}" data-filter="${element}">${tipo_el.data('title')}<span>x</span></div>`);
                    }
                });

                callback();
                fixPagination();
                ajax = null;
            },
            error: function(data) {
                ajax = null;
            }
        });
    }

    function parseLandingUrl() {
        var slug  = window.location.href.replace($archive_retiros_url, '');
        slug = slug.replace('/l/', '').split('?')[0];

        if ( slug != '' ) {
            var slug_params = slug.split('/');
            if ( slug_params.length ) {
                is_landing = true;
                landing_url = window.location.href;

                landing_type_slug = slug_params[0].replace(/_/g, '-');

                // Agafar els checkbox pel slug de la URL
                var tipo_el = $('input[data-post_name=' + landing_type_slug + ']');

                // Fer check als checkboxes
                tipo_el.attr('checked', true);

                // Afegir els filtres al searcher
                if (!$(`.tag[data-filter="${landing_type_slug}"]`).length) {
                    $(filters_container).append(`<div class="tag" data-cf="${tipo_el.data('cf')}" data-filter="${landing_type_slug}">${tipo_el.data('title')}<span>x</span></div>`);
                }
                
                $(filters_container).removeClass('hide');

                // Posar els filtres al metas i loc
                landing_metas.push( [tipo_el.data('cf'), 'like', '%' + tipo_el.data('id') + '%'] );
                landing_metas_or.push( tipo_el.data('cf') );

                var title_loc = '';

                if ( slug_params.length > 1 ) {
                    landing_loc_slug = slug_params[1].replace(/-/g, '_');

                    var loc_el = $('input[data-slug=' + landing_loc_slug + ']');
                    loc_el.attr('checked', true);

                    if (!$(`.tag[data-filter="${landing_loc_slug}"]`).length) {
                        $(filters_container).append(`<div class="tag" data-cf="${loc_el.data('cf')}" data-filter="${landing_loc_slug}">${loc_el.data('title')}<span>x</span></div>`);
                    }

                    landing_loc.push( loc_el.attr('name') );
                    landing_loc = removeDups(landing_loc);
                    title_loc = $('input[data-slug=' + landing_loc_slug + ']').data('title');
                }

                // Modificar el títol de la cerca
                var title_tipo = $('input[data-post_name=' + landing_type_slug + ']').data('title');

                if ( title_tipo != '' && title_loc != '' ) {
                    $('.tit-div h2').html(`<span><span class="num">{{ (isset($totalRetiros) && $totalRetiros) ? $totalRetiros : '' }}</span> @lang('retiros de ')${title_tipo}</span> @lang('en ')${title_loc}`);

                    $('.banner-inner .ban-search h1').html(`<span>{{ ucfirst(__('retiros de ')) }}${title_tipo}</span> @lang('en ')${title_loc}`);

                    $('.mbl-filter h2').html(`<img src="{{ asset($_front.'images/title-arrw.jpg') }}" alt=""><span><span class="num">{{ (isset($totalRetiros) && $totalRetiros) ? $totalRetiros : '' }}</span> @lang('retiros de ')${title_tipo}</span> @lang('en ')${title_loc}`);
                } else if (title_tipo != '') {
                    $('.tit-div h2').html(`<span><span class="num">{{ (isset($totalRetiros) && $totalRetiros) ? $totalRetiros : '' }}</span> @lang('retiros de ')${title_tipo}</span>`);

                    $('.banner-inner .ban-search h1').html(`<span>{{ ucfirst(__('retiros de ')) }}${title_tipo}</span>`);

                    $('.mbl-filter h2').html(`<img src="{{ asset($_front.'images/title-arrw.jpg') }}" alt=""><span><span class="num">{{ (isset($totalRetiros) && $totalRetiros) ? $totalRetiros : '' }}</span> @lang('retiros de ')${title_tipo}</span>`);
                }

                if ( slug_params.length > 1 ) {
                    landing_loc_slug = loc_el.data('post_name');
                }
            }
        }
    }

    var $ = jQuery.noConflict();
    // $j is now an alias to the jQuery function; creating the new alias is optional.
    $(document).ready(function () {
        var max_range = parseInt('{{ $max }}');
        var url = new URL(window.location.href);
        var min_price = (url.searchParams.get('min')) ? url.searchParams.get('min') : 0;
        var max_price = (url.searchParams.get('max')) ? url.searchParams.get('max') : parseInt('{{ $max }}');
        var currentValues = [min_price, max_price];
        
        // Posar Max Value
        range_slider = $(".js-range-slider").ionRangeSlider({
            type: "double",
            grid: true,
            step: 50,
            min: 0,
            max: max_range,
            from: min_price,
            to: max_price,
            onFinish: function (value) {
                var range = [value.from, value.to];
                isPrice = true;

                if (range_slider.hasClass('mobile')) {
					$filters = $(range_slider).closest('.filters');
					$filters.removeClass('visible');
					$filters.animate({
						'left': '-=100%'
					});
                    $("body.hide").removeClass("hide");
				}

                [url, metas] = process_url(range);
                window.history.pushState({},"", decodeURIComponent(url.href));
                $('.retiros-sec').addClass('loading');
                carregarRetiros(metas);
            }
        });

        parseLandingUrl();

        [url, metas] = process_url();
        if (!is_landing) {
            carregarRetiros(metas);
        }

        $('.fancybox').fancybox();
        /*FancyBox*/
        $(".fancybox1").fancybox({
            afterLoad: function () {
                $('#flexslider2').flexslider({
                    animation: "slide",
                    slideshowSpeed: 10000,
                    animationDuration: 5000,
                    easing: "swing",
                    directionNav: true,
                    controlNav: false,
                    pausePlay: false,
                    autoPlay: true,
                    start: function (slider) {
                        $('body').removeClass('loading');
                        if ($(window).width() < 768) {
                            $('.flex-prev').trigger("click");
                        }
                    }

                });
            }
        });

        window.onpopstate = function(e) {
            if (landing_url !== '' && landing_url === window.location.href) {
                parseLandingUrl()
            }

            [url, metas] = process_url();
            $('.retiros-sec').addClass('loading');
            carregarRetiros(metas);
        };

        initial = false;
    });

    $(document).on('click', '.searcher-duration', function () {
        var post_name = $(this).data('filter');

        var filter = document.querySelector(`[data-post_name="${post_name}"]`);

        if (typeof filter !== 'undefined' && filter) {
            filter.click();
        }
    });

    function fixPagination() {
        var new_url = new URL(window.location.href);

        $('.pagination a').each((i, p) => {
            new_url.searchParams.delete('page');
            new_url.searchParams.delete('order');
            var aux = new URL(p.href);

            if (page = aux.searchParams.get('page')) {
                aux.searchParams.delete('page');
                new_url.searchParams.append('page', page);

                if (order = aux.searchParams.get('order')) {
                    new_url.searchParams.append('order', order);
                }

                p.href = decodeURI(new_url.href);
            }
        });
    }
    /* search drop down script */

    $(document).on('click', '.arrival-date', function (e) {
        var $this = $(this).find("input");
        var url = new URL(window.location.href);
        var checked = !$this.prop('checked');
        $('.datepicker-here').data('datepicker').clear();

        $(".post-date").each(function() {
            $(this).prop('checked', false);
        });

        if (e.target.tagName == "INPUT") $this.prop('checked', !checked);
        else $this.prop('checked', checked);
        
        if ($this.is(':checked')) {
            url.searchParams.set('date', $this.data('date'));
            url.searchParams.set('range', 15);
        } else {
            url.searchParams.delete('range');
            url.searchParams.delete('date');
        }

        window.history.pushState({},"", decodeURIComponent(url.href));
        [url, metas] = process_url();
        window.history.pushState({},"", decodeURIComponent(url.href));
        $('.retiros-sec').addClass('loading');
        carregarRetiros(metas);
    });

</script>
<script src="{{ asset($_front.'js/pages/llista.js') }}"></script>
@endsection