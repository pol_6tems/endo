@extends('Front::layouts.base')

@section('contingut')
@php
    $subcategoria = $item->get_field('subcategoria');
    $categoria = $subcategoria->get_field('categoria');

    $grups = get_posts([
        'post_type' => 'grup-preguntes',
        'metas' => [
            ['subcategoria', $subcategoria->id]
        ]
    ]);

    $preguntes = get_posts([
        'post_type' => 'soporte-preguntas',
        'metas' => [
            ['grupo-preguntas', $item->id]
        ]
    ]);
@endphp

@includeIf('Front::partials.header')
<!-- centro-yellow starts -->
<section class="centro-grey">
    <div class="row1"> 
        <h2>Centro de ayuda</h2>
        {{--<div class="search">
            <a href="javascript:void(0);" class="srch-btn search-txt"></a>
            <input type="text" placeholder="¿Qué estás buscando?" class="search-txt">
        </div>--}}
        <h3><a href="tel:+34972664640">@Lang('+34 972 66 46 40')</a></h3>
    </div>
</section>
<!-- centro-yellow ends -->

<!-- ayuda-sec starts -->
<section class="ayuda-sec">
<div class="row1"> 
    <div class="dudas-lft">
        <ul>
            @foreach($grups as $grup)
                <li class="{{ ($grup->id == $item->id) ? 'active'  : '' }}"><a href="{{ $grup->get_url() }}">{{ $grup->title }}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="dudas-rgt">
        <ul class="breadgrum">
            <li><a href="{{ $categoria->get_url() }}">{{ $categoria->title }}</a></li>
            <li><a href="{{ $subcategoria->get_url() }}" class="active">{{ $subcategoria->title }}</a></li>
            <li>{{ $trans->title }}</li>
        </ul>
        <div id="accordion">
        @foreach($preguntes as $pregunta)
            <div class="accordion_in">
                <h2 class="acc_head">{{ $pregunta->title }}</h2>
                <div class="acc_content">
                    <div class="lorem-pad">
                        <p>{!! strip_tags($pregunta->description, 'p') !!}</p>
                    </div>
                    {{--<div class="servido-pad">
                        <p>¿Le ha servido nuestra ayuda?</p>
                        <ul>
                            <li><a href="javascript:void(0);">SI</a></li>
                            <li><a href="javascript:void(0);">NO</a></li>
                        </ul>
                    </div>--}}
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>
</section>
<!-- ayuda-sec ends -->
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset($_front.'css/accordion_custom.css') }}">
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/smk-accordion.js') }}"></script>
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script>
/*$( "#accordion" ).smk_Accordion({
    closeOther: true,
    closeAble: true,
});*/
</script>
@endsection