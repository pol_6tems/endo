@extends('Front::layouts.base')


@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
    <link href="{{ asset($_front.'css/custom-datepicker.css') }}" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="{{ asset($_front.'css/easy-responsive-tabs.css') }}" />
    <style>
        .jx_ui_Widget {display: none!important;}
    </style>
@endsection

@section('contingut')
@includeIf('Front::partials.header')
@php
    $post = $item;
    $habitaciones = [];
    $ubicacion = null;
    $alojamiento = null;

    $ofertes = $post->get_field('ofertas-retiro');

    $ofertesWithValue = [];
    if ($ofertes) {
        foreach ($ofertes as $oferta) {
            if (isset($oferta['precio']['value']) && $oferta['precio']['value'] && isset($oferta['habitacion']['value']) && $oferta['habitacion']['value']) {
                $ofertesWithValue[] = $oferta;
            }
        }
    }

    if ($ofertes) {
        foreach( $ofertesWithValue as $key => $oferta ) {
            if (is_array($oferta) && is_object($oferta['habitacion']['value']) && $oferta['habitacion']['value']) {
                $habitacion = $oferta['habitacion']['value'];
                if ( $habitacion ) $habitaciones[$habitacion->id] = $habitacion;
            }
        }
        if ( isset($habitacion) ) {
            $alojamiento = $habitacion->get_field('alojamiento');
            $ubicacion = ($alojamiento && is_object($alojamiento)) ? $alojamiento->get_field('localizacion') : null;

            if (!is_object($alojamiento)) {
                $alojamiento = null;
            }
        }
    }

    $ofertaBarata = obtenirOfertaBarata($post);

    $trans = $post->translate();
    $galeria = $post->get_field('galeria');
    $calendari = $post->get_field('calendario');
    $author = $post->author;

    $author->load('metas.customField');

    $minDeposito = 0;
    if ($author) {
        $moneda = ($author->get_field('moneda')) ? $author->get_field('moneda') : null;

        $deposito = $author->get_field('deposito');
        $pago = $author->get_field('pago');
        $politica = $author->get_field('politica');

        $minDeposito = (is_numeric($deposito)) ? round(obtenirPreuAmbDescompteOferta($ofertaBarata) * ($deposito / 100), 0, PHP_ROUND_HALF_DOWN) : 0;

        $payDaysBefore = 0;

        if ($pago == '10_antes') {
            $payDaysBefore = 10;
        }

        if ($pago == '30_antes') {
            $payDaysBefore = 30;
        }

        $cancelDaysBefore = 10;

        if ($politica == 'moderada') {
            $cancelDaysBefore = 30;
        }
    }

    $currencySymbol = userCurrency();

    $tipusreserva = json_decode($post->get_field('tipo-reserva'),true);
    /* TAGS */
    $tags = array('tipos-de-retiro', 'categoria', 'beneficio', 'formato', 'arte');
    $totalTags = collect();
    foreach ( $tags as $tag ) {
        $aux = $post->get_field($tag) ?: [];
        $totalTags = $totalTags->merge($aux);
    }

    /* BREADCRUMB */
    $tipo = !empty($totalTags) && !empty($totalTags[0]) ? $totalTags[0]->translate(true)->title : 'none';
    $postName = !empty($totalTags) && !empty($totalTags[0]) ? $totalTags[0]->translate(true)->post_name : 'none';
    $tipo_slug = str_slug($postName);

    $breadcum_urls = array();

    if ( ($alojamiento = $item->get_field('alojamiento')) && !empty($tipo) && !empty($alojamiento->id) ) {
        $location = \App\Modules\EncuentraTuRetiro\Models\Location::where('alojamiento_id', $alojamiento->id)->first();
        $breadcum_urls[] = [
            'text' => $tipo,
            'url' => route('retiros.landing', ['filter' => $tipo_slug]),
        ];
        if ( !empty($location) ) {
            if ( !empty($location->pais) ) {
                $breadcum_urls[] = [
                    'text' => $tipo . ' ' . $location->pais,
                    'url' => route('retiros.landing', ['filter' => $tipo_slug, 'location' => str_slug($location->pais)]),
                ];
            }
            if ( !empty($location->region) ) {
                $breadcum_urls[] = [
                    'text' => $tipo . ' ' . $location->region,
                    'url' => route('retiros.landing', ['filter' => $tipo_slug, 'location' => str_slug($location->region)]),
                ];
            }
            if ( !empty($location->provincia) ) {
                $breadcum_urls[] = [
                    'text' => $tipo . ' ' . $location->provincia,
                    'url' => route('retiros.landing', ['filter' => $tipo_slug, 'location' => str_slug($location->provincia)]),
                ];
            }
        }
    }

    $peticionesUser = \App\User::where('email', 'peticiones@encuentraturetiro.com')->first();
@endphp
<input id="post_name" type="hidden" value="{{ $post->post_name }}"/>
<!-- banner starts -->
<section class="banner-load retiros">
    @includeIf('Front::partials.single-gallery', ['gallery' => $galeria])
</section>
<div id="flexslider2" class="flexslider mob-slider">
    <ul class="slides">
        @if ( !empty($galeria) )
            @foreach($galeria as $imatge)
                @if ($imatge)
                <li><img src="{{ $imatge->get_thumbnail_url("medium") }}" alt="">
                    <div class="flex-ccn">
                        {{-- <h2>Lorem ipsum</h2> --}}
                    </div>
                </li>
                @endif
            @endforeach
        @endif
    </ul>
</div>
{{--
<div class="ab-breadgr mob-bread mob-show">
    <div class="row">
        <ul>
            <li><a href="{{ get_home_page() }}">@Lang('Home')</a></li>
            <li><a href="{{ \App\Post::get_archive_link('retiros') }}">@Lang('Retiros')</a></li>

            @foreach( $breadcum_urls as $bread )
                <li><a href="{{ $bread['url'] }}">{{ $bread['text'] }}</a></li>
            @endforeach
        </ul>
    </div>
</div>
--}}

<div class="retiro-single">
    <div class="breadcrumb mob-brr">
        <div class="row">
            <ul>
                <li><a href="{{ get_home_page() }}">@Lang('Home')</a></li>
                <li><a href="{{ \App\Post::get_archive_link('retiros') }}">@Lang('Retiros')</a></li>
                
                @foreach( $breadcum_urls as $bread )
                    <li><a href="{{ $bread['url'] }}">{{ $bread['text'] }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

<!-- banner ends -->
<section class="retiros-sec retiros">
    @if ( $ofertes )
    <div class="row">
        <div class="reserva-mbl">
            <div class="reserva">
                @if ($ofertes && count($ofertes) && !empty($calendari) && !empty($calendari->calendari) && $minDeposito)
                    @if($tipusreserva["immediata"]!== "0")
                        <a href="#cheapest-offer" class="submit smooth-link">@lang('Reserva')</a>
                    @else
                        <a href="#cheapest-offer" class="submit smooth-link">@lang('Petición de reserva')</a>
                    @endif
                @endif
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="retro-lft">
            @if (session('compra'))
                <div class="alert alert-success">
                    @Lang('Retiro Comprado Correctamente')
                </div>
            @endif

            <div class="white-box-lft">
                {{--
                @if ($author)
                        <div class="ret-tit-rgt"> <img src="@if ($author->get_field('foto-organizador')){{ $author->get_field('foto-organizador')->get_thumbnail_url() }}@else{{ $author->get_avatar_url() }}@endif" alt="">
                        <h3>{{ $author->fullname() }}</h3>
                    </div>
                @endif
                --}}
                <div class="ret-tit-lft description">
                    <h1>{{ $trans->title }}</h1>
                    <ul class="date-icon">
                        @if( $ubicacion )
                            <li class="loc">{{ print_location($ubicacion, ['locality', 'administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'country']) }}</li>
                        @endif
                    </ul>

                    <div class="descripcion-vermas">
                        <p>{!! strip_tags(remove_style_tags($trans->description), "<span><div><p>") !!}</p>
                    </div>

                    <button class="btn-org more">@Lang('Ver más')</button>

                </div>

            </div>
            <div class="retro-lft single-retiro">
                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border single-retiro">
                        <div class="cnt-divider">
                            @if ($totalTags)
                                <h2>@Lang('Características')</h2>
                                <ul class="tags">
                                    @foreach($totalTags as $k => $tipo)
                                        @if ($k > 12) @break @endif
                                        <li><a href="javascript:void(0);"><h3>{{ $tipo->title }}</h3></a></li>
                                    @endforeach
                                </ul>
                            @endif
                            @includeIf('Front::partials.carac-list', ['post' => $post])
                        </div>
                        <button class="toggle-btn" data-open-txt="@lang('MENOS INFORMACIÓN')" data-closed-txt="@lang('MÁS INFORMACIÓN')">@Lang('MÁS INFORMACIÓN')</button>
                    </div>
                </div>
            </div>

            <!-- FACILITADORES -->
            @if ($facilitadores = $post->get_field('facilitadores'))
            <div class="white-box-lft white-box-tit">
                <h2>@Lang('Facilitadores y organizadores del retiro')</h2>
                <ul class="facil-lst retiro">
                @foreach($facilitadores as $key => $facilitador)
                    @if ( empty($facilitador['facilitador']['value']) ) @continue @endif
                    
                    @php( $facilitador = $facilitador['facilitador']['value'] )
                    @php( $facilitador = is_object($facilitador) ? $facilitador : \App\Post::find($facilitador) )
                    
                    @if ( empty($facilitador) ) @continue @endif

                    @php($facilitadorUrl = $facilitador->get_url())

                    @if ($key == 4)
                        <div style="text-align: center;">
                            <button class="btn-org more">@Lang('Ver más')</button>
                        </div>
                        <div class="slide">
                    @endif
                    <li>
                        <div class="ret-tit-lft">
                            <div class="lft-img">
                                @if ($media = $facilitador->media())
                                    <a href="{{ $facilitador->get_url() }}">
                                        <img src="{{ $media->get_thumbnail_url("medium") }}" alt="">
                                    </a>
                                @endif
                            </div>
                            <div class="cont-div">
                                @php($name = explode(" ", $facilitador->title)[0])
                                <h3><span>{{ $name }}</span></h3>
                                <p>{{ get_excerpt($facilitador->description, 45) }} <a href="{{ $facilitadorUrl }}">@Lang('Leer más')</a></p>
                            </div>
                        </div>
                        <div class="ret-tit-rgt"> <a href="{{ $facilitadorUrl }}" target="_blank" class="en-btn">@Lang('Ver Ficha')</a></div>
                    </li>
                    @if ($key > 1 && $key == (count($facilitadores) - 1) )

                        <div style="text-align: center;">
                            <button class="btn-org less" style="display:none;">@Lang('Ver menos')</button>
                        </div>
                    @endif
                @endforeach
                </ul>
            </div>
            @endif

                @if ($youtubeVideo = $post->get_field('video-youtube'))
                    @php($youtubeData = getYoutubePreviewData($youtubeVideo))
                    <div class="white-box-lft white-box-tit">
                        <h2>@Lang('Conoce más al Organizador del Retiro')</h2>

                        <div class="ytb-embed">
                            <iframe src="{{ $youtubeData->embedLink }}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                @endif
            <!-- end FACILITADORES -->

            @php( $programa = $post->get_field('programa') )
            @if ( !empty( $programa->dies) )
                @php ($dies = array())
                
                @foreach((array) $programa->dies as $dia)
                    @php( $dia = array_values((array) $dia))
                    @php( usort($dia, 'ordenarXdata') )
                    @php( $dies[] = $dia)
                @endforeach

                <div class="white-box-lft white-box-tit">
                    <h2>@Lang('Programa')</h2>
                    <div class="prog-tab">
                        <div id="horizontalTab">
                            <ul class="resp-tabs-list">
                                @foreach($dies as $i => $dia)
                                    <li>@Lang('Día') {{ $i + 1 }}</li>
                                @endforeach
                            </ul>
                            <div class="resp-tabs-container">
                                @foreach($dies as $dia)
                                    <div>
                                        <ul>
                                            @foreach($dia as $activitat)
                                                @if ($activitat->hora && $activitat->valor)
                                                <li>
                                                    <div class="time">{{ $activitat->hora }}</div>
                                                    <div class="descp">{{ $activitat->valor }}</div>
                                                </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @php($beneficios = groupCustomList($post, 'beneficios', 'beneficios2'))
            @if ( !empty($beneficios) && count($beneficios) > 0)
            <div class="retro-lft single-retiro">
                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border single-retiro">
                        <div class="cnt-divider">
                            <h2>@Lang('Beneficios')</h2>
                            @foreach(array_chunk($beneficios, (round(count($beneficios) / 2))) as $costat)
                            <div class="ben-lft">
                                <ul class="benef-lst">
                                    @foreach($costat as $beneficio)
                                    <li>{{ $beneficio['item']['value'] }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endforeach
                        </div>
                        <button class="toggle-btn" data-open-txt="@lang('MENOS INFORMACIÓN')" data-closed-txt="@lang('MÁS INFORMACIÓN')">@Lang('MÁS INFORMACIÓN')</button>
                    </div>
                </div>
            </div>
            @endif
            
            @if ( $alojamiento && is_object($alojamiento) )
            @php($t_alojamiento = $alojamiento->translate())
            <div class="white-box-lft white-box-tit">
                <h2>@Lang('Alojamiento')</h2>
                <ul class="facil-lst rerr">
                    <li>
                        <div class="ret-tit-lft">
                            <div class="lft-img">
                                @php($src = $alojamiento->get_field('galeria'))
                                @if ( count($src) > 0 )
                                <a href="{{ $alojamiento->get_url() }}">
                                    <img src="{{ $src[0]->get_thumbnail_url("medium") }}" alt="">
                                </a>
                                @endif
                            </div>
                            <div class="cont-div">
                                {{-- <h2 class="cont-mob">{{ $espacio->title }}</h2> --}}
                                <p>{!! get_excerpt($alojamiento->description, 35) !!}</p>
                            </div>
                        </div>
                        <div class="ret-tit-rgt full-wmob"> <a href="{{ $alojamiento->get_url() }}" target="_blank" class="en-btn">@Lang('Ver Ficha')</a></div>
                    </li>
                </ul>

                <ul class="aloj-list">
                @foreach($habitaciones as $habitacion)
                    <li>
                        <div class="aloj-lft">
                            @if ($galeria = $habitacion->get_field('galeria'))
                                @if (($imagen = array_values($galeria)) && count($imagen) > 0)
                                    <div class="alog-img">
                                        <img src="{{ $imagen[0]->get_thumbnail_url('medium') }}" alt="" onclick="$('.fancybox{{ $habitacion->id }}').click();">
                                    </div>
                                @endif
                                <div class="alog-cont">
                                    <h3>{{ $habitacion->title }}&nbsp;&nbsp;<span><a class="fancybox{{ $habitacion->id }} show" href="#poup-txt{{ $habitacion->id }}">@lang('(más información)')</a></span></h3>
                                    @if ($prestaciones = $habitacion->get_field('prestaciones'))
                                    <ul>
                                        @foreach($prestaciones as $prestacion)
                                            @if ( !empty($prestacion->title) )
                                                <li class="green-tick">{{ $prestacion->title }}</li>
                                            @endif
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            @endif
                        </div>
                    </li>
                @endforeach
                </ul>
            </div>
            @endif
            
            @if ($alimentacionTXT = $post->get_field('alimentacion-text'))
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Alimentación')</h2>
                <p>{!! $alimentacionTXT !!}</p>
            </div>
            @endif


            @php($incluye = groupCustomList($post, 'incluye', 'incluye2'))
            @if ( !empty($incluye) && count($incluye) > 0)
            <div class="white-box-lft white-box-tit">
                <h2>@Lang('El retiro incluye')</h2>
                @foreach(array_chunk($incluye, (round(count($incluye) / 2))) as $costat)
                <div class="ben-lft">
                    <ul class="benef-lst">
                        @foreach($costat as $incl)
                        <li>{{ $incl['item']['value'] }}</li>
                        @endforeach
                    </ul>
                </div>
                @endforeach
            </div>
            @endif
            
            @if ($normativa = $post->get_field('normativa-y-politica-de-cancelacion'))
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Normativa y politica de cancelacion')</h2>
                <p>{!! $normativa !!}</p>
            </div>
            @endif
            
            @if ( $ubicacion )
                @php($coordenades = explode(',', $ubicacion->coordenades))
                @if (count($coordenades) > 1)
                    <div class="white-box-lft white-box-tit white-box-para">
                        <h2>@Lang('Ubicación')</h2>
                        <div id="gmaps" class="box-img"></div>
                    </div>
                @endif
            @endif

            @if ($infoAdicional = $post->get_field('informacion-adicional'))
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Información adicional')</h2>
                <p>{!! $infoAdicional !!}</p>
            </div>
            @endif

            @if ($transporte = $post->get_field('transporte-publico'))
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Transporte Público')</h2>
                <p>{!! $transporte !!}</p>
            </div>
            @endif

            <!-- RATINGS / COMMENTS -->
            @includeIf('Front::partials.reviews')
        </div>
        {{--
        <div class="aun-car-mbl mob-ree">
            <ul>
                <li>
                    <div class="obr-pad"> <a href="javascript:void(0);">
                            <div class="obr-img"> <img src="{{ asset($_front.'images/img-1.jpg') }}" alt=""> </div>
                            <div class="obr-cnt">
                                <h1>Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</h1>
                                <ul>
                                    <li class="date">Escoge tu fecha</li>
                                    <li class="loc">Limon, Cahuita, Costa Rice</li>
                                </ul>
                            </div>
                        </a>
                        <div class="price"><a href="javascript:void(0);">
                            </a>
                            <div class="p-lft"><a href="javascript:void(0);"> </a><a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-empty.svg') }}" alt=""></a> <span>4/5</span> </div>
                            <div class="p-rgt">
                                <h2>1350 <i class="fa fa-eur" aria-hidden="true"></i></h2>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="obr-pad"> <a href="javascript:void(0);">
                            <div class="obr-img"> <img src="{{ asset($_front.'images/img-2.jpg') }}" alt=""> </div>
                            <div class="obr-cnt">
                                <h1>Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</h1>
                                <ul>
                                    <li class="date">Escoge tu fecha</li>
                                    <li class="loc">Limon, Cahuita, Costa Rice</li>
                                </ul>
                            </div>
                        </a>
                        <div class="price"><a href="javascript:void(0);">
                            </a>
                            <div class="p-lft"><a href="javascript:void(0);"> </a><a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-empty.svg') }}" alt=""></a> <span>4/5</span> </div>
                            <div class="p-rgt">
                                <h2>1350 <i class="fa fa-eur" aria-hidden="true"></i></h2>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="obr-pad"> <a href="javascript:void(0);">
                            <div class="obr-img"> <img src="{{ asset($_front.'images/img-3.jpg') }}" alt=""> </div>
                            <div class="obr-cnt">
                                <h1>Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</h1>
                                <ul>
                                    <li class="date">Escoge tu fecha</li>
                                    <li class="loc">Limon, Cahuita, Costa Rice</li>
                                </ul>
                            </div>
                        </a>
                        <div class="price"><a href="javascript:void(0);">
                            </a>
                            <div class="p-lft"><a href="javascript:void(0);"> </a><a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-empty.svg') }}" alt=""></a> <span>4/5</span> </div>
                            <div class="p-rgt">
                                <h2>1350 <i class="fa fa-eur" aria-hidden="true"></i></h2>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="obr-pad"> <a href="javascript:void(0);">
                            <div class="obr-img"> <img src="{{ asset($_front.'images/img-3.jpg') }}" alt=""> </div>
                            <div class="obr-cnt">
                                <h1>Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</h1>

                                <ul>
                                    <li class="date">Escoge tu fecha</li>
                                    <li class="loc">Limon, Cahuita, Costa Rice</li>
                                </ul>
                            </div>
                        </a>
                        <div class="price"><a href="javascript:void(0);">
                            </a>
                            <div class="p-lft"><a href="javascript:void(0);"> </a><a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a> <a href="javascript:void(0);"><img
                                        src="{{ asset($_front.'images/heart-empty.svg') }}" alt=""></a> <span>4/5</span> </div>
                            <div class="p-rgt">
                                <h2>1350 <i class="fa fa-eur" aria-hidden="true"></i></h2>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        --}}

        <!-- right side -->
        <div class="retro-rgt">
            @if ($ofertaBarata)
            <form id="retiro-reserva-form" action="{{ route('retiros.detalles', $post->post_name) }}" method="POST">
                @if ( !empty($calendari->calendari[0]) )
                    <input type="hidden" name="dia" value="{{ \Carbon\Carbon::parse($calendari->calendari[0])->format('d/m/Y') . '@' . \Carbon\Carbon::parse($calendari->calendari[0])->addDays($calendari->duracion - 1)->format('d/m/Y') }}" />
                @else
                    <input type="hidden" name="dia" value="" />
                @endif

                @csrf
                <input type="hidden" name="retiro" value="{{ $post->id }}">
                <input type="hidden" name="user_id_filter" value="{{ $author->id }}">
                <input type="hidden" name="pay_days_before" value="{{ $payDaysBefore }}">
                <input type="hidden" name="cancel_policy" value="{{ $politica }}">
                <input type="hidden" name="cancel_days_before" value="{{ $cancelDaysBefore }}">

                <div class="white-box-rgt">
                    <div id="cheapest-offer" class="price-no">
                        @if ( $ofertaBarata )
                            <h4 style="display: inline-block;">@Lang('Desde') {{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', obtenirPreuAmbDescompteOferta($ofertaBarata)) }}</h4>
                        @endif
                        <h5>{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $minDeposito)}}&nbsp;@Lang('de depósito')</h5>
                        <input type="hidden" id="form_deposito" name="deposito" value="{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $minDeposito, false) }}">
                        @if ($ofertaBarata['descuento']['value'] != '')
                            @php($data = \Carbon\Carbon::createFromFormat('d/m/Y', "31/12/2099"))
                            @if ($ofertaBarata['fecha-limite']['value'] != "")
                                @php($data = convertDate($ofertaBarata['fecha-limite']['value']))   
                            @endif
                            
                            @if ($data && $data->gte(new \Carbon\Carbon()))
                                <div class="dis-txt">{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $ofertaBarata['descuento']['value']) }} dto. reservando antes del {{ $data->format('d M. Y') }}</div>
                            @endif
                        @endif
                    </div>
                    <div class="dat-select cal">
                        @php($diesRetiro = obtenirProximesDates($post))

                        @if ($diesRetiro)
                            @php($diesRetiro = $diesRetiro->values())
                            @if ($diesRetiro && $diesRetiro->count() > 0)
                                @if ( !empty($calendari) && !empty($calendari->tipus) && $calendari->tipus == 1)
                                    <h5>@Lang('Fecha Retiro:')</h5>
                                @else
                                    <h5>@Lang('Selecciona una fecha:')</h5>
                                @endif
                            @endif
                        @endif

                        @if ( !empty($calendari) && !empty($calendari->calendari) )
                            @php($events = collect($calendari->calendari)->chunk($calendari->duracion))
                            @php($events = $events->filter(function($item, $key) {
                                return Date::parse($item->first()) >= Date::now();
                            })->values())
                            
                            
                            @php($count = $events->count())
                            @if ($calendari->tipus == 1 || ($calendari->tipus == 2 && $count < 6)) {{-- Evento unico --}}
                                @foreach($diesRetiro as $key => $dret)
                                    @php($primerDia = $dret->first())
                                    @php($ultimDia = $dret->last())

                                    @if (!isset($realFirstDay) && $primerDia)
                                        @php($realFirstDay = $primerDia)
                                    @endif

                                    @if($primerDia && $ultimDia)
                                        @php($date = $primerDia->format('d') . '/' . $primerDia->format('m') . '/' . $primerDia->format('Y') . '@' . $ultimDia->format('d') . '/' . $ultimDia->format('m') . '/' . $ultimDia->format('Y'))
                                        <div class="unic {{ ($key == 0) ? 'selected' : '' }}" data-date="{{ $date }}">
                                            @if ($key == 0)
                                                <input id="dia" type="hidden" name="dia" value="{{ $date }}" />
                                            @endif
                                            <div class="block inici">
                                                <span class="mes">{{ ucfirst($primerDia->format('F')) }} {{ $primerDia->format('Y') }}</span>
                                                <div class="dia">
                                                    <span class="nom-dia">{{ ucfirst($primerDia->format('l')) }}</span>
                                                    <span class="num-dia">{{ $primerDia->format('d') }}</span>
                                                </div>
                                            </div>
                                            <h5>al</h5>
                                            <div class="block final">
                                                <span class="mes">{{ ucfirst($ultimDia->format('F')) }} {{ $ultimDia->format('Y') }}</span>
                                                <div class="dia">
                                                    <span class="nom-dia">{{ ucfirst($ultimDia->format('l')) }}</span>
                                                    <span class="num-dia">{{ $ultimDia->format('d') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @elseif (($calendari->tipus == 2 && $count >= 6) || $calendari->tipus == 3 || $calendari->tipus == 4)
                                @if (!isset($realFirstDay))
                                    @php($realFirstDay = Date::parse($calendari->calendari[0]))
                                @endif
                                <div class="calendario"></div>
                            @endif
                        @endif
                    </div>
                    <div class="dat-select edit-person">
                        <h5>@Lang('Seleccionar oferta')</h5>
                        <ul>
                            @if ($ofertes && count($ofertes))
                                @php($barata = null)
                                @foreach($ofertes as $key => $oferta)
                                    @if( is_object($oferta['habitacion']['value']) && $habitacion = $oferta['habitacion']['value'] )
                                        @php( $precio = obtenirPreuAmbDescompteOferta($oferta))
                                        @if ( !is_numeric($precio) || $precio == 0) @continue @endif
                                        @if ($oferta['persones']['value'] == null) @continue @endif

                                        @if ( is_null($barata) || obtenirPreuAmbDescompteOferta($ofertaBarata) == $precio)
                                            @php($barata = $precio)
                                        @endif

                                        <li class="offer bck {{ ($barata == $precio) ? 'bck-orange' : '' }}">
                                            <div class="select-name">
                                                <input type="radio" name="offer" {{ ($barata == $precio) ? 'checked' : '' }} value="{{ $key }}">
                                                <input type="hidden" name="habitacion[{{ $key }}]" value="{{ $habitacion->id }}">
                                                <input type="hidden" name="precio[{{ $key }}]" value="{{ $precio }}">
                                            </div>
                                            <div class="edit-pers">
                                                @if ($oferta['persones']['value'] > 1)
                                                    <h2>{{ $oferta['persones']['value'] }} @Lang('personas')</h2>
                                                @else
                                                    <h2>{{ $oferta['persones']['value'] }} @Lang('persona')</h2>
                                                @endif

                                                <input type="hidden" name="personas[{{ $key }}]" value="{{ $oferta['persones']['value'] }}">

                                                <span>{{ $habitacion->title }}</span>
                                                <small>{{ $habitacion->get_field('subtitulo') }}</small>
                                                <div class="">
                                                    <a class="mas-info fancybox{{ $habitacion->id }} show" href="#poup-txt{{ $habitacion->id }}">@Lang('Más información')</a>
                                                </div>
                                            </div>
                                            <div class="edit-price" data-price="{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) }}" data-deposit="{{ (is_numeric($deposito)) ? toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio * ($deposito / 100), false) : 0 }}" >
                                                @if ($oferta['precio']['value'] > $precio)
                                                    <span class="full-price">{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $oferta['precio']['value']) }}</span>
                                                @endif
                                                {{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio) }}
                                                <small>@Lang('Precio total')</small>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </div>

                    @if (Auth::check())
                        @php($user = Auth::user())
                        @php($userDharmasMoney = $user->dharmas / 100)
                        <div class="dat-select dharmas">
                            {{-- @if ($user->dharmas > 0) --}}
                                <div class="info bck bck-orange">
                                    <h5>@Lang('Dto. Dharmas') {{ round($user->dharmas, 2) }} <img src="{{ asset($_front.'images/Dharmas-icon.svg') }}"></h5>
                                        <span>@if ($user->dharmas > 0)@endif {{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', -$userDharmasMoney) }}</span>
                                </div>

                                <div class="discount">
                                    <input id="dharmas" type="hidden" name="dharmas" value="{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $userDharmasMoney, false) }}" />
                                    <input id="chkdharm" type="checkbox" name="dharmas" value="0">
                                    <label for="chkdharm">@Lang('No descontar mis Dharmas ahora')</label>
                                </div>
                            {{-- @endif --}}
                        </div>
                    @endif

                    @if ($ofertes && count($ofertes))
                        @php( $appliedDeposito = $minDeposito )
                        @php( $total = (is_numeric($barata)) ? $barata : 0 )
                        @php( $total_deposito = $total - $appliedDeposito )

                        @if (Auth::check())
                            @php( $appliedDeposito = $minDeposito - $userDharmasMoney)
                            @php( $total = (is_numeric($barata)) ? ($barata - $userDharmasMoney) : 0 )
                            @php( $total_deposito = $total - $appliedDeposito )
                        @endif

                        @php($moreDharmas = isset($moneda) && $moneda ? toEur($moneda->get_field('iso-code'), $total) : $total)

                        <div class="dat-select total">
                            <div class="resu-txt">
                                <h5>@Lang('Total Retiro')</h5>
                            </div>
                            @if ($total != 0)
                            <div class="resu-price">
                                {{ $currencySymbol }} <span id="precio_total" data-total="{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', round($total, 2), false) }}">{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', round($total, 2), false) }}</span>
                            </div>
                            @endif
                        </div>
                        <div id="btn-reserva" class="dat-select disclaimer">
                            <ul>
                                <li>@Lang('Depósito para reservar:') <strong>{{ $currencySymbol }} <span id="total_deposito">{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $appliedDeposito, false) }}</span></strong></li>
                                <li>
                                    <strong>@Lang('Paga los') {{ $currencySymbol }} <span id="total_resumen">{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $total_deposito, false) }}</span></strong>@if(isset($payDaysBefore))
                                        @if (isset($realFirstDay))
                                            @lang('restantes directamente al organizador el ')<span class="str-pay-date">{{ \Jenssegers\Date\Date::parse($realFirstDay)->subDays($payDaysBefore)->format('j \\d\\e F \\d\\e Y') }}</span>
                                        @else
                                            @if (!$payDaysBefore)
                                                @lang('restantes directamente al organizador a la llegada')
                                            @else
                                                {{ $payDaysBefore }}@lang(' días antes del retiro')
                                            @endif
                                        @endif
                                    @endif</li>
                                <li>
                                    @if(isset($cancelDaysBefore))
                                        @if (isset($realFirstDay))
                                            @if ($politica == 'estricta')
                                                @lang('Canjeable por otras fechas hasta el ')<span class="str-cancel-date">{{ \Jenssegers\Date\Date::parse($realFirstDay)->subDays($cancelDaysBefore)->format('d/m/Y') }}</span>
                                            @else
                                                <h5 class="cancel-policy"><strong>@lang('Cancelación gratuita hasta el ')<span class="str-cancel-date">{{ \Jenssegers\Date\Date::parse($realFirstDay)->subDays($cancelDaysBefore)->format('d/m/Y') }}</span></strong></h5>
                                            @endif
                                        @else
                                            @if ($politica == 'estricta')
                                                @lang('Canjeable por otras fechas hasta '){{ $cancelDaysBefore }}@lang(' días antes del retiro')
                                            @else
                                                <h5 class="cancel-policy"><strong>@lang('Cancelación gratuita hasta '){{ $cancelDaysBefore }}@lang(' días antes del retiro')</strong></h5>
                                            @endif
                                        @endif
                                    @endif
                                </li>
                            </ul>

                            @if ($ofertes && count($ofertes) && !empty($calendari) && !empty($calendari->calendari) && $minDeposito)
                                @if($tipusreserva["immediata"]!== "0")
                                    <input type="submit" class="submit submit-green" value="@lang('Reserva')">
                                @else
                                    <input type="submit" class="submit submit-green" value="@lang('Petición de reserva')">
                                @endif
                            @endif
                        </div>
                    @endif
                </div>
            </form>
            @endif
            <div class="dat-select promos">
                <ul>
                    <li>@Lang('Mejor precio garantizado')</li>
                    <li>@Lang('Sin gastos ni comisión')</li>
                    <li>@Lang('Confianza de Encuentraturetiro')</li>
                </ul>
                @if (isset($total))
                    <div class="promo">
                        <span><img src="{{ asset($_front.'images/Dharmas-icon.svg') }}"></span><p>@lang('Suma')&nbsp;<strong><a href="{{ get_page_url('dharmas-1') }}"><span class="get-more-dharmas">{{ round($moreDharmas) }}</span>&nbsp;Dharmas</a></strong>&nbsp;@lang('para tu próxima reserva')</p>
                    </div>
                @endif
                <div class="promo">
                    <span><img src="{{ asset($_front.'images/Imagen_22.png') }}"></span> Plantamos&nbsp;<strong>1 árbol</strong>&nbsp;por ti
                </div>
                @if ($author)
                <div class="duda-org">
                    <div class="organizador">
                        <img src="@if ($author->get_field('foto-organizador')){{ $author->get_field('foto-organizador')->get_thumbnail_url("medium") }}@else{{ $author->get_avatar_url() }}@endif" alt="">
                        {{-- <h2>{{ $author->name }}</h2> --}}
                        <h5 style="margin-bottom: 10px;">@Lang('¿Tienes alguna duda?')</h5>
                    </div>
                    {{-- <h3>@Lang('¿Tienes alguna duda?')</h3> --}}
                    <form id="FormEnviarMensaje" class="messaging" style="position: relative;">
                        <div class="lds-ripple"><div></div><div></div></div>
                        <input type="hidden" name="params[Retiro]" value="{{ $trans->title }}">
                        <input type="hidden" name="params[retiro_url]" value="{{ $post->get_url() }}">
                        <input type="hidden" name="params[Organizador]" value="{{ $author->fullname() }}">
                        <input type="hidden" name="no_mensaje_allowed" value=true>
                        <input type="hidden" name="type" value="pregunta_organizador">
                        <input type="hidden" name="to" value="{{ $peticionesUser ? $peticionesUser->id : $author->id }}">

                        @if ( !Auth::check())
                            <input type="text" name="name" placeholder="@Lang('Nombre')" required>
                            <input type="email" name="email" placeholder="@Lang('Email')" required>
                            <input type="text" name="params[Telefono]" placeholder="@lang('Teléfono')" required>
                        @else
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                            <input type="hidden" name="name" value="{{ auth()->user()->fullname() }}">
                            <input type="hidden" name="email" value="{{ auth()->user()->email }}">
                            <input type="hidden" name="params[Telefono]" value="{{ auth()->user()->get_field('phone-number') }}">
                        @endif
                        <div class="message-sent" style="padding: 10px;margin: 10px;display:none;">
                            {{ __(':name sent successfully', ['name' => __('Message')]) }}
                        </div>
                        <textarea name="params[Mensaje]" rows="3" placeholder="@Lang('Escríbenos...')" required></textarea>
                        <div class="login-form single-form">
                            <input id="policy" type="checkbox" name="policy" value="true" required="">
                            <label for="policy">
                                <span><span></span></span>
                                <div>@Lang('He leído y acepto la <a href=":link" target="_blank">Política de Privacidad</a>', [ 'link' => get_page_url('politica-de-privacidad') ])</div>
                            </label>
                        </div>
                        <button type="submit" form="FormEnviarMensaje" class="anim">@Lang('Enviar Pregunta')</button>
                    </form>
                </div>
                @endif
                @includeIf('Front::partials.carousel-equipo')
            </div>
        </div>
    </div>
    @if ($tipos = $item->get_field('tipos-de-retiro'))
        @php($metas = $tipos->map(function($item) { return ['tipos-de-retiro', 'like', '%"'.$item->id.'"%']; })->all())
        @php($relatedParams = [
            'post_type' => 'retiro',
                'where' => [
                    ['posts.id', '<>', $item->id],
                ]])
        @if (!empty($location->code_region))
            @php($relatedParams = array_merge($relatedParams, [
                'location' => [$location->code_region]
            ]))
        @else
            @php($relatedParams = array_merge($relatedParams, [
                'metas' => $metas,
                'metas_or' => ['tipos-de-retiro']
            ]))
        @endif
        @php($related = loadRetiros($relatedParams, false)->getCollection())
        @php($related = $related->filter(function ($retiro) use ($item) {
            return $retiro->id != $item->id;
        }))
        @php($related = filterRetirosByDates($related))
        @if ($related && count($related))
        <div class="row">
            <div class="related-retiros">
                <h3>@lang('Retiros Relacionados')</h3>
                <div class="owl-carousel obres" id="aun-car1">
                    @foreach($related as $retiro)
                        <div class="item">
                            {!! renderRetiroHome($retiro) !!}
                        </div>
                    @endforeach
                </div>
                <div class="aun-car-mbl interes">
                    <ul>
                        @foreach($related as $retiro)
                            <li>
                                {!! renderRetiroHome($retiro, true) !!}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        @endif
    @endif
</section>


@foreach($habitaciones as $key => $habitacion)
    @php($gallery = $habitacion->get_field('galeria'))
    @php($prestaciones = $habitacion->get_field('prestaciones'))
    <div id="poup-txt{{ $habitacion->id }}" class="popup habitaciones custom_scrollbar">
        <div class="pro-lft-pad">
            @if ($gallery)
            <div id="flexslider{{ $habitacion->id }}" class="flexslider">
                <ul class="slides">
                    @foreach($gallery as $img)
                        @if ($img->get_thumbnail_url())
                            <li><img src="{{ $img->get_thumbnail_url('medium_large') }}" alt=""></li>
                        @endif
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="pro-rgt-pad">
            <div class="prod-txt">
                <div class="title">
                @if ( $alojamiento && is_object($alojamiento) )
                    <h3>{{ $alojamiento->title }}</h3>
                    <h2>{{ $habitacion->title }}</h2>
                    <p class="est">{{ $habitacion->get_field('subtitulo') }}</p>
                    <ul class="carac-list">
                        @if ( $habitacion->get_field('plazas') )
                            @if ( $habitacion->get_field('plazas') == 1)
                                <li><i class="icon publico"></i> {{ $habitacion->get_field('plazas') }} @Lang('plaza')</li>
                            @else
                                <li><i class="icon publico"></i> {{ $habitacion->get_field('plazas') }} @Lang('plazas')</li>
                            @endif
                        @endif
                    </ul>
                @endif
                </div>

                @if (is_array($prestaciones) || $prestaciones instanceof Illuminate\Database\Eloquent\Collection)
                <div class="servicios">
                    <h2>@Lang('Prestaciones')</h2>
                    @php($chunks = array_chunk($prestaciones->toArray(), 2))
					@foreach($prestaciones->chunk(2) as $key => $chunk)
					<ul {{ ($key == 1) ? 'class="li-pad"' : ''}}>
						@foreach($chunk as $prestacion)
						<li>{{ $prestacion->title }}</li>
						@endforeach
					</ul>
					@endforeach
                </div>
                @endif
                <p class="padd-btm">{!! $habitacion->get_field('descripcion') !!}</p>
            </div>
        </div>
    </div>
@endforeach
@endsection


@section('scripts1')
<script src="{{ asset($_front.'js/moment-with-locales.js') }}"></script>
@if( $ubicacion )
    
    @php($coordenades = explode(',', $ubicacion->coordenades))

    @if (count($coordenades) > 1)
        @php($lat = $coordenades[0])
        @php($lng = $coordenades[1])

        <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ !empty($_config['google_maps']['value']) ? $_config['google_maps']['value'] : '' }}&callback=initMap"></script>
        <script>
        function initMap() {
            var myLatlng = new google.maps.LatLng({{ $lat }},{{ $lng }});
            var mapOptions = {
                zoom: 15,
                center: myLatlng,
                scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
                disableDoubleClickZoom: true,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    mapTypeIds: ['roadmap', 'terrain']
                }
            };
            map = new google.maps.Map(document.getElementById("gmaps"), mapOptions);

            marker = new google.maps.Marker({
                position: myLatlng,
                map: map
            });
            marker.setMap(map);
        }
        </script>
	@endif
@endif

@if ( $ofertaBarata && !empty($calendari) && !empty($calendari->calendari) )
    @php($aux = $events->map(function($item, $key) {
        return $item->filter(function($value, $key) {
            return Date::parse($value) >= Date::parse(date('Y-m-d'));
        });
    }))

    @php($events = $aux->filter(function($value, $key) {
        return Date::parse($value->first()) >= Date::now();
    })->values())

    @if($events->count() > 0 && ($primer = $events->first()))
    <script>
        var duracion = {{ $calendari->duracion }};
        var dateFirst = new Date('{{ array_first($primer) }}');
        var dia_inici = moment(dateFirst);
        var dia_fi = dia_inici.clone().add(duracion - 1, "days");
    </script>
    @endif

    <script>
        moment.locale('{{ app()->getLocale() }}');
        @if ($calendari->tipus == "2")
            @php($cal = array_chunk($calendari->calendari, $calendari->duracion))
            @foreach($cal as $key => $c)
                @php($cal[$key] = $c[0])
            @endforeach
        var dies = @json($cal);    
        @else
        var dies = @json($calendari->calendari);
        @endif
        var pay_days_before = {{ $payDaysBefore }};
        var cancel_days_before = {{ $cancelDaysBefore }};

        $('.facil-lst.retiro .btn-org').click(function() {
            $('.facil-lst.retiro .slide').slideToggle();

            if ($(this).hasClass('.more')) {
                $('.facil-lst.retiro .btn-org.more').toggle();
                $('.facil-lst.retiro .btn-org.less').toggle();
            } else {
                $('.facil-lst.retiro .btn-org.more').toggle();
                $('.facil-lst.retiro .btn-org.less').toggle();
            }
        });

        $(document).on('click', '.duracion .search-accordion li', function(e) {
            e.preventDefault();
        });

        /* Modal */
        $(document).ready(function() {

            $.ajax({
                url: ajaxURL,
                method: 'POST',
                data: {
                    action: 'addRetiroVisitCookie',
                    parameters: {
                        value: '{{ $item->id }}'
                    }
                },
                success: function(data) {},
                error: function(data) {
                    console.log(data);
                }
            });

            @foreach($habitaciones as $key => $habitacion)
            $(".fancybox{{ $habitacion->id }}").fancybox({
                /*scrolling: 'yes',*/
                afterLoad: function() {
                    $('#flexslider{{ $habitacion->id }}').flexslider({
                        animation:"slide",
                        slideshowSpeed:10000,
                        animationDuration: 5000,
                        easing: "swing",
                        directionNav: true,
                        controlNav: false,
                        pausePlay: false,
                        autoPlay:true,
                        start: function(slider) { // Canviar EVENT per On Show
                            setTimeout(function() {
                                $('#flexslider{{ $habitacion->id }}').resize();
                            }, 200);

                            $('body').removeClass('loading');
                            if($(window).width() < 768) {
                                $('.flex-prev').trigger( "click" );
                            }
                        }
                    });
                }
            });
            @endforeach
        });
        /* fi modal */
    </script>
@endif

<script>
    var lang = '{{ $language_code }}';
    var deposito = {{ $minDeposito }};
    var toEuroRate = {{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', 1, false) }};
</script>
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script src="{{ asset($_front.'js/easyResponsiveTabs.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.fancybox.js') }}"></script>
<script src="{{ asset($_front.'js/bootstrap-datepicker.min.js') }}"></script>

<script src="{{ asset($_front.'js/bootstrap-datepicker.'.$language_code.'.js') }}"></script>
<script src="{{ asset($_front.'js/pages/fitxa-retiros.js') }}"></script>

<!-- ENVIAR PREGUNTA -->
<script>
$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});
$('#FormEnviarMensaje').on('submit', function(e) {
    var form_ele = $(this).addClass('loading');
    form_ele.addClass('loading');
    var form = $(this).serialize();
    $.ajax({
        url: ajaxURL,
        method: "POST",
        data: {
            action: "enviar_mensaje",
            method: "no_json",
            parameters: form
        },
        success: function(data) {
            form_ele.removeClass('loading');
            form_ele.find('textarea').val('');
            form_ele.find('.message-sent').show();
        }
    });
    e.preventDefault();
    return false;
});
</script>
<!-- end ENVIAR PREGUNTA -->

<!-- DESCRIPTION VER MÁS -->
<script>
$(document).ready(function(){
    $('.zopim').remove();
    @if ($ofertaBarata)
    var observer = new IntersectionObserver(function(entries, observer) { 
        if (window.innerWidth < 639) {
            for (entry of entries) {
                if (entry.isIntersecting) {
                    $('.reserva-mbl').fadeOut();
                } else {
                    if (entry.boundingClientRect.y > 0) {
                        $('.reserva-mbl').fadeIn();
                    }
                }
            }
        } else {
            $('.reserva-mbl').hide();
        }
    });
    
    observer.observe(document.querySelector('#btn-reserva'));
    @endif

    var desvm_total_h = $('.descripcion-vermas').prop('scrollHeight');
    var desvm_h = $('.descripcion-vermas').height();
    if ( desvm_total_h > desvm_h ) {
        $('.description .btn-org.more').show();
        $('.description .btn-org.more').css('display', 'block');
    }
});
$(document).on('click', '.description .btn-org.more', function(){
    $('.descripcion-vermas').css('max-height', '2000px');
    $(this).hide();
});

var owl = $('#aun-car1');
owl.owlCarousel({
    goToFirstSpeed :1000,
    loop:true,
    items:3,
    margin:15,
    autoplay:true,
    autoplayTimeout:5500,
    autoplayHoverPause:true,		
    nav : true,
    dots : false,
    responsive: {
        0:{ items:1, margin:0,},
        480:{ items:1},
        640:{ items:2},
        768:{ items:3},
        1024:{ items:3}
    },
});
</script>
<!-- end DESCRIPTION VER MÁS -->

@endsection


@section('analytics_ec')
    gtag('event', 'view_item', {
        "items": [
            {
                "id": "{{ $post->id }}",
                "name": "{{ $trans->title }}",
                "category": "Retiro",
                "price": "{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', obtenirPreuAmbDescompteOferta($ofertaBarata), false) }}"
            }
        ]
    });
@endsection