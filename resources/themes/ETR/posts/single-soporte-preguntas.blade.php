@php($gdp = $item->get_field('grupo-preguntas'))
@if ($gdp)
<script>
    window.location.href = '{{ $gdp->get_url() }}'; //using a named route
</script>
@else
<script>
    window.location.href = '{{ \App\Post::get_url_by_post_id(412) }}'; //using a named route
</script>
@endif