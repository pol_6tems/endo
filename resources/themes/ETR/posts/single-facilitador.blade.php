@extends('Front::layouts.base')

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
<link rel="stylesheet" href="{{ asset($_front.'css/style2.css') }}" />
@endsection

@section('styles')
    <link type="text/css" rel="stylesheet" href="{{ asset($_front.'css/org-easy-responsive-tabs.css') }}" />
@endsection

@section('contingut')
@php
    $retiros = get_posts([
        'post_type' => 'retiro',
        'metas' => [
            ['facilitador', $item->id]
        ]
    ]);

    /*$retiros1 = get_posts([
        'post_type' => 'retiro',
        'where' => [
            ['author_id', $item->id]
        ]
    ]);

    $retiros = $retiros->merge($retiros1);*/

    $posts = get_posts([
        'post_type' => 'blog-post',
        'metas' => [
            ['facilitador', $item->id]
        ]
    ]);

    /*$posts1 = get_posts([
        'post_type' => 'blog-post',
        'where' => [
            ['author_id', $item->author_id]
        ]
    ]);

    $posts = $posts->merge($posts1);*/

    $galeria = $item->get_field('galeria');
@endphp
@includeIf('Front::partials.header')
<section class="retiros-sec centros-sec">
    <div class="row1">
        <div class="centro-lft facilitador">
            <div class="flexslider facilitadores centro">
                @if ( $trans->get_thumbnail_url() || $galeria)
                <ul class="slides">
                    @if ($trans->get_thumbnail_url())
                        <li data-thumb="{{ $trans->get_thumbnail_url() }}">
                            <img src="{{ $trans->get_thumbnail_url() }}" />
                        </li>
                    @endif
                </ul>
                @endif
            </div>
            @php($frase = $item->get_field('frase'))
            @if ($frase != '')
            <div class="escrib">
                {{ $frase }}
            </div>
            @endif
        </div>
        <div class="centro-rht ret-tit-lft" style="padding-right: 0px;">
            {{-- <h1>{{ $item->author->name }}</h1> --}}
            @php($name = explode(" ", $item->title)[0])
            <h1>{{ $name }}</h1>
            @if ($categorias = $item->get_field('habilidades'))
            <ul class="tags">
                @foreach($categorias as $categoria)
                <li><a href="javascript:void(0);">{{ $categoria->title }}</a></li>
                @endforeach
            </ul>
            @endif
            <ul class="date-icon">
                @if ($location = $item->get_field('localizacion'))
                <li class="loc">
                    @if ($location->object)
                        @php($location = json_decode($location->object))
                        @php($address_components = $location->address_components)
                        @php(array_pop($address_components))
                
                        @foreach($address_components as $key => $component)
                            @if ($component->types[0] != 'route')
                                {{ $component->long_name }}{{ ($key < count($address_components) - 1) ? ', ' : '' }}
                            @endif
                        @endforeach
                    @endif
                </li>
                @endif
            </ul>
            <div class="divider">
                <div class="description cnt-divider">
                    {!! $item->description !!}
                </div>

                <button class="toggle-btn" data-open-txt="@lang('LEER MENOS')" data-closed-txt="@lang('LEER MÁS')">@lang('LEER MÁS')</button>
            </div>
        </div>
    </div>       
</section>

@if ($retiros->count() || $posts->count())
<section class="retiros-sec centro-p">
    <div class="row1">
        <div class="retiros-top tabs">
            <ul class="resp-tabs-list links">
                @if ($retiros->count() > 0)<li><a href="javascript:void(0);">@Lang('Retiros')</a></li>@endif
                @if ($posts->count() > 0)<li><a href="javascript:void(0);">@Lang('Artículos')</a></li>@endif
            </ul>
            <div class="resp-tabs-container obres retrios-lis">
                @if ($retiros->count() > 0)
                <div>
                    <ul class="list">
                        @foreach($retiros as $retiro)
                        @includeIf('Front::partials.single-retiro', ['retiros' => $retiros])
                        @endforeach
                    </ul>
                </div>
                @endif
                @if ($posts->count() > 0)
                <div>
                    <ul class="list">
                        @foreach($posts as $post)
                        <li class="items">
                            <div id="{{ $post->id }}" class="obr-pad">
                                <a href="{{ $post->get_url() }}">
                                    <div class="obr-img">
                                        <img src="{{ $post->media()->get_thumbnail_url('medium') }}" alt="">
                                        @if ($categoria = $post->get_field('categoria'))
                                            <span class="exp-btn">{{ $categoria->title }}</span>
                                        @endif
                                    </div>
                                    <div class="obr-cnt">
                                        <h1>{{ $post->title }}</h1>
                                        <ul class="date-icon"> 
                                            @php ($publishDate = $post->get_field('fecha-publicacion') ? \Carbon\Carbon::createFromFormat('d/m/Y', $post->get_field('fecha-publicacion')) : $post->created_at)
                                            <li class="date">{{ ucfirst(\Jenssegers\Date\Date::parse($publishDate)->format('l j \\d\\e F Y')) }}</li>
                                        </ul>
                                        {{ strip_tags(remove_style_tags(html_entity_decode(get_excerpt($post->description, 20)))) }}
                                    </div>
                                    <div class="price">
                                        <div class="p-lft">
                                            <div class="rating">
                                                <!-- VALORACIONES -->
                                                @php ( do_action('show_ratings_avg', $post->id) )
                                            </div>
                                        </div>
                                        <div class="p-rgt">

                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endif
@endsection

@section('scripts1')
<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.showmore.js') }}"></script>
<script src="{{ asset($_front.'js/easyResponsiveTabs.js') }}"></script>
<script src="{{ asset($_front.'js/smk-accordion.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.mb.slider.js') }}"></script>
<script src="{{ asset($_front.'js/pages/organizadores.js') }}"></script>
<script>
$('.tabs').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion           
    width: 'auto', //auto or any width like 600px
    fit: true,   // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    activate: function(event) { // Callback function if tab is switched
        var $tab = $(this);
        var $info = $('#tabInfo');
        var $name = $('span', $info);

        $name.text($tab.text());

        $info.show();
    }
});
</script>
@endsection