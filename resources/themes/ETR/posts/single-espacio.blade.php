@extends('Front::layouts.base')

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style1.css') }}" />
@endsection

@section('styles')
<link href="{{ asset($_front.'css/datepicker.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('contingut')
@php
    $t_espacio = $item->translate();
    $gmaps = $item->get_field('ubicacion');
    $num_salas = $item->get_field('num-salas');
    $prestaciones = $item->get_field('prestaciones');
    $plazas = $item->get_field('plazas');

    $galeria = $item->get_field('galeria');
    $fechas = $item->get_field('fechas');
@endphp
<!-- Header Corresponent -->
@includeIf('Front::partials.header')
<!-- banner starts -->
<section class="banner-load">
    @includeIf('Front::partials.single-gallery', ['gallery' => $galeria])
</section>
<!-- TODO: MOBILE -->
<div id="flexslider2" class="flexslider mob-slider">
    <ul class="slides">
        @if ( !empty($galeria) )
            @foreach($galeria as $imatge)
                @if ($imatge)
                <li><img group="fancybox" src="{{ $imatge->get_thumbnail_url("medium") }}" alt="">
                    <div class="flex-ccn">
                        {{-- <h2>Lorem ipsum</h2> --}}
                    </div>
                </li>
                @endif
            @endforeach
        @endif
    </ul>
</div>
<!-- TODO: MOBILE -->
<!-- banner ends -->
<section class="retiros-sec espacios">
    <div class="row">
        <div class="retro-lft">
            <div class="white-box-lft">
                <!--
            <div class="ret-tit-rgt"> <img src="images/centro-mandala.png" alt="">
                <h3>Centro <br>
                  Mandala</h3>
              </div>
              -->
                <div class="ret-tit-lft description">
                    <h1>{{ $t_espacio->title }}</h1>
                    <ul class="date-icon">
                        @if ( $gmaps && $gmaps->adreca )
                        <li class="loc">{{ $gmaps->adreca }}</li>
                        @endif
                    </ul>
                    
                    <div class="descripcion-vermas">
                        <p>{!! $t_espacio->description !!}</p>
                    </div>

                    <button class="btn-org more">@Lang('Ver más')</button>

                </div>

            </div>
            <div class="white-box-lft white-box-tit">
                <h2>@Lang('Características')</h2>
                <div class="carac-list">
                    <ul>
                        <li>
                            <div class="cara-lft">
                                <div class="ls-lft">
                                    <i class="tipo"></i>
                                    <h3>@Lang('Tipo:')</h3>
                                </div>
                                <div class="ls-rgt">
                                    {{ separatedList($item->get_field('tipos'), 'title', ', ') }}
                                </div>
                            </div>
                            <div class="cara-rgt">
                                <div class="ls-lft">
                                    <i class="publico"></i>
                                    <h3>@Lang('Plazas')</h3>
                                </div>
                                <div class="ls-rgt">{{ $plazas }} @Lang('personas')</div>
                            </div>
                        </li>
                        <li>
                            <div class="cara-lft">
                                <div class="ls-lft">
                                    <i class="sabad"></i>
                                    <h3>@Lang('Ubicación:')</h3>
                                </div>
                                @if ( $gmaps && $gmaps->adreca )
                                <div class="ls-rgt">{{ $gmaps->adreca }}</div>
                                @endif
                            </div>
                            <div class="cara-rgt">
                                <div class="ls-lft">
                                    <i class="entorno"></i>
                                    <h3>@Lang('Entorno:')</h3>
                                </div>
                                <div class="ls-rgt">
                                    {{ separatedList($item->get_field('entorn'), 'title', ', ') }}
                                </div>
                            </div>
                        </li>
                        <li>
                            @if ($item->hasField('tipo-habitacion'))
                            <div class="cara-lft">
                                <div class="ls-lft">
                                    <i class="habitacion"></i>
                                    <h3>@Lang('Tipo Habitaciones:')</h3>
                                </div>
                                <div class="ls-rgt">
                                    {{ separatedList($item->get_field('tipo-habitacion'), 'title', ', ') }}
                                </div>
                            </div>
                            @endif
                            <div class="cara-rgt">
                                <div class="ls-lft">
                                    <i class="facilidades"></i>
                                    <h3>@Lang('Salas actividades:')</h3>
                                </div>
                                <div class="ls-rgt">{{ $num_salas }}</div>
                            </div>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            @if ($item->hasField('regimen'))
                            <div class="cara-lft">
                                <div class="ls-lft">
                                    <i class="reg"></i>
                                    <h3>@Lang('Régimen disponible:')</h3>
                                </div>
                                <div class="ls-rgt">
                                    {{ separatedList($item->get_field('regimen'), 'title', ', ') }}
                                </div>
                            </div>
                            @endif
                            <div class="cara-rgt">
                                <div class="ls-lft">
                                    <i class="ali"></i>
                                    <h3>@Lang('Alimentación:')</h3>
                                </div>
                                <div class="ls-rgt">
                                    {{ separatedList($item->get_field('alimentacio'), 'title', ', ') }}
                                </div>
                            </div>
                        </li>
                        <div class="cara-full">
                            <ul>
                                @if (isset($prestaciones) && count($prestaciones) > 0)
                                <li class="nb nbb">
                                    <div class="cara-full-lft ffl">
                                        <i class="facilidades"></i>
                                        <h3>@Lang('Prestaciones:')</h3>
                                    </div>
                                    <div class="cara-full-rgt">
                                        <ul>
                                            @foreach($prestaciones as $prestacion)
                                                @php($t_prestacion = $prestacion->getTranslation( \App::getLocale() ))
                                                <li> <span class="tick-lst">{{ $t_prestacion->title }}</span> </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                                @endif
                            </ul>
                        </div>
                        <li>
                            @if ($item->hasField('idiomas'))
                            <div class="cara-rgt">
                                <div class="ls-lft">
                                    <i class="habit"></i>
                                    <h3>@Lang('Idioma:')</h3>
                                </div>
                                <div class="ls-rgt">
                                    {{ separatedList($item->get_field('idiomas'), 'title', ', ') }}
                                </div>
                            </div>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>

            @if ($item->get_field('espacio-y-entorno') != '')
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Espacio y Entorno')</h2>
                <p>{{ strip_tags($item->get_field('espacio-y-entorno')) }}</p>
            </div>
            @endif

            <!-- Salas de Actividades -->
            @if ($item->get_field('salas-de-actividades') != '')
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Salas de trabajo')</h2>
                <p>{{ strip_tags($item->get_field('salas-de-actividades')) }}</p>
            </div>
            @endif

            <!-- Salas de Actividades -->
            @if ($item->get_field('alimentacion') != '')
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Alimentación')</h2>
                <p>{{ strip_tags($item->get_field('alimentacion')) }}</p>
            </div>
            @endif
            
            @if ($item->get_field('transporte-publico') != '')
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Transporte publico')</h2>
                <p>{{ strip_tags($item->get_field('transporte-publico')) }}</p>
            </div>
            @endif

            @if ($item->get_field('condiciones-reserva') != '')
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Condiciones de Reserva')</h2>
                <p>{{ strip_tags($item->get_field('condiciones-reserva')) }}</p>
            </div>
            @endif

            @if ($item->get_field('informacion-adicional') != '')
            <div class="white-box-lft white-box-tit white-box-para">
                <h2>@Lang('Información Adicional')</h2>
                <p>{{ strip_tags($item->get_field('informacion-adicional')) }}</p>
            </div>
            @endif

            @if ( $gmaps )
                @php($coordenades = explode(',', $gmaps->coordenades))
                @if (count($coordenades) > 1)
                    <div class="white-box-lft white-box-tit white-box-para">
                        <h2>@Lang('Ubicación')</h2>
                        <div id="gmaps" class="box-img" style="height: 500px;"></div>
                    </div>
                @endif
            @endif

            <!-- RATINGS / COMMENTS -->
            @includeIf('Front::ratings-comments')

        </div>
        <!-- right side -->
        <form id="FormEnviarMensaje" class="messaging" style="position: relative;">
            @php ( $author = $item->author )

            <div class="lds-ripple"><div></div><div></div></div>
            <input type="hidden" name="type" value="espacio">
            <input type="hidden" name="params[Espacio]" value="{{ $t_espacio->title }}">
            <input type="hidden" name="params[Organizador]" value="{{ ($author) ? $author->fullname() : 'empty' }}">
            <input type="hidden" name="params[espacio_url]" value="{{ $item->get_url() }}">
            <input type="hidden" name="no_mensaje_allowed" value=true>

            @if ( !empty($author) )
                <input type="hidden" name="to" value="{{ $author->id }}">
            @endif

        <div class="retro-rgt">
            <div class="white-box-rgt">
                <div class="frm-ryt">
                    <div class="price-no no">
                        <a href="javascript:void(0);" class="r-arrow-yellow"><img src="{{ asset($_front.'images/rig-mob-arr.png') }}" alt=""></a>
                        <h1><span>@Lang('Pide presupuesto para tu grupo')</span></h1>
                    </div>

                    @if ( !Auth::check())
                        <div class="frm-grp selet-per">
                            <label>@Lang('Nombre')</label>
                            <input class="plazas" type="text" name="name" required/>
                        </div>
                        <div class="frm-grp selet-per">
                            <label>@Lang('Email')</label>
                            <input class="plazas" type="email" name="email" required/>
                        </div>
                    @else
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                    @endif

                    <div class="frm-grp selet-per">
                        <label>@Lang('Teléfono')</label>
                        <input class="plazas" type="text" name="params[Teléfono]" required/>
                    </div>

                    @for($i = 1; $i <= 3; $i++)
                        <div class="frm-grp selet-per">
                            <label>@lang($i . 'ª opción Fechas')</label>
                            <input type="text" name="params[Fecha Inicio {{ $i }}]" class="datepicker-here" autocomplete="off" placeholder="@lang('Entrada')" {{ $i == 1 ? 'required' : '' }}>
                            <input type="text" name="params[Fecha Fin {{ $i }}]" class="datepicker-here" autocomplete="off" placeholder="@lang('Salida')" {{ $i == 1 ? 'required' : '' }}>
                        </div>
                    @endfor

                    <div class="frm-grp selet-per">
                        <label>@Lang('Plazas requeridas')</label>
                        <input class="plazas" type="text" name="params[Plazas]" required/>
                        {{--
                        <select class="select_box">
                            @for($i = 1; $i <= $plazas; $i++)
                            <option>{{ $i }} @Lang('Personas')</option>
                            @endfor
                        </select>
                        --}}
                    </div>
                    @if ($regimens = $item->get_field('regimen'))
                    <div class="frm-grp selet-per regimen">
                        <label>@Lang('Régimen elegido')</label>
                        <select class="select_box" name="params[Regimen]" required>
                            @foreach($regimens as $regimen)
                                @php($t_regimen = $regimen->translate())
                                <option value="{{ $t_regimen->title }}">{{ $t_regimen->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    <div class="frm-grp">
                        <label>@Lang('Mensaje')</label>
                        <textarea name="params[Contacto]" class="frm-ctrl text-area" style="max-height: 100px;" required></textarea>
                    </div>

                    <div class="message-sent" style="padding: 10px;margin: 10px;display:none;">
                        {{ __(':name sent successfully', ['name' => __('Message')]) }}
                    </div>

                    <div class="desimiler login-form single-form">
                        <div class="check full-wid">
                            <input id="policy" type="checkbox" name="policy" value="options" required>
                            <label for="policy">
                                <span>
                                    <span></span>
                                </span>
                                <div>@Lang('He leído y acepto la <a href=":link" target="_blank">Política de Privacidad</a>', [ 'link' => get_page_url('politica-de-privacidad') ])</div>
                            </label>
                        </div>

                        <div class="check full-wid">
                            <input id="newsletter" type="checkbox" name="newsletter" value="options">
                            <label for="newsletter">
                                <span>
                                    <span></span>
                                </span>
                                <div>@Lang('Quiero recibir la newsletter para organizadores')</div>
                            </label>
                        </div>
                    </div>

                    <div class="frm-grp envir-but">
                        <input type="submit" form="FormEnviarMensaje" class="anim" value="@Lang('Send')">
                    </div>
                </div>

                <div class="leer-list">
                    {{--
                    <h2>@Lang('Leer <span>politica de cancelacion')</span></h2>
                    <ul>
                        <li>@Lang('Mejores precios')</li>
                        <li>@Lang('Cancelacion gratuita')</li>
                        <li>@Lang('Sin gastos de targeta de credito')</li>
                    </ul>
                    --}}
                </div>
            </div>
        </div>
        </form>

    </div>
</section>
@endsection

@section('scripts1')
<script>var lang = '{{ $language_code }}';</script>

<script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
<script src="{{ asset($_front.'js/easyResponsiveTabs.js') }}"></script>
<script src="{{ asset($_front.'js/jquery.fancybox.js') }}"></script>
<script src="{{ asset($_front.'js/datepicker.min.js') }}"></script>
<script src="{{ asset($_front.'js/datepicker.'.$language_code.'.js') }}"></script>
<script src="{{ asset($_front.'js/pages/fitxa.js') }}"></script>

@if( $gmaps )
    @php($coordenades = explode(',', $gmaps->coordenades))

    @if (count($coordenades) > 1)
        @php($lat = $coordenades[0])
        @php($lng = $coordenades[1])

        <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ !empty($_config['google_maps']['value']) ? $_config['google_maps']['value'] : '' }}&callback=initMap"></script>
        <script>
        function initMap() {
            var myLatlng = new google.maps.LatLng({{ $lat }},{{ $lng }});
            var mapOptions = {
                zoom: 15,
                center: myLatlng,
                scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
                disableDoubleClickZoom: true,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    mapTypeIds: ['roadmap', 'terrain']
                }
            };
            map = new google.maps.Map(document.getElementById("gmaps"), mapOptions);

            marker = new google.maps.Marker({
                position: myLatlng,
                map: map
            });
            marker.setMap(map);
        }
        </script>
	@endif
@endif

<!-- ENVIAR PREGUNTA -->
<script>
$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});

$('#FormEnviarMensaje').on('submit', function(e) {
    var form_ele = $(this).addClass('loading');
    form_ele.addClass('loading');
    var form = $(this).serialize();
    $.ajax({
        url: ajaxURL,
        method: "POST",
        data: {
            action: "enviar_mensaje",
            method: "no_json",
            parameters: form
        },
        success: function(data) {
            form_ele.removeClass('loading');
            form_ele.find('.message-sent').show();
            form_ele.trigger('reset');
            toastr.success(form_ele.find('.message-sent').html());
            form_ele.find('input[type="submit"]').slideUp('fast');
        },
        error: function(data) {
            console.log(data);
        }
    });
    e.preventDefault();
    return false;
});
</script>
<!-- end ENVIAR PREGUNTA -->

<!-- DESCRIPTION VER MÁS -->
<script>
$(document).ready(function(){
    $('#flexslider2').flexslider({
        animation:"slide",
        slideshowSpeed:10000,
        animationDuration: 5000,
        easing: "swing",
        directionNav: true,
        controlNav: false,
        pausePlay: false,
        autoPlay:true,
    });

    var elements = $('.description .descripcion-vermas').find('div, p');
    
    elements.each(function(i, el) {
        $(el).replaceWith(function() {
            return $("<p />").append($(this).contents());
        });
    });

    var desvm_total_h = $('.descripcion-vermas').prop('scrollHeight');
    var desvm_h = $('.descripcion-vermas').height();
    if ( desvm_total_h > desvm_h ) {
        $('.description .btn-org.more').show();
        $('.description .btn-org.more').css('display', 'block');
    }

    $.each($('.datepicker-here'), function() {
        $(this).data('datepicker').update('onSelect', function (formattedDate, date, inst) {
            var name = inst.$el.attr('name');
            if (name.indexOf('Inicio') >= 0 && date) {
                name = name.replace('Inicio', 'Fin');

                var nextDatepicker = $(`.datepicker-here[name="${name}"]`);
                nextDatepicker.data('datepicker').update('minDate', date);
                nextDatepicker.focus();
            }
        });
    });

});
$(document).on('click', '.description .btn-org.more', function(){
    $('.descripcion-vermas').css('max-height', '2000px');
    $(this).hide();
});
</script>
<!-- end DESCRIPTION VER MÁS -->

<!-- FECHAS -->
<script>
    function isElementInViewport(el) {
        var rect = el.getBoundingClientRect();
        var fitsLeft = (rect.left >= 0 && rect.left <= $(window).width());
        var fitsTop = (rect.top >= 0 && rect.top <= $(window).height());
        var fitsRight = (rect.right >= 0 && rect.right <= $(window).width());
        var fitsBottom = (rect.bottom >= 0 && rect.bottom <= $(window).height());
        return {
            top: fitsTop,
            left: fitsLeft,
            right: fitsRight,
            bottom: fitsBottom,
            all: (fitsLeft && fitsTop && fitsRight && fitsBottom)
        };
    }

    $('.datepicker-here').datepicker({
        timepicker: false,
        language: lang,
        minDate: new Date(),
        autoClose: true,
        onHide: function(inst){
            inst.update('position', 'bottom left'); // Update the position to the default again
        },
        onShow: function(inst, animationComplete){
            // Just before showing the datepicker
            if(!animationComplete){
                var iFits = false;
                // Loop through a few possible position and see which one fits
                $.each(['bottom left', 'bottom center', 'top center', 'left bottom', 'right top', 'top center', 'bottom center'], function (i, pos) {
                    if (!iFits) {
                        inst.update('position', pos);
                        var fits = isElementInViewport(inst.$datepicker[0]);
                        if (fits.all) {
                            iFits = true;
                        }
                    }
                });
            }
        },
    });
</script>
<!-- end FECHAS -->

@endsection