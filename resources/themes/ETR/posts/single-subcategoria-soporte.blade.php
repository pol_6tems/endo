@extends('Front::layouts.base')

@section('contingut')
@php
    $categories = get_posts('categoria-soporte');
    $currentCategoria = $item->get_field('categoria');

    $subcategories = get_posts([
        'post_type' => 'subcategoria-soporte',
        'metas' => [
            ['categoria', $currentCategoria->id]
        ],
    ]);

    $subcategories = $subcategories->sortBy('title');
@endphp
<!-- Header Corresponent -->
@includeIf('Front::partials.header')
    <!-- centro-yellow starts -->
    <section class="centro-yellow">
        <div class="row1">
            <h2>@Lang('Centro de ayuda')</h2>
            {{--<div class="search">
                <a href="javascript:void(0);" class="srch-btn search-txt"></a>
                <input type="text" placeholder="¿Qué estás buscando?" class="search-txt">
            </div>--}}
            <h3><a href="tel:+34972664640">@Lang('+34 972 66 46 40')</a></h3>
        </div>
    </section>
    <!-- centro-yellow ends -->
    <!-- ayuda-sec starts -->
    <section class="ayuda-sec">
        <div class="row1">
            <div class="ayuda-top">
                <ul>
                    @foreach($categories as $categoria)
                        <li><a class="{{ ($currentCategoria->id == $categoria->id) ? 'active' : '' }}" href="{{ $categoria->get_url() }}">{{ $categoria->title }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="dudas-lft">
                <ul>
                    @foreach($subcategories as $subcategoria)
                        <li class="{{ ($item->id == $subcategoria->id) ? 'active' : '' }}"><a href="{{ $subcategoria->get_url() }}">{{ $subcategoria->title }}</a></li>
                    @endforeach
                </ul>
            </div>
            @if( isset($item) )
                @php($preguntes = get_posts([
                    'post_type' => 'grup-preguntes',
                    'metas' => [
                        ['subcategoria', $item->id]
                    ],
                ]))

                <div class="dudas-rgt">
                    {{-- <h2>{{ $item->title }}</h2> --}}
                    <ul class="dudas-hab">
                        @foreach($preguntes as $pregunta)
                            <li><a href="{{ $pregunta->get_url() }}"><img src="">{{ $pregunta->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </section>
@endsection