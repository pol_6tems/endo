<!-- VALORACIONES -->
@php
    $ratings = $params['ratings'];
    $post_id = $params['post_id'];
    $post_type = $params['post_type'];
    
    $rat_id = 0;
    if ( count($ratings['children']) == 1 ) {
        $rat = $ratings['children'][0];
        $rat_id = $rat->id;
    }
@endphp

@if ( !empty($ratings) && !empty($ratings['children']) && count($ratings['children']) > 0 )

<div class="rate-title">
    
    @if ( in_array($post_type, array('blog-post')) )
        <h2 style="font-size: 30px;">
            <img src="{{ $ratings['full'] }}" style="width: 38px;">
            {{ $ratings['num'] }}
        </h2>
    @else
        <h2>{{ $ratings['num'] }} @Lang('Ratings')</h2>
    @endif

    <div class="tit-ratings">
        @for ( $i = 1; $i <= $ratings['max']; $i++ )
            <a href="javascript:void(0);">
                <img class="rating-star"
                    src="{{ $i <= $ratings['avg'] ? $ratings['full'] : $ratings['empty'] }}"
                    alt=""
                    data-value="{{ $i }}"
                    data-id="{{ $rat_id }}"
                >
            </a>

        @endfor
        
        <svg class="checkmark" style="display:none;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>
        
        <!--<span>4/5</span>-->
    </div>
</div>

@if ( count($ratings['children']) > 1 )
<div class="list-rating">

    @if ( !empty( $ratings['children'] ) )

        @php ( $ratings_per_column = floor(count($ratings) / 2) )

        <div class="ls-rat-lft">
            <ul>
                @php ( $i = 0 )
                @foreach ( $ratings['children'] as $rat )

                    @if ( $i % $ratings_per_column == 0 ) </ul></div><div class="ls-rat-lft"><ul> @endif

                    <li>
                        <div class="rat-left">
                            <h3>{{ $rat->name }}:</h3>
                        </div>
                        <div class="rat-right">
                        @php ( $rat_avg = $rat->get_avg_value($post_id) )
                        @for ( $j = 1; $j <= $rat->maximo; $j++ )
                            <a href="javascript:void(0);">
                                <img class="rating-img {{ $rat_avg >= $j ? 'full' : 'empty' }} {{ $rat_avg >= $j ? 'original-full' : 'original-empty' }}" 
                                    src="{{ $rat_avg >= $j ? $rat->icono_full->get_thumbnail_url('thumbnail') : $rat->icono_empty->get_thumbnail_url('thumbnail') }}" 
                                    data-full="{{ $rat->icono_full->get_thumbnail_url('thumbnail') }}" 
                                    data-empty="{{ $rat->icono_empty->get_thumbnail_url('thumbnail') }}"
                                    data-value="{{ $j }}"
                                    data-id="{{ $rat->id }}"
                                >
                            </a>
                        @endfor

                            <svg class="checkmark" style="display:none;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>

                        </div>
                    </li>
                    
                    @php ( $i += 1 )
                @endforeach
            </ul>
        </div>

    @endif

</div>
@endif

@endif
<!-- end VALORACIONES -->

<script>
var $rating_selector = '{{ count($ratings['children']) > 1 ? '.list-rating ul li a .rating-img' : '.tit-ratings a img' }}';

window.onload = function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click', $rating_selector, function(){
        var me = $(this);
        var rating_id = $(this).data('id');
        var value = $(this).data('value');
        var parent = $(this).parent().parent();
        var checkmark = parent.find(".checkmark");
        if ( checkmark.css('display') == 'none' ) {
            ratings_save_value(checkmark, rating_id, value, function(data) {
                var rating = data.rating.value;
                me.closest('.rat-right').find(".rating-img").each(function(id, el) {
                    if (id < rating) {
                        $(el).attr('src', '{{ asset($_front.'images/heart-full.svg') }}');
                    } else {
                        $(el).attr('src', '{{ asset($_front.'images/heart-empty.svg') }}');
                    }
                });
            });
        }
    });

    function ratings_save_value(checkmark, rating_id, value, callback) {
        var user_id = {{ Auth::check() ? Auth::user()->id : 0 }};
        var post_id = {{ $post_id }};
        if ( user_id > 0 ) {
            $.ajax({
                url: '{{ route('ratings.store') }}',
                method: 'POST',
                data: { '_token': '{{csrf_token()}}', 'user_id': user_id, 'rating_id': rating_id, 'post_id': post_id, 'value': value },
                success: function(data) {
                    checkmark.show();
                    callback(data);
                }
            });
        }
    }
}
</script>

<style>
.checkmark__circle {
  stroke-dasharray: 166;
  stroke-dashoffset: 166;
  stroke-width: 2;
  stroke-miterlimit: 10;
  stroke: #7ac142;
  fill: none;
  animation: stroke 0.6s cubic-bezier(0.65, 0, 0.45, 1) forwards;
}

.checkmark {
    width: 15px;
    height: 15px;
    border-radius: 50%;
    display: inline-block;
    stroke-width: 2;
    stroke: #fff;
    stroke-miterlimit: 10;
    margin-left: 10%;
    box-shadow: inset 0px 0px 0px #7ac142;
    animation: fill .4s ease-in-out .4s forwards, scale .3s ease-in-out .9s both;
}

.checkmark__check {
  transform-origin: 50% 50%;
  stroke-dasharray: 48;
  stroke-dashoffset: 48;
  animation: stroke 0.3s cubic-bezier(0.65, 0, 0.45, 1) 0.8s forwards;
}

@keyframes stroke {
  100% {
    stroke-dashoffset: 0;
  }
}
@keyframes scale {
  0%, 100% {
    transform: none;
  }
  50% {
    transform: scale3d(1.1, 1.1, 1);
  }
}
@keyframes fill {
  100% {
    box-shadow: inset 0px 0px 0px 30px #7ac142;
  }
}
</style>