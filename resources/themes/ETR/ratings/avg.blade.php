@php ( $ratings = $params['ratings'] )

@if ( !empty($ratings) && !empty($ratings['children']) && count($ratings['children']) > 0 && $ratings['avg'] )

        @for ( $i = 1; $i <= $ratings['max']; $i++ )
            <a href="javascript:void(0);">
                <img src="{{ $i <= $ratings['avg'] ? $ratings['full'] : $ratings['empty'] }}" alt="">
            </a>
        @endfor

@endif