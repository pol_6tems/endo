<input id="count" type="hidden" value="{{ $total }}">
<ul class="ret-lst">
@foreach($llista as $espacio)
    @if ($espacio->type == 'banner-publicidad' && $espacio->media())
        @includeIf("Front::ajax.banner", ['banner' => $espacio])
        @continue
    @endif
    
    @php($gmaps = $espacio->get_field('ubicacion'))

    @if (!$gmaps)
        @php($gmaps = $espacio->get_field('ubicacion-y-localizacion'))
    @endif

    <li>
        <div class="obr-pad">
            <div class="obr-img">
                <a href="{{ $espacio->get_url() }}">
                    @php($galeria = $espacio->get_field('galeria'))
                    @if ($galeria && count($galeria) > 0)
                        <img src="{{ reset($galeria)->get_thumbnail_url('medium') }}" alt="">
                    @endif

                    @if (isFavorite($espacio->id))
                        <span class="pin clear" data-post-id="{{ $espacio->id }}" data-post-type="{{ $espacio->type }}">
                            <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                        </span>
                    @else
                        <span class="pin" data-post-id="{{ $espacio->id }}" data-post-type="{{ $espacio->type }}">
                            <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                            <div class="tooltip"><span class="tooltiptext">@Lang('ANADIR A LA LISTA')</span> </div>
                        </span>
                    @endif
                </a>
            </div>
            <div class="obr-cnt">
                <a href="{{ $espacio->get_url() }}"><h3>{{ $espacio->title }}</h3></a>
                
                <ul class="date-icon">
                    @if ($gmaps)
                        <li class="loc">{{ print_location($gmaps, ['locality', 'administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'country']) }}</li>
                    @endif
                </ul>

                <div class="mob-retiros-lst">
                    <ul class="lst-icon">
                        @if ($tipos = $espacio->get_field('tipos'))
                            <li class="noc"><i class="icon tipo"></i>{{ separatedList($tipos, 'title', ', ', 1, '(y más)') }}</li>
                        @endif
                        @if ($plazas = $espacio->get_field('plazas'))
                            <li class="idiom"><i class="icon publico"></i>{{ $plazas }} @lang('plazas')</li>
                        @endif
                        @if($regimen = $espacio->get_field('regimen'))
                            <li class="menu"><i class="icon reg"></i>{{ separatedList($regimen, 'title', ', ', 1, '(y más)') }}</li>
                        @endif
                        @if ($alimentacion = $espacio->get_field('alimentacio'))
                            <li class="tran"><i class="icon ali"></i>{{ separatedList($alimentacion, 'title', ', ', 1, '(y más)') }}</li>
                        @endif
                        @if ($trabajos = $espacio->get_field('trabajos-a-realizar'))
                            <li class="noc"><i class="icon trabajo"></i>{{ separatedList($trabajos, 'title', ', ', 1, '(y más)') }}</li>
                        @endif
                        @if ($duracion = $espacio->get_field('duracion-minima'))
                            <li class="idiom"><i class="icon duracion-min"></i>{{ separatedList($duracion, 'title', ', ', 1, '(y más)') }}</li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="price">
                <div class="rating">
                    <!-- VALORACIONES -->
                    @php ( do_action('show_ratings_avg', $espacio->id) )
                </div>

                <div class="p-lft">
                    <a href="{{ $espacio->get_url() }}" class="mas-btn">@Lang('VER MÁS')</a> 
                </div>

            </div>
        </div>
    </li>
@endforeach
</ul>

{{ $llista->appends($_GET)->links('Front::pagination.bootstrap-4') }}