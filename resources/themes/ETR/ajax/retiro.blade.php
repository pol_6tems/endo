<input id="count" type="hidden" value="{{ $totalRetiros }}">
<ul class="ret-lst">
@foreach($retiros as $index => $retiro)
    @if ($retiro->type == 'banner-publicidad' && $retiro->media())
        @includeIf("Front::ajax.banner", ['banner' => $retiro])
        @continue
    @endif
    
    @php($barata = obtenirOfertaBarata($retiro))
    @php($moneda = ($retiro->author && $retiro->author->get_field('moneda')) ? $retiro->author->get_field('moneda') : null)
    <li>
        <div id="{{ $retiro->id }}" data-post-type="{{ $retiro->type }}" class="obr-pad {{ ($retiro->get_field('destacado') == 1) ? 'destacado' : '' }}">
            @if (Auth::user() && Auth::user()->isAdmin() && isset($retiro->punctuation))
                <div class="show_punctuation">
                    <span></span>
                    <div class="punctuation">
                        {{--
                        @if ($_SERVER['REMOTE_ADDR'] == '80.64.37.127' || $_SERVER['REMOTE_ADDR'] == '91.126.218.219')
                            <div class="punctuation-item">
                                <label for="calidad">@Lang('Distance')</label>
                                {{ $retiro->punctuation->distance }} Punts: {{ $retiro->punctuation->distpunts }} Tu: ({{ $retiro->punctuation->tuCoords }}) Retiro: ({{ $retiro->punctuation->retiroCoords }})
                            </div>
                        @endif
                        --}}
                        <div class="punctuation-item">
                            <label for="calidad">@Lang('Location')</label>
                            @php($value = $retiro->punctuation->location / 10 * 100)
                            <div id="calidad" class="meter">
                                <span data-value="{{ $retiro->punctuation->location }}" style="width: {{ ($value > 100) ? '100' : $value }}%">{{ $retiro->punctuation->location }}</span>
                            </div>
                        </div>
                        <div class="punctuation-item">
                            <label for="calidad">@Lang('Calidad')</label>
                            @php($value = $retiro->punctuation->calidad / 3 * 100)
                            <div id="calidad" class="meter">
                                <span data-value="{{ $retiro->punctuation->calidad }}" style="width: {{ ($value > 100) ? '100' : $value }}%">{{ $retiro->punctuation->calidad }}</span>
                            </div>
                        </div>
                        <div class="punctuation-item">
                            <label for="punct_org">@Lang('Puntuación ORG.')</label>
                            @php($value = $retiro->punctuation->custom_rating / 10 * 100)
                            <div class="meter">
                                <span data-value="{{ $retiro->punctuation->custom_rating }}" style="width: {{ ($value > 100) ? '100' : $value }}%">{{ $retiro->punctuation->custom_rating }}</span>
                            </div>
                        </div>
                        <div class="punctuation-item">
                            <label for="politica">@Lang('Cancelación')</label>
                            @php($value = $retiro->punctuation->politica / 5 * 100)
                            <div id="politica" class="meter">
                                <span data-value="{{ $retiro->punctuation->politica }}" style="width: {{ ($value > 100) ? '100' : $value }}%">{{ $retiro->punctuation->politica }}</span>
                            </div>
                        </div>
                        <div class="punctuation-item">
                            <label for="rating">@Lang('Punt. Medias')</label>
                            @php($value = $retiro->punctuation->rating / 5 * 100)
                            <div id="rating" class="meter">
                                <span data-value="{{ $retiro->punctuation->rating }}" style="width: {{ ($value > 100) ? '100' : $value }}%">{{ $retiro->punctuation->rating }}</span>
                            </div>
                        </div>
                        <div class="punctuation-item">
                            <label for="random">@Lang('Random')</label>
                            @php($value = $retiro->punctuation->random / 3 * 100)
                            <div id="random" class="meter">
                                <span data-value="{{ $retiro->punctuation->random }}" style="width: {{ ($value > 100) ? '100' : $value }}%">{{ $retiro->punctuation->random }}</span>
                            </div>
                        </div>
                        <div class="punctuation-item total-punctuation">
                            <label>@Lang('Total Punctuation'):</label> <div class="total-p">{{ $retiro->punctuation->total }}</div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="obr-img">
                <a href="{{ $retiro->get_url() }}">
                    @php($galeria = $retiro->get_field('galeria'))
                    @if ($galeria && count($galeria) > 0)
                        <img src="{{ reset($galeria)->get_thumbnail_url('medium') }}" alt="">
                    @endif
                    @if (isFavorite($retiro->id))
                        <span class="pin clear">
                            <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                        </span>
                    @else
                        <span class="pin">
                            <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                            <div class="tooltip"><span class="tooltiptext">@Lang('ANADIR A LA LISTA')</span> </div>
                        </span>
                    @endif
                </a>
            </div>
            <div class="obr-cnt">
                <a href="{{ $retiro->get_url() }}"><h3>{{ $retiro->title }}</h3></a>
                <ul class="date-icon"> 
                    @if ( $allDies = obtenirProximesDates($retiro) )
                        @php( $dies = $allDies->first() )
                        @php( $primerDia = ($dies instanceof Illuminate\Support\Collection && $dies->isNotEmpty()) ? $dies->first() : $allDies->first() )
                        @php( $ultimDia = ($dies instanceof Illuminate\Support\Collection && $dies->isNotEmpty()) ? $dies->last() : $allDies->last() )
                        
                        @if ($primerDia && $ultimDia)
                            <li class="date">{{ $primerDia->format('d') }} {{ $primerDia->format('M') }}. al {{ $ultimDia->format('d') }} {{ $ultimDia->format('M') }}. de {{ $ultimDia->format('Y') }} @if ($allDies->count() > 1)@lang('y otras fechas')@endif</li>
                        @endif
                    @endif

                    @if ( $alojamiento = $retiro->get_field('alojamiento') )
                        @if(is_object($alojamiento) && $localizacion = $alojamiento->get_field('localizacion'))
                            <li class="loc">{{ print_location($localizacion, ['locality', 'administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'country']) }}</li>
                        @endif
                    @endif
                </ul>
                <div class="mob-retiros-lst">
                    <ul class="lst-icon">
                        @if ($duracion = $retiro->get_field('calendario'))
                            <li class="noc"><i class="icon sabad"></i>{{ $duracion->duracion }} dias / {{ ($duracion->duracion - 1) }} noches</li>
                        @endif
                        @if ($idiomes = $retiro->get_field('idioma'))
                            <li class="idiom"><i class="icon habit"></i>{{ $idiomes->first()->title }}</li>
                        @endif
                        @if($alimentacion = $retiro->get_field('alimentacion'))
                            <li class="menu"><i class="icon ali"></i>{{ is_string($alimentacion) ? cut_text($alimentacion) : $alimentacion->first()->title }}</li>
                        @endif

                        @if ($tipoHabitaciones = $retiro->get_field('tipo-habitaciones'))
                            @if ($tipoHabitaciones->count() > 0)
                            <li class="tran"><i class="icon habitacion"></i>{{ $tipoHabitaciones->first()->title }}</li>
                            @endif
                        @endif
                    </ul>
                    @if ($barata && $barata['descuento']['value'] != '' && $barata && $barata['fecha-limite']['value'] != '')
                        @php($data = \Carbon\Carbon::createFromFormat('d/m/Y', "31/12/2099"))
                        @if ($barata['fecha-limite']['value'] != "")
                            @php($data = convertDate($barata['fecha-limite']['value']))
                        @endif
                        @if ($data && $data->gte(new \Carbon\Carbon()))
                            <div class="dis-txt">{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $barata['descuento']['value']) }} dto. reservando antes del {{ $data->format('d M. Y') }}</div>
                        @endif
                    @endif
                </div>
            </div>
            <div class="price">
                <div class="mob-price">
                    <div class="p-rgt">
                        @if ($barata && $barata['precio']['value'] != '')
                            @if ($barata && $barata['descuento']['value'] != '' && $barata && $barata['fecha-limite']['value'] != '')
                                <h4>{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', obtenirPreuAmbDescompteOferta($barata)) }}</h4>
                                @php($data = \Carbon\Carbon::createFromFormat('d/m/Y', "31/12/2099"))
                                @if ($barata['fecha-limite']['value'] != "")
                                    @php($data = convertDate($barata['fecha-limite']['value']))
                                @endif
                                @if ($data && $data->gte(new \Carbon\Carbon()))
                                    <h5>{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $barata['precio']['value']) }}</h5>
                                @endif
                            @else
                                <h4>{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $barata['precio']['value']) }}</h4>
                            @endif
                        @endif
                    </div>
                    @if ($barata && $barata['descuento']['value'] != '' && $barata && $barata['fecha-limite']['value'] != '')
                        @php($data = \Carbon\Carbon::createFromFormat('d/m/Y', "31/12/2099"))
                        @if ($barata['fecha-limite']['value'] != "")
                            @php($data = convertDate($barata['fecha-limite']['value']))
                        @endif
                        
                        @if ($data && $data->gte(new \Carbon\Carbon()))
                            <div class="dis-txt">{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $barata['descuento']['value']) }} dto. reservando antes del {{ $data->format('d M. Y') }}</div>
                        @endif
                    @endif
                    <div class="p-lft">
                        <div class="rating">
                            <!-- VALORACIONES -->
                            @include('Front::partials.reviews-avg-rating', ['retObj' => $retiro])
                        </div>
                        <a class="mas-btn" href="{{ $retiro->get_url() }}">@Lang('VER MÁS')</a>
                    </div>
                </div>
            </div>
        </div>
    </li>
    @endforeach
</ul>

{{ $retiros->appends($_GET)->links('Front::pagination.bootstrap-4') }}