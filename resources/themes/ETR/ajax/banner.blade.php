@php($label = $banner->get_field('texto-link'))
@php($link = $banner->get_field('link'))
<li>
    <div class="ban-image" style="background-image: url('{{ $banner->media()->get_thumbnail_url("large") }}')">
        <h3>{{ $banner->title }}</h3>
        <p>{{ $banner->get_field('content') }}</p>
        @if ($link && $label)
            <a href="{{ $link }}" target="_blank">{{ $label }}</a>
        @endif
        <div class="shadow"></div>
    </div>
</li>