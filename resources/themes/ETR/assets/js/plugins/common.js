var $ = jQuery.noConflict();
// $j is now an alias to the jQuery function; creating the new alias is optional.
$(document).ready(function () {
	$('.fancybox, .fancybox1').fancybox({
		helpers : {
			overlay : {
				locked : true // false (page scrolls behind fancybox)
			}
		}
	});
});


$(window).on('load', function () {
	// flexslider
	$('.banner-home .flexslider').flexslider({
		animation: "fade",
		slideshowSpeed: 5000,
		animationDuration: 3000,
		easing: "swing",
		directionNav: false,
		controlNav: true,
		pausePlay: false
	});
});

// Input Box Placeholder
function fnremove(arg,val)	{
	if (arg.value == '') {arg.value = val}
}	
function fnshow(arg,val)	{
	if (arg.value == val) {arg.value = ''}
}

$(document).ready(function () {	
	// Sticky Header		
	$("header.inner-header").sticky({topSpacing:0});
	
	$(".select_box").selectbox();
	
	// Mmenu - Mobile Menu
	//$('#mobNav').mmenu();	
	$( ".mmoverly" ).appendTo( $( "body" ) );
   
   jsUpdateSize();
});

$(window).resize(function(){	
	jsUpdateSize();
});
function jsUpdateSize()
{
	var width = $(window).width();
    var height = $(window).height();	
	if(width<=767)
	{
		$('.banner-home .flexslider .slides li').each(function()
		{
			var img = $('img', this).attr('src');
			$(this).css('background-image','url(' + img + ')');		
			$(".banner-home").css('height',415);
			$(this).css('height',415);		
		});
	}
	else{
		$('.banner-home .flexslider .slides li').each(function()
		{
			$(this).css('background-image','');
			$(".banner-home").css('height','auto');
			$(this).css('height','auto');
		});	
		
	}
	
	if(width<=767)
	{
		$('.banner-home1 .flexslider .slides li').each(function()
		{
			var img = $('img', this).attr('src');
			$(this).css('background-image','url(' + img + ')');		
			$(".banner-home1").css('height',530);
			$(this).css('height',530);		
		});
	}
	else{
		$('.banner-home1 .flexslider .slides li').each(function()
		{
			$(this).css('background-image','');
			$(".banner-home1").css('height','auto');
			$(this).css('height','auto');
		});	
		
	}
	if(width<=480)
	{
		$('.resp-banner .bann-img').each(function()
		{
			var img = $('img', this).attr('src');
			$(this).css('background-image','url(' + img + ')');		
			$(".bann-img").css('height',200);
			$(this).css('height',200);		
		});
	}
	else{
		$('.resp-banner .bann-img').each(function()
		{
			$(this).css('background-image','');
			$(".bann-img").css('height','auto');
			$(this).css('height','auto');
		});	
		
	}
	if(width<=767)
	{
		$('.necesit .necesit-img').each(function()
		{
			var img = $('img', this).attr('src');
			$(this).css('background-image','url(' + img + ')');		
			$(".necesit-img").css('height',260);
			$(this).css('height',260);		
		});
	}
	else{
		$('.necesit .necesit-img').each(function()
		{
			$(this).css('background-image','');
			$(".necesit-img").css('height','auto');
			$(this).css('height','auto');
		});	
		
	}
	
}	

/* carousal */
$(document).ready(function(){
// Item Carousel
	var owl = $('#aun-car');
	owl.owlCarousel({
		goToFirstSpeed :1000,
		loop:true,
		items:4,
		margin:15,
		autoplay:true,
		autoplayTimeout:5500,
		autoplayHoverPause:true,		
	    nav : true,
	    dots : false,
		responsive: {
			0:{ items:1, margin:0,},
			480:{ items:1},
			640:{ items:2},
			768:{ items:3},
			1024:{ items:4}
		},
	});
	
var owl = $('#aun-car1');
	owl.owlCarousel({
		goToFirstSpeed :1000,
		loop:true,
		items:3,
		margin:15,
		autoplay:true,
		autoplayTimeout:5500,
		autoplayHoverPause:true,		
	    nav : true,
	    dots : false,
		responsive: {
			0:{ items:1, margin:0,},
			480:{ items:1},
			640:{ items:2},
			768:{ items:3},
			1024:{ items:3}
		},
	});	


	var owl = $('#abt-car');
	owl.owlCarousel({
		goToFirstSpeed :1000,
		loop:true,
		items:1,
		margin:0,
		autoplay:true,
		autoplayTimeout:5500,
		autoplayHoverPause:true,		
	    nav : true,
	    dots : true,
		responsive: {
			0:{ items:1, margin:0,},
			480:{ items:1},
			640:{ items:1},
			768:{ items:1},
			1024:{ items:1}
		},
	});	
	
	var owl = $('#testmnl-car');
	owl.owlCarousel({
		goToFirstSpeed :1000,
		loop:true,
		items:1,
		margin:1,
		autoplay:true,
		autoplayTimeout:5500,
		autoplayHoverPause:true,		
	    nav : true,
	    dots : false,
		responsive: {
			0:{ items:1, margin:0,},
			480:{ items:1},
			640:{ items:1},
			768:{ items:1},
			1024:{ items:1}
		},
	});
	
	
});	

	/* mobile menu */
$('.logo-mbl').click(function(e){
	$('.mbl-menu').toggleClass("mbl-menuon");
});


$('.lang-mbl').click(function(e){
	$(this).next().toggleClass('show');
	$('.rs-div').removeClass('show');
});
$('.lang-rs').click(function(e){
	$(this).next().toggleClass('show');
	$('.lang-div').removeClass('show');
});


//Tab list 
	$('.tab_content').hide();
	$('.tab_content:first').show();
	$('.tabs li:first').addClass('active');
	
	$('.tabs li').click(function() {
		$('.tabs li').removeClass('active');
		$(this).addClass('active');
		$('.tab_content').hide();			
		var selectTab = $(this).find('a').attr("href");			
		$(selectTab).fadeIn();
	});
//Tab list



