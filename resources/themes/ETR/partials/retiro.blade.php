<div id="{{ $retiro->id }}" data-post-type="{{ $retiro->type }}" class="obr-pad">
    <a href="{{ $retiro->get_url() }}">
        <div class="obr-img">
            @php($galeria = collect($retiro->get_field('galeria')))
            @if ($galeria && count($galeria) > 0 && !is_string($galeria->first()))
                <img {{-- class="{{ ($owl) ? 'owl-lazy' : '' }}" --}} src="{{ $galeria->first()->get_thumbnail_url('medium') }}" alt="">
            @endif

            @if (isFavorite($retiro->id))
                <span class="pin clear">
                    <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                </span>
            @else
                <span class="pin">
                    <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                    <div class="tooltip"><span class="tooltiptext">@Lang('ANADIR A LA LISTA')</span> </div>
                </span>
            @endif
        </div>
        <div class="obr-cnt">
            <h3>{{ $retiro->title }}</h3>
            <ul class="date-icon"> 
                @if ( $allDies = obtenirProximesDates($retiro) )
                    @php( $dies = $allDies->first() )
                    @php( $primerDia = ($dies instanceof Illuminate\Support\Collection && $dies->isNotEmpty()) ? $dies->first() : $allDies->first() )
                    @php( $ultimDia = ($dies instanceof Illuminate\Support\Collection && $dies->isNotEmpty()) ? $dies->last() : $allDies->last() )

                    @if ($primerDia && $ultimDia)
                        <li class="date">{{ $primerDia->format('d') }} {{ $primerDia->format('M') }}. al {{ $ultimDia->format('d') }} {{ $ultimDia->format('M') }}. de {{ $ultimDia->format('Y') }} @if ($allDies->count() > 1)@lang('y otras fechas')@endif</li>
                    @endif
                @endif

                @php($alojamiento = $retiro->get_field('alojamiento'))
                @if( $alojamiento && is_object($alojamiento) && $localizacion = $alojamiento->get_field('localizacion') )
                    <li class="loc" style="padding-left: 30px;">{{ print_location($localizacion, ['locality', 'administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'country']) }}</li>
                @endif
            </ul>
            @if (!isset($mobile) || !$mobile)
            <div class="mob-retiros-lst">
                <ul class="lst-icon">
                    @if ($duracion = $retiro->get_field('calendario'))
                        <li class="noc"><i class="icon sabad"></i>{{ $duracion->duracion }} dias / {{ ($duracion->duracion - 1) }} noches</li>
                    @endif
                    @if ($idiomes = $retiro->get_field('idioma'))
                        <li class="idiom"><i class="icon habit"></i>{{ $idiomes->first()->title }}</li>
                    @endif
                    @if($alimentacion = $retiro->get_field('alimentacion'))
                    <li class="menu"><i class="icon ali"></i>{{ $alimentacion->first()->title }}</li>
                    @endif

                    @php ($tipoHabitaciones = $retiro->get_field('tipo-habitaciones'))
                    @if ($tipoHabitaciones && $tipoHabitaciones->count())
                        <li class="tran"><i class="icon habitacion"></i>{{ $tipoHabitaciones->first()->title }}</li>
                    @endif
                </ul>
            </div>
            @endif
        </div>
        <div class="price">
            <div class="p-lft">
                <div class="rating">
                    <!-- VALORACIONES -->
                    @include('Front::partials.reviews-avg-rating', ['retObj' => $retiro])
                </div>
            </div>
            <div class="p-rgt">
                @php( $barata = obtenirOfertaBarata($retiro) )
                @if ($precio = obtenirPreuAmbDescompteOferta($barata))
                    <h4>{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio) }}</h4>
                @endif

                @php($data = \Carbon\Carbon::createFromFormat('d/m/Y', "31/12/2099"))
                @if ($barata['fecha-limite']['value'] != "")
                    @php($data = convertDate($barata['fecha-limite']['value']))
                @endif
                
                @if ($data && $data->gte(new \Carbon\Carbon()) && isset($barata['precio']['value']) && $precio && $precio < $barata['precio']['value'])
                    <h5>{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $barata['precio']['value']) }}</h5>
                @endif
            </div>
        </div>
    </a>
</div>