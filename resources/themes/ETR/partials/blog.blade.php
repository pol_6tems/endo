<section class="home-blog volu">
    <div class="row1">
        <h2>@Lang('Cultura del Retiro')</h2>
        <div class="home-blog-lft fullwidth">
            <ul>
                @foreach(get_posts(['post_type' => 'blog-post', 'numberposts' => 6]) as $post)
                @php($t_post = $post->translate())
                <li>
                    <div class="blog-img">
                        @if ($t_post->media)
                        <a href="{{ $post->get_url() }}"><img src="{{ $t_post->media->get_thumbnail_url("small") }}" alt="{{ $t_post->media->alt }}"></a>
                        @endif
                    </div>
                    <div class="blog-cont">
                        <a href="{{ $post->get_url() }}"><h3>{{ $t_post->title }}</h3></a>
                        @if($post->author)
                            @php ($publishDate = $post->get_field('fecha-publicacion') ? \Carbon\Carbon::createFromFormat('d/m/Y', $post->get_field('fecha-publicacion')) : $post->created_at)
                            <h4>{{ ucfirst(\Jenssegers\Date\Date::parse($publishDate)->format('l j \\d\\e F Y')) }} - @lang('por') {{ $post->author->name }}</h4>
                        @endif
                        <p>{{ get_excerpt($t_post->description, 40) }} <a href="{{ $post->get_url() }}">@Lang('Read more')</a></p>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</section>