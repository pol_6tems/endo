<header class="header1 header2">
    <div class="main-menu">
        <div class="row">
            <div class="logo"> <a href="{{ route('index') }}" class="logo-dsk"><img src="{{ asset($_front.'images/yellow-logo.jpg') }}"
                        alt="ENCUENTRA TU RETIRO" title="ENCUENTRA TU RETIRO"> </a>
                <a href="javascript:void(0);" class="logo-mbl logo-mbl-y"><img src="{{ asset($_front.'images/logo-yellow.svg') }}"
                        alt="ENCUENTRA TU RETIRO" title="ENCUENTRA TU RETIRO"> </a>
            </div>
            <div class="head-rgt">
                <div class="lang_select">
                    <div class="selet selet-lang">
                        <select class="select_box">
                            <option>ES</option>
                            <option>EN</option>
                            <option>SP</option>
                        </select>
                    </div>
                </div>
                <div class="hd-links">
                    <ul class="icon-links">
                        <li class="user"><a href="{{ route('perfil.show') }}"><span><img src="{{ asset($_front.'images/user-ico.png') }}"
                                        alt=""></span>Usuarios</a></li>
                        <li class="euro"><a href="javascript:void(0);"><span><i class="fa fa-eur"
                                        aria-hidden="true"></i></span></a></li>
                        <li class="pin-w"><a href="javascript:void(0);"><span><img src="{{ asset($_front.'images/pushpin.svg') }}"
                                        alt=""></span></a></li>
                        <li class="support"><a href="javascript:void(0);"><span><img src="{{ asset($_front.'images/support-ico.png') }}"
                                        alt=""></span></a></li>
                    </ul>
                </div>
                <!-- mobile nav starts-->
                <!--<div class="m-menu"><a href="javascript:void(0);" id="hie_menu"> <span></span> <span></span> <span></span></a></div>-->
                <!-- mobile nav ends-->
                <div class="menu">
                    <div id="smoothmenu" class="ddsmoothmenu">
                        {{--
                        <ul>
                            <li><a href="javascript:void(0);">Retiros</a></li>
                            <li><a href="javascript:void(0);">Blog</a></li>
                            <li><a href="javascript:void(0);">Faqs</a></li>
                        </ul>
                        --}}
                    </div>
                </div>
            </div>

            <div class="search-div">
                <div class="search-head">
                    <input style="display:none" type="password" name="fakesearch"/>
                    <input type="text" class="searchTerm" name="search" placeholder="Retiros de meditacion en Espana" autocomplete="off">
                    <button type="submit" class="searchButton"></button>
                </div>
            </div>
            <div class="mbl-menu">
                <ul>
                    <li> <a href="javascript:void(0);" class="lang-mbl">ES</a>
                        <div class="lang-div">
                            <ul>
                                <li>
                                    <label class="control control-radio">
                                        English - EN
                                        <input type="radio" name="radio" />
                                        <div class="control_indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control-radio">
                                        Deutsch - DE
                                        <input type="radio" name="radio" />
                                        <div class="control_indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control-radio">
                                        Français - FR
                                        <input type="radio" name="radio" />
                                        <div class="control_indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control-radio">
                                        Nederlands - NL
                                        <input type="radio" name="radio" />
                                        <div class="control_indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control-radio">
                                        Español - ES
                                        <input type="radio" name="radio" />
                                        <div class="control_indicator"></div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li> <a href="javascript:void(0);" class="lang-rs">RS</a>
                        <div class="rs-div">
                            <ul>
                                <li>
                                    <label class="control control-radio">
                                        AUD - Australian Dollar
                                        <input type="radio" name="radio" />
                                        <div class="control_indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control-radio">
                                        CAD - Canadian Dollar
                                        <input type="radio" name="radio" />
                                        <div class="control_indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control-radio">
                                        € - Euro
                                        <input type="radio" name="radio" />
                                        <div class="control_indicator"></div>
                                    </label>
                                </li>
                                <li>
                                    <label class="control control-radio">
                                        US$ - United States Dollar
                                        <input type="radio" name="radio" />
                                        <div class="control_indicator"></div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @if (Auth::check())
                    <li><a href="{{ route('perfil.favoritos') }}"><img src="{{ asset($_front.'images/pushpin-grey.svg') }}" title=""> Mi lista</a></li>
                    <li><a href="{{ get_post(507)->get_url() }}"><img src="{{ asset($_front.'images/dollar-coin-stack-grey.svg') }}" title="">Dharmas({{ Auth::user()->id }})</a></li>
                    @endif
                    <li><a href="javascript:void(0);">Retiros</a></li>
                    <li><a href="javascript:void(0);">Blog</a></li>
                    <li><a href="javascript:void(0);">Ayuda</a></li>
                    <li><a href="javascript:void(0);">Usuarios</a></li>
                    <li><a class="fancybox1 show" href="#poup-txt2">Registro</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
@includeIf('Front::partials.popup')