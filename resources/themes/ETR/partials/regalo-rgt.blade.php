<div class="retro-rgt no-tpad">
    <div class="white-box-rgt">
        <img class="detalles-retiro" src="{{ $kit->media()->get_thumbnail_url() }}" alt="">

        <div class="clearfix-block"></div>
        <div class="white-box-cnt">
            <div class="item-des">
                <span>@lang('Incluye Caja Regalo con'):</span>

                {!! $kit->description !!}
            </div>

            <div class="dat-select resumen with-borders">
                <h2>@Lang('Pagos')</h2>
                <ul>
                    <li>
                        <div class="resu-txt">
                            <h2>@Lang('Precio')</h2>
                        </div>
                        <div class="resu-price">{{ toUserCurrency('EUR', $kit->get_field('precio')) }}</div>
                    </li>
                    <li>
                        <div class="resu-txt">
                            <h2>@Lang('Gastos de envío')</h2>
                        </div>
                        <div class="resu-price">{{ toUserCurrency('EUR', $portes) }}</div>
                    </li>
                </ul>
            </div>

            <div class="dat-select resumen total">
                <ul>
                    <li>
                        <div class="resu-txt">
                            <h2>@lang('Total')</h2>
                        </div>
                        <div class="resu-price">{{ toUserCurrency('EUR', $kit->get_field('precio') + $portes) }}</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>