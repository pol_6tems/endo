<div class="accordion_example2">
    <!-- Section 1 -->
    <div class="accordion_in acc_active">
        <div class="acc_head">TIPO</div>
        <div class="acc_content">
            <ul class="tip-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput"><img src="{{ asset($_front.'images/crecimiento-personal.svg') }}" alt=""
                                class="lft2">Crecimiento personal</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput"><img src="{{ asset($_front.'images/descanso.svg') }}" alt=""
                                class="lft2">Descanso</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput"> <img src="{{ asset($_front.'images/meditacion.svg') }}" alt=""
                                class="lft2">Meditacion</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput"><img src="{{ asset($_front.'images/yoga.svg') }}" alt="">Yoga</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput"><img src="{{ asset($_front.'images/viajes.svg') }}" alt="" class="lft2">Viajes</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput"><img src="{{ asset($_front.'images/detox.svg') }}" alt="" class="lft2">Detox</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput"><img src="{{ asset($_front.'images/arte.svg') }}" alt="">Arte</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">PRECIO</div>
        <div class="acc_content">
            <div class="price-range">
                <div id="ex0" style="">
                    <div id="s1" class="mb_slider" data-property="{startAt:2100, minVal:0, maxVal:3500}"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">CATEGORIAS</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Biodanza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Descanso</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Detox</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Meditacion</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Tai-chi</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Aventura</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Ayauasca</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Ayurveda</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Budista</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creativo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Espiritual</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Biodanza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Descanso</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">BENIFICIO</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Control mental</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Mejora de las Relaciones</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Comunicacion</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Control stress</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Trabajo en equipo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Comunicacion</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Control stress</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">GEOLOCALIZACION</div>
        <div class="acc_content"> <a href="javascript:void(0);" class="ver-link">Ver</a>
            <div class="geo-map"> <img src="{{ asset($_front.'images/geo-map.jpg') }}" alt=""> </div>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">INDICADO PARA </div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- Section 2 -->
    <div class="accordion_in">
        <div class="acc_head">DURACION</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- Section 3 -->
    <div class="accordion_in">
        <div class="acc_head">ENTORNO</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">TERAPIAS</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">YOGA</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">DISCIPLINA</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">DEPORTE</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">FORMATO</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">EL RETIRO INCLUYE</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">IDIOMAS</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">PUBLICO</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">TIPO DE ALOJAMIENTO</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">HABITACIONES</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="accordion_in">
        <div class="acc_head">ALIMENTACION</div>
        <div class="acc_content">
            <ul class="cat-list">
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Antiestresd</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Conexion conla naturaleza</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Abertura de coinciencia</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Creatividad</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Liderazgo</label>
                    </div>
                </li>
                <li>
                    <div class="input-container">
                        <input type="checkbox" value="1" name="" />
                        <label for="customCheckboxInput">Hablar en publico</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>