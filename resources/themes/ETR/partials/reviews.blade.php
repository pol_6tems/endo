@if ($reviews && $reviews->count())
    <div class="white-box-lft white-box-tit white-box-para retiro-reviews">
        <h2>{{ $reviews->count() == 1 ? __('1 Valoración') : __(':num Valoraciones', ['num' => $reviews->count()]) }}
            <span>
                @for ( $j = 1; $j <= $one_rating->maximo; $j++ )
                    <img class="rating-img {{ $reviews_avgs['total_avg'] >= $j ? 'full' : 'empty' }} {{ $reviews_avgs['total_avg'] >= $j ? 'original-full' : 'original-empty' }}"
                         src="{{ $reviews_avgs['total_avg'] >= $j ? $one_rating->icono_full->get_thumbnail_url('thumbnail') : $one_rating->icono_empty->get_thumbnail_url('thumbnail') }}">
                @endfor
            </span>
        </h2>

        <div class="reviews-avg">
            @foreach($reviews_avgs['ratings_avgs'] as $rating_avg)<div class="ret-ratings">
                    <dl class="dl-horizontal">
                        <dt>{{ $rating_avg['name'] }}:</dt><dd>
                            @for ( $j = 1; $j <= $one_rating->maximo; $j++ )
                                <img class="rating-img {{ $rating_avg['avg'] >= $j ? 'full' : 'empty' }} {{ $rating_avg['avg'] >= $j ? 'original-full' : 'original-empty' }}"
                                     src="{{ $rating_avg['avg'] >= $j ? $one_rating->icono_full->get_thumbnail_url('thumbnail') : $one_rating->icono_empty->get_thumbnail_url('thumbnail') }}">
                            @endfor
                        </dd>
                    </dl>
                </div>@endforeach
        </div>

        @foreach($reviews as $review)
            <div class="rev-container">
                <div class="rev-head">
                    <h2>{{ ucfirst($review->purchase->user->name) }}<span>@lang(':age años', ['age' => $review->purchase->present()->extraAge]). {{ ucfirst($review->purchase->present()->extraCity) }} ({{ ucfirst($review->purchase->present()->extraCountry) }})</span></h2>
                    <div class="rgt">
                        {{ $review->created_at->format('d / m / Y') }}
                        <div>
                            <h3>@lang('Valoración')
                                <span class="hearts">
                                    @for ( $j = 1; $j <= $one_rating->maximo; $j++ )
                                        <img class="rating-img {{ $review->avg >= $j ? 'full' : 'empty' }} {{ $review->avg >= $j ? 'original-full' : 'original-empty' }}"
                                             src="{{ $review->avg >= $j ? $one_rating->icono_full->get_thumbnail_url('thumbnail') : $one_rating->icono_empty->get_thumbnail_url('thumbnail') }}">
                                    @endfor
                                </span>
                                <span class="mark">{{ round($review->avg, 1) }} / {{ $one_rating->maximo }}</span>
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="rev-content">
                    @if ($review->like)
                        <img class="likes" src="{{ asset($_front . 'images/icons/like-me-ha-gustado.svg') }}"> <h4>@lang('Me ha gustado')</h4>
                        <p>{!! $review->like !!}</p>
                    @endif

                    @if ($review->improve)
                        <img class="likes" src="{{ asset($_front . 'images/icons/llave-inglesa-a-mejorar.svg') }}"> <h4>@lang('A mejorar')</h4>
                        <p>{!! $review->improve !!}</p>
                    @endif

                    @foreach($ratings as $rating)<div class="ret-ratings">
                            <dl class="dl-horizontal">
                                <dt>{{ $rating->name }}:</dt><dd>
                                    @for ( $j = 1; $j <= $rating->maximo; $j++ )
                                        @php($revRating = $review->decodedRatings->where('rating_id', $rating->id)->first())
                                        <img class="rating-img {{ $revRating->value >= $j ? 'full' : 'empty' }} {{ $revRating->value >= $j ? 'original-full' : 'original-empty' }}"
                                             src="{{ $revRating->value >= $j ? $rating->icono_full->get_thumbnail_url('thumbnail') : $rating->icono_empty->get_thumbnail_url('thumbnail') }}">
                                    @endfor
                                </dd>
                            </dl>
                        </div>@endforeach
                </div>
            </div>


        @endforeach
    </div>
@endif

@if ((!$reviews || !$reviews->count() || $reviews->count() < 4) && isset($comments) && $comments->count())
    <div class="white-box-lft white-box-tit">
        <h2>{{ $reviews && $reviews->count() ? __('Más comentarios') : __('Comentarios') }}</h2>

        <!-- COMENTARIOS -->
        @include('Front::comments.single-comments',['params' => ['post_id' => $item->id, 'post_url' => $item->get_url(), 'comments' => $comments], 'is_retiro' => true])
    </div>
@endif