<li class="item">
    <div id="{{ $retiro->id }}" data-post-type="{{ $retiro->type }}" class="obr-pad">
        <a href="{{ $retiro->get_url() }}">
            <div class="obr-img">
                @php($galeria = $retiro->get_field('galeria'))
                @if ($galeria && count($galeria) > 0)
                    <img src="{{ reset($galeria)->get_thumbnail_url('medium') }}" alt="">
                @endif

                @if (isFavorite($retiro->id))
                    <span class="pin clear">
                        <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                    </span>
                @else
                    <span class="pin">
                        <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                        <div class="tooltip"><span class="tooltiptext">@Lang('ANADIR A LA LISTA')</span> </div>
                    </span>
                @endif
            </div>
            <div class="obr-cnt">
                <h1>{{ $retiro->title }}</h1>
                <ul class="date-icon"> 
                    @if ( $allDies = obtenirProximesDates($retiro) )
                        @php($dies = $allDies->first())
                        @php( $primerDia = ($dies instanceof Illuminate\Support\Collection && $dies->isNotEmpty()) ? $dies->first() : $allDies->first() )
                        @php( $ultimDia = ($dies instanceof Illuminate\Support\Collection && $dies->isNotEmpty()) ? $dies->last() : $allDies->last() )

                        @if ($primerDia && $ultimDia)
                            <li class="date">{{ $primerDia->format('d') }} {{ $primerDia->format('M') }}. al {{ $ultimDia->format('d') }} {{ $ultimDia->format('M') }}. de {{ $ultimDia->format('Y') }} @if ($allDies->count() > 1)@lang('y otras fechas')@endif</li>
                        @endif
                    @endif

                    @php($alojamiento = $retiro->get_field('alojamiento'))
                    @if( $alojamiento && $localizacion = $alojamiento->get_field('localizacion') )
                        <li class="loc" style="padding-left: 30px">{{ print_location($localizacion, ['locality', 'administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'country']) }}</li>
                    @endif
                </ul>
                {!! get_excerpt($retiro->description, 20) !!}
            </div>
            <div class="price">
                <div class="p-lft">
                    <div class="rating">
                        <!-- VALORACIONES -->
                        @include('Front::partials.reviews-avg-rating', ['retObj' => $retiro])
                    </div>
                </div>
                @php($oferta = obtenirOfertaBarata($retiro))
                @if ($oferta['precio']['value'] != '')
                <div class="p-rgt">
                    <h2>{{ $oferta['precio']['value'] }} <i class="fa fa-eur" aria-hidden="true"></i></h2>
                </div>
                @endif
            </div>
        </a>
    </div>
</li>