<header class="logo-head">
    <div class="main-menu">
        <div class="row">
            <div class="logo">
                <a href="{{ route('index') }}" class="logo-dsk">
                    <img src="{{ asset($_front.'images/logo-encuentra-tu-retiro.svg')}}" alt="ENCUENTRA TU RETIRO" title="ENCUENTRA TU RETIRO">
                </a>
                <a href="{{ route('index') }}" class="real-logo-mbl">
                    <img src="{{ asset($_front.'images/logo-encuentra-tu-retiro.svg') }}" alt="ENCUENTRA TU RETIRO" title="ENCUENTRA TU RETIRO">
                </a>
            </div>
            <div class="head-rgt">
                <a href="javascript:void(0);" class="logo-mbl">
                    <span class="menu-box">
                        <span class="menu-box-inner"></span>
                    </span>
                </a>
                <ul class="list-links">
                    <li><a href="{{ get_archive_link('retiro') }}">@lang('Retiros') <img style="height: 22px;width: 22px;" src="{{ asset($_front.'images/icons/icona-etr-persona.svg') }}" alt=""></a></li>
                    @if (!auth()->check())
                        <li>
                            <div class="perfil quienes-somos">
                                <span>@lang('Quiénes somos') <img style="height: 6px; width: auto" src="{{ asset($_front.'images/select-arrw-yellow.png') }}" alt=""></span>
                                <div class="links five-links">
                                    <ul>
                                        <li><a href="{{ get_page_url('nuestro-proyecto') }}">@lang('Nuestro proyecto')</a></li>
                                        <li><a href="{{ get_page_url('quienes-somos') }}">@lang('Quiénes somos')</a></li>
                                        <li><a href="{{ get_page_url('equipo') }}">@lang('Equipo')</a></li>
                                        <li><a href="{{ get_page_url('plantamos-un-arbol') }}">@lang('Plantamos un árbol')</a></li>
                                        <li><a href="{{ get_page_url('dharmas') }}">@lang('Dharmas')</a></li>
                                        <li><a href="{{ get_page_url('contactar') }}">@lang('Contacto')</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    @endif

                    @if (!auth()->check() || auth()->user()->isUser())
                        <li><a href="{{ get_page_url('regalo-retiro') }}">@lang('Regala Retiro') <img style="height: 22px;width: 22px;" src="{{ asset($_front.'images/regalo.svg') }}" alt=""></a></li>
                    @endif

                    @if (Auth::check())
                        @if (auth()->user()->role == 'Organizador')
                            <li class="pushpin"><a href="{{ get_page_url('espacios-y-lugares-de-retiro') }}">@lang('Espacios') <img class="org-home-img" src="{{ asset($_front.'images/icons/home.png') }}" alt=""></a></li>
                        @else
                            @if ($user_favorites['totalRetiros'] > 0)
                                <li class="pushpin"><a href="{{ route('perfil.favoritos') }}">@lang('Mi lista') <img src="{{ asset($_front.'images/ico-pin.svg') }}" alt=""></a></li>
                            @endif
                            @if (auth()->user()->isUser())
                                <li class="dollar"><a href="{{ route('perfil.dharmas') }}">@lang('Dharmas') <span>{{ !empty(Auth::user()->dharmas) && Auth::user()->dharmas > 0 ? round(Auth::user()->dharmas, 2) : 0 }}</span><img src="{{ asset($_front.'images/Dharmas-icon.svg') }}" alt=""></a></li>
                            @endif
                        @endif
                    @else
                        @if ($user_favorites['totalRetiros'] > 0)
                            <li class="pushpin"><a href="{{ route('perfil.favoritos') }}">@lang('Mi lista') <img src="{{ asset($_front.'images/ico-pin.svg') }}" alt=""></a></li>
                        @endif
                    @endif
                </ul>

                {{--<div class="lang_select">
                    <div class="selet selet-lang">
                        <select class="select_box">
                            <option>ES</option>
                        </select>
                    </div>
                    <div id="currency-select-desk" class="selet">
                        <select class="select_box currency_select">
                            @foreach($monedas as $moneda)
                                <option @if ($moneda->id == $currentMoneda->id)selected @endif value="{{ $moneda->id }}">{{ cut_text($moneda->title, 26) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>--}}

                <div class="hd-links">
                    @if ( !Auth::check() )
                        <ul class="user">
                            <li><a id="login-popup-btn" class="fancybox1 show" href="#login">@lang('Inicio sesión')</a></li>
                            <li class="register-head-btn"><a id="register-popup-btn" class="fancybox1 show" href="tel:+34972664640"><div class="phone_icon"><?php include(public_path($_front.'images/phone.svg')) ?></div>&nbsp;<span class="phone-prefix">+34</span> 972 664 640</a></li>
                        </ul>
                    @else
                        @if (auth()->user()->isUser())
                            <ul class="user">
                                <li class="register-head-btn"><a id="register-popup-btn" class="fancybox1 show" href="tel:+34972664640"><div class="phone_icon"><?php include(public_path($_front.'images/phone.svg')) ?></div>&nbsp;<span class="phone-prefix">+34</span> 972 664 640</a></li>
                            </ul>
                        @endif

                        @if (!in_array(\Request::route()->getName(), ['perfil.register-organizer']))
                        <div class="perfil">
                            <img src="{{ Auth::user()->get_avatar_url() }}" alt="">
                            <h2>{{ Auth::user()->name }} <span class="head-user-lastname">{{ ' ' . Auth::user()->lastname }}</span></h2>
                            <div class="links @if (auth()->check() && auth()->user()->role == 'Organizador')five-links @endif">
                                <ul>
                                    @if ( !Auth::user()->isAdmin() )
                                        {{-- Menu de No Admin --}}
                                        @if (auth()->check() && auth()->user()->role == 'Organizador')
                                            <li><a href="{{ route('admin') }}">@lang('Mi Panel')</a></li>
                                            <li><a href="{{ get_page_url('espacios-y-lugares-de-retiro') }}">@lang('Espacios')</a></li>
                                            <li><a href="{{ get_page_url('terminos-y-condiciones-del-servicio-a-organizadores') }}">@lang('Condiciones')</a></li>
                                            <li><a href="{{ get_page_url('ayuda-organizadores') }}">@lang('Ayuda')</a></li>
                                        @else
                                            <li><a href="{{ route('perfil.show') }}">@lang('Mi Cuenta')</a></li>
                                            <li><a href="{{ get_page_url('ayuda-usuarios') }}">@lang('Ayuda')</a></li>
                                            <li><a href="{{ route('perfil.mensajes') }}">@lang('Mensajes')</a></li>
                                        @endif
                                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('Logout')</a></li>
                                    @else
                                        {{-- Menu de Admin --}}
                                        <li><a href="{{ !in_array(auth()->user()->role, array('admin', 'Organizador')) ? route('perfil.show') : route('admin') }}">@lang('Mi Perfil')</a></li>
                                        <li><a href="{{ route('perfil.reservas') }}">@lang('Reservas')</a></li>
                                        <li><a href="{{ route('perfil.mensajes') }}">@lang('Mensajes')</a></li>
                                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('Logout')</a></li>
                                    @endif
                                </ul>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                            </div>
                        </div>
                        @endif
                    @endif
                </div>
                <div class="menu"></div>
            </div>
            <div class="mbl-menu">
                <ul>
                    <li><a href="{{ get_archive_link('retiro') }}">@Lang('Retiros')</a></li>
                    <li> <a href="javascript:void(0);" class="lang-rs">@lang('Quiénes somos')</a>
                        <div class="rs-div">
                            <ul>
                                <li><a href="{{ get_page_url('nuestro-proyecto') }}">@lang('Nuestro proyecto')</a></li>
                                <li><a href="{{ get_page_url('quienes-somos') }}">@lang('Quiénes somos')</a></li>
                                <li><a href="{{ get_page_url('equipo') }}">@lang('Equipo')</a></li>
                                <li><a href="{{ get_page_url('plantamos-un-arbol') }}">@lang('Plantamos un árbol')</a></li>
                                <li><a href="{{ get_page_url('dharmas') }}">@lang('Dharmas')</a></li>
                                <li><a href="{{ get_page_url('contactar') }}">@lang('Contacto')</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="{{ get_page_url('regalo-retiro') }}">@lang('Regala Retiro')</a></li>
                    <li><a href="{{ get_page_url('espacios-y-lugares-de-retiro') }}">@Lang('Espacios para organizadores')</a></li>
                    {{--<li><a href="{{ get_page_url('voluntariado-trabajo-retiro') }}">@Lang('Voluntariados')</a></li>--}}

                    {{-- Menu de No Admin --}}
                    @if (auth()->check())
                        @if ( !auth()->user()->isAdmin() )
                            @if (auth()->user()->role == 'Organizador')
                                <li class="hd-mbl-links"><a href="{{ route('admin') }}">@lang('Mi Panel')</a></li>
                                <li class="hd-mbl-links"><a href="{{ get_page_url('terminos-y-condiciones-del-servicio-a-organizadores') }}">@lang('Condiciones')</a></li>
                                <li class="hd-mbl-links"><a href="{{ get_page_url('ayuda-organizadores') }}">@lang('Ayuda')</a></li>
                            @else
                                <li class="hd-mbl-links"><a href="{{ route('perfil.show') }}">@lang('Mi Cuenta')</a></li>
                                <li class="hd-mbl-links"><a href="{{ get_page_url('ayuda-usuarios') }}">@lang('Ayuda')</a></li>
                                <li class="hd-mbl-links"><a href="{{ route('perfil.mensajes') }}">@lang('Mensajes')</a></li>
                            @endif
                        @else
                            <li><a href="{{ !in_array(auth()->user()->role, array('admin', 'Organizador')) ? route('perfil.show') : route('admin') }}">@lang('Mi Perfil')</a></li>
                            <li><a href="{{ route('perfil.reservas') }}">@lang('Reservas')</a></li>
                            <li><a href="{{ route('perfil.mensajes') }}">@lang('Mensajes')</a></li>
                        @endif
                    @else
                        <li><a id="login-popup-btn" class="fancybox1 show" href="#login">@lang('Inicio sesión')</a></li>
                        <li><a class="fancybox1 show" href="#register">@lang('Registro')</a></li>
                    @endif

                    {{--<li> <a href="javascript:void(0);" class="lang-mbl">ES</a>
                        <div class="lang-div">
                            <ul>
                                <li>
                                    <label class="control control-radio">
                                        Español - ES
                                        <input type="radio" name="radio_lang" checked="checked" />
                                        <div class="control_indicator"></div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li> <a href="javascript:void(0);" class="lang-rs">{{ $currentMoneda->get_field('iso-code') }}</a>
                        <div class="rs-div">
                            <ul>
                                @foreach($monedas as $moneda)
                                    <li>
                                        <label class="control control-radio">
                                            {{ $moneda->title }}
                                            <input type="radio" name="radio_currency" class="currency_select" value="{{ $moneda->id }}" @if ($moneda->id == $currentMoneda->id)checked="checked"@endif />
                                            <div class="control_indicator"></div>
                                        </label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>--}}


                    @if (Auth::check())
                        @if (auth()->user()->isUser())
                            <li><a href="{{ route('perfil.dharmas') }}"><img src="{{ asset($_front.'images/dollar-coin-stack-grey.svg') }}" title="">Dharmas <span>{{ Auth::user()->dharmas }}</span></a></li>
                        @endif
                    {{--@else
                        <li><a href="{{ get_page_url('dharmas') }}"><img src="{{ asset($_front.'images/dollar-coin-stack-grey.svg') }}" title="">Dharmas</a></li>--}}
                    @endif

                    @if ($user_favorites['totalRetiros'] > 0)
                        <li><a href="{{ route('perfil.favoritos') }}"><img src="{{ asset($_front.'images/pushpin-grey.svg') }}" title=""> @lang('Mi lista')</a></li>
                    @endif

                    <li><a href="tel:+34972664640">@lang('Llámanos') <span class="phone-prefix">+34</span> 972 664 640</a></li>

                    @if (Auth::check())
                        <li class="hd-mbl-links"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('Logout')</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</header>
<div class="sticky-header"></div>
<!-- banner starts -->

@if (!auth()->user())
    @includeIf('Front::partials.popup')
    @includeIf('Front::partials.register-popup')
@endif