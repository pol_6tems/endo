@if ($equipo->count())
<div class="equipo">
    <div class="header">
        <h4>@lang('Estamos encantados de ayudarte')</h4>
    </div>
    <div class="body">
        <h2><img style="margin-right: 10px;" src="{{ asset($_front.'images/phone.svg') }}" alt=""><a href="tel:+34972664640">@lang('Llámanos +34 <span>972 664 640</span>')</a></h2>
        <div id="equipo-carousel" class="owl-carousel equipo-carousel">
            @foreach($equipo as $equipo)
            @php($name = explode(" ", $equipo->title))
            <div class="item">
                <div class="image">
                    @if ($img = $equipo->get_field('perfil'))
                        <img src="{{ $img->get_thumbnail_url() }}" alt="">
                    @endif
                </div>
                <h3>{{ ucfirst(strtolower($name[0])) }}</h3>
                <div class="frase">&quot;{{ $equipo->get_field('frase') }}&quot;</div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@section('scripts1')
    @parent
    <script>
        $(function() {
            $('.equipo-carousel').owlCarousel({
                goToFirstSpeed :1000,
                loop:true,
                items:1,
                margin:15,
                autoplay:true,
                autoplayTimeout:5500,
                autoplayHoverPause:true,		
                nav : true,
                dots : true,
                navText: ["<i class=\"arrow left\"></i>","<i class=\"arrow right\"></i>"]
            });
        })
    </script>
@endsection
@endif