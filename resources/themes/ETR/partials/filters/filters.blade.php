@php
    $advanced = [];
    foreach($objects as $key => $object) {
        if (isset($object['advanced']) && $object['advanced']) {
            $advanced[$key] = $object;
            unset($objects[$key]);
        }
    }

    
    $firstArrivalDate = \Carbon\Carbon::today();
    $firstArrivalDate->day = 15;
    $lastArrivalDate = clone $firstArrivalDate;
    $lastArrivalDate->addYear();
    $llegadaString = '';

    $arrivalDates = collect();

    while ($firstArrivalDate < $lastArrivalDate) {
        $arrivalDates->push(Date::parse($firstArrivalDate));
        $firstArrivalDate->addMonth();
    }
    
    $filter = request()->query('filter', []);

    $duraciones = get_posts('retiro-duracion');
    $duracionYLlegadaString = '';
    $llegadaString = '';

    $currentDuraciones = $duraciones->filter(function($value) use ($filter) {
        return in_array($value->post_name, $filter);
    });
    
    if ($currentDuraciones->count()) {
        foreach ($currentDuraciones as $currentDuracion) {
            if ($currentDuracion != $currentDuraciones->first()) {
                $duracionYLlegadaString .= ', ';
            }

            $duracionYLlegadaString .= $currentDuracion->title;
        }
    }
    
    if (request('date')) {
        $date = Date::parse(request('date'));
        $range = request('range');

        if ($currentDuraciones->count()) {
            $duracionYLlegadaString .= ' | ';
        }

        if ($date->day == 15 && $range == 15) {
            $llegadaString = $date->format('F Y');
            $duracionYLlegadaString .= $llegadaString;
        } else {
            $duracionYLlegadaString .= $date->format('d/m/Y');
        }
    }
@endphp

<div class="acc-menu">
    <div class="accordion_example1">
        @php($metas = array())
        @foreach($objects as $title => $objecte)
            @if (!isset($objecte['objs']) || $objecte['objs']->count())
                @if ($desplegable = (isset($objecte['desplegable']) ? $objecte['desplegable'] : false))
                    <div class="accordion_in {{ (isset($objecte['obert']) && $objecte['obert']) ? 'acc_active' : '' }}">
                        <div class="acc_head">
                            @if ( !isset($objecte['title']) || isset($objecte['title']) && $objecte['title'])
                                {{ __($title) }}
                            @endif
                        </div>
                        <div class="acc_content">
                        @else
                            <div class="white-box-lft">
                                @if ( !isset($objecte['title']) || isset($objecte['title']) && $objecte['title'])
                                    <h5>{{ __($title) }}</h5>
                                @endif
                                @endif

                                {{-- Mostrar Localitzcacio / Preus --}}
                                @if(isset($objecte['type']) && $objecte['type'] == 'price')
                                    @includeIf('Front::partials.filters.price', [
                                        'title' => $title,
                                        'objecte' => $objecte,
                                        'mobile' => $mobile,
                                    ])
                                @elseif (isset($objecte['type']) && $objecte['type'] == 'location')
                                    @includeIf('Front::partials.filters.location', ['mobile' => $mobile])
                                @else
                                    @includeIf('Front::partials.filters.checkboxes', [
                                        'title' => $title,
                                        'objecte' => $objecte,
                                        'mobile' => $mobile,
                                    ])
                                @endif

                            @if ($desplegable)
                            </div>
                        </div>
                        @else
                    </div>
                @endif
            @endif
        @endforeach
        @if($post_object == 'retiro')
            <div class="accordion_in acc_active">
                <div class="acc_head">@lang('Fechas')</div>
                <div class="acc_content">
                    <div class="calendr">
                        <div class="datepicker-here @if ($mobile)mobile @endif"></div>
                    </div>

                    <ul style="float: left;margin-top: 20px;">
                        @foreach($arrivalDates as $arrivalDate)
                            {{-- <li class="arrival-date {{ $llegadaString == $arrivalDate->format('F Y') ? 'selected' : '' }}" data-date="{{ $arrivalDate->toDateString() }}">{{ $arrivalDate->format('F Y') }} {!! $llegadaString == $arrivalDate->format('F Y') ? '<span class="cross"></span>' : '' !!}</li> --}}
                            <li class="arrival-date {{ $llegadaString == $arrivalDate->format('F Y') ? 'selected' : '' }}">
                                <div class="input-container">
                                    <input
                                        class="post-date"
                                        type="checkbox"
                                        data-date="{{ $arrivalDate->toDateString() }}"
                                        {{ $llegadaString == $arrivalDate->format('F Y') ? 'selected' : '' }}
                                    />

                                    <label>
                                        {{ $arrivalDate->format('F Y') }}
                                    </label>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        @if (count($advanced))
            <button class="filter btn-filter" onclick="@if (!$mobile)$('.adv').fadeToggle(); @else var me = $(this);$('.adv').fadeToggle('fast', function() {$('.filter-menu').stop(true, false).animate({scrollTop: me.offset().top - me.offsetParent().offset().top}, 300);});@endif">
                @lang('Filtros Avanzados')
                <img style="margin-right: 5px;" src="{{ asset($_front.'images/icons/filter-results.svg') }}" alt="">
            </button>
        
            <div id="adv-filters">
                @foreach($advanced as $title => $objecte)
                    @if ($desplegable = (isset($objecte['desplegable']) ? $objecte['desplegable'] : false))
                    <div class="accordion_in adv">
                        <div class="acc_head">
                            @if ( !isset($objecte['title']) || isset($objecte['title']) && $objecte['title'])
                                {{ __($title) }}
                            @endif
                        </div>
                        <div class="acc_content">
                        @else
                            <div class="white-box-lft {{ (!$desplegable) ? 'adv' : '' }} ">
                                @if ( !isset($objecte['title']) || isset($objecte['title']) && $objecte['title'])
                                    <h5>{{ __($title) }}</h5>
                                @endif
                                @endif

                                {{-- Mostrar Localitzcacio / Preus --}}
                                @if(isset($objecte['type']) && $objecte['type'] == 'price')
                                    @includeIf('Front::partials.filters.price', [
                                        'title' => $title,
                                        'objecte' => $objecte,
                                        'mobile' => $mobile,
                                    ])
                                @elseif (isset($objecte['type']) && $objecte['type'] == 'location')
                                    @includeIf('Front::partials.filters.location', ['mobile' => $mobile])
                                @else
                                    @includeIf('Front::partials.filters.checkboxes', [
                                        'title' => $title,
                                        'objecte' => $objecte,
                                        'mobile' => $mobile,
                                    ])
                                @endif

                            @if ($desplegable)
                            </div>
                        </div>
                        @else
                    </div>
                    @endif
                @endforeach
            </div>
        @endif
    </div>
</div>