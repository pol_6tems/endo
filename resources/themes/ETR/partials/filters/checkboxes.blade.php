<ul class="cat-list">
@php($filter = request()->query())
@php($filter = isset($filter['filter']) ? $filter['filter'] : [])

@php($postTypes = isset($objecte['objs']) ? $objecte['objs'] : null)

@if (!$postTypes || !$postTypes->count())
    @php($postTypes = get_posts($objecte['post_type']))
@endif

@foreach($postTypes as $item)
    @php($checked = ( !empty($filter) && in_array($item->post_name, $filter)) ? 'checked' : '')
    @if ($checked == 'checked')
        @php($metas[] = [$objecte['custom_field'], 'like', "%$item->id%"])
    @endif
    <li class="{{ ($mobile) ? 'mobile' : '' }}">
        <div class="input-container">
            <input
                id="{{ $item->id }}-{{ ($mobile) ? 'mob' : 'dsk'}}"
                class="post {{ ($mobile) ? 'mobile' : '' }}"
                type="checkbox"
                data-id="{{ $item->id }}"
                data-cf="{{ $objecte['custom_field'] }}"
                data-post_name="{{ $item->post_name }}"
                data-slug="{{ str_slug($item->title, '_') }}"
                data-title="{{ $item->title }}"
                name="{{ $title }}[]" {{ ( $checked ) ? 'checked' : '' }}
            />

            <label for="{{ $item->id }}-{{ ($mobile) ? 'mob' : 'dsk'}}">
                @if ($objecte['custom_field'] == 'tipos-de-retiro' && $item->media())
                    <img width="20" height="20" style="margin-right: 10px;" src="{{ $item->media()->get_thumbnail_url("medium_large") }}" />
                @endif
                {{ $item->title }}
            </label>
        </div>
    </li>
    @endforeach
</ul>