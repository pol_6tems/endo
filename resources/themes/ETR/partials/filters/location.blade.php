@php
$countries = cacheable(\App\Modules\EncuentraTuRetiro\Repositories\PostsRepository::class)->get_active_countries();
@endphp

@foreach($countries as $code => $regions)
    @if ($regions->count())
    @php($current = $regions->flatten()->first())
    <div class="input-container">
        <div class="item {{ ($mobile) ? 'mobile' : '' }}">
            <input
                id="pais-{{ $current->id }}-{{ ($mobile) ? 'mob' : 'dsk' }}"
                class="post location pais {{ ($mobile) ? 'mobile' : '' }}"
                type="checkbox"
                name="{{ $current->code_pais }}"
                data-slug="{{ str_slug($current->pais, '_') }}"
                data-post_name="{{ $current->code_pais }}"
                data-id="{{ $current->code_pais }}"
                data-title="{{ $current->pais }}"
            />
    
            <label for="pais-{{ $current->id }}-{{ ($mobile) ? 'mob' : 'dsk'}}">
                {{ $current->pais }}
            </label>
            @if (count($regions) > 0)
                <a class="desplegable {{ ($current->code_pais == 'ES') ? 'open' : '' }}" href="javascript:void(0)"><img class="subregion" src="{{ asset($_front.'images/select-arrw-yellow.png') }}" alt=""></a>
            @endif
        </div>
        <ul class="child {{ ($current->code_pais == 'ES') ? 'open' : '' }}">
            @php($regions = $regions->sortKeys())
            @foreach($regions as $province => $provinces)
                @if ($region = $provinces->flatten()->first())
                    <li class="mobile">
                        <div class="input-container">
                            <div class="item">
                                <input
                                    id="region-{{ $region->id }}-{{ ($mobile) ? 'mob' : 'dsk' }}"
                                    class="post location comunidad {{ ($mobile) ? 'mobile' : '' }}"
                                    type="checkbox"
                                    name="{{ $region->code_region }}"
                                    data-post_name="{{ $region->code_region }}"
                                    data-slug="{{ str_slug($region->region, '_') }}"
                                    data-id="{{ $region->code_region }}"
                                    data-title="{{ $region->region }}"
                                />
                                <label for="region-{{ $region->id }}-{{ ($mobile) ? 'mob' : 'dsk'}}">
                                    {{ $region->region }}
                                </label>
                                @if ($provinces->count() > 1 || (!is_null($region->code_provincia) && !is_null($region->provincia)))
                                    <a class="desplegable" href="javascript:void(0)"><img class="subregion" src="{{ asset($_front.'images/select-arrw-yellow.png') }}" alt=""></a>
                                @endif
                            </div>
                            <ul class="child provincia">
                                @foreach($provinces as $key => $province)
                                    @php($province = $province->first())
                                    @if($key != "")
                                        <li class="mobile">
                                            <div class="input-container" style="margin-bottom: 10px;">
                                                <input
                                                    id="province-{{ $province->id }}-{{ ($mobile) ? 'mob' : 'dsk' }}"
                                                    class="post location province {{ ($mobile) ? 'mobile' : '' }}"
                                                    type="checkbox"
                                                    name="{{ $province->code_provincia }}"
                                                    data-post_name="{{ $province->code_provincia }}"
                                                    data-id="{{ $region->code_provincia }}"
                                                    data-slug="{{ str_slug($province->provincia, '_') }}"
                                                    data-title="{{ $province->provincia }}"
                                                />
                                                <label for="province-{{ $province->id }}-{{ ($mobile) ? 'mob' : 'dsk'}}">
                                                    {{ $province->provincia }}
                                                </label>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
    @endif
@endforeach