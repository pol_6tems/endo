@php($subtitol = $item->get_field('subtitulo'))
@php($categoria = $item->get_field('categoria'))
<li><a href="{{ $item->get_url() }}">
        <div class="img-div">
            @if ($item->translate()->media)
                <img src="{{ $item->translate()->media->get_thumbnail_url() }}" alt="">
            @else
                <img src="" alt="">
            @endif
            <div class="list-cont">
                <div>
                    <h2>{{ $item->translate()->title }}</h2>
                    <h3>{{ $subtitol }}</h3>
                    <h4><img src="{{ asset($_front.'images/heart-white.png') }}" alt=""><span>{{ round(do_action('get_ratings', $item->id)['avg'], 0) }}</span> @if ($item->author) {{ $item->author->name }} @endif</h4>
                </div>
            </div>
        </div>
        @if ($categoria)
            <span class="exp-btn">{{ $categoria->translate()->title }}</span>
        @endif
    </a>
</li>