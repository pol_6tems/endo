@if (isset($post_type) && $post_type == 'retiro' && !isset($post))
    <div style="font-style: italic">
        Antes de introducir un NUEVO RETIRO se debe haber añadido previamente las fichas correspondientes al:

        <ul>
            <li>Alojamiento</li>
            <li>Habitaciones</li>
            <li>Facilitadores</li>
        </ul>
    </div>
@endif