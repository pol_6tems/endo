@php
    $query = request()->query();
    $locations = isset($query['loc']) ? $query['loc'] : [];
    
    // Objecte auxiliar per obtenir els custom fields selection
    $retiroObj = \App\Post::with(['cfGroups.customFields'])->where('type', 'retiro')->get()->first();

    $tags = [];
    // Obtenir les tags del filtre de continguts
    $countries = get_active_countries($locations);

    foreach($countries as $pais => $country) {
        // Pais
        $pais = $country->flatten()->first();
        if (in_array($pais->code_pais, $locations)) $tags["pais"][$pais->code_pais] = $pais;
        foreach($country as $region) {
            // Region
            $community = $region->flatten()->first();
            if (in_array($community->code_region, $locations)) $tags["region"][$community->code_region] = $region;
            foreach($region as $province) {
                // Province
                $province = $province->first();
                if (in_array($province->code_provincia, $locations)) $tags["provincia"][$province->code_provincia] = $province;
            }
        }
    }

    $titles = [
        'tipos-retiro' => 'Tipos',
        'retiro-categoria' => 'Categorías',
        'beneficio' => 'Beneficios',
        'tipos-de-yoga' => 'Yoga',
        'tipo-de-meditacion' => 'Meditación' 
    ];
@endphp
<div class="filters-container es-pad">
    <div class="out-filters filter-container filters {{ count($tags) > 0 || count($filter) > 0 ? '' : 'hide'}}">
        @php($flattenTypes = $types->flatten())
        @php($allcountries = $countries->flatten())

        @foreach($filter as $filterItem)
        @php($filterType = $flattenTypes->filter(function ($type) use ($filterItem) {
                return $type->post_name == $filterItem;
            })->first())
            @if ($filterType)
                <div class="tag" data-cf="{{ $filterType->title }}" data-filter="{{ $filterType->post_name }}">{{ $filterType->title }}<span>x</span></div>
            @endif
        @endforeach
        @foreach($tags as $type => $tag)
            @foreach($tag as $code => $t)
                @if ($location = $allcountries->firstWhere('code_pais', $code) ?? $allcountries->firstWhere('code_region', $code) ?? $allcountries->firstWhere('code_provincia', $code))
                    <div class="tag" data-filter="{{ $code }}">{{ $location->$type }}<span>x</span></div>
                @endif
            @endforeach
        @endforeach
    </div>
</div>
<script>var filterURL = '{{ route('filters') }}';</script>
<form id="formbuscador" action="{{ route('buscador') }}" method="POST" @if (isset($isHome) && $isHome)data-is-home="yes"@endif>
    @csrf
    <input type="hidden" name="post_type" value="retiro">
    <div class="es-pad">
        <ul>
            <li class="ser">
                <input id="buscador" type="text" placeholder="@lang('¿Qué estás buscando?')" class="text" autocomplete="off" onClick="$('li.date .search-modal > .search-modal-wrapper').hide();">
                <div class="buscador-results custom_scrollbar">
                    <div class="close-filters">
                        <img src="{{ asset($_front.'images/group-16.svg') }}" />
                    </div>
                    <div class="filter-container filters {{ count($tags) > 0 || count($filter) > 0 ? '' : 'hide'}}">
                        @php($flattenTypes = $types->flatten())
                        @foreach($filter as $filterItem)
                            @php($filterType = $flattenTypes->filter(function ($type) use ($filterItem) {
                                return $type->post_name == $filterItem;
                            })->first())
                            @if ($filterType)
                                <div class="tag" data-cf="{{ $filterType->title }}" data-filter="{{ $filterType->post_name }}">{{ $filterType->title }}<span>x</span></div>
                            @endif
                        @endforeach
                        @foreach($tags as $type => $tag)
                            @foreach($tag as $code => $t)
                                @if ($location = $allcountries->firstWhere('code_pais', $code) ?? $allcountries->firstWhere('code_region', $code) ?? $allcountries->firstWhere('code_provincia', $code))
                                    <div class="tag" data-filter="{{ $code }}">{{ $location->$type }}<span>x</span></div>
                                @endif
                            @endforeach
                        @endforeach
                    </div>
                    <div class="busquedas-populares">
                        <h5>@lang('Búsquedas Populares')</h5>
                        <ul>
                            @foreach($popularSearches as $popularSearch)
                                @if ($popularSearch)
                                    @if ($selection = $retiroObj->getSelectionsCfFromPost($popularSearch))
                                        <li data-cf="{{ $selection->name }}" data-filter="{{ $popularSearch->post_name }}" data-value="{{ $popularSearch->title }}" data-buscador="{{ $popularSearch->id }}">{{ $popularSearch->title }}</li>
                                    @endif
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="search-accordion"> 
                        <!-- Section 1 -->
                        <div class="accordion_in">
                            <div class="acc_head">@lang('Regiones')</div>
                            <div class="acc_content">
                                <ul>
                                    @foreach($countries as $regions)
                                        {{-- Pais --}}
                                        @php($current = $regions->flatten()->first())
                                        <li data-filter="{{ $current->code_pais }}" data-value="{{ $current->pais }}" data-buscador="{{ 'pais-' . $current->code_pais }}">{{ $current->pais }}</li>
                                        @foreach($regions as $provinces)
                                            {{-- Region --}}
                                            @php($region = $provinces->flatten()->first())
                                            <li style="margin-left: 15px;" data-filter="{{ $region->code_region }}" data-value="{{ $region->region }}" data-buscador="{{ 'region-' . $region->code_region }}">{{ $region->region }}</li>
                                            @foreach($provinces as $province)
                                                {{-- Province --}}
                                                @php($province = $province->first())
                                                @if ($province->code_provincia != '' && $province->provincia)
                                                    <li style="margin-left: 30px;" data-filter="{{ $province->code_provincia }}" data-value="{{ $province->provincia }}" data-buscador="{{ 'provincia-' . $province->code_provincia }}">{{ $province->provincia }}</li>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                    </li>
                                </ul>
                            </div>
                        </div>

                        @foreach($types as $key => $posts)
                            <!-- Section -->
                            <div class="accordion_in">
                                <div class="acc_head">{{ $titles[$key] }}</div>
                                <div class="acc_content">
                                    <ul>
                                    @foreach($posts as $item)
                                        @php($selection = $retiroObj->getSelectionsCfFromPost($item))
                                        <li data-cf="{{ $selection->name }}" data-filter="{{ $item->post_name }}" data-value="{{ $item->title }}" data-buscador="{{ $item->id }}">{{ $item->title }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </li>
            <li class="date">
                <input id="duracion-y-llegada" name="duracion-y-llegada" type="text" placeholder="@lang('Duración y llegada')" value="{{ $duracionYLlegadaString }}" class="text1" autocomplete="off" onClick="$($(this).next().children()[0]).show();" readonly>
                <div class="search-modal search-modal-right calendar-modal">
                    <div class="search-modal-wrapper">
                        <div class="close-filters" onClick="$(this).parent().hide();">
                            <img src="{{ asset($_front.'images/group-16.svg') }}" />
                        </div>
                        <div class="sear-rgt duracion custom_scrollbar">
                            <div class="search-accordion">
                                <div class="accordion_in acc_active">
                                    <div class="acc_head">@lang('Duración')</div>
                                    <div class="acc_content">
                                        <ul>
                                            @foreach($duraciones as $duracion)
                                                <li class="searcher-duration {{ (in_array($duracion->post_name, $filter)) ? 'selected' : '' }}" data-cf="duracion" data-filter="{{ $duracion->post_name }}" data-value="{{ $duracion->title }}" id="{{ $duracion->id }}">{{ $duracion->title }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div class="accordion_in acc_active">
                                    <div class="acc_head">@lang('Mes de llegada')</div>
                                    <div class="acc_content">
                                        <ul>
                                            @foreach($arrivalDates as $arrivalDate)
                                                <li class="arrival-date {{ $llegadaString == $arrivalDate->format('F Y') ? 'selected' : '' }}" data-date="{{ $arrivalDate->toDateString() }}">{{ $arrivalDate->format('F Y') }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sear-calendar">
                            <select name="range" class="select_box">
                                <option value="0" @if (!request('range'))selected @endif>@lang('Fecha exacta')</option>
                                <option value="1" @if (request('range') == 1)selected @endif>@lang('1 días antes/después')</option>
                                <option value="2" @if (request('range') == 2)selected @endif>@lang('2 días antes/después')</option>
                                <option value="3" @if (request('range') == 3)selected @endif>@lang('3 días antes/después')</option>
                                <option value="7" @if (request('range') == 7)selected @endif>@lang('7 días antes/después')</option>
                                <option value="15" @if (request('range') == 15)selected @endif>@lang('15 días antes/después')</option>
                            </select>
                            <div class="calendr">
                                <div class="datepicker-here"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
{{--
            @if (isset($isHome) && $isHome)
                <li class="btn">
                    <input type="submit" value="Buscar">
                </li>
            @endif--}}
        </ul>
    </div>
</form>