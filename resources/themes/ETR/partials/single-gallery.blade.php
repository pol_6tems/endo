@php
    if (!empty($gallery)) {
        $portada = array_shift($gallery);
    }

    $altresImg = array();

    $k=0;
    if (!empty($gallery)) {
        foreach($gallery as $img) {
            if ($k > 3 && $img) {
                $altresImg[] = $img->get_thumbnail_url("medium_large");
            }
            $k++;
        }

        $gallery = array_chunk($gallery, 2);
        $col1 = current($gallery);
        $col2 = next($gallery);
    }
@endphp

@if( !empty($portada) )
<div class="load-img-lft">
    <div class="demo-container">
        <a href="{{ $portada->get_thumbnail_url("large") }}" rel="group" class="fancybox">
            <img class="blur" src="{{ $portada->get_thumbnail_url("medium_large") }}">
        </a>
    </div>
</div>
@endif

@if ( !empty($col1) )
<div class="load-img-cen">
    <ul>
        @foreach($col1 as $img)
        <li>
            <div class="demo-container">
                <a href="{{ $img->get_thumbnail_url("large") }}" rel="group" class="fancybox">
                    <img class="blur" src="{{ $img->get_thumbnail_url("medium_large") }}" alt="{{ $img->alt }}">
                </a>
            </div>
        </li>
        @endforeach
    </ul>
</div>
@endif

@if ( !empty($col2) )
<div class="load-img-cen load-img-rgt">
    <ul>
        @if (isset($col2[0]))
        <li>
            <div class="demo-container">
                <a href="{{ $col2[0]->get_thumbnail_url("large") }}" rel="group" class="fancybox">
                    <img class="blur" src="{{ $col2[0]->get_thumbnail_url("medium_large") }}" alt="{{ $col2[0]->alt }}">
                    @if (isset($post))
                        <div class="pin-ret">
                            @if (isFavorite($post->id))
                                <span class="pin clear" data-post-id="{{ $post->id }}" data-post-type="{{ $post->type }}">
                                    <img class="blur" src="{{ asset($_front . 'images/pushpin.svg') }}" alt="">
                                </span>
                            @else
                                <span class="pin" data-post-id="{{ $post->id }}" data-post-type="{{ $post->type }}">
                                    <img class="blur" src="{{ asset($_front . 'images/pushpin.svg') }}" alt="">
                                    <div class="tooltip">
                                        <span class="tooltiptext">@Lang('ANADIR A LA LISTA')</span>
                                    </div>
                                </span>
                            @endif
                        </div>
                    @endif
                </a>
            </div>
        </li>
        @endif

        @if (isset($col2[1]))
        <li>
            <div class="demo-container">
                <a href="{{ $col2[1]->get_thumbnail_url("large") }}" rel="group" class="fancybox">
                    <img class="blur" src="{{ $col2[1]->get_thumbnail_url("medium_large") }}" alt="{{ $col2[1]->alt }}">
                </a>
                @if (count($altresImg) > 0)
                    <a class="foto-btn" onclick="$('.fancybox').eq(5).trigger('click')">@Lang('Ver más fotos')</a>
                @else
                    <a class="foto-btn" onclick="$('.fancybox').eq(0).trigger('click')">@Lang('Ver fotos')</a>
                @endif
            </div>
        </li>
        @endif
    </ul>
</div>
@endif

<div class="hidden">
    @foreach($altresImg as $img)
        <a class="fancybox" rel="group" href="{{ $img }}"></a>
    @endforeach
</div>