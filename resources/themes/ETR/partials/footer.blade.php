@php($retirosArchiveUrl = \App\Post::get_archive_link('retiros'))
<section class="foot-txt">
    <div class="row1"> <img src="{{ asset($_front.'images/isotip-encuentra-tu-retiro.svg') }}">
        <a href="{{ $retirosArchiveUrl }}"><span class="footer-claim">@lang('<span>...más de :retiros retiros para crecer.</span> iEncuentra el tuyo!', ['retiros' => $activeRetiros])</span></a>
    </div>
</section>

@if (isUsersNewsletter(url()->current()))
    @includeIf('Front::partials.users-newsletter')
@elseif(isOrganizadoresNewsletter(url()->current()))
    @includeIf('Front::partials.organizadores-newsletter')
@endif

<footer>
    <div class="row1">
        <div class="foot-top">
            <ul>
                <li>
                    <span class="foot-title">@lang('Nosotros')</span>
                    <ul>
                        <li><a href="{{ get_page_url('nuestro-proyecto') }}">@lang('Nuestro proyecto')</a></li>
                        <li><a href="{{ get_page_url('quienes-somos') }}">@lang('Quiénes somos')</a></li>
                        <li><a href="{{ get_page_url('equipo') }}">@lang('Equipo')</a></li>
                        <li><a href="{{ get_page_url('plantamos-un-arbol') }}">@lang('Plantamos un árbol')</a></li>
                        <li><a href="{{ get_page_url('dharmas') }}">@lang('Dharmas')</a></li>
                        <li><a href="{{ get_page_url('contactar') }}">@lang('Contacto')</a></li>
                    </ul>
                </li>
                <li>
                    <span class="foot-title">@lang('Usuarios')</span>
                    <ul>
                        <li><a href="{{ get_page_url('que-es-un-retiro') }}">@lang('¿Qué es un retiro?')</a></li>
                        <li><a href="{{ get_page_url('ventajas') }}">@lang('Ventajas Encuentraturetiro')</a></li>
                        <li><a href="{{ get_page_url('regalo-retiro') }}">@lang('Retiro Regalo')</a></li>
                        <li><a href="{{ get_page_url('voluntariado-trabajo-retiro') }}">@lang('Voluntariado')</a></li>
                        <li><a href="{{ get_page_url('blog-cultura-retiros') }}">@lang('Blog-Cultura de los retiros')</a></li>
                        <li><a href="{{ get_post_url('ayuda-usuarios') }}">@lang('Centro de Ayuda Usuarios')</a></li>
                    </ul>
                </li>
                <li>
                    <span class="foot-title">@lang('Retiros')</span>

                    <ul>
                        <li><a href="{{ route('retiros.landing', ['filter' => 'crecimiento-personal']) }}">@lang('Retiros Crecimiento')</a></li>
                        <li><a href="{{ route('retiros.landing', ['filter' => 'yoga']) }}">@lang('Retiros Yoga')</a></li>
                        <li><a href="{{ route('retiros.landing', ['filter' => 'descanso']) }}">@lang('Retiros Descanso')</a></li>
                        <li><a href="{{ route('retiros.landing', ['filter' => 'meditacion']) }}">@lang('Retiros Meditación')</a></li>
                        <li><a href="{{ route('retiros.landing', ['filter' => 'detox']) }}">@lang('Retiros Detox')</a></li>
                        <li><a href="{{ route('retiros.landing', ['filter' => 'viaje']) }}">@lang('Viajes Conscientes')</a></li>
                    </ul>
                </li>
                <li>
                    <span class="foot-title">@lang('Organizadores')</span>
                    <ul>
                        <li><a href="{{ get_page_url('espacios-y-lugares-de-retiro') }}">@lang('Directorio Espacios')</a></li>
                        <li><a href="{{ get_page_url('promociona-tu-retiro') }}">@lang('Promociona tu Retiro')</a></li>
                        <li><a href="{{ get_page_url('promociona-tu-espacio') }}">@lang('Promociona tu Espacio')</a></li>
                        <li><a href="{{ get_page_url('voluntariado-trabajo-retiro') }}">@lang('Voluntariado')</a></li>
                        <li><a href="{{ get_page_url('contactar') }}">@lang('Contacto')</a></li>
                        <li><a href="{{ get_post_url('ayuda-organizadores') }}">@lang('Centro Ayuda Organizadores')</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix-block"></div>
    <div class="foot-top-social">
        <ul class="social-lst">
            <li><a href="https://www.facebook.com/encuentraturetiro" target="_blank" rel="nofollow"><img src="{{ asset($_front.'images/facebook-symbol.svg') }}"></a></li>
            <li><a href="https://www.instagram.com/encuentraturetiro/" target="_blank" rel="nofollow"><img src="{{ asset($_front.'images/instagram-symbol.svg') }}"></a></li>
        </ul>
    </div>

    <div class="row1">
        <div class="foot-btm">
            <div class="logo">
                <a href="{{ route('index') }}" class="logo-dsk">
                    <img src="{{ asset($_front.'images/logo-encuentra-tu-retiro.svg')}}" alt="">
                </a>
            </div>
            <div class="legal">
                <div>
                    <ul>
                        <li><a href="{{ get_page_url('aviso-legal') }}">@lang('Nota legal')</a></li>
                        <li><a href="{{ get_page_url('politica-de-privacidad') }}">@lang('Privacidad')</a></li>
                        <li><a href="{{ get_page_url('terminos-y-condiciones-generales-de-contratacion') }}">@lang('Términos y Condiciones')</a></li>
                        <li><a href="{{ get_page_url('politica-de-cookies') }}">@lang('Cookies')</a></li>
                    </ul>
                </div>

                <div class="copyr">
                    © {{ date('Y') }}, Encuentraturetiro.com, @lang('Todos los derechos reservados').
                </div>
            </div>
            <div class="payments">
                <div class="secure lang_select">
                    <div id="footer-currency-select-desk" class="selet footer-currencies">
                        <select class="select_box currency_select">
                            @foreach($monedas as $moneda)
                                <option @if ($moneda->id == $currentMoneda->id)selected @endif value="{{ $moneda->id }}">{{ cut_text($moneda->title, 26) }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- 
                        <img src="{{ asset($_front . 'images/locked-padlock.svg') }}">
                        <span>Pago 100% Seguro</span>
                    --}}
                </div>

                <div class="methods">
                    <img class="paypal-img" src="{{ asset($_front . 'images/paypal-foot.svg') }}">
                    <img class="visa-img" src="{{ asset($_front . 'images/visa-foot.svg') }}">
                    <img class="mc-img" src="{{ asset($_front . 'images/mastercard1.svg') }}">
                    <img class="mc-img" src="{{ asset($_front . 'images/mastercard2.svg') }}">
                </div>
            </div>
            <div class="foot-logo-mbl">
                <div>
                    <a href="{{ route('index') }}" class="logo-dsk">
                        {{--<img src="{{ asset($_front.'images/logo-gran-final.png')}}" alt="">--}}
                    </a>
                    <div class="copyr">
                        © {{ date('Y') }}, Encuentraturetiro.com, @lang('Todos los derechos reservados').
                    </div>
                </div>
            </div>
            {{--<div class="foot-btm-lft">
                <div class="payment-sec">
                    <ul class="pay-lst">
                        <li><a href="javascript:void(0);"><img src="{{ asset($_front.'images/pago-seguro.svg') }}"></a></li>
                        <li><a href="javascript:void(0);"><img src="{{ asset($_front.'images/paypal.svg') }}"></a></li>
                        <li><a href="javascript:void(0);"><img src="{{ asset($_front.'images/visa.svg') }}"></a></li>
                        <li><a href="javascript:void(0);"><img src="{{ asset($_front.'images/mastercard.svg') }}"></a></li>
                        <li><a href="javascript:void(0);"><img src="{{ asset($_front.'images/maestro.svg') }}"></a></li>
                    </ul>
                </div>
            </div>--}}
        </div>
    </div>
    {{--<div class="payment-sec payment-sec-mb">
        <ul class="pay-lst">
            <li><a href="javascript:void(0);"><img src="{{ asset($_front.'images/pago-seguro.svg') }}"></a></li>
            <li><a href="javascript:void(0);"><img src="{{ asset($_front.'images/paypal.svg') }}"></a></li>
            <li><a href="javascript:void(0);"><img src="{{ asset($_front.'images/visa.svg') }}"></a></li>
            <li><a href="javascript:void(0);"><img src="{{ asset($_front.'images/mastercard.svg') }}"></a></li>
            <li><a href="javascript:void(0);"><img src="{{ asset($_front.'images/maestro.svg') }}"></a></li>
        </ul>
    </div>--}}
</footer>