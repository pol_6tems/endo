@php ( $precio_noche_aux = 0 )
@php ( $barata_aux = obtenirOfertaBarata($post) )
@if ( !empty($barata_aux) ) @php ( $precio_noche_aux = obtenirPreuAmbDescompteOferta($barata_aux) ) @endif

@php ( $duracion_aux = $post->get_field('calendario') )
@if ( !empty($duracion_aux->duracion) && $precio_noche_aux > 0 )
    @php($duration = (intval($duracion_aux->duracion) > 1) ? $duracion_aux->duracion - 1 : $duracion_aux->duracion )
    @php ( $precio_noche_aux = $precio_noche_aux / $duration )
@endif

<div class="carac-list">
    <ul>
        <li>
            @if ($duracion = $post->get_field('calendario'))
            <div class="cara-lft">
                <div class="ls-lft">
                    <i class="icon duracion"></i>
                    <h5>@Lang('Duración:')</h5>
                </div>
            <div class="ls-rgt"> <span>{{ $duracion->duracion }} @lang('días') / {{ ($duracion->duracion - 1) }} @lang('noches')</span> </div>
            </div>
            @endif

            @if ( !empty($precio_noche_aux) && $precio_noche_aux > 0 )
            <div class="cara-rgt">
                <div class="ls-lft" style="width: 55%;">
                    <i class="icon precio-noche"></i>
                    <h5>@Lang('Precio por noche:')</h5>
                </div>
                <div class="ls-rgt"> {{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio_noche_aux) }} @Lang('por noche')</div>
            </div>
            @endif

            @if ($precio = $post->get_field('precio-por-persona'))
            <div class="cara-rgt">
                <div class="ls-lft">
                    <i class="icon preci"></i>
                    <h5>@Lang('Precio por dia:')</h5>
                </div>
                <div class="ls-rgt"> {{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio) }} @Lang('por persona')</div>
            </div>
            @endif
        </li>
        @php($llegada = $post->get_field('hora-llegada'))
        @php($salida = $post->get_field('hora-salida'))


        @if ( $llegada || $salida )
        <li>
            @if ( $llegada )
            <div class="cara-lft">
                <div class="ls-lft">
                    <i class="icon horas-trabajo"></i>
                    <h5>@Lang('Hora Llegada:')</h5>
                </div>
            <div class="ls-rgt">{{ $llegada }}</div>
            </div>
            @endif

            @if ( $salida )
            <div class="cara-rgt">
                <div class="ls-lft">
                    <i class="icon horas-trabajo"></i>
                    <h5>@Lang('Hora Salida:')</h5>
                </div>
                <div class="ls-rgt"> {{ $salida }}</div>
            </div>
            @endif
        </li>
        @endif
        <li>
            @php($alojamiento = $post->get_field('alojamiento'))
            @if($alojamiento && is_object($alojamiento) && ($localizacion = $alojamiento->get_field('localizacion')))
            <div class="cara-lft">
                <div class="ls-lft">
                    <i class="icon sabad"></i>
                    <h5>@Lang('Localización:')</h5>
                </div>
                <div class="ls-rgt">{{ print_location($localizacion, ['locality', 'administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'country']) }}</div>
            </div>
            @endif

            @if($alojamiento && is_object($alojamiento) && ($entornos = $alojamiento->get_field('entorno')))
            <div class="cara-rgt">
                <div class="ls-lft">
                    <i class="icon entorno"></i>
                    <h5>@Lang('Entorno:')</h5>
                </div>
                <div class="ls-rgt">{{ separatedList($entornos, 'title', ', ') }}</div>
            </div>
            @endif
        </li>
    </ul>
    <div class="cara-full">
        <ul>
            @php($actividades = groupCustomList($post, 'actividades', 'actividades2'))
            @if ( !empty($actividades) )
            <li>
                <div class="cara-full-lft">
                    <i class="icon act"></i>
                    <h5>@Lang('Actividades:')</h5>
                </div>
                <div class="cara-full-rgt">
                    <ul>
                        @foreach($actividades as $actividad)
                            @if ( !empty($actividad['item']) )
                                <li><span class="tick-lst">{{ $actividad['item']['value'] }}</span></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </li>
            @endif
            
            @if ($yogas = $post->get_field('tipos-de-yoga'))
            <li>
                <div class="cara-full-lft">
                    <i class="icon yoga"></i>
                    <h5>@Lang('Tipos de Yoga:')</h5>
                </div>
                <div class="cara-full-rgt">
                    <ul>
                        @foreach($yogas as $yoga)
                            <li><span class="tick-lst">{{ $yoga->title }}</span></li>
                        @endforeach
                    </ul>
                </div>
            </li>
            @endif
            @if ($meditacion = $post->get_field('tipo-de-meditacion'))
                <li>
                    <div class="cara-full-lft">
                        <i class="icon meditacion"></i>
                        <h5>@Lang('Tipos de Meditación:')</h5>
                    </div>
                    <div class="cara-full-rgt">
                        <ul>
                            @foreach($meditacion as $medi)
                                <li><span class="tick-lst">{{ $medi->title }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @endif
        </ul>
    </div>
    <ul>
        {{-- Nivel + Formato Publico --}}
        @php($nivel = $post->get_field('nivel'))
        @php($formato = $post->get_field('formato'))

        @if ( $nivel && !$nivel->isEmpty() || $formato && !$formato->isEmpty() )
        <li>
            @if ( $nivel && !$nivel->isEmpty() )
            <div class="cara-lft">
                <div class="ls-lft">
                    <i class="icon nivel"></i>
                    <h5>@Lang('Nivel:')</h5>
                </div>
                <div class="ls-rgt">{{ separatedList($nivel, 'title', ', ') }}</div>
            </div>
            @endif
            @if( $formato && !$formato->isEmpty() )
            <div class="cara-rgt">
                <div class="ls-lft">
                    <i class="icon publico"></i>
                    <h5>@Lang('Formato:')</h5>
                </div>
                <div class="ls-rgt">{{ separatedList($formato, 'title', ', ') }}</div>
            </div>
            @endif
        </li>
        @endif
        

        @php( $alojamiento = $post->get_field('alojamiento') )
        @php( $alojamiento && is_object($alojamiento) && ($tipoAlojamiento = $alojamiento->get_field('tipo-alojamiento')) )
        @php( $tipoHabitaciones = $post->get_field('tipo-habitaciones') )
        @if ( $tipoHabitaciones && !$tipoHabitaciones->isEmpty() || isset($tipoAlojamiento) && !$tipoAlojamiento->isEmpty() )
        <li>
            @if( isset($tipoAlojamiento) && !$tipoAlojamiento->isEmpty() )
            <div class="cara-lft">
                <div class="ls-lft">
                    <i class="icon tipo"></i>
                    <h5>@Lang('Tipo Alojamiento:')</h5>
                </div>
                <div class="ls-rgt">{{ separatedList($tipoAlojamiento, 'title', ', ') }}</div>
            </div>
            @endif
            @if( isset($tipoHabitaciones) && !$tipoHabitaciones->isEmpty() )
            <div class="cara-rgt">
                <div class="ls-lft">
                    <i class="icon habitacion"></i>
                    <h5>@Lang('Habitaciones:')</h5>
                </div>
                <div class="ls-rgt">{{ separatedList($tipoHabitaciones, 'title', ', ') }}</div>
            </div>
            @endif
        </li>
        @endif

        @if($alimentacion = $post->get_field('alimentacion'))
        <li>
            <div class="cara-full-lft">
                <i class="icon ali"></i>
                <h5>@Lang('Alimentación:')</h5>
            </div>
            <div class="cara-full-rgt">
                <ul>
                    @foreach($alimentacion as $ali)
                        <li><span class="tick-lst">{{ $ali->title }}</span></li>
                    @endforeach
                </ul>
            </div>
        </li>
        @endif

        @php($idioma = $post->get_field('idioma'))
        @php($publico = $post->get_field('publico'))
        
        @if ( $idioma && !$idioma->isEmpty() || $publico && !$publico->isEmpty() )
        <li>
            @if( $idioma && !$idioma->isEmpty() )
            <div class="cara-lft">
                <div class="ls-lft">
                    <i class="icon habit"></i>
                    <h5>@Lang('Idioma:')</h5>
                </div>
                <div class="ls-rgt">{{ separatedList($idioma, 'title', ', ') }}</div>
            </div>
            @endif
            @if( $publico && !$publico->isEmpty() )
            <div class="cara-rgt">
                <div class="ls-lft">
                    <i class="icon publico"></i>
                    <h5>@Lang('Público:')</h5>
                </div>
                <div class="ls-rgt">{{ separatedList($publico, 'title', ', ') }}</div>
            </div>
            @endif
        </li>
        @endif
    </ul>
</div>