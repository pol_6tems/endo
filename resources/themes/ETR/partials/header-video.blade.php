<section class="banner-home">
    <div class="home-video">
        @php($rand = random_int(0, 2))
        <video id="banner-video" width="100%" autoplay loop="1" muted preload="auto">
            <source src="{{ asset($_front.'video/etr-header-2.mp4') }}" type="video/mp4">
            <em>Sorry, your browser doesn't support HTML5 video.</em>
        </video>

        <img class="img-principal" src="{{ asset($_front.'images/foto-home-responsive.jpg') }}" alt="">
    </div>
    @if ( $title = $item->get_field('titulo') )
    <div class="mb-ban-title">
        <div class="row1">
            <h1>{!! $title !!}</h1>
        </div>
    </div>
    @endif
    <script>
        /* video load for chrome browser */
        var herovide = document.getElementById('banner-video');
        herovide.autoplay = true;
        herovide.load();
    </script>
</section>