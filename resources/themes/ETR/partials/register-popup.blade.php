<div id="register" class="popup register padding custom_scrollbar">
    <div class="login-pad">
        <h4>@lang('Crea una cuenta')</h4>
        <div class="login-form">
            <div class="reg-type-select">
                <div class="reg-type-active">
                    <a class="js-reg-type" href="javascript:void(0);" data-type="usr">@lang('Usuario')</a>
                </div><div>
                    <a class="js-reg-type" href="javascript:void(0);" data-type="org">@lang('Organizador')</a>
                </div>
            </div>

            <form method="POST" action="{{ route('register') }}">
                @csrf

                @if (request()->isMethod('POST'))
                    <input type="hidden" name="after_register" value="{{ get_after_login_url() }}">
                @endif

                <ul>
                    <li>
                        <div class="frm-grp">
                            <label for="user-name" class="js-usr-name" data-usr-string="@lang('Su nombre')" data-org-string="@lang('Nombre Organización')">@lang('Su nombre')</label>
                            <input type="text" id="user-name" class="user-name @if ($errors->has('nombre'))with-error @endif" name="nombre" placeholder="@lang('Nombre')" required value="{{ old('nombre') }}">
                        </div>
                        @if ($errors->has('nombre'))
                            <div class="form-error" role="alert">
                                <span class="help-block">
                                    {{ $errors->first('nombre') }}
                                </span>
                            </div>
                        @endif
                    </li>
                    <li class="js-last-name">
                        <div class="frm-grp">
                            <label for="user-name">@lang('Apellido')</label>
                            <input type="text" class="user-name @if ($errors->has('apellido'))with-error @endif" name="apellido" placeholder="@lang('Apellido')" value="{{ old('apellido') }}">
                        </div>
                        @if ($errors->has('apellido'))
                            <div class="form-error" role="alert">
                                <span class="help-block">
                                    {{ $errors->first('apellido') }}
                                </span>
                            </div>
                        @endif
                    </li>
                    <li>
                        <div class="frm-grp">
                            <label for="user-name">@lang('Correo')</label>
                            <input type="text" class="user-name @if ($errors->has('correo'))with-error @endif" name="correo" placeholder="encuentraturetiro@mail.com" required value="{{ old('correo') }}">
                        </div>
                        @if ($errors->has('correo'))
                            <div class="form-error" role="alert">
                                <span class="help-block">
                                    {{ $errors->first('correo') }}
                                </span>
                            </div>
                        @endif
                    </li>
                    <li>
                        <div class="frm-grp">
                            <label for="user-name">@lang('Contraseña')</label>
                            <input type="password" class="pass-word @if ($errors->has('contrasena'))with-error @endif" name="contrasena" placeholder="*******" required>
                            <img src="{{ asset($_front.'images/fill-15.svg') }}" class="toggle-password">
                        </div>
                        @if ($errors->has('contrasena'))
                            <div class="form-error" role="alert">
                                <span class="help-block">
                                    {{ $errors->first('contrasena') }}
                                </span>
                            </div>
                        @endif
                    </li>
                    <li class="full-wid js-usr-policy">
                        <input id="policy-login-usr" type="checkbox" name="policy-login-usr" value="options" required>
                        <label for="policy-login-usr">
                            <span><span></span></span>
                            <div>@Lang('He leído y acepto la <a href=":link" target="_blank">Política de Privacidad</a>', [ 'link' => get_page_url('politica-de-privacidad') ])</div>
                        </label>
                    </li>
                    <li class="full-wid js-policy-org" style="display: none">
                        <input id="policy-login-org" type="checkbox" name="policy-login-org" value="options">
                        <label for="policy-login-org">
                            <span><span></span></span>
                            <div>@Lang('He leído y acepto la <a href=":link" target="_blank">Política de Privacidad</a>', [ 'link' => get_page_url('politica-de-privacidad') ])</div>
                        </label>
                    </li>

                    <li class="full-wid js-policy-org" style="display: none">
                        <input id="policy-terms-org" type="checkbox" name="policy-terms-org" value="options">
                        <label for="policy-terms-org">
                            <span><span></span></span>
                            <div>@Lang('He leído y acepto los <a href=":link" target="_blank">Términos y Condiciones Organizador</a>', [ 'link' => get_page_url('terminos-y-condiciones-del-servicio-a-organizadores') ])</div>
                        </label>
                    </li>
                    <li class="check full-wid">
                        <input id="regRemember" type="checkbox" name="remember" value="options" checked>
                        <label for="regRemember"><span><span></span></span>@lang('Recuerdame')</label>
                    </li>
                    <li style="display: none">
                        <input id="regOrganizer" type="checkbox" name="organizer" value="organizer">
                    </li>
                    <li>
                        <div class="btn-rightbg full-wid center-btn">
                            <input type="submit" name="" value="@lang('Regístrate')" class="cont-btn">
                        </div>
                    </li>
                </ul>
            </form>
        </div>
        <div class="clearfix-block"></div>
        <div class="login-google">
            <div class="or-row">
                <span>@lang('O')</span>
            </div>

            <div class="google-button">
                <a id="google-sign-btn" href="javascript:void(0);">
                    <div>
                        <img src="{{ asset($_front.'images/icons8-logo-de-google.svg') }}">
                        <span>@lang('Regístrate con Google')</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>