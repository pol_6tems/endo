
<section class="newsletter">
    <h5>
        <p>@lang('¿Quieres Recibir las últimas novedades, artículos y ofertas?')
        <span>@lang('¡Suscríbete!')</span></p>
    </h5>
    <div class="form">
        <div class="message errors"></div>
        <input type="hidden" name="acumba_list" value="269954">
        <input type="email" data-url="{{ route("newsletter.subscribe") }}" placeholder="emailturetiro@gmail.com" name="email_newsletter" value="">
        <button id="newsletter">@lang('Suscribirme')</button>
        <div class="login-form">
            <input id="policy" type="checkbox" name="policy" value="true" required="">
            <label for="policy">
                <span><span></span></span>
                @Lang('He leído y acepto la <a href=":link" target="_blank">Política de Privacidad</a>', [ 'link' => get_page_url('politica-de-privacidad') ])
            </label>
        </div>
    </div>
</section>