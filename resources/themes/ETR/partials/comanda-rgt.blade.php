<div class="retro-rgt no-tpad">
    <div class="white-box-rgt">
        @if ($retiro && count($galeria))
            <img class="detalles-retiro" src="{{ reset($galeria)->get_thumbnail_url('medium_large') }}" alt="">
        @endif
        <div class="clearfix-block"></div>
        <div class="white-box-cnt">
            @if ($retiro)
                <div class="item-des">
                    <div class="left no-right">
                        <h2 style="padding-right: 5px;">{{ $retiro->title }}</h2>
                        {{--@if ($author)
                            @php($name = explode(" ", $author->name)[0])
                            <div class="small-dec">{{ $name }}</div>
                        @endif--}}
                        @php($ratings = do_action('get_ratings', $retiro->id))
                        <div class="rat-stars"> @include('Front::partials.reviews-avg-rating', ['retObj' => $retiro]) @if ($ratings['avg'])<span>{{ $ratings['avg'] }}/5</span>@endif </div>
                    </div>
                    {{--@if ($author)
                        <div class="right">
                            <img src="@if ($author->get_field('foto-organizador')){{ $author->get_field('foto-organizador')->get_thumbnail_url() }}@else{{ $author->get_avatar_url() }}@endif" alt="">
                        </div>
                    @endif--}}
                </div>
            @endif
            <div class="date-person-dtl">
                <ul>
                    <li class="date-dtl">{{ $entrada }} al {{ $sortida }}</li>
                    @if ($personas > 1)
                        <li class="person-dtl">{{ $personas }} personas</li>
                    @else
                        <li class="person-dtl">{{ $personas }} persona</li>
                    @endif
                </ul>
            </div>
            <div class="dat-select edit-person">
                <h2>@lang('Habitación')</h2>
                <ul class="habitacion">
                    <li>
                        <div class="select-name">
                            <h2 class="hab-dtl">{{ !empty($habitacion) ? $habitacion->title : '' }}</h2>
                        </div>
                        <div class="edit-price"> {{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio) }} </div>
                    </li>
                </ul>
            </div>
            <div class="dat-select resumen with-borders">
                <h2>@Lang('Pagos')</h2>
                <ul>
                    <li>
                        <div class="resu-txt">
                            <h2>@Lang('Depósito reserva')</h2>
                        </div>
                        <div class="resu-price"> {{ $currencySymbol }} <span class="js-reserve-payment">{{ $deposito }}</span> </div>
                    </li>
                    <li>
                        <div class="resu-txt">
                            <h2>@Lang('Pago al organizador')</h2>
                        </div>
                        <div class="resu-price"> {{ $currencySymbol }} <span class="js-organizer-payment">{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) - $deposito }}</span> </div>
                    </li>
                    @if($dharmas > 0 && (!isset($coupon) || !$coupon))
                        <li class="js-dharmas-line">
                            <div class="resu-txt">
                                <h2>@Lang('Dto ') {{ round($dharmasQuantity, 0) }} Dharmas</h2>
                            </div>
                            <div class="resu-price">{{ $currencySymbol }}  - <span class="js-dharmas-value">{{ $dharmas }}</span> </div>
                        </li>
                    @endif
                    <li class="js-coupon-line" @if (!isset($coupon) || !$coupon)style="display: none"@endif>
                        <div class="resu-txt">
                            <h2>@Lang('Dto cupón') <span class="coupon-code js-coupon-code">{{ isset($coupon) && $coupon ? $coupon->code : 0 }}</span></h2>
                        </div>
                        <div class="resu-price">{{ $currencySymbol }}  - <span class="js-coupon-value">{{ isset($coupon_amount) && $coupon_amount ? toUserCurrency('EUR', $coupon_amount, false) : 0 }}</span> </div>
                    </li>
                </ul>
            </div>
            <div class="dat-select resumen total">
                <ul>
                    <li>
                        <div class="resu-txt">
                            <h2>@lang('Total')</h2>
                        </div>
                        <div class="resu-price"> {{ $currencySymbol }} <span class="js-total-order">{{ (toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) - $dharmas) }}</span></div>
                    </li>
                    @if ( auth()->check() && ($es_reserva_inmediata || (!empty($retiro_aceptado) && $retiro_aceptado)))
                        <li>
                            <div class="resu-txt">
                                <h1 class="alt">@Lang('Pagar ahora')</h1>
                            </div>
                            <div class="resu-price"> {{ $currencySymbol }} <span class="js-pay-now">{{ $deposito - $dharmas }}</span></div>
                        </li>
                    @endif
                </ul>
                <p class="alt">Los <span>{{ $currencySymbol }} {{ (toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) - $deposito) }} restantes</span> se pagarán
                    @if ($pay_days_before)
                        @lang('al organizador el') <span>{{ \Jenssegers\Date\Date::createFromFormat('d/m/Y', $entrada)->subDays($pay_days_before)->format('j \\d\\e F Y') }}.</span>
                    @else
                        @lang('directamente en el retiro el') <span>{{ \Jenssegers\Date\Date::createFromFormat('d/m/Y', $entrada)->subDays($pay_days_before)->format('j \\d\\e F Y') }}.</span>
                    @endif
                </p>
                
                <p class="alt">
                    @php($cancel_date = \Jenssegers\Date\Date::createFromFormat('d/m/Y', $entrada)->subDays($cancel_days_before))
                    @if ($cancel_policy == 'estricta')
                        @lang('Canjeable por otras fechas hasta el ')<span class="str-cancel-date">{{ $cancel_date->format('d/m/Y') }}</span>
                    @elseif ($cancel_date > \Carbon\Carbon::now())
                            <h4><strong>@lang('Cancelación gratuita hasta el ')<span class="str-cancel-date">{{ $cancel_date->format('d/m/Y') }}</span></strong></h4>
                    @endif
                </p>
            </div>
        </div>
    </div>

    @if ( auth()->check() && ($es_reserva_inmediata || (!empty($retiro_aceptado) && $retiro_aceptado)))
        <div class="white-box-rgt">
            <div class="white-box-cnt desimiler frm-ryt coupon-frm">
                <h3>@lang('¿Tienes un código promocional?')</h3>
                <div class="frm-grp">
                    <input type="text" class="frm-ctrl js-coupon-input" data-amount="{{ $precio }}" placeholder="@lang('Escribe tu código')">
                </div>

                <div class="coupon-msg js-coupon-message">

                </div>

                <button type="button" class="sub-btn js-coupon-submit">@lang('APLICAR')</button>
            </div>
        </div>
    @endif

    @if (!route_name('retiros.reservado'))
        <div class="dat-select promos">
            {{--<div class="promo">
                <span><img src="{{ asset($_front.'images/Dharmas-icon.svg') }}"></span> <p>@lang('Suma')&nbsp;<strong class="js-add-dharmas"><a href="{{ get_page_url('dharmas-1') }}">{{ round($moreDharmas) }}&nbsp;Dharmas</a></strong>&nbsp;@lang('para tu próxima reserva')</p>
            </div>--}}
            <div class="promo">
                <span><img src="{{ asset($_front.'images/Imagen_22.png') }}"></span> @lang('Plantamos')&nbsp;<strong>1 @lang('árbol')</strong>&nbsp;@lang('por ti')
            </div>
        </div>
    @endif
    <div class="mob-hidden">
        @includeIf('Front::partials.carousel-equipo')
    </div>
</div>