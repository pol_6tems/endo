<div id="login" class="popup login padding custom_scrollbar">
    <div class="login-pad">
        <h4>@Lang('Login')</h4>
        <div class="login-form">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                @if (request()->isMethod('POST'))
                    <input type="hidden" name="after_login" value="{{ get_after_login_url() }}">
                @endif
                <ul>
                    {{--<li>
                        --}}{{-- <div class="g-signin2" data-onsuccess="onSignIn" data-theme="Claro"></div> --}}{{--
                    </li>--}}
                    <li>
                        <div class="frm-grp">
                            <label for="user-name">@lang('Correo')</label>
                            <input type="text" class="user-name @if ($errors->has('email'))with-error @endif" name="email" placeholder="encuentraturetiro@mail.com" required value="{{ old('email') }}">
                        </div>
                        @if ($errors->has('email'))
                            <div class="form-error" role="alert">
                                <span class="help-block">
                                    {{ $errors->first('email') }}
                                </span>
                            </div>
                        @endif
                    </li>
                    <li>
                        <div class="frm-grp">
                            <label for="user-name">@lang('Contraseña')</label>
                            <input type="password" class="pass-word @if ($errors->has('password'))with-error @endif" name="password" placeholder="*******" required>
                            <img src="{{ asset($_front.'images/fill-15.svg') }}" class="toggle-password">
                        </div>
                        @if ($errors->has('password'))
                            <div class="form-error" role="alert">
                                <span class="help-block">
                                    {{ $errors->first('password') }}
                                </span>
                            </div>
                        @endif
                    </li>
                    <li class="check full-wid">
                        <input id="logRemember" type="checkbox" name="remember" value="options" checked>
                        <label for="logRemember"><span><span></span></span>@Lang('Recuérdame')</label>
                    </li>
                    <li>
                        <div class="btn-rightbg full-wid center-btn">
                            <input type="submit" name="" value="@lang('Iniciar sesión')" class="cont-btn pd-10">
                        </div>
                    </li>
                    <li><a class="link-pad" href="{{ route('password.request') }}">@Lang('¿Has olvidado la contraseña?')</a></li>
                </ul>
            </form>
        </div>
        <div class="encara-pad">@lang('¿Todavía no tienes cuenta?') <span>@lang('Regístrate'):</span></div>

        <div class="clearfix-block"></div>

        <div class="login-form login-reg-buttons">
            <div class="btn-rightbg full-wid center-btn">
                <a href="#register" class="cont-btn fancybox1 show" data-type="usr">@lang('Nuevo usuario')</a><a href="#register" class="cont-btn fancybox1 show" data-type="org">@lang('Nuevo organizador')</a>
            </div>
        </div>

        <div class="clearfix-block"></div>

        <div class="login-google">
            <div class="or-row">
                <span>@lang('O')</span>
            </div>

            <div class="google-button">
                <a id="google-signin-btn" href="javascript:void(0);">
                    <div>
                        <img src="{{ asset($_front.'images/icons8-logo-de-google.svg') }}">
                        <span>@lang('Iniciar sesión con Google')</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>