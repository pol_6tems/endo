@if (isset($item) && $item && $item->get_field('banner-imagen'))
<section class="funciona necesit">
    <div class="row1">
        <div class="necesit-img" style="height: auto;">
            <img src="{{ $item->get_field('banner-imagen')->get_thumbnail_url('large') }}">
            <div class="caption-imgs">
                <h2>{!! $item->get_field('banner-titulo') !!}</h2>
                <a @if (!auth()->user())class="fancybox1 show" href="#register"@else href="{{ $item->get_field('banner-link') }}"@endif>{{ $item->get_field('texto-boton') }}</a>
            </div>
        </div>
    </div>
</section>
@endif