<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Factura</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('Front::pdfs.common-styles')
    <style>
        .with-bg {
            background-color: #f9af02
        }

        .with-border-btm, .table td.with-border-btm, .table td.with-border-btm strong {
            color: #fff;
        }

        .with-content-bg {
            background-color: #f2f2f2
        }
    </style>
</head>

<body>
<div class="container">
    <table style="margin-left: auto; margin-right: auto" width="600px">
        <tr>
            <td width="360px" valign="top" style="padding-bottom: 14px">
                <img src="{{ request()->root() . '/' . $_front . 'images/logo-gran-final.png' }}" style="width: 160px;"/>
            </td>

            <!-- Organization Name / Image -->
            <td align="left">
                <h1>{{ __('RESERVA') }}</h1>
                <table width="100%" style="margin-top: 14px; font-size: 10px">
                    <tr>
                        <td width="33%" style="padding: 3px 0 4px">{{ __('Nº Reserva: ') }}</td>
                        <td style="padding: 3px 0 4px">{{ $order_id }}</td>
                    </tr>
                    <tr>
                        <td width="33%" style="padding: 3px 0 4px">{{ __('FECHA') }}</td>
                        <td style="padding: 3px 0 4px">{{ $order_date }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%" class="table">
                    <tr class="with-bg">
                        <td colspan="2" class="with-border-btm"><strong>{{ __('DATOS RESERVA') }}</strong></td>
                    </tr>
                    <tr class="with-content-bg">
                        <td style="padding-top: 12px">{{ __('Plazas: ') }}</td>
                        <td style="text-align: right; padding-top: 12px">{{ $personas }}</td>
                    </tr>
                    <tr class="with-content-bg">
                        <td>{{ __('Habitación: ') }}</td>
                        <td style="text-align: right">{{ $habitacion }}</td>
                    </tr>
                    <tr class="with-content-bg">
                        <td>{{ __('Fechas del Retiro: ') }}</td>
                        <td style="text-align: right">{!! str_replace('-', '&nbsp;&nbsp; - &nbsp;&nbsp;', $fechas)  !!}</td>
                    </tr>
                    <tr class="with-content-bg">
                        <td style="padding-bottom: 12px">{{ __('Retiro: ') }}</td>
                        <td style="text-align: right; padding-bottom: 12px">{!! trim($retiro_link, '"') !!}</td>
                    </tr>
                </table>
            </td>
        </tr>

        @foreach($participants as $key => $participant)
            <tr>
                <td colspan="2">
                    <table width="100%" class="table">
                        <tr class="with-bg">
                            <td colspan="2" class="with-border-btm"><strong>{{ __('DATOS PARTICIPANTE :key', ['key' => $key > 1 ? $key : '']) }}</strong></td>
                        </tr>
                        <tr class="with-content-bg">
                            <td style="padding-top: 12px">{{ __('Nombre Participante: ') }}</td>
                            <td style="text-align: right; padding-top: 12px">{{ $participant['nombre'] . ($participant['apellidos'] ? ' ' . $participant['apellidos'] : '') }}</td>
                        </tr>
                        <tr class="with-content-bg">
                            <td>{{ __('Su Email: ') }}</td>
                            <td style="text-align: right; color: #f9af02">{{ $participant['email'] }}</td>
                        </tr>
                        <tr class="with-content-bg">
                            <td @if(!isset($participant['fecha_nacimiento']) && !isset($participant['city']) && !isset($participant['country']))style="padding-bottom: 12px" @endif>{{ __('Género: ') }}</td>
                            <td style="text-align: right; @if(!isset($participant['fecha_nacimiento']) && !isset($participant['city']) && !isset($participant['country']))padding-bottom: 12px @endif">{{ $participant['genero'] }}</td>
                        </tr>
                        @if (isset($participant['fecha_nacimiento']))
                            @php ($birth = \Carbon\Carbon::parse($participant['fecha_nacimiento']['ano'] . '-' . $participant['fecha_nacimiento']['mes'] . '-' . $participant['fecha_nacimiento']['dia']))
                            @php ($years = $birth->diffInYears(\Carbon\Carbon::now()))
                            <tr class="with-content-bg">
                                <td @if(!isset($participant['city']) && !isset($participant['country']))style="padding-bottom: 12px" @endif>{{ __('Edad: ') }}</td>
                                <td style="text-align: right; @if(!isset($participant['city']) && !isset($participant['country']))padding-bottom: 12px @endif">{{ $years }}</td>
                            </tr>
                        @endif
                        @if (isset($participant['city']))
                            <tr class="with-content-bg">
                                <td @if(!isset($participant['country']))style="padding-bottom: 12px" @endif>{{ __('Localidad: ') }}</td>
                                <td style="text-align: right; @if(!isset($participant['country']))padding-bottom: 12px @endif">{{ $participant['city'] }}</td>
                            </tr>
                        @endif
                        @if (isset($participant['country']))
                            <tr class="with-content-bg">
                                <td style="padding-bottom: 12px">{{ __('Pais: ') }}</td>
                                <td style="text-align: right; padding-bottom: 12px">{{ $participant['country'] }}</td>
                            </tr>
                        @endif
                    </table>
                </td>
            </tr>
        @endforeach

        @if (count($participants) >= 3)
    </table>

    <div class="page-break"></div>

    <table style="margin-left: auto; margin-right: auto" width="600px">
        @endif
        <tr>
            <td colspan="2">
                <table width="100%" class="table">
                    <tr class="with-bg">
                        <td colspan="2" class="with-border-btm"><strong>{{ __('PAGOS') }}</strong></td>
                    </tr>
                    <tr class="with-content-bg">
                        <td style="padding-top: 12px">{{ __('Habitación: ') }}</td>
                        <td style="text-align: right; padding-top: 12px">{{ $habitacion }}</td>
                    </tr>
                    <tr class="with-content-bg">
                        <td>{{ __('Plazas: ') }}</td>
                        <td style="text-align: right">{{ $personas }}</td>
                    </tr>
                    <tr class="with-content-bg">
                        <td>{{ __('Depósito pagado: ') }}</td>
                        <td style="text-align: right">{!! str_replace('€', '<span class="eur">€</span>', $deposito) !!}</td>
                    </tr>
                    <tr class="with-content-bg">
                        <td width="50%"><strong>{{ __('Precio TOTAL retiro: ') }}</strong></td>
                        <td width="50%" style="text-align: right"><strong>{!! str_replace('€', '<span class="eur">€</span>', $total) !!}</strong></td>
                    </tr>
                    <tr class="with-content-bg">
                        <td width="50%"><strong style="color: #f9af02;">{{ __('Importe TOTAL Pendiente: ') }}</strong></td>
                        <td width="50%" style="text-align: right"><strong style="color: #f9af02;">{!! str_replace('€', '<span class="eur">€</span>', $al_organizador) !!}</strong></td>
                    </tr>
                    <tr class="with-content-bg">
                        <td style="padding-bottom: 12px">{{ __('Importe Pendiente por Persona: ') }}</td>
                        <td style="text-align: right; padding-bottom: 12px">{!! str_replace('€', '<span class="eur">€</span>', $al_organizador_per_person) !!}</td>
                    </tr>
                </table>
            </td>
        </tr>

        @if (count($participants) == 2)
    </table>

    <div class="page-break"></div>

    <table style="margin-left: auto; margin-right: auto" width="600px">
        @endif
        <tr>
            <td colspan="2">
                <table width="100%" class="table">
                    <tr class="with-bg">
                        <td colspan="2" class="with-border-btm"><strong>{{ __('CUENTAS ETR') }}</strong></td>
                    </tr>
                    <tr class="with-content-bg">
                        <td style="padding-top: 12px; @if (!$etr_pending)padding-bottom: 12px @endif">{{ __('Factura Comisión (Impuestos incluidos): ') }}</td>
                        <td style="text-align: right; padding-top: 12px; @if (!$etr_pending)padding-bottom: 12px @endif">{!! str_replace('€', '<span class="eur">€</span>', $commission) !!}</td>
                    </tr>
                    @if ($etr_pending)
                        <tr class="with-content-bg">
                            <td>{{ __('Pago pendiente de EncuentratuRetiro*: ') }}</td>
                            <td style="text-align: right">{!! str_replace('€', '<span class="eur">€</span>', $etr_pending) !!}</td>
                        </tr>
                        <tr class="with-content-bg">
                            <td colspan="2" style="font-size: 9px; padding: 1px 10px">{{ __('(Realizaremos la transferencia del importe unos días antes del retiro)') }}</td>
                        </tr>
                        <tr class="with-content-bg">
                            <td colspan="2" style="font-size: 9px; padding: 1px 10px 12px">{{ __('*Pago Pendiente = Depósito pagado por el usuario - Comisión ETR') }}</td>
                        </tr>
                    @endif
                </table>
            </td>
        </tr>

        @if (count($participants) == 1)
    </table>

    <div class="page-break"></div>

    <table style="margin-left: auto; margin-right: auto" width="600px">
        @endif

        <tr>
            <td colspan="2"><br><br></td>
        </tr>

        @if ($message_to_org)
            <tr class="with-content-bg">
                <td colspan="2">
                    <table width="100%" class="table" style="margin-bottom: 0">
                        <tr>
                            <td colspan="2" style="padding: 9px 10px 13px"><strong>{{ __('COMENTARIOS ') }}</strong></td>
                        </tr>

                        <tr>
                            <td colspan="2" style="padding: 6px 10px 16px">{!! nl2br($message_to_org) !!}</td>
                        </tr>
                    </table>
                </td>

            </tr>

            <tr>
                <td colspan="2"><br></td>
            </tr>
        @endif

        <tr class="with-content-bg">
            <td colspan="2">
                <table width="100%" class="table" style="margin-bottom: 0">
                    <tr>
                        <td colspan="2" style="padding: 9px 10px 13px"><strong>{{ __('CANCELACIÓN') }}</strong></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <ul style="margin: 0; padding: 0 16px 16px">
                                <li style="padding-bottom: 7px">{{ $cancel_l1 }}</li>
                                <li>{{ $cancel_l2 }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="2"><br></td>
        </tr>

        <tr class="with-content-bg">
            <td colspan="2">
                <table width="100%" class="table" style="margin-bottom: 0">
                    <tr>
                        <td colspan="2" style="padding: 9px 10px 13px"><strong>{{ __('NOTAS') }}</strong></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <ul style="margin: 0; padding: 0 16px 16px; font-size: 10px">
                                <li style="padding-bottom: 7px">{!! __('Esta reserva está sujeta por los términos y condiciones de organizadores aceptados al hacer uso de la cuenta de organizador. Puedes <a href=":terms_url">leerlos aquí</a>.', ['terms_url' => get_page_url('terminos-y-condiciones-del-servicio-a-organizadores')]) !!}</li>
                                <li style="padding-bottom: 7px">{!! __('En caso de cancelación del retiro por causas justificadas, se cobrará 15<span class="eur">€</span> de gastos de gestión.') !!}</li>
                                <li>{{ __('Estamos aquí para ti. Cualquier consulta o incidencia nos puedes contactar al centro de atención: +34 972 664 640 o mandar un email.') }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="2"><br><br></td>
        </tr>

        <tr>
            <td colspan="2">
                {{ __('Un abrazo,') }}<br>
                Encuentra tu Retiro
            </td>
        </tr>
    </table>

    <div style="position: absolute; bottom: 0; right: 0; left: 0">
        <table style="margin-left: auto; margin-right: auto" width="600px">
            <tr>
                <td style="text-align: center; padding-bottom: 8px"><img src="{{ request()->root() . '/' . $_front . 'images/logo-gran-final.png' }}" style="width: 128px;"/></td>
            </tr>
            <tr>
                <td style="color: #f9af02; font-weight: 500; text-align: center">{{ __('"Un viaje interior te está esperando..."') }}</td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>