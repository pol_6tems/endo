<style>
    @font-face {
        font-family: 'Barlow';
        font-style: normal;
        font-weight: 300;
        src: url('{{ storage_path('fonts/barlow-v3-latin-300.ttf') }}') format('truetype');
    }
    /* barlow-regular - latin */
    @font-face {
        font-family: 'Barlow';
        font-style: normal;
        font-weight: normal;
        src: url('{{ storage_path('fonts/barlow-v3-latin-regular.ttf') }}') format('truetype');
    }
    /* barlow-600 - latin */
    @font-face {
        font-family: 'Barlow';
        font-style: normal;
        font-weight: bold;
        src: url({{ storage_path('fonts/barlow-v3-latin-600.ttf') }}) format('truetype');
    }
    /* barlow-500 - latin */
    @font-face {
        font-family: 'Barlow';
        font-style: normal;
        font-weight: 500;
        src: url('{{ storage_path('fonts/barlow-v3-latin-500.ttf') }}') format('truetype');
    }
    /* barlow-700 - latin */
    @font-face {
        font-family: 'Barlow';
        font-style: normal;
        font-weight: 700;
        src: url('{{ storage_path('fonts/barlow-v3-latin-700.ttf') }}') format('truetype');
    }

    body {
        background: #fff;
        background-image: none;
        font-size: 12px;
        font-family: 'barlow', sans-serif;
        color: #4e4e4e;
    }
    h1 {
        margin-bottom: 6px;
        font-weight: 600;
        font-size: 20px;
        margin-top: 0;
    }
    h2 {
        font-size:20px;
    }
    .container {
        padding-top:30px;
    }
    table, .table {
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 10px;
    }
    .table th {
        padding: 8px;
        line-height: 12px;
        text-align: left;
        vertical-align: top;
    }

    .table td {
        padding: 3px 10px 3px;
        line-height: 12px;
        text-align: left;
        vertical-align: middle;
        color: #000;
    }

    strong {
        /*font-family: 'barlow-semibold';*/
        font-weight: bold;
        color: #4e4e4e;
    }

    .page-break {
        page-break-after: always;
    }

    .with-border-btm, .table td.with-border-btm {
        padding: 7px 10px;
    }

    a {
        text-decoration: none;
        color: #f9af02;
    }

    span.eur {
        font-family: Helvetica;
    }
</style>