<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ __('Reserva') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('Front::pdfs.common-styles')
    <style>
        .with-bg {
            background-color: #f2f2f2
        }

        .with-border-btm, .table td.with-border-btm {
            border-bottom: 1px solid #f9af02;
            color: #4e4e4e;
        }
    </style>
</head>

<body>
<div class="container">
    <table style="margin-left: auto; margin-right: auto" width="600px">
        <tr>
            <td width="360px" valign="top" style="padding-bottom: 14px">
                <img src="{{ public_path($_front . 'images/logo-gran-final.png') }}" style="width: 128px;"/><br>

                <span style="color: #f9af02; font-weight: 500">{{ __('"Un viaje interior te está esperando..."') }}</span>
            </td>

            <!-- Organization Name / Image -->
            <td align="left">
                <h1>{{ $target == 'organizador' ? __('RESERVA CONFIRMADA') : __('RESERVA') }}</h1>
                <table width="100%" style="margin-top: 14px; font-size: 10px">
                    <tr>
                        <td width="33%" style="padding: 3px 0 4px">{{ __('Nº Reserva: ') }}</td>
                        <td style="padding: 3px 0 4px">{{ $order_id }}</td>
                    </tr>
                    <tr>
                        <td width="33%" style="padding: 3px 0 4px">{{ __('FECHA') }}</td>
                        <td style="padding: 3px 0 4px">{{ $order_date }}</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <table width="100%" class="table">
                    <tr class="with-bg">
                        <td colspan="2" class="with-border-btm"><strong>{{ __('DATOS RESERVA') }}</strong></td>
                    </tr>
                    <tr>
                        <td style="padding-top: 12px">{{ __('Plazas: ') }}</td>
                        <td style="text-align: right; padding-top: 12px">{{ $personas }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('Habitación: ') }}</td>
                        <td style="text-align: right">{{ $habitacion }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('Fechas del Retiro: ') }}</td>
                        <td style="text-align: right">{!! str_replace('-', '&nbsp;&nbsp; - &nbsp;&nbsp;', $fechas)  !!}</td>
                    </tr>
                    <tr>
                        <td>{{ __('Retiro: ') }}</td>
                        <td style="text-align: right; color: #f9af02">{!! trim($retiro_link, '"') !!}</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <table width="100%" class="table">
                    @if ($target == 'organizador')
                        @foreach($participants_arr as $key => $participant)
                            @if (isset($participant['nombre']) && isset($participant['email']))
                                <tr class="with-bg">
                                    <td colspan="2" class="with-border-btm">
                                        <strong>{{ __('DATOS PARTICIPANTE :key', ['key' => $key]) }}</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>@Lang('Nombre Participante: ')</td>
                                    <td style="text-align: right">{{ $participant['nombre'] . ($participant['apellidos'] ? ' ' . $participant['apellidos'] : '') }}</td>
                                </tr>
                                @if (isset($participant['email']))
                                    <tr>
                                        <td>@Lang('Email: ')</td>
                                        <td style="text-align: right">{{ $participant['email'] }}</td>
                                    </tr>
                                @endif
                                @if (isset($participant['genero']))
                                    <tr>
                                        <td>@Lang('Género: ')</td>
                                        <td style="text-align: right">{{ $participant['genero'] }}</td>
                                    </tr>
                                @endif
                                @if (isset($participant['telefono']))
                                    <tr>
                                        <td>@Lang('Teléfono: ')</td>
                                        <td style="text-align: right">{{ $participant['telefono'] }}</td>
                                    </tr>
                                @endif
                                @if (isset($participant['city']))
                                    <tr>
                                        <td>@Lang('Ciudad: ')</td>
                                        <td style="text-align: right">{{ $participant['city'] }}</td>
                                    </tr>
                                @endif
                                @if (isset($participant['country']))
                                    <tr>
                                        <td>@Lang('País: ')</td>
                                        <td style="text-align: right">{{ $participant['country'] }}</td>
                                    </tr>
                                @endif
                                @if (isset($participant['fecha_nacimiento']))
                                    @php($birth = Date::parse($participant['fecha_nacimiento']['ano'] . '-' . $participant['fecha_nacimiento']['mes'] . '-' . $participant['fecha_nacimiento']['dia']))
                                    @php($years = $birth->diffInYears(Date::now()))
                                    <tr>
                                        <td>@Lang('Edad: ')</td>
                                        <td style="text-align: right">{{ $years }}</td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    @else
                        <tr class="with-bg">
                            <td colspan="2" class="with-border-btm">
                                <strong>{{ __('DATOS ORGANIZADOR') }}</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 12px">{{ __('Nombre Contacto: ') }}</td>
                            <td style="text-align: right; padding-top: 12px">{{ $author_name }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('Su Email: ') }}</td>
                            <td style="text-align: right">{{ $author_email }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('Teléfono: ') }}</td>
                            <td style="text-align: right">{{ $author_phone }}</td>
                        </tr>
                    @endif
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <table width="100%" class="table">
                    <tr class="with-bg">
                        <td colspan="2" class="with-border-btm"><strong>{{ __('PAGO') }}</strong></td>
                    </tr>
                    <tr>
                        <td width="50%" style="padding-top: 12px">{{ __('Habitación: ') }}</td>
                        <td width="50%" style="text-align: right; padding-top: 12px">{{ $habitacion }}</td>
                    </tr>
                    <tr>
                        <td width="50%">{{ __('Plazas: ') }}</td>
                        <td width="50%" align="right" style="text-align: right">{{ $personas }}</td>
                    </tr>
                    @if ($discount_value || !$is_org)
                        <tr>
                            <td width="50%" width="50%">{{ __('Depósito pagado: ') }}</td>
                            <td width="50%" style="text-align: right"><span class="eur">€</span> {{ $deposito_value }}</td>
                        </tr>
                        @if ($discount_value)
                            <tr>
                                <td width="50%" width="50%">{{ __('Descuento Aplicado: ') }}</td>
                                <td width="50%" style="text-align: right"><span class="eur">€</span> {{ $discount_value }}</td>
                            </tr>
                        @endif
                    @endif
                    <tr>
                        <td width="50%" width="50%">{{ __('Depósito TOTAL: ') }}</td>
                        <td width="50%" style="text-align: right"><span class="eur">€</span> {{ $discount_value + $deposito_value }}</td>
                    </tr>
                    <tr>
                        <td width="50%"><strong>{{ __('Precio TOTAL retiro: ') }}</strong></td>
                        <td width="50%" style="text-align: right"><strong>{!! str_replace('€', '<span class="eur">€</span>', $total) !!}</strong></td>
                    </tr>
                    <tr>
                        <td width="50%" style="padding-bottom: 4px"><strong style="color: #f9af02;">{{ __('Importe TOTAL Pendiente: ') }}</strong></td>
                        <td width="50%" style="text-align: right; padding-bottom: 4px"><strong style="color: #f9af02;">{!! str_replace('€', '<span class="eur">€</span>', $al_organizador) !!}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size: 9px; padding: 1px 10px">{{ __('(Directamente al organizador)') }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size: 9px; padding: 1px 10px">{{ __('El organizador te informará sobre las modalidades de pago del importe restante') }}</td>
                    </tr>
                    <tr>
                        <td width="50%" style="padding-top: 12px">{{ __('Precio Retiro por persona: ') }}</td>
                        <td width="50%" style="text-align: right; padding-top: 12px">{!! str_replace('€', '<span class="eur">€</span>', $total_per_person) !!}</td>
                    </tr>
                    <tr>
                        <td width="50%">{{ $is_org ? __('Depósito por Persona: ') : __('Depósito pagado por Persona: ') }}</td>
                        <td width="50%" style="text-align: right">{!! str_replace('€', '<span class="eur">€</span>', $deposito_per_person) !!}</td>
                    </tr>
                    <tr>
                        <td width="50%">{{ __('Importe Pendiente por Persona: ') }}</td>
                        <td width="50%" style="text-align: right">{!! str_replace('€', '<span class="eur">€</span>', $al_organizador_per_person) !!}</td>
                    </tr>

                </table>
            </td>
        </tr>
        @if ($target == 'organizador')
            <tr>
                <td colspan="2">
                    <table width="100%" class="table">
                        <tr class="with-bg">
                            <td colspan="2" class="with-border-btm"><strong>{{ __('CUENTAS ETR') }}</strong></td>
                        </tr>
                        <tr class="with-content-bg">
                            <td style="padding-top: 12px; @if (!$etr_pending)padding-bottom: 12px @endif">{{ __('Factura EncuentratuRetiro: ') }}</td>
                            <td style="text-align: right; padding-top: 12px; @if (!$etr_pending)padding-bottom: 12px @endif">{!! str_replace('€', '<span class="eur">€</span>', $commission) !!}</td>
                        </tr>
                        @if ($etr_pending >= 0)
                            <tr class="with-content-bg">
                                <td>{{ __('Pago pendiente EncuentratuRetiro*: ') }}</td>
                                <td style="text-align: right">{!! str_replace('€', '<span class="eur">€</span>', $etr_pending) !!}</td>
                            </tr>
                            <tr class="with-content-bg">
                                <td colspan="2" style="font-size: 9px; padding: 1px 10px">{{ __('(Realizaremos la transferencia del importe unos días antes del retiro)') }}</td>
                            </tr>
                            <tr class="with-content-bg">
                                <td colspan="2" style="font-size: 9px; padding: 1px 10px 12px">{{ __('*Pago Pendiente = Depósito pagado por el usuario - Comisión ETR') }}</td>
                            </tr>
                        @endif
                    </table>
                </td>
            </tr>
        @endif

        <tr>
            <td colspan="2"><br></td>
        </tr>
        @if ($message_to_org)
            <tr class="with-bg">
                <td colspan="2">
                    <table width="100%" class="table" style="margin-bottom: 0">
                        <tr>
                            <td colspan="2" style="padding: 9px 10px 13px"><strong>{{ __('COMENTARIOS ') }}</strong></td>
                        </tr>

                        <tr>
                        <td colspan="2" style="padding: 6px 10px 16px">{!! nl2br($message_to_org) !!}</td>
                        </tr>
                    </table>
                </td>

            </tr>

            <tr>
                <td colspan="2"><br></td>
            </tr>
        @endif

        <tr class="with-bg">
            <td colspan="2">
                <table width="100%" class="table" style="margin-bottom: 0">
                    <tr>
                        <td colspan="2" style="padding: 9px 10px 13px"><strong>{{ __('CANCELACIÓN') }}</strong></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <ul style="margin: 0; padding: 0 16px 0">
                                <li style="padding-bottom: 7px">{{ $cancel_l1 }}</li>
                                <li>{{ $cancel_l2 }}</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-bottom: 12px; font-size: 9px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! __('Para tramitar una cancelación mandar un email a :emailto', ['emailto' => '&nbsp;<a href="mailto:reservas@encuentraturetiro.com">reservas@encuentraturetiro.com</a>']) !!}</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="2"><br></td>
        </tr>

        <tr class="with-bg">
            <td colspan="2">
                <table width="100%" class="table" style="margin-bottom: 0">
                    <tr>
                        <td colspan="2" style="padding: 9px 10px 13px"><strong>{{ __('NOTAS') }}</strong></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <ul style="margin: 0; padding: 0 16px 16px; font-size: 10px">
                                <li style="padding-bottom: 7px">{{ __('El organizador se reserva el derecho de modificar o suspender el retiro por causas justificadas. En este caso se realizará el reembolso total del depósito.') }}</li>
                                <li style="padding-bottom: 7px">{!! __('Estamos aquí para tí. Cualquier consulta o incidencia nos puedes contactar al centro de atención: :phone_number o mandar un email a :emailto', ['phone_number' => '+34 972 664 640', 'emailto' => '&nbsp;<a href="mailto:reservas@encuentraturetiro.com">reservas@encuentraturetiro.com</a>']) !!}</li>
                                <li>{{ __('Esta reserva está sujeta a los tèrminos y condiciones aceptados en el momento de efectuar la reserva.') }}</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div class="page-break"></div>

    <table style="margin-left: auto; margin-right: auto" width="600px">
        <tr>
            <td colspan="5"><br><br></td>
        </tr>

        <tr>
            <td colspan="5">
                <img src="{{ public_path($_front . 'images/reserva-pdf-arbol.png') }}" width="100%">
            </td>
        </tr>

        <tr>
            <td colspan="5">
                GLOBAL EMOTIMES, S.L. (ENCUENTRA TU RETIRO) es Responsable del tratamiento de sus datos en conformidad con lo que dispone el RGPD, la LOPDGDD y demás normativa legal vigente en materia de protección de datos personales, con la finalidad de mantener una relación comercial con Ud. Los conservará mientras se mantenga esta relación y el tiempo legalmente establecido. No se comunicarán a terceros salvo por obligación legal. Puede ejercer sus derechos de acceso, rectificación, supresión, portabilidad, limitación y oposición a C/ Carme nº 46-48 2-3, 17004 Girona -España-, o enviando un correo electrónico a
                <a href="mailto:info@encuentraturetiro.com">info@encuentraturetiro.com</a>. Para cualquier reclamación puede dirigirse a <a href="https://www.aepd.es">www.aepd.es</a>.
                Para más información puede consultar nuestra política de privacidad en: <a href="{{ get_page_url('politica-de-privacidad') }}">{{ get_page_url('politica-de-privacidad') }}</a>
            </td>
        </tr>

    </table>

    <div style="position: absolute; bottom: 0; right: 0; left: 0">
        <table style="margin-left: auto; margin-right: auto" width="600px">
            <tr>
                <td style="text-align: center; padding-bottom: 8px"><img src="{{ public_path($_front . 'images/logo-gran-final.png') }}" style="width: 128px;"/></td>
            </tr>
            <tr>
                <td style="color: #f9af02; font-weight: 500; text-align: center">{{ __('"Un viaje interior te está esperando..."') }}</td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>