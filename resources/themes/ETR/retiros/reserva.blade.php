@extends('Front::layouts.base')

@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')

@endsection

@section('contingut')

    @includeIf('Front::partials.header')

    <div class="process-steps">
        <div class="row">
            <ul>
                <li>1. @lang('Detalles Retiro')</li>
                <li>2. @lang('Petición de Reserva')</li>
                <li class="active">3. @lang('Aprobación plazas')</li>
                <li>4. @lang('Pago Depósito')</li>
                <li>5. @lang('Confirmación')</li>
            </ul>
        </div>
    </div>

    <section class="retiros-sec no-pt detalles-retiro">
        <div class="row">
            <div class="retro-lft">
                <div class="reserva-done">
                    <h1><img src="{{ asset($_front.'images/tick-yellow.svg') }}"> @lang('Tu solicitud de reserva ha sido enviada')</h1>
                    <p>@lang('Hemos enviado una copia de tu mensaje a tu dirección de correo electrónico: <span>:email</span>', ['email' => auth()->user()->email])</p>

                    <h1>@lang('¿Qué sigue?')</h1>
                    <div class="instructions">
                        <img src="{{ asset($_front.'images/icons/contact.svg') }}">

                        <div>
                            <h3>@lang('El organizador se pondrá en contacto contigo')</h3>
                            <p>@lang('<span>:org_name</span> confirmará su disponibilidad por correo electrónico y resolverá los detalles contigo. Este organizador normalmente responde en un día. Asegúrate de revisar tu bandeja de entrada y la carpeta de spam. También puedes ver todos tus mensajes con los organizadores en tu Área de usuario en la sección de Mensajes.', ['org_name' => $author->fullname()])</p>
                            <p>@lang('Recibirás una notificación y el enlace para reservar por correo electrónico')</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- right side -->
            @include('Front::partials.comanda-rgt')
        </div>
    </section>

@endsection

@section('scripts1')

@endsection