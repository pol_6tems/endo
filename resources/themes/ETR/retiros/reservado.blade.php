@extends('Front::layouts.base')


@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')

@endsection


@section('contingut')
    @includeIf('Front::partials.header')

    <div class="process-steps">
        <div class="row">
            <ul>
                <li>1. @lang('Detalles Retiro')</li>
                @if ($es_reserva_inmediata)
                    <li>2. @lang('Datos Personales')</li>
                @else
                    <li>2. @lang('Petición de Reserva')</li>
                    <li>3. @lang('Aprobación plazas')</li>
                @endif
                <li>{{ $es_reserva_inmediata ? 3 : 4 }}. @lang('Resumen Reserva')</li>
                <li>{{ $es_reserva_inmediata ? 4 : 5 }}. @lang('Pago Depósito')</li>
                <li class="active">{{ $es_reserva_inmediata ? 5 : 6 }}. @lang('Confirmación')</li>
            </ul>
        </div>
    </div>

    <section class="retiros-sec no-pt detalles-retiro confirm">
        <div class="row">
            <div class="retro-lft">
                <div class="reserva-done">
                    <h1><img src="{{ asset($_front.'images/tick-yellow.svg') }}"> @lang('Reserva confirmada')</h1>
                    <p>@lang('Hemos enviado un email con todos los detalles de la reserva a: <span>:email</span>', ['email' => auth()->user()->email])</p>

                    <br>

                    <div class="instructions">
                        <img src="{{ asset($_front.'images/icons/contact.svg') }}">

                        <div>
                            <h3>@lang('El organizador se pondrá en contacto contigo para revisar los detalles de tu llegada')</h3>
                        </div>
                    </div>
                </div>

                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border">
                        <h3>@lang('Te dejamos sus datos por si quieres contactarle directamente')</h3>
                        <ul>
                            <li>{{ $author->fullname() }}</li>
                            <li>{{ $author->get_field('telefono-persona-de-contacto') }}</li>
                            <li>{{ $author->email }}</li>
                        </ul>
                    </div>
                </div>

                <div class="white-box-lft white-box-tit no-pad">
                    <div class="divider no-border">
                        <h3>@lang('Si necesitas cualquier cosa, contacta con nosotros, estamos para ayudarte')</h3>
                        <ul>
                            <li>+34 972 664640</li>
                            <li>info@encuentraturetiro.com</li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- right side -->
            @include('Front::partials.comanda-rgt')
        </div>
    </section>
@endsection

@section('scripts1')
    <script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
@endsection

@section('analytics_ec')
    @if ($es_reserva_inmediata)
        gtag('event', 'purchase', {
            "transaction_id": "{{ $reservation->order_id }}",
            "value": {{ $deposito }},
            "currency": "{{ userCurrency(true) }}",
            "items": [
                {
                    "id": "{{ $retiro->id }}",
                    "name": "{{ $retiro->title }}",
                    "category": "Retiro",
                    "variant": "{{ $es_reserva_inmediata ? 'Reserva inmediata' : 'Con petición de reserva' }}",
                    "price": "{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) }}"
                }
            ]
        });
    @else
    @endif
@endsection