
@extends('Front::layouts.base')


@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
@endsection

@section('contingut')

@includeIf('Front::partials.header')
@if (auth()->check() && ($es_reserva_inmediata || (!empty($retiro_aceptado) && $retiro_aceptado)))


<form id="command-form" action="{{ route('retiros.comprar', $retiro->post_name) }}" method="POST" enctype="multipart/form-data">
@endif
    @csrf
    <input type="hidden" name="retiro" value="{{ $retiro->id }}">
    <input type="hidden" name="preu" value="{{ $precio }}">
    <input type="hidden" name="habitacion" value="{{ !empty($habitacion) ? $habitacion->id : 0 }}">
    <input type="hidden" name="deposito" value="{{ $deposito - $dharmas }}">
    <input type="hidden" name="dharmas" value="{{ $dharmas }}">
    <input type="hidden" name="dia" value="{{ $entrada . '@' . $sortida }}">
    <input type="hidden" name="offer" value="{{ $offer }}">
    <input type="hidden" name="coupon_code" value="">

    @if ($external_order_id)
        <input type="hidden" name="external_order_id" value="{{ $external_order_id }}">
    @endif

    <div class="process-steps">
        <div class="row">
            <ul>
                <li><a href="{{ $retiro->get_url() }}">1. @lang('Detalles Retiro')</a></li>
                @if ($es_reserva_inmediata)
                    <li @if(!auth()->check())class="active"@endif>2. @lang('Datos Personales')</li>
                @else
                    <li @if(!(!empty($retiro_aceptado) && $retiro_aceptado))class="active"@endif>2. @lang('Petición de Reserva')</li>
                    <li>3. @lang('Aprobación plazas')</li>
                @endif
                <li @if(auth()->check() && ($es_reserva_inmediata || (!empty($retiro_aceptado) && $retiro_aceptado)))class="active"@endif>{{ $es_reserva_inmediata ? 3 : 4 }}. @lang('Resumen Reserva')</li>
                <li>{{ $es_reserva_inmediata ? 4 : 5 }}. @lang('Pago Depósito')</li>
                <li>{{ $es_reserva_inmediata ? 5 : 6 }}. @lang('Confirmación')</li>
            </ul>
        </div>
    </div>
    <section class="retiros-sec no-pt detalles-retiro">
        <div class="row">
            <div class="reserva-mbl">
                <div class="reserva">
                    <a href="#input-data" class="submit smooth-link">
                        @Lang('Rellena tus datos')
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="retro-lft">
                <h1 class="big-titlt command-title">{{ $es_reserva_inmediata && !auth()->check() ? __('Datos Personales') : (auth()->check() && ($es_reserva_inmediata || (!empty($retiro_aceptado) && $retiro_aceptado)) ? __('Reserva') : ($es_reserva_inmediata ? __('Resumen Reserva') : __('Resumen Petición de Reserva'))) }}:<br> <span>{{ $retiro->title }}</span></h1>

                @if ((!auth()->check() || $send_request) && !((auth()->check() && $es_reserva_inmediata) || (!empty($retiro_aceptado) && $retiro_aceptado)))
                    <form method="POST" action="{{ route('retiros.detalles.register', $retiro->post_name) }}">
                        <div id="input-data" class="white-box-lft white-box-tit no-pad">
                            <div class="divider no-border">
                                <h2>@Lang('Tus datos')</h2>

                                @csrf
                                <input type="hidden" name="retiro" value="{{ $retiro->id }}">
                                <input type="hidden" name="offer" value="{{ $offer }}">
                                <input type="hidden" name="habitacion[{{ $offer }}]" value="{{ !empty($habitacion) ? $habitacion->id : 0 }}">
                                <input type="hidden" name="precio[{{ $offer }}]" value="{{ $precio }}">
                                <input type="hidden" name="personas[{{ $offer }}]" value="{{ $personas }}">
                                <input type="hidden" name="deposito" value="{{ $deposito }}">
                                <input type="hidden" name="dharmas" value="{{ $dharmas }}">
                                <input type="hidden" name="dia" value="{{ $entrada . '@' . $sortida }}">
                                <input type="hidden" name="pay_days_before" value="{{ $pay_days_before }}">
                                <input type="hidden" name="cancel_policy" value="{{ $cancel_policy }}">
                                <input type="hidden" name="cancel_days_before" value="{{ $cancel_days_before }}">
                                <input type="hidden" name="user_id_filter" value="{{ $author->id }}">

                                <div class="frm-ryt">
                                    <ul>
                                        <li>
                                            <div class="frm-grp">
                                                <label>@Lang('Nombre')</label>
                                                <input type="text" name="nombre" class="frm-ctrl" required value="{{ old('nombre', auth()->user() ? auth()->user()->name : '') }}">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="frm-grp">
                                                <label>@Lang('Apellidos')</label>
                                                <input type="text" name="apellido" class="frm-ctrl" required value="{{ old('apellido', auth()->user() ? auth()->user()->lastname : '') }}">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="frm-grp">
                                                <label>@Lang('Email')</label>
                                                <input type="text" name="correo" class="frm-ctrl" required value="{{ old('correo', auth()->user() ? auth()->user()->email : '') }}">
                                            </div>
                                        </li>
                                        @if (!auth()->user())
                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('Contraseña')</label>
                                                    <input type="password" name="contrasena" class="frm-ctrl" value="" required>
                                                </div>
                                            </li>
                                        @else
                                            @php($birthdate = \Carbon\Carbon::parse(auth()->user()->birthdate))
                                        @endif

                                        <li>
                                            <div class="frm-grp">
                                                <label>@Lang('Fecha de nacimiento')</label>
                                                <div class="col-3">
                                                    <select class="select_box" name="fecha_nacimiento[dia]">
                                                        @if (!isset($birthdate))
                                                            <option value="0" selected>Dia</option>
                                                        @endif
                                                        @for($j = 1 ; $j <= 31; $j++)
                                                            <option value="{{ $j }}" {{ (isset($birthdate) && sprintf('%02d', $j) == $birthdate->format('d')) ? 'selected' : '' }}>{{ $j }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="col-3">
                                                    <select class="select_box" name="fecha_nacimiento[mes]">
                                                        @if (!isset($birthdate))
                                                            <option value="0" selected>Mes</option>
                                                        @endif
                                                        @for($j = 1 ; $j <= 12; $j++)
                                                            <option value="{{ $j }}" {{ (isset($birthdate) && sprintf('%02d', $j) == $birthdate->format('m')) ? 'selected' : '' }}>{{ $j }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="col-3">
                                                    <select class="select_box" name="fecha_nacimiento[ano]">
                                                        @if (!isset($birthdate))
                                                            <option value="0" selected>Año</option>
                                                        @endif
                                                        @for($j = \Carbon\Carbon::now(); $j > \Carbon\Carbon::parse('01-01-1920'); $j->subYear())
                                                            <option value="{{ $j->format('Y') }}" {{ (isset($birthdate) && $j->format('Y') == $birthdate->format('Y')) ? 'selected' : '' }}>{{ $j->format('Y') }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="frm-grp">
                                                <label>@Lang('Género')</label>
                                                <select class="select_box" name="genero">
                                                    <option value="hombre" {{ (auth()->user() && auth()->user()->gender == 'hombre') ? 'selected' : '' }}>@Lang('Hombre')</option>
                                                    <option value="mujer" {{ (auth()->user() && auth()->user()->gender == 'mujer') ? 'selected' : '' }} @if (!auth()->user())selected @endif>@Lang('Mujer')</option>
                                                </select>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="frm-grp">
                                                <label>@Lang('Teléfono')</label>
                                                <input type="text" name="phone-number" class="frm-ctrl" required value="{{ old('phone-number', auth()->user() && auth()->user()->get_field('phone-number') ? auth()->user()->get_field('phone-number') : '') }}">
                                            </div>
                                        </li>

                                        <li>
                                            <div class="frm-grp">
                                                <label>@Lang('Localidad')</label>
                                                <input type="text" name="city" class="frm-ctrl" required value="{{ old('city', auth()->user() ? auth()->user()->city : '') }}">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="frm-grp">
                                                <label>@Lang('País')</label>
                                                <input type="text" name="country" class="frm-ctrl" required value="{{ old('country', auth()->user() ? auth()->user()->country : 'España') }}">
                                            </div>
                                        </li>
                                        <li></li>
                                    </ul>
                                    @if (!$es_reserva_inmediata)
                                        <div class="line-diveder mt-5"></div>
                                        <div class="clear"></div>
                                        <h2>@Lang('¿Algún Mensaje para el organizador?')</h2>
                                        <ul>
                                            <li class="w100">
                                                <div class="frm-grp comanda-mensaje">
                                                    <textarea class="frm-ctrl text-area-small" name="mensaje" placeholder="@Lang('Escribe un mensaje')"></textarea>
                                                </div>
                                            </li>
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="desimiler login-form">
                            <div class="center request-prereserve">
                                <li class="check full-wid">
                                    <input id="policy" type="checkbox" name="policy" value="options" required>
                                    <label for="policy">
                                        <span><span></span></span>
                                        @Lang('He leído y acepto la <a href=":link" target="_blank">Política de Privacidad</a>', [ 'link' => get_page_url('politica-de-privacidad') ])
                                    </label>
                                </li>
        
                                {{--<p>Clickando en el siguiente boton, significa que estas de acuerdo con el precio total, y tambien de
                                    acuerdo con nuestros
                                    <a href="javascript:void(0)">Terminos y condiciones</a>,
                                    <a href="javascript:void(0);">politica de provacidad</a> y
                                    <a href="javascript:void(0);">politica de cancelacion</a>
                                </p>--}}

                                <button id="solicitar" type="submit" class="sub-btn command" >{{ !$es_reserva_inmediata ? __('Enviar Petición') : __('Siguiente paso') }}</button>
                            </div>
                        </div>
                    </form>
                @else
                    @if ( auth()->check() && ($es_reserva_inmediata || (!empty($retiro_aceptado) && $retiro_aceptado)))
                        {{-- Acompanyants --}}
                        <div class="white-box-lft white-box-tit no-pad">
                            <div id="input-data" class="divider no-border">
                                @for ($i = 1; $i <= $personas; $i++)
                                    <h2>@if ($i == 1)@lang('Tus datos')@endif</h2>
                                    @if ($i > 1)
                                        <h3 class="form-small-heading">@Lang('Acompañante') {{ $i - 1 }}</h3>
                                    @endif
                                    <div class="frm-ryt">
                                        <ul>
                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('Nombre')</label>
                                                    <input type="text" name="acompanantes[{{ $i }}][nombre]" class="frm-ctrl" value="{{ ($i == 1) ? auth()->user()->name : '' }}">
                                                </div>
                                            </li>
                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('Apellidos')</label>
                                                    <input type="text" name="acompanantes[{{ $i }}][apellidos]" class="frm-ctrl" value="{{ ($i == 1) ? auth()->user()->lastname : '' }}">
                                                </div>
                                            </li>

                                            @if ($i == 1)
                                                <li>
                                                    <div class="frm-grp">
                                                        <label>@Lang('Email')</label>
                                                        <input type="text" name="acompanantes[{{ $i }}][email]" class="frm-ctrl" value="{{ ($i == 1) ? auth()->user()->email : '' }}">
                                                    </div>
                                                </li>

                                                @php($birthdate = \Carbon\Carbon::parse(Auth::user()->birthdate))
                                                <li class="fecha-nacimiento">
                                                    <div class="frm-grp">
                                                        <label>@Lang('Fecha de nacimiento')</label>
                                                        <div class="col-3">
                                                            <select class="select_box dia" name="acompanantes[{{ $i }}][fecha_nacimiento][dia]">
                                                                @for($j = 1 ; $j <= 31; $j++)
                                                                    <option value="{{ $j }}" {{ ($i == 1 && isset($birthdate) && sprintf('%02d', $j) == $birthdate->format('d')) ? 'selected' : '' }}>{{ $j }}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-3">
                                                            <select class="select_box mes" name="acompanantes[{{ $i }}][fecha_nacimiento][mes]">
                                                                @for($j = 1 ; $j <= 12; $j++)
                                                                    <option value="{{ $j }}" {{ ($i == 1 && isset($birthdate) && sprintf('%02d', $j) == $birthdate->format('m')) ? 'selected' : '' }}>{{ $j }}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-3">
                                                            @php($firstDate = \Carbon\Carbon::parse('01-01-1900'))
                                                            <select class="select_box any" name="acompanantes[{{ $i }}][fecha_nacimiento][ano]">
                                                                @for($j = \Carbon\Carbon::now(); $j > \Carbon\Carbon::parse('01-01-1920'); $j->subYear())
                                                                    <option value="{{ $j->format('Y') }}" {{ ($i == 1 && isset($birthdate) && $j->format('Y') == $birthdate->format('Y')) ? 'selected' : '' }}>{{ $j->format('Y') }}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="frm-grp">
                                                        <label>@Lang('Género')</label>
                                                        <select class="select_box" name="acompanantes[{{ $i }}][genero]">
                                                            <option value="hombre" {{ ($i == 1 && Auth::user()->gender == 'hombre') ? 'selected' : '' }}>@Lang('Hombre')</option>
                                                            <option value="mujer" {{ ($i == 1 && Auth::user()->gender == 'mujer') ? 'selected' : '' }}@if (($i == 1 && !auth()->user()) || $i > 1)selected @endif>@Lang('Mujer')</option>
                                                        </select>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="frm-grp">
                                                        <label>@Lang('Teléfono')</label>
                                                        <input type="text" name="acompanantes[{{ $i }}][telefono]" class="frm-ctrl" required value="{{ ($i == 1) ?  auth()->user()->get_field('phone-number') : '' }}">
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="frm-grp">
                                                        <label>@Lang('Localidad')</label>
                                                        <input type="text" name="acompanantes[{{ $i }}][city]" class="frm-ctrl" required value="{{ ($i == 1) ?  auth()->user()->city : '' }}">
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="frm-grp">
                                                        <label>@Lang('País')</label>
                                                        <input type="text" name="acompanantes[{{ $i }}][country]" class="frm-ctrl" required value="{{ ($i == 1) ? auth()->user()->country : 'España' }}">
                                                    </div>
                                                </li>
                                            @endif
                                            <li></li>
                                        </ul>
                                    </div>
                                @endfor

                                <h2>@Lang('¿Algún Mensaje para el organizador?')</h2>
                                <div class="frm-ryt">
                                    <ul>
                                        <li class="w100">
                                            <div class="frm-grp comanda-mensaje">
                                                <textarea class="frm-ctrl text-area-small" name="mensaje" placeholder="@Lang('Ejemplo: Alergias, Intolerancias...')"></textarea>
                                            </div>
                                        </li>
                                        <li></li>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                        <!-- TARGETA -->
                        {{-- No tenen activat el pagament per web, s'ha de fer redirecció --}}

                        {{--<div class="white-box-lft white-box-tit no-pad">
                            <div class="divider no-border pad-45">
                                <h2>@Lang('Datos de pago seguro')</h2>
                                <div class="card-dtls">
                                    <div class="frm-grp full">
                                        <label>@Lang('Numero de targeta')</label>
                                        <input type="text" name="tarjeta_credito" class="frm-ctrl card-number  credit-card-number" pattern="(\d*\s){3}\d*"
                                            inputmode="numeric" autocomplete="cc-number" autocompletetype="cc-number"
                                            x-autocompletetype="cc-number" required>
                                        <ul class="card-img">
                                            <li class="american-exp"></li>
                                            <li class="visa"></li>
                                            <li class="master"></li>
                                            <li class="discover"></li>
                                            <li class="paypal"></li>
                                        </ul>
                                    </div>
                                    <div class="frm-grp full">
                                        <div class="col-2">
                                            <label>@Lang('Fecha Expiración (MM/AA)')</label>
                                            <input type="text" name="expiracion" class="frm-ctrl expiration-month-and-year" Placeholder="MM/YY" required>
                                        </div>
                                        <div class="col-2">
                                            <label>CVV</label>
                                            <input type="password" name="cvv" class="frm-ctrl security-code " placeholder="CVV"
                                                inputmode="numeric" pattern="\d*" type="text" name="security-code" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-right">
                                    <div class="card-bg">
                                        <div class="chip"></div>
                                        <div class="card-num"><span>0000</span> <span>0000</span> <span>0000</span>
                                            <span>0000</span></div>
                                        <div class="small-number">0000</div>
                                        <div class="validity-ctrl"> <span class="month">MES/AÑO</span> <span class="valid"> FECHA
                                                <br>
                                                VALIDEZ</span> <span class="month-number">00/00</span> </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class="line-diveder"></div>
                                <div class="total">
                                    <div class="left">@Lang('Total')</div>
                                    <div class="right">{{ $deposito }} <i class="fa fa-eur"></i></div>
                                </div>
                            </div>
                        </div>--}}
                        <!-- end TARGETA -->

                        <div class="orange-box-lft">
                            <h4 class="purchase-total">@lang('Total a pagar ahora: ') <span>{{ $currencySymbol }} <span class="js-reserve-button-amount">{{ $deposito - $dharmas }}</span></span></h4>
                            <p>
                                Los <span>{{ $currencySymbol }} {{ (toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) - $deposito) }} restantes</span> se pagarán
                                @if ($pay_days_before)
                                    @lang('al organizador el') <span>{{ \Jenssegers\Date\Date::createFromFormat('d/m/Y', $entrada)->subDays($pay_days_before)->format('j \\d\\e F Y') }}.</span>
                                @else
                                    @lang('directamente en el retiro el') <span>{{ \Jenssegers\Date\Date::createFromFormat('d/m/Y', $entrada)->subDays($pay_days_before)->format('j \\d\\e F Y') }}.</span>
                                @endif
                            </p>
                        </div>
                    @endif

                    <div class="desimiler login-form">
                        <div class="center">
                            <ul>
                                <li class="check full-wid command">
                                    <input id="policy" type="checkbox" name="policy" value="options" required>
                                    <label for="policy">
                                        <span><span></span></span>
                                        @Lang('He leído y acepto la <a href=":link" target="_blank">Política de Privacidad</a>', [ 'link' => get_page_url('politica-de-privacidad') ])
                                    </label>
                                </li>
    
                                @if ( $es_reserva_inmediata )
                                <li class="check full-wid command">
                                    <input id="policy-usr" type="checkbox" name="policy-usr" value="options" required>
                                    <label for="policy-usr">
                                        <span><span></span></span>
                                        @Lang('He leído y acepto los <a href=":link" target="_blank">Términos y Condiciones Usuarios</a>', [ 'link' => get_page_url('terminos-y-condiciones-generales-de-contratacion') ])
                                    </label>
                                </li>
                                @endif
                            </ul>


                            {{--<p>Clickando en el siguiente boton, significa que estas de acuerd con el precio total, y tambien de
                                acuerdo con nuestros
                                <a href="javascript:void(0)">Terminos y condiciones</a>,
                                <a href="javascript:void(0);">politica de provacidad</a> y
                                <a href="javascript:void(0);">politica de cancelacion</a>
                            </p>--}}

                            @if ( auth()->check() && ($es_reserva_inmediata || !empty($retiro_aceptado) && $retiro_aceptado ))
                                <div class="payment-buttons">
                                    <button type="submit" class="sub-btn command">@Lang('Pagar con tarjeta')</button>
                                    <div class="separator"></div>
                                    <div class="paypal-button" id="paypal-button"></div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif

                {{--@if(!(auth()->check() && ($es_reserva_inmediata || (!empty($retiro_aceptado) && $retiro_aceptado))))
                    <div class="white-box-lft white-box-tit no-pad" style="margin-top: 32px">
                        <div class="divider command-divider">
                            <div class="cnt-divider">
                                <h2>@Lang('El Retiro incluye')</h2>
                                @php($incluye = groupCustomList($retiro, 'incluye', 'incluye2'))

                                @if ( !empty($incluye) )
                                    @foreach(array_chunk($incluye, round(count($incluye) / 2, 0)) as $costat)
                                        <div class="ben-lft">
                                            <ul class="benef-lst">
                                                @foreach($costat as $incl)
                                                    <li>{{ $incl['item']['value'] }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <button class="toggle-btn" data-open-txt="@lang('MENOS INFORMACIÓN')" data-closed-txt="@lang('MÁS INFORMACIÓN')">@Lang('MÁS INFORMACIÓN')</button>
                        </div>
                        <div class="divider no-border  command-divider">
                            <div class="cnt-divider">
                                @php($tags = array('tipos-de-retiro', 'categoria', 'beneficio', 'formato', 'arte'))
                                @php($totalTags = collect())
                                @foreach($tags as $tag)
                                    @php($aux = $retiro->get_field($tag) ?: [])
                                    @php($totalTags = $totalTags->merge($aux))
                                @endforeach

                                @if ($totalTags)
                                    <h2>@Lang('Características')</h2>
                                    <ul class="tags">
                                        @foreach($totalTags as $k => $tipo)
                                            @if ($k > 12) @break @endif
                                            <li><a href="javascript:void(0);">{{ $tipo->title }}</a></li>
                                        @endforeach
                                    </ul>
                                @endif

                                @includeIf('Front::partials.carac-list', ['post' => $retiro])

                            </div>
                            <button class="toggle-btn" data-open-txt="@lang('MENOS INFORMACIÓN')" data-closed-txt="@lang('MÁS INFORMACIÓN')">@Lang('MÁS INFORMACIÓN')</button>
                        </div>
                    </div>
                @endif--}}

                <div class="mob-show">
                    @includeIf('Front::partials.carousel-equipo')
                </div>
            </div>
            <!-- right side -->
            @include('Front::partials.comanda-rgt')
        </div>
    </section>

@if (auth()->check() && $es_reserva_inmediata || (!empty($retiro_aceptado) && $retiro_aceptado))
</form>
@endif

@endsection

@section('scripts1')
    <script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
    @if (auth()->check() && ($es_reserva_inmediata || !empty($retiro_aceptado) && $retiro_aceptado ))
        <script src="{{ asset($_front.'js/checkout.js') }}"></script>

        <script>
            var paypalOptions = {
                env: '{{ env('PAYPAL_MODE', 'sandbox') }}',
                client: {
                    '{{ env('PAYPAL_MODE', 'sandbox') }}': '{{ env('PAYPAL_CLIENT_ID') }}'
                },
                post_url: '{{ route('retiros.comprar.ok', ['locale' => app()->getLocale(), 'postName' => $retiro->post_name]) }}'
            };
        </script>
    @endif
    <script>
        $("#command-form").submit(function(e) {
            var dia = $('.fecha-nacimiento').find('select.dia');
            var mes = $('.fecha-nacimiento').find('select.mes');
            var any = $('.fecha-nacimiento').find('select.any');

            var date1 = new Date();
            var date2 = new Date(any.val(), (mes.val() - 1), dia.val());

            var diffTime = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            var years = diffDays / 365;
            if (years < 18) {
                e.preventDefault();
                dia.siblings('.sbHolder').css('border', '1px solid red');
                mes.siblings('.sbHolder').css('border', '1px solid red');
                any.siblings('.sbHolder').css('border', '1px solid red');
                $('html, body').animate({
                    scrollTop: $("#input-data").offset().top
                }, 500);
            } else {
                dia.siblings('.sbHolder').css('border', '1px solid #f2f2f2');
                mes.siblings('.sbHolder').css('border', '1px solid #f2f2f2');
                any.siblings('.sbHolder').css('border', '1px solid #f2f2f2');
            }
        });

        $(document).ready(function() {
            $('.zopim').remove()
        });
    </script>
    {{--<script src="{{ asset($_front.'js/creditly.js') }}"></script>--}}
    <script src="{{ asset($_front.'js/pages/comanda.js') }}"></script>
@endsection


@section('analytics_ec')
    @if ($es_reserva_inmediata)
        @if (!auth()->check() || (auth()->check()  && !$from_login))
            gtag('event', 'begin_checkout', {
                "items": [
                    {
                        "id": "{{ $retiro->id }}",
                        "name": "{{ $retiro->title }}",
                        "category": "Retiro",
                        "variant": "Reserva inmediata",
                        "price": "{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) }}"
                    }
                ]
            });
        @endif

        @if (auth()->check())
            gtag('event', 'checkout_progress', {
                "items": [
                    {
                        "id": "{{ $retiro->id }}",
                        "name": "{{ $retiro->title }}",
                        "category": "Retiro",
                        "variant": "Reserva inmediata",
                        "price": "{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) }}"
                    }
                ]
            });
        @endif
    @else
        @if(!(!empty($retiro_aceptado) && !$retiro_aceptado))
            gtag('event', 'begin_checkout', {
                "items": [
                    {
                        "id": "{{ $retiro->id }}",
                        "name": "{{ $retiro->title }}",
                        "category": "Retiro",
                        "variant": "Con petición de reserva",
                        "price": "{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) }}"
                    }
                ]
            });
        @else
            gtag('event', 'checkout_progress', {
                "items": [
                    {
                        "id": "{{ $retiro->id }}",
                        "name": "{{ $retiro->title }}",
                        "category": "Retiro",
                        "variant": "Con petición de reserva",
                        "price": "{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) }}"
                    }
                ]
            });
        @endif
    @endif
@endsection