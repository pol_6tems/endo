@extends('Front::layouts.base')


@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
    <style>
        .banner-home .banner {
            height: 183px;
            overflow: hidden;
            position: relative;
        }

        .bann-cont h1 {
            font-weight: 500;
        }

        h1 span {
            color: #f9af02;
        }
    </style>
@endsection


@section('contingut')
    @includeIf('Front::partials.header')

    <section class="banner-home banner-home-img resp-banner promociona">
        <div class="banner" style="background-image: url('{{ asset($_front . 'images/valora-header.jpg') }}'); background-position: center; background-size: cover; background-repeat: no-repeat;">
            <div class="bann-cont">
                <div class="row1">
                    <h1>@lang('Tu experiencia <span>guía</span> a otras personas')</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix-block"></div>

    <section class="valora">
        <div class="row1">
            <div class="white-box-lft">
                <div class="retiro-title">
                    <div class="ret-tit-lft description">
                        <h1>{{ $trans->title }}</h1>
                        <ul class="date-icon">
                            @if( $ubicacion )
                                <li class="loc">{{ print_location($ubicacion, ['locality', 'administrative_area_level_3', 'administrative_area_level_2', 'administrative_area_level_1', 'country']) }}</li>
                            @endif
                        </ul>
                    </div>@if ($author)<div class="ret-tit-rgt"> <img src="@if ($author->get_field('foto-organizador')){{ $author->get_field('foto-organizador')->get_thumbnail_url() }}@else{{ $author->get_avatar_url() }}@endif" alt="">
                            <h3>{{ $author->fullname() }}</h3>
                        </div>
                    @endif
                </div>

                <form method="POST">
                    @csrf

                    @if (count($ratings['children']) && !empty( $ratings['children']))
                        <h5>@lang('Valora')</h5>
                        <div class="ratings">
                            <ul>
                                @foreach ( $ratings['children'] as $rat )
                                    <li>
                                        <div class="rat-left">
                                            <h3>{{ $rat->name }}</h3>
                                        </div><div class="rat-right">
                                            <input type="hidden" name="rating[{{ $rat->id }}]" value="1">

                                            @for ( $j = 1; $j <= $rat->maximo; $j++ )
                                                @if (!isset($oneRat))
                                                    @php($oneRat = $rat)
                                                @endif
                                                <a href="javascript:void(0);">
                                                    <img class="rating-img"
                                                         src="@if ($j == 1){{ $rat->icono_full->get_thumbnail_url('thumbnail') }}@else{{ $rat->icono_empty->get_thumbnail_url('thumbnail') }}@endif"
                                                         data-full="{{ $rat->icono_full->get_thumbnail_url('thumbnail') }}"
                                                         data-empty="{{ $rat->icono_empty->get_thumbnail_url('thumbnail') }}"
                                                         data-id="{{ $rat->id }}"
                                                         data-value="{{ $j }}"
                                                    >
                                                </a>
                                            @endfor
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <h5>@lang('Comentarios')</h5>

                    <div class="frm-ryt">
                        <div class="frm-grp">
                            <label>@lang('Me ha gustado')</label>
                            <textarea name="like" class="frm-ctrl text-area"></textarea>
                        </div>
                    </div>

                    <div class="frm-ryt">
                        <div class="frm-grp">
                            <label>@lang('¿Algo para mejorar?')</label>
                            <textarea name="improve" class="frm-ctrl text-area"></textarea>
                        </div>
                    </div>

                    <div class="rat-encuentra-img">
                        <img src="{{ asset($_front.'images/logo-encuentra-tu-retiro.svg') }}" alt="ETR">
                    </div>

                    @if (isset($oneRat))
                        <h5>@lang('Valora')</h5>
                        <div class="ratings">
                            <ul>
                                <li>
                                    <div class="rat-left">
                                        <h3>@lang('Tu experiencia con nosotros')</h3>
                                    </div><div class="rat-right">
                                        <input type="hidden" name="rating[encuentra]" value="1">

                                        @for ( $j = 1; $j <= 5; $j++ )
                                            <a href="javascript:void(0);">
                                                <img class="rating-img"
                                                     src="@if ($j == 1){{ $rat->icono_full->get_thumbnail_url('thumbnail') }}@else{{ $rat->icono_empty->get_thumbnail_url('thumbnail') }}@endif"
                                                     data-full="{{ $oneRat->icono_full->get_thumbnail_url('thumbnail') }}"
                                                     data-empty="{{ $oneRat->icono_empty->get_thumbnail_url('thumbnail') }}"
                                                     data-id="encuentra"
                                                     data-value="{{ $j }}"
                                                >
                                            </a>
                                        @endfor
                                    </div>
                                </li>
                            </ul>
                        </div>
                    @endif

                    <div class="frm-ryt">
                        <div class="frm-grp">
                            <label>@lang('Comentario')</label>
                            <textarea name="comment" class="frm-ctrl text-area"></textarea>
                        </div>
                    </div>

                    <div class="desimiler login-form">
                        <button type="submit" class="sub-btn">@lang('Enviar mi experiencia')</button>
                    </div>

                    <div class="valora-dharmas">
                        <span>
                            <img src="{{ asset($_front . 'images/Dharmas-icon.svg') }}">
                        </span>
                        <p>
                            @lang('Te agradecemos con 300 Dharmas para tu próximo retiros con esta valoración')
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('scripts1')
    <script>
        $(document).on('click', 'img.rating-img', function(){
            var rating_id = $(this).data('id');
            var value = $(this).data('value');
            var fullImg = $(this).data('full');
            var emptyImg = $(this).data('empty');

            $('input[name="rating[' + rating_id + ']"]').val(value);

            $(this).closest('.rat-right').find('.rating-img').each(function () {
                if ($(this).data('value') <= value) {
                    $(this).attr('src', fullImg);
                } else {
                    $(this).attr('src', emptyImg);
                }
            });
        });
    </script>
@endsection