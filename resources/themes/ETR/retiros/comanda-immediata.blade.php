@extends('Front::layouts.base')


@section('styles-parent')
    <link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('styles')
@endsection

@section('contingut')

    @includeIf('Front::partials.header')
    <form id="command-form" action="{{ route('retiros.comprar', $retiro->post_name) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="retiro" value="{{ $retiro->id }}">
        <input type="hidden" name="preu" value="{{ $precio }}">
        <input type="hidden" name="habitacion" value="{{ !empty($habitacion) ? $habitacion->id : 0 }}">
        <input type="hidden" name="deposito" value="{{ $deposito - $dharmas }}">
        <input type="hidden" name="dharmas" value="{{ $dharmas }}">
        <input type="hidden" name="dia" value="{{ $entrada . '@' . $sortida }}">
        <input type="hidden" name="offer" value="{{ $offer }}">
        <input type="hidden" name="coupon_code" value="">

        @if ($external_order_id)
            <input type="hidden" name="external_order_id" value="{{ $external_order_id }}">
        @endif

        <div class="process-steps">
            <div class="row">
                <ul>
                    <li><a href="{{ $retiro->get_url() }}">1. @lang('Detalles Retiro')</a></li>
                    <li class="js-step-2 @if(!auth()->check())active @endif">2. @lang('Datos Personales')</li>
                    <li class="js-step-3 @if(auth()->check() && $es_reserva_inmediata)active @endif">3. @lang('Resumen Reserva')</li>
                    <li>4. @lang('Pago Depósito')</li>
                    <li>5. @lang('Confirmación')</li>
                </ul>
            </div>
        </div>
        <section class="retiros-sec no-pt detalles-retiro">
            <div class="row">
                <div class="reserva-mbl">
                    <div class="reserva">
                        <a href="#input-data" class="submit smooth-link">
                            @Lang('Rellena tus datos')
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="retro-lft">
                    <h1 class="big-titlt command-title">@lang('Reserva'):<br> <span>{{ $retiro->title }}</span></h1>

                    {{-- Acompanyants --}}
                    <div class="white-box-lft white-box-tit no-pad">
                        <div id="input-data" class="divider no-border">
                            @for ($i = 1; $i <= $personas; $i++)
                                <div class="frm-ryt">
                                    @if ($i == 1)
                                        <input type="hidden" name="acompanantes[{{ $i }}][nombre]" class="frm-ctrl js-first-ac-name">
                                        <input type="hidden" name="acompanantes[{{ $i }}][apellidos]" class="frm-ctrl js-first-ac-surname">
                                        <input type="hidden" name="acompanantes[{{ $i }}][email]" class="frm-ctrl js-first-ac-email">
                                        <input type="hidden" name="acompanantes[{{ $i }}][fecha_nacimiento][dia]" class="frm-ctrl js-first-ac-birth-day">
                                        <input type="hidden" name="acompanantes[{{ $i }}][fecha_nacimiento][mes]" class="frm-ctrl js-first-ac-birth-month">
                                        <input type="hidden" name="acompanantes[{{ $i }}][fecha_nacimiento][ano]" class="frm-ctrl js-first-ac-birth-year">
                                        <input type="hidden" name="acompanantes[{{ $i }}][genero]" class="frm-ctrl js-first-ac-genero">
                                        <input type="hidden" name="acompanantes[{{ $i }}][telefono]" class="frm-ctrl js-first-ac-phone">
                                        <input type="hidden" name="acompanantes[{{ $i }}][city]" class="frm-ctrl js-first-ac-city">
                                        <input type="hidden" name="acompanantes[{{ $i }}][country]" class="frm-ctrl js-first-ac-country">
                                            <h2>@lang('Tus datos')</h2>
                                    @else
                                        <h3 class="form-small-heading">@Lang('Acompañante') {{ $i - 1 }}</h3>
                                    @endif

                                    <ul>
                                        @if ($i == 1)
                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('Nombre')</label>
                                                    <input type="text" name="nombre" class="frm-ctrl" required value="{{ old('nombre', auth()->user() ? auth()->user()->name : '') }}">
                                                </div>
                                            </li>
                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('Apellidos')</label>
                                                    <input type="text" name="apellido" class="frm-ctrl" required value="{{ old('apellido', auth()->user() ? auth()->user()->lastname : '') }}">
                                                </div>
                                            </li>
                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('Email')</label>
                                                    <input type="text" name="correo" class="frm-ctrl" required value="{{ old('correo', auth()->user() ? auth()->user()->email : '') }}">
                                                </div>
                                            </li>
                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('Contraseña para tu cuenta de usuario')</label>
                                                    <input type="password" name="contrasena" class="frm-ctrl" value="" required>
                                                </div>
                                            </li>

                                            <li class="fecha-nacimiento">
                                                <div class="frm-grp">
                                                    <label>@Lang('Fecha de nacimiento')</label>
                                                    <div class="col-3">
                                                        <select class="select_box dia" name="fecha_nacimiento[dia]">
                                                            @if (!isset($birthdate))
                                                                <option value="0" selected>Dia</option>
                                                            @endif
                                                            @for($j = 1 ; $j <= 31; $j++)
                                                                <option value="{{ $j }}">{{ $j }}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <div class="col-3">
                                                        <select class="select_box mes" name="fecha_nacimiento[mes]">
                                                            @if (!isset($birthdate))
                                                                <option value="0" selected>Mes</option>
                                                            @endif
                                                            @for($j = 1 ; $j <= 12; $j++)
                                                                <option value="{{ $j }}">{{ $j }}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                    <div class="col-3">
                                                        <select class="select_box any" name="fecha_nacimiento[ano]">
                                                            @if (!isset($birthdate))
                                                                <option value="0" selected>Año</option>
                                                            @endif
                                                            @for($j = \Carbon\Carbon::now(); $j > \Carbon\Carbon::parse('01-01-1920'); $j->subYear())
                                                                <option value="{{ $j->format('Y') }}">{{ $j->format('Y') }}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('Género')</label>
                                                    <select class="select_box" name="genero">
                                                        <option value="hombre" {{ (auth()->user() && auth()->user()->gender == 'hombre') ? 'selected' : '' }}>@Lang('Hombre')</option>
                                                        <option value="mujer" {{ (auth()->user() && auth()->user()->gender == 'mujer') ? 'selected' : '' }} @if (!auth()->user())selected @endif>@Lang('Mujer')</option>
                                                    </select>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('Teléfono')</label>
                                                    <input type="text" name="phone-number" class="frm-ctrl" required value="{{ old('phone-number', auth()->user() && auth()->user()->get_field('phone-number') ? auth()->user()->get_field('phone-number') : '') }}">
                                                </div>
                                            </li>

                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('Localidad')</label>
                                                    <input type="text" name="city" class="frm-ctrl" required value="{{ old('city', auth()->user() ? auth()->user()->city : '') }}">
                                                </div>
                                            </li>
                                            <li>
                                                <div class="frm-grp">
                                                    <label>@Lang('País')</label>
                                                    <input type="text" name="country" class="frm-ctrl" required value="{{ old('country', auth()->user() ? auth()->user()->country : 'España') }}">
                                                </div>
                                            </li>
                                            <li></li>
                                        @else
                                            <div class="frm-ryt">


                                                <ul>
                                                    <li>
                                                        <div class="frm-grp">
                                                            <label>@Lang('Nombre')</label>
                                                            <input type="text" name="acompanantes[{{ $i }}][nombre]" class="frm-ctrl" value="{{ ($i == 1) ? auth()->user()->name : '' }}">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="frm-grp">
                                                            <label>@Lang('Apellidos')</label>
                                                            <input type="text" name="acompanantes[{{ $i }}][apellidos]" class="frm-ctrl" value="{{ ($i == 1) ? auth()->user()->lastname : '' }}">
                                                        </div>
                                                    </li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                        @endif
                                    </ul>
                                </div>
                            @endfor

                            <h2>@Lang('¿Algún Mensaje para el organizador?')</h2>
                            <div class="frm-ryt">
                                <ul>
                                    <li class="w100">
                                        <div class="frm-grp comanda-mensaje">
                                            <textarea class="frm-ctrl text-area-small" name="mensaje" placeholder="@Lang('Ejemplo: Alergias, Intolerancias...')"></textarea>
                                        </div>
                                    </li>
                                    <li></li>
                                </ul>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                    <div class="orange-box-lft">
                        <h4 class="purchase-total">@lang('Total a pagar ahora: ') <span>{{ $currencySymbol }} <span class="js-reserve-button-amount">{{ $deposito - $dharmas }}</span></span></h4>
                        <p>
                            Los <span>{{ $currencySymbol }} {{ (toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) - $deposito) }} restantes</span> se pagarán
                            @if ($pay_days_before)
                                @lang('al organizador el') <span>{{ \Jenssegers\Date\Date::createFromFormat('d/m/Y', $entrada)->subDays($pay_days_before)->format('j \\d\\e F Y') }}.</span>
                            @else
                                @lang('directamente en el retiro el') <span>{{ \Jenssegers\Date\Date::createFromFormat('d/m/Y', $entrada)->subDays($pay_days_before)->format('j \\d\\e F Y') }}.</span>
                            @endif
                        </p>
                    </div>

                    <div class="desimiler login-form">
                        <div class="center request-prereserve">
                            <ul class="js-cb-list">
                                <li class="check full-wid command">
                                    <input id="policy" type="checkbox" name="policy" value="options" required>
                                    <label for="policy">
                                        <span><span></span></span>
                                        @Lang('He leído y acepto la <a href=":link" target="_blank">Política de Privacidad</a>', [ 'link' => get_page_url('politica-de-privacidad') ])
                                    </label>
                                </li>

                                <li class="check full-wid command">
                                    <input id="policy-usr" type="checkbox" name="policy-usr" value="options" required>
                                    <label for="policy-usr">
                                        <span><span></span></span>
                                        @Lang('He leído y acepto los <a href=":link" target="_blank">Términos y Condiciones Usuarios</a>', [ 'link' => get_page_url('terminos-y-condiciones-generales-de-contratacion') ])
                                    </label>
                                </li>
                            </ul>

                            <button id="solicitar" type="button" class="sub-btn command js-reg-button" >@lang('Escoge tu modo de pago')</button>

                            <div class="payment-buttons js-payment-buttons" style="display: none">
                                <button type="submit" class="sub-btn command">@Lang('Pagar con tarjeta')</button>
                                <div class="separator"></div>
                                <div class="paypal-button" id="paypal-button"></div>
                            </div>
                        </div>
                    </div>

                    <div class="mob-show">
                        @includeIf('Front::partials.carousel-equipo')
                    </div>
                </div>
                <!-- right side -->
                @include('Front::partials.comanda-rgt')
            </div>
        </section>

    </form>

@endsection

@section('scripts1')
    <script src="{{ asset($_front.'js/owl.carousel.js') }}"></script>
    <script src="{{ asset($_front.'js/checkout.js') }}"></script>
    <script src="{{ asset($_front.'js/pages/comanda-immediata.js') }}"></script>

    <script>
        new ImmediateOrder({
           paypalOptions: {
               env: '{{ env('PAYPAL_MODE', 'sandbox') }}',
               client: {
                   '{{ env('PAYPAL_MODE', 'sandbox') }}': '{{ env('PAYPAL_CLIENT_ID') }}'
               },
               post_url: '{{ route('retiros.comprar.ok', ['locale' => app()->getLocale(), 'postName' => $retiro->post_name]) }}'
           },
           regUrl: '{{ route('retiros.detalles.ajax-register', ['post_name' => $retiro->post_name]) }}',
           gtagData: {
               "items": [
                   {
                       "id": "{{ $retiro->id }}",
                       "name": "{{ $retiro->title }}",
                       "category": "Retiro",
                       "variant": "Reserva inmediata",
                       "price": "{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) }}"
                   }
               ]
           }
        });
    </script>
@endsection


@section('analytics_ec')

    gtag('event', 'begin_checkout', {
        "items": [
            {
                "id": "{{ $retiro->id }}",
                "name": "{{ $retiro->title }}",
                "category": "Retiro",
                "variant": "Reserva inmediata",
                "price": "{{ toUserCurrency($moneda ? $moneda->get_field('iso-code') : 'EUR', $precio, false) }}"
            }
        ]
    });
@endsection