@extends('Front::layouts.base')

@section('styles-parent')
<link rel="stylesheet" href="{{ asset($_front.'css/style.css') }}" />
@endsection

@section('contingut')
@includeIf('Front::partials.header')
<section class="banner-home banner-home-img resp-banner">
    <div class="home-video">
        <div class="bann-img">
        {{--
            @if ( !empty($trans->get_thumbnail_url()) )
            <img src="{{ $trans->get_thumbnail_url() }}" alt="">
            @endif
        --}}
        </div>
    </div>
</section>
<!-- banner ends -->
<section class="home-title">
    <div class="row1">
        {{--
        <h1>{!! $item->get_field('titulo') !!}</h1>
        --}}
    </div>
</section>
<section class="search-sec voluntar espais">
    <div class="row1">
        <div class="es-pad">
            <ul>
                <li class="loca"><input type="text" placeholder="Localizacion"></li>
                <li class="capa"><input type="text" placeholder="Capacidad"></li>
                <li><input type="submit" value="Buscar"></li>
            </ul>
        </div>
    </div>
</section>
<section class="aun-car categ-sec voluntar">
    <div class="row1">
        {{--
        <h1>{!! $item->get_field('espacios-destacados') !!}</h1>
        <p>{!! $item->get_field('destacados-descripcion') !!}</p>
        --}}

        <div class="obres" id="aun-car1">
            <ul>
            {{--
            @foreach( get_posts(['post_type' => 'espacio', 'numberposts' => 6]) as $espacio )
                @php($translated = $espacio->translate())
                @php($selection = $espacio->get_field('selection'))
                @php($gmaps = $espacio->get_field('ubicacion'))
                <li>
                    <div class="obr-pad">
                        <a href="{{ $espacio->get_url() }}">
                            <div class="obr-img item">
                                @if($translated->media)
                                    <img src="{{ asset($translated->media->get_thumbnail_url()) }}" alt="">
                                @endif
                                <span class="pin">
                                    <img src="{{ asset($_front.'images/pushpin.svg') }}" alt="">
                                    <div class="tooltip"><span class="tooltiptext">ANADIR A LA LISTA</span></div>
                                </span>
                            </div>
                            <div class="obr-cnt">
                                <h1>{{ $translated->title }}</h1>
                                <ul>
                                    <li class="loc">@if ($gmaps->coordenades){{ $gmaps->adreca }}@endif</li>
                                </ul>
                                <p>{{ get_excerpt(strip_tags($translated->description), 40) }}</p>
                            </div>
                                <div class="price">
                                    <div class="p-lft">
                                        <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a>
                                        <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a>
                                        <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a>
                                        <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-full.svg') }}" alt=""></a>
                                        <a href="javascript:void(0);"><img src="{{ asset($_front.'images/heart-empty.svg') }}" alt=""></a>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                @endforeach
                --}}
            </ul>
        </div>
        {{-- <a href="{{ route('retiros.list') }}" class="todo-btn">@Lang('ver todos')</a> --}}
    </div>
</section>
<section class="aun-list eapais">
    <div class="row1">
        <h1>Destinos</h1>
        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed</p>
        <ul>
            <li>
                <a href="javascript:void(0);">
                    <div class="img-div">
                        <img src="{{ asset($_front.'images/spain.jpg') }}" alt="">
                        <div class="list-cont">
                            <h2>Spain</h2>
                        </div>
                    </div>
                    <span class="exp-btn">EXPLORA</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <div class="img-div">
                        <img src="{{ asset($_front.'images/mexico.jpg') }}" alt="">
                        <div class="list-cont">
                            <h2>Mexico</h2>
                        </div>
                    </div>
                    <span class="exp-btn">EXPLORA</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <div class="img-div">
                        <img src="{{ asset($_front.'images/norway.jpg') }}" alt="">
                        <div class="list-cont">
                            <h2>Norway</h2>
                        </div>
                    </div>
                    <span class="exp-btn">EXPLORA</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <div class="img-div">
                        <img src="{{ asset($_front.'images/bali.jpg') }}" alt="">
                        <div class="list-cont">
                            <h2>Bali</h2>
                        </div>
                    </div>
                    <span class="exp-btn">EXPLORA</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <div class="img-div">
                        <img src="{{ asset($_front.'images/italy.jpg') }}" alt="">
                        <div class="list-cont">
                            <h2>Italy</h2>
                        </div>
                    </div>
                    <span class="exp-btn">EXPLORA</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <div class="img-div">
                        <img src="{{ asset($_front.'images/tibet.jpg') }}" alt="">
                        <div class="list-cont">
                            <h2>Tibet</h2>
                        </div>
                    </div>
                    <span class="exp-btn">EXPLORA</span>
                </a>
            </li>
        </ul>
        <a href="javascript:void(0);" class="todo-btn">ver todos los destinos</a>
    </div>
</section>
@includeIf('Front::partials.banner-inferior')
@endsection