@extends('Front::layouts.main')

@section('content')
    <div class="content-pad">
        <div class="row">
            <h1>{{ $home->title }}</h1>
            <p>
                {!! $home->description !!}
            </p>
            <ul>
                @foreach($brands as $brand)
                    <li>
                        <a href="{{ route('brand', ['locale' => app()->getLocale(), 'brand' => $brand->getTranslation()->post_name]) }}">
                            <div class="pro-imgg">
                                <img src="@if ($brand->translate()->media){{ $brand->translate()->media->get_thumbnail_url() }}@endif" alt="">
                                <div class="img-cnt">
                                    <p>{{ $brand->title }}</p>
                                </div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

    <footer>
        <div class="row">
            <div class="foot-r">
                <ul>    
                    <li>
                        <a href="https://www.runtal.es/" target="_blank"><img src="{{ asset($_front.'images/runtal-logo-mini.png') }}" alt=""></a>
                    </li>
                    <li>
                        <p>y</p>
                    </li>
                    <li>
                        <a href="https://www.zehnder.es/" target="_blank"><img src="{{ asset($_front.'images/zehnder-logo-mini.png') }}" class="zen-mini" alt=""></a>
                    </li>
                    <li>
                        <p>son marcas de Zehnder Group</p>
                    </li>
                </ul>
            </div>
            <div class="foot-l">
                <h2>ZEHNDER GROUP IBÉRICA IC, S.A.</h2>
                <p>C/ Argenters, 7 Parque Tecnológico del Vallés<br>
                    ES-08290 Cerdanyola - BCN<br>
                    TEL +34 900 700 110</p>
                <a href="mailto:customerservice.zges@zehndergroup.com">customerservice.zges@zehndergroup.com</a>
                <a href="https://www.runtal.es/" target="_blank">www.runtal.es</a>
                <a href="https://www.zehnder.es/" target="_blank">www.zehnder.es</a>
            </div>

        </div>
    </footer>
@endsection
