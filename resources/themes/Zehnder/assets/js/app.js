
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

/*require('./bootstrap');*/

require('./plugins/html5');
window.$ = window.jQuery = require('jquery');

require('./plugins/easyResponsiveTabs');
require('./plugins/smk-accordion');
require('./plugins/jquery.mmenu.min.all');

require('./common');
