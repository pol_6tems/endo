@extends('Front::layouts.main')

@section('content')
    <div class="pro-detail">
        <div class="row">
            <div id="verticalTab">
                <div id="verticalTab">
                    <div class="pro-left">
                        <div class="logo-imgs">
                            <a href="{{ route('index') }}"><img src="{{ asset('Themes/Zehnder/images/runtal-logo-2.png') }}"></a>
                            <a href="{{ route('index') }}" class="zender"><img src="{{ asset('Themes/Zehnder/images/zehder-logo-2.png') }}"></a>
                        </div>

                        <ul class="resp-tabs-list">
                            @foreach($brands as $brandItem)
                                <li class="js-brand-item @if ($brand && $brandItem->id == $brand->id)js-default-tab @endif" data-post-name="{{ $brandItem->post_name }}">{{ $brandItem->title }}</li>
                            @endforeach

                            @foreach($videos as $videoItem)
                                <li class="js-brand-item @if ($video && $videoItem->id == $video->id)js-default-tab @endif" data-post-name="videos-{{ $videoItem->post_name }}">{{ $videoItem->title }}</li>
                            @endforeach
                        </ul>

                        <div class="pro-footer" id="des">
                            <div class="foot-r">
                                <ul>
                                    <li>
                                        <a href="https://www.runtal.es/" target="_blank"><img src="{{ asset('Themes/Zehnder/images/runtal-logo-mini.png') }}" alt=""></a>
                                    </li>
                                    <li>
                                        <p>y</p>
                                    </li>
                                    <li>
                                        <a href="https://www.zehnder.es/" target="_blank"><img src="{{ asset('Themes/Zehnder/images/zehnder-logo-mini.png') }}" class="zen-mini" alt=""></a>
                                    </li>
                                    <li>
                                        <p>son marcas de Zehnder Group</p>
                                    </li>
                                </ul>
                            </div>

                            <div class="foot-l">
                                <h2>ZEHNDER GROUP IBÉRICA IC, S.A.</h2>
                                <p>C/ Argenters, 7 Parque Tecnológico del Vallés<br>
                                    ES-08290 Cerdanyola - BCN<br>
                                    TEL +34 900 700 110</p>
                                <a href="mailto:customerservice.zges@zehndergroup.com">customerservice.zges@zehndergroup.com</a>
                                <a href="https://www.runtal.es/" target="_blank">www.runtal.es</a>
                                <a href="https://www.zehnder.es/" target="_blank">www.zehnder.es</a>
                            </div>
                        </div>
                    </div>

                    <div class="pro-right">
                        <div class="resp-tabs-container">
                            @foreach($brands as $brandItem)
                                <div>
                                    <h1>{{ $brandItem->title }}</h1>
                                    <div class="documen-pa">
                                        <p>Seleccionar carpeta</p>
                                        <p class="rgt">Total Documentos</p>
                                    </div>

                                    <div class="accordion_example1">
                                        @foreach($brandItem->categories as $category)
                                            @if ($category)
                                                <div class="accordion_in">
                                                    <div class="acc_head">
                                                        <p class="d-p-lft"><span>{{ $category->title }}</span>&nbsp;&nbsp; {{ $category->get_field('subtitulo') }}</p>
                                                        <p class="d-p-rgt">{{ $category->products->count() + $category->subcategories->sum(function ($subcategory) { return $subcategory->products->count(); }) }}</p>
                                                    </div>
                                                    <div class="acc_content">
                                                        <ul>
                                                            @foreach($category->products as $product)
                                                                <li>
                                                                    <a href="@if ($doc = $product->get_field('documento')){{ $doc->get_thumbnail_url() }}@else javascript:void(0);@endif" target="_blank">
                                                                        <div class="doc-img">
                                                                            @if ($product->translate()->media)
                                                                                <img src="{{ $product->translate()->media->get_thumbnail_url() }}" alt="">
                                                                            @endif
                                                                        </div>
                                                                        <div class="doc-cnt">
                                                                            <p>{{ $product->title }}</p>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            @endforeach

                                                            @foreach($category->subcategories as $subcategory)
                                                                <li class="subcategory">
                                                                    <h4>{{ $subcategory->title }}</h4>

                                                                    <ul class="subcategory-child" style="display: none">
                                                                        @foreach($subcategory->products as $product)
                                                                            <li>
                                                                                <a href="@if ($doc = $product->get_field('documento')){{ $doc->get_thumbnail_url() }}@else javascript:void(0);@endif" target="_blank">
                                                                                    <div class="doc-img">
                                                                                        @if ($product->translate()->media)
                                                                                            <img src="{{ $product->translate()->media->get_thumbnail_url() }}" alt="">
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="doc-cnt">
                                                                                        <p>{{ $product->title }}</p>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach

                            @foreach($videos as $videoItem)
                                <div class="videos-detail">
                                    <h1>{{ $videoItem->title }}</h1>

                                    <ul>
                                        @php($enlaces = $videoItem->get_field('enlaces'))
                                        @if ($enlaces)
                                            @foreach($enlaces as $enlace)
                                                @if (isset($enlace['enlace']['value']) && is_string($enlace['enlace']['value']) && getYoutubePreviewUrl($enlace['enlace']['value']))
                                                    <li>
                                                        <a href="{{ $enlace['enlace']['value'] }}" target="_blank" title="{{ $enlace['titulo']['value'] }}">
                                                            <div>
                                                                <div class="player">
                                                                    <img src="{{ getYoutubePreviewUrl($enlace['enlace']['value']) }}">

                                                                    <div class="center-img">
                                                                        <img src="{{ asset('Themes/Zehnder/images/yt_icon_rgb.png') }}">
                                                                    </div>
                                                                </div>

                                                                <p>{{ cut_text($enlace['titulo']['value'], 60) }}</p>
                                                            </div>
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('bottom_foot_scripts')
    <script type="text/javascript">
        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });

        $(".accordion_example1").smk_Accordion({
            closeAble: true, //boolean
        });

        var windowSize = $(window).height();

        if (windowSize >767) {
            $('.pro-right').css({ height: $(document).height() });
        }

        if (windowSize <= 828) {
            $('.pro-left').css({ 'overflow-y': 'scroll', top:0, bottom:0, });
            $('.pro-footer').css("position", "static");
        } else {
            $('.pro-left').css({ height: $(window).innerHeight() });
            $('.pro-footer').css("position", "absolute");
        }

        $('.js-default-tab').click();

        $('.subcategory h4').on('click', function (e) {
            var subCatChild = $(this).parent().find('.subcategory-child');

            var isVisible = subCatChild.is(':visible');

            $('.subcategory-child').slideUp('fast');

            if (!isVisible) {
                $(this).parent().find('.subcategory-child').slideDown('fast');
            }
        });

        $('li.js-brand-item').on('click', function () {
            var url = new URL(window.location.href);

            var currPathExploded = url.pathname.split('/');
            currPathExploded.pop();
            currPathExploded.push($(this).data('post-name'));

            url.pathname = currPathExploded.join('/');

            window.history.pushState({},"", decodeURIComponent(url.href));
        });
    </script>
@endsection