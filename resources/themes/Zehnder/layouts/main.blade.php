@extends('Front::layouts.html')

@section('title', config('app.name'))

@section('body')
    <div id="page">
        @if (route_name('index'))
            @include('Front::partials.header')
        @endif

        @yield('content')

    </div>
@endsection