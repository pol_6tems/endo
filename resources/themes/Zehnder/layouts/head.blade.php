<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<link rel="icon" type="image/x-icon" href="{{ asset($_front.'images/favicons/favicon.ico') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset($_front.'images/favicons/favicon-96x96.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset($_front.'images/favicons/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset($_front.'images/favicons/apple-icon-180x180.png') }}">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield("title", config('app.name', 'Endo'))</title>

<link href="{{ mix('css/app.css', 'Themes/' . env('PROJECT_NAME')) }}" rel="stylesheet">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-153264886-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-153264886-1');
</script>

@yield('head_metas')