@extends('Front::layouts.registre_login')

@section('content')
<section class="area-container">
	<div class="area-content">
		<h2>@Lang('Private area')</h2>
		<form method="get" action="{{ route('acces.dni') }}">
			<img src="{{asset('Themes/Batalle/images/BATALLE-INTRANET-logo-VERMELL.png')}}" alt="BATALLE-INTRANET-logo-VERMELL">
			<h3>{{__('Sign up for the workers area')}}</h3>
			@if (session('message'))
				<div class="alert alert-info">{{ session('message') }}</div>
				<br/>
			@endif
			<input type="text" name="dni" class="frm-ctrl" Value="@Lang('DNI')"  onBlur="if (this.value == ''){this.value = '@Lang('DNI')'; }" onFocus="if (this.value == '@Lang('DNI')') {this.value = ''; }">
			<div class="area-style">
				<select onchange="redireccionar(this);">
					@foreach ($_languages as $language)
                        <option data-url="{{$language->url}}" value="{{$language->code}}" {{$language->code == app()->getLocale() ? 'selected' : ''}}>@lang($language->name)</option>
                    @endforeach
				</select>
			</div>
			<button type="submit">@Lang('Enter')</button>
			<div class="clear"></div>
			<p>@Lang('acces-footer', ['url' => \App\Post::get_url_by_post_id(10)])</p>
		</form>
	</div>
</section>
@endsection

@section("scripts")
<script>
	function redireccionar(e) {
		$url = $(e).find(":selected").data('url');
		window.location.href = $url;
	}
</script>
@endsection