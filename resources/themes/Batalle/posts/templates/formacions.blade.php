@extends('Front::layouts.app')

@section('header')
<div class="title-head cal-ico" style="background-image: url({{ $trans->get_thumbnail_url() }});">
	<h1>{{ $trans->title }}</h1>              
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
@endsection

@section('content')
@php
$descomptes = \App\Post::get_posts([['type', 'formacio']]);
@endphp
<section class="formacio">
    <div class="col-lft">
        <div class="row1">
            {!! $trans->description !!}
        </div>
    </div>
    <!-- <div class="col-rgt">
        <div class="row1">
            <div class="dropdown-hotel">
                <a href="javascript:void(0);" class="arw-toggle"></a>
                <div class="title-box">
                    <h2 id="hotel-name" onFocus="this.blur()">Llegenda</h2>
                </div>
                <ul id="hotel-list" style="display:none;">
                    <li class="lightpink"><a href="javascript:void(0);">Avui <span></span></a></li>
                </ul>
            </div>
        </div>
    </div> -->
</section>
<section class="formacio llista-formacio">
    <div class="row1">
        @foreach ($descomptes as $des)
            @php
            $d_trans = $des->translate(true);
            $preguntes = $des->get_field('form');
            if ( !is_array($preguntes) ) $preguntes = (array)json_decode( $preguntes );
            $custom_field_id = $des->get_field_id('form');
            @endphp
            <article id="{{ $des->id }}">
                <div class="header-image" style="@if ($d_trans->media) background-image: url({{ $d_trans->media->get_thumbnail_url('medium') }}) @else background-color: lightgray; @endif" ></div>
                <div class="content">
                    <span class="date">{{ $des->created_at }}</span>
                    <h2>{{ $d_trans->title }}</h2>
                    <p class="descripcio">
                        {!! $d_trans->description !!}
                    </p>
                    @if ($des->get_field('fitxer'))
                        @php
                        $media = \App\Models\Media::where('id', $des->get_field('fitxer'))->first();
                        @endphp
                        @if ($media)
                            <a href="{{ $media->get_thumbnail_url() }}" target="_blank" class="formacio link">@Lang('Accedir a la formació')</a>
                        @endif
                    @elseif ( !empty( $des->get_field('url') ) )
                        <a href="{{ $des->get_field('url') }}" target="_blank" class="formacio">@Lang('Accedir a la formació')</a>
                    @endif
                    @if ( !empty($preguntes) )
                        <a href="javascript:veure_enquesta( {{ $des->id }} );" target="_blank" class="formacio" style="margin-top: 5px;">
                            @Lang('Realitza test de formació')
                        </a>
                    @endif
                </div>
                <div class="checkboxes">
                    @php
                    $vista = \App\Post::formacio_vista($des->id);
                    @endphp
                    <input id="chk_{{ $des->id }}" onclick="accedirFormacio(event)" type="checkbox" name="acceptacio" {{ $vista }}>
                    <label for="chk_{{ $des->id }}">
                        <span></span>
                        @Lang('He llegit i entés la formació')</label>
                </div>
            </article>
        @endforeach
    </div>

    <!-- Enquestes -->
    @foreach ($descomptes as $des)
        @php
        $d_trans = $des->translate(true);
        $preguntes = $des->get_field('form');
        if ( !is_array($preguntes) ) $preguntes = (array)json_decode( $preguntes );
        $custom_field_id = $des->get_field_id('form');
        @endphp
        @if ( !empty($preguntes) )
        <div class="col-rgt formacio-enquesta" id="enquesta-{{ $des->id }}">
            <h4>
                {{ $d_trans->title }}
            </h4>
            <form id="formulari_enquesta">
                <div class="formulari">
                    <div class="form-pad">
                        <input type="hidden" name="post_id" value="{{ $des->id }}">
                        <input type="hidden" name="custom_field_id" value="{{ $custom_field_id }}">
                        <ul>
                            @foreach ($preguntes as $k => $p)
                                <li class="textarea">
                                    @if ( !empty($p->question) )
                                        <span><b>{{$k+1}}. {{$p->question}}</b></span>
                                    @endif
                                    @if ( $p->answer == 'text' )
                                        <input type="text" class="txt-box" name="resultats[{{$k}}]" value="" required>
                                    @elseif ( $p->answer == 'textarea' )
                                        <textarea class="txt-box" name="resultats[{{$k}}]"></textarea>
                                    @elseif ( $p->answer == 'checkbox' )
                                        <br>
                                        @php ( $choices = preg_split('/\n|\r\n?/', $p->choices) )
                                        @foreach ($choices as $k2 => $choice)
                                            @php ( $c_parts = explode(" : ", $choice) )
                                            <div class="checkbox">
                                                <input onclick="change_check(this, {{$k}});" class="check-{{$k}}" id="check-{{$k}}-{{$k2}}" type="checkbox" name="resultats[{{$k}}]" value="{{ $c_parts[0] }}">
                                                <label for="check-{{$k}}-{{$k2}}">{{ $c_parts[1] }}</label>
                                            </div>
                                        @endforeach
                                    @endif
                                </li>
                            @endforeach
                            <li class="last-btn">
                                <input onclick="veure_enquesta( {{ $des->id }} );enviar_enquesta();" class="enviar_mensaje_btn" type="button" value="Enviar">
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
        @endif
    @endforeach
    <!-- end Enquestes -->

</section>

<div id="pop-info-session-message" class="simplePopup especific" style=" margin: 25% auto;width: 70%;">
    <div class="desktop_pop_info">
        <a href="javascript:void(0);" class="close-btn"><img src="{{asset($_front.'images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
        <ul>
            <span style="color:#000;" class="missatge"></span>
        </div>
        <div class="mobile_pop_info" style="display: none;">
            <a href="javascript:void(0);" class="close-btn"><img src="{{asset($_front.'images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
        <div class="mbl_pop_inner">
        </div>
    </div>
</div>
@endsection

@section('scripts2')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function accedirFormacio(e) {
        var $parent = $(e.target).parents('article');
        var $id = $parent.attr('id');
        var $href = $parent.find('.formacio.link').attr('href');

        $.ajax({
            url: '{{ route('formacions.store') }}',
            method: 'POST',
            data: {
                '_token': '{{csrf_token()}}',
                'id': $id
            },
            success: function(data) {
                $(e.target).prop('checked', 'checked');
            }
        });
        e.preventDefault();
    }

    // mobile dropdown
    $("#hotel-name, .arw-toggle").click(function() {
        $("#hotel-list").slideToggle();
        $(".dropdown").slideUp();
    });
</script>

<script>
    var $active_item = null;
    $('ul.item-list li').click(function() {
        $('.descomptes ul.item-list li').removeClass('active');
        $('.descomptes').removeClass('active');
        $('.descomptes .col-rgt').children().hide();
        
        if ( $active_item != $(this).attr('id') ) {
            $active_item = $(this).attr('id');
            $(this).addClass('active');
            $('.descomptes').addClass('active');
            $('.descomptes .col-rgt').children().show();
            var date = $(this).find('input[type=hidden][name=date]').val();
            var description = $(this).find('div.description').html();
            $('.col-rgt .date').html(date);
            $('.col-rgt .description').html(description);
        } else {
            $active_item = null;
        }
    });
</script>
<script>
/* ENQUESTA */
function veure_enquesta( formacio_id ) {
    var right_desplegat = '0px';
    var right_plegat = '-10000px';
    var is_desplegat = $('#enquesta-' + formacio_id).css('right') == right_desplegat;
    var height = $('.relative-pad').css('min-height');
    $('.formacio-enquesta').css('min-height', height);
    $('.formacio-enquesta').css('right', right_plegat);
    
    if ( !is_desplegat ) $('#enquesta-' + formacio_id).css('right', right_desplegat);
}
function change_check(e, num) {
    var check_id = e.id;
    if ( e.checked ) {
        $('.check-' + num).prop('checked', false);
        $('#formulari_enquesta #' + check_id).prop('checked', true);
        e.checked = true;
    }
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function enviar_enquesta() {
    //console.log(respostes);
    var boto = $(this);
    var post_id = $('#formulari_enquesta').find('input[name=post_id]').val();
    var custom_field_id = $('#formulari_enquesta').find('input[name=custom_field_id]').val();
    var respostes = get_respostes_enquesta();
    $.ajax({
        url: '{{ route('admin.user_meta.store') }}',
        method: 'POST',
        data: {'_token': '{{csrf_token()}}', 'post_id': post_id, 'custom_field_id': custom_field_id, 'respostes': respostes},
        success: function(data) {
            obrir_popup("{!!__('Formation sent successfully')!!}");
            boto.remove();
        },
        error: function(data) {
            obrir_popup("@Lang('Error saving data')");
        }
    });
}
function obrir_popup(missatge) {
    $('#pop-info-session-message.especific span.missatge').html( missatge );
    $('#pop-info-session-message.especific').addClass('p-open');
    $('body').addClass('overlay');
    var width_pg = $(window).width();
}

$('#pop-info-session-message.especific .close-btn img').click(function(){
    $('#pop-info-session-message.especific').removeClass('p-open');
    $('body').removeClass('overlay');
    var width_pg = $(window).width();
});

function get_respostes_enquesta() {
    return $('#formulari_enquesta').serializeArray();
}
/* end ENQUESTA */
</script>
@endsection