@extends('Front::layouts.app')

@section('header')
<div class="title-head cal-ico" style="background-image: url({{ $trans->get_thumbnail_url() }});">
	<h1>{{ $trans->title }}</h1>
</div>
@endsection

@section('content')
@php ( $noticies = \App\Post::get_posts([['type', 'noticia']]))
<section class="descomptes">
    <div class="col-lft">
        <div class="">
            {!! $trans->description !!}
        </div>
        <ul class="item-list">
            @foreach ($noticies as $noticia)
                @php( $d_trans = $noticia->translate(true) )
                <li id="{{ $noticia->id }}">
                    <img src="{{ $d_trans->get_thumbnail_url('medium_large') }}">
                    <h4>
                        <span>{{ $noticia->get_field('data') }}</span>
                        {{ $d_trans->title }}
                    </h4>
                    <input type="hidden" name="date" value="{{ $noticia->get_field('data') }}">
                    <input type="hidden" name="title" value="{{ $d_trans->title }}">
                    <div class="description" style="display: none !important;">
                        {!! $d_trans->description !!}
                        <br><br>
                        <img src="{{ $d_trans->get_thumbnail_url('medium_large') }}" style="width: 100%;">
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="col-rgt">
        <div class="">
            <span class="date"></span>
            <h4 class="title"></h4>
            <p class="description"></p>
        </div>
    </div>
</section>
@endsection

@section('scripts2')
<script>
    var $active_item = null;
    $('ul.item-list li').click(function() {
        $('.descomptes ul.item-list li').removeClass('active');
        $('.descomptes').removeClass('active');
        $('.descomptes .col-rgt').children().hide();
        
        if ( $active_item != $(this).attr('id') ) {
            $active_item = $(this).attr('id');
            $(this).addClass('active');
            $('.descomptes').addClass('active');
            $('.descomptes .col-rgt').children().show();
            var date = $(this).find('input[type=hidden][name=date]').val();
            var title = $(this).find('input[type=hidden][name=title]').val();
            var description = $(this).find('div.description').html();
            $('.col-rgt .title').html(title);
            $('.col-rgt .date').html(date);
            $('.col-rgt .description').html(description);
        } else {
            $active_item = null;
        }
        if ( $(document).innerWidth() < 768 ) {
            $('html, body').animate({
                scrollTop: $('.col-rgt').offset().top
            });
        }
    });
</script>
@endsection