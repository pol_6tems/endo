@extends('Front::layouts.app')

@section('header')
<div class="title-head cal-ico" style="background-image: url({{ $trans->get_thumbnail_url() }});">
	<h1>{{ $trans->title }}</h1>              
</div>
@endsection

@section('content')
@php
$descomptes = \App\Post::get_posts([['type', 'enquesta']]);
@endphp
<section class="descomptes enquestes">
    <div class="col-lft">
        <div class="">
            {!! $trans->description !!}
        </div>
        <ul class="item-list">
            @foreach ($descomptes as $des)
            @php
                $d_trans = $des->translate(true);
                $data = $des->get_field('data');
                $preguntes = $des->get_field('form');
                if ( !is_array($preguntes) ) $preguntes = (array)json_decode( $preguntes );
                $custom_field_id = $des->get_field_id('form');
            @endphp
                
                <li id="{{ $des->id }}">
                    <h4>
                        <span>{{$data}}</span>
                        {{ $d_trans->title }}
                    </h4>
                    <input type="hidden" name="date" value="{{ $data }}">
                    <input type="hidden" name="title" value="{{ $d_trans->title }}">
                    <div class="description" style="display: none !important;">
                        {!! $d_trans->description !!}
                    </div>
                    <div class="formulari" style="display: none !important;">
                        <div class="form-pad">
                            <input type="hidden" name="post_id" value="{{ $des->id }}">
                            <input type="hidden" name="custom_field_id" value="{{ $custom_field_id }}">
                            <ul>
                                @foreach ($preguntes as $k => $p)
                                    <li class="textarea">
                                        @if ( !empty($p->question) )
                                            <span><b>{{$k+1}}. {{$p->question}}</b></span>
                                        @endif
                                        @if ( $p->answer == 'text' )
                                            <input type="text" class="txt-box" name="resultats[{{$k}}]" value="" required>
                                        @elseif ( $p->answer == 'textarea' )
                                            <textarea class="txt-box" name="resultats[{{$k}}]"></textarea>
                                        @elseif ( $p->answer == 'checkbox' )
                                            <br>
                                            @php ( $choices = preg_split('/\n|\r\n?/', $p->choices) )
                                            @foreach ($choices as $k2 => $choice)
                                                @php ( $c_parts = explode(" : ", $choice) )
                                                <div class="checkbox">
                                                    <input onclick="change_check(this, {{$k}});" class="check-{{$k}}" id="check-{{$k}}-{{$k2}}" type="checkbox" name="resultats[{{$k}}]" value="{{ $c_parts[0] }}">
                                                    <label for="check-{{$k}}-{{$k2}}">{{ $c_parts[1] }}</label>
                                                </div>
                                            @endforeach
                                        @endif
                                    </li>
                                @endforeach
                                <li class="last-btn">
                                    <input onclick="enviar_enquesta();" class="enviar_mensaje_btn" type="button" value="Enviar">
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    
    <div class="col-rgt">
        <div class="">
            <span class="date"></span>
            <h4 class="title"></h4>
            <p class="description"></p>
        </div>
        <form id="formulari_enquesta"></form>
    </div>
</section>

<div id="pop-info-session-message" class="simplePopup especific" style=" margin: 25% auto;width: 70%;">
    <div class="desktop_pop_info">
        <a href="javascript:void(0);" class="close-btn"><img src="{{asset($_front.'images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
        <ul>
            <span style="color:#000;" class="missatge"></span>
        </div>
        <div class="mobile_pop_info" style="display: none;">
            <a href="javascript:void(0);" class="close-btn"><img src="{{asset($_front.'images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
        <div class="mbl_pop_inner">
        </div>
    </div>
</div>
@endsection

@section('styles')
@endsection

@section('scripts2')
<script>
    $('.col-lft ul.item-list li').click(function() {
        if ( !$(this).hasClass('textarea') ) {
            if ( !$(this).hasClass('active') ) {
                $('.col-lft ul.item-list li').removeClass('active');
                $(this).addClass('active');
                $('.descomptes').addClass('active');
                $('.descomptes .col-rgt').children().show();
                var date = $(this).find('input[type=hidden][name=date]').val();
                var title = $(this).find('input[type=hidden][name=title]').val();
                var description = $(this).find('div.description').html();
                var formulari = $(this).find('div.formulari').html();
                $('.col-rgt .date').html(date);
                $('.col-rgt .title').html(title);
                $('.col-rgt .description').html(description);
                $('.col-rgt #formulari_enquesta').html(formulari);
            }/* else {
                $('.col-lft ul.item-list li').removeClass('active');
                $('.descomptes').removeClass('active');
                $('.descomptes .col-rgt').children().hide();
                $active_item = null;
            }*/
        }
    });
    function change_check(e, num) {
        var check_id = e.id;
        if ( e.checked ) {
            $('.check-' + num).prop('checked', false);
            $('#formulari_enquesta #' + check_id).prop('checked', true);
            e.checked = true;
        }
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function enviar_enquesta() {
        console.log(respostes);
        var boto = $(this);
        var post_id = $('#formulari_enquesta').find('input[name=post_id]').val();
        var custom_field_id = $('#formulari_enquesta').find('input[name=custom_field_id]').val();
        var respostes = get_respostes_enquesta();
        $.ajax({
            url: '{{ route('admin.user_meta.store') }}',
            method: 'POST',
            data: {'_token': '{{csrf_token()}}', 'post_id': post_id, 'custom_field_id': custom_field_id, 'respostes': respostes},
            success: function(data) {
                obrir_popup("{!!__('Survey sent successfully')!!}");
                boto.remove();
            },
            error: function(data) {
                obrir_popup("@Lang('Error saving data')");
            }
        });
    }

    function obrir_popup(missatge) {
        $('#pop-info-session-message.especific span.missatge').html( missatge );
        $('#pop-info-session-message.especific').addClass('p-open');
        $('body').addClass('overlay');
		var width_pg = $(window).width();
    }

    $('#pop-info-session-message.especific .close-btn img').click(function(){
        $('#pop-info-session-message.especific').removeClass('p-open');
        $('body').removeClass('overlay');
        var width_pg = $(window).width();
    });

    function get_respostes_enquesta() {
        return $('#formulari_enquesta').serializeArray();
    }
</script>
@endsection