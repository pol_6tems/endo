@extends('Front::layouts.app')

@section('header')
<div class="title-head cal-ico" style="background-image: url({{ $trans->get_thumbnail_url() }});">
	<h1>{{ $trans->title }}</h1>          
</div>
@endsection

@section('content')
<div class="cal-cnt pad-left cate suggeriments">
    <div class="row1">  
        <div class="cal-lft contact-l">
            {!! $trans->description !!}
        </div>

        <div class="cal-rht contact-r" style="border: none;">
        <div class="form-pad">
            <ul>
                <li class="textarea">
                    <input id="subject-box" type="text" class="txt-box" name="@Lang('Subject')*" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" value="@Lang('Subject')*" required>
                </li>
                <li class="textarea">
                        <textarea id="mensaje-box" class="txt-box" name="@Lang('Write your suggestion ...')" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">@Lang('Write your suggestion ...')</textarea>
                </li>
                <li class="last-btn">
                    <input id="enviar_mensaje_btn" type="button" value="Enviar">
                </li>
            </ul>
        </div>
        </div>
    </div>
</div>

<div id="pop-info-session-message" class="simplePopup especific" style=" margin: 25% auto;width: 70%;">
    <div class="desktop_pop_info">
        <a href="javascript:void(0);" class="close-btn"><img src="{{asset($_front.'images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
        <ul>
            <span style="color:#000;" class="missatge"></span>
        </div>
        <div class="mobile_pop_info" style="display: none;">
            <a href="javascript:void(0);" class="close-btn"><img src="{{asset($_front.'images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
        <div class="mbl_pop_inner">
        </div>
    </div>
</div>
@endsection

@section('scripts2')
<script>
    $.ajaxSetup({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#enviar_mensaje_btn').click(function(){
        var subject = $('#subject-box');
        if ( subject.val() == '' || subject.val() == '@Lang('Subject')*' ) {
            subject.val('@Lang('Subject')*');
            subject.css('border', 'solid 3px red');
        } else {
            enviar_formulari();
        }
    });

    function enviar_formulari() {
        var subject = $('#subject-box').val();
        var message = $('#mensaje-box').val();
        $.ajax({
            url: '{{ route('suggeriment.store') }}',
            method: 'POST',
            data: {'_token': '{{csrf_token()}}', 'subject': subject, 'message': message},
            success: function(data) {
                obrir_popup("@Lang('Suggestion sent successfully')");
            },
            error: function(data) {
                obrir_popup("@Lang('Error saving data')");
            }
        });
    }

    function obrir_popup(missatge) {
        $('#pop-info-session-message.especific span.missatge').html( missatge );
        $('#pop-info-session-message.especific').addClass('p-open');
        $('body').addClass('overlay');
		var width_pg = $(window).width();
    }

    $('#pop-info-session-message.especific .close-btn img').click(function(){
        $('#pop-info-session-message.especific').removeClass('p-open');
        $('body').removeClass('overlay');
        var width_pg = $(window).width();
    });
</script>
@endsection