@extends('Front::layouts.app')

@section('header')
<div class="title-head cal-ico" style="background-image: url({{ $trans->get_thumbnail_url() }});">
	<h1>{{ $trans->title }}</h1>              
</div>
@endsection

@section('content')
@php
$descomptes = \App\Post::get_posts([['type', 'descompte']]);
@endphp
<section class="descomptes">
    <div class="col-lft">
        <div class="">
            {!! $trans->description !!}
        </div>
        <ul class="item-list">
            @foreach ($descomptes as $des)
                @php
                $d_trans = $des->translate(true);
                $data_inici = $des->get_field('data_inici');
                $data_fi = $des->get_field('data_fi');
                $copo = $des->get_field('copo');
                $copo = !empty($copo) ? $copo->get_thumbnail_url('medium_large') : '';
                $codi_promocional = $des->get_field('codi_promocional');
                $valid = !empty($data_inici) ? __('Valid') . ': ' . $data_inici : '';
                $valid .= !empty($data_inici) && !empty($data_fi) ?  ' - ' . $data_fi : '';
                @endphp
                <li id="{{ $des->id }}">
                    <img src="{{ $d_trans->get_thumbnail_url('medium') }}">
                    <h4>
                        <span>{{ $valid }}</span>
                        {{ $d_trans->title }}
                    </h4>
                    <input type="hidden" name="date" value="{{ $valid }}">
                    <input type="hidden" name="copo" value="{{ $copo }}">
                    <input type="hidden" name="codi_promocional" value="{{ strtoupper($codi_promocional) }}">
                    <div class="description" style="display: none !important;">
                        {!! $d_trans->description !!}
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="col-rgt">
        <div class="">
            <span class="date"></span>
            <h4 class="title"></h4>
            <p class="description"></p>
            <div class="codi_promocional_blanc">
                <p>@Lang('descompte-codi-url', ['url' => 'http://www.loremipsum.com', 'url-title' => 'www.loremipsum.com'])</p>
                <div class="codi_promocional">
                    <span>@Lang('Promotional code'):</span>
                    <br>
                    <p></p>
                </div>
            </div>
            <div class="copo">
                <img>
                <a class="download" target="_blank" href="javascript:void(0);">@Lang('Download')</a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts2')
<script>
    var $active_item = null;
    $('ul.item-list li').click(function() {
        $('.descomptes ul.item-list li').removeClass('active');
        $('.descomptes').removeClass('active');
        $('.descomptes .col-rgt').children().hide();
        
        if ( $active_item != $(this).attr('id') ) {
            $active_item = $(this).attr('id');
            $(this).addClass('active');
            $('.descomptes').addClass('active');
            $('.descomptes .col-rgt').children().show();
            var copo = $(this).find('input[type=hidden][name=copo]').val();
            var codi_promocional = $(this).find('input[type=hidden][name=codi_promocional]').val();
            var date = $(this).find('input[type=hidden][name=date]').val();
            var description = $(this).find('div.description').html();
            $('.col-rgt .date').html(date);
            $('.col-rgt .description').html(description);
            $('.col-rgt .codi_promocional p').html(codi_promocional);
            $('.col-rgt .copo img').attr('src', copo);
            $('.col-rgt .copo a.download').attr('href', copo);
            
            if ( copo == '' ) $('.col-rgt .copo').hide();
            else $('.col-rgt .copo').show();
            if ( codi_promocional == '' ) $('.col-rgt .codi_promocional_blanc').hide();
            else $('.col-rgt .codi_promocional_blanc').show();
        } else {
            $active_item = null;
        }

        if ( $(document).innerWidth() < 768 ) {
            $('html, body').animate({
                scrollTop: $('.col-rgt').offset().top
            });
        }
    });
</script>
@endsection