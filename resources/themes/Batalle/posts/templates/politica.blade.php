@extends('Front::layouts.frontend')

@php( $user_actual = Auth::check() ? \App\Modules\CalendarioVacaciones\Models\User::find(Auth::user()->id) : Auth::user() )
@php( $dni = !empty(request()->dni) ? request()->dni : '' )
@php( $name = !empty(request()->name) ? urldecode(request()->name) : '' )

@section('page')
<section class="single-page">
    <div class="terms">
        <img src="{{asset($_front.'images/BATALLE-INTRANET-logo-VERMELL.png')}}" class="logo">
        <p class="quote">
            {{ $trans->title }}
        </p>
        <p>
        <p>
        @if( !empty($name) || !empty($dni) )
            El sotasignat, <strong>{{$name}},</strong>  amb DNI/NIE <strong>{{$dni}}</strong>,<br>
        @elseif( Auth::check() && !empty($user_actual->name) )
            El sotasignat, <strong>{{$user_actual->name}},</strong>  amb DNI/NIE <strong>{{$user_actual->dni}}</strong>,<br>
        @endif
        {!! $trans->description !!}
        <button type="submit" onClick="window.history.back();">
        @if ( Auth::check() )
            @Lang('Back')
        @else
            @Lang('I Accept')
        @endif
        </button>
    </div>
</section>
@endsection

@section('scripts_parent')
<script src="{{ asset($_front.'js/maqueta/jquery.min.js') }}"></script>
<script src="{{ asset($_front.'js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script>
    function setHeight() {
        var getHeaderHeight = $('header').outerHeight();
        var getFooterHeight = $('footer').outerHeight();
        var getWindowHeight =$(window).height();
        var setHeight = getWindowHeight - (getHeaderHeight + getFooterHeight);
        $('.relative-pad').css('min-height', setHeight);	
    }

    $(document).ready(function() {	
		setHeight();
		$(".select_box").selectbox();
    });

    $(window).resize(function(){
        setHeight();
    });
</script>
@endsection