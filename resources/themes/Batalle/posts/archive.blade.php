@extends('Front::layouts.app')

@section('header')
<div class="title-head">
	<h1>{{ __(ucfirst($post_type)) }}</h1>
</div>
@endsection

@section('content')
<section class="single-page register-done">
    <div class="row" style="text-align: left;">
        <ul>
        @forelse($items as $item)
            @php($trans = $item->translate( \App::getLocale() ))
            <li>
                <a href="{{$item->get_url()}}">
                    {{ $trans->title }}
                    {!! $trans->description !!}
                </a>
            </li>        
        @empty
            <h3>@Lang('No entries found.')</h3>
        @endforelse
        </ul>
    </div>
</section>
@endsection

@section('scripts')
<script src="{{ asset('Themes/Batalle/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('Themes/Batalle/js/popper.js') }}"></script>
<script src="{{ asset('Themes/Batalle/js/bootstrap-material-design.js') }}"></script>
<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
<script src="{{ asset('Themes/Batalle/js/maqueta/jquery.sticky.js') }}"></script>
<script src="{{ asset('Themes/Batalle/js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script>
    function setHeight() {
        var getHeaderHeight = $('header').outerHeight();
        var getFooterHeight = $('footer').outerHeight();
        var getWindowHeight =$(window).height();
        var setHeight = getWindowHeight - (getHeaderHeight + getFooterHeight);
        $('.register-done').css('min-height', setHeight);
    }
            
        
    $(document).ready(function() {
        $(".select_box").selectbox();
        setHeight()
    });

    $(window).resize(function(){
        setHeight()
    });
</script>
@endsection