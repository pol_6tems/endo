@extends('Front::layouts.frontend')

@section('page')
<section class="single-page register-done page-contingut">
    <div class="row">
        <!--<img src="{{asset($_front.'images/BATALLE-INTRANET-logo-VERMELL.png')}}" class="logo">-->
        <p class="quote">
            {{ $trans->title }}
        </p>
        <p>
        {!! $trans->description !!}
    </div>
</section>
@endsection

@section('scripts')
<script src="{{ asset($_front.'js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset($_front.'js/popper.js') }}"></script>
<script src="{{ asset($_front.'js/bootstrap-material-design.js') }}"></script>
<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
<script src="{{ asset($_front.'js/maqueta/jquery.sticky.js') }}"></script>
<script src="{{ asset($_front.'js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script>
    function setHeight() {
        var getHeaderHeight = $('header').outerHeight();
        var getFooterHeight = $('footer').outerHeight();
        var getWindowHeight =$(window).height();
        var setHeight = getWindowHeight - (getHeaderHeight + getFooterHeight);
        $('.register-done').css('min-height', setHeight);
    }
            
        
    $(document).ready(function() {
        $(".select_box").selectbox();
        setHeight()
    });

    $(window).resize(function(){
        setHeight()
    });
</script>
@endsection