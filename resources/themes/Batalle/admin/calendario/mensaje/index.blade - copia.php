@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Calendar') - @Lang('Messages')</h1>
</div>

<div class="panel-body">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th width="5%">@Lang('Id')</th>
                <th width="15%">@Lang('Name')</th>
                <th width="10%">@Lang('Email')</th>
                <th>@Lang('Last message')</th>
                <th width="5%">@Lang('Actions')</th>
            </tr>
        </thead>
        <tbody>
            @forelse($items as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }} {{ $item->lastname }}</td>
                <td>{{ $item->email }}</td>
                
                <td>{{ $item->mensajes()->latest()->first()->from()->first()->fullname() }}: {{ $item->mensajes()->latest()->first()->mensaje }}</td>

                <td class="cela-opcions">
                    <a role="button" class="btn btn-primary btn-raised" href="{{route('calendario.mensajes.contestar', $item->id)}}">@Lang('Answer')</a>
                </td>
            </tr>
            @empty
                <tr>
                    <td colspan="3">@Lang('No entries found.')</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    {{ $items->links() }}

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">Eliminar</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Eliminar</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection	

@section('scripts')
<script>
   $('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var url = button.data('url');
		var nombre = button.data('nombre');
		var modal = $(this);
		modal.find('form').attr('action', url);
		modal.find('.modal-body').html("@Lang('Are you sure to delete it?')");
	});
</script>
@endsection