@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Edit') @Lang('Manager'): {{$item->user->fullname()}} ({{$item->user->email}})</h1>
</div>

<div class="panel-body">

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('calendario.niveles.encarregat.update', $item->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="calendario_nivel_id" value="{{ $item->calendario_nivel_id }}" />
        
        <div class="form-group col-md-12 mb-5">
			<label for="user_id" class="bmd-label-floating">@Lang('User')</label>
			<select name="user_id" class="custom-select col-md-12" data-live-search="true" title="@Lang('User')">
			@foreach ($users as $user)
				<option value="{{$user->id}}" @if ($user->id == $item->user_id) selected @endif>{{$user->fullname()}} ({{$user->email}}) - @Lang('Role'): {{ __($user->role) }}</option>
			@endforeach
			</select>
		</div>

        <div class="form-group col-md-12 mb-5">
            <input onclick="window.location.href='{{ route('calendario.niveles.edit', $item->calendario_nivel_id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
        </div>
             
        </div>
    </form>

</div>
@endsection