@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Add new') - @Lang('Manager')</h1>
</div>

<div class="panel-body">
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('calendario.niveles.encarregat.store') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="calendario_nivel_id" value="{{$id}}" />

        <div class="form-group col-md-12 mb-5">
			<label for="user_id" class="bmd-label-floating">@Lang('User')</label>
			<select name="user_id" class="custom-select col-md-12" data-live-search="true" title="@Lang('User')">
			@foreach ($users as $user)
				<option value="{{$user->id}}">{{$user->fullname()}} ({{$user->email}}) - @Lang('Role'): {{ __($user->role) }}</option>
			@endforeach
			</select>
		</div>

        <div class="form-group col-md-12 mb-5">
            <input onclick="window.location.href='{{ route('calendario.niveles.edit', $id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
        </div>
    </form>

</div>
@endsection