@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Add new')</h1>
</div>

<div class="panel-body">
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('configuracion.rols.store') }}" method="post">
        {{ csrf_field() }}

        @Lang('Name')
        <br />
        <input type="text" name="name" value="{{ old('name') }}" style="font-size: 30px;width: 100%;" required/>
        <br /><br />
        @Lang('Level')
        <br/>
        <input type="number" min="0" name="level" value="{{ old('level') }}" style="font-size: 30px;width: 100%;" required/>
        <br /><br />
        <input onclick="window.location.href='{{ route('configuracion.rols.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
        <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
    </form>

</div>
@endsection