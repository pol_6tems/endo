@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>
    <i class="material-icons pr-2 admin-menu-icona">menu</i>
        @Lang('Edit'): {{__($item->name)}}
    </h1>
</div>

<div class="panel-body">

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('configuracion.menus.update', $item->id) }}" method="post">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        
        @Lang('Order')
        <br />
        <input type="number" min="0" name="order" value="{{ $item->order }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        @Lang('Name'):
        <br />
        <input type="text" name="name" value="{{ $item->name }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        @Lang('Icon'):
        <br />
        <input type="text" name="icon" value="{{ $item->icon }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        <div class="form-group">
            <label for="role" class="bmd-label-floating">@Lang('Role')</label>
            <select class="form-control custom-select" name="rol_id" data-live-search="true" title="@Lang('Role')">
            @foreach ($rols as $rol)
                <option value="{{$rol->id}}" {{($rol->id == $item->rol_id ? 'selected' : '')}} >{{__($rol->name)}}</option>
            @endforeach
            </select>
        </div>
        <input onclick="window.location.href='{{ route('configuracion.menus.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
        <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
             
        </div>
    </form>

    <!-- MenuSub -->
    <div class="col-sm-12 mt-5 mb-5">
        <h2>
            @Lang('Submenus')
            <a href="{{ route('configuracion.menus.subs.create', $item->id) }}" class="btn btn-default btn-raised">@Lang('Add')</a>
        </h2>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="5%">@Lang('Order')</th>
                    <th>@Lang('Title')</th>
                    <th>@Lang('Name')</th>
                    <th width="15%">@Lang('Role')</th>
                    <th>@Lang('Controller')</th>
                    <th width="5%">@Lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @forelse($subs as $sub)
                <tr>
                    <td>{{ $sub->order }}</td>
                    <td>{{ __($sub->name) }}</td>
                    <td>{{ $sub->name }}</td>
                    <td>{{ __($sub->rol->name) }}</td>
                    <td>{{ $sub->controller }}</td>
                    <td class="cela-opcions">
                        <div class="btn-group">
                            <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $sub->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </button>  
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $sub->id }}">
                                <a class="dropdown-item" href="{{ route('configuracion.menus.subs.edit', $sub->id) }}">@Lang('Edit')</a>
                                <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('configuracion.menus.subs.destroy', $sub->id) }}" data-nombre="@Lang('Submenu'): {{ __($sub->name) }} - @Lang('Email'): {{ $sub->name }}">@Lang('Delete')</button>
                            </div>
                        </div>
                    </td>
                    
                </tr>
                @empty
                    <tr>
                        <td colspan="5">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        {{ $subs->links() }}
    </div>
    <!-- end MenuSub -->

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script>
   $('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var url = button.data('url');
		var nombre = button.data('nombre');
		var modal = $(this);
		modal.find('form').attr('action', url);
		modal.find('.modal-body').html("@Lang('Are you sure to delete it?')");
	});
</script>
@endsection