<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield("title", config('app.name', 'Endo'))</title>

    <!-- Styles -->
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
	
	<!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <!-- Material Design for Bootstrap CSS -->
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
	
	@yield("styles")
	
	<link href="{{asset('css/endo.css')}}" rel="stylesheet" type="text/css">
	
	<style>
		.navbar-brand i, .navbar-brand span { vertical-align: middle; }
	</style>
	
	
</head>
<body>

	<div class="bmd-layout-container bmd-drawer-f-l bg-white border-bottom box-shadow">
		<header class="bmd-layout-header">
			<div class="navbar navbar-light bg-faded">
				@auth
					<button class="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-s1">
						<span class="sr-only">Toggle drawer</span>
						<i class="material-icons">menu</i>
					</button>
					<span class="header-seccio my-0 font-weight-bold ml-md-3" onclick="window.location.href = '{{url('/admin')}}';">
						Àrea privada
					</span>
				@endauth
				<i onclick="window.location.href = '{{url('/')}}';" class="logo ml-2 ml-md-auto"></i>
				<!--<h5 class="my-0 font-weight-normal ml-md-auto">{{ config('app.name', 'BATALLE') }}</h5>-->
				<nav class="my-2 my-md-0 mr-md-3">
					<!--<a class="p-2 text-dark" href="#">Pricing</a>-->
				</nav>
			</div>
		</header>
		<div id="dw-s1" class="bmd-layout-drawer">
			<header class="pb-0">
				<a class="navbar-brand pl-0" data-toggle="collapse" href="#menu-configuracion" role="button" aria-expanded="false" aria-controls="menu-configuracion">
					<i class="material-icons pr-2">tune</i><span>Configuración</span>
				</a>
			</header>
			<div class="collapse" id="menu-configuracion">
				<ul class="list-group pt-0">
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/configuracion/menus*')) active @endif" href="{{ url('admin/configuracion/menus') }}">Menús</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/configuracion/usuarios*')) active @endif" href="{{ url('admin/configuracion/usuarios') }}">Usuarios</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/configuracion/librerias*')) active @endif" href="{{ url('admin/configuracion/librerias') }}">Librerias</a></li>
				</ul>
			</div>
			<header class="pb-0">
				<a class="navbar-brand pl-0" data-toggle="collapse" href="#menu-calendario" role="button" aria-expanded="false" aria-controls="menu-calendario">
					<i class="material-icons pr-2">date_range</i><span>Calendario</span>
				</a>
			</header>
			<div class="collapse" id="menu-calendario">
				<ul class="list-group pt-0">
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/calendario*')) active @endif" href="{{ url('admin/calendario') }}">Calendario</a></li>
				</ul>
			</div>
			<footer>
				<a class="btn btn-raised btn-primary" href="{{ route('logout') }}"
					onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
					Sortir
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
				</form>
			</footer>
		</div>
		
		<main class="bmd-layout-content">

			<div class="header">
				<div class="container">
					@yield("header")

					@auth
						<a class="ml-md-auto" href="{{url('/perfil')}}">
							<img class="avatar rounded-circle" src="/storage/avatars/{{ !empty(Auth::user()->avatar) ? Auth::user()->avatar : 'user.jpg' }}" />
						</a>
                    @endauth
					
					@if (Session::has('message'))
						<div class="alert alert-success alert-dismissible" role="alert">
							{{Session::get('message')}}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					@endif
					@if (Session::has('error'))
						<div class="alert alert-danger alert-dismissible" role="alert">
							{{Session::get('error')}}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					@endif
				</div>
			</div>

			<div class="container">
				<div class="content">
					@yield("content")
				</div>
			</div>
		</main>

	</div>
	
	<footer>
		@yield("footer")
	</footer>	

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" crossorigin="anonymous"></script>
    <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	
	@yield("scripts")
	
</body>
</html>
