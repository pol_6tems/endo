@extends('Front::layouts.frontend')

@section('page')
<div class="clear"></div>
<section class="relative-pad"> 
<!-- logo section starts -->
<div class="user-section pad-left">
	<div class="menu">
		<a href="javascript:void(0);" id="m-menu"><img src="{{asset($_front.'images/BATALLE-INTANET-icona-menu.png')}}" class="m_menu"></a> 
		
		<!-- Maga Menu -->
			<div class="button-dropdown nav-toggle-red">
			<div class="button-inner">
			<div class="menu-header">
				<div class="w-menu-p">
				<div id="m-menu-close" class="nav-toggle">
					<a href="javascript:void(0);"><span></span> <p>@Lang('Menu')</p></a>
				</div> 
				</div>          
			</div>
			<div id="divfocus" class="button-dropdown-int">
				<div class="menu-list">
				<ul>
					<li><a href="{{route('calendari.index')}}"><img src="{{asset($_front.'images/menu-cal-ico.png')}}"> @Lang('Vacancy calendar')</a></li>
					<li><a href="{{\App\Post::get_url_by_post_id(38)}}"><img src="{{asset($_front.'images/menu-forma-ico.png')}}"> @Lang('Formation')</a></li>
					<li><a href="{{\App\Post::get_url_by_post_id(37)}}"><img src="{{asset($_front.'images/menu-not-ico.png')}}"> @Lang('News')</a></li>
					<li><a href="{{\App\Post::get_url_by_post_id(20)}}"><img src="{{asset($_front.'images/menu-des-ico.png')}}"> @Lang('Discounts')</a></li>
					<li><a href="{{\App\Post::get_url_by_post_id(39)}}"><img src="{{asset($_front.'images/menu-enques-ico.png')}}"> @Lang('Surveys')</a> </li>
					<li><a href="{{\App\Post::get_url_by_post_id(40)}}"><img src="{{asset($_front.'images/menu-sugger-ico.png')}}"> @Lang('Suggestions box')</a> </li>
					<li><a href="{{route('contactar.index')}}"><img src="{{asset($_front.'images/menu-cont-ico.png')}}"> @Lang('Contact')</a> </li>
				</ul> 
				</div>
			</div>
			</div>
			</div>
		<!-- /Maga Menu -->
	</div>
	
	<div class="row1">
	
		@if (Auth::check())
			<div class="user-rht">
				<a href="javascript:void(0);">
					<img src="{{asset('storage/avatars/'.Auth::user()->avatar)}}" style="margin-left: 10px;">
					<br>
					<h5 style="width: 100%;text-align: center;">{{Auth::user()->name}}</h5>
				</a>
				<div class="user-popup">
					<h3>{{Auth::user()->name}} {{Auth::user()->lastname}}</h3>
					<ul>
						<li><a href="{{route('usuario.perfil')}}">@Lang('Editar perfil')</a></li>
						<!--<li><a href="javascript:void(0);">Modificar contrasenya</a></li>-->
						<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@Lang('Logout')</a></li>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							{{ csrf_field() }}
						</form>
					</ul>
				</div>         
			</div>
		@endif

		@yield('header')

	</div>
</div>  
<!-- logo section ends -->

	@yield("content")

</section>
<div class="clear"></div>
@endsection

@section('styles_parent')
<link rel="stylesheet" href="{{ asset($_front.'css/megamenu.css') }}" />
@endsection

@section('scripts_parent')
<script src="{{ asset($_front.'js/maqueta/jquery.min.js') }}"></script>
<script src="{{ asset($_front.'js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script src="{{ asset($_front.'js/maqueta/menu.js') }}"></script>
@endsection
@section('scripts')
<script>
    function setHeight() {
        var getHeaderHeight = $('header').outerHeight();
        var getFooterHeight = $('footer').outerHeight();
        var getWindowHeight =$(window).height();
        var setHeight = getWindowHeight - (getHeaderHeight + getFooterHeight);
        $('.relative-pad').css('min-height', setHeight);	
    }

    $(document).ready(function() {	
			setHeight();
			$(".select_box").selectbox();
    });

    $(window).resize(function(){
        setHeight();
	});
	
	$('.nav-toggle a span').click(function() {
		$('body').removeClass('active-drop');
	});
</script>
@yield('scripts2')
@endsection