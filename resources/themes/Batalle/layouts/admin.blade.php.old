<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield("title", config('app.name', 'Endo'))</title>
	
	<link href="{{asset('css/bootstrap/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/bootstrap/css/bootstrap-editable.css')}}" rel="stylesheet" type="text/css">
	@yield("styles_before")
	
    <!-- Styles -->
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
	
	<!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <!-- Material Design for Bootstrap CSS -->
	<link href="{{asset('css/bootstrap-material-design.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('js/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css">
	
	@yield("styles")
	
	<link href="{{asset('css/endo-admin.css')}}" rel="stylesheet" type="text/css">
	
	<style>
		.navbar-brand i, .navbar-brand span { vertical-align: middle; }
	</style>
	
	
</head>
<body>

	<div class="bmd-layout-container bmd-drawer-f-l bg-white border-bottom box-shadow">
		<header class="bmd-layout-header">
			<div class="navbar navbar-light bg-faded">
				<button class="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-s1">
					<span class="sr-only">Toggle drawer</span>
					<i class="material-icons">menu</i>
				</button>
				<span class="header-seccio my-0 font-weight-bold ml-md-3" onclick="window.location.href = '{{url('/')}}';">
					{{ config('app.name', 'Endo') }}
				</span>
				<div class="btn-group selector-idiomas">
					<button class="btn dropdown-toggle" type="button" id="languages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						@Lang( (!empty(session()->get('locale_name')) ? session()->get('locale_name') : 'Idioma') )
					</button>
					<div class="dropdown-menu" aria-labelledby="languages">
						@foreach ($_languages as $language)
							<a class="dropdown-item" href="{{ url('/locale/' . $language->code) }}">@lang($language->name)</a>
						@endforeach
					</div>
				</div>
				<i onclick="window.location.href = '{{url('/')}}';" class="logo-admin ml-2 ml-md-auto"></i>
				<div class="title-admin"><h5>Endo</h5></div>
				<!--<h5 class="my-0 font-weight-normal ml-md-auto">{{ config('app.name', 'BATALLE') }}</h5>-->
				<nav class="my-2 my-md-0 mr-md-3">
					<!--<a class="p-2 text-dark" href="#">Pricing</a>-->
				</nav>
			</div>
		</header>
		<div id="dw-s1" class="bmd-layout-drawer">
			<!-- CONFIGURACION -->
			@if ( in_array(Auth::user()->role, array('admin')) )
			<header class="pb-0">
				<a class="navbar-brand pl-0" data-toggle="collapse" href="#menu-configuracion" role="button" aria-expanded="false" aria-controls="menu-configuracion">
					<i class="material-icons pr-2">tune</i><span>@Lang('Configuration')</span>
				</a>
			</header>
			<div class="collapse show in" id="menu-configuracion">
				<ul class="list-group pt-0">
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/configuracion/rols*')) active @endif" href="{{ route('configuracion.rols.index') }}">@Lang('Roles')</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/configuracion/menus*')) active @endif" href="{{ route('configuracion.menus.index') }}">@Lang('Menus')</a></li>
					<!--<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/configuracion/librerias*')) active @endif" href="{{ url('admin/configuracion/librerias') }}">@Lang('Libraries')</a></li>-->
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('languages*') || request()->is('translations*')) active @endif" href="{{ url('languages') }}">@Lang('Translations')</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('opciones*') || request()->is('opciones*')) active @endif" href="{{ route('opciones.index') }}">@Lang('Options')</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/configuracion/usuarios*')) active @endif" href="{{ url('admin/configuracion/usuarios') }}">@Lang('Users')</a></li>
				</ul>
			</div>
			@endif
			<!-- end CONFIGURACION -->
			<!-- CONTENIDO -->
			@if ( in_array(Auth::user()->role, array('admin')) )
			<header class="pb-0">
				<a class="navbar-brand pl-0" data-toggle="collapse" href="#menu-contenido" role="button" aria-expanded="false" aria-controls="menu-contenido">
					<i class="material-icons pr-2">library_books</i><span>@Lang('Content')</span>
				</a>
			</header>
			<div class="collapse show in" id="menu-contenido">
				<ul class="list-group pt-0">
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/posts*')) active @endif" href="{{ url('admin/posts') }}">Posts</a></li>
				</ul>
			</div>
			@endif
			<!-- end CONTENIDO -->
			<!-- CALENDARIO -->
			@if ( in_array(Auth::user()->role, array('admin', 'editor')) )
			<header class="pb-0">
				<a class="navbar-brand pl-0" data-toggle="collapse" href="#menu-calendario" role="button" aria-expanded="false" aria-controls="menu-calendario">
					<i class="material-icons pr-2">date_range</i><span>@Lang('Vacations')</span>
				</a>
			</header>
			<div class="collapse show in" id="menu-calendario">
				<ul class="list-group pt-0">
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/calendario-vacaciones*')) active @endif" href="{{ url('admin/calendario-vacaciones') }}">@Lang('Vacancy calendar')</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/calendario/niveles*')) active @endif" href="{{ url('admin/calendario/niveles') }}">@Lang('Levels')</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/calendario/subniveles*')) active @endif" href="{{ url('admin/calendario/subniveles') }}">@Lang('Sublevels')</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/calendario/festivos*')) active @endif" href="{{ route('calendario.festivos.index') }}">@Lang('Holidays')</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/calendario/vetados*')) active @endif" href="{{ route('calendario.vetados.index') }}">@Lang('Days banned')</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/calendario/mensajes*')) active @endif" href="{{ route('calendario.mensajes.index') }}">@Lang('Messages')</a></li>
				</ul>
			</div>
			@endif
			<!-- end CALENDARIO -->
			<!-- EMPRESA -->
			@if ( in_array(Auth::user()->role, array('admin', 'editor')) )
			<header class="pb-0">
				<a class="navbar-brand pl-0" data-toggle="collapse" href="#menu-empresa" role="button" aria-expanded="false" aria-controls="menu-empresa">
					<i class="material-icons pr-2">business</i><span>@Lang('Company')</span>
				</a>
			</header>
			<div class="collapse show in" id="menu-empresa">
				<ul class="list-group pt-0">
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/empleados*') && !request()->is('admin/empleados/categorias*')) active @endif" href="{{ url('admin/empleados') }}">@Lang('Employees')</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/empleados/categorias*')) active @endif" href="{{ url('admin/empleados/categorias') }}">@Lang('Categories') @Lang('Employees')</a></li>
					<li><a class="list-group-item pt-2 pb-2 @if (request()->is('admin/mensajes*')) active @endif" href="{{ route('mensajes.index') }}">@Lang('Contact') @Lang('Messages')</a></li>
				</ul>
			</div>
			@endif
			<!-- end EMPRESA -->
			<footer>
				<a class="btn btn-raised btn-primary" href="{{ route('logout') }}"
					onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
					@Lang('Logout')
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
				</form>
			</footer>
		</div>
		
		<main class="bmd-layout-content">

			<div class="header">
				<div class="container">
					@yield("header")
				</div>
			</div>

			<div class="container">
				@if (Session::has('message'))
					<div class="alert alert-success alert-dismissible" role="alert">
						@lang(Session::get('message'))
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="alert alert-danger alert-dismissible" role="alert">
						@lang(Session::get('error'))
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
				<div class="content">
					@yield("content")
				</div>
			</div>
		</main>

	</div>
	
	<footer>
		@yield("footer")
	</footer>	

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.js') }}"></script>
    <script src="{{ asset('js/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-design.js') }}"></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	
	<script>$(document).ready(function() { $('select').selectpicker(); });</script>
	
	@yield("scripts")
	
</body>
</html>
