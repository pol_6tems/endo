@extends('Front::layouts.app')

@section('header')
<div class="title-head cal-ico">
	<h1>@Lang('Dashboard')</h1>              
</div>
@endsection

@section('content')
<div class="benvigut">
	<div class="row1">
		<img src="{{asset('Themes/Batalle/images/BATALLE-INTRANET-logo-VERMELL.png')}}">
		<p>Benvigut/da a l’àrea privada de Batallé, <span>aquest canal està pensat per tenir una relació més estreta i àgil entre treballador/a i empresa.</span></p>
		<div class="beguit-row">
			<ul>
			<li class="ben-cal"><a href="{{route('calendari.index')}}"><span></span> <h3>@Lang('Vacancy calendar')</h3></a></li>
			<li class="ben-prop"><a href="{{\App\Post::get_url_by_post_id(38)}}"><span></span> <h3>@Lang('Formation')</h3></a></li>
			<li class="ben-book"><a href="{{\App\Post::get_url_by_post_id(37)}}"><span></span> <h3>@Lang('News')</h3></a></li>
			<li class="ben-off"><a href="{{\App\Post::get_url_by_post_id(20)}}"><span></span> <h3>@Lang('Discounts')</h3></a></li>
			<li class="ben-list"><a href="{{\App\Post::get_url_by_post_id(39)}}"><span></span> <h3>@Lang('Surveys')</h3></a></li>
			<li class="ben-drop"><a href="{{\App\Post::get_url_by_post_id(40)}}"><span></span> <h3>@Lang('Suggestions box')</h3></a></li>
			<li class="ben-cont"><a href="{{route('contactar.index')}}"><span></span> <h3>@Lang('Contact')</h3></a></li>
			</ul>
		</div>
	</div>
</div>
@endsection