@extends('layouts.admin')

@section('header')
<div class="title m-b-md flex-center" style="padding-top: 20vh;">
	Endo v3
</div>
@endsection

@section('content')
<div class="links">
	@if ( Auth::check() )
		@if ( Auth::user()->user == 1 )
			<a href="{{url('admin')}}">Panel de Administrador</a>
		@endif
		<a href="{{url('user')}}">{{Auth::user()->name}}</a>
		<a href="{{url('auth/logout')}}">Salir</a>
	@else
		<a href="{{url('auth/login')}}">Iniciar sesión</a>
   @endif
</div>
@endsection