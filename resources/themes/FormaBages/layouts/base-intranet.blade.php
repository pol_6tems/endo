<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    @php( get_header() )

    <link rel="shortcut icon" href="{{ asset($_front."images/favicon/favicon.ico") }}" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset($_front."images/favicon/favicon-16x16.png") }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($_front."images/favicon/favicon-32x32.png") }}">

    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset($_front."images/favicon/android-chrome-192x192.png") }}">
    <link rel="icon" type="image/png" sizes="256x256" href="{{ asset($_front."images/favicon/android-chrome-256x256.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset($_front."images/favicon/apple-touch-icon.png") }}">



    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>formaBAGES</title>
    <!--CSS Start-->
    <link rel="canonical" href="{{ url()->current() }}"/>
    <link href="{{ mix('css/app.css', 'Themes/' . env('PROJECT_NAME')) }}" rel="stylesheet">
    @yield('styles')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2219521-23"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-2219521-23');
    </script>
</head>
<body class="@yield('body_classes', 'intranet')">
    @php(admin_bar())

    @if (route_name('login'))
        <section class="area-container @yield('extra_section_classes', '')">
            <div class="area-content @yield('extra_login_class', '')">
                @yield('content')
            </div>
        </section>
    @else
        <div class="hole_div">
            <div id="page" @if (auth()->check() && auth()->user()->isAdmin())style="top: 32px;"@endif>
                @include('Front::intranet.header')

                <section class="inner-page">
                    @include('Front::intranet.breadcrumbs')

                    <div class="row">
                        @yield('content')
                    </div>
                </section>

                @includeIf('Front::partials.footer')
            </div>
        </div>
    @endif

    <script src="{{ mix('js/app.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>
    @include('Front::partials.scripts.notifications')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @yield('scripts')
    <div id="new"></div>
</body>

</html>