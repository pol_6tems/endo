<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    @php( get_header() )

    <link rel="shortcut icon" href="{{ asset($_front."images/favicon/favicon.ico") }}" />
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset($_front."images/favicon/favicon-16x16.png") }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset($_front."images/favicon/favicon-32x32.png") }}">

    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset($_front."images/favicon/android-chrome-192x192.png") }}">
    <link rel="icon" type="image/png" sizes="256x256" href="{{ asset($_front."images/favicon/android-chrome-256x256.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset($_front."images/favicon/apple-touch-icon.png") }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>formaBAGES</title>
    <!--CSS Start-->
    <link rel="canonical" href="{{ url()->current() }}"/>
    <link href="{{ mix('css/app.css', 'Themes/' . env('PROJECT_NAME')) }}" rel="stylesheet">
    @yield('styles')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2219521-23"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-2219521-23');
    </script>
<body>
    
    <div class="hole_div">
        <div id="page">
            @php( admin_bar() )
            <!--Header Starts-->
            @yield('header', View::make('Front::partials.inner-header'))
            <!--Header Ends-->
            @yield('content')
            @includeIf('Front::partials.footer')
        </div>
        <!-- footer ends -->
        @includeIf('Front::partials.mobile-menu')
    </div>
    <script src="{{ mix('js/app.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>
    @yield('scripts')
    <div id="new"></div>
</body>

</html>