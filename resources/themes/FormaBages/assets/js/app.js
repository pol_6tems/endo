require('./maqueta/html5');
window.$ = window.jQuery = require('jquery');
window.Swal = require('sweetalert2');

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

require('./maqueta/jquery.sticky.js');

require('./maqueta/jquery.flexslider');
require('./maqueta/ddsmoothmenu');
require('./maqueta/jquery.selectbox-0.2');
require('./maqueta/jquery.fancybox');
require('./maqueta/jquery.fancybox-media');
require('./maqueta/owl.carousel');
require('./maqueta/easyResponsiveTabs');
require('./maqueta/jquery-ui-datepicker.min');
require('./maqueta/smk-accordion');
require('./maqueta/sweetalert2.all.min');
require('./maqueta/vmenuModule');
require('./maqueta/jquery-datepicker');

window.toastr = require('toastr');

require('./maqueta/common');

var lang = $('.jquery-datepicker').data('lang');
$('.jquery-datepicker').jqDatepicker({lang: typeof lang !== 'undefined' ? lang : 'ca'});
