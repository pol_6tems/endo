// Input Box Placeholder
function fnremove(arg,val)	{
	if (arg.value == '') {arg.value = val}
}	
function fnshow(arg,val)	{
	if (arg.value == val) {arg.value = ''}
}

$(document).ready(function () {	
	// Sticky Header		
	$(".main-menu").sticky({topSpacing:0});
	
	$(".select_box").selectbox();
	
	// Mmenu - Mobile Menu	
	$( ".mmoverly" ).appendTo( $( "body" ) );
   
   jsUpdateSize();
   Updatefilter();

   $('.datepicker').datepicker();
});

$(window).resize(function(){	
	jsUpdateSize();
	Updatefilter();
});
function jsUpdateSize()
{
	var width = $(window).width();
    var height = $(window).height();	
	if(width<=767)
	{
		$('.banner-home .flexslider .slides li').each(function()
		{
			var img = $('img', this).attr('src');
			$(this).css('background-image','url(' + img + ')');		
			$(".banner-home").css('height',350);
			$(this).css('height',350);		
		});
	}
	else{
		$('.banner-home .flexslider .slides li').each(function()
		{
			$(this).css('background-image','');
			$(".banner-home").css('height','auto');
			$(this).css('height','auto');
		});	
		
	}	
}	

/*======================================================================================
Filter Script 
======================================================================================*/

function Updatefilter()
{
	var width = $(window).width();
    var height = $(window).height();
	if(width<768)
	{
		$(".la-tag.desktop .sline").appendTo(".la-tag.mob");
		$(".la-tag.desktop .recent-list").appendTo(".la-tag.mob");
		 
	}else{
		$(".la-tag.mob .sline").appendTo(".la-tag.desktop");
		$(".la-tag.mob .recent-list").appendTo(".la-tag.desktop");
	}
}

/*======================================================================================
Filter Script End
======================================================================================*/

var myVideo = document.getElementById("video"); 
	function playPause() { 		
		if (myVideo.paused) {
			myVideo.play();
		}
		else {
			myVideo.pause();		 
			$('#play').removeClass('pause');
			$('#play').addClass('play');
			$(".clickToPlay").css("display","none");
			
		}
	}
	$("#play").click(function() {
		playPause();
		
		if($("#play").hasClass("play")) {    	 
			$(this).removeClass('play');
			$(this).addClass('pause');
			
    	}else {	 
			$(this).removeClass('pause');
			$(this).addClass('play');
		}
		
});

var win_hgt;

$(document).ready(function() {
	
	$(".accordion_example1").smk_Accordion({
		closeAble: true, //boolean
		closeOther: false, //boolean
	});

	$("#search-icon").click(function() {
		$(this).toggleClass('open');
		$('.inner-search').toggleClass('open-search');
	});

	$("#filter-icon").click(function() {
		$(this).toggleClass('open');
		$('.filter-box').toggleClass('open-filter');
	});

	$(window).scroll(function() {
		if ($(window).scrollTop() > 1) {
			$('.inner-search').addClass("search-float");
		} else {
			$('.inner-search').removeClass("search-float");
		}
	});

	$(window).resize(function() {
		win_hgt = $(window).height();
		$(".move #page").css({"height":win_hgt,"overflow":"hidden"});
	});

	win_hgt = $(window).height();
	$("#hie_menu").click( function() {
		$(".hole_div").addClass("move");
		//$(this).hide();
		$("#new").show();
		$("#page").css({"height":win_hgt,"overflow":"hidden"});
	});
	
	$("#new").click(function() {
		$(".hole_div").removeClass("move");
		//$("#hie_menu").show();
		$(this).hide();
		$("#page").css({"height":"auto","overflow":"auto"});
	});
	
	$("#close-men").click(function() {
		$(".hole_div").removeClass("move");
		$("#hie_menu").show();
		$("#new").hide();
		$("#page").css({"height":"auto","overflow":"auto"});
	});
});

//Mobile Multi Level Menu
$(".u-menu").vmenuModule({
	Speed: 200,
	autostart: true,
	autohide: true
});
$(".u-menu ul li ul").hide();

function loadImgLazy() {
	$('.lazy-img:visible').each(function() {
		var elementTop = $(this).offset().top;
		var elementBottom = elementTop + $(this).outerHeight();
		var viewportTop = $(window).scrollTop();
		var viewportBottom = viewportTop + $(window).height();
		var srcImg = $(this).data('src');
		if (elementBottom > viewportTop && elementTop < viewportBottom && typeof srcImg !== 'undefined') {
			$(this).fadeTo(250, 0.30, function() {
				$(this).attr("src", srcImg);
			}).fadeTo(250, 1);
			$(this).removeClass('lazy-img');
			$(this).removeData('src');
		}
	});
}

$(window).on('resize scroll', function() {
	loadImgLazy();
	loadVideoLazy();
});

$(window).on('load', function() {
	loadImgLazy();
	loadVideoLazy();
});

$('.arees-list li').on('mouseover', function () {
	loadImgLazy();
	loadVideoLazy();
});

function loadVideoLazy() {
	$('.lazy-vid').each(function() {
		var elementTop = $(this).offset().top;
		var elementBottom = elementTop + $(this).outerHeight();
		var viewportTop = $(window).scrollTop();
		var viewportBottom = viewportTop + $(window).height();
		var srcVid = $(this).data('src');

		if (elementBottom > viewportTop && elementTop < viewportBottom && typeof srcVid !== 'undefined') {
			var vidType = $(this).data('type');
			var vidText = $(this).data('text');

			$(this).append('<video id="video" width="100%" loop controls>' +
				'<source src="' + srcVid + '" type="' + vidType + '"' +
				'<p>' + vidText + '</p>' +
				'</video>');
			$(this).removeClass('lazy-vid');
			$(this).removeData('src');
		}
	});
}