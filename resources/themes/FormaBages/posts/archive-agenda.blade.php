@extends('Front::layouts.base')

@section('content')
@php
    $cursos = get_posts("curso");
@endphp
<!--Inner Page Starts-->
<section class="inner-page cursos1">
    <div class="bread-cum">
        <div class="row">
            <ul>
                <li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
                <li>@lang('Agenda')</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <h1 class="page-title">@lang('Agenda')</h1>
        <div class="la-tag desktop">
            <div class="sline">@lang('Estàs visualitzant el dia:')</div>
            <ul class="recent-list">
            </ul>
        </div>
        <!--Main Box Starts-->
        <section class="main-box">
            <div class="filter-div"><a href="javascript:void(0);" id="filter-icon"><span>@lang('Filtrar')</span></a></div>
            <!--  Filter Section in Mobile  -->
            <div class="la-tag mob">
            </div>
            <div class="left-content">
                <div class="filter-box">
                    <div id='getcal'>
                        <div id="calendar" class="ch-cal"></div>
                    </div>

                    <div class="white-box">
                        <!--White Box Starts-->
                        <h1>@lang('Filtra per')</h1>
                        <div class="ltitle">@lang('Cerca per text')</div>
                        <div class="lsearch">
                            <input id="input_search" type="text" name="search" placeholder="@lang('Cerca')">
                            <input id="search" type="button" value="Search">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!--White Box Ends-->
                </div>
            </div>
            <!--Left Content Ends-->
            <!--Right Content Starts-->
            <div class="right-content">
                <ul id="curs_found" class="agenda">
                @for($i = 0; $i < 10; $i++)
                    <li class="skeleton">
                        <div class="title">
                            <h1></h1>
                        </div>
                        <div class="values">
                            <span></span>
                            <p></p>
                        </div>
                        <span></span>
                    </li>
                @endfor
                </ul>
            </div>
            <!--Right Content Ends-->
        </section>
        <!--Main Box Ends-->

    </div>
</section>
<!--Inner Page Ends-->
@endsection

@section('scripts')
<script>
var calendari = [];

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function paintDates(year, month) {
    if (typeof calendari[year] !== "undefined") {
        if (typeof calendari[year][month] !== "undefined") {
            calendari[year][month].forEach(function(dia) {
                var dia = $(`[data-year=${year}][data-month=${month}]`).filter(function() {
                    return $(this).text() == dia;
                });
                dia.find("a").addClass("activitat");
            });
        }
    }
}

$(function() {
    @foreach($cursos as $curs)
        @if($inici = $curs->get_field('data-inici-del-curs'))
            @php($date = Date::createFromFormat("d/m/Y", $inici))
            @php($year = $date->format('Y'))
            @php($mes = intval(ltrim($date->format('m'), 0)) - 1)

            if (typeof calendari[{{ $year }}] === "undefined") {
                calendari[{{ $year }}] = [];
            }

            if (typeof calendari[{{ $year }}][{{ $mes }}] === "undefined") {
                calendari[{{ $year }}][{{ $mes }}] = [];
            }

            calendari[{{ $year }}][{{ $mes }}].push({{ ltrim($date->format('d'), 0) }});
        @endif
    @endforeach


    @foreach($items as $item)
        @if ($publicacio = $item->get_field('publicacio'))
            @php($date = Date::createFromFormat("d/m/Y", $publicacio))
            @php($year = $date->format('Y'))
            @php($mes = intval(ltrim($date->format('m'), 0)) - 1)

            if (typeof calendari[{{ $year }}] === "undefined") {
                calendari[{{ $year }}] = [];
            }

            if (typeof calendari[{{ $year }}][{{ $mes }}] === "undefined") {
                calendari[{{ $year }}][{{ $mes }}] = [];
            }

            calendari[{{ $year }}][{{ $mes }}].push({{ ltrim($date->format('d'), 0) }});
        @endif
    @endforeach

    var currentDate = new Date($('#calendar').datepicker( "getDate" ));
    var year = currentDate.getFullYear();
    var month = currentDate.getMonth();

    paintDates(year, month);
})
</script>

<script type="text/javascript">
    var metas = [];
    var metas_or = [];
    var orwhere = [];
    var ajax = null;

    function searchText(value) {
        orwhere = [
            ['title', 'LIKE', '%' + value + '%'],
            ['description', 'LIKE', '%' + value + '%']
        ];
        carregarAgenda();
    }

    $("#input_search").keypress(function(e) {
        if (e.keyCode == 13) {
            var value = $(this).val();
            searchText(value);
            return false;
        }
    });

    $("#search").on('click', function(e) {
        e.preventDefault();
        var value = $("#input_search").val();
        if (value.length > 0) searchText(value);
    });

    $(document).on("click", ".close-co", function() {
        $(this).parent().remove();
        var url = new URL(window.location.href);
        
        url.searchParams.delete("page");
        url.searchParams.delete("data");
        metas = [];
        window.history.pushState({},"", decodeURIComponent(url.href));
        carregarAgenda()
    });

    function carregarAgenda() {
        if (ajax !== null) ajax.abort();

        var url = new URL(window.location.href);
        $('.right-content').addClass('loading');
        
        ajax = $.ajax({
            url: "{{ route('ajax') }}",
            method: "POST",
            data: {
                action: "loadAgenda",
                method: "html",
                parameters: {
                    orwhere: orwhere,
                    metas: metas,
                    metas_or: metas_or,
                    page: url.searchParams.get('page')
                }
            },
            success: function(data) {
                $('.right-content').html(data);
            },
            complete: function() {
                $('.right-content').removeClass('loading');
                ajax = null;
            }
        })
    }

    $('#calendar').datepicker({
        inline: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNames: ["Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre",
            "Octubre", "Novembre", "Desembre"
        ],
        dayNamesMin: ['Dg', 'Dl', 'Dt', 'Dc', 'Dj', 'Dv', 'Ds'],
        dateFormat: "dd / mm / yy",
        onSelect: function (date, el) {
            var selected = $(this).val();
            var selectedDayClass = selected;
            selectedDayClass = selectedDayClass.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '')

            var alreadySelected = 1;
            $('ul.recent-list li').each(function (i) {
                if ($(this).text().trim() == selected) {
                    alreadySelected = 0;
                    $(this).remove();
                }
            });

            if (alreadySelected == 1) {
                $(".recent-list").html('<li>' + selected +
                    ' <a href="javascript:void(0)" class="close-co"><img class="close_tag" src="{{ asset($_front.'images/close.png') }}" alt=""></a></li>');
            }

            var url = new URL(window.location.href);
            var currentDate = date.split(" / ");
            currentDate = new Date(`${currentDate[2]}-${currentDate[1]}-${currentDate[0]}`);
            var year = currentDate.getFullYear();
            var month = currentDate.getMonth();
            var dia = currentDate.getDate();

            var data = pad(dia, 2) + "-" + pad((month + 1), 2) + "-" + year;

            metas = [
                ['publicacio', data.replace(/-/g, "/")]
            ];
            
            url.searchParams.delete("page");
            url.searchParams.set("data", data);
            window.history.pushState({},"", decodeURIComponent(url.href));

            carregarAgenda();
            
            setTimeout(function () {
                // fnCalendarClass(selectedDayClass);
                paintDates(year, month);
            }, 15);
        },
        onChangeMonthYear: function(year, month) {
            var month = month - 1;
            setTimeout(function() {
                paintDates(year, month);
            }, 300)
        }
    });

    function fnCalendarClass(selectedDayClass) {
        $('.ui-datepicker-current-day').attr("id", "selecteddate" + selectedDayClass);
        $('ul.recent-list li').each(function (i) {
            var selectedDayClass = $(this).text().trim();
            selectedDayClass = selectedDayClass.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '')
            $('#selecteddate' + selectedDayClass).find('a').attr('data-model', 'agenda-calendar');
        });
    }


    $(function() {

    });

    $(document).ready(function () {
        var url = new URL(window.location.href);
        var data = url.searchParams.get("data");

        if (data) {
            data = data.split("-");
            currentDate = new Date(`${data[1]}-${data[0]}-${data[2]}`);
            
            console.log(currentDate);
            
            var year = currentDate.getFullYear();
            var month = currentDate.getMonth();
            var dia = currentDate.getDate();
            var data = pad(dia, 2) + "/" + pad((month + 1), 2) + "/" + year;

            metas = [ ["publicacio", data] ];
            
            $(".recent-list").html('<li>' + dia + " / " + month + " / " + year +
                    ' <a href="javascript:void(0)" class="close-co"><img class="close_tag" src="{{ asset($_front.'images/close.png') }}" alt=""></a></li>');
        }

        carregarAgenda();
    });
</script>
@endsection