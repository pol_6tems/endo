@extends('Front::layouts.base')

@section('content')
<!--Inner Page Starts-->
<section class="inner-page">
    <div class="bread-cum">
        <div class="row">
            <ul>
                <li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
                <li>@lang('Notícies')</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <h1 class="page-title">@lang('Notícies')</h1>

        <!--Main Box Starts-->
        <section class="main-box noti">
            <!--Left Content Starts-->
            <div class="filter-div"><a href="javascript:void(0);" id="filter-icon"><span>@lang('Filtrar')</span></a></div>
            <div class="left-content">
                <div class="filter-box">
                    <div class="white-box">
                        <!--White Box Starts-->
                        <h1>@lang('Filtra per')</h1>
                        <div class="ltitle">@lang('Cerca per text')</div>
                        <div class="lsearch">
                            <input id="input_search" type="text" placeholder="@lang('Cerca')">
                            <input id="search" type="button" value="@lang('Search')">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!--White Box Ends-->
                </div>
            </div>
            <!--  Filter Section -->
            <div class="la-tag la-tag-notice desktop">
                <div class="sline">@lang('Estas Visualitzant:')</div>
                <ul class="recent-list">
                    <li>@lang('Totes les notícies') <a href="javascript:void(0)" class="close-ico"> <img src="{{ asset($_front.'images/close.png') }}" alt=""> </a></li>
                </ul>
            </div>

            <!--Left Content Ends-->
            <!--Right Content Starts-->
            <div class="right-content">
                @for($i = 0; $i < 10; $i++)
                    <li class="skeleton">
                        <div class="title">
                            <h1></h1>
                            <span>
                                <a href="javascript:void(0);"></a>
                                <a href="javascript:void(0);"></a>
                            </span>
                        </div>
                        <div class="values">
                            <span></span>
                            <p></p>
                            <span></span>
                        </div>
                    </li>
                @endfor
            </div>
            <!--Right Content Ends-->
        </section>
        <!--Main Box Ends-->

    </div>
</section>
<!--Inner Page Ends-->
@endsection

@section('scripts')
<script type="text/javascript">
    var metas = [];
    var orwhere = [];
    var metas_or = [];
    var ajax = null;
    
    function searchText(value) {
        orwhere = [
            ['title', 'LIKE', '%' + value + '%'],
            ['description', 'LIKE', '%' + value + '%']
        ];
        carregarNoticies();
    }

    $("#input_search").keypress(function(e) {
        if (e.keyCode == 13) {
            var value = $(this).val();
            searchText(value);
            return false;
        }
    });

    $("#search").on('click', function(e) {
        e.preventDefault();
        var value = $("#input_search").val();
        if (value.length > 0) searchText(value);
    });

    
    function carregarNoticies() {
        if (ajax !== null) ajax.abort();

        var url = new URL(window.location.href);
        $('.right-content').addClass('loading');

        ajax = $.ajax({
            url: "{{ route('ajax') }}",
            method: "POST",
            data: {
                action: "loadNoticias",
                method: "html",
                parameters: {
                    metas: metas,
                    metas_or: metas_or,
                    orwhere: orwhere,
                    page: url.searchParams.get('page')
                }
            },
            success: function(data) {
                $('.right-content').html(data);
            },
            complete: function() {
                $('.right-content').removeClass('loading');
                ajax = null;
            }
        })
    }

    $(document).ready(function() {
        carregarNoticies();
    });
</script>
@endsection