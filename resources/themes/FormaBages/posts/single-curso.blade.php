<!--Banner Starts-->
@extends('Front::layouts.base')

@section("styles")
	<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5db2ae08020ee70012a2ad50&product=inline-share-buttons' async='async'></script>
@endsection

@section('content')
@php
$tipo = $item->get_field('tipus-formacio');
$familia = $item->get_field('familia-formativa');

$temario = $item->get_field('temari');
$horario = $item->get_field('horari');
$centre = $item->get_field('centre');

$web = $item->get_field('enllac');
$email = $centre ? $centre->get_field('email') : null;

$webCentre = $centre ? $centre->get_field('url') : null;

if (is_null($email) && strrpos($webCentre, "@") !== false) {
    $aux = explode("@", $webCentre);
    $email = $webCentre;
    $webCentre = (strpos($aux[1], 'http://') !== false || strpos($aux[1], 'https://') !== false) ? $aux[1] : 'http://'.$aux[1];
} else if (is_null($webCentre) && strrpos($email, "@") !== false) {
    $aux = explode("@", $email);
    $webCentre = (strpos($aux[1], 'http://') !== false || strpos($aux[1], 'https://') !== false) ? $aux[1] : 'http://'.$aux[1];
}

if ($webCentre && !isset(parse_url($webCentre)['scheme'])) {
    $webCentre = 'http://' . $webCentre;
}

if (!$web) {
    $web = $webCentre;
}

if (is_null($email) && strrpos($web, "@") !== false) {
    $aux = explode("@", $web);
    $email = $web;
    $web = (strpos($aux[1], 'http://') !== false || strpos($aux[1], 'https://') !== false) ? $aux[1] : 'http://'.$aux[1];
} else if (is_null($web) && strrpos($email, "@") !== false) {
    $aux = explode("@", $email);
    $web = (strpos($aux[1], 'http://') !== false || strpos($aux[1], 'https://') !== false) ? $aux[1] : 'http://'.$aux[1];
}

if ($web && !isset(parse_url($web)['scheme'])) {
    $web = 'http://' . $web;
}

if (!$webCentre) {
    $webCentre = $web;
}

if ($email) {
    $email = strtolower($email);
}

$introduccio = $item->get_field('introduccio');

if ($familia) {
    $relacionats = get_posts([
        "post_type" => "curso",
        "metas" => [ ["familia-formativa", is_object($familia) ? $familia->id : $familia] ],
    ])->shuffle();

    $relacionats = $relacionats->filter(function($post) use ($item) {
        return $post->id != $item->id;
    });

    $relacionats->splice(3);
}

$requisits = $item->get_field('requisits');
$tramits = $item->get_field('text-tramits');

$menuLinks = 0;
@endphp

<!--Cursos Box Starts-->
<section class="inner-page">
    <div class="bread-cum">
        <div class="row">
            <ul>
                <li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
                <li><a href="{{ get_archive_link('curso') }}">@lang('Cursos')</a></li>
                @if ($tipo && is_object($tipo))
                    <li><a href="{{ get_archive_link('curso') . '?tipus-formacio=' . $tipo->post_name }}">{{ $tipo->title }}</a></li>
                @endif
                <li>{{ $item->title }}</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="cursos-out">
            <div class="curs-lft">
                <h1>
                    <a href="javascript:void(0);">{{ $item->title }}</a>
                    <img src="{{ asset($_front.'images/share-mob.svg') }}" alt="" border="0">
                </h1>
                <span>
                    {{-- <a href="javascript:void(0);"><strong>{{ ucfirst(strtolower($familia->title)) }}</strong></a> --}}
                    @if ($tipo && is_object($tipo))
                        <a href="{{ get_archive_link('curso') . '?tipus-formacio=' . $tipo->post_name }}"><strong>{{ $tipo->title }}</strong></a>
                    @endif
                    @if ($familia && is_object($familia))<a href="{{ get_archive_link('curso') . '?familia-formativa=' . $familia->post_name }}">{{ $familia->title }}</a>@endif
                </span>
                <ul>
                    <li class="time">{{ $item->get_field('duracio') }}</li>
                    @if ($localitzacio = $item->get_field('localitzacio'))
                        <li class="loc">{{ $localitzacio->title }}</li>
                    @endif
                    @php($inici = $item->get_field('data-inici-del-curs'))
                    @if ($inici && $inici != "")
                        @php($final = $item->get_field('data-final-del-curs'))
                        <li class="date">
                            @lang('Inici:') {{ print_course_date($item, $inici, false) }}
                            @if ($final) - &nbsp; @lang('Fi:') {{ print_course_date($item, $final, false) }} @endif
                        </li>
                    @endif
                </ul>
            </div>

            <div class="curs-rgt">
                <div class="curs-btns">
                    <ul>
                        <li>
                            <div class="sharethis-inline-share-buttons"></div>
                        </li>
                        @if ($inici && $centre && $web)
                            @if (Date::createFromFormat("d/m/Y", $inici) > Date::now())
                                <li><a href="{{ $web }}" target="_blank" class="pink-btn1">@lang('Preinscripció')</a></li>
                            @else
                                @if (!$final || Date::createFromFormat("d/m/Y", $final) >= Date::now())
                                    <li><a href="{{ $web }}" target="_blank" class="pink-btn1">@lang('Per a més informació')</a></li>
                                @else
                                    <li><a href="{{ $web }}" target="_blank" class="pink-btn1">@lang('Curs Finalitzat')</a></li>
                                @endif
                            @endif
                        @endif
                    </ul>
                </div>

            </div>
        </div>

        <div class="curso-menu">
            <ul class="curso-menu-links">
                @if($introduccio)
                    <li><a href="javascript:void(0);" class="intro-mn">@lang('Introducció')</a></li>
                    @php($menuLinks++)
                @endif
                @if($temario)
                    <li><a href="javascript:void(0);" class="temari-mn">@lang('Temari')</a></li>
                    @php ($menuLinks++)
                @endif
                @if($horario)
                    <li><a href="javascript:void(0);" class="horari-mn">@lang('Horari')</a></li>
                    @php ($menuLinks++)
                @endif
                @if($centre)
                    <li><a href="javascript:void(0);" class="centre-mn">@lang('Centre')</a></li>
                    @php ($menuLinks++)
                @endif
                @if($requisits && $menuLinks < 4)
                    <li><a href="javascript:void(0);" class="requisits-mn">@lang('Requisits')</a></li>
                    @php ($menuLinks++)
                @endif
                @if($tramits && $menuLinks < 4)
                    <li><a href="javascript:void(0);" class="tramits-mn">@lang('Tràmits')</a></li>
                    @php ($menuLinks++)
                @endif
            </ul>
            <div class="curso-menu-cont">
                <section class="intro" id="intro">
                    @if ($introduccio)
                        <h1>@lang('Introducció')</h1>
                        <p>{!! $introduccio !!}</p>
                    @endif

                    <h1>@lang('Sobre el curs')</h1>
                    <ul class="intro-list">
                        <li>
                            <div class="lst-lft">@lang('Acció formativa')</div>
                            <div class="lst-rgt">{{ $item->title }}</div>
                        </li>

                        @if ($cifo = $item->get_field('cifo'))
                        <li>
                            <div class="lst-lft">@lang('CIFO')</div>
                            <div class="lst-rgt">{{ $cifo }}</div>
                        </li>
                        @endif

                        @if ($modalitat = $item->get_field('modalitat'))
                        <li>
                            <div class="lst-lft">@lang('Modalitat')</div>
                            <div class="lst-rgt">{{ ucfirst($modalitat) }}</div>
                        </li>
                        @endif

                        @if ($estat = $item->get_field('estat-curs'))
                        <li>
                            <div class="lst-lft">@lang('Estat curs')</div>
                            <div class="lst-rgt">{{ $estat }}</div>
                        </li>
                        @endif

                        @if (isset($familia) && $familia && is_object($familia))
                            <li>
                                <div class="lst-lft">@lang('Família professional')</div>
                                <div class="lst-rgt">{{ $familia->title }}</div>
                            </li>
                        @endif

                        @if ($id_curs = $item->get_field('identificador-del-curs'))
                        <li>
                            <div class="lst-lft">@lang('Identicador del curs:')</div>
                            <div class="lst-rgt">{{ $id_curs }}</div>
                        </li>
                        @endif

                        @if ($preseleccio = $item->get_field('data-inici-preseleccio'))
                        <li>
                            <div class="lst-lft">@lang('Data Inici Preseleccio:')</div>
                            <div class="lst-rgt">{{ print_course_date($item, $preseleccio) }}</div>
                        </li>
                        @endif

                        @if ($inici = $item->get_field('data-inici-del-curs'))
                        <li>
                            <div class="lst-lft">@lang('Data inici del curs:')</div>
                            <div class="lst-rgt">{{ print_course_date($item, $inici) }}</div>
                        </li>
                        @endif
                        
                        @if ($final = $item->get_field('data-final-del-curs'))
                        <li>
                            <div class="lst-lft">@lang('Data final del curs:')</div>
                            <div class="lst-rgt">{{ print_course_date($item, $final) }}</div>
                        </li>
                        @endif

                        @if($altres = $item->get_field('altres'))
                            @foreach($altres as $altre)
                                <li>{{ $altre['item']['value'] }}</li>
                            @endforeach
                        @endif
                    </ul>
                </section>

                @if( $temario )
                <section class="temari" id="temari">
                    <h1>@lang('Temari')</h1>
                    <ul class="temari-list">
                        @foreach($temario as $tema)
                            <li>{{ $tema['tema']['value'] }}</li>
                        @endforeach
                    </ul>
                </section>
                @endif

                @if( $horario )
                <section class="horari" id="horari">
                    <h1>@lang('Horari')</h1>
                    <ul class="temari-list">
                        @foreach($horario as $dia)
                        <li>{{ $dia['dia']['value'] }}</li>
                        @endforeach
                    </ul>
                </section>
                @endif

                @if( $centre )
                    <section class="centre" id="centre">
                        <h1>@lang('Centre')</h1>
                        <p>{{ centre_title($centre->title) }}<br>
                        {{ $centre->get_field('direccion') }}<br>
                        <a href="tel:{{ $centre->get_field('telefono') }}">{{ $centre->get_field('telefono') }}</a><br>
                        <a href="mailto:{{ $email }}">{{ $email }}</a>
                        @if ($poblacion = $centre->get_field('poblacion'))
                            {{ $poblacion->title }}
                        @endif
                        @if ($web)
                            <br><br><a href="{{ $webCentre }}" target="_blank" class="pink-btn1" title="{{ $centre->title }}">@lang('Veure web')</a>
                        @endif
                        </p>
                    </section>
                @endif

                @if($requisits)
                    <section class="requisits" id="requisits">
                        <h1>@lang('Requisits')</h1>
                        <ul class="temari-list">
                            @foreach($requisits as $requisit)
                                <li>{{ ucfirst($requisit['requisit']['value']) }}</li>
                            @endforeach
                        </ul>
                    </section>
                @endif

                @if($tramits)
                    <section class="tramits" id="tramits">
                        <h1>@lang('Tràmits')</h1>
                        {!! $tramits !!}
                    </section>
                @endif

                @if($continuitat = $item->get_field('text-continuitat'))
                    <section class="continuitat" id="continuitat">
                        <h1>@lang('Continuïtat')</h1>
                        {!! $continuitat !!}
                    </section>
                @endif

                {{--
                <div class="down-btn">
                    <a href="javascript:void(0);" class="down">@lang('Descarregar informació')<span></span> </a>
                </div>
                --}}
            </div>
        </div>


        @if(isset($relacionats) && count($relacionats) > 0)
            <div class="agenda-list curso-list curs-lst">
                <h1>@lang('Cursos relacionats')</h1>
                <ul>
                    @foreach($relacionats as $curs)
                        @includeIf('Front::partials.small-curso', ['curs' => $curs])
                    @endforeach
                </ul>


                <div class="mobile-link-emp mobile-link-curs">
                    <a href="{{ get_archive_link('curso') }}" class="veure-tot">@lang('Veure \'ls tots')</a>
                    <div class="owl-carousel" id="curs-links">
                        @foreach($relacionats as $curs)
                        <div class="item">
                            <ul>
                                @includeIf('Front::partials.small-curso', ['curs' => $curs])
                            </ul>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>
<!--Cursos Box Ends-->
@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    /*Smooth scrolling*/
    $(".intro-mn").click(function() {
        $('html, body').animate({scrollTop: $("#intro").offset().top - $('#sticky-wrapper').height() - 60}, 1000);
    });

    @if($temario)
    $(".temari-mn").click(function() {
        $('html, body').animate({scrollTop: $("#temari").offset().top - $('#sticky-wrapper').height() - 60}, 1000);
    });
    @endif

    @if($horario)
    $(".horari-mn").click(function() {
        $('html, body').animate({scrollTop: $("#horari").offset().top - $('#sticky-wrapper').height() - 60}, 1000);
    });
    @endif
        
    $(".centre-mn").click(function() {
        $('html, body').animate({scrollTop: $("#centre").offset().top - $('#sticky-wrapper').height() - 60}, 1000);
    });

    @if($requisits)
        $(".requisits-mn").click(function() {
            $('html, body').animate({scrollTop: $("#requisits").offset().top - $('#sticky-wrapper').height() - 60}, 1000);
        });
    @endif

    @if($tramits)
        $(".tramits-mn").click(function() {
            $('html, body').animate({scrollTop: $("#tramits").offset().top - $('#sticky-wrapper').height() - 60}, 1000);
        });
    @endif
});

$(document).ready(function() {
    var owl = $('#curs-links');
    owl.owlCarousel({
        goToFirstSpeed :1000,
        loop:true,
        items:1,
        margin:0,
        autoplay:true,
        autoplayTimeout:5500,
        autoplayHoverPause:true,		
        nav : false,
        dots : true,
        responsive: {
            0:{ items:1, margin:0,},
            480:{ items:1},
            640:{ items:1},
            768:{ items:1},
            1024:{ items:1}
        },
    });
});
</script>
@endsection