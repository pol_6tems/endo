@extends('Front::layouts.base')

@section("styles")
	<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5dcd6ae554b7c100121d598b&product=inline-share-buttons&cms=sop' async='async'></script>
@endsection

@section('content')
<!--Cursos Box Starts-->
<section class="inner-page">
	<div class="bread-cum">
		<div class="row">
			<ul>
				<li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
				<li><a href="{{ get_archive_link('noticia') }}">@lang('Notícies')</a></li>
				<li>{{ $item->title }}</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<h1 class="page-title">@lang('Notícies')</h1>
	</div>

	<div class="qui-som-outer">
		<div class="row">
			<div class="agenda-cont">
				@if ($publicacio = $item->get_field('data-publicacio'))
					<h4 class="date">{{ getDayAndMonthCat($publicacio) }}</h4>
				@endif

				<h1>{{ $item->title }}</h1>
				<div class="notices-det-img">
					@if($img = $item->translate(true)->media)
						<img src="{{ $img->get_thumbnail_url() }}" alt="" />
					@endif
				</div>
				<p>{!! $item->description !!}</p>
				<ul class="link-list">
					@if ($url = $item->get_field('url'))
						<li class="link">
							<a href="{{ $url }}" target="_blank">@lang('Veure més informació')<span></span> </a>
						</li>
					@endif

					@if ($pdf = $item->get_field('pdf'))
						<li class="pdf">
							<a target="_blank" href="{{ $pdf->get_thumbnail_url() }}">@lang('Descarregar informació')</a>
						</li>
					@endif
				</ul>
				<span>
					<div class="sharethis-inline-share-buttons"></div>
				</span>
			</div>
		</div>
	</div>

	@php($noticies = get_posts([
		'post_type' => 'noticia',
		'where' => [ ['posts.id', '<>', $item->id] ]
	]))
	@if ($noticies = $noticies->shuffle())
	<div class="agenda-list curs-lst">
		<div class="row">
			<h1>@lang('Altres notícies')</h1>
			<ul>
				@foreach($noticies->take(3) as $noticia)
					<li>
						<a href="{{ $noticia->get_url() }}">
							<h2>{{ $noticia->title }}</h2>

							@if ($publicacio = $item->get_field('data-publicacio'))
								<h3 class="date">{{ getDayAndMonthCat($publicacio) }}</h3>
							@endif
							
							{!! get_excerpt( $noticia->description, 20 ) !!}
							<span class="info-link">@lang('Més informació')</span>
						</a>
					</li>
				@endforeach
			</ul>

			<div class="mobile-agenda-list">
				<a href="{{ get_page_url('noticia') }}" class="veure-tot">@lang('Veure-ls totes')</a>
				<div class="owl-carousel" id="agenda-list">
					@foreach($noticies as $noticia)
					<div class="item">
						<ul>
							<li> <a href="{{ $noticia->get_url() }}">
									<h2>{{ $noticia->title }}</h2>

									@if ($publicacio = $item->get_field('data-publicacio'))
										<h3 class="date">{{ getDayAndMonthCat($publicacio) }}</h3>
									@endif

									{!! get_excerpt( $noticia->description, 20 ) !!}
									<span class="info-link">@lang('Més informació')</span>
								</a>
							</li>
						</ul>
					</div>
					@endforeach
				</div>
			</div>

		</div>
	</div>
	@endif
</section>
<!--Cursos Box Ends-->
@endsection

@section('scripts')
<script type="text/javascript">
    var owl = $('#agenda-list');
    owl.owlCarousel({
        goToFirstSpeed: 2000,
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 7000,
        smartSpeed: 2000,
        autoplayHoverPause: true,
        nav: true,
        dots: true
    });
</script>
@endsection