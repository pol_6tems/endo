<!--Banner Starts-->
@extends('Front::layouts.base')

@section("styles")
	<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5dcd6ae554b7c100121d598b&product=inline-share-buttons&cms=sop' async='async'></script>
@endsection

@section('content')
@php
$relacionades = get_posts([
    'post_type' => 'agenda',
    'where' => [ ['posts.id', '<>', $item->id] ]
]);
@endphp
    <!--Cursos Box Starts-->
    <section class="inner-page">
        <div class="bread-cum">
            <div class="row">
                <ul>
                    <li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
                    <li><a href="{{ get_archive_link('agenda') }}">@lang('Agenda')</a></li>
                    <li>{!! get_excerpt( $item->title, 10) !!}</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <h1 class="page-title">@lang('Agenda')</h1>
        </div>
        <div class="qui-som-outer">
            <div class="row">
                <div class="agenda-cont">
                    @if ($publicacio = getDayAndMonthCat($item->get_field('publicacio')))
                        <h4 class="date">{{ $publicacio }}</h4>
                    @endif

                    <h1>{{ $item->title }}</h1>
                    
                    @if ($item->get_field('subtitol') && $item->get_field('subtitol') != "")
                    <span class="subtitol">
                        {{ $item->get_field('subtitol') }}
                    </span>
                    @endif

                    <div class="image">
                        @if($image = $item->media())
                            <img src="{{ $image->get_thumbnail_url() }}" alt="">
                        @endif
                    </div>

                    <div class="metas">
                        <span class="loc">{{ $item->get_field('localitzacio') }}</span>
                        <span><strong>@lang("Organitzador")</strong>: {{ $item->get_field('organitzador') }}</span>
                    </div>
                    <p>{!! strip_tags($item->description, "<img><br>") !!}</p>

                    @php($url = $item->get_field('url'))
                    @if ($url != '')
                        @if (filter_var($url, FILTER_VALIDATE_URL))
                        <ul class="link-list">
                            <li class="link"><a target="_blank" href="{{ $url }}">@lang('Veure més informació')</a></li>
                        </ul>
                        @endif
                    @endif
                    <ul>
                        <li>
                            <div class="sharethis-inline-share-buttons"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        @if ($relacionades = $relacionades->shuffle())
        <div class="agenda-list curs-lst">
            <div class="row">
                <h1>@lang('Altres esdeveniments')</h1>
                <ul>
                    @foreach($relacionades->take(3) as $relacionada)
                    <li> <a href="{{ $relacionada->get_url() }}">
                            <h2>{{ $relacionada->title }}</h2>
                            <div class="metas">
                                @if ($publicacio = getDayAndMonthCat($item->get_field('publicacio')))
                                    <span class="cdate">{{ $publicacio }}</span>
                                @endif
                                <span class="loc">{{ $item->get_field('localitzacio') }}</span>
                            </div>

                            {!! get_excerpt( $relacionada->description, 25) !!}
                            <br><span class="info-link">@lang('Més informació')</span>
                        </a> </li>
                    @endforeach
                </ul>

                <div class="mobile-agenda-list">
                    <a href="{{ get_archive_link('agenda') }}" class="veure-tot">@lang('Veure-ls totes')</a>
                    <div class="owl-carousel" id="agenda-list">
                        @foreach($relacionades->take(3) as $relacionada)
                        <div class="item">
                            <ul>
                                <li> <a href="{{ $relacionada->get_url() }}">
                                        <h2>{{ $relacionada->title }}</h2>
                                        {!! get_excerpt( $relacionada->description, 25) !!}
                                        <span class="info-link">@lang('Més informació')</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
    </section>
    <!--Cursos Box Ends-->
@endsection

@section('scripts')
<script type="text/javascript">
    var metas = [];
    var metas_or = [];
    var ajax = null;

    var owl = $('#agenda-list');
    owl.owlCarousel({
        goToFirstSpeed :1000,
        loop:true,
        items:1,
        margin:0,
        autoplay:true,
        autoplayTimeout:5500,
        autoplayHoverPause:true,		
        nav : false,
        dots : true,
        responsive: {
            0:{ items:1, margin:0,},
            480:{ items:1},
            640:{ items:1},
            768:{ items:1},
            1024:{ items:1}
        },
    });

    $(document).on('ready', function() {
        $("#search-icon").click(function() {
            $(this).toggleClass('open');
            $('.inner-search').toggleClass('open-search');
        });     
    });
</script>
<script type="text/javascript">
$(function() {
    $('.compar').on('click', function() {
        $('.comp-share-ico').show(); 
    });
});
</script>
<script>
$(document).ready(function(){ 
    var win_hgt;
    $(window).resize(function(){ 
        win_hgt = $(window).height();  
        $(".move #page").css({"height":win_hgt,"overflow":"hidden"}); 
    });
    
    $(document).ready(function(){  
        
        win_hgt = $(window).height();		 
        $("#hie_menu").click( function(){
            $(".hole_div").addClass("move");
            //$(this).hide();
            $("#new").show();
            $("#page").css({"height":win_hgt,"overflow":"hidden"});
        });
                    
        $("#new").click(function(){
            $(".hole_div").removeClass("move");
            //$("#hie_menu").show();
            $(this).hide();
            $("#page").css({"height":"auto","overflow":"auto"});
        });
        
        $("#close-men").click(function(){
            $(".hole_div").removeClass("move");
            $("#hie_menu").show();
            $("#new").hide();
            $("#page").css({"height":"auto","overflow":"auto"});
        });
    });	
});

//Mobile Multi Level Menu		
    $(".u-menu").vmenuModule({
        Speed: 200,
        autostart: true,
        autohide: true
    });
    $(".u-menu ul li ul").hide();
</script>
@endsection