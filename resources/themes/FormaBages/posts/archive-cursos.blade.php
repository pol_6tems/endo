@extends('Front::layouts.base')


@section('content')
<!--Inner Page Starts-->
<section class="inner-page cursos">
    <div class="bread-cum">
        <div class="row">
            <ul>
                <li><a href="{{ get_page_url('inici') }}">@lang('Inici')</a></li>
                <li>@lang('Cursos')</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <h1 class="page-title"></h1>

        <!--  Filter Section -->
        <div class="la-tag desktop">
            <div class="sline recent-title">@if(request()->search_query)@lang('La teva selecció actual:')@else @lang('Utilitza el filtre per trobar els cursos del teu interès')@endif</div>
            <ul class="recent-list">
                @if ($search = request()->search_query)
                    <li>@lang('Cerca: :search', ['search' => $search]) <a data-cf="search_query" href="javascript:void(0)" onclick="$('[name=search_query]').val('')" class="close-ico"><img src="{{ asset($_front."images/close.png") }}" alt=""></a></li>
                @endif
            </ul>
        </div>

        <!--Main Box Starts-->
        <section class="main-box">
            <!--Left Content Starts-->
            <div class="filter-div"><a href="javascript:void(0);" id="filter-icon"><span>@lang('Filtrar')</span></a></div>
            <div class="left-content">
                <div class="filter-box">
                    <div class="white-box">
                        <!--White Box Starts-->
                        @includeIf('Front::partials.filters')
                    </div>
                    <!--White Box Ends-->

                    @php ($cercaError = \App\Post::ofName('errors-cerca')->ofType('page')->first())
                    <div class="trobes">
                        <div><img src="{{ asset($_front.'images/not-found.png') }}" alt="" border="0"></div>
                        <h3>
                            @if ($cercaError && $cercaError->get_field('titol-caixa-lateral'))
                                {{ $cercaError->get_field('titol-caixa-lateral') }}
                            @else
                                @lang('No trobes el curs que estaves buscant?')
                            @endif
                        </h3>
                        @if ($cercaError && $cercaError->get_field('descripcio-caixa-lateral'))
                            {!! $cercaError->get_field('descripcio-caixa-lateral') !!}
                        @else
                            @lang('Contacta’ns i fes-nos saber què t’interessa')
                        @endif
                        <a href="{{ get_page_url('contacte') }}" class="link">@lang('Contactar')</a>
                    </div>
                </div>
            </div>
            <!--Left Content Ends-->

            <!--  Filter Section in Mobile  -->
            <div class="la-tag mob">

            </div>

            <!--Right Content Starts-->
            <div class="right-content">
                <div class="order-container">
                    <div>@lang('Ordenar per'):</div>
                    <div class="recom">
                        <select class="select_box js-order-by">
                            {{--<option value="default" @if (request('order', 'default') == 'default')selected @endif>@lang('Recomanat')</option>--}}
                            <option value="alphabetical" @if (request('order', 'alphabetical') == 'alphabetical')selected @endif>@lang('Ordre alfabètic')</option>
                            <option value="data_inici" @if (request('order', 'alphabetical') == 'data_inici')selected @endif>@lang('Data d\'inici')</option>
                            <option value="duracio" @if (request('order', 'alphabetical') == 'duracio')selected @endif>@lang('Duració')</option>
                        </select>
                    </div>
                </div>
                <ul id="curs_found">
                    @for($i = 0; $i < 10; $i++)
                    <li class="skeleton">
                        <div class="title">
                            <h1></h1>
                            <span>
                                <a href="javascript:void(0);"></a>
                                <a href="javascript:void(0);"></a>
                            </span>
                        </div>
                        <div class="values">
                            <span></span>
                            <br><span></span>
                            <br><span style="width: 60%"></span>
                            <span style="width: 60%"></span>
                            <div class="mlink"><a href="javascript:void(0);" class="skeleton-btn"></a></div>
                        </div>
                    </li>
                    @endfor
                </ul>
            </div>
            <!--Right Content Ends-->
        </section>
        <!--Main Box Ends-->

    </div>
</section>
<!--Inner Page Ends-->
@endsection


@section('scripts')
<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function focusInput() {
    $("#txt-filter").select();
}

$(function() {
    $('#v-1').on('click', function() {
        $(this).hide();
		$('#vi-course-1').show();
    });
	 $('#v-2').on('click', function() {
        $(this).hide();
		$('#vi-course-2').show();
    });

    $('#v-3').on('click', function() {
        $(this).hide();
		$('#vi-course-3').show();
    });

	$('#v-4').on('click', function() {
		$(this).hide();
		$('#vi-course-4').show();
	});
});
</script>
<script type="text/javascript">
    function removeDups(names) {
        let unique = {};
        names.forEach(function(i) {
            if(!unique[i]) {
                unique[i] = true;
            }
        });
        return Object.keys(unique);
    }

    var metas = [];
    var metas_or = [];
    var ajax = null;
    /*======================================================================================
    Filter Script 
    ======================================================================================*/
    function htmlDecode(input){
        var e = document.createElement('div');
        e.innerHTML = input;
        return e.childNodes[0].nodeValue;
    }

    function carregarCursos() {
        if (ajax !== null) ajax.abort();

        var url = new URL(window.location.href);
        $('.right-content').addClass('loading');
        ajax = $.ajax({
            url: "{{ route('ajax') }}" + url.search,
            method: "POST",
            data: {
                action: "loadCursos",
                method: "html",
                parameters: {
                    metas: metas,
                    metas_or: metas_or,
                    page: url.searchParams.get('page'),
                    search_query: url.searchParams.get('search_query'),
                    order: $('.js-order-by').val()
                }
            },
            success: function(data) {
                $('#curs_found').remove();
                $('.pagination').remove();
                $('.right-content').append(data.view);
                var title = "@lang('S’han trobat <span id=\"current\">:total</span> cursos de formació dels :total disponibles', ['total' => filterOldCourses($items)->count()])";
                var element = $('<div>' + title + '</div>');
                var $current = element.find("span");
                $current.html($("#ncursos").val());
                $(".page-title").html(element.html());

                $.each(data.filter_counts, function (index, item) {
                    $('li[data-cfid="' + index + '"] > div span.right').html(item)/*.show()*/;
                    /*$('li.active[data-cfid="' + index + '"] > div span.right').hide();

                    var activeLi = $('li.active[data-cfid="' + index + '"]');
                    var closestParentLi = activeLi.closest('li[data-cf="tipus-formacio"]');

                    closestParentLi.find('div span.right').first().show();

                    if (activeLi.data('cf') == 'subtipus-formacio') {
                        closestParentLi.find('div span.right').first().hide();
                    }*/
                });
            },
            complete: function() {
                $('.right-content').removeClass('loading');
                ajax = null;
            }
        })
    }

    function removeMeta(customField, customFieldId, cfname) {
        var url = new URL(window.location.href);
        
        metas = metas.filter(function(el) { return !(el.includes(customField) && el.includes(customFieldId)); });
        var quedan = metas.filter(function(el) { return el.includes(customField); }).length;

        if (metas_or.includes(customField) && quedan == 0) {
            var idx = metas_or.indexOf(customField);
            metas_or.splice(idx, 1);
        }

        if (url.searchParams.has(customField)) {
            var current = url.searchParams.get(customField).split(',');
            var idx = current.indexOf(cfname);
            current.splice(idx, 1);
            cfname = current.join(',');
        }

        if (cfname != 'undefined' && cfname != 'tots') {
            if (cfname == "") {
                url.searchParams.delete(customField);
            } else {
                url.searchParams.set(customField, cfname);
            }
        }

        url.searchParams.delete('page');

        window.history.pushState({},"", decodeURIComponent(url.href));
    }

    function addMeta(customField, customFieldId, cfname, click) {
        var url = new URL(window.location.href);

        metas.push([customField, customFieldId]);
        if (! metas_or.includes(customField)) {
            metas_or.push(customField);
        }

        if (url.searchParams.has(customField)) {
            var current = url.searchParams.get(customField).split(',');
            current.push(cfname);
            current = removeDups(current);
            cfname = current.join(',');
        }
        if (cfname != 'undefined' && cfname != 'tots') {
            url.searchParams.set(customField, cfname);
            fillTags();
        }

        if (click) {
            url.searchParams.delete('page');
        }
        
        window.history.pushState({},"", decodeURIComponent(url.href));
    }

    $('.js-order-by').on('change', function () {
        var url = new URL(window.location.href);

        if (url.searchParams.has('order')) {
            url.searchParams.delete('order');
        }

        if ($(this).val() != 'default') {
            url.searchParams.set('order', $(this).val());
        }

        window.history.pushState({},"", decodeURIComponent(url.href));

        carregarCursos();
    });

    $(document).ready(function() {
        var url = new URL(window.location.href);
        for( var key of url.searchParams.keys() ) {
            if ($.inArray(key, ['tipus-formacio', 'subtipus-formacio', 'familia-formativa', 'localitzacio', 'modalitat']) >= 0) {
                var param = url.searchParams.get(key);
                var params = param.split(',');

                params.forEach(function(e) {
                    if (e == 'tots') {
                        removeMeta('tipus-formacio', null, 'tots');
                    } else {
                        var element = $('[data-cf=' + key + '][data-post_name=' + e + ']');
                        var cf = element.data('cf');
                        var cfid = element.data('cfid');
                        var cfname = element.data('post_name');
                        element.addClass('active');
                        element.find('.child').show();
                        element.closest('ul.child').show();
                        element.find('.child li').addClass('active');
                        var parent = element.closest('.accordion_in');
                        var hidden = element.closest('.cat-list');
                        
                        if (! parent.hasClass('acc_active') ) {
                            parent.addClass('acc_active');
                            parent.find('.acc_content').slideDown();
                        }

                        if (hidden.is(":hidden")) {
                            parent.find('a.view').trigger('click');
                        }

                        if (cf) {
                            addMeta(cf, cfid, cfname, false);
                        }

                        $.each(element.find('.child li'), function () {
                            addMeta($(this).data('cf'), $(this).data('cfid'), $(this).data('post_name'), false);
                        });
                    }
                });
            }
        }

        carregarCursos();
    });

    $(document).on('click',".acc_content ul li > div",function() {
        var li = $(this).closest('li');
        var cf = li.data('cf');
        var cfid = li.data('cfid');
        var cfname = li.data('post_name');
        var add = true;

        if(!li.hasClass('active')) {
            li.addClass('active');
            addMeta(cf, cfid, cfname, true);
        } else {
            add = false;
            removeMeta(cf, cfid, cfname);

            li.removeClass('active');

            /*$(".recent-list li:contains("+li.find('div.container label').first().text()+")").remove();
            var test = li.children().find('label').text();
            var trim_test = $.trim(test);

            $(".recent-list li").each(function( index ) {
                var text = li.text();
                var trim_text = $.trim(text);
                if(trim_text == trim_test){
                    li.remove();
                }
            });

            if ($('.recent-list li').length == 0) {
                $('.recent-title').html('@lang('Utilitza el filtre per trobar els cursos del teu interès')')
            }*/
        }

        var parentUl = li.closest('ul.child');
        if (parentUl.length) {
            var parentLi = parentUl.closest('li');

            if (parentUl.find('li.active').length !== parentUl.find('li').length) {
                if (parentLi.hasClass('active')) {
                    removeMeta(parentLi.data('cf'), parentLi.data('cfid'), parentLi.data('post_name'));
                }

                parentLi.removeClass('active');

                if (!parentUl.find('li.active').length) {
                    parentUl.slideUp('fast');
                    $.each(parentUl.find('li'), function () {
                        removeMeta($(this).data('cf'), $(this).data('cfid'), $(this).data('post_name'))
                    });
                }
            } else if (!parentLi.hasClass('active')) {
                parentLi.addClass('active');
                addMeta(parentLi.data('cf'), parentLi.data('cfid'), parentLi.data('post_name'));
            }
        } else {
            if (li.hasClass('active')) {
                li.find('.child').slideDown('fast');
                li.find('.child li').addClass('active');

                $.each(li.find('.child li.active'), function () {
                    addMeta($(this).data('cf'), $(this).data('cfid'), $(this).data('post_name'))
                });
            } else {
                li.find('.child').slideUp('fast');
                li.find('.child li').removeClass('active');
                $.each(li.find('.child li'), function () {
                    removeMeta($(this).data('cf'), $(this).data('cfid'), $(this).data('post_name'))
                });
            }
        }

        fillTags();

        carregarCursos();
    });

    $(document).on("click",".recent-list li a",function(){
        var cf = $(this).data('cf');
        var cfid = $(this).data('cfid');
        var li = $('.acc-menu .acc_content li[data-cfid="' + cfid + '"] > div').click();
        /*removeMeta(cf, cfid, li.data['post_name']);

        $(".recent-list li:contains("+li.find('div.container label').first().text()+")").remove();
        var test = li.children().find('label').text();
        var trim_test = $.trim(test);
        li.removeClass('active');

        var parentUl = li.closest('ul.child');
        if (parentUl.length) {
            var parentLi = parentUl.closest('li');

            if (parentUl.find('li.active').length !== parentUl.find('li').length) {
                parentLi.removeClass('active');
                removeMeta(parentLi.data('cf'), parentLi.data('cfid'), parentLi.data('post_name'));

                if (!parentUl.find('li.active').length) {
                    parentUl.slideUp('fast');
                    $.each(parentUl.find('li'), function () {
                        removeMeta($(this).data('cf'), $(this).data('cfid'), $(this).data('post_name'))
                    });
                }
            } else {
                parentLi.addClass('active');
                addMeta(parentLi.data('cf'), parentLi.data('cfid'), parentLi.data('post_name'));
            }
        } else {
            if (li.hasClass('active')) {
                li.find('.child').slideDown('fast');
                li.find('.child li').addClass('active');

                $.each(li.find('.child li.active'), function () {
                    addMeta($(this).data('cf'), $(this).data('cfid'), $(this).data('post_name'))
                });
            } else {
                li.find('.child').slideUp('fast');
                li.find('.child li').removeClass('active');
                $.each(li.find('.child li'), function () {
                    removeMeta($(this).data('cf'), $(this).data('cfid'), $(this).data('post_name'))
                });
            }
        }

        carregarCursos();

        if ($('.recent-list li').length == 0) {
            $('.recent-title').html('@lang('Utilitza el filtre per trobar els cursos del teu interès')')
        }*/
    });

    function fillTags() {
        $(".acc_content ul li").each(function( index ) {
            if($(this).hasClass('active')) {
                var names = $(this).children().find('label').first().text();
                if($(".recent-list li a[data-cfid=\"" + $(this).data('cfid') + "\"]").length == 0) {
                    $(".recent-list").append('<li>'+names+' <a data-cf="' + $(this).data('cf') + '" data-cfid="' + $(this).data('cfid') + '" href="javascript:void(0)" class="close-ico"><img src="{{ asset($_front.'images/close.png') }}" alt=""></a></li>');
                }

                if ($('.recent-list li').length > 0) {
                    $('.recent-title').html('@lang('La teva selecció actual:')')
                }
            } else {
                $(".recent-list li a[data-cfid='" + $(this).data('cfid') + "']").closest('li').remove();

                if ($('.recent-list li').length == 0) {
                    $('.recent-title').html('@lang('Utilitza el filtre per trobar els cursos del teu interès')')
                }
            }
        });
    }

/*======================================================================================
Filter Script End
======================================================================================*/
</script>
@endsection