<?php
$user = auth()->user();

if (!$user || ($user && !$user->isAdmin())) {
    abort(404);
}
?>

@extends('Front::layouts.base')


@section('content')
    <!--Inner Page Starts-->
    <section class="inner-page cursos">
        <div class="bread-cum">
            <div class="row">
                <ul>
                    <li><a href="{{ get_page_url('inici') }}">@lang('Inici')</a></li>
                    <li>@lang('Cursos')</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <h1 class="page-title"></h1>

            <!--Main Box Starts-->
            <section class="main-box">
                <!--Left Content Starts-->
                <div class="left-content">
                    <div class="filter-box">

                        @php ($cercaError = \App\Post::ofName('errors-cerca')->ofType('page')->first())
                        <div class="trobes">
                            <div><img src="{{ asset($_front.'images/not-found.png') }}" alt="" border="0"></div>
                            <h3>
                                @if ($cercaError && $cercaError->get_field('titol-caixa-lateral'))
                                    {{ $cercaError->get_field('titol-caixa-lateral') }}
                                @else
                                    @lang('No trobes el curs que estaves buscant?')
                                @endif
                            </h3>
                            @if ($cercaError && $cercaError->get_field('descripcio-caixa-lateral'))
                                {!! $cercaError->get_field('descripcio-caixa-lateral') !!}
                            @else
                                @lang('Contacta’ns i fes-nos saber què t’interessa')
                            @endif
                            <a href="{{ get_page_url('contacte') }}" class="link">@lang('Contactar')</a>
                        </div>
                    </div>
                </div>
                <!--Left Content Ends-->

                <!--Right Content Starts-->
                <div class="right-content">
                    <ul id="curs_found">
                        @php ($cercaError = \App\Post::ofName('errors-cerca')->ofType('page')->first())
                        <li>
                            <div class="no-result">
                                <div class="simg"><img src="{{ asset($_front.'images/warning.svg') }}" alt="" border="0"></div>
                                <h3>
                                    @if ($cercaError && $cercaError->get_field('titol-caixa-principal'))
                                        {{ $cercaError->get_field('titol-caixa-principal') }}
                                    @else
                                        @lang('No s\'ha trobat cap resultat')
                                    @endif
                                </h3>
                                @if ($cercaError && $cercaError->get_field('descripcio-caixa-principal'))
                                    {!! $cercaError->get_field('descripcio-caixa-principal') !!}
                                @else
                                    @lang('És possible que, ara mateix, no existeixi cap curs amb les característiques del teu interès. Periòdicament a FORMABAGES es publiquen nous cursos, o per a més informació, <a href=":url">contacta\'ns</a>.', ['url' => get_page_url('contacte')])
                                @endif
                                <div class="link-btn">
                                    <a href="javascript:void(0)" onclick="focusInput()">@lang('Nova cerca')</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--Right Content Ends-->
            </section>
            <!--Main Box Ends-->

        </div>
    </section>
    <!--Inner Page Ends-->
@endsection


