@extends('Front::layouts.base')

@section('content')
<!--Inner Page Starts-->
<section class="inner-page">
    <div class="bread-cum">
        <div class="row">
            <ul>
                <li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
                <li>{{ $item->title }}</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <h1 class="page-title">{{ $item->title }}</h1>
        <div class="contact-box">
            <div class="contact-right">
                <h2>{{ $item->get_field('titulo-sidebar') }}</h2>
                <span>{!! $item->get_field('subtitulo-sidebar') !!}</span>
                <ul>
                    <li class="loc">{{ $item->get_field('adress') }}</li>
                    <li><a href="mailto:{{ $item->get_field('email') }}">{{ $item->get_field('email') }}</a></li>
                    <li class="phone"><a href="tel:{{ ($item->get_field('telefono')) ? join("", explode(" ", $item->get_field('telefono'))) : '' }}">{{ $item->get_field('telefono') }}</a></li>
                    <li class="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11911.410653607785!2d1.8156284045695725!3d41.72369673494411!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7d44677e86ae43e4!2sConsell%20Comarcal%20del%20Bages!5e0!3m2!1ses!2ses!4v1571325518028!5m2!1ses!2ses" width="400" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </li>
                </ul>
            </div>
            <div class="contact-left">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h2>@lang('Per a qualsevol consulta:')</h2>
                <form id="formcontacte" action="{{ route("contactar") }}" class="form-box" method="POST" novalidate>
                    @csrf
                    <div class="frm-input">
                        <div class="frm-input-ctrl">
                            <input class="form-control" name="nombre" placeholder="@lang('Nom')*" type="text" required>
                        </div>
                    </div>
                    <div class="frm-input">
                        <div class="frm-input-ctrl">
                            <input class="form-control" name="email" placeholder="@lang('Email')*" type="text" required>
                        </div>
                    </div>
                    <div class="frm-input">
                        <div class="frm-input-ctrl">
                            <input class="form-control" name="poblacion" placeholder="@lang('Població')" type="text">
                        </div>
                    </div>
                    <div class="frm-input">
                        <div class="frm-input-ctrl">
                            <input class="form-control" name="organitzacio" placeholder="@lang('Organització')" type="text">
                        </div>
                    </div>
                    <div class="frm-input">
                        <div class="frm-input-ctrl">
                            <input class="form-control" name="telefono" placeholder="@lang('Telèfon')" type="text">
                        </div>
                    </div>
                    <div class="frm-input">
                        <div class="frm-input-ctrl">
                            <textarea class="txt-box" name="observaciones" placeholder="@lang('Observacions')"></textarea>
                        </div>
                    </div>
                    <div class="chck-bx">
                        @lang('*camps obligatoris')
                        <div class="frm-input checkbox">
                            <input id="check1" name="rgpd" type="checkbox" required>
                            <label for="check1"><a href="{{ get_page_url('politica-de-privacitat') }}" target="_blank">@lang('Acceptació de la LOPD')</a></label>
                        </div>
                        <div class="frm-input checkbox">
                            <input id="check2" name="promo" type="checkbox">
                            <label for="check2">@lang('Accepto rebre informació promocional')</label>
                        </div>
                    </div>
                    <div class="btn-ctr"><button type="submit" class="env-btn">@lang('Enviar')</button></div>
                </form>
            </div>
        </div>
    </div>
</section>
<!--Inner Page Ends-->
@endsection

@section('scripts')
@if (Session::has('status') && Session::get('status') == "success")
<script>
    $(function() {
        Swal.fire({
            title: '@lang("Gràcies! Ens posarem en contacte amb tu el més aviat possible")',
            type: 'success',
        })
    });
</script>
@endif
<script>
$("#formcontacte .env-btn").click(function(e) {
    var nombre = $("#formcontacte [name=\"nombre\"]");
    var email = $("#formcontacte [name=\"email\"]");
    var error = false;

    if (nombre.val() == "") {
        error = true;
        nombre.css('border', '1px solid red');
    } else nombre.css('border', '0px');

    if (email.val() == "") {
        error = true;
        email.css('border', '1px solid red');
    } else email.css('border', '0px');

    if (! $("#check1").get(0).checked ) {
        $("#check1").siblings('label').css('border', '1px solid red');
        error = true;
    }

    console.log(error);

    if ( error ) e.preventDefault();
});
</script>
@endsection