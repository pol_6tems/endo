@extends('Front::layouts.base')

@section('styles')
    {{--@if ($icons = $item->get_field('images-1'))
        @foreach($icons as $key => $icon)
            @php($hover = $icon['icono-hover']['value'])
            @if($img = $icon['icono']['value'])
            <style>
                .round-div span.emp{{ $key }} {
                    background: url('{{ $img->get_thumbnail_url() }}') no-repeat center center;
                }

                ul.empres-list1 li:hover .round-div span.emp{{ $key }} {
                    background: url('{{ $hover->get_thumbnail_url() }}') no-repeat center center;
                }
            </style>
            @endif
        @endforeach
    @endif--}}
    
    @if ($icons = $item->get_field('imagenes-2'))
        @foreach($icons as $key => $icon)
            @php($hover = $icon['icono-hover']['value'])
            @if($img = $icon['icono']['value'])
            <style>
                .round-div span.emp-2-{{ $key }} {
                    background: url('{{ $img->get_thumbnail_url() }}') no-repeat center center;
                }

                ul.empres-list2 li:hover .round-div span.emp-2-{{ $key }} {
                    background: url('{{ $hover->get_thumbnail_url() }}') no-repeat center center;
                }
            </style>
            @endif
        @endforeach
    @endif
@endsection

@section('content')
@php($tipus = $item->get_field("llista-tipus"))
<!--Cursos Box Starts-->
<section class="inner-page">
    <div class="bread-cum">
        <div class="row">
            <ul>
                <li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
                <li>{{ $item->title }}</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <h1 class="page-title">{{ $item->title }}</h1>
    </div>
    <div class="qui-som-img">
        @if ($img = $item->translate()->media)
            <img src="{{ $item->translate()->media->get_thumbnail_url() }}" alt="" border="0" class="qui-img-desk">
            <img src="{{ $item->translate()->media->get_thumbnail_url("medium") }}" alt="" border="0" class="qui-img-mobile">
        @endif
        <div class="qui-ban-cont">
            <div class="row">
                <h1 class="types-title">{!! $item->get_field('titulo-cabecera') !!}</h1>
                @if ($item->get_field('subtitulo-cabecera'))
                    <span class="subtitle">{{ $item->get_field('subtitulo-cabecera') }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="qui-som-outer">
        <div class="row">
            <div class="qui-som-cont formacio-types">
                <div class="intro">
                    @if ($item->get_field('titulo-contenido'))
                        <h2>{{ $item->get_field('titulo-contenido') }}</h2>
                    @endif

                    @if ($item->description)
                        <p>
                            {!! strip_tags($item->description, '<br><p><b>') !!}
                        </p>
                    @endif
                </div>

                @foreach($tipus as $tipo)
                    @if (isset($tipo['relacio-tipus']['value']) && isset($tipo['titol-tipus']['value']))
                        @php($typeItem = $tipo['relacio-tipus']['value']->first())
                        @php($numCursos = 0)
                        @php($typePosts = [])
                        @foreach($tipo['relacio-tipus']['value'] as $typeItemToCount)
                            @php($numCursos += countCourses("tipus-formacio", $typeItemToCount))
                            @php($typePosts[] = $typeItemToCount->post_name)
                        @endforeach
                        <div class="tipus">
                            <div class="type-icon">
                                @if ($typeItem && $media = $typeItem->media())
                                    <img src="{{ $media->get_thumbnail_url() }}" alt="">
                                @else
                                    <img src="{{ asset($_front."images/red-icon-01.svg") }}" alt="">
                                @endif
                            </div><div class="type-txt">
                                <h3>{{ $tipo['titol-tipus']['value'] }}</h3>
                                <span>
                                    @if($tipo['titol-tipus']['value'] == 'Estudis Universitaris')
                                        @lang(":num estudis", ["num" => $numCursos])
                                    @else
                                        @lang(":num cursos", ["num" => $numCursos])
                                    @endif
                                </span>

                                <div class="content">
                                    @if (isset($tipo['descripcio-tipus']['value']))
                                        {!! strip_tags($tipo['descripcio-tipus']['value'], '<br><p><b><a>') !!}
                                    @endif
                                </div>

                                <div class="type-link">
                                    <a href="{{ get_archive_link('curso') . '?tipus-formacio=' . implode(',', $typePosts) }}" class="pink-btn">@lang('Veure cursos')</a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
                {{-- <h1>{{ $item->get_field('titulo-contenido') }}</h1>
                <p>{!! strip_tags($item->description) !!}</p>

                @if($icons = $item->get_field('images-1'))
                <ul class="empres-list1">
                    @foreach($icons as $key => $icon)
                    <li>
                        <div class="round-div">
                            <div class="emp-list-cont">
                                <span class="emp{{ $key }}"></span>
                                <h2>{{ number_format(intval($icon['valor']['value']), 0, ',', '.') }}</h2>
                            </div>
                        </div>
                        <h3>{!! $icon['titulo']['value'] !!}</h3>
                    </li>
                    @endforeach
                </ul>
                @endif

                <div class="empres-lft">
                    <p>{{ $item->get_field('texto-izquierda') }}</p>
                </div>
                <div class="empres-rgt">
                    <p>{{ $item->get_field('texto-derecha') }}</p>
                </div>
                --}}

                @if($imagenes = $item->get_field('imagenes-2'))
                <ul class="empres-list2">
                    @foreach($imagenes as $key => $imagen)
                    <li>
                        <div class="round-div">
                            <div class="emp-list-cont">
                                <span class="emp-2-{{ $key }}"></span>
                                <h2>{{ number_format(intval($imagen['valor']['value']), 0, ',', '.') }}</h2>
                            </div>
                        </div>
                        <h3>{!! $imagen['titulo']['value'] !!}</h3>
                    </li>
                    @endforeach
                </ul>
                @endif
                {{--
                <div class="emp-link-list emp-link-list-mb">
                    <h1>@lang('Consulti les diferentes opcions de formació per a la seva empresa')</h1>
                    <ul>
                        <li>
                            <h2>Formació professional</h2>
                            <a href="javascript:void(0);">@lang('Veure més')<span></span></a>
                        </li>
                        <li>
                            <h2>Formació professional</h2>
                            <a href="javascript:void(0);">@lang('Veure més')<span></span></a>
                        </li>
                        <li>
                            <h2>Formació professional</h2>
                            <a href="javascript:void(0);">@lang('Veure més')<span></span></a>
                        </li>
                        <li>
                            <h2>Formació professional</h2>
                            <a href="javascript:void(0);">@lang('Veure més')<span></span></a>
                        </li>
                    </ul>

                    <div class="mobile-link-emp">
                        <!--<a href="javascript:void(0);" class="veure-tot">Veure 'ls tots </a>-->
                        <div class="owl-carousel" id="emp-links">
                            <div class="item">
                                <ul>
                                    <li>
                                        <h2>Formació professional</h2>
                                        <a href="javascript:void(0);">@lang('Veure més')<span></span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <ul>
                                    <li>
                                        <h2>Formació professional</h2>
                                        <a href="javascript:void(0);">@lang('Veure més')<span></span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <ul>
                                    <li>
                                        <h2>Formació professional</h2>
                                        <a href="javascript:void(0);">@lang('Veure més')<span></span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="item">
                                <ul>
                                    <li>
                                        <h2>Formació professional</h2>
                                        <a href="javascript:void(0);">@lang('Veure més')<span></span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                --}}
            </div>
        </div>
    </div>
</section>
<!--Cursos Box Ends-->
@endsection

@section('scripts')
<script>
$(document).ready(function() {
	var win_hgt;
	$(window).resize(function(){ 
		win_hgt = $(window).height();  
		$(".move #page").css({"height":win_hgt,"overflow":"hidden"}); 
	});
	
    var owl = $('#emp-links');
    owl.owlCarousel({
        goToFirstSpeed :1000,
        loop:true,
        items:1,
        margin:0,
        autoplay:true,
        autoplayTimeout:5500,
        autoplayHoverPause:true,
        nav : false,
        dots : true,
        responsive: {
            0:{ items:1, margin:0,},
            480:{ items:1},
            640:{ items:1},
            768:{ items:1},
            1024:{ items:1}
        },
    });
		
    win_hgt = $(window).height();		 
    $("#hie_menu").click( function(){
        $(".hole_div").addClass("move");
        //$(this).hide();
        $("#new").show();
        $("#page").css({"height":win_hgt,"overflow":"hidden"});
    });
                
    $("#new").click(function(){
        $(".hole_div").removeClass("move");
        //$("#hie_menu").show();
        $(this).hide();
        $("#page").css({"height":"auto","overflow":"auto"});
    });
    
    $("#close-men").click(function(){
        $(".hole_div").removeClass("move");
        $("#hie_menu").show();
        $("#new").hide();
        $("#page").css({"height":"auto","overflow":"auto"});
    });
});

//Mobile Multi Level Menu		
$(".u-menu").vmenuModule({
    Speed: 200,
    autostart: true,
    autohide: true
});
$(".u-menu ul li ul").hide();

$(document).on('ready', function() {
	$("#search-icon").click(function() {
        $(this).toggleClass('open');
        $('.inner-search').toggleClass('open-search');
    });
});

</script>
@endsection