<?php
$user = auth()->user();

if (!$user || ($user && !$user->isAdmin())) {
    abort(404);
}
?>

@extends('Front::layouts.base')

@section('content')
@endsection