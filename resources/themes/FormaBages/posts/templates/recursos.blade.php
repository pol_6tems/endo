@extends('Front::layouts.base')

@section('content')
<!--Cursos Box Starts-->
<section class="inner-page">
    <div class="bread-cum">
        <div class="row">
            <ul>
                <li><a href="{{ get_page_url('inici') }}">@lang('Inici')</a></li>
                <li>{{ $item->title }}</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <h1 class="page-title">{{ $item->title }}</h1>
        <p class="page-desc page-desc-des">{!! nl2br($item->get_field('subtitol-dsk')) !!}</p>
        <p class="page-desc page-desc-mob">{!! nl2br($item->get_field('subtitol-mbl')) !!}</p>
        <div class="recurs-cont">
            <div class="recurs-tab">
                <div id="parentHorizontalTab">
                    <div class="row">
                        <ul class="resp-tabs-list hor_1">
                            @php($categorias = get_posts('categoria-recursos')->sortBy('order'))
                            @foreach($categorias as $categoria)
                            <li>
                                <div class="tab-menu">
                                    @if ($media = $categoria->media())
                                        <span class="centre" style="background: url('{{ $media->get_thumbnail_url() }}') no-repeat center center;"></span>
                                        @if ($hover = $categoria->get_field('hover'))
                                            <span class="centre centre-hover" style="background: url('{{ $hover->get_thumbnail_url() }}') no-repeat center center;"></span>
                                        @endif
                                    @endif
                                    <p>{{ $categoria->title }}</p>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="resp-tabs-container hor_1">
                        @foreach($categorias as $categoria)
                        <div>
                            @php($recursos = get_posts([
                                'post_type' => 'recurs',
                                'metas' => [ ['categoria', $categoria->id] ],
                            ])->sortBy('title', SORT_NATURAL|SORT_FLAG_CASE))
                            <div class="tab-recurs">
                                <div class="filter">
                                    <input id="input-{{ $categoria->id }}" type="text" class="filter-search" placeholder="Filtrar" />
                                </div>
                                <h2>{{ $categoria->get_field('subtitol') }}</h2>
                                <ul id="cat-{{ $categoria->id }}" class="recurs-det">
                                    @foreach($recursos as $recurs)
                                    @php($web = $recurs->get_field('web'))
                                    @php($web = (strpos($web, 'http://') !== false || strpos($web, 'https://') !== false) ? $web : 'http://'.$web)
                                    @php($email = $recurs->get_field('email'))

                                    @if ($web == "")
                                        @php($aux = explode("@", $email))
                                        @php($web = (strpos($aux[1], 'http://') !== false || strpos($aux[1], 'https://') !== false) ? $aux[1] : 'http://'.$aux[1])
                                    @endif

                                    <li>
                                        <h3>{{ $recurs->title }}</h3>
                                        <ul class="radio-btn">
                                            <p class="loc">
                                                <a target="_blank" href="https://maps.google.com/?q={{ $recurs->get_field('direccion') }}">{{ $recurs->get_field('direccion') }}</a>
                                            </p>
                                            <p class="phn"><a href="tel:{{ $recurs->get_field('telefono') }}">{{ $recurs->get_field('telefono') }}</a></p>
                                            <p class="mail"><a href="mailto:{{ $recurs->get_field('email') }}">{{ $recurs->get_field('email') }}</a></p>
                                            <p class="web"><a href="{{ $web }}" target="_blank">@lang('Web')</a></p>
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="btn-div"> <a href="javascript:void(0);" class="veure-mes">@lang('Veure més')</a></div> --}}
    </div>
</section>
<!--Cursos Box Ends-->
@endsection

@section('scripts')
<script>
@foreach($categorias as $categoria)
    @php($recursos = get_posts([
        'post_type' => 'recurs',
        'metas' => [ ['categoria', $categoria->id] ],
    ])->sortBy(function($item) { return $item->title; }))
    var input_{{ $categoria->id }} = document.getElementById("input-{{ $categoria->id }}");
    input_{{ $categoria->id }}.addEventListener("keyup", function(event) {
        // Declare variables
        var filter, ul, li, a, i, txtValue;
        filter = input_{{ $categoria->id }}.value.toUpperCase();
        ul = document.getElementById("cat-{{ $categoria->id }}");
        li = ul.getElementsByTagName('li');

        // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("h3")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    });
@endforeach
</script>
<script type="text/javascript">
    $(document).on('ready', function() {
   $("#search-icon").click(function() {
        $(this).toggleClass('open');
        $('.inner-search').toggleClass('open-search');
    });
   jsUpdateTab();
}); 
</script>
<script type="text/javascript">
$(document).ready(function(){
//Horizontal Tab
   $('#parentHorizontalTab').easyResponsiveTabs({
       type: 'default', //Types: default, vertical, accordion
       width: 'auto', //auto or any width like 600px
       fit: true, // 100% fit in a container
       tabidentify: 'hor_1', // The tab groups identifier
       activate: function(event) { // Callback function if tab is switched
           var $tab = $(this);
           var $info = $('#nested-tabInfo');
           var $name = $('span', $info);
           $name.text($tab.text());
           $info.show();
       }
   });			
});
</script>
<script>
    $(document).ready(function() {
        var win_hgt;

        $(window).resize(function() {
            win_hgt = $(window).height();  
            $(".move #page").css({"height":win_hgt,"overflow":"hidden"}); 
        });
    
        $(document).ready(function() {
            win_hgt = $(window).height();		 
            $("#hie_menu").click( function(){
                $(".hole_div").addClass("move");
                //$(this).hide();
                $("#new").show();
                $("#page").css({"height":win_hgt,"overflow":"hidden"});
            });
                        
            $("#new").click(function(){
                $(".hole_div").removeClass("move");
                //$("#hie_menu").show();
                $(this).hide();
                $("#page").css({"height":"auto","overflow":"auto"});
            });
            
            $("#close-men").click(function(){
                $(".hole_div").removeClass("move");
                $("#hie_menu").show();
                $("#new").hide();
                $("#page").css({"height":"auto","overflow":"auto"});
            });
        });	
    });

    //Mobile Multi Level Menu		
    $(".u-menu").vmenuModule({
        Speed: 200,
        autostart: true,
        autohide: true
    });

    $(".u-menu ul li ul").hide();

    $(window).resize(function() {
        jsUpdateTab();
    });

    function jsUpdateTab()
    {
        var width = $(window).width();
        var height = $(window).height();	
        if(width<=767)
        {
            $('.resp-accordion').removeClass('resp-tab-active');  
            $('.resp-tab-content').attr('style', 'display:none');	
        }
    }
</script>
@endsection