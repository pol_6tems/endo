@extends('Front::layouts.base')

@section('content')
<!--Cursos Box Starts-->
<section class="inner-page">
    <div class="bread-cum">
        <div class="row">
            <ul>
                <li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
                <li>{{ $item->title }}</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <h1 class="page-title">{{ $item->title }}</h1>
        <p class="page-desc">{!! strip_tags($item->description, '<br>') !!}</p>
    </div>

    @php ($webPages = $item->get_field('pagines'))
    @if ($webPages && count($webPages))
        <div class="qui-som-outer">
            <div class="row">
                <div class="url-cont">
                    <h1>@lang('Pàgines web')</h1>
                    <ul class="url-list">
                        @foreach($webPages as $webPage)
                            <li>
                                <div>
                                    <h3>{{ $webPage['titol']['value'] }}</h3>
                                    <a href="{{ $webPage['url']['value'] }}" target="_blank" title="{{ $webPage['titol']['value'] }}">
                                       @lang('Web')
                                    </a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif

    @php ($catLvl = $item->get_field('catala'))
    @php ($comLvl = $item->get_field('comarcal'))
    @if (($catLvl && count($catLvl)) || ($comLvl && count($comLvl)))
        <div class="qui-som-outer">
            <div class="row">
                <div class="document-cont">
                    <h1>@lang('Estudis, informes i publicacions')</h1>

                    @if ($catLvl && count($catLvl))
                        <h3>@lang('A nivell català')</h3>
                        <ul class="document-list">
                            @foreach($catLvl as $catLvlItem)
                                @if ($catLvlItem['document']['value'])
                                    <li>
                                        <h4>
                                            <a href="{{ $catLvlItem['document']['value']->get_thumbnail_url() }}" download>{{ $catLvlItem['titol']['value'] }}</a>
                                        </h4>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    @endif

                    @if ($comLvl && count($comLvl))
                        <h3>@lang('A nivell comarcal')</h3>
                        <ul class="document-list">
                            @foreach($comLvl as $comLvlItem)
                                @if ($comLvlItem['document']['value'])
                                    <li>
                                        <h4>
                                            <a href="{{ $comLvlItem['document']['value']->get_thumbnail_url() }}" download>{{ $comLvlItem['titol']['value'] }}</a>
                                        </h4>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    @endif
</section>
<!--Cursos Box Ends-->
@endsection