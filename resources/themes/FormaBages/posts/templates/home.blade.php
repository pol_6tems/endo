<!--Banner Starts-->
@extends('Front::layouts.base')

@section('header')
    @includeIf('Front::partials.header')
@endsection

@section('content')
@php
    $ndsk = 10;
    $nmbl = 4;
    
    $agenda = get_posts("agenda");
    $agenda_destacades = get_posts([
        "post_type" => "agenda",
        "metas" => [ ['destacado', 1] ]
    ]);
    $agenda_destacades = $agenda_destacades->merge($agenda);
    $agenda_destacades = $agenda_destacades->filter(function($value) {
        $data = $value->get_field("publicacio");
        if ($data) return Date::now()->timestamp <= Date::createFromFormat("d/m/Y", $data)->timestamp;
    });
    $agenda = sortByCF($agenda_destacades, "publicacio", 'desc')->take(4);


    $noticies_destacades = get_posts([
        "post_type" => "noticia",
        "metas" => [ ['destacado', 1] ]
    ]);
    $noticies = get_posts("noticia");
    $noticies_destacades = $noticies_destacades->merge($noticies);
    $noticies = sortByCF($noticies_destacades, "data-publicacio", "desc")->take($agenda->count() ? 3 : 7)->values();
@endphp


<section class="banner-home">
    <div id="home-slider" class="owl-carousel">
        <div class="fotos">
            @if ($banners = $item->get_field("imatges"))
                @foreach($banners as $banner)
                    @if ($img = $banner["imatge"]["value"])
                        <div class="item"><img src="{{ $img->get_thumbnail_url() }}" alt="" class="desk-home" /></div>
                    @endif
                @endforeach
            @endif
        </div>
        <div id="shadow"></div>
    </div>
    
    @if ($mobil = $item->get_field('fons-mobil'))
        <img src="{{ $mobil->get_thumbnail_url() }}" alt="" class="mob-home">
    @endif

    <div class="banner-caption">
        <div class="row">
            <h1>{{ $item->get_field('titulo') }}<span>{{ $item->get_field('subtitulo') }}</span></h1>
            @includeIf('Front::partials.buscador')
        </div>
    </div>
</section>
<!--Banner Ends-->
<div class="row">
    <section class="caption-txt">
        {!! $item->description !!}
    </section>
</div>
<!--Tipus Box Starts-->
<section class="tipus-box">
<div class="row">
    <h2>@lang('Tipus de Formació')</h2>
    @php($tipus = get_posts('tipo-formacion')->sortBy('order'))
    @php($tipus = $tipus->each(function ($tipo) {
        $tipo->count_courses = countCourses("tipus-formacio", $tipo);
    })->filter(function ($tipo) {
        return $tipo->url != 'altres' && $tipo->count_courses;
    }))

    <div class="{{ ($tipus->count() > $ndsk) ? 'owl-carousel' : ''}} tipus-car" id="tipus">
        @foreach($tipus->chunk($ndsk) as $tipu)
            <div class="item">
                <ul>
                    @foreach($tipu as $key => $t)
                        @if($num = $t->count_courses)
                            @php($hover = $t->get_field('hover'))
                            <li><a href="{{ get_archive_link('curso') . '?tipus-formacio=' . $t->post_name }}">
                                <div class="icon1" style="{{ ($t->translate(true)->media) ? 'background: url(\'' . $t->translate(true)->media->get_thumbnail_url() . '\') no-repeat;' : '' }}"></div>
                                @if ($hover)
                                    <div class="icon1 hover" style="{{ ($hover->get_thumbnail_url()) ? 'background: url(\'' . $hover->get_thumbnail_url() . '\') no-repeat;' : '' }}"></div>
                                @endif
                                <div class="caption">{{ $t->title }}</div>
                                <div class="curs">@lang(':num cursos', ['num' => $num])</div>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        @endforeach
    </div>
    <div class="{{ ($tipus->count() > $nmbl) ? 'owl-carousel' : ''}} tipus-car" id="tipus-mob">
        {{--@php($tipus = get_posts('tipo-formacion'))--}}
        @foreach($tipus->chunk($nmbl) as $tipu)
            <div class="item">
                <ul>
                    @foreach($tipu as $key => $t)
                        @if($num = countCourses("tipus-formacio", $t))
                            @php($hover = $t->get_field('hover'))
                            <li><a href="{{ $t->get_url() }}">
                                <div class="icon1" style="{{ ($t->translate(true)->media) ? 'background: url(\'' . $t->translate(true)->media->get_thumbnail_url() . '\') no-repeat;' : '' }}"></div>
                                @if ($hover)
                                    <div class="icon1 hover" style="{{ ($hover->media) ? 'background: url(\'' . $hover->media->get_thumbnail_url() . '\') no-repeat;' : '' }}"></div>
                                @endif
                                <div class="caption">{{ $t->title }}</div>
                                <div class="curs">@lang(':num cursos', ['num' => $num])</div>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        @endforeach
    </div>
</div>
</section>

<section class="tipus-box">
    <div class="row">
        <h2>@lang('Vídeo explicatiu')</h2>
        
        <div class="img-video-container">
            <a class="video-fancybox" data-width="640" data-height="360" href="{{ asset($_front.'video/Video_FORMABAGES.mp4') }}">
                <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" class="lazy-img" data-src="{{ asset($_front.'images/video-02.jpg') }}" alt="Vídeo explicatiu">
            </a>
        </div>
    </div>
</section>
<!--Tipus Box Ends-->
<section class="caption-txt caption-txt-mbl">
    {!! $item->description !!}
</section>
<!--Arees Box Starts-->
<section class="arees-box">
@php($families_formatives = cacheable(\App\Modules\FormaBages\Repositories\FiltersRepository::class, 10)->getFamiliesFormatives()->sortBy(function($item) { return $item->post_name; })->values())
<div class="row"> <a href="{{ get_archive_link('curso') }}" class="lnk lnk-mob">@lang('Veure \'ls tots')</a>
    {{-- <h2>@lang('Àrees de formació')</h2> --}}
    <h2>@lang('Famílies Professionals')</h2>
    <div class="arees-list">
        <div class="list">
            <ul>
                @foreach($families_formatives as $key => $area)
                    <li class="{{ ($key == 0) ? 'appear' : '' }}">
                        <a href="{{ get_archive_link('curso') . '?' . $area->type . '=' . $area->post_name }}">
                            <div class="arees-listpad">
                                <h2>{{ $area->title }}</h2>
                            </div>
                            <div class="player-info">
                                @if($img = $area->translate(true)->media)
                                    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" class="lazy-img" data-src="{{ $img->get_thumbnail_url() }}"{{-- loading="lazy"--}} alt="">
                                @endif
                                <div class="equip-txt">
                                    <h1>{{ $area->title }}</h1>
                                    {!! get_excerpt( $area->description, 40) !!}
                                </div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="btn-div1"> <a href="javascript:void(0);" class="pink-btn">@lang('Veure mes')</a> </div>
    </div>
</div>
</section>
<!--Arees Box Ends-->
<!--Cursos Box Starts-->
{{--@php($cursos = get_posts([
    'post_type' => 'curso',
    'numberposts' => 4,
    'metas' => [ ['destacat', 1] ]
]))
@if (count($cursos) > 0)
<section class="cursos-box">
    <div class="row">
        <a href="{{ get_archive_link('curso') }}" class="lnk lnk-des">@lang('Veure cursos destacats')</a>
        <a href="{{ get_archive_link('curso') }}" class="lnk lnk-mob">@lang('Veure\'ls tots')</a>
        <h2>@lang('Cursos destacats')</h2>
        <ul>
            @foreach($cursos as $curso)
            @php($tipo = $curso->get_field('tipus-formacio'))
            @php($area = $curso->get_field('area-formativa'))
            <li>
                <div class="title">
                    <a href="{{ $curso->get_url() }}">
                        <h1>{{ $curso->title }}</h1>
                    </a>
                    <span>
                        @if ($tipo && is_object($tipo))
                            <a href="{{ get_archive_link('curso') . '?tipus-formacio=' . $tipo->post_name }}"><strong>{{ ucfirst(strtolower($tipo->title)) }}</strong></a>
                        @endif
                        @if ($area)
                            <a href="{{ get_archive_link('curso') . '?area-formativa=' . $area->post_name }}">{{ ucfirst(strtolower($area->title)) }}</a>
                        @endif
                    </span>
                </div>
                <div class="values">
                    @php ($loc = $curso->get_field('localitzacio'))
                    <div class="info-lft">
                        @if ($loc != '')
                            <a href="javascript:void(0);"><span class="loc">{{ $loc }}</span></a>
                        @endif
                        <br>

                        @if ($inici = $curso->get_field('data-inici-del-curs'))
                            <span class="cdate">@lang('Inici:') {{ getDayAndMonthCat($inici) }}</span>
                        @endif

                        @if ($final = $curso->get_field('data-final-del-curs'))
                            <span>@lang('Fi:') {{ getDayAndMonthCat($final) }}</span>
                        @endif
                    </div>
                    <div class="btn-rgt"><a href="{{ $curso->get_url() }}" class="pink-btn">@lang('Veure curs')</a></div>
                </div>
            </li>
            @endforeach
        </ul>
        <div class="mobile-link-cursos">
            @if ($cursos->count() > 1)
            <div class="owl-carousel" id="cursos-links">
                @foreach($cursos as $curso)
                <div class="item">
                    <ul>
                        <li>
                            <div class="title">
                                <h1>{{ $curso->title }}</h1>
                                <span><a href="{{ $curso->get_url() }}">
                                    <strong>{{ $curso->get_field('tipus-formacio') }}</strong></a>
                                    <a href="javascript:void(0);">{{ $curso->get_field('area-formativa') }}</a>
                                </span>
                            </div>
                            <div class="values">
                                @php ($loc = $curso->get_field('localitzacio'))
                                @if ($loc != '')
                                    <span class="loc">{{ $loc }}</span> <br>
                                @endif

                                @if ($inici = getDayAndMonthCat($curso->get_field('data-inici-del-curs')))
                                    <span class="cdate">@lang('Inici: :fecha', ['fecha' => $inici])</span>
                                @endif

                                @if ($final = getDayAndMonthCat($curso->get_field('data-final-del-curs')))
                                    <span>@lang('Fi: :fecha', ['fecha' => $final])</span>
                                @endif

                                <div class="btn-rgt">
                                    <a href="{{ $curso->get_url() }}" class="pink-btn">@lang('Veure curs')</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                @endforeach
            </div>
            @else
                @if($curso = $cursos->first())
                <div class="item">
                    <ul>
                        <li>
                            <div class="title">
                                <h1>{{ $curso->title }}</h1>
                                <span><a href="{{ $curso->get_url() }}">
                                    <strong>Formació ocupacional</strong></a>
                                    <a href="javascript:void(0);">Indústries alimentàries</a>
                                </span>
                            </div>
                            <div class="values">
                                @php ($loc = $curso->get_field('localitzacio'))
                                @if ($loc != '')
                                    <span class="loc">{{ $loc }}</span> <br>
                                @endif
                                @if ($inici = getDayAndMonthCat($curso->get_field('data-inici-del-curs')))
                                    <span class="cdate">@lang('Inici: :fecha', ['fecha' => $inici])</span>
                                @endif

                                @if ($final = getDayAndMonthCat($curso->get_field('data-final-del-curs')))
                                    <span>@lang('Fi: :fecha', ['fecha' => $final])</span>
                                @endif

                                <div class="btn-rgt">
                                    <a href="{{ $curso->get_url() }}" class="pink-btn">@lang('Veure curs')</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                @endif
            @endif
        </div>
    </div>
</section>
<!--Cursos Box Ends-->
@endif--}}
<!--Agno Box Starts-->
<section class="agno-box">
<div class="row">
    @if ($agenda->count())
        <div class="hagenda">
            <a href="{{ get_archive_link('agenda') }}" class="lnk">@lang('Veure agenda')</a>
            <h2>@lang('Agenda')</h2>
            <ul>
                @foreach($agenda as $item_agenda)
                <li><a href="{{ $item_agenda->get_url() }}">
                        @if (validateValue($item_agenda->get_field('publicacio'), "date_format:d/m/Y"))
                            @php($publicacio = Date::createFromFormat("d/m/Y", $item_agenda->get_field('publicacio')))
                            <div class="a-lft">{{ $publicacio->format('d') }} <span>{{ strtoupper($publicacio->format('M')) }}</span></div>
                        @endif

                        <div class="a-rgt">
                            <div class="ag-title">{{ $item_agenda->title }}</div>
                            <div class="veure">@lang('Veure esdeveniment')</div>
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="hnotices @if (!$agenda->count())noticies-no-agenda @endif"> <a href="{{ get_archive_link('noticia') }}" class="lnk">@lang('Veure notícies')</a>
        <h2>@lang('Notícies')</h2>
        <ul>
            @foreach($noticies as $key => $noticia)
                @if($date = $noticia->get_field('data-publicacio'))
                    @php($date = Date::createFromFormat("d/m/Y", $date))
                    <li class="{{ (($key + 1)  % 2 == 0) ? 'brown' : '' }}">
                        <a href="{{ $noticia->get_url() }}">
                            @if ($categoria = $noticia->get_field('categoria'))
                                <p>{{ $categoria->title }}</p>
                            @endif
                            <div class="ntitle">{{ $noticia->title }}</div>
                            <div>
                                <p>{{ $date->format('d/m/Y') }}</p>
                                <span class="n-arrow">@lang('Llegir notícia')</span>
                            </div>
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
</section>
<!--Agno Box Ends-->
<!--Coneix Box Starts-->
<section class="coneix-box">
<div class="row">
    <h2>{!! $item->get_field('titol-banner') !!}</h2>
    <div class="video-right">
        <div class="video-img">
            @if ($video = $item->get_field('video'))
                @if (is_string($video) && substr($video, -(strlen('.mp4')) === '.mp4'))
                    <img src="{{ asset($_front.'images/video.jpg') }}" alt="">
                    <div class="lazy-vid" data-src="{{ $video }}" data-type="video/mp4" data-text="@lang('Your browser doesn\'t support HTML5 video.')">

                    </div>
                    {{--<video id="video" width="100%" loop controls>
                        <source src="{{ $video }}" type="video/mp4">
                        <p>@lang('Your browser doesn\'t support HTML5 video.')</p>
                    </video>--}}
                @elseif (is_string($video))
                    @php($url = str_replace("watch?v=", "embed/", $video))
                    <iframe src="{{ $url }}" width="560" height="334" frameborder="0"></iframe>
                @else
                    <img src="{{ asset($_front.'images/video.jpg') }}" alt="">
                    <div class="lazy-vid" data-src="{{ $video->get_thumbnail_url() }}" data-type="video/mp4" data-text="@lang('Your browser doesn\'t support HTML5 video.')">

                    </div>
                    {{--<video id="video" width="100%" loop controls>
                        <source src="{{ $video->get_thumbnail_url() }}" type="video/mp4">
                        <p>@lang('Your browser doesn\'t support HTML5 video.')</p>
                    </video>--}}
                @endif
            @endif
        </div>
    </div>
    <div class="video-left">
        <h3>{{ $item->get_field('subtitol-banner') }}</h3>
        <p>{{ $item->get_field('description-banner') }}</p>
        <a href="{{ ($item->get_field('link-url') != "") ? $item->get_field('link-url') : 'javascript:void(0)' }}" class="pink-btn">{{ $item->get_field('link-title') }}</a>
    </div>
</div>
</section>
<!--Coneix Box Ends-->
@endsection

@section('scripts')
<script type="text/javascript">
    @if ($tipus->count() > $ndsk)
    var owl = $('#tipus');
    owl.owlCarousel({
        goToFirstSpeed: 2000,
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 7000,
        smartSpeed: 2000,
        autoplayHoverPause: true,
        nav: true,
        dots: true
    });
    @endif
    @if ($tipus->count() > $nmbl)
    var owl = $('#tipus-mob');
    owl.owlCarousel({
        goToFirstSpeed: 2000,
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 7000,
        smartSpeed: 2000,
        autoplayHoverPause: true,
        nav: true,
        dots: true
    });
    @endif
    var owl = $('#cursos-links');
    owl.owlCarousel({
        goToFirstSpeed: 1000,
        loop: true,
        items: 1,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 7000,
        smartSpeed: 2000,
        autoplayHoverPause: true,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1,
                margin: 0,
            },
            480: {
                items: 1
            },
            640: {
                items: 1
            },
            768: {
                items: 1
            },
            1024: {
                items: 1
            }
        },
    });

    var owl = $('#home-slider');
    owl.owlCarousel({
        goToFirstSpeed: 1000,
        loop: true,
        items: 1,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 7000,
        nestedItemSelector: 'item',
        smartSpeed: 2000,
        autoplayHoverPause: true,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1,
                margin: 0,
            },
            480: {
                items: 1
            },
            640: {
                items: 1
            },
            768: {
                items: 1
            },
            1024: {
                items: 1
            }
        },
    });

    $('.fancybox-media')
        .attr('rel', 'media-gallery')
        .fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            prevEffect: 'none',
            nextEffect: 'none',
            arrows: false,
            helpers: {
                media: {},
                buttons: {}
            }
        });

    $('.video-fancybox').on('click', function (e) {
        e.preventDefault();

        $.fancybox({
            hideOnContentClick: false,
            autoScale: false,
            transitionIn: 'none',
            transitionOut: 'none',
            title: this.title, // optional
            width: '80%', // or your size
            height: '60%',
            href: this.href,
            type: 'iframe',
            fitToView   : false,
            autoSize    : false
        });
    });
</script>
<script>
    $(document).ready(function () {
        var win_hgt;
        $(window).resize(function () {
            win_hgt = $(window).height();
            $(".move #page").css({
                "height": win_hgt,
                "overflow": "hidden"
            });
        });

        $(document).ready(function () {

            win_hgt = $(window).height();
            $("#hie_menu").click(function () {
                $(".hole_div").addClass("move");
                //$(this).hide();
                $("#new").show();
                $("#page").css({
                    "height": win_hgt,
                    "overflow": "hidden"
                });
            });

            $("#new").click(function () {
                $(".hole_div").removeClass("move");
                //$("#hie_menu").show();
                $(this).hide();
                $("#page").css({
                    "height": "auto",
                    "overflow": "auto"
                });
            });

            $("#close-men").click(function () {
                $(".hole_div").removeClass("move");
                $("#hie_menu").show();
                $("#new").hide();
                $("#page").css({
                    "height": "auto",
                    "overflow": "auto"
                });
            });
        });
    });

    //Mobile Multi Level Menu		
    $(".u-menu").vmenuModule({
        Speed: 200,
        autostart: true,
        autohide: true
    });
    $(".u-menu ul li ul").hide();
</script>
@endsection