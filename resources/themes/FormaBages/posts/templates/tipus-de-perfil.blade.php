@extends('Front::layouts.base')

@section('content')
<!--Cursos Box Starts-->
<section class="inner-page">
     <div class="bread-cum">
          <div class="row">
               <ul>
                    <li><a href="{{ get_page_url('inici') }}">@lang('Inici')</a></li>
                    <li>{{ $item->title }}</li>
               </ul>
          </div>
     </div>
     <div class="row">
          <h1 class="page-title">{{ $item->title }}</h1>
          <p class="page-desc">{!!$item->get_field('subtitulo') !!}</p>
     </div>

     <div class="perfils-outer">
          <div class="row">
               <div class="perfil-cont">
                    @php($blocs = array('estudiant', 'atur', 'empresa', 'formar'))
                    @foreach($blocs as $idx => $bloc)
                    <!-- $bloc -->
                    <div class="perfil-out">
                         <div class="perfil-rgt {{ (($idx + 1) % 2 == 0) ? 'perfil-rgt1' : '' }}">
                              <div class="img-div">
                                   @if($img = $item->get_field('imatge-'.$bloc))
                                        <img src="{{ $img->get_thumbnail_url() }}" alt="" border="0">
                                   @endif
                              </div>
                         </div>
                         <div class="perfil-lft {{ (($idx + 1) % 2 == 0) ? 'perfil-lft1' : '' }}">
                              <h2>{{ $item->get_field('titol-'.$bloc) }}</h2>
                              <p>{{ $item->get_field('descripcio-'.$bloc) }}</p>

                              @if($links = $item->get_field('links-'.$bloc))
                              <ul>
                              @foreach($links as $link)
                                   @if ($link['text']['value'] != "")
                                        <li><a href="{{ ($link['link']['value'] != "") ? $link['link']['value'] : 'javascript:void(0)' }}">{{ $link['text']['value'] }} <span></span></a></li>
                                   @endif
                              @endforeach
                              </ul>
                              @endif
                         </div>
                    </div>
                    @endforeach
               </div>
          </div>
     </div>
</section>
<!--Cursos Box Ends-->
@endsection