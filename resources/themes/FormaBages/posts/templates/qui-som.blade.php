@extends('Front::layouts.base')

@section('content')
<!--Cursos Box Starts-->
<section class="inner-page">
	<div class="bread-cum">
		<div class="row">
			<ul>
				<li><a href="{{ get_page_url('home') }}">Inici</a></li>
				<li>{{ $item->title }}</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<h1 class="page-title">{{ $item->title }}</h1>
		<p class="page-desc">{!!$item->get_field('titol-header') !!}</p>
	</div>
	{{--<div class="qui-som-img">
		@if($img = $item->translate()->media)
		<img src="{{ $img->get_thumbnail_url() }}" alt="" border="0" class="qui-img-desk">
		<img src="{{ $img->get_thumbnail_url('medium') }}" alt="" border="0" class="qui-img-mobile">
		@endif
		<div class="qui-ban-cont">
			<div class="row">
				<h1>{!! $item->get_field('titol-header') !!}</h1>
			</div>
		</div>
	</div>--}}
	<div class="qui-som-outer">
		<div class="row">
			<div class="qui-som-cont">
				<div class="qui-som-row">
					<div class="qui-som-col-txt">
						<h1>{!! $item->get_field('titol-content') !!}</h1>
						{!! $item->get_field('content') !!}
						{{--<p class="forma">@lang('FormaBages és una<br> iniciativa impulsada pel:')</p>
						<img src="{{ asset($_front.'images/logo-consell.png') }}" alt="" class="i-forma">--}}
					</div>{{--<div class="qui-som-col-img">
						<img src="{{ asset($_front . 'images/qui-som-imatge-2.jpg') }}" alt="@lang('Qui som')">
					</div>--}}
				</div>
				
				<div class="logo-cont">
					<h2>@lang('Entitats adherides')</h2>

					@if($entitats = $item->get_field('logos-generalitat'))
					<div class="logo-row">
						<h3>{{ $item->get_field('titol-generalitat') }}</h3><ul>
							@foreach($entitats as $entitat)
								@if ($img = $entitat['logos']['value'])
									@php($link = $entitat['link']['value'] ?? "javascript:void(0);")
									<li><a href="{{ $link }}" target="_blank"><div class="img-container"><span class="img-helper"></span><img src="{{ $img->get_thumbnail_url() }}" alt="" border="0"></div></a></li>
								@endif
							@endforeach
						</ul>
					</div>
					@endif

					@if($entitats = $item->get_field('logos-local'))
						<div class="logo-row">
							<h3>{{ $item->get_field('titol-local') }}</h3><ul>
								@foreach($entitats as $entitat)
									@if ($img = $entitat['logo']['value'])
										@php($link = $entitat['link']['value'] ?? "javascript:void(0);")
										<li><a href="{{ $link }}" target="_blank"><div class="img-container"><span class="img-helper"></span><img src="{{ $img->get_thumbnail_url() }}" alt="" border="0"></div></a></li>
									@endif
								@endforeach
							</ul>
						</div>
					@endif

					@if($entitats = $item->get_field('logos-centres-formacio'))
						<div class="logo-row">
							<h3>{{ $item->get_field('titol-centres-formacio') }}</h3><ul>
								@foreach($entitats as $entitat)
									@if ($img = $entitat['logo']['value'])
										@php($link = $entitat['link']['value'] ?? "javascript:void(0);")
										<li><a href="{{ $link }}" target="_blank"><div class="img-container"><span class="img-helper"></span><img src="{{ $img->get_thumbnail_url() }}" alt="" border="0"></div></a></li>
									@endif
								@endforeach
							</ul>
						</div>
					@endif

					@if($entitats = $item->get_field('logos-agrupacions-empresarials'))
						<div class="logo-row">
							<h3>{{ $item->get_field('titol-agrupacions-empresarials') }}</h3><ul>
								@foreach($entitats as $entitat)
									@if ($img = $entitat['logo']['value'])
										@php($link = $entitat['link']['value'] ?? "javascript:void(0);")
										<li><a href="{{ $link }}" target="_blank"><div class="img-container"><span class="img-helper"></span><img src="{{ $img->get_thumbnail_url() }}" alt="" border="0"></div></a></li>
									@endif
								@endforeach
							</ul>
						</div>
					@endif

					@if($entitats = $item->get_field('logos-institucions-comarcals'))
						<div class="logo-row">
							<h3>{{ $item->get_field('titol-institucions-comarcals') }}</h3><ul>
								@foreach($entitats as $entitat)
									@if ($img = $entitat['logo']['value'])
										@php($link = $entitat['link']['value'] ?? "javascript:void(0);")
										<li><a href="{{ $link }}" target="_blank"><div class="img-container"><span class="img-helper"></span><img src="{{ $img->get_thumbnail_url() }}" alt="" border="0"></div></a></li>
									@endif
								@endforeach
							</ul>
						</div>
					@endif
				</div>

				@if ($item->get_field('text-inferior'))
					<div class="qui-som-row">
						{!! $item->get_field('text-inferior') !!}
						<p class="forma">@lang('FormaBages és una<br> iniciativa impulsada pel:')</p>
						<img src="{{ asset($_front.'images/logo-consell.png') }}" alt="" class="i-forma">
					</div>
				@endif

				@if($entitats = $item->get_field('logos-suport-inferior'))
				<div class="support-logo">
					<h2>@lang('Amb el suport de')</h2>
					<ul>
						@foreach($entitats as $entitat)
							@php($link = $entitat['link']['value'] ?? "javascript:void(0);")
							@if ($img = $entitat['media']['value'])
							<li><a href="{{ $link }}" target="_blank"><img src="{{ $img->get_thumbnail_url() }}" alt="" border="0"></a></li>
							@endif
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>
	</div>
</section>
<!--Cursos Box Ends-->
@endsection