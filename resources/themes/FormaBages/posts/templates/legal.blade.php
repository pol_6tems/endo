@extends('Front::layouts.base')

@section('content')
<!--Cursos Box Starts-->
<section class="inner-page">
    <div class="bread-cum">
        <div class="row">
            <ul>
                <li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
                <li>{{ $item->title }}</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <h1 class="page-title">{{ $item->title }}</h1>
    </div>

    <div class="perfils-outer">
        <div class="row">
            <div class="perfil-cont">
                {!! strip_tags($item->description, '<br><p>') !!}
            </div>
        </div>
    </div>
</section>
@endsection