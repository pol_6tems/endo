<ul id="curs_found">
    <input id="ncursos" type="hidden" name="count" value="{{ $cursos->total() }}">
    @forelse($cursos as $curs)
        @php($tipo = $curs->get_field('tipus-formacio'))
        @php($familia = $curs->get_field('familia-formativa'))
        <li>
            <div class="title">
                <h1><a href="{{ $curs->get_url() }}">{{ $curs->title }}</a></h1>
                <span>
                    @if ($tipo && is_object($tipo))
                        <a href="{{ get_archive_link('curso') . '?tipus-formacio=' . $tipo->post_name }}"><strong>{{ $tipo->title }}</strong></a>
                    @endif
                    @if ($familia && is_object($familia))<a href="{{ get_archive_link('curso') . '?familia-formativa=' . $familia->post_name }}">{{ $familia->title }}</a>@endif
                </span>
            </div>
            <div class="values">
                @if ($curs->get_field('duracio'))
                    <span class="time">{{ $curs->get_field('duracio') }}</span>
                @endif
                @if ($centre = $curs->get_field('centre'))
                    @if ($poblacio = $centre->get_field('poblacion'))
                        <br><span class="loc">{{ centre_title($centre->title) }} ({{ $poblacio->title }})</span>
                    @endif
                @endif

                @if ($data_inici = $curs->get_field('data-inici-del-curs'))
                    <br><span class="cdate">@lang('Inici:') {{ print_course_date($curs, $data_inici, false) }}</span>
                @endif

                @if ($data_final = $curs->get_field('data-final-del-curs'))
                    <span>@lang('Fi:') {{ print_course_date($curs, $data_final, false) }}</span>
                @endif

                <div class="mlink"><a href="{{ $curs->get_url() }}" class="pink-btn">@lang('Veure curs')</a></div>
            </div>

            @php($inici = $curs->get_field('data-inici-del-curs'))
            @if ($inici && Date::createFromFormat("d/m/Y", $inici) > Date::now())
                <div class="ribbon-container ribbon-top-right">
                    <span>@lang('Curs no iniciat')</span>
                </div>
            @endif
        </li>
    @empty
        @php ($cercaError = \App\Post::ofName('errors-cerca')->ofType('page')->first())
        <li>
            <div class="no-result">
                <div class="simg"><img src="{{ asset($_front.'images/warning.svg') }}" alt="" border="0"></div>
                <h3>
                    @if ($cercaError && $cercaError->get_field('titol-caixa-principal'))
                        {{ $cercaError->get_field('titol-caixa-principal') }}
                    @else
                        @lang('No s\'ha trobat cap resultat')
                    @endif
                </h3>
                @if ($cercaError && $cercaError->get_field('descripcio-caixa-principal'))
                    {!! $cercaError->get_field('descripcio-caixa-principal') !!}
                @else
                    @lang('És possible que, ara mateix, no existeixi cap curs amb les característiques del teu interès. Periòdicament a FORMABAGES es publiquen nous cursos, o per a més informació, <a href=":url">contacta\'ns</a>.', ['url' => get_page_url('contacte')])
                @endif
                <div class="link-btn">
                    <a href="javascript:void(0)" onclick="focusInput()">@lang('Nova cerca')</a>
                </div>
            </div>
        </li>
    @endforelse
</ul>

<div class="btn-div pagination">
    {{ $cursos->appends($_GET)->links('Front::pagination.bootstrap-4') }}
</div>