<ul class="notices">
    @foreach($noticies as $noticia)
    <li>
        <div class="notices-txt">
            <h2><a href="{{ $noticia->get_url() }}">{{ $noticia->title }}</a></h2>
            
            @if ($data = $noticia->get_field('data-publicacio'))
                <span class="ndate">{{ getDayAndMonthCat($data, 'd/m/Y') }}</span>
            @endif

            <p>{!! get_excerpt(strip_tags($noticia->description, 20)) !!}</p>
            <a href="{{ $noticia->get_url() }}" class="more">@lang('Més informació')</a>
        </div>

    </li>
    @endforeach
</ul>

<div class="btn-div pagination">
    {{ $noticies->links('Front::pagination.bootstrap-4') }}
</div>