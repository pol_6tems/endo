<ul class="agenda">
    @foreach($agendas as $item)
        @if ($item->type == "curso")
            <li>
                <a href="{{ $item->get_url() }}">
                    <h2>{{ $item->title }}</h2>
                    <div class="values">
                    
                        @if ($publicacio = getDayAndMonthCat($item->get_field('data-inici-del-curs')))
                            <span class="cdate">{{ $publicacio }}</span>
                        @endif
                        
                        @if ($centre = $item->get_field('centre'))
                            @if ($poblacio = $centre->get_field('poblacion'))
                                <span class="loc">{{ $poblacio->title }}</span>
                            @endif
                            <span class="org"><strong>@lang('Organitza:')</strong> {{ $centre->title }}</span>
                        @endif

                    </div>
                    <p>{!! strip_tags(get_excerpt( $item->get_field('introduccio'), 15)) !!}</p>
                    <span class="more">@lang('Més informació')</span>

                    @php($inici = $item->get_field('data-inici-del-curs'))
                    @if ($inici && Date::createFromFormat("d/m/Y", $inici) > Date::now())
                        <div class="ribbon-container ribbon-top-right">
                            <span>@lang('Curs no iniciat')</span>
                        </div>
                    @endif
                </a>
            </li>
        @else
            <li>
                <a href="{{ $item->get_url() }}">
                    <h2>{{ $item->title }}</h2>
                    <div class="values">
                    
                        @if ($publicacio = getDayAndMonthCat($item->get_field('publicacio')))
                            <span class="cdate">{{ $publicacio }}</span>
                        @endif
                        
                        @if ($loc = $item->get_field('localitzacio'))
                            <span class="loc">{{ $loc }}</span>
                        @endif
                    </div>
                    <span class="org"><strong>@lang('Organitza:')</strong> {{ $item->get_field('organitzador') }}</span>
                    <p>{!! strip_tags(get_excerpt( $item->description, 15)) !!}</p>
                    <span class="more">@lang('Més informació')</span>
                </a>
            </li>
        @endif
    @endforeach
</ul>
<div class="btn-div">
    {{ $agendas->links('Front::pagination.bootstrap-4') }}
</div>