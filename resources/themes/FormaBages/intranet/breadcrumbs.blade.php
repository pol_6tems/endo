<div class="bread-cum">
    <div class="row">
        <ul>
            <li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
            @if (!route_name('intranet.index') && !route_name('intranet.resources'))
                <li><a href="{{ route('intranet.index') }}">@lang('Cursos')</a></li>
            @endif
            <li>{{ route_name('intranet.index') ? __('Cursos') : (route_name('intranet.user') ? __('Dades d\'usuari') : (route_name('intranet.resources') ? __('Recursos') : (route_name('intranet.create') ? __('Publicar') : __('Editar')))) }}</li>
        </ul>
    </div>
</div>