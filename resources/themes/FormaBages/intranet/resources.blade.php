@extends('Front::layouts.base-intranet')

@section('content')
    <div class="row">
        <h1 class="page-title">@lang('Recursos')</h1>
        {{--<p class="page-desc"></p>--}}
    </div>

    <div class="qui-som-outer">
        <div class="row">
            <div class="document-cont">
                {{--<h1>@lang('Estudis, informes i publicacions')</h1>--}}
                @if (isset($docs) && $docs && count($docs))
                    {{--<h3>@lang('Documentació')</h3>--}}
                    <ul class="document-list">
                        @foreach($docs as $doc)
                            @if ($file = $doc->get_field('document'))
                                <li>
                                    <h4>
                                        <a href="{{ $file->get_thumbnail_url() }}" download>{{ $doc->title }}</a>
                                    </h4>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
@endsection