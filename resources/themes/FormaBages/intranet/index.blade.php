@extends('Front::layouts.base-intranet')

@section('content')
    <h1>@lang('Cursos publicats')</h1>

    <table class="index-table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>@lang('Data de publicació')</th>
                <th>@lang('Títol del curs')</th>
                <th>@lang('Data d\'inici')</th>
                <th>@lang('Editar')</th>
                <th>@lang('Clonar')</th>
                <th>@lang('Eliminar')</th>
            </tr>
        </thead>
        <tbody>
            @foreach($courses as $course)
                <tr>
                    <td>{{ $course->created_at->format('d / m / y') }}</td>
                    <td class="title">{{ $course->title }}</td>
                    <td>{{ str_replace('/', ' / ', $course->get_field('data-inici-del-curs')) }}</td>
                    <td>
                        <a href="{{ route('intranet.edit', ['id' => $course->id]) }}">
                            <img src="{{ asset($_front . 'images/edit.svg') }}">
                        </a>
                    </td>
                    <td>
                        <button data-url="{{ route('intranet.duplicate', ['id' => $course->id]) }}" data-name="@lang('Are you sure to duplicate :name?', ['name' => $course->title])" class="duplicate" type="button" title="@lang('Clonar')">
                            <img src="{{ asset($_front . 'images/clonar.svg') }}">
                        </button>
                    </td>
                    <td>
                        <button data-url="{{ route('intranet.destroy', ['id' => $course->id]) }}" data-name="@lang('Are you sure to delete :name?', ['name' => $course->title])" class="remove" type="button" data-toggle="tooltip" data-placement="top" title="@lang('Delete')">
                            <img src="{{ asset($_front . 'images/eliminar.svg') }}">
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="btn-div pagination">
        {{ $courses->links('Front::pagination.bootstrap-4') }}
    </div>
@endsection

@section('scripts')
    <!--  Plugin for Sweet Alert -->
    <script src="{{asset($_admin . 'js/plugins/sweetalert2.js')}}"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

    <script>
        $('.remove').on('click', function(e) {
            var $tr = $(this).closest('tr');
            var url = $(this).data('url');
            var title = $(this).data('name');

            swal({
                text: title,
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonText: '@lang('Delete')',
                cancelButtonText: '@lang('Cancel')',
                buttonsStyling: false
            }).then(function(result) {
                if ( result.value ) {
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {'_token': '{{csrf_token()}}', '_method': 'DELETE'},
                        success: function(data) {
                            swal({
                                title: "@lang('Deleted')",
                                text: "@lang('Item deleted succesfully.')",
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            });
                            $tr.remove();
                            e.preventDefault();
                        },
                        error: function(data) {
                            swal({
                                title: "@lang('Error')",
                                text: "@lang('Error saving data')",
                                type: 'error',
                                confirmButtonClass: "btn",
                                buttonsStyling: false
                            });
                        }
                    });
                }
            });
        });

        $('.duplicate').on('click', function(e) {
            var url = $(this).data('url');
            var title = $(this).data('name');
            swal({
                text: title,
                type: 'info',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonText: '@lang('Duplicate')',
                cancelButtonText: '@lang('Cancel')',
                buttonsStyling: false
            }).then(function(result) {
                if ( result.value ) {
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {'_token': '{{ csrf_token() }}', '_method': 'PUT'},
                        success: function(data) {
                            swal({
                                title: "@lang('Duplicated')",
                                text: "@lang('Item duplicated succesfully.')",
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }).then(function(result) {
                                location.reload();
                            });
                        },
                        error: function(data) {
                            swal({
                                title: "@lang('Error')",
                                text: "@lang('Error saving data')",
                                type: 'error',
                                confirmButtonClass: "btn",
                                buttonsStyling: false
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection