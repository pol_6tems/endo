<?php
/**
 * @title: Text
 */
?>

@php
    $max = (isset($params->max_length) && $params->max_length) ? $params->max_length : '';
    $min = (isset($params->min_length) && $params->min_length) ? $params->min_length : '';
    $placeholder = (isset($params->placeholder) && $params->placeholder) ? $params->placeholder : '' ;
    $required_title = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '';
    $required = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? 'required' : '';
    $type = (isset($params->input_type) && $params->input_type) ? $params->input_type : 'text';
@endphp

<div id="cf{{ $custom_field->id }}" data-id="{{ $custom_field->id }}" class="mt-4 m-auto custom-field text-field">
    <div class="flex-row pt-3">
        <div class="col-md-12">
            <div class="form-group">
                <input
                    name="{{ $name }}"
                    type="{{ $type }}"
                    class="form-control"
                    value="{{ $value }}"
                    placeholder="{{ $placeholder }}"
                    {{ $required }}/>
            </div>
        </div>
    </div>
</div>