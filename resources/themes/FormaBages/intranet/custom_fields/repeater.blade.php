    <?php
/**
 * @title: Repeater
 */
?>

@php
    $cf_id = $custom_field->id;
    $max = (isset($params->max_length) && $params->max_length) ? $params->max_length : '';
    $min = (isset($params->min_length) && $params->min_length) ? $params->min_length : '';
    $placeholder = (isset($params->placeholder) && $params->placeholder) ? $params->placeholder : '' ;
    $required_title = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '';
    $required = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? 'required' : '';

    $name .= '[fields]';
    $k = 0;
    $nfields = 0;
    $repeater_field = ( !empty($post) ) ? $post->get_field($custom_field->name, isset($language_item) ? $language_item->code : null) : null;
@endphp
<div id="cf{{ $cf_id }}" class="mt-4 m-auto custom-field repeater-field">
    <div class="table">
        <ul id="sortable-{{ $cf_id }}" class="sortable tbody">
            @if ( !empty($repeater_field) )
                @php($nfields = count($repeater_field))
                @foreach ( $repeater_field as $k => $subfields )
                    <div class="cf-item" data-cfid="{{ $cf_id }}" data-lang="{{ isset($language_item) ? $language_item->code : '' }}">
                        <div class="cf-row">
                            <input class="order" type="hidden" name="{{$name}}[{{$k}}][order]" value={{ $k }}>
                            <li><div class="li-field-order"><span>{{ $k + 1 }}</span></div></li>
                            <li class="li-field-text">
                                @foreach ($custom_field->fields as $k2 => $cf)
                                    @includeIf('Front::intranet.custom_fields.' . $cf->type, [
                                        'title' => $cf->title,
                                        'name' => $name . "[".$k."][".$cf->id."]",
                                        'value' => $subfields[$cf->name]['value'],
                                        'params' => json_decode($cf->params),
                                        'position' => $cf_group->position,
                                        'custom_field' => $cf,
                                        'order' => $k,
                                    ])
                                @endforeach
                            </li>
                            <li class="li-field-delete">
                                <div class="row2">
                                    <div class="col-lg-12">
                                        <a href="javascript:void(0);" onclick="remove_row(this);" style="float: right;" type="button" rel="tooltip" class="">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </div>
                    </div>
                @endforeach
            @endif
        </ul>
    </div>
</div>

@section("scripts-cf$cf_id")
<script>
$(document).ready(function(){
    recalcularGrid_{{$cf_id}}();
});

function remove_row(e) {
    
    var order = $(e).closest('.cf-item').find('.order').val();
    var parent = $(e).closest('.cf-item').data('cfid');
    var lang = $(e).closest('.cf-item').data('lang');
    var posts = $(e).closest('.cf-item').find('.custom-field');
    
    posts.each(function(idx, el) {
        var custom_field_id = $(el).data('id');
        $(e).closest('.sortable').prepend($(`<input type="hidden" name="custom_fields[deleted][${lang}][${parent}][${order}]" value="" />`));
    });

    var name = $(e).closest('.cf-item').find('input.order').attr('name').split('[');
    name.pop();
    name = name.join('[') + '[deleted]';
    $(e).closest('.cf-item').slideUp(function(){
        // $(e).closest('.cf-item').remove();
    });
}

$('#add_field-{{ $cf_id }}').click(function(e) {
    var ordre = $('#sortable-{{ $cf_id }}>.cf-item').length;

    if ("{{ $max }}" == "" || !isNaN("{{ $max }}") && ordre < "{{ $max }}") {
        // if ("{{ $min }}" == "" || !isNaN("{{ $min }}") && ordre >= "{{ $min }}") {
            var customEvent = new CustomEvent('repeater_add_field', {
                detail: { id: "{{ $cf_id }}" },
                bubbles: true,
                cancelable: true
            });

            var $element = $(`
            <div class="cf-item" data-cfid="{{ $cf_id }}">
                <div class="cf-row">
                    <input class="order" type="hidden" name="{{$name}}[${ordre}][order]" value="${ordre}">
                    <li><div class="li-field-order"><span>${ordre + 1}</span></div></li>
                    <li class="li-field-text">
                        @foreach ($custom_field->fields as $k2 => $cf)
                            @includeIf('Front::intranet.custom_fields.' . $cf->type, [
                                'title' => $cf->title,
                                'name' => $name . "[".$nfields."][".$cf->id."]",
                                'value' => '',
                                'params' => json_decode($cf->params),
                                'position' => $cf_group->position,
                                'custom_field' => $cf,
                            ])
                        @endforeach
                    </li>
                    <li class="li-field-delete">
                        <div class="row2">
                            <div class="col-lg-12">
                                <a href="javascript:void(0);" onclick="remove_row(this);" style="float: right;" type="button" rel="tooltip" class="">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                    </li>
                </div>
            </div>
            `);

            // Replace all input names with new order
            $element.find('input, select').each(function(i, e) {
                var e_name = $(e).attr('name');
                if ( typeof(e_name) !== 'undefined' ) {
                    e_name = e_name.replace('{{$name}}', '');

                    var i_pos = e_name.indexOf('[');
                    var f_pos = e_name.indexOf(']');
                    var old_ordre = e_name.substr(i_pos, f_pos + 1);
                    e_name = e_name.replace(old_ordre, '[' + ordre + ']');
                    
                    $(e).attr('name', '{{$name}}' + e_name);
                }
            });

            $element.find('input.order').first().val(ordre);
            
            $('#sortable-{{ $cf_id }}').append($element);
            recalcularGrid_{{$cf_id}}();

            $element.find('input[type="text"]').focus();
            // e.target.dispatchEvent(customEvent);
        // }
    }
});

function recalcularGrid_{{$cf_id}}() {
    var sortable = '#sortable-{{ $cf_id }}';
    $( sortable ).sortable({
        axis: "y",
        opacity: 0.5,
        update: function( event, ui ) {
            $(sortable + ">.cf-item").each(function(idx, element) {
                updateFieldOrder_{{$cf_id}}(element, idx);
            });
        }
    });
}

function updateFieldOrder_{{$cf_id}}(element, order) {
    $(element).attr('id', 'field-'+order);
    $(element).attr('data-order', order);
    $(element).find('.cf-row .li-field-order span').first().text(order);
    $(element).find('input.order').first().val(order);
}
</script>

{{-- Declarem tots els espais per els scripts dels CF del repeater --}}
@foreach ($custom_field->fields as $cf)
    <!-- Scripts CF Repeater {{ $cf_id }} id: {{ $cf->id }}-->
    @yield("scripts-cf$cf->id")
@endforeach

@endsection