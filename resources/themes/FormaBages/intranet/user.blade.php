@extends('Front::layouts.base-intranet')

@section('content')
    <h1>@lang('Dades d\'usuari')</h1>
    <h2>@lang('Pot modificar les seves dades d\'accés')</h2>

    <div class="edit-course">
        <h3>@lang('Modificar usuari')</h3>
        <form method="POST">
            @csrf
            <ul>
                <li>
                    <input type="text" name="name" class="frm-ctrl" placeholder="@lang('Nom Entitat')" value="{{ auth()->user()->name }}" required>
                </li>

                <li>
                    <input type="text" name="email" class="frm-ctrl" placeholder="E-mail" value="{{ auth()->user()->email }}" required>
                </li>
            </ul>

            <div class="btn-container">
                <button type="submit" class="mt10">@lang('Guardar')</button>
            </div>
        </form>
    </div>

    <div class="edit-course">
        <h3>@lang('Modificar la clau d\'accés')</h3>

        <form action="{{ route('intranet.user.pwd') }}" method="POST">
            @csrf
            <ul>
                <li>
                    <input type="password" name="password" class="frm-ctrl" placeholder="@lang('Nova contrasenya')" required>
                </li>

                <li>
                    <input type="password" name="password_confirmation" class="frm-ctrl" placeholder="@lang('Repetir contrasenya')" required>
                </li>
            </ul>

            <div class="btn-container">
                <button type="submit" class="mt10">@lang('Guardar')</button>
            </div>
        </form>
    </div>
@endsection