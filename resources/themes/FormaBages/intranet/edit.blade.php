@extends('Front::layouts.base-intranet')

@section('content')
    <h1>{{ route_name('intranet.create') ? __('Introdueix les dades del curs') : __('Edita les dades del curs') }}</h1>

    <div class="edit-course">
        <form action="{{ isset($course) ? route('intranet.update', ['id' => $course->id]) : route('intranet.store') }}" method="POST">
            @csrf

            @if (isset($course))
                @method('PUT')
            @endif

            @php ($centre = $user->get_field('centre'))
            @if ($centre && is_object($centre))
                <input type="hidden" name="centre" value="{{ $centre->id }}">
            @endif

            <ul>
                <li>
                    <label>@lang('Nom del Curs')</label>
                    <input type="text" name="title" class="frm-ctrl" placeholder="@lang('Escriu el nom o títol del curs')" value="{{ isset($course) ? $course->title : old('title') }}" required>
                </li>

                <li>
                    <label>@lang('Escull el tipus de formació on pertany')</label>
                    <select name="tipus-formacio" class="select_box" required>
                        <option value="0">@lang('Escollir formació')</option>
                        @foreach($types as $type)
                            <option value="{{ $type->id }}" {{ (isset($course) && $course->get_field('tipus-formacio')) ? ($course->get_field('tipus-formacio')->id == $type->id) : (old('tipus-formacio') == $type->id) ? 'selected' : '' }}>{{ $type->title }}</option>
                        @endforeach
                    </select>
                </li>
                @if (!isset($course))
                    <li>
                        <label>@lang('Si no existeix el tipus de formació, omple aquest camp')</label>
                        <input type="text" name="tipus-formacio-proposta" class="frm-ctrl" placeholder="@lang('Introdueix la proposta de tipus de formació')" value="{{ old('tipus-formacio-proposta') }}">
                    </li>
                @endif

                <li>
                    <label>@lang('Escull el subtipus de formació on pertany')</label>
                    <select name="subtipus-formacio" class="select_box" required>
                        <option value="0">@lang('Escollir subtipus formació')</option>
                        @foreach($subtypes as $subtype)
                            <option value="{{ $subtype->id }}" {{ (isset($course) && $course->get_field('subtipus-formacio')) ? ($course->get_field('subtipus-formacio')->id == $subtype->id) : (old('tipus-formacio') == $subtype->id) ? 'selected' : '' }}>{{ $subtype->title }}</option>
                        @endforeach
                    </select>
                </li>
                @if (!isset($course))
                    <li>
                        <label>@lang('Si no existeix el subtipus de formació, omple aquest camp')</label>
                        <input type="text" name="subtipus-formacio-proposta" class="frm-ctrl" placeholder="@lang('Introdueix la proposta de subtipus de formació')" value="{{ old('subtipus-formacio-proposta') }}">
                    </li>
                @endif

                <li>
                    <label>@lang('Escull la família professional')</label>
                    <select name="familia-formativa" class="select_box" required>
                        <option value="0">@lang('Escollir família professional')</option>
                        @foreach($families as $familia)
                            <option value="{{ $familia->id }}" {{ (isset($course) && $course->get_field('familia-formativa')) ? ($course->get_field('familia-formativa')->id == $familia->id) : (old('familia-formativa') == $familia->id) ? 'selected' : '' }}>{{ $familia->title }}</option>
                        @endforeach
                    </select>
                </li>

                @if (!isset($course))
                    <li>
                        <label>@lang('Si no existeix el la família professional, omple aquest camp')</label>
                        <input type="text" name="familia-professional-proposta" class="frm-ctrl" placeholder="@lang('Introdueix la proposta de família professional')" value="{{ old('familia-professional-proposta') }}">
                    </li>
                @endif

                <li>
                    <label>@lang('Duració del curs')</label>
                    <input type="text" name="duracio" class="frm-ctrl" placeholder="@lang('Escriu el total d\'hores del curs (ex. 100h)')" value="{{ isset($course) ? $course->get_field('duracio') : old('duracio') }}">
                </li>
                @if (!$user->get_field('centre'))
                    <li>
                        <label>@lang('Escull el centre')</label>
                        <select name="centre" class="select_box" required>
                            <option value="0">@lang('Seleccionar modalitat')</option>
                            @foreach($centres as $centre)
                                <option value="{{ $centre->id }}" {{ (isset($course) && $course->get_field('centre') && $course->get_field('centre')->id) ? ($course->get_field('centre')->id == $centre->id) : (old('centre') == $centre->id) ? 'selected' : '' }}>{{ $centre->title }}</option>
                            @endforeach
                        </select>
                    </li>
                @endif

                <li>
                    <label>@lang('Enllaç')</label>
                    <input type="text" name="enllac" class="frm-ctrl" placeholder="@lang('Introdueix un enllaç')" value="{{ isset($course) ? $course->get_field('enllac') : old('enllac') }}">
                </li>

                <li>
                    <label>@lang('Escull modalitat')</label>
                    <select name="modalitat" class="select_box" required>
                        <option value="0">@lang('Seleccionar modalitat')</option>
                        @foreach($modalitats as $modalitat)
                            @php($choice = explode(':', $modalitat))
                            <option value="{{ $choice[0] }}" {{ isset($course) ? ($course->get_field('modalitat') == $choice[0]) : (old('modalitat') == $choice[0]) ? 'selected' : '' }}>{{ $choice[1] }}</option>
                        @endforeach
                    </select>
                </li>

                <li>
                    <div class="jquery-datepicker" data-lang="{{ app()->getLocale() }}">
                        <input type="text" name="data-inici-preseleccio" class="frm-ctrl jquery-datepicker__input bg-date" placeholder="@lang('Data d\'inici preselecció')" value="{{ isset($course) ? $course->get_field('data-inici-preseleccio') : old('data-inici-preseleccio') }}" autocomplete="off">
                    </div>
                </li>
                <li>
                    <div class="jquery-datepicker half" data-lang="{{ app()->getLocale() }}">
                        <input type="text" name="data-inici-del-curs" class="frm-ctrl jquery-datepicker__input bg-date" placeholder="@lang('Data d\'inici')" value="{{ isset($course) ? $course->get_field('data-inici-del-curs') : old('data-inici-del-curs') }}" autocomplete="off">
                    </div><div class="jquery-datepicker half" data-lang="{{ app()->getLocale() }}">
                        <input type="text" name="data-final-del-curs" class="frm-ctrl jquery-datepicker__input bg-date" data-lang="{{ app()->getLocale() }}" placeholder="@lang('Data de fi')" value="{{ isset($course) ? $course->get_field('data-final-del-curs') : old('data-final-del-curs') }}" autocomplete="off">
                    </div>
                </li>

                <li>
                    <div class="checkbox">
                        <input type="checkbox" id="aprox-dates" name="data-no-definitiva" {{ isset($course) && $course->get_field('data-no-definitiva') ? 'checked' : (old('remember') ? 'checked' : '') }}>
                        <label for="aprox-dates">@lang('Dates aproximades (marca en cas que les dates siguin aproximades)')</label>
                    </div>
                </li>

                <li>
                    <label>@lang('Introducció')</label>
                    <textarea rows="5" name="introduccio" class="frm-ctrl" placeholder="@lang('Explica els objectius i la presentació del curs')">{{ isset($course) ? strip_tags(str_replace(["<br />","<br>","<br/>"], "\r\n", $course->get_field('introduccio'))) : old('introduccio') }}</textarea>
                </li>

                <li>
                    <div class="cf-title">
                        <span>@lang('Introdueix els diferents blocs del temari formatiu')</span><button id="add_field-{{ $temariCF->id }}" type="button" rel="tooltip">
                            @lang('Add')
                        </button>
                    </div>

                    @include('Front::intranet.custom_fields.repeater', [
                        'name' => "custom_fields[".$temariCF->id . "]",
                        'value' => isset($course) ? $course->get_field('temari') : null,
                        'params' => json_decode($temariCF->params),
                        'custom_field' => $temariCF,
                        'cf_group' => $temariCF->group,
                        'post' => isset($course) ? $course : null
                    ])
                </li>

                <li>
                    <div class="cf-title">
                        <span>@lang('Introdueix els diferents horaris del curs')</span><button id="add_field-{{ $horariCF->id }}" type="button" rel="tooltip">
                            @lang('Add')
                        </button>
                    </div>

                    @include('Front::intranet.custom_fields.repeater', [
                        'name' => "custom_fields[".$horariCF->id . "]",
                        'value' => isset($course) ? $course->get_field('horari') : null,
                        'params' => json_decode($horariCF->params),
                        'custom_field' => $horariCF,
                        'cf_group' => $horariCF->group,
                        'post' => isset($course) ? $course : null
                    ])
                </li>

                <li>
                    <div class="cf-title">
                        <span>@lang('Escriu els requisits per poder realitzar el curs')</span><button id="add_field-{{ $requisitsCF->id }}" type="button" rel="tooltip">
                            @lang('Add')
                        </button>
                    </div>

                    @include('Front::intranet.custom_fields.repeater', [
                        'name' => "custom_fields[".$requisitsCF->id . "]",
                        'value' => isset($course) ? $course->get_field('requisits') : null,
                        'params' => json_decode($requisitsCF->params),
                        'custom_field' => $requisitsCF,
                        'cf_group' => $requisitsCF->group,
                        'post' => isset($course) ? $course : null
                    ])
                </li>

                <li>
                    <label>@lang('Tràmits d\'accés')</label>
                    <textarea rows="5" name="text-tramits" class="frm-ctrl" placeholder="@lang('En cas que sigui necessari, explica els tràmits d\'accés necessaris')">{{ isset($course) ? strip_tags(str_replace(["<br />","<br>","<br/>"], "\r\n", $course->get_field('text-tramits'))) : old('text-tramits') }}</textarea>
                </li>

                <li>
                    <label>@lang('Continuïtat del curs')</label>
                    <textarea rows="5" name="text-continuitat" class="frm-ctrl" placeholder="@lang('En cas que sigui necessari, explica aspectes relacionats amb la continuïtat del curs')">{{ isset($course) ? strip_tags(str_replace(["<br />","<br>","<br/>"], "\r\n", $course->get_field('text-continuitat'))) : old('text-continuitat') }}</textarea>
                </li>

                @if (!isset($course))
                    <li>
                        <label>@lang('Observacions')</label>
                        <textarea rows="5" name="observacions" class="frm-ctrl" placeholder="@lang('Escriu qualsevol comentari que consideris necessari per als administradors de FORMABAGES')">{{ old('observacions') }}</textarea>
                    </li>
                @endif
            </ul>

            <div class="btn-container">
                <button type="submit">{{ isset($course) ? __('Actualitzar') : __('Publicar') }}</button>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="{{asset($_admin.'js/core/jquery-ui.min.js')}}"></script>

    @yield("scripts-cf$temariCF->id")
    @yield("scripts-cf$horariCF->id")
    @yield("scripts-cf$requisitsCF->id")
@endsection