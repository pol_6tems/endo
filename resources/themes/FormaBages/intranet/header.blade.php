<header class="inner-header">
    <div class="main-menu">
        <div class="row">
            <div class="logo-mobile">
                <a href="{{ get_page_url('home') }}">
                    <h1 class="logo">forma<span style="color:#FB6262">BAGES</span></h1>
                </a>
            </div>
            <div class="menu">
                <div id="smoothmenu" class="ddsmoothmenu">
                    <ul class="intranet-right">
                        <li><a href="{{ route('intranet.index') }}" class="{{ route_name('intranet.index') ? 'active' : '' }}">@lang('Veure cursos')</a></li>
                        <li><a href="{{ route('intranet.create') }}" class="{{ route_name('intranet.create') ? 'active' : '' }}">@lang('Publicar curs')</a></li>
                        @if (get_posts('recurs-intranet')->count())
                            <li><a href="{{ route('intranet.resources') }}" class="{{ route_name('intranet.resources') ? 'active' : '' }}">@lang('Recursos')</a></li>
                        @endif
                        <li><span>@lang('Hola'), <a href="{{ route('intranet.user') }}">{{ auth()->user()->name }}</a></span></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('Sortir')</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- mobile nav starts-->
            <div class="m-menu"><a href="javascript:void(0);" id="hie_menu"> <span></span> <span></span> <span></span></a></div>
            <!-- mobile nav ends-->
            <div class="search-div"> <a href="javascript:void(0);" id="search-icon"></a></div>
        </div>
        <section class="inner-search">
            <div class="row">
                <div class="logo-inner">
                    <a href="{{ get_page_url('home') }}"> <img src="{{ asset($_front.'images/logo-white.png') }}" alt="" border="0"> </a>
                </div>
                @php ($centre = auth()->user()->get_field('centre'))
                @if ($centre && is_object($centre))
                    <div class="intranet-center">El teu centre: {{ mb_strtoupper($centre->title) }}</div>
                @endif
            </div>
        </section>
    </div>
</header>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>