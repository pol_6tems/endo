@extends('Admin::layouts.admin')

@section('section-title')
@Lang(ucfirst($post_type_plural))
@endsection

@section('content')
@php
    $params = ($params) ? json_decode($params) : null;
    $trobat = false;
    foreach ($cf_groups as $key => $cf_group) {
        if($cf_group->position == 'r-sidebar') {
            $trobat = true;
            break;
        }
    }

    // Revisem tots els parametres per saber si hem de reservar espai per la sidebar
    $sidebar = in_array($post->type, ['post', 'page']) || ($params && isset($params->published) && $params->published ||
    $params && isset($params->author) && $params->author ||
    $params && isset($params->imatge) && $params->imatge) || $trobat;
@endphp
<form id="PostForm" class="form-horizontal" action="{{ route($section_route . ".update", ['post' => $post->id, 'post_type' => $post_type]) }}" method="post">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="status" value="{{ $post->status }}" />
    <input type="hidden" name="type" value="{{ $post->type }}" />
    @includeIf('Admin::partials.languages', ['idiomes' => $_languages, 'prepend' => 'general', 'currentLang' => $language_code])
    <div class="tab-content">
        @foreach($_languages as $language_item)
            @php($current_lang = $language_item->code)
            <div id="general{{ $current_lang }}" class="tab-pane {{ ($current_lang == $language_code) ? 'active' : '' }}">
                <div class="actions-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copy-lang dropdown">
                                <button class="btn btn-primary dropdown-info btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">file_copy</i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach($_languages as $lang)
                                        @if ($lang->code == $current_lang) @continue @endif
                                        <a class="dropdown-item" data-code="{{ $lang->code }}" data-lang="{{ $lang->name }}" href="#">@lang("Copy from :lang", ['lang' => $lang->name])</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        {{ mostrarErrors($errors) }}
                        <div class="flex-row">
                            <div class="col-md-{{ ($sidebar) ? '8 mr-auto' : '12' }}">
                                <article id="post{{ $post->id }}" data-post="{{ $post->id }}" class="card">
                                    <div class="card-header card-header-primary card-header-icon">
                                        <div class="card-icon">
                                            <i class="material-icons">library_books</i>
                                        </div>
                                        <h4 class="card-title">
                                            @Lang('Edit')
                                            <a href='{{ route($section_route.".create", ["post_type" => $post_type]) }}' class="btn btn-primary btn-sm">@Lang('Add')</a>
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        @includeIf('Front::partials.edit-post-disclaimer')
                                        @includeIf('Front::admin.partials.general-form')
                                        <!-- Fi General -->

                                        @foreach($subforms as $key => $subform)
                                            @php($template = explode('.', $subform['titol_tab'])[0])
                                            <div id="{{ $template }}" class="tab-pane">
                                                @includeIf($subform['template'])
                                            </div>
                                        @endforeach
                                    </div>
                                </article>
                            </div>
                            <!-- Sidebar -->
                            @if($sidebar)
                                @includeIf('Front::admin.partials.posts-sidebar')
                            @endif

                            <div class="col-md-12">
                                {{-- CF GROUPS --}}
                                @foreach ($cf_groups as $key => $cf_group)
                                    @if($cf_group->position == 'bottom')
                                    <article id="post{{ $post->id}}-cfg{{ $cf_group->id }}" data-post="{{ $post->id }}" class="card pm">
                                        <div class="card-header card-header-primary card-header-icon">
                                            <div class="flex-row" style="justify-content: space-between;">
                                                <div class="header-left">
                                                    <h4 class="card-title">{{ $cf_group->title }}</h4>
                                                </div>
                                                <div class="header-right">
                                                    <a data-toggle="collapse" href="#cf_group-{{ $current_lang }}-{{ $cf_group->id }}">
                                                        <i class="material-icons">keyboard_arrow_down</i></a>
                                                        @if(Auth::user()->isAdmin())
                                                        <a href="{{ route('admin.custom_fields.edit', ['custom_field' => $cf_group->id, 'post_type' => $post_type]) }}" target="_blank" rel="tooltip">
                                                            <i class="material-icons">settings</i>
                                                        </a>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div id="cf_group-{{ $current_lang }}-{{ $cf_group->id }}" class="collapse">
                                            <div class="card-body pb-5">
                                                <div class="custom_field_group_fields">

                                                @foreach ($cf_group->fields as $k => $custom_field)
                                                    @php($viewParams = [
                                                        'title' => $custom_field->title,
                                                        'name' =>
                                                        "custom_fields[".$current_lang."][".$custom_field->id . "]",
                                                        'value' => $post->get_field( $custom_field->name, $current_lang ),
                                                        'params' => json_decode($custom_field->params),
                                                        'position' => $cf_group->position,
                                                        'custom_field' => $custom_field,
                                                        'order' => $k,
                                                        'lang' => $current_lang,
                                                    ])

                                                    @if (View::exists('Admin::default.custom_fields.' . $custom_field->type))
                                                    <!-- General -->
                                                        @includeIf('Admin::default.custom_fields.' . $custom_field->type, $viewParams)
                                                    @else
                                                        @php($module = explode(".", $custom_field->type)[0])
                                                        @php($field = explode(".", $custom_field->type)[1])
                                                        @includeIf('Modules::'.$module.'.resources.views.custom_fields.'.$field, $viewParams)
                                                    @endif
                                                @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    @endif
                                @endforeach
                            </div>

                            @php ( execute_actions('post_form_fields', $post) )

                            <div class="col-md-12">
                                @if (isset($save_disclaimer) && $save_disclaimer)
                                <div>
                                    {{ $save_disclaimer }}
                                </div>
                                @endif

                                <!-- Post Meta -->
                                <article class="card sticky bottom">
                                    <!-- Footer -->
                                    <div class="card-footer">
                                        <div class="mr-2">
                                            <input type="button"
                                                class="btn btn-previous btn-fill btn-default btn-wd"
                                                name="previous" value="@Lang('Tornar')"
                                                onclick="window.location.href='{{ route('admin.posts.index', ['post_type' => $post_type]) }}'">
                                        </div>
                                        @if (view()->exists("Front::posts.single-{$post->type}") || in_array($post_type, ['post', 'page']))
                                        <div class="mr-auto">
                                            <input type="button" class="btn btn-previous btn-fill btn-info" name="show" value="@lang('View')" onclick="window.open('{{ $post->get_url() }}')">
                                        </div>
                                        @endif

                                        <div class="ml-auto">
                                            <input id="SaveBtnSubmit" type="submit"
                                                class="btn btn-next btn-fill btn-primary btn-wd" name="next"
                                                value="@Lang( ($post->status != 'draft' ? 'Update' : 'Publish') ) {{ $post_type_title }}"
                                                onclick="{{ $post->status == 'draft' ? 'publish_draft();' : '' }}">
                                        </div>

                                        @if ( $post->status != 'draft' )
                                        <div class="ml-2">
                                            <input type="button" class="btn btn-previous btn-fill btn-default btn-wd"
                                                name="previous" value="@Lang('Draft')" onclick="save_draft();">
                                        </div>
                                        @endif

                                        @if ( $can_aprove_pending )
                                        @php($url = route($section_route . ".aprove_pending", ['post' => $post->id, 'post_type' => $post_type]))
                                        <div class="ml-2">
                                            <input type="button" class="btn btn-success btn-fill" name="aprove"
                                                value="@Lang('Aprove')"
                                                onclick="var form_aux = document.getElementById('PostForm');form_aux.action = '{{ $url }}';form_aux.submit();">
                                        </div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</form>
@endsection

@section('styles')
<link href="{{asset($_admin.'js/summernote/summernote.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset($_admin.'js/summernote/plugin/codemirror/codemirror.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset($_admin.'js/summernote/plugin/codemirror/monokai.css')}}">
@endsection

@section('scripts')
<script>
    var post = @json($post);
    var lang = '{{ $language_code }}';
</script>
<script src="{{asset($_admin.'js/summernote/plugin/codemirror/codemirror.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/plugin/codemirror/xml.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/plugin/codemirror/formatting.js')}}"></script>

<script src="{{asset($_admin.'js/summernote/summernote.js')}}"></script>
<script src="{{asset($_admin.'js/summernote.initialize.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/plugin/image-list/summernote-image-list.min.js')}}"></script>
<script src="{{asset($_admin.'js/clipboard.min.js')}}"></script>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<script>
    $(document).on('click', '.copy-lang', function(event) {
        var target = $(event.target);
        var current_lang = $('.nav-link.active');

        Swal.fire({
            title: '@lang('Estas seguro de copiar el contenido de ')' + target.data('lang'),
            text: '@lang('Se eliminarà todo el contenido de ')' + current_lang.data('lang'),
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: '@lang('Cancelar')',
            cancelButtonColor: '#d33',
            confirmButtonText: '@lang('Aceptar')',
            confirmButtonColor: '#28a745'
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '{{ route("admin.posts.copy_content", ["post" => $post]) }}',
                    method: 'PUT',
                    data: {
                        "from": current_lang.data('code'),
                        "to": target.data('code')
                    },
                    success: function(data) {
                        var data = JSON.parse(data);
                        document.location.href = data.url;
                    }
                });
            }
        })
    });
</script>
    @php( $summernote_lang = 'en-US' )
    @if( \App::getLocale() == 'ca' )
        @php( $summernote_lang = 'ca-ES' )
    @else
        @php( $summernote_lang = strtolower(\App::getLocale()) . '-' . strtoupper(\App::getLocale()) )
    @endif

    @if ( \App::getLocale() != 'en' )
        <script src="{{asset($_admin.'js/summernote/lang/summernote-'.$summernote_lang.'.js')}}"></script>
    @endif

    <script>
        $('button.copy').click(function (e) {
            e.preventDefault()
        });

        $(document).ready(function() {
            var elementPosition = $('.toolbar').offset();
            if (elementPosition) {
                $(window).scroll(function() {
                        if($(window).scrollTop() > elementPosition.top){
                            $('.toolbar').css('left', '0px');
                            $('.toolbar').css('padding', '10px');
                            $('.toolbar').css('margin-left', '200px');
                            $('.toolbar').css('width', 'calc(100% - 200px)');
                            $('.toolbar').css('background', 'rgb(255, 255, 255)');
                            $('.toolbar').css('position', 'fixed').css('top', '0');
                            $('.toolbar').css('box-shadow', '1px 1px 5px #c8c8c8');
                        } else {
                            $('.toolbar').css('padding', '0');
                            $('.toolbar').css('left', 'auto');
                            $('.toolbar').css('width', 'auto');
                            $('.toolbar').css('position', 'static');
                            $('.toolbar').css('margin-left', 'auto');
                            $('.toolbar').css('background', 'initial');
                            $('.toolbar').css('box-shadow', 'none');
                        }    
                });
            }
        })

        /* Summernote de Descripcio */
        $(document).ready(function () {
            var clipboard = new ClipboardJS('button.copy', {
                text: function (trigger) {
                    return trigger.textContent;
                }
            });
            registerSummernote('.summernote', '@Lang('Leave a comment ')', 400, '{{ $summernote_lang }}');
        });
    </script>

    <script>
        function save_draft() {
            var form_aux = $('#PostForm');
            form_aux.find('input[type=hidden][name=status]').val('draft');
            form_aux.submit();
        }

        function publish_draft() {
            $('#PostForm').find('input[type=hidden][name=status]').val('publish');
            $('#PostForm').find('#SaveBtnSubmit').click();
        }
    </script>

    @includeIf('Admin::posts.media_modal')

    {{-- Declarem tots els espais per els scripts dels CF --}}
    @foreach ($cf_groups as $key => $cf_group)
        @foreach ($cf_group->fields as $custom_field)

        <!-- Scripts CF id: {{ $custom_field->id }}-->
        @yield("scripts-cf$custom_field->id")

        @if ( $custom_field->type == 'repeater' )
            @if( $repeater_children = $post->get_field($custom_field->name) )
                @foreach ( $repeater_children as $nrc => $r_child_fields )
                    @foreach ( $r_child_fields as $r_child_field )
                        @php ( $id_element_nrc = 'cf' . $r_child_field['id'] . '_k' . $nrc )
                        <!-- Scripts CF id: {{ $custom_field->id }} repeater field id: {{ $r_child_field['id'] }} num: {{ $nrc }}-->
                        @yield("scripts-$id_element_nrc")
                    @endforeach
                @endforeach
            @endif
        @endif

        @endforeach
    @endforeach
@endsection