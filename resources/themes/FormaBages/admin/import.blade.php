@extends('Admin::layouts.admin')

@section('section-title')
    @lang('Import')
@endsection

@section('content')
    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">library_books</i>
                </div>
                <h4 class="card-title">
                    @Lang('Imports')
                    {{-- 
                    @if ( $status != 'trash')
                        <a href='{{ route($section_route . ".create", ["post_type" => $post_type]) }}' class="btn btn-primary btn-sm">@Lang('Add')</a>
                    @endif
                     --}}
                </h4>
            </div>
            <div class="card-body">
                <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>@Lang('Curso')</th>
                                <th>@Lang('Identificador Curs')</th>
                                <th>@Lang('Tipus Formació')</th>
                                <th>@Lang('Especialitat Formativa')</th>
                                <th>@Lang('Centre de Formació')</th>
                            </tr>
                        </thead>
                        <tbody>
                        {{--
                            @foreach($items as $key => $item)
                            <tr data-post_id="{{ $item->id }}">
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->post_name }}</td>
                                <td>@if ($item->updated_at) {{ $item->updated_at->format('d/m/Y') . ' ' . $item->updated_at->format('H:i') }} @endif</td>
                            </tr>
                            @endforeach
                        --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(function() {

        fetch("https://www.oficinadetreball.gencat.cat/opendata/recursos/ofertaCursos.json")
            .then((response) => response.json())
            .then(function(data) {
                $("#progress").attr("max", (data.length))
                data.forEach(function(el, i) {
                    $("#progress").attr("value", i);
                    $("table.table tbody").append(`
                        <tr>
                            <td>${el.curs}</td>
                            <td>${el.identificador_curs}</td>
                            <td>${el.tipus}</td>
                            <td>${el.especialitat_formativa}</td>
                            <td>${el.nom_centre}</td>
                        </tr>
                    `);
                });
                $.ajax({
                    url: '{{ route("admin.insert") }}',
                    method: "POST",
                    data: { elements: data },
                    success: function(result) {
                        console.log(result);
                    }
                });
            })
    });
</script>
@endsection