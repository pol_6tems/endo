<!-- Titol -->
<div class="pt-3 flex-row ">
    <label class="col-md-12">@Lang('Title')</label>
    <div class="col-md-12">
        <div class="form-group has-default">
            <input name="{{$current_lang}}[title]" type="text" class="form-control" value="{{ $post->translate($current_lang, true)->title }}" tabindex=1 {{ ($current_lang == $language_code) ? 'required' : '' }}>
        </div>
    </div>
</div>

<!-- URL -->
<div class="pt-3 flex-row ">
    <label class="col-md-12">@Lang('URL')</label>
    <div class="col-md-12">
        <div class="form-group has-default slug">
            @php($plural = (!in_array($post_type, ['page'])) ? strtolower($post_type_plural) .'/' : '')
            @php($url = Request::getHost() . '/' . $current_lang . '/' . str_slug($plural))
            <span for="slug-id-{{ $current_lang }}" class="slug-label">{{ $url }}</span>
            <input name="{{ $current_lang }}[post_name]" type="text" class="form-control" value="{{ $post->translate($current_lang, true)->post_name }}" tabindex=2 {{ ($current_lang == $language_code) ? 'required' : '' }}>
        </div>
    </div>
</div>

@if( in_array($post->type, ['post', 'page']) || ($params && $params->content) )
<!-- WYSIWYG EDITOR -->
<div class="pt-3 flex-row  {{ ($current_lang != $language_code) ? 'summernote-not-first' : '' }}">
    <label class="col-md-12">@Lang('Description')</label>
    <div class="col-md-12">
        <div class="form-group has-default">
        <textarea id="description_{{ $current_lang }}" data-lang="{{ $current_lang }}" name="{{ $current_lang }}[description]" class="form-control summernote" tabindex=3>{!! $post->translate($current_lang, true)->description !!}</textarea>
        <div id="descripcio_count_{{ $current_lang }}"></div>
        </div>
    </div>
</div>
@endif

@foreach ($cf_groups as $key => $cf_group)
    @if($cf_group->position == 'main' && !$cf_group->fields->isEmpty())
        @if (Auth::user()->isAdmin())
        <div class="flex-row mt-4" style="justify-content: flex-end;">
            <div class="header-right">
                <a href="{{ route('admin.custom_fields.edit', $cf_group->id) }}?post_type={{$post_type}}" target="_blank" rel="tooltip">
                    <i class="material-icons">settings</i>
                </a>
            </div>
        </div>
        @endif

        <div id="cf_group{{ $cf_group->id }}">
            <div class="custom_field_group_fields">
                @foreach ($cf_group->fields as $k => $custom_field)
                    @php($viewParams = [
                        'title' => $custom_field->title,
                        'name' => "custom_fields[".$current_lang."][".$custom_field->id . "]",
                        'value' => $post->get_field( $custom_field->name, $current_lang ),
                        'params' => json_decode($custom_field->params),
                        'position' => $cf_group->position,
                        'custom_field' => $custom_field,
                        'order' => $k,
                        'lang' => $current_lang,
                    ])

                    @if (View::exists('Admin::default.custom_fields.' . $custom_field->type))
                        <!-- General -->
                        @includeIf('Admin::default.custom_fields.' . $custom_field->type, $viewParams)
                    @else
                        @php($module = explode(".", $custom_field->type)[0])
                        @php($field = explode(".", $custom_field->type)[1])
                        @includeIf('Modules::'.$module.'.resources.views.custom_fields.'.$field, $viewParams)
                    @endif
                @endforeach
            </div>
        </div>
    @endif
@endforeach