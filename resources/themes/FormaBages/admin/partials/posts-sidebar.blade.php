{{--
    Aqui només mostrarem els camps que han d'estar dins la barra lateral

    Els camps que es mostren són:
    -   Data Publicació
    -   Autor
    -   Parent
    -   Template
    -   Imatge
    -   Custom fields Groups amb posició (sidebar)

--}}
@if ( $sidebar )
<div id="sidebar" class="col-md-4" style="padding: 25px 10px;">
    @if (in_array($post_type, ['post', 'page']) || (isset($params) && ($params->imatge || $params->content || $params->author || $params->parent || $params->published)))
    <div class="card" style="margin-top: 5px;">
        <div class="col-lg-12">
            @if( ( !empty($post) && in_array($post->type, ['post', 'page']) && ($params && isset($params->published) && $params->published) ))
            <div class="flex-row">
                <div class="col-lg-12 col-md-12">
                    <label class="col-form-label">@Lang('Published Date')</label>
                    <div class="form-group">
                        <strong>{{ $post->created_at->format('H:m @ d/m/Y') }}</strong>
                    </div>
                </div>
            </div>
            @endif
            
            @if(in_array($post_type, ['post', 'page']) || ($params && isset($params->author) && $params->author) )
            <div class="flex-row">
                <div class="col-lg-12 col-md-12">
                    <label class="col-form-label">@Lang('Author')</label>
                    <div class="form-group">
                        @if (!Auth::user()->isAdmin())
                            <div class="btn btn-primary" style="width:100%">
                            @if ( !isset($post) || (isset($post) && !$post->author) )
                                {{ Auth::user()->name . ' ' . Auth::user()->lastname }}
                            @else
                                {{ $post->author->name . ' ' . $post->author->lastname }}
                            @endif
                            </div>
                        @else
                            <select class="selectpicker" data-size="7" name="author_id" data-style="btn btn-primary" title="@Lang('Author')" data-live-search="true">
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}" {{ ( !isset($post) && $user->id == Auth::id() ) ? 'selected' : ((isset($post) && $user->id == $post->author_id) ? 'selected' : '') }}>{{ $user->name . ' ' . $user->lastname }}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            
            {{--
            @if( in_array($post->type, ['post', 'page']) || ($params && isset($params->author) && $params->author) )
            <div class="flex-row">
                <div class="col-lg-12 col-md-12">
                    <label class="col-form-label">@Lang('Author')</label>
                    <div class="form-group">
                        @php($users = \App\User::all())
                        <select class="selectpicker" data-size="7" name="author_id" data-style="btn btn-primary" title="@Lang('Author')" data-live-search="true">
                            <option value="0" {{ ($post->author_id && $post->author_id == 0) ? 'selected' : '' }}>@Lang('No Author')</option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ ($post->author_id == $user->id) ? 'selected' : '' }}>{{ $user->name }}&nbsp;{{ $user->lastname }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            @endif
            --}}

            @if ( $post_type == 'page')
                <!-- Parent -->
                @if( (in_array($post_type, ['post', 'page']) || ($params && isset($params->parent) && $params->parent) ))
                <div class="flex-row">
                    <div class="col-lg-12 col-md-12">
                        <label class="col-form-label">@Lang('Parent')</label>
                        <div class="form-group">
                            @php($pagines = \App\Post::where('type', 'page')->get())
                            <select class="selectpicker" data-size="7" name="{{ $current_lang }}[parent_id]" data-style="btn btn-primary" title="@Lang('Template')" data-live-search="true">
                                <option value="0" {{ (!empty($post) && $post->parent_id == 0) ? 'selected' : '' }}>@Lang('No Parent')</option>
                                <optgroup label="@Lang('Pages')">
                                    @foreach ($pagines as $pagina)
                                        @php($p_trans = $pagina->translate( \App::getLocale(), true ))
                                        <option value="{{ $pagina->id }}" {{ (!empty($post) && $pagina->id == $post->parent_id) ? 'selected' : '' }}>{{ $p_trans->title }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
                @endif
                
                <!-- Template -->
                <div class="flex-row">
                    <div class="col-lg-12 col-md-12">
                        <label class="col-form-label">@Lang('Template')</label>
                        <div class="form-group">
                            <select class="selectpicker" data-size="7" name="{{ $current_lang }}[template]" data-style="btn btn-primary" title="@Lang('Template')" data-live-search="true" tabindex=4>
                                @foreach ($templates as $template)
                                <option value="{{$template}}" {{ ( !empty($post) && $post->template == $template ) ? 'selected' : ''}}>
                                    {{ ucfirst(strtolower($template)) }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            @endif
        </div>

        @php ( $post_media = (!empty($post)) ?  $post->translate($current_lang, true)->media : null)
        @php ( $has_thumbnail = !empty($post_media) && $post_media->has_thumbnail() )
        @if( in_array($post_type, ['post', 'page']) || ($params && $params->imatge) )
            <!-- Imatge -->
            <div class="flex-row">
                <div class="form-group col-md-12">
                    <label class="col-md-12 col-form-label">{{ isset($post_type_title) ? __(':post_type image', ['post_type' => $post_type_title]) : __('Image')}}</label>
                    <div class="fileinput miniatura {{ $has_thumbnail ? 'fileinput-exists' : 'fileinput-new' }} text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                            <img src="{{ asset('images/image_placeholder.jpg') }}">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="">
                            @if ( $has_thumbnail )
                                <img src="{{ $post_media->get_thumbnail_url('medium') }}" >
                            @endif
                        </div>
                        <div>
                            <span class="btn btn-rose btn-round btn-file btn-sm edit-media">
                                {{-- <span class="fileinput-new">@Lang('Select image')</span> --}}
                                <span class="fileinput-new"><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
                                {{-- <span class="fileinput-exists">@Lang('Change')</span> --}}
                                <span class="fileinput-exists"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                {{-- <input type="hidden"><input class="media_input" type="hidden" name="{{$current_lang}}[media_id]" value="{{ ( !empty($post) ) ? $post->translate($current_lang, true)->media_id : null }}"> --}}
                                <input type="hidden"><input class="media_input" type="hidden" name="{{ $current_lang }}[media_id]" value="{{ ( !empty($post) ) ? $post->translate($current_lang, true)->media_id : null }}">
                                <div class="ripple-container"></div>
                            </span>
                            <a href="#" class="btn btn-danger btn-round fileinput-exists btn-sm remove-media" data-dismiss="fileinput">
                                {{-- <i class="fa fa-times"></i> @Lang('Delete') --}}
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    @endif

    @foreach ($cf_groups as $key => $cf_group)
        @if($cf_group->position == 'r-sidebar' && !$cf_group->fields->isEmpty())
        <div class="flex-row">
            <article class="card pm" style="margin-top:5px;">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="flex-row">
                        <h4 class="card-title">@Lang($cf_group->title)</h4>
                        <a data-toggle="collapse" aria-expanded="true" href="#cf_group-{{ $current_lang }}-{{ $cf_group->id }}"><i class="material-icons">keyboard_arrow_down</i></a>
                        @if(Auth::user()->isAdmin())
                        <a href="{{ route('admin.custom_fields.edit', ['custom_field' => $cf_group->id, 'post_type' => $post_type]) }}" target="_blank" rel="tooltip">
                            <i class="material-icons">settings</i>
                        </a>
                        @endif
                    </div>
                </div>
                <div id="cf_group-{{ $current_lang }}-{{ $cf_group->id }}" class="collapse show">
                    <div class="card-body">
                        @foreach ($cf_group->fields as $custom_field)
                            @php($viewParams = [
                                'title' => $custom_field->title,
                                'name' => "custom_fields[".$current_lang."][".$custom_field->id . "]",
                                'value' => ( !empty($post) ) ? $post->get_field( $custom_field->name, $current_lang ) : null,
                                'params' => json_decode($custom_field->params),
                                'position' => $cf_group->position,
                            ])

                            @if (View::exists('Admin::default.custom_fields.' . $custom_field->type))
                                <!-- General -->
                                @includeIf('Admin::default.custom_fields.' . $custom_field->type, $viewParams)
                            @else
                                @php($module = explode(".", $custom_field->type)[0])
                                @php($field = explode(".", $custom_field->type)[1])
                                @includeIf('Modules::'.$module.'.resources.views.custom_fields.'.$field, $viewParams)
                            @endif
                        @endforeach
                    </div>
                </div>
            </article>
        </div>
        @endif
    @endforeach
</div>
@endif