@extends('Front::layouts.base-intranet')

@section('content')
    <div class="form">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <img src="{{ asset($_front.'images/logo.png') }}" alt="Forma Bages">
            <h3>@lang('Gestió de Cursos')</h3>
            <input type="text" name="email" class="frm-ctrl" placeholder="Email" value="{{ old('email') }}" required autofocus>
            <div class="clear"></div>
            <div class="password-pad">
                <input type="password" name="password" class="frm-ctrl" placeholder="@lang('Contrasenya')" required>
            </div>
            <div class="checkbox">
                <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="remember">@lang('Recordar contrasenya')</label>
            </div>
            <div class="btn-container">
                <button type="submit" class="mt10">@lang('Accedir')</button>
            </div>

            <div class="clear"></div>

            @php ($intendedUrl = Session::get('url.intended'))
            @if ($intendedUrl && strpos($intendedUrl, '/admin') !== false)
                <p class="mt10"><a href="{{ route('password.request') }}">@lang('Recuperar clau')</a></p>
            @endif
        </form>
    </div>
@endsection