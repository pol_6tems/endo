<header class="inner-header">
    <div class="main-menu">
        <div class="row">
            <div class="logo-mobile">
                <a href="{{ get_page_url('home') }}">
                    <h1 class="logo">forma<span style="color:#FB6262">BAGES</span></h1>
                </a>
            </div>
            <div class="menu">
                <div id="smoothmenu" class="ddsmoothmenu">
                    <ul>
                        <li><a href="{{ get_page_url('home') }}" class="{{ (get_page_url('home') == url()->current()) ? 'active' : '' }}">@lang('Inici')</a></li>
                        <li><a href="{{ get_page_url('empreses') }}" class="{{ (get_page_url('empreses') == url()->current()) ? 'active' : '' }}">@lang('Tipus Formació')</a></li>
                        <li><a href="{{ get_archive_link('curso') }}" class="{{ (get_archive_link('curso') == url()->current()) ? 'active' : '' }}">@lang('Cursos')</a></li>
                        <li><a href="{{ get_archive_link('agenda') }}" class="{{ (get_archive_link('agenda') == url()->current()) ? 'active' : '' }}">@lang('Agenda')</a></li>
                        <li><a href="{{ get_archive_link('noticia') }}" class="{{ (get_archive_link('noticia') == url()->current()) ? 'active' : '' }}">@lang('Notícies')</a></li>
                        <li><a href="{{ get_page_url('recursos') }}" class="{{ (get_page_url('recursos') == url()->current()) ? 'active' : '' }}">@lang('Recursos')</a></li>
                        <li><a href="{{ get_page_url('documentacio') }}" class="{{ (get_page_url('documentacio') == url()->current()) ? 'active' : '' }}">@lang('Documentació')</a></li>
                        <li><a href="{{ get_page_url('tipus-de-perfil') }}" class="{{ (get_page_url('tipus-de-perfil') == url()->current()) ? 'active' : '' }}">@lang('Perfils')</a></li>
                        <li><a href="{{ get_page_url('qui-som') }}" class="{{ (get_page_url('qui-som') == url()->current()) ? 'active' : '' }}">@lang('Qui som')</a></li>
                        <li><a href="{{ get_page_url('contacte') }}" class="{{ (get_page_url('contacte') == url()->current()) ? 'active' : '' }}">@lang('Contactar')</a></li>
                    </ul>
                </div>
            </div>
            <!-- mobile nav starts-->
            <div class="m-menu"><a href="javascript:void(0);" id="hie_menu"> <span></span> <span></span> <span></span></a></div>
            <!-- mobile nav ends-->
            <div class="search-div"> <a href="javascript:void(0);" id="search-icon"></a></div>
        </div>
        <section class="inner-search">
            <div class="row">
                <div class="logo-inner">
                    <a href="{{ get_page_url('home') }}"> <img src="{{ asset($_front.'images/logo-white.png') }}" alt="" border="0"> </a>
                </div>
                @includeIf('Front::partials.buscador')
            </div>
        </section>
    </div>
</header>