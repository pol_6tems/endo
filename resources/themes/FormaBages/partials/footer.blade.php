@if (strpos(route_name(), 'intranet.') === false)
@php ($newsletter = \App\Post::ofName('newsletter')->ofType('page')->first())
<!--Newsletter Box Starts-->
<section class="newsletter-box">
    <div class="row">
        <div class="lft-news">
            <h2>
                @if ($newsletter && $newsletter->get_field('titol-newsletter'))
                {{ $newsletter->get_field('titol-newsletter') }}
                @else
                @lang('Estigues al corrent de tot!')
                @endif

                <span>
                    @if ($newsletter && $newsletter->get_field('subtitol-newsletter'))
                    {{ $newsletter->get_field('subtitol-newsletter') }}
                    @else
                    @lang('Subscriu-te a la nostra newsletter')
                    @endif
                </span>
            </h2>
        </div>
        <div class="rht-news">
            <form id="NewsletterForm">
                <div class="form-response" style="display:none;"></div>
                <div class="loader" style="display:none;"></div>

                <div class="subscribe">
                    <input type="email" placeholder="@lang('El teu correu electrònic')" required>
                    <input type="submit" value="Enviar">
                </div>
                <div class="check">
                    <input id="options" type="checkbox" name="field" value="options" required>
                    <label for="options">
                        <span></span>
                        <p><a href="{{ get_page_url('politica-de-privacitat') }}" target="_blank">@lang('Accepto la política de privacitat')</a></p>
                    </label>
                </div>
            </form>
        </div>
    </div>
</section>
<!--Newsletter Box Ends-->
<script>
    window.addEventListener('load', function(event) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#NewsletterForm').submit(function(e) {
            e.preventDefault();
            var email = $(this).find('input[type=email]').val();
            $('.form-response').hide();
            $('.loader').show();
            $('.form-response').html(null);
            $.ajax({
                url: "{{route('newsletter.subscribe')}}",
                method: 'POST',
                data: {
                    '_token': '{{csrf_token()}}',
                    'email': email
                },
                success: function(data) {
                    $('.form-response').html(`<div class="notice notice-success"><p>Subscrit correctament.</p></div>`)
                    $('.loader').hide();
                    $('.form-response').show();
                }
            });
            return false;
        });
    });
</script>
<style>
    .subscribe input[type=submit]:hover {
        background: #393939;
        color: #fff;
    }

    .subscribe input[type=submit] {
        font-family: barlowmedium;
        background: #555;
        color: #fff;
        font-size: 16px;
        line-height: 34px;
        letter-spacing: .5px;
        padding: 6px 50px;
        border-radius: 2px;
        -moz-border-radius: 2px;
        -webkit-border-radius: 2px;
        transition: all .5s ease;
        -moz-transition: all .5s ease;
        -webkit-transition: all .5s ease;
        -o-transition: all .5s ease;
        display: inline-block;
        border: 0 solid #fff;
        cursor: pointer;
    }

    .newsletter-box .check {
        position: relative;
    }

    .check input[type=checkbox]:not(old) {
        display: block;
        top: 58px;
        left: 0px;
    }

    .subscribe input[type=email] {
        float: left;
        width: 70%;
        border: none;
        background: #fff;
        font-size: 16px;
        font-family: barlowregular;
        color: #393939;
        padding: 0 10px;
        height: 46px;
    }

    /* LOADER */
    .loader {
        width: 50px;
        height: 50px;
        border: 10px solid #f3f3f3;
        border-top: 10px solid #3498db;
        border-radius: 50%;
        animation: spin 2s linear infinite;
        margin-left: auto;
        transition: all .5s ease;
        -moz-transition: all .5s ease;
        -webkit-transition: all .5s ease;
        -o-transition: all .5s ease;
        margin: auto;
        margin-top: 40px;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    /* end LOADER */

    /* NOTIFICACIONS */
    .notice {
        display: inline-block;
        line-height: 19px;
        padding: 11px 15px;
        font-size: 14px;
        text-align: left;
        background-color: #fff;
        /*border: 1px solid #ffba00;*/
        border-left: 4px solid #ffba00;
        box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, .1);
        width: 99%;
        margin: 5px 0px 5px 2px;
        margin-bottom: 20px;
    }

    .notice p {
        padding: 0px 5px;
        font-size: 14px;
        margin: 0 !important;
        color: #000 !important;
    }

    .notice.notice-info {
        border-color: #00a0d2;
    }

    .notice.notice-warning {
        border-color: #ffb900;
    }

    .notice.notice-success {
        border-color: #46b450;
    }

    .notice.notice-error {
        border-color: #dc3232;
    }

    /* /NOTIFICACIONS */
</style>
@endif

<!-- footer starts-->
@php ($footer = \App\Post::ofName('footer')->ofType('page')->first())
<footer>
    <div class="row">
        <div class="foot-top">
            <ul>
                <li><a href="{{ get_page_url('home') }}"><img src="{{ asset($_front.'images/logo.png') }}" alt="" border="0"></a><br>
                    <p>
                        @if ($footer && $footer->get_field('subtitol-sota-logo'))
                        {!! $footer->get_field('subtitol-sota-logo') !!}
                        @else
                        @lang('Àrea de Desenvolupament Comarcal<br>
                        del Consell Comarcal del Bages')
                        @endif
                    </p>
                </li>
                <li>
                    <h3>
                        @if ($footer && $footer->get_field('titol-bloc-central'))
                        {{ $footer->get_field('titol-bloc-central') }}
                        @else
                        @lang('Entitat impulsora')
                        @endif
                    </h3>
                    <a href="{{ $footer && $footer->get_field('url-bloc-central') ? $footer->get_field('url-bloc-central') : 'https://www.ccbages.cat/' }}" target="_blank"><img src="{{ $footer && $footer->get_field('imatge-bloc-central') ? $footer->get_field('imatge-bloc-central')->get_thumbnail_url() : asset($_front.'images/img-footer1.png') }}" alt="" border="0"></a>
                </li>
                <li>
                    <h3>
                        @if ($footer && $footer->get_field('titol-bloc-suport'))
                        {{ $footer->get_field('titol-bloc-suport') }}
                        @else
                        @lang('Amb el suport de')
                        @endif
                    </h3>
                    <ul>
                        @if ($footer && $footer->get_field('entitats') && count($footer->get_field('entitats')))
                        @foreach($footer->get_field('entitats') as $entitat)
                        <li><a href="{{ $entitat['url']['value'] }}" target="_blank"><img src="{{ $entitat['logo']['value']->get_thumbnail_url() }}" alt="" border="0"></a></li>
                        @endforeach
                        @else
                        <li><a href="https://www.diba.cat/es" target="_blank"><img src="{{ asset($_front.'images/img-footer2.png') }}" alt="" border="0"></a></li>
                        <li><a href="https://serveiocupacio.gencat.cat/ca/inici/" target="_blank"><img src="{{ asset($_front.'images/img-footer3.png') }}" alt="" border="0"></a></li>
                        <li><a href="http://www.mitramiss.gob.es/" target="_blank"><img src="{{ asset($_front.'images/img-footer4.png') }}" alt="" border="0"></a></li>
                        <li><a href="https://web.gencat.cat/ca/inici" target="_blank"><img src="{{ asset($_front.'images/img-footer5.png') }}" alt="" border="0"></a></li>
                        @endif
                    </ul>
                </li>
            </ul>
        </div>
        <div class="foot-btm">
            <ul>
                <li><a href="{{ get_page_url('contacte') }}">@lang('Contactar')</a></li>
                <li><a href="{{ get_page_url('politica-de-privacitat') }}">@lang('Política de privacitat')</a></li>
                <li><a href="{{ get_page_url('nota-legal') }}">@lang('Nota Legal')</a></li>
                <li><a href="{{ get_page_url('politica-de-cookies') }}">@lang('Política de Cookies')</a></li>
                <li><a href="{{ route('intranet.index') }}" target="_blank">@lang('Àrea privada')</a></li>
            </ul>
        </div>
    </div>
</footer>