<li>
    @php($centre = $curs->get_field('centre'))
    <a href="{{ $curs->get_url() }}">
        <h2>{{ $curs->title }}</h2>
        <div class="curs-rel-list">
            <ul>
                @if ($data_inici = $curs->get_field('data-inici-del-curs'))
                    <li class="date">{{ print_course_date($curs, $data_inici, false) }}</li>
                @endif
                
                @if ($centre && $poblacion = $centre->get_field('poblacion') )
                    <li class="location">{{ $poblacion->title }}</li>
                @endif
            </ul>
            @if ($centre)
                <h3><span>@lang('Organitza:')</span> {{ centre_title($centre->title) }}</h3>
            @endif
        </div>
        <p>{{ strip_tags(get_excerpt( $curs->get_field('introduccio'), 10)) }}</p>
        <span class="info-link">@lang('Més informació')</span>
    </a>
</li>