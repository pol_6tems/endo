<h1>@lang('Filtra per')</h1>
<div class="acc-menu">
    <div class="accordion_example1">
        <!-- Section 1 -->
        <div class="accordion_in acc_active">
            <div class="acc_head">@lang('Tipus de formació')</div>
            @php($hidden = $tipos_formacion->splice(10))
            <div class="acc_content">
                <ul id="tipus_de" class="cat-list">
                    @foreach($tipos_formacion as $tipo)
                        @php($count = isset($tipo->count_courses) ? $tipo->count_courses : countCourses("tipus-formacio", $tipo))
                        <li data-cf="tipus-formacio" data-post_name="{{ $tipo->post_name }}" data-cfid="{{ $tipo->id }}">
                            <div class="input-container">
                                <span class="right">{{ $count }}</span>
                                <label>{{ $tipo->title }}</label>
                            </div>
                            @if ($tipo->subtipus_formacio->count())
                                <ul class="child subtipus">
                                    @foreach($tipo->subtipus_formacio as $subtipus)
                                        <li data-cf="subtipus-formacio" data-post_name="{{ $subtipus->post_name }}" data-cfid="{{ $subtipus->id }}">
                                            <div class="input-container">
                                                <span class="right">{{ $subtipus->count_courses }}</span>
                                                <label>{{ $subtipus->title }}</label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
                <ul class="cat-list" id="vi-course-1">
                    @foreach($hidden as $tipo)
                        @php($count = isset($tipo->count_courses) ? $tipo->count_courses : countCourses("tipus-formacio", $tipo))
                        <li data-cf="tipus-formacio" data-post_name="{{ $tipo->post_name }}" data-cfid="{{ $tipo->id }}">
                            <div class="input-container">
                                <span class="right">{{ $count }}</span>
                                <label>{{ $tipo->title }}</label>
                            </div>
                            @if ($tipo->subtipus_formacio->count())
                                <ul class="child subtipus">
                                    @foreach($tipo->subtipus_formacio as $subtipus)
                                        <li data-cf="subtipus-formacio" data-post_name="{{ $subtipus->post_name }}" data-cfid="{{ $subtipus->id }}">
                                            <div class="input-container">
                                                <span class="right">{{ $subtipus->count_courses }}</span>
                                                <label>{{ $subtipus->title }}</label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
                @if (! $hidden->isEmpty())
                    <br><a href="javascript:void(0);" class="view" id="v-1">@lang('Veure més')</a>
                @endif
            </div>
        </div>
        <!-- Section 2 -->
        <div class="accordion_in">
            <div class="acc_head">@lang('Famílies Professionals')</div>
            @php($hidden = $familia_formativa->splice(10))
            <div class="acc_content">
                <ul id="arees" class="cat-list">
                    @foreach($familia_formativa as $area)
                        @php($count = isset($area->count_courses) ? $area->count_courses : countCourses("familia-formativa", $area))
                        <li data-cf="familia-formativa" data-post_name="{{ $area->post_name }}" data-cfid="{{ $area->id }}">
                            <div class="input-container">
                                <span class="right">{{ $count }}</span>
                                <label>{{ $area->title }}</label>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <ul class="cat-list" id="vi-course-2">
                    @foreach($hidden as $area)
                        @php($count = isset($area->count_courses) ? $area->count_courses : countCourses("familia-formativa", $area))
                        <li data-cf="familia-formativa" data-post_name="{{ $area->post_name }}" data-cfid="{{ $area->id }}">
                            <div class="input-container">
                                <span class="right">{{ $count }}</span>
                                <label>{{ $area->title }}</label>
                            </div>
                        </li>
                    @endforeach
                </ul>
                @if (! $hidden->isEmpty())
                    <br><a href="javascript:void(0);" class="view" id="v-2">@lang('Veure més')</a>
                @endif
            </div>
        </div>
        <!-- Section 3 -->
        <div class="accordion_in">
            <div class="acc_head">@lang('Població')</div>
            @php($hidden = $poblacions->splice(10))
            <div class="acc_content">
                <ul id="poblacio" class="cat-list">
                    @foreach($poblacions as $poblacio)
                        @if($poblacio->title == "") @continue @endif
                        @php($count = isset($poblacio->count_courses) ? $poblacio->count_courses : countCourses("localitzacio", $poblacio))
                        <li data-cf="localitzacio" data-post_name="{{ $poblacio->post_name }}" data-cfid="{{ $poblacio->id }}">
                            <div class="input-container">
                                <span class="right">{{ $count }}</span>
                                <label>{{ mayusWords($poblacio->title, ["de"]) }}</label>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <ul class="cat-list" id="vi-course-3">
                    @foreach($hidden as $poblacio)
                        @if($poblacio->title == "") @continue @endif
                        @php($count = isset($poblacio->count_courses) ? $poblacio->count_courses : countCourses("localitzacio", $poblacio))
                        <li data-cf="localitzacio" data-post_name="{{ $poblacio->post_name }}" data-cfid="{{ $poblacio->id }}">
                            <div class="input-container">
                                <span class="right">{{ $count }}</span>
                                <label>{{ mayusWords($poblacio->title, ["de"]) }}</label>
                            </div>
                        </li>
                    @endforeach
                </ul>
                @if (! $hidden->isEmpty())
                    <br><a href="javascript:void(0);" class="view" id="v-3">@lang('Veure més')</a>
                @endif
            </div>
        </div>
        @if (isset($modalitats))
            <!-- Section 4 -->
            <div class="accordion_in">
                <div class="acc_head">@lang('Característiques')</div>
                <div class="acc_content">
                    <ul id="varacter" class="cat-list">
                        @foreach($modalitats as $key => $modalitat)
                            @php($count = isset($modalitat->count_courses) ? $modalitat->count_courses : countCourses("modalitat", $key))
                            <li data-cf="modalitat" data-post_name="{{ $key }}" data-cfid="{{ $modalitat }}">
                                <div class="input-container">
                                    <span class="right">{{ $count }}</span>
                                    <label>{{ $modalitat }}</label>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
</div>