<form action="{{ get_archive_link('curso') }}" method="GET">
    <div class="bsearch">
        <ul>
            <li>
                <input id="txt-filter" title="@lang('Minimo 3 characteres')" type="text" name="search_query" placeholder="Què vols estudiar?" value="{{ isset($value) ? $value : $search_query }}" pattern=".{3,}">
            </li>
            <li>
                <select name="tipus-formacio" class="select_box">
                    <option value="tots" {{ (isset($search_formacio) && 'tots' == $search_formacio) ? 'selected' : '' }}>@lang('Tots')</option>
                    @foreach($tipus as $tipu)
                        <option value="{{ $tipu->post_name }}" {{ (isset($search_formacio) && $tipu->post_name == $search_formacio) ? 'selected' : '' }}>{{ $tipu->title }}</option>
                    @endforeach
                </select>
            </li>
            <li>
                <input type="submit" value="Buscar">
            </li>
        </ul>
    </div>
</form>