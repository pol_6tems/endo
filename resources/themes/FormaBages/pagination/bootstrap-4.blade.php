@if ($paginator->hasPages())
<div class="pagination">
    <ul>
        {{-- Previous Page Link --}}
        @if ($paginator->currentPage() > 1)
            <li class="first"><a href="{{ $paginator->previousPageUrl() }}"><i class="arrow left"></i></a></li>
        @endif

        @if($paginator->currentPage() > 2)
            <li><a href="{{ $paginator->url(1) }}">1</a></li>
        @endif

        @if($paginator->currentPage() > 3)
            <li class="no-hover"><a href="javascript:void(0);">...</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if($page >= $paginator->currentPage() - 1 && $page <= $paginator->currentPage() + 1)
                        @if ($page == $paginator->currentPage())
                            <li><a class="active" href="{{ $url }}">{{ $page }}</a></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endif
                @endforeach
            @endif
        @endforeach

        @if($paginator->currentPage() < $paginator->lastPage() - 2)
            <li class="no-hover"><a href="javascript:void(0);">...</a></li>
        @endif

        @if($paginator->currentPage() < $paginator->lastPage() - 1)
            <li><a href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->currentPage() < $paginator->lastPage())
            <li class="last"><a href="{{ $paginator->nextPageUrl() }}"><i class="arrow right"></i></a></li>
        @endif
    </ul>
</div>
@endif
