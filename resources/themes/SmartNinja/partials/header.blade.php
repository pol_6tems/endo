<header class="@if($item->type == 'curso' || $item->id == 11)inner-top @endif">
    <div class="top-header"> 
        <div class="row">	 
            <div class="logo">
                <a href="https://smartninja.cursos-programacion-web-barcelona.es/es" title="Smart Ninja">
                    <img src="{{ asset('Themes/SmartNinja/images/logo.svg') }}" alt="Smart Ninja">
                </a>
            </div>
            <div class="top-rgt">
                <span>
                    @Lang('Llama e infórmate:')
                    <a href="tel:{{ str_replace(' ', '', get_option('telefono')) }}" alt="@Lang('Llama al') {{ get_option('telefono') }} @Lang('para informarte')">
                        {{ get_option('telefono') }}
                    </a>
                </span>
            </div>
        </div>  
    </div>    
</header>  