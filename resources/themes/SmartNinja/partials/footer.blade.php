<footer>
    <div class="row">
        <div class="copy-rights">
            <h4>
                <a href="{{ get_option('enlace_google_maps') }}" target="_blank">
                    {{ get_option('direccion') }}
                </a>
                <span class="line-brd">|</span>
                <span>
                    T
                    <a href="tel:{{ get_option('telefono') }}">{{ get_option('telefono') }}</a>
                </span>
            </h4>

            @if($item->get_field('cursos-footer') !== null && count($item->get_field('cursos-footer')) > 0)
                <section class="donde-encon donde-encon-links">
                    <ul>
                        @foreach($item->get_field('cursos-footer') as $cursFooter)
                            <li>
                                <a href="{{ $cursFooter['link']['value'] }}">
                                    {{ $cursFooter['titulo']['value'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </section>
            @endif
        </div>

        <ul>
            <li>
                <a href="{{ get_page_url('politica-de-privacidad-aviso-legal') }}">
                    @Lang('Política Privacidad & Aviso Legal')
                </a>
            </li>
            <li>
                <a href="javascript:void(0);">
                    @Lang('Cookies')
                </a>
            </li>
        </ul>

        <ul class="social">
            <li>
                <a href="{{ get_option('facebook') }}" target="_blank">
                    <img src="{{ asset('Themes/SmartNinja/images/fb.svg') }}" alt="">
                </a>
            </li>
            <li>
                <a href="{{ get_option('linkedin') }}" target="_blank">
                    <img src="{{ asset('Themes/SmartNinja/images/in.svg') }}" alt="">
                </a>
            </li>
        </ul>
        
    </div>
</footer> 