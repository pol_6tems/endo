@extends('Front::layouts.base')

@section('content')
    <section class="page-content">
        <div class="row">
            <h1>{{ $item->title }}</h1>
            {!! $item->description !!}
        </div>
    </section>
@endsection