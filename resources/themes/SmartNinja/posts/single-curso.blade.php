@extends('Front::layouts.base')

@section('content')

<section class="curso-presencial">
    <div class="row">	
        <h1>
            @if($item->get_field('curso-presencial') == true)
                @Lang('Curso Presencial')
            @elseif($item->get_field('curso-online') == true)
                @Lang('Curso Online')
            @endif
            “{{ $item->title }}”
        </h1>

        <div class="curso-cont">
            <div class="curso-lft">
                <div id="partleftMob"></div>
                    <ul>
                        <li>
                            <div class="img-ico">
                                <img src="{{ asset('Themes/SmartNinja/images/curso-01.svg') }}" alt="@Lang('Fechas')">
                            </div>
                            <div class="img-desc">
                                <h2>@Lang('Fechas')</h2>
                                @if($item->get_field('fecha-inicio') != null && $item->get_field('fecha-fin') != null)
                                    <h5>
                                        @Lang('Del') 
                                        {{ Date::createFromFormat('d/m/Y', $item->get_field('fecha-inicio'))->format('d') }}
                                        @Lang('de')
                                        {{ Date::createFromFormat('d/m/Y', $item->get_field('fecha-inicio'))->format('F') }}
                                        @Lang('al')
                                        {{ Date::createFromFormat('d/m/Y', $item->get_field('fecha-fin'))->format('d') }}
                                        @Lang('de')
                                        {{ Date::createFromFormat('d/m/Y', $item->get_field('fecha-fin'))->format('F') }}
                                        <br>
                                        {{ $item->get_field('horario') }}
                                    </h5>
                                @endif
                            </div>
                        </li>
                        <li>
                            <div class="img-ico">
                                    <img src="{{ asset('Themes/SmartNinja/images/curso-02.svg') }}" alt="@Lang('Precios')">
                            </div>
                            <div class="img-desc">
                                <h2>@Lang('Precios')</h2>
                                {!! $item->get_field('precios') !!}
                            </div>
                        </li>
                        <li>
                            <div class="img-ico">
                                <img src="{{ asset('Themes/SmartNinja/images/curso-03.svg') }}" alt="@Lang('Tipología')">
                            </div>
                            <div class="img-desc">
                                <h2>@Lang('Tipología')</h2>
                                <h5>{{ $item->get_field('tipologia') }}</h5>
                            </div>
                        </li>
                        <li>
                            <div class="img-ico">
                                <img src="{{ asset('Themes/SmartNinja/images/curso-04.svg') }}" alt="@Lang('Horas lectivas y duración')">
                            </div>
                            <div class="img-desc">
                            <h2>@Lang('Horas lectivas y duración')</h2>
                            <h5>{{ $item->get_field('horas-duracion') }}</h5>
                            </div>
                        </li>
                        <li>
                            <div class="img-ico">
                                <img 
                                    src="{{ asset('Themes/SmartNinja/images/curso-05.svg') }}" 
                                    alt="
                                        @if($item->get_field('curso-presencial') == true)
                                            @Lang('Curso Presencial')
                                        @elseif($item->get_field('curso-online') == true)
                                            @Lang('Curso Online')
                                        @endif
                                        @Lang('en Barcelona')
                                    "
                                />
                            </div>
                            <div class="img-desc">
                                <h2>
                                    @if($item->get_field('curso-presencial') == true)
                                        @Lang('Curso Presencial')
                                    @elseif($item->get_field('curso-online') == true)
                                        @Lang('Curso Online')
                                    @endif
                                    @Lang('en Barcelona')
                                </h2>
                            <h5>{{ $item->get_field('ubicacion') }}</h5>
                            </div>
                        </li>
                        <li>
                            <div class="img-ico">
                                <img src="{{ asset('Themes/SmartNinja/images/curso-06.svg') }}" alt="@Lang('Profesor/a')">
                            </div>
                            <div class="img-desc">
                                <h2>@Lang('Profesor/a')</h2>
                                {!! $item->get_field('profesor') !!}
                            </div>
                        </li>
                        <li>
                            <div class="img-ico">
                                <img src="{{ asset('Themes/SmartNinja/images/curso-07.svg') }}" alt="@Lang('Descripción del curso')">
                            </div>
                            <div class="img-desc">
                                <h2>@Lang('Descripción del curso')</h2>
                                {!! $item->get_field('descripcion') !!}
                            </div>
                        </li>
                        <li>
                            <div class="img-ico">
                                <img src="{{ asset('Themes/SmartNinja/images/curso-08.svg') }}" alt="@Lang('Testimonios')">
                            </div>
                            <div class="img-desc">
                                <h2>@Lang('Testimonios')</h2>
                                {!! $item->get_field('testimonios') !!}
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="curso-rgt">
                    <div class="curso-rgt-new">
                        <div class="curso-form">	
                            <h2>
                                @Lang('¡Pre-inscríbete')
                                <br> 
                                @Lang('(y paga más adelante)!')
                            </h2>
                            <p>
                                @Lang('La pre-inscripción en sí no es vinculante a ningún pago, pero de ésta manera te reservas el derecho al descuento EARLY BIRD y te informamos sobre los próximos pasos.')
                            </p>
                            <h3>
                                @Lang('¡Apúntate, es gratis') ;-)
                                <span class="mob-none">
                                    @Lang('y si sois 2, tenemos regalo*!')
                                </span>
                            </h3>
                            <form class="form-box" id="FormCurso">
                                <div class="frm-input">
                                    <div class="frm-input-ctrl">
                                    <input class="form-control" name="name" placeholder="Tu nombre" id="name" type="text" required>
                                    <div id="ab" class="err-msg"></div>
                                    </div>
                                </div>
                                <div class="frm-input">
                                    <div class="frm-input-ctrl">
                                    <input class="form-control" name="email" placeholder="Email Address" id="email" type="text" required>
                                    <div id="cd" class="err-msg"></div>
                                    </div> 
                                </div>
                                <div class="frm-input">
                                    <div class="frm-input-ctrl">
                                    <input class="form-control" name="phone" placeholder="Teléfono" id="phone" type="text" required>
                                    <div id="bc" class="err-msg"></div>
                                    </div>
                                </div>
                                <div class="frm-input checkbox">
                                    <input id="check7" name="check7" type="checkbox" required>
                                    <label for="check7">@Lang('Sí, acepto las condiciones de Smartninja')</label>
                                </div>
                                <div class="frm-input checkbox">
                                    <input id="check8" name="check8" type="checkbox">
                                    <label for="check8">@Lang('Sí, quiero recibir vuestro Newsletter con información sobre cursos nuevos, promociones y deadlines')</label>
                                </div>

                                <input type="hidden" value=" " name="lastname" />
                                <input type="hidden" value="{{ $item->title }}" name="curso" />
                                <input type="hidden" value="{{ $item->get_field('correo-curso') }}" name="to" />
							    <input type="hidden" value="4" name="email_id" />

                                <div class="resultContainer">
                                    <p class="resultMessage">
                                    </p>
                                </div>

                                <div class="btn-ctr">
                                    <button class="env-btn" type="submit">
                                        @Lang('Enviar')
                                    </button>
                                </div>
                            </form>
                        </div> 
                    </div>
                    <div class="curso-emag">
                        <h4>@Lang('Valoraciones de l@s alumn@s:')</h4>
                        <img src="{{ asset('Themes/SmartNinja/images/curso-emagister.jpg') }}"/>
                    </div> 
                </div>
            </div>
        </div>
    </section>
@endsection