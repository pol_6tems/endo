@extends('Front::layouts.base')

@section('content')
    <section class="top-banner">
        @php($cursos = get_posts(
            array (
                'post_type' => 'curso',
                'with' => 'metas'
            )))
        
        @php($numPreguntas = count($item->get_field('preguntas')))
        <div class="home-video">
            <video id="banner-video" width="100%" height="" loop autoplay muted preload="auto" poster="{{ asset('Themes/SmartNinja/images/home.jpg') }}" >
                <source src="{{ asset('Themes/SmartNinja/video/home.mp4') }}" type="video/mp4">
                <source src="{{ asset('Themes/SmartNinja/video/home.ogv') }}" type="video/ogv" />
                <source src="{{ asset('Themes/SmartNinja/video/home.ogg') }}" type="video/ogg">
                <source src="{{ asset('Themes/SmartNinja/video/home.webm') }}" type="video/webm">
                <em>@Lang('Sorry, your browser doesn\'t support HTML5 video.')</em> 
            </video>
        </div> 
        <div class="banner-cont">
            <div class="row">
                <div class="banner-top">  
                    <div class="banner-caption">
                        <h1 class="desk-tit">{{ $item->get_field('frase-promocional') }}</h1>
                        
                        @if (count($item->get_field('logos-programacion')) > 0)
                            <ul>
                                @foreach ($item->get_field('logos-programacion') as $logoProgramacion)
                                    <li>
                                        <a href="{{ $logoProgramacion['link']['value'] }}">
                                            <img src="{{ $logoProgramacion['imagen']['value']->get_thumbnail_url() }}" alt="{{ $logoProgramacion['titulo']['value'] }}">
                                        </a>
                                    </li>	
                                @endforeach
                            </ul>
                        @endif

                        <h1 class="mob-tit">{{ $item->get_field('frase-promocional') }}</h1>
                        <div class="mob-call">
                            <a class="call-btn" href="tel:93 362 10 64">¡LLÁMANOS AHORA!</a>
                        </div>

                        @if (count($item->get_field('puntos-promocionales')) > 0)
                            <ul class="cont-list">
                                @foreach ($item->get_field('puntos-promocionales') as $puntoPromocional)
                                    <li>{{ $puntoPromocional['texto']['value'] }}</li>
                                @endforeach
                            </ul>
                        @endif

                    </div>
                    <div class="banner-form">
                        <h1>@Lang('¿Qué te gustaría aprender?')</h1>
                        <form id="FormCursoHome" class="form-box">
                            <div class="frm-input dropdown">   
                                <select name="curso">
                                    <option value="">@Lang('Selecciona un curso')</option>
                                    @foreach($cursos as $curso)
                                        <option value="{{ $curso->title}}">{{ $curso->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="frm-input">
                                <div class="frm-input-ctrl">
                                <input class="form-control" name="name" value="" placeholder="Tu nombre" type="text" required>
                                </div>
                            </div>
                            <div class="frm-input">
                                <div class="frm-input-ctrl">
                                <input class="form-control" name="email" value="" placeholder="Email Address" type="text" required>
                                </div>
                            </div>
                            <div class="frm-input">
                                <div class="frm-input-ctrl">
                                <input class="form-control" name="phone" value="" placeholder="Teléfono" type="text" required>
                                </div>
                            </div>

                            <input type="hidden" value=" " name="lastname" />
                            <input type="hidden" value="{{ $item->get_field('correo-cursos') }}" name="to" />
							<input type="hidden" value="1" name="email_id" />
                            
                            <div class="frm-input checkbox">
                                <input id="check1" type="checkbox" name="check1" required>
                                <label for="check1">@Lang('Sí, acepto las condiciones de Smartninja')</label>
                            </div>
                            <div class="frm-input checkbox">
                                <input id="check2" type="checkbox" name="check2">
                                <label for="check2">@Lang('Sí, quiero recibir vuestro Newsletter con información sobre cursos nuevos, promociones y deadlines')</label>
                            </div>

                            <div class="resultContainer">
                                <p class="resultMessage">
                                </p>
                            </div>

                            <div class="btn-ctr"><button class="env-btn">@Lang('Enviar')</button></div>
                        </form>
                    </div>
                </div> 
            </div> 
        </div>
    </section>

    <section class="emagister">
        <div class="row">
            <div class="emag-img">
                <h4>@Lang('Valoraciones de l@s alumn@s:')</h4>
                <img src="{{ asset('Themes/SmartNinja/images/home-emagister.jpg') }}" />
            </div> 
        </div>
    </section>

    <section class="porque-apren">
        <div class="row">
            <h1>@Lang('¿Porque aprender con Smartninja?')</h1>
            <h2>@Lang('Más de 10.000 personas se convirtieron ya en Smartninjas, crearon Webs o Bases de<br> Datos y aprendieron a programar a través de uno o varios de nuestros cursos.')</h2>
            
            @if (count($item->get_field('bloques-porque-aprender')) > 0)
                <div class="porque-list">
                    <ul>
                        @foreach ($item->get_field('bloques-porque-aprender') as $blocAprender)
                            <li>
                                <span><img src="{{ asset('Themes/SmartNinja/images/home-check.svg') }}"/></span>
                                <h4>{{ $blocAprender['titulo']['value'] }}</h4>
                                {!!$blocAprender['texto']['value'] !!}
                            </li>
                        @endforeach
                    </ul>
                    <div class="btn-ctr">
                        <a href="javascript:void(0);" class="mas-btn">@Lang('Más información')</a>
                    </div>
                </div>

                <div class="owl-carousel" id="porque-slider">
                    @foreach ($item->get_field('bloques-porque-aprender') as $blocAprender)
                        <div class="item">
                            <div class="porque-cont">
                                <span><img src="{{ asset('Themes/SmartNinja/images/home-check.svg') }}"/></span>
                                <h4>{{ $blocAprender['titulo']['value'] }}</h4>
                                {!!$blocAprender['texto']['value'] !!}
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="btn-ctr mob"><a href="javascript:void(0);" class="mas-btn">@Lang('Más información')</a></div>
            @endif
        </div>
    </section>

    <section class="nuestros-cursos">
        <div class="row">
            <h1>@Lang('Nuestros cursos')</h1>
            <h2>@Lang('Cada mes programamos fechas nuevas para nuestros cursos.')</h2>
            <h3>@Lang('¡Entra y pre-inscríbete sin compromiso!')</h3>
            <div class="nuestros-list">
                <ul>
                    @foreach($cursos as $curso)
                        <li>
                            <h4>{{ $curso->title}}</h4>
                            @if($curso != "")
                                <span class="price">@Lang('Full price:') {{ $curso->get_field('precio') }}€</span>
                                <h5>@Lang('Early bird:') {{ $curso->get_field('precio-descuento') }}€</h5>
                            @else
                                <h5>{{ $curso->get_field('precio') }}€</h5>
                            @endif
                            
                            @if($curso->get_field('lista-espera') == true)
                                <span class="cal-ico">
                                    <img src="{{ asset('Themes/SmartNinja/images/cal-ico.png') }}"/> @Lang('Lista de espera')
                                </span>
                            @else
                                <span class="cal-ico">
                                        <img src="{{ asset('Themes/SmartNinja/images/cal-ico.png') }}"/> {{ Date::createFromFormat('d/m/Y', $curso->get_field('fecha-inicio'))->format('d/m') }} - {{ Date::createFromFormat('d/m/Y', $curso->get_field('fecha-fin'))->format('d/m') }}
                                </span>
                            @endif

                            <div class="btn-ctr">
                                <a href="
                                    @if($curso->get_field('dominio') != "")
                                        https://{{ $curso->get_field('dominio') }}
                                    @else
                                        {{ $curso->get_url() }}
                                    @endif
                                " class="ent-btn">@Lang('Entrar')</a>
                            </div>

                            @if($curso->get_field('precio-descuento') != "")
                                <span class="discount">
                                    <img src="{{ asset('Themes/SmartNinja/images/discount.svg') }}" alt="">
                                </span>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="nuestros-cursos-list">
                <ul>
                    @foreach($cursos as $index=>$curso)
                        @if($index < 4)
                             <li>
                                <h4>{{ $curso->title}}</h4>
                                @if($curso != "")
                                    <span class="price">@Lang('Full price:') {{ $curso->get_field('precio') }}€</span>
                                    <h5>@Lang('Early bird:') {{ $curso->get_field('precio-descuento') }}€</h5>
                                @else
                                    <h5>{{ $curso->get_field('precio') }}€</h5>
                                @endif
                                
                                @if($curso->get_field('lista-espera') == true)
                                    <span class="cal-ico">
                                        <img src="{{ asset('Themes/SmartNinja/images/cal-ico.png') }}"/> @Lang('Lista de espera')
                                    </span>
                                @else
                                    <span class="cal-ico">
                                        <img src="{{ asset('Themes/SmartNinja/images/cal-ico.png') }}"/> {{ Date::createFromFormat('d/m/Y', $curso->get_field('fecha-inicio'))->format('d/m') }} - {{ Date::createFromFormat('d/m/Y', $curso->get_field('fecha-fin'))->format('d/m') }}
                                    </span>
                                @endif
    
                                <div class="btn-ctr">
                                    <a href="
                                        @if($curso->get_field('dominio') != "")
                                            https://{{ $curso->get_field('dominio') }}
                                        @else
                                            {{ $curso->get_url() }}
                                        @endif
                                    " class="ent-btn">@Lang('Entrar')</a>
                                </div>
    
                                @if($curso->get_field('precio-descuento') != "")
                                    <span class="discount">
                                        <img src="{{ asset('Themes/SmartNinja/images/discount.svg') }}" alt="">
                                    </span>
                                @endif
                            </li>
                        @endif
                    @endforeach
                </ul> 
                @if(count($cursos) > 4)
                    <div class="more-nuestros-list">   
                        <ul> 
                            @foreach($cursos as $index=>$curso)
                                @if($index >= 4)
                                    <li>
                                        <h4>{{ $curso->title}}</h4>
                                        @if($curso != "")
                                            <span class="price">@Lang('Full price:') {{ $curso->get_field('precio') }}€</span>
                                            <h5>@Lang('Early bird:') {{ $curso->get_field('precio-descuento') }}€</h5>
                                        @else
                                            <h5>{{ $curso->get_field('precio') }}€</h5>
                                        @endif
                                        
                                        @if($curso->get_field('lista-espera') == true)
                                            <span class="cal-ico">
                                                <img src="{{ asset('Themes/SmartNinja/images/cal-ico.png') }}"/> @Lang('Lista de espera')
                                            </span>
                                        @else
                                            <span class="cal-ico">
                                                <img src="{{ asset('Themes/SmartNinja/images/cal-ico.png') }}"/> {{ Date::createFromFormat('d/m/Y', $curso->get_field('fecha-inicio'))->format('d/m') }} - {{ Date::createFromFormat('d/m/Y', $curso->get_field('fecha-fin'))->format('d/m') }}
                                            </span>
                                        @endif
            
                                        <div class="btn-ctr">
                                            <a href="
                                                @if($curso->get_field('dominio') != "")
                                                    https://{{ $curso->get_field('dominio') }}
                                                @else
                                                    {{ $curso->get_url() }}
                                                @endif
                                            " class="ent-btn">@Lang('Entrar')</a>
                                        </div>
            
                                        @if($curso->get_field('precio-descuento') != "")
                                            <span class="discount">
                                                <img src="{{ asset('Themes/SmartNinja/images/discount.svg') }}" alt="">
                                            </span>
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>   
                    <div class="view-more-btn">
                        <a class="view-more" href="javascript:void(0);">@Lang('VER MÁS')</a>
                        <a class="view-less" href="javascript:void(0);">@Lang('VER MENOS')</a>
                    </div>  
                @endif
            </div>
        </div>
    </section>

    @if (count($item->get_field('bloques-porque-aprender')) > 0)
        <section class="que-dicen">
            <h1>¿Qué dicen de nosotros?</h1>
            <div id="slider-center" class="center slider">
                @foreach ($item->get_field('bloques-comentarios') as $blocComentario)
                    <div>
                        <div class="slider-cont">
                            <h2>{{ $blocComentario['autor']['value'] }}</h2>
                            <p>{{ $blocComentario['comentario']['value'] }}</p>
                        </div>
                    </div>
                @endforeach
           </div>
           <div class="row">
               <div class="owl-carousel" id="que-dicen-slider">
                    @foreach ($item->get_field('bloques-comentarios') as $blocComentario)
                        <div class="item">
                            <div class="slider-cont">
                                <h2>{{ $blocComentario['autor']['value'] }}</h2>
                                <p>{{ $blocComentario['comentario']['value'] }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    <section id="contactanos" class="contact-anos">
        <div class="row">
            <h1>@Lang('¡Contáctanos!')</h1>
            <h2>@Lang('Aun no sabes cual curso hacer? ¿Necesitas algo más de información?')</h2>
            <h3>@Lang('¡Ponte en contacto con nosotros y te contactaremos con la mayor brevedad!')</h3>
            <form id="FormContactoHome" class="form-box" method="POST">
                <div class="frm-input">
                    <div class="frm-input-ctrl">
                        <textarea class="txt-box" name="comment" value="" placeholder="Tu comentario" required></textarea>
                    </div>
                </div>
                <div class="frm-input txt-box">
                    <div class="frm-input-ctrl">
                        <input class="form-control" name="email" value="" placeholder="Tu email" type="text" required>
                    </div>
                </div>
                <div class="frm-input txt-box">
                    <div class="frm-input-ctrl">
                        <input class="form-control" name="phone" value="" placeholder="Tu teléfono" type="text" required>
                    </div>
                </div>
                <div class="frm-input txt-box">
                    <div class="frm-input-ctrl">
                        <input class="form-control" name="name" value="" placeholder="Tu nombre" type="text" required>
                    </div>
                </div>

                <input type="hidden" value=" " name="lastname" />
                <input type="hidden" value="{{ $item->get_field('correo-contacto') }}" name="to" />
				<input type="hidden" value="2" name="email_id" />
                
                <div class="frm-input checkbox">
                    <input id="check3" type="checkbox" required>
                    <label for="check3">@Lang('Sí, acepto las condiciones de Smartninja')</label>
                </div>
                <div class="frm-input checkbox">
                    <input id="check4" type="checkbox">
                    <label for="check4">@Lang('Sí, quiero recibir vuestro Newsletter con información sobre cursos nuevos, promociones y deadlines.')</label>
                </div>

                <div class="resultContainer">
                    <p class="resultMessage">
                    </p>
                </div>

                <div class="btn-ctr">
                    <button class="env-btn">
                        @Lang('Más información')
                    </button>
                </div>
            </form>
        </div>
    </section>

    @if (count($item->get_field('slider-imagenes')) > 0)
        <section class="banner-slider">
            <div class="flexslider">
                <ul class="slides">
                    @foreach($item->get_field('slider-imagenes') as $sliderImagen)
                        <li>
                            <img 
                                alt="{{ $sliderImagen['titulo']['value'] }}"
                                src="{{ $sliderImagen['imagen']['value']->get_thumbnail_url() }}"
                            />
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    @endif

    @if (count($item->get_field('preguntas')) > 0)
        <section class="preguntas-frecuentes">
            <div class="row">
                <h1>@Lang('Preguntas frecuentes')</h1>
                <div class="acc-dropdown">
                    <div class="acc-lft">  
                        <ul>
                            <li>
                                <div class="accordion_example1">
                                    @foreach($item->get_field('preguntas') as $index=>$pregunta)
                                        @if($index < (int)$numPreguntas/2)
                                            <div class="accordion_in acc_active">
                                                <div class="acc_head">
                                                    <h2>{{ $pregunta['pregunta']['value'] }}</h2>
                                                </div>
                                                <div class="acc_content">
                                                    {!! $pregunta['respuesta']['value'] !!}
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="acc-rgt">  
                        <ul>
                            <li>
                                <div class="accordion_example2">
                                    @foreach($item->get_field('preguntas') as $index=>$pregunta)
                                        @if($index >= (int)$numPreguntas/2)
                                            <div class="accordion_in acc_active">
                                                <div class="acc_head">
                                                    <h2>{{ $pregunta['pregunta']['value'] }}</h2>
                                                </div>
                                                <div class="acc_content">
                                                    {!! $pregunta['respuesta']['value'] !!}
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach  
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <h4>@Lang('¿Tienes más preguntas y no encuentras la respuesta?')</h4>
                <div class="btn-ctr">
                    <a href="#contactanos" class="cont-btn">
                        @Lang('¡Contáctanos!')
                    </a>
                </div>       
            </div>
        </section>
    @endif

    <section class="aun-no">
        <div class="aun-cont">
            <h1>@Lang('¿Aun no conoces nuestro Ebook “5 Pasos para convertirte en un Programador Profesional”?')</h1>
            <span>@Lang('Pon aquí tu email para enviarte el link de descarga gratuita')</span>
            <form id="FormEbook" class="form-box" method="POST">
                <div class="frm-input">
                    <div class="frm-input-ctrl">
                        <input class="form-control" name="email" value="" placeholder="Tu email" type="text" required>
                    </div>
                </div>
                <div class="frm-input checkbox">
                    <input id="check5" type="checkbox" name="check5" required>
                    <label for="check5">@Lang('Sí, acepto las condiciones de Smartninja')</label>
                </div>
                <div class="frm-input checkbox">
                    <input id="check6" type="checkbox" name="check6">
                    <label for="check6">@Lang('Sí, quiero recibir vuestro Newsletter con información sobre cursos nuevos, promociones y deadlines.')</label>
                </div>
                
                <input type="hidden" value=" " name="lastname" />
                <input type="hidden" value="{{ $item->get_field('correo-ebook') }}" name="to" />
                <input type="hidden" value="3" name="email_id" />
                
                <div class="resultContainer">
                    <p class="resultMessage">
                    </p>
                </div>
                <div class="btn-ctr aun">
                    <button class="des-btn">@Lang('Descarga el Ebook')</button>
                </div>
            </form>
        </div>	
    </section>

    <section class="somos-en">
        <div class="row">
            <h1>@Lang('Somos Smartninja, tu Coding School en Barcelona')</h1>
            <div class="somos-cont"> 
                <div class="somos-lft">
                    <p>@Lang('Smartninja nació ya en 2014 en Eslovenia donde se encuentra hoy en día el Head Quater pero ya son 15 sedes que han nacido en toda Europa, y en 2017 en Barcelona.')</p> 
                    <p>@Lang('Nicolas Buchholz y Henroique Távora, socios fundadores de Enfusión, una Agencia Digital que se dedica al Inbound Marketing y Diseño Web, fueron los elegidos para dar vida a Smartninja en Barcelona. Fusionan el talento de los expertos en programación y su visión empresarial que consiste en aportar valor a la vida profesional, amar lo que haces e intercambiarse constantemente con las personas que te rodean a diario.')</p>
                </div>
                <div class="somos-rgt">
                    <img src="{{ $item->get_field('imagen-historia')->get_thumbnail_url() }}"/>
                </div>
            </div>  
        </div>
    </section>

    <section class="donde-encon">
        <h1>@Lang('Dónde encontrarnos')</h1>
        <div class="map-sec">
            <img src="{{ asset('Themes/SmartNinja/images/home-04.jpg') }}"/>
            <span><img src="{{ asset('Themes/SmartNinja/images/home-location.png') }}"/></span>
        </div>
    </section>
@endsection