@extends('Front::layouts.base')

@section('content')
    
    <section class="gracias-banner">
        <div class="row">	
            <div class="gracias-cont">	
                <h2>@Lang('¡Gracias!')</h2>
                <h3>@Lang('Hemos recibido tu solicitud de información.')</h3>
                <p>@Lang('En breve nos pondremos en contacto contigo para informarte sobre el curso, el temario, la pre-inscripción, coste y fechas y pasos a seguir.')</p>
            </div>  
        </div>
    </section>
    
    <section class="ahora-si">
    	<div class="row">
        	<h2>@Lang('Ahora si quieres puedes seguirnos en')</h2>
            <div class="ahora-btn">
                <a href="{{ get_option('facebook') }}" target="_blank">
                    @Lang('Facebook')
                </a>
                <a href="{{ get_option('web') }}" target="_blank">
                    @Lang('Visitar nuestra web')
                </a>
            </div>

            <h3>
                @Lang('Si tienes cualquier duda, nos puedes llamar al')
                <span> {{ get_option('telefono') }}</span>
                <br>
                @Lang('o escribirnos un mail a')
                <a href="mailto:{{ get_option('email') }}">{{ get_option('email') }}</a>
                <br>
                @Lang('¡Hasta pronto!')
            </h3>
            
            <h3>
                @Lang('Tu team Smartninja.')
            </h3>

            <img src="{{ asset('Themes/SmartNinja/images/gracias-logo.png') }}"/>
        </div>
    </section>

@endsection