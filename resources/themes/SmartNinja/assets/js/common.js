// Input Box Placeholder
function fnremove(arg,val)	{
	if (arg.value == '') {arg.value = val}
}	
function fnshow(arg,val)	{
	if (arg.value == val) {arg.value = ''}
}

$(document).ready(function () {	

	// Sticky Header		
	$(".top-header").sticky({topSpacing:0});	
	
	//$('#mobNav').mmenu();	
	$(".select_box").selectbox();
	$(".frm-input.dropdown select").selectbox();
	
	$(".view-more-btn").click(function(){
		$(".view-more-btn").toggleClass("show");
		$(".more-nuestros-list").toggleClass("show");
	});
	
	jsUpdateSize(); 	
 
});

$(window).resize(function(){	
	jsUpdateSize();
});

function jsUpdateSize()
{ 
	var width = $(window).width();
    var height = $(window).height();	
	if(width<=767)
	{
		$('.accordion_in').removeClass('acc_active');		
		$('.accordion_in .acc_content').css("display", "none");	
	}else{		
		$('.accordion_in').addClass('acc_active');	
		$('.accordion_in .acc_content').css("display", "block");
	}	
}