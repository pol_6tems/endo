$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});
$('#FormCurso').on('submit', function(e) {
    var form = $(this).serialize();
    $.ajax({
        url: ajaxURL,
        method: "POST",
        data: {
            action: "enviar_email",
            method: "no_json",
            parameters: form
        },
        success: function(data) {
            if(data.success == false) {
                $("#FormCurso .resultContainer p").text("No se ha podido enviar el mensaje");
                $("#FormCurso .resultContainer").removeClass("messageOK");
                $("#FormCurso .resultContainer").addClass("messageKO");
                $("#FormCurso .resultContainer").show();
                hideMessage();
            } else {
                $("#FormCurso .resultContainer p").text("Mensaje enviado correctamente");
                $("#FormCurso .resultContainer").removeClass("messageKO");
                $("#FormCurso .resultContainer").addClass("messageOK");
                $("#FormCurso .resultContainer").show();
                hideMessage();

                if($("#FormEbook [name='check8']:checked").length > 0) {
                    const email = $("#FormCurso [name='email']").val();
                    registerSendInBlue(email);   
                }

                window.location.replace("https://smartninja.cursos-programacion-web-barcelona.es/es/thankyou-page");
            }
        },
        fail : function(data) {
            $("#FormCurso .resultContainer p").text("No se ha podido enviar el mensaje");
            $("#FormCurso .resultContainer").removeClass("messageOK");
            $("#FormCurso .resultContainer").addClass("messageKO");
            $("#FormCurso .resultContainer").show();
            hideMessage();
        }
    });
    e.preventDefault();
    return false;
});

function hideMessage() {
	setTimeout(
		function() {
			$(".resultContainer").hide()
		}, 5000);
}

function registerSendInBlue(email) {
    const data = {
        "email" : email,
        "updateEnabled" : true
    };

    fetch(
        "https://api.sendinblue.com/v3/contacts",
        {
            method : 'POST',
            body: JSON.stringify(data),
            headers : {
                'Content-Type': 'application/json',
                'api-key': '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'
            }
        }
    ).then( res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));
}