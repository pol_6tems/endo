$(document).ready(function() {
    $('.banner-slider .flexslider').flexslider({
        animation: "fade",
        slideshowSpeed: 8000,
        animationSpeed:1000,
        easing: "swing",
        directionNav: true,
        controlNav: false,
        pausePlay: false
    });

    $('.center').slick({
        centerMode: true,
        variableWidth: true,
        centerPadding: '0px',
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
});

var owl = $('#porque-slider');
owl.owlCarousel({
    goToFirstSpeed :7000,
    loop:true,
    items:1,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    nav : true,
    dots : false
});	

var owl = $('#que-dicen-slider');
owl.owlCarousel({
    goToFirstSpeed :9000,
    loop:true,
    items:1,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    nav : false,
    dots : true
});	

jQuery(document).ready(function($){
    $(".accordion_example1, .accordion_example2").smk_Accordion
    ({
        closeAble:  true,
        closeOther: false
    });

    $('.nuestros-list ul li').each(function() {
        var len = $(this).find("h4").text().length;
        if(len < 25){
            $(this).addClass("test2");
        }
        else {
            $(this).addClass("test1");
        }
    });
});

// Formulari cursos Home
$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});
$('#FormCursoHome').on('submit', function(e) {
    if($("#FormCursoHome [name='curso']").val() == "") {
        alert("Selecciona un curso");
        return false;
    }
    
    var form = $(this).serialize();
    $.ajax({
        url: ajaxURL,
        method: "POST",
        data: {
            action: "enviar_email",
            method: "no_json",
            parameters: form
        },
        success: function(data) {
            if(data.success == false) {
                $("#FormCursoHome .resultContainer p").text("No se ha podido enviar el mensaje");
                $("#FormCursoHome .resultContainer").removeClass("messageOK");
                $("#FormCursoHome .resultContainer").addClass("messageKO");
                $("#FormCursoHome .resultContainer").show();
                hideMessage();
            } else {
                $("#FormCursoHome .resultContainer p").text("Mensaje enviado correctamente");
                $("#FormCursoHome .resultContainer").removeClass("messageKO");
                $("#FormCursoHome .resultContainer").addClass("messageOK");
                $("#FormCursoHome .resultContainer").show();
                hideMessage();

                if($("#FormCursoHome [name='check2']:checked").length > 0) {
                    const email = $("#FormCursoHome [name='email']").val();
                    registerSendInBlue(email);   
                }
            }
        },
        fail : function(data) {
            $("#FormCursoHome .resultContainer p").text("No se ha podido enviar el mensaje");
            $("#FormCursoHome .resultContainer").removeClass("messageKO");
            $("#FormCursoHome .resultContainer").addClass("messageKO");
            $("#FormCursoHome .resultContainer").show();
            hideMessage();
        }
    });
    e.preventDefault();
    return false;
});

function hideMessage() {
	setTimeout(
		function() {
			$(".resultContainer").hide()
		}, 5000);
}

$('#FormContactoHome').on('submit', function(e) {

    var form = $(this).serialize();
    $.ajax({
        url: ajaxURL,
        method: "POST",
        data: {
            action: "enviar_email",
            method: "no_json",
            parameters: form
        },
        success: function(data) {
            if(data.success == false) {
                $("#FormContactoHome .resultContainer p").text("No se ha podido enviar el mensaje");
                $("#FormContactoHome .resultContainer").removeClass("messageOK");
                $("#FormContactoHome .resultContainer").addClass("messageKO");
                $("#FormContactoHome .resultContainer").show();
                hideMessage();
            } else {
                $("#FormContactoHome .resultContainer p").text("Mensaje enviado correctamente");
                $("#FormContactoHome .resultContainer").removeClass("messageKO");
                $("#FormContactoHome .resultContainer").addClass("messageOK");
                $("#FormContactoHome .resultContainer").show();
                hideMessage();

                if($("#FormContactoHome [name='check4']:checked").length > 0) {
                    const email = $("#FormContactoHome [name='email']").val();
                    registerSendInBlue(email);   
                }
            }
        },
        fail : function(data) {
            $("#FormContactoHome .resultContainer p").text("No se ha podido enviar el mensaje");
            $("#FormContactoHome .resultContainer").removeClass("messageOK");
            $("#FormContactoHome .resultContainer").addClass("messageKO");
            $("#FormContactoHome .resultContainer").show();
            hideMessage();
        }
    });
    e.preventDefault();
    return false;
});

$('#FormEbook').on('submit', function(e) {
    var form = $(this).serialize();
    $.ajax({
        url: ajaxURL,
        method: "POST",
        data: {
            action: "enviar_email",
            method: "no_json",
            parameters: form
        },
        success: function(data) {
            if(data.success == false) {
                $("#FormEbook .resultContainer p").text("No se ha podido enviar el mensaje");
                $("#FormEbook .resultContainer").removeClass("messageOK");
                $("#FormEbook .resultContainer").addClass("messageKO");
                $("#FormEbook .resultContainer").show();
                hideMessage();
            } else {
                $("#FormEbook .resultContainer p").text("Mensaje enviado correctamente");
                $("#FormEbook .resultContainer").removeClass("messageKO");
                $("#FormEbook .resultContainer").addClass("messageOK");
                $("#FormEbook .resultContainer").show();
                hideMessage();
                
                if($("#FormEbook [name='check6']:checked").length > 0) {
                    const email = $("#FormEbook [name='email']").val();
                    registerSendInBlue(email);   
                }
            }
        },
        fail : function(data) {
            $("#FormEbook .resultContainer p").text("No se ha podido enviar el mensaje");
            $("#FormEbook .resultContainer").removeClass("messageOK");
            $("#FormEbook .resultContainer").addClass("messageKO");
            $("#FormEbook .resultContainer").show();
            hideMessage();
        }
    });
    e.preventDefault();
    return false;
});

function registerSendInBlue(email) {
    const data = {
        "email" : email,
        "updateEnabled" : true
    };

    fetch(
        "https://api.sendinblue.com/v3/contacts",
        {
            method : 'POST',
            body: JSON.stringify(data),
            headers : {
                'Content-Type': 'application/json',
                'api-key': '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'
            }
        }
    ).then( res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));
}