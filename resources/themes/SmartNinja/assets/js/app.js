window.$ = window.jQuery = require('jquery');
require('./plugins/jquery.flexslider');
require('./plugins/jquery.selectbox-0.2');
require('./plugins/jquery.sticky');

require('owl.carousel');
require('./plugins/respond');
require('./plugins/slick');
require('./plugins/smk-accordion');
require('./plugins/html5');
require('./common');

require('./home');
require('./curso');