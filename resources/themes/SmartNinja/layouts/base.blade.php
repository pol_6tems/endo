@extends('Front::layouts.html')

@section('body')
    <script> var ajaxURL = "{{ route('ajax') }}"; var lang = '{{ app()->getLocale() }}';</script>
    
    @include('Front::partials.header')

    @yield('content')

    @include('Front::partials.footer')
@endsection