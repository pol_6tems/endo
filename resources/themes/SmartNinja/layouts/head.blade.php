<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<link rel="icon" type="image/x-icon" href="{{ asset('Themes/SmartNinja/images/favicon.ico') }}">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield("title", config('app.name', 'Endo'))</title>

<link href="{{ mix('css/app.css', 'Themes/' . env('PROJECT_NAME')) }}" rel="stylesheet">