<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('Front::layouts.head')
    </head>

    <body>
        @yield('body')

        @include('Front::layouts.foot_scripts')
    </body>
</html>