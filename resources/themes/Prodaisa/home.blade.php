@extends('Front::layouts.main')

@section('content')
    <div class="accordionWrapper">
        @foreach($categories as $category)
            <div class="js-cat-container">
                <div class="head-pad @if ($category == $categories->first())mrgn-top @endif"><h2 class="flip open"> {{ $category->title }} <span></span></h2><a href="{{ route('document.create', ['category' => $category->post_name, 'client' => request('client')]) }}">@lang('AFEGIR DOCUMENT') <img src="{{ asset($_front.'images/add-coc-icon.svg') }}" alt=""></a></div>
                <div class="scroll-div js-cat-content">
                    <table class="table-responsive tab-1" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <thead>
                        <tr>
                            <th width="4%">@lang('Descarrega PDF')</th>
                            <th width="5%">@lang('Info')</th>
                            <th width="10%">@lang('Nom arxiu')</th>
                            <th width="10%">@lang('Propietari')</th>
                            <th width="10%">@lang('Empresa')</th>
                            <th width="10%">@lang('Data Publicació')</th>
                            <th width="10%">@lang('Data Màxima')</th>
                            <th width="10%">@lang('Data Validació')</th>
                            <th width="10%">@lang('Estat Validació')</th>
                            <th width="10%">@lang('Validat per')</th>
                            <th width="10%">@lang('Comentaris')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($category->documents as $document)
                            @php($creator = $document->creator_id ? \App\User::find($document->creator_id) : null)
                            <tr>
                                @php($file = $document->get_field('fitxer'))
                                <td>@if ($file)<a href="{{ $file->get_thumbnail_url() }}" download><img src="{{ asset($_front.'images/combined-shape.svg') }}" alt=""></a>@endif</td>
                                <td><a class="fancybox show" href="#popup-div"><textarea class="js-doc-description" style="display: none">{!! $document->description !!}</textarea><img src="{{ asset($_front.'images/info-icon-g.png') }}" alt=""></a></td>
                                <td>{{ $document->title }}</td>
                                <td>{{ $creator ? $creator->fullname() : ($document->author ? $document->author->fullname() : '') }}</td>
                                <td>{{ $creator ? $creator->get_field('empresa') : ($document->author ? $document->author->get_field('empresa') : '') }}</td>
                                <td>{{ $document->created_at->format('d/m/Y') }}</td>
                                <td>{{ $document->get_field('data-maxima-validacio') ?: $document->get_field('data-caducitat') }}</td>
                                <td>{{ $document->get_field('data-validacio') ?: '' }}</td>
                                @php ($docEstat = $document->get_field('estat-validacio'))
                                <td><img class="img-pad{{ $docEstat != 'validat' ? '-t' : '' }}" src="{{ asset($_front.'images/' . ($docEstat == 'validat' ? 'green-tick-icon' : (($docEstat == 'pendent') ? 'red-circle' : 'orange-circle')) . '.svg') }}" alt=""> <span class="rgt-pad">{{ __(ucfirst(str_replace('-', ' ', $docEstat))) }} @if ($docEstat != 'validat')<a class="link-pad" href="{{ route('document.validate', ['doc' => $document->post_name, 'client' => request('client')]) }}">{{ $docEstat == 'pendent' ? __('Validar') : __('Modificar') }}</a> @endif</span></td>
                                <td>{{ $docEstat == 'validat' && $document->get_field('validador') ? $document->get_field('validador')->fullname() : '' }}</td>
                                <td>
                                    <a class="comments" href="{{ route('document.comment', ['doc' => $document->post_name, 'client' => request('client')]) }}"><img src="{{ asset($_front.'images/edit-icon.svg') }}" alt=""></a>
                                    <a class="fancybox show comments" href="#popup-comments" data-document="{{ $document->post_name }}"><img src="{{ asset($_front.'images/eye-icon.svg') }}" alt=""></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <!-------------------->
                    <div class="mob-table">
                        <ul class="head-tit">
                            <li>@lang('PDF')</li>
                            <li>@lang('Info')</li>
                            <li>@lang('Nom arxiu')</li>
                            <li>@lang('Estat Validació')</li>
                            <li></li>
                        </ul>
                        @foreach($category->documents as $document)
                            @php($creator = $document->creator_id ? \App\User::find($document->creator_id) : null)

                            <div class="js-mobcat-container">
                                <ul>
                                    @php($file = $document->get_field('fitxer'))
                                    <li>@if ($file)<a href="{{ $file->get_thumbnail_url() }}" download><img src="{{ asset($_front.'images/combined-shape.svg') }}" alt=""></a>@endif</li>
                                    <li><a class="fancybox show" href="#popup-div"><textarea class="js-doc-description" style="display: none">{!! $document->description !!}</textarea><img src="{{ asset($_front.'images/info-icon-g.png') }}" alt=""></a></li>
                                    <li>{{ $document->title }}</li>
                                    @php ($docEstat = $document->get_field('estat-validacio'))
                                    <li {!! $docEstat != 'validat' ? 'class="mob-circle"' : '' !!} ><img class="img-pad{{ $docEstat != 'validat' ? '-t' : '' }}" src="{{ asset($_front.'images/' . ($docEstat == 'validat' ? 'green-tick-icon' : (($docEstat == 'pendent') ? 'red-circle' : 'orange-circle')) . '.svg') }}" alt=""> <span class="">{{ __(ucfirst(str_replace('-', ' ', $docEstat))) }}</span> @if ($docEstat != 'validat')<span class="rgt-pad"><a class="link-pad" href="{{ route('document.validate', ['doc' => $document->post_name, 'client' => request('client')]) }}">{{ $docEstat == 'pendent' ? __('Validar') : __('Modificar') }}</a></span> @endif</li>
                                    <li><span class="plus-ico"></span></li>
                                </ul>
                                <div class="scroll-div hide-sect js-mobcat-content" style="display:none;">
                                    <ul>
                                        <li>@lang('Propietari') <span class="rgt-array">{{ $creator ? $creator->fullname() : ($document->author ? $document->author->fullname() : '') }}</span></li>
                                        <li>@lang('Validat per') <span class="rgt-array">{{ $docEstat == 'validat' && $document->get_field('validador') ? $document->get_field('validador')->fullname() : '' }}</span></li>
                                        <li>@lang('Data Publicació') <span class="rgt-array">{{ $document->created_at->format('d/m/Y') }}</span></li>
                                        <li>@lang('Data Màxima Validació/Caducitat') <span class="rgt-array">{{ $document->get_field('data-maxima-validacio') ?: $document->get_field('data-caducitat') }}</span></li>
                                        <li>@lang('Data Validació') <span class="rgt-array">{{ $document->get_field('data-validacio') ?: '' }}</span></li>
                                        <li>@lang('Comentaris') <span class="rgt-array comments"><a href="{{ route('document.comment', ['doc' => $document->post_name, 'client' => request('client')]) }}"><img src="{{ asset($_front.'images/edit-icon.svg') }}" alt=""></a><a class="fancybox show" href="#popup-comments" data-document="{{ $document->post_name }}"><img src="{{ asset($_front.'images/eye-icon.svg') }}" alt=""></a></span></li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach

        <div id="popup-div" class="popup">
            <img src="{{ asset($_front.'images/logo-prodaisa.svg') }}" alt="PRODAISA">
            <h3><img src="{{ asset($_front.'images/info-icon.svg') }}" alt=""> @lang('Més informació')</h3>
            <div class="content demo-y js-popup-description">
                <p></p>
            </div>
        </div>

        <div id="popup-comments" class="popup">
            <img src="{{ asset($_front.'images/logo-prodaisa.svg') }}" alt="PRODAISA">
            <h3><img src="{{ asset($_front.'images/info-icon.svg') }}" alt=""> @lang('Més informació')</h3>
            <div class="content demo-y js-popup-comment">
            </div>
        </div>
    </div>
@endsection

@section('bottom_foot_scripts')
    <script>
        var commentsUrl = '{{ route('document.comments', ['doc' => '::doc']) }}';
    </script>
@endsection