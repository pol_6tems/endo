@extends('Front::layouts.login_main')

@section('extra_section_classes', auth()->user() ? 'aceptacio canvi' : '')

@section('content')
    <div class="form">
        <form id="document-form" action="{{ route('document.store', ['category' => $category]) }}" method="POST" enctype="multipart/form-data">
            @csrf

            <input id="file_id" type="hidden" name="file_id" value="" required>

            <h3><img src="{{ asset($_front.'images/edit-icon-title.svg') }}" alt="">@lang('Afegir document')</h3>
            <p>@lang('Completa les dades per afegir un document a la categoria document. Aquest document no serà Públic fins que PRODAISA l’hagi validat.')</p>
            <input type="text" name="title" class="frm-ctrl top-mrgn" placeholder="@lang('Nom document')" value="{{ old('title') }}" required>
            <textarea class="txt-box margn-top" name="description" placeholder="@lang('Descripció')…">{{ old('description') }}</textarea>

            @if ($clients)
                <label class="who-label">@lang('Escull a qui vols compartir el document:')</label>
                <div class="frm-input-ctrl browse">
                    <select class="js-validator-type" name="tipus-validador">
                        <option value="validador" selected>@lang('Usuari/Proveïdor')</option>
                        <option value="grup-validadors">@lang('Grup')</option>
                    </select>
                </div>

                <div class="frm-input-ctrl browse js-validators">
                    <select name="validador">
                        <option value="0">@lang('Seleccionar usuari/proveïdor')</option>
                        @foreach($clients as $client)
                            <option value="{{ $client->id }}" @if ($client->id == request('client'))selected @endif>{{ $client->fullname() }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="frm-input-ctrl browse js-validators" style="display: none">
                    <select name="grup-validadors">
                        <option value="0">@lang('Seleccionar grup validadors')</option>
                        @foreach($groups as $group)
                            @php($creator = $group->creator_id ? \App\User::find($group->creator_id) : null)
                            <option value="{{ $group->id }}" >{{ $group->title }} ({{ $creator ? $creator->get_field('empresa') . ' - ' . $creator->get_field('departament') : ($group->author ? $group->author->get_field('empresa') . ' - ' . $group->author->get_field('departament') : '') }})</option>
                        @endforeach
                    </select>
                </div>
                <br>
            @endif

            <div class="frm-input-ctrl browse">
                <textarea class="js-file-name" data-default="@lang('Afegir Document')">@lang('Afegir Document')</textarea>
                <label>
                    <img src="{{ asset($_front.'images/upload-icon-blue.svg') }}">
                    <input id="File" name="file" type="file" data-url="{{ route('document.upload-file', ['category' => $category]) }}">
                </label>
            </div>
            <div class="jquery-datepicker" data-lang="{{ app()->getLocale() }}">
                <input type="text" name="data-maxima-validacio" class="frm-ctrl blue-line jquery-datepicker__input" placeholder="@lang('Data màxima validació')" value="{{ old('data-maxima-validacio') }}">
            </div>

            <div class="jquery-datepicker" data-lang="{{ app()->getLocale() }}">
                <input type="text" name="data-caducitat" class="frm-ctrl blue-line jquery-datepicker__input" placeholder="@lang('Data caducitat')" value="{{ old('data-caducitat') }}">
            </div>

            <div class="clear"></div>
            <button type="submit" class="mt10 submit-btn">@lang('AFEGIR')</button>
            <div class="progress-block" style="display: none">
                <div class="progress-container">
                    <div id="progress" class="progress-bar"></div>
                </div>
                <div class="progress-number">0%</div>
            </div>
        </form>
    </div>
@endsection

@section('bottom_foot_scripts')
    <script src="{{ mix('js/vendor/jquery.ui.widget.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>
    <script src="{{ mix('js/jquery.iframe-transport.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>
    <script src="{{ mix('js/jquery.fileupload.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>

    <script>
        var fileMaxSize = {{ $maxSize }};
        var fileMaxSizeMsg = '{{ $maxSizeMsg }}';
        new FileUploader;
    </script>
@endsection