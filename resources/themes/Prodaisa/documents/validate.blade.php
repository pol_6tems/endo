@extends('Front::layouts.login_main')

@section('extra_section_classes', auth()->user() ? 'aceptacio canvi' : '')

@section('content')
    <div class="form">
        <form method="POST">
            @csrf

            <h3><img src="{{ asset($_front.'images/edit-icon-title.svg') }}" alt="">@lang('Canvi d\'estat del document')</h3>
            <p>@lang('Selecciona el nou estat del document')</p>
            <div class="radio-pad">
                <div class="radio">
                    <input type="radio" name="estat-validacio" value="validat" id="check1" @if ($validationStatus == 'validat')checked @endif> <label for="check1">@lang('Acceptat / Validat')</label>
                </div>
                <div class="radio">
                    <input type="radio" name="estat-validacio" value="revisio" id="check2" @if ($validationStatus == 'revisio')checked @endif> <label for="check2">@lang('En revisió')</label>
                </div>
                <div class="radio no-accept">
                    <input class="flip" type="radio" id="check3" name="estat-validacio" value="no-acceptat" @if ($validationStatus == 'no-acceptat')checked @endif> <label for="check3">@lang('No acceptat')</label>
                </div>
            </div>
            <div class="panel">
                <p>@lang('Escriu el motiu de la no conformitat')</p>
                <textarea class="txt-box" name="comment" placeholder="@lang('Comentari')"></textarea>
            </div>
            <div class="clear"></div>
            <button type="submit" class="mt10">@lang('CANVIAR ESTAT')</button>
        </form>
    </div>
@endsection