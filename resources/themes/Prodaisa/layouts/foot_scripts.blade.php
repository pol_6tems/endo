<script src="{{ asset($_front.'js/respond.js') }}"></script>
<script crossorigin="anonymous" src="https://polyfill.io/v3/polyfill.min.js?features=URL"></script>
<script src="{{ mix('js/app.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>

@include('Front::partials.scripts.notifications')

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@yield('bottom_foot_scripts')