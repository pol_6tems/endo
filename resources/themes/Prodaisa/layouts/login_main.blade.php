@extends('Front::layouts.html')

@section('body')
    @hasSection('extra_section_classes')
        @include('Front::partials.header')
    @endif

    <section class="area-container @yield('extra_section_classes', '')">
        <div class="area-content @yield('extra_login_class', '')">
            @yield('content')
        </div>
    </section>
@endsection