<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('Front::layouts.head')
</head>

<body class="area-body @yield('body_classes', '')">
    @php( admin_bar() )

    @yield('body')

    @include('Front::layouts.foot_scripts')
</body>
</html>