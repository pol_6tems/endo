@if (auth()->user())
    <header>
        <div class="row">
            <div class="head-left">
                <div class="logo"> <a href="{{ route('index', auth()->user()->role == 'gestor' && request('client') ? ['index' => '', 'client' => request('client')] : []) }}"><img src="{{ asset($_front.'images/logo-prodaisa.svg') }}" alt="PRODAISA"></a> </div>
                <ul>
                    <li>@lang('Extranet<br> gestió documental')</li>
                    <li class="user-name"><a href="{{ route('user.edit') }}"><div><span>{{ substr(auth()->user()->name, 0, 1) }}</span> @lang('Benvingut :name', ['name' => auth()->user()->name])</div></a></li>
                </ul>
            </div>
            <div class="head-rgt">
                @if (isset($clients) && $clients->count())
                    <div class="descar">
                        <select class="js-change-client" data-route="{{ request()->url() }}">
                            @foreach($clients as $client)
                                <option value="{{ $client->id }}" @if (request('client') == $client->id )selected @endif>{{ $client->fullname() }}</option>
                            @endforeach
                        </select>
                        {{--<a href="javascript:void(0);">@lang('DESCARREGA EXCEL')</a>--}}
                    </div>
                @endif
                <div class="language-container">
                    <form method="POST" action="{{ route('user.update-language') }}">
                        @csrf

                        <input type="hidden" name="after_route" value="{{ route_name() }}">

                        <select class="js-lang-select" name="lang" data-route="{{ request()->url() }}">
                            @foreach($languages as $language)
                                <option value="{{ $language['code'] }}" @if ($language['code'] == auth()->user()->get_field('idioma'))selected @endif>@lang($language['name'])</option>
                            @endforeach
                        </select>
                    </form>
                </div>
                <div class="sortir">
                    <a href="javascript:void(0);" onclick="$('#logout-form').submit();">@lang('Sortir')</a>
                </div>
            </div>
        </div>
    </header>

    <form id="logout-form" action="{{ route('logout') }}" method="POST">
        @csrf
    </form>
@endif