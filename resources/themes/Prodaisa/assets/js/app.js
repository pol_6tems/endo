
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./plugins/html5');
window.$ = window.jQuery = require('jquery');

require('./plugins/jquery.fancybox');
require('./plugins/jquery.mCustomScrollbar.concat.min');
require('./plugins/jquery-datepicker');

window.toastr = require('toastr');

require('./plugins/widget-updater');
require('./plugins/file-uploader');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

$('.js-view-pass').on('click', function () {
    var passInput = $(this).closest('.password-pad').find('input[name="password"]');
    if (passInput.attr('type') === "password") {
        passInput.attr('type', 'text');
    } else {
        passInput.attr('type', 'password');
    }
});

$(".flip").click(function() {
    var panel = $(this).closest('.js-cat-container').find('.js-cat-content');
    if($(this).hasClass('open')) {
        panel.slideUp(300);
        $(this).removeClass('open');
    }else{
        $(this).addClass('open');
        panel.slideDown(500);
    }
});

$(".plus-ico").click(function() {
    var width = $(window).width();
    if (width < 1023) {
        var panel = $(this).closest('.js-mobcat-container').find('.js-mobcat-content');

        if ($(this).hasClass('open')) {
            panel.slideUp(300);
            $(this).removeClass('open');
        } else {
            $(this).addClass('open');
            panel.slideDown(500);
        }
    }
});

var fancyBoxs = $(".fancybox");

fancyBoxs.fancybox();

fancyBoxs.on('click', function () {
    var textArea = $(this).find('.js-doc-description').first();
    var popupContent = $('.js-popup-description').first();
    var popupCommentsContent = $('.js-popup-comment').first();
    var popupDocument = $(this).data('document');

    if (typeof textArea !== 'undefined' && textArea !== null && textArea.length > 0 &&
        typeof popupContent !== 'undefined' && popupContent !== null && popupContent.length > 0) {
        popupContent.find('.mCSB_container').html(textArea.val());
        $('.demo-y').mCustomScrollbar('update');
    }

    if (typeof popupDocument !== 'undefined') {
        var url = commentsUrl.replace('::doc', popupDocument);
        var container = popupCommentsContent.find('.mCSB_container');

        if (typeof container !== 'undefined') {
            WidgetUpdater.update(url, container);
        } else {
            container.html('');
            $('.demo-y').mCustomScrollbar('update');
        }
    }
});

var lang = $('.jquery-datepicker').data('lang');
$('.jquery-datepicker').datepicker({lang: typeof lang !== 'undefined' ? lang : 'es'});

(function($){
    $(window).on("load",function(){

        $.mCustomScrollbar.defaults.theme="light-2"; //set "light-2" as the default theme

        $(".demo-y").mCustomScrollbar();

        $(".scrollTo a").click(function(e){
            e.preventDefault();
            var $this=$(this),
                rel=$this.attr("rel"),
                el=rel==="content-y" ? ".demo-y" : rel==="content-x" ? ".demo-x" : ".demo-yx",
                data=$this.data("scroll-to"),
                href=$this.attr("href").split(/#(.+)/)[1],
                to=data ? $(el).find(".mCSB_container").find(data) : el===".demo-yx" ? eval("("+href+")") : href,
                output=$("#info > p code"),
                outputTXTdata=el===".demo-yx" ? data : "'"+data+"'",
                outputTXThref=el===".demo-yx" ? href : "'"+href+"'",
                outputTXT=data ? "$('"+el+"').find('.mCSB_container').find("+outputTXTdata+")" : outputTXThref;
            $(el).mCustomScrollbar("scrollTo",to);
            output.text("$('"+el+"').mCustomScrollbar('scrollTo',"+outputTXT+");");
        });

    });
})(jQuery);

$('.js-change-client').on('change', function () {
    window.location = $(this).data('route') + '?client=' + $(this).val();
});


var check3 = $('#check3').first();

if (typeof check3 !== 'undefined' && check3 !== null && check3.length > 0) {
    if (check3.is(':checked')) {
        $(".panel").slideDown(300);
    }

    $(".radio").click(function () {
        if (check3.is(':checked')) {
            $(".panel").slideDown(300);
        } else {
            $(".panel").slideUp(300);
        }
    });
}

$('#File').on('change', function () {
    var files = $(this).prop("files");
    var names = $.map(files, function(val) { return val.name; });

    var namesString = names.join(', ');

    var fileTextArea = $('.js-file-name')

    fileTextArea.html(namesString !== '' ? namesString : fileTextArea.data('default'));
});


$('.js-lang-select').on('change', function () {
    $(this).closest('form').submit();
});

$('.js-validator-type').on('change', function () {
    var me = $(this);

    $('.js-validators:visible').slideUp('fast', function () {
        $('select[name="' + me.val() + '"]').closest('.js-validators').slideDown('fast');
    });
});