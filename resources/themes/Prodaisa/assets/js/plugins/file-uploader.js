(function(window, $) {
    var submitted = false;

    var FileUploader = function() {
        this.init();
    };

    FileUploader.prototype = {
        init: function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });

            this.bindings();
        },

        bindings: function () {
            var documentForm = $('#document-form');

            $('#File').fileupload({
                dataType: 'json',
                add: function (e, data) {
                    if(typeof data.originalFiles[0]['size'] !== 'undefined' && data.originalFiles[0]['size'] > fileMaxSize) {
                        toastr.error(fileMaxSizeMsg);
                    }

                    data.context = documentForm.off('submit').on('submit', function (e) {
                        e.preventDefault();
                        if(typeof data.originalFiles[0]['size'] !== 'undefined' && data.originalFiles[0]['size'] > fileMaxSize) {
                            toastr.error(fileMaxSizeMsg);
                        } else {
                            data.submit();
                        }
                    });
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);

                    $('.progress-block').show();
                    $('.submit-btn').hide();
                    $('#progress').css('width', progress + '%');
                    $('.progress-number').html(progress + '%');
                },
                done: function (e, data) {
                    $('.progress-block').hide();

                    $('#file_id').val(data.result.id);

                    documentForm.unbind('submit').submit();
                    if (!submitted) {
                        submitted = true;
                        documentForm.submit();
                    }

                    $('#progress').css('width', 0);
                    $('.progress-number').html('');
                },
                error: function (data) {
                    console.log('error');
                    console.log(data);
                    $('.progress-block').hide();
                    $('.submit-btn').show();
                    $.each(data.errors.file, function(index, message) {
                        toastr.error(message);
                    });
                    $('#progress').css('width', 0);
                    $('.progress-number').html('');
                }
            });
        }
    };

    if (typeof self !== 'undefined') {
        self.FileUploader = FileUploader;
    }

    // Expose as a CJS module
    if (typeof exports === 'object') {
        module.exports = FileUploader;
    }
})(window, $);