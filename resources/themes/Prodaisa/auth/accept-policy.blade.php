@extends('Front::layouts.login_main')

@section('extra_login_class', 'aceptacio')

@section('content')
    <style>
    .area-content div.form .demo-y p { padding: 5px; }
    </style>
    
    <div class="form">
        <form method="POST">
            @csrf

            <img src="{{ asset($_front.'images/logo-prodaisa.svg') }}" alt="PRODAISA">
            <h3>{{ $content->title }}</h3>
            <div class="content demo-y">
                {!! $content->description !!}
            </div>
            <div class="checkbox">
                <input type="checkbox" id="check1" name="accepta-politica" required>
                <label for="check1">@lang('Accepto la normativa d’ús i RGPD')</label>
            </div>
            <button type="submit" class="mt10">@lang('ACCEDIR')</button>
            <div class="clear"></div>
        </form>
    </div>
@endsection