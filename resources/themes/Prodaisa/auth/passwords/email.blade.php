@extends('Front::layouts.login_main')

@section('extra_login_class', session('status') ? 'email' : 'recuperar')

@section('content')
	<div class="form">
		@if (session('status'))
			<img src="{{ asset($_front.'images/logo-prodaisa.svg') }}" alt="PRODAISA">
			<h3>@lang('Email enviat')</h3>
			<p>@lang('Comprovi el seu correu. Li hem enviat un<br> nou link per modificar la contrasenya')</p>
			<div class="email-img"><img src="{{ asset($_front.'images/mail-icon.svg') }}" alt=""></div>
			<button type="submit" onclick="window.location = '{{ route('login') }}';" class="mt10">@lang('TORNAR')</button>
			<div class="clear"></div>
		@else

			<form method="POST" action="{{ route('password.email') }}">
				@csrf

				<img src="{{ asset($_front.'images/logo-prodaisa.svg') }}" alt="PRODAISA">
				<h3>@lang('Recuperar contrasenya')</h3>
				<p>@lang('Introdueixi la seva direcció de correu<br> electrònic per rescuperar la contrasenya')</p>
				<label>Email</label>
				<input type="text" name="email" class="frm-ctrl" placeholder="ex. prodaisa@email.com" required>
				<button type="submit" class="mt10">@lang('ENVIAR')</button>
				<div class="clear"></div>
			</form>
		@endif
	</div>
@endsection
