@extends('Front::layouts.login_main')

@section('content')
    <div class="form">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <img src="{{ asset($_front.'images/logo-prodaisa.svg') }}" alt="PRODAISA">
            <h3>@lang('Extranet gestió documental')</h3>
            <label>@lang('Email')</label>
            <input type="text" name="email" class="frm-ctrl" placeholder="ex. prodaisa@email.com" required>
            <label>@lang('Paraula clau')</label>
            <div class="clear"></div>
            <div class="password-pad">
                <input type="password" name="password" class="frm-ctrl" placeholder="********" required><a class="js-view-pass" href="javascript:void(0);"></a>
            </div>
            <div class="checkbox">
                <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="remember">@lang('Recorda\'m')</label>
            </div>
            <button type="submit" class="mt10">@lang('ACCEDIR')</button>
            <div class="clear"></div>
            <p class="mt10"><a href="{{ route('password.request') }}">@lang('Recuperar clau')</a></p>
        </form>
    </div>
@endsection