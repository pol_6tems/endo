@extends('Front::layouts.login_main')

@section('extra_section_classes', auth()->user() ? 'aceptacio canvi' : '')
@section('extra_login_class', 'dades')

@section('content')
    <div class="form">
        <form action="{{ route('user.update') }}" method="POST">
            @csrf
            @method('PUT')

            <h3>@lang('Dades d’usuari')</h3>
            <p>@lang('A continuació pot modificar les dades d’accés')</p>
            <div class="frm-left">
                <label>@lang('Nom usuari')</label>
                <input type="text" name="name" value="{{ auth()->user()->name }}" class="frm-ctrl" placeholder="@lang('Nom')" required>
                <label>Email</label>
                <input type="text" name="email" value="{{ auth()->user()->email }}" class="frm-ctrl" placeholder="ex. prodaisa@email.com" required>
                <label>@lang('Empresa')</label>
                <input type="text" value="{{ auth()->user()->get_field('empresa') }}" class="frm-ctrl"  disabled>
                <label>@lang('Departament')</label>
                <input type="text" value="{{ auth()->user()->get_field('departament') }}" class="frm-ctrl" disabled>
                <div class="clear"></div>
                <button type="submit" class="mt10">@lang('GUARDAR')</button>
            </div>
        </form>
        <div class="frm-right">
            <form action="{{ route('user.update-pwd') }}" method="POST">
                @csrf
                @method('PUT')

                <label>@lang('Contrasenya')</label>
                <div class="password-pad">
                    <input type="password" name="password" class="frm-ctrl" placeholder="********" id="myInput" required><a href="javascript:void(0);" class="js-view-pass" ></a>
                </div>
                <label>@lang('Repetir contrasenya')</label>
                <div class="password-pad">
                    <input type="password" name="password_confirmation" class="frm-ctrl" placeholder="********" id="myInput1" required><a href="javascript:void(0);" class="js-view-pass"></a>
                </div>
                <div class="clear"></div>
                <button type="submit" class="mt10">@lang('GUARDAR')</button>
            </form>
        </div>
    </div>
@endsection