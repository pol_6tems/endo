@extends('Front::layouts.login_main')

@section('extra_section_classes', auth()->user() ? 'aceptacio canvi' : '')

@section('content')
    <div class="form">
        <form id="document-form" action="{{ route('document.store', ['category' => $category->post_name]) }}" method="POST" enctype="multipart/form-data">
            @csrf

            <input id="file_ids" type="hidden" name="file_ids" value="" required>

            <h3><img src="{{ asset($_front.'images/edit-icon-title.svg') }}" alt="">@lang('Afegir document')</h3>
            <p>@lang('Completa les dades per afegir un document a la categoria document. Aquest document no serà Públic fins que ARRAY l’hagi validat.')</p>
            <p>* @lang('Camps obligatoris')</p>
            <input type="text" name="title" class="frm-ctrl top-mrgn" placeholder="* @lang('Nom document')" value="{{ old('title') }}" required>
            <textarea class="txt-box margn-top" name="description" placeholder="@lang('Descripció')…">{{ old('description') }}</textarea>

            @if ($clients)
                <label class="who-label">@lang('Escull a qui vols compartir el document:')</label>
                <div class="frm-input-ctrl browse">
                    <select class="js-validator-type" name="tipus-validador">
                        <option value="validador" selected>@lang('Usuari/Proveïdor')</option>
                        <option value="grup-validadors">@lang('Grup')</option>
                    </select>
                </div>

                <div class="frm-input-ctrl browse js-validators">
                    <select name="validador">
                        <option value="0">* @lang('Seleccionar usuari/proveïdor')</option>
                        @foreach($clients as $client)
                            <option value="{{ $client->id }}" @if ($client->id == request('client'))selected @endif>{{ $client->fullname() }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="frm-input-ctrl browse js-validators" style="display: none">
                    <select name="grup-validadors">
                        <option value="0">* @lang('Seleccionar grup validadors')</option>
                        @foreach($groups as $group)
                            @php($creator = $group->gestors->first())
                            <option value="{{ $group->id }}" >{{ $group->title }} ({{ $creator ? $creator->get_field('empresa') . ' - ' . $creator->get_field('departament') : ($group->author ? $group->author->get_field('empresa') . ' - ' . $group->author->get_field('departament') : '') }})</option>
                        @endforeach
                    </select>
                </div>
                <br>
            @endif

            <div class="frm-input-ctrl browse">
                <textarea class="js-file-name" data-default="* @lang('Afegir Documents')" data-full="@lang('Modificar Documents')">* @lang('Afegir Documents')</textarea>
                <label>
                    <img src="{{ asset($_front.'images/upload-icon-blue.svg') }}">
                    <input id="File" name="files[]" type="file" data-url="{{ route('document.upload-file', ['category' => $category->post_name]) }}" multiple>
                </label>
            </div>

            <div>
                <ul id="file-list" style="display: none">

                </ul>
            </div>
            @if ($clients)
                <div class="frm-input-ctrl browse">
                    <select name="tipus-validacio">
                        @foreach($validationTypes as $validationType)
                            @php($choice = explode(':', $validationType))
                            <option value="{{ $choice[0] }}">@lang($choice[1])</option>
                        @endforeach
                    </select>
                </div>

                <div class="frm-input-ctrl browse">
                    <select name="periode-maxim-validacio">
                        <option value="0">@lang('Seleccionar periode màxim de validació')</option>
                        @foreach($maxPeriods as $maxPeriod)
                            @php($choice = explode(':', $maxPeriod))
                            <option value="{{ $choice[0] }}">@lang($choice[1])</option>
                        @endforeach
                    </select>
                </div>
            @else
                <input type="hidden" name="tipus-validacio" value="{{ \App\Modules\ArrayPlastics\Models\UserDocument::DOC_TYPE_WEB_VALIDABLE }}">

                <div class="jquery-datepicker" data-lang="{{ app()->getLocale() }}" @if ($category->get_field('proveedor-sube'))style="display: none"@endif>
                    <input type="text" name="data-maxima-validacio" class="frm-ctrl blue-line jquery-datepicker__input" placeholder="@lang('Data màxima validació')" value="{{ old('data-maxima-validacio') }}" autocomplete="off">
                </div>
            @endif

            <div class="jquery-datepicker" data-lang="{{ app()->getLocale() }}" @if ($category->get_field('proveedor-sube'))style="display: none"@endif>
                <input type="text" name="data-caducitat" class="frm-ctrl blue-line jquery-datepicker__input" placeholder="@lang('Data caducitat')" value="{{ old('data-caducitat') }}" autocomplete="off">
            </div>

            <div class="clear"></div>
            <button type="submit" class="mt10 submit-btn">@lang('AFEGIR')</button>
            <div class="progress-block" style="display: none">
                <div class="progress-container">
                    <div id="progress" class="progress-bar"></div>
                </div>
                <div class="progress-number">0%</div>
            </div>
        </form>
    </div>
@endsection

@section('bottom_foot_scripts')
    <script src="{{ mix('js/vendor/jquery.ui.widget.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>
    <script src="{{ mix('js/jquery.iframe-transport.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>
    <script src="{{ mix('js/jquery.fileupload.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>

    <script>
        var fileMaxSize = {{ $maxSize }};
        var fileMaxSizeMsg = '{{ $maxSizeMsg }}';
        new FileUploader;
    </script>
@endsection