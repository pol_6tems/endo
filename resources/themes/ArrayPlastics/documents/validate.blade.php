@extends('Front::layouts.login_main')

@section('extra_section_classes', auth()->user() ? 'aceptacio canvi' : '')

@section('content')
    <div class="form">
        <form @if ($document->get_field('tipus-validacio') == 'signature')id="document-form"@endif method="POST" enctype="multipart/form-data">
            @csrf

            @if ($document->get_field('tipus-validacio') == 'signature')
                <input id="file_ids" type="hidden" name="file_ids" value="" required>
            @endif


            <h3><img src="{{ asset($_front.'images/edit-icon-title.svg') }}" alt="">@lang('Canvi d\'estat del document')</h3>
            
            @if ($document->get_field('tipus-validacio') == 'signature')
            <div class="alert error">
                @Lang("Aquest document necessita ser firmat abans de validar")
            </div>
            @endif
            
            <p>@lang('Selecciona el nou estat del document')</p>
            

            <div class="radio-pad">
                <div class="radio">
                    <input type="radio" name="estat-validacio" value="validat" id="check1" @if ($validationStatus == 'validat')checked @endif> <label for="check1">@lang('Acceptat / Validat')</label>
                </div>
                <div class="radio">
                    <input type="radio" name="estat-validacio" value="revisio" id="check2" @if ($validationStatus == 'revisio')checked @endif> <label for="check2">@lang('En revisió')</label>
                </div>
                <div class="radio no-accept">
                    <input class="flip" type="radio" id="check3" name="estat-validacio" value="no-acceptat" @if ($validationStatus == 'no-acceptat')checked @endif> <label for="check3">@lang('No acceptat')</label>
                </div>
            </div>
            <div class="panel">
                <p>@lang('Escriu el motiu de la no conformitat')</p>
                <textarea class="txt-box" name="comment" placeholder="@lang('Comentari')"></textarea>
            </div>

            @if ($document->get_field('tipus-validacio') == 'signature')
                <div class="frm-input-ctrl browse">
                    <textarea class="js-file-name" data-default="* @lang('Afegir Documents')" data-full="@lang('Modificar Documents')">* @lang('Afegir Documents')</textarea>
                    <label>
                        <img src="{{ asset($_front.'images/upload-icon-blue.svg') }}">
                        <input id="File" name="files[]" type="file" data-url="{{ route('document.upload-file', ['category' => $category]) }}" multiple>
                    </label>
                </div>

                <div>
                    <ul id="file-list" style="display: none">

                    </ul>
                </div>
            @endif
            <div class="clear"></div>
            <button type="submit" class="mt10">@lang('CANVIAR ESTAT')</button>
            <div class="progress-block" style="display: none">
                <div class="progress-container">
                    <div id="progress" class="progress-bar"></div>
                </div>
                <div class="progress-number">0%</div>
            </div>
        </form>
    </div>
@endsection

@section('bottom_foot_scripts')
    @if ($document->get_field('tipus-validacio') == 'signature')
        <script src="{{ mix('js/vendor/jquery.ui.widget.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>
        <script src="{{ mix('js/jquery.iframe-transport.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>
        <script src="{{ mix('js/jquery.fileupload.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>

        <script>
            var fileMaxSize = {{ $maxSize }};
            var fileMaxSizeMsg = '{{ $maxSizeMsg }}';
            new FileUploader;
        </script>
    @endif
@endsection