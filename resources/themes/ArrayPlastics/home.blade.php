@extends('Front::layouts.main')

@section('content')
    @if (!route_name('index-all') && auth()->user()->rol->level >= 50)
        <div style="margin-bottom: 10px; height: 50px">
            <a href="{{ route('index-all', ['client' => request('client')]) }}" style="background: #4ca6db;
    color: #fff;
    font-size: 14px;
    font-family: Avenir LT Std\ 55 Roman;
    border-radius: 5px;
    padding: 10px 20px;
    float: right;
    border: 0;
    cursor: pointer;
    transition: .3s ease-in-out;
    -webkit-transition: .3s ease-in-out;">@lang('Veure totes les categories')</a>
        </div>
    @endif
    @if (route_name('index-all'))
        <a href="{{ route('index', ['index' => '', 'client' => request('client')]) }}" style="background: #4ca6db;
            color: #fff;
            font-size: 14px;
            font-family: Avenir LT Std\ 55 Roman;
            border-radius: 5px;
            padding: 10px 20px;
            float: right;
            border: 0;
            cursor: pointer;
            transition: .3s ease-in-out;
            -webkit-transition: .3s ease-in-out;">@lang('Tornar')</a>
        <h1>@lang('Veient totes les categories')</h1>
        <p style="margin-bottom: 20px;">@lang('Aquesta vista només és per gestors/usuaris de departament. No es mostren els documents donat que no es té en compte l\'usuari seleccionat')</p>
    @endif
    <div class="accordionWrapper">
        @foreach($categories as $category)
            @php($trash = false)
            <div class="js-cat-container @if ($category->get_field('proveedor-sube'))provider-upload @endif" id="category{{ $category->id }}">
                <div class="head-pad @if ($category == $categories->first())mrgn-top @endif">
                    @if ($locale == "ca")
                        <h2 class="flip open"> {{ $category->title }} <span></span></h2>
                    @else
                        <h2 class="flip open"> {{ $category->get_field('titulo-'.$locale) }} <span></span></h2>
                    @endif
                    <a href="{{ route('document.create', ['category' => $category->post_name, 'client' => request('client')]) }}">@lang('AFEGIR DOCUMENT') <img src="{{ asset($_front.'images/add-coc-icon.svg') }}" alt=""></a>

                    @if ($category->get_field('proveedor-sube'))
                        <div class="provider-upload-msg">
                            <p>@lang('Categoria amb instruccions. Només descarregar l\'arxiu per visualitzar-les.')</p>
                        </div>
                    @endif
                </div>
                <div class="scroll-div js-cat-content">
                    <table class="table-responsive tab-1" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <thead>
                            <tr>
                                <th width="4%">@lang('Descàrrega arxius')</th>
                                <th width="5%">@lang('Info')</th>
                                <th width="10%">@lang('Nom arxiu')</th>
                                <th width="10%">@lang('Propietari')</th>
                                <th width="10%">@lang('Empresa')</th>
                                <th width="10%">@lang('Data Publicació')</th>
                                @if (!$category->get_field('proveedor-sube'))
                                    <th width="10%"><strong>@lang('Data Màxima')</strong></th>
                                    @if (!route_name('index-all'))
                                        <th width="10%">@lang('Data Validació')</th>
                                    @endif
                                @endif

                                @if (!route_name('index-all'))
                                    <th width="10%">@lang('Estat Validació')</th>
                                    <th width="10%">@lang('Validat per')</th>
                                    <th width="10%">@lang('Comentaris')</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                        @if (!route_name('index-all'))
                            @foreach(collect($category->documents)->merge($category->documents_trashed) as $document)
                                @php($creator = $document->post->creator_id ? \App\User::find($document->post->creator_id) : null)
                                @php($author = null)
                                @if (!$creator)
                                    @php($author = $document->post->author)
                                @endif
                                @if (!$trash && $document->trashed())
                                <tr>
                                    <td colspan="100">
                                        <span>@Lang("Veure històric")</span>
                                        <button class="btn-slide"><i class="arrow down"></i></button>
                                    </td>
                                </tr>
                                <tr class="{{ $document->trashed() ? 'trashed' : '' }}">
                                    <td colspan="100" style="padding:0">
                                        <div style="display: none;">
                                            <table class="table-responsive" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <thead>
                                                    <tr>
                                                        <th width="4%">@lang('Descàrrega arxius')</th>
                                                        <th width="5%">@lang('Info')</th>
                                                        <th width="10%">@lang('Nom arxiu')</th>
                                                        <th width="10%">@lang('Propietari')</th>
                                                        <th width="10%">@lang('Empresa')</th>
                                                        <th width="10%">@lang('Data Publicació')</th>
                                                        @if (!$category->get_field('proveedor-sube'))
                                                            <th width="10%"><strong>@lang('Data Màxima')</strong></th>
                                                            <th width="10%">@lang('Data Validació')</th>
                                                        @endif
                                                        <th width="10%">@lang('Estat Validació')</th>
                                                        <th width="10%">@lang('Validat per')</th>
                                                        <th width="10%">@lang('Comentaris')</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                @endif
                                @php($trash = $document->trashed())
                                <tr>
                                    @php($files = $document instanceof \App\Modules\ArrayPlastics\Models\UserDocument ? $document->getAllFiles() : $document->get_field('fitxers'))
                                    @php($file = $document->get_field('fitxer'))
                                    <td>
                                        @if ($files)
                                            @if (count($files) == 1)
                                                <a @if (!$document->downloaded)class="js-download-file" data-doc="{{ $document->id }}" @endif href="{{ $files[0]->get_thumbnail_url() }}" download>
                                                    @if (($document->validation_status != 'pendent' || auth()->user()->rol->level >= 50) && ($document->user_id != $document->post->creator_id || auth()->user()->rol->level >= 50))
                                                        <img src="{{ asset($_front.'images/combined-shape.svg') }}" alt="">
                                                    @else
                                                        <img src="{{ asset($_front.'images/combined-shape-red.svg') }}" alt="">
                                                    @endif
                                                </a>
                                            @else
                                                <span class="js-view-files view-files" data-text="@lang('Veure')">@lang('Veure') +</span>
                                            @endif
                                        @elseif ($file)
                                            <a @if (!$document->downloaded)class="js-download-file" data-doc="{{ $document->id }}"@endif href="{{ $file->get_thumbnail_url() }}" download><img src="{{ asset($_front.'images/combined-shape.svg') }}" alt=""></a>
                                        @endif
                                    </td>
                                    <td>@if ($document->post->description)<a class="fancybox show" href="#popup-div"><textarea class="js-doc-description" style="display: none">{!! $document->post->description !!}</textarea><img src="{{ asset($_front.'images/info-icon.svg') }}" alt=""></a>@endif</td>
                                    <td>{{ $document->post->title }}</td>
                                    <td>{{ $creator ? $creator->fullname() : ($author ? $author->fullname() : '') }}</td>
                                    <td>{{ $creator ? $creator->get_field('empresa') : ($author ? $author->get_field('empresa') : '') }}</td>
                                    <td>{{ $document->post->created_at->format('d/m/Y') }}</td>
                                    @if (!$category->get_field('proveedor-sube'))
                                        <td><strong>{!! $document->max_validation_date ? $document->max_validation_date->format('d/m/Y') : ($document->expiry_date ? $document->expiry_date->format('d/m/Y') . '<div class="signature">' . __('El document s\'eliminarà') .'</div>' : '' )  !!}</strong></td>
                                        <td>{{ $document->validation_date ? $document->validation_date->format('d/m/Y') : '' }}</td>
                                    @endif
                                    <td class="status">
                                        <img class="img-pad{{ $document->validation_status != 'validat' ? '-t' : '' }}" src="{{ asset($_front.'images/' . ($document->validation_status == 'validat' ? 'green-tick-icon' : ($document->validation_status == 'pendent' ? 'blue-circle' : ($document->validation_status == 'trash' ? 'gray-circle' : ($document->validation_status == 'no-acceptat' ? 'red-circle' :'orange-circle')))) . '.svg') }}" alt="">

                                        <span class="rgt-pad">{{ __(ucfirst(str_replace('-', ' ', $document->validation_status))) }}<br>
                                        @if (!$document->trashed())
                                            @if ($document->validation_status != 'validat' && ($document->validation_status != 'pendent' || auth()->user()->rol->level >= 50) && ($document->user_id != $document->post->creator_id || auth()->user()->rol->level >= 50))
                                                <a class="link-pad" href="{{ route('document.validate', ['doc' => $document->post->post_name, 'client' => request('client')]) }}">{{ $document->validation_status == 'pendent' ? __('Validar') : __('Modificar') }}</a>
                                                @if ($document->isSignature())
                                                <div class="signature">
                                                    @Lang("Aquest document necessita ser firmat abans de validar")
                                                </div>
                                                @endif
                                            @endif
                                        @else
                                            <small>@Lang("Eliminat el :data", ["data" => $document->deleted_at->format("d/m/Y")])</small>
                                        @endif
                                        </span>
                                    </td>
                                    <td>{{ $document->validation_status == 'validat' && $document->user ? ($document->validator ? $document->validator->fullname() : $document->user->fullname()) : '' }}</td>
                                    <td>
                                        <a class="comments" href="{{ route('document.comment', ['doc' => $document->post->post_name, 'client' => request('client')]) }}"><img src="{{ asset($_front.'images/edit-icon.svg') }}" alt=""></a>
                                        <a class="fancybox show comments js-show-comments" href="#popup-comments" data-document="{{ $document->post->post_name }}" data-doc="{{ $document->id }}" data-viewed-url="@if ($document->comments->isEmpty()){{ asset($_front.'images/eye-icon.svg') }}@else{{ asset($_front.'images/eye-icon-blue.svg') }}@endif">
                                            @if ($document->areAllCommentsViewed())
                                                @if ($document->comments->isEmpty())
                                                    <img src="{{ asset($_front.'images/eye-icon.svg') }}" alt="">
                                                @else
                                                    <img src="{{ asset($_front.'images/eye-icon-blue.svg') }}" alt="">
                                                @endif
                                            @else
                                                <img src="{{ asset($_front.'images/red-eye-fill.svg') }}" alt="">
                                            @endif
                                        </a>
                                    </td>
                                </tr>

                                @if ($files && count($files) > 1)
                                    <tr class="js-view-files-tr view-files-tr">
                                        <td colspan="11" style="display: none">
                                            <ul>
                                                @php($ids = array_map(function ($f) {
                                                    return $f->id;
                                                }, $files))
                                                @foreach($files as $file)
                                                    <li>
                                                        <a @if (!$document->downloaded)class="js-download-file" data-doc="{{ $document->id }}"@endif href="{{ $file->get_thumbnail_url() }}" download><img src="{{ asset($_front.'images/combined-shape.svg') }}" alt="">{{ $file->file_name }} <span class="file-type">{{ strtoupper($file->file_type) }}</span> <span class="file-size">{{ file_exists($file->getFilePath()) ? formatSizeUnits(filesize($file->getFilePath())): formatSizeUnits(0) }}</span></a>
                                                        @if ($document instanceof \App\Modules\ArrayPlastics\Models\UserDocument && $document->isValidationFile($file->id))
                                                            <span class="pull-right"><a href="#delete-modal" class="fancybox show js-delete-doc del-doc" data-toggle="modal" data-target="#desistModal" data-doc="{{ $document->id }}" data-media="{{ $file->id }}">@lang('Eliminar')</a> </span>
                                                        @endif
                                                    </li>
                                                @endforeach
                                                <li>
                                                    <a class="download-all-btn @if (!$document->downloaded)js-download-file @endif" data-doc="{{ $document->id }}" href="{{ route('home.download-files', ['ids' => implode(',', $ids), 'filename' => str_slug($document->post) . '.zip']) }}" download><img src="{{ asset($_front.'images/combined-shape-white.svg') }}" alt=""><span>@lang('Descarregar tots')</span></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endif
                                @if ($trash && $loop->last)
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @else
                            @foreach(collect($category->author_documents) as $document)
                                @php($creator = $document->creator_id ? \App\User::find($document->creator_id) : null)
                                @php($author = null)
                                @if (!$creator)
                                    @php($author = $document->author)
                                @endif

                                    <tr>
                                        @php($files = $document instanceof \App\Modules\ArrayPlastics\Models\UserDocument ? $document->getAllFiles() : $document->get_field('fitxers'))
                                        @php($file = $document->get_field('fitxer'))
                                        <td>
                                            @if ($files)
                                                @if (count($files) == 1)
                                                    <a href="{{ $files[0]->get_thumbnail_url() }}" download>
                                                        <img src="{{ asset($_front.'images/combined-shape.svg') }}" alt="">
                                                    </a>
                                                @else
                                                    <span class="js-view-files view-files" data-text="@lang('Veure')">@lang('Veure') +</span>
                                                @endif
                                            @elseif ($file)
                                                <a href="{{ $file->get_thumbnail_url() }}" download><img src="{{ asset($_front.'images/combined-shape.svg') }}" alt=""></a>
                                            @endif
                                        </td>
                                        <td>@if ($document->description)<a class="fancybox show" href="#popup-div"><textarea class="js-doc-description" style="display: none">{!! $document->description !!}</textarea><img src="{{ asset($_front.'images/info-icon.svg') }}" alt=""></a>@endif</td>
                                        <td>{{ $document->title }}</td>
                                        <td>{{ $creator ? $creator->fullname() : ($author ? $author->fullname() : '') }}</td>
                                        <td>{{ $creator ? $creator->get_field('empresa') : ($author ? $author->get_field('empresa') : '') }}</td>
                                        <td>{{ $document->created_at->format('d/m/Y') }}</td>
                                        @if (!$category->get_field('proveedor-sube'))
                                            <td><strong>{!! $document->get_field('periode-maxim-validacio') ? __($document->get_field('periode-maxim-validacio')) : ($document->get_field('data-caducitat') ? $document->get_field('data-caducitat') . '<div class="signature">' . __('El document s\'eliminarà') .'</div>' : '' )  !!}</strong></td>
                                        @endif
                                    </tr>

                                    @if ($files && count($files) > 1)
                                        <tr class="js-view-files-tr view-files-tr">
                                            <td colspan="11" style="display: none">
                                                <ul>
                                                    @php($ids = array_map(function ($f) {
                                                        return $f->id;
                                                    }, $files))
                                                    @foreach($files as $file)
                                                        <li>
                                                            <a href="{{ $file->get_thumbnail_url() }}" download><img src="{{ asset($_front.'images/combined-shape.svg') }}" alt="">{{ $file->file_name }} <span class="file-type">{{ strtoupper($file->file_type) }}</span> <span class="file-size">{{ file_exists($file->getFilePath()) ? formatSizeUnits(filesize($file->getFilePath())): formatSizeUnits(0) }}</span></a>
                                                        </li>
                                                    @endforeach
                                                    <li>
                                                        <a class="download-all-btn" data-doc="{{ $document->id }}" href="{{ route('home.download-files', ['ids' => implode(',', $ids), 'filename' => str_slug($document->post) . '.zip']) }}" download><img src="{{ asset($_front.'images/combined-shape-white.svg') }}" alt=""><span>@lang('Descarregar tots')</span></a>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    @endif
                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                    </table>

                    <!-------------------->
                    <div class="mob-table">
                        <ul class="head-tit">
                            <li>@lang('PDF')</li>
                            <li>@lang('Info')</li>
                            <li>@lang('Nom arxiu')</li>
                            <li>@lang('Estat Validació')</li>
                            <li></li>
                        </ul>
                        @foreach($category->documents as $document)
                            @php($creator = $document->post->creator_id ? \App\User::find($document->post->creator_id) : null)

                            <div class="js-mobcat-container">
                                <ul>
                                    @php($files = $document instanceof \App\Modules\ArrayPlastics\Models\UserDocument ? $document->getAllFiles() : $document->get_field('fitxers'))
                                    @php($file = $document->get_field('fitxer'))
                                    <li>
                                        @if ($files)
                                            @if (count($files) == 1)
                                                <a @if (!$document->downloaded)class="js-download-file" data-doc="{{ $document->id }}"@endif href="{{ $files[0]->get_thumbnail_url() }}" download>
                                                    @if (($document->validation_status != 'pendent' || auth()->user()->rol->level >= 50) && ($document->user_id != $document->post->creator_id || auth()->user()->rol->level >= 50))
                                                        <img src="{{ asset($_front.'images/combined-shape.svg') }}" alt="">
                                                    @else
                                                        <img src="{{ asset($_front.'images/combined-shape-red.svg') }}" alt="">
                                                    @endif
                                                </a>
                                            @else
                                                @php($ids = array_map(function ($f) {
                                                    return $f->id;
                                                }, $files))
                                                <a @if (!$document->downloaded)class="js-download-file" data-doc="{{ $document->id }}"@endif href="{{ route('home.download-files', ['ids' => implode(',', $ids), 'filename' => str_slug($document->post) . '.zip']) }}" download><img src="{{ asset($_front.'images/combined-shape.svg') }}" alt=""></a>
                                            @endif
                                        @elseif ($file)
                                            <a @if (!$document->downloaded)class="js-download-file" data-doc="{{ $document->id }}"@endif href="{{ $file->get_thumbnail_url() }}" download><img src="{{ asset($_front.'images/combined-shape.svg') }}" alt=""></a>
                                        @endif
                                    </li>
                                    <li>@if ($document->post->description)<a class="fancybox show" href="#popup-div"><textarea class="js-doc-description" style="display: none">{!! $document->post->description !!}</textarea><img src="{{ asset($_front.'images/info-icon.svg') }}" alt=""></a>@endif</li>
                                    <li>{{ $document->post->title }}</li>
                                    <li {!! $document->validation_status != 'validat' ? 'class="mob-circle"' : '' !!} ><img class="img-pad{{ $document->validation_status != 'validat' ? '-t' : '' }}" src="{{ asset($_front.'images/' . ($document->validation_status == 'validat' ? 'green-tick-icon' : ($document->validation_status == 'pendent' ? 'blue-circle' : ($document->validation_status == 'trash' ? 'gray-circle' : ($document->validation_status == 'no-acceptat' ? 'red-circle' :'orange-circle')))) . '.svg') }}" alt=""> <span class="">{{ __(ucfirst(str_replace('-', ' ', $document->validation_status))) }}</span> @if ($document->validation_status != 'validat' && ($document->validation_status != 'pendent' || auth()->user()->rol->level >= 50) && ($document->user_id != $document->post->creator_id || auth()->user()->rol->level >= 50))<span class="rgt-pad"><a class="link-pad" href="{{ route('document.validate', ['doc' => $document->post->post_name, 'client' => request('client')]) }}">{{ $document->validation_status == 'pendent' ? __('Validar') : __('Modificar') }}</a></span> @endif</li>
                                    <li><span class="plus-ico"></span></li>
                                </ul>
                                <div class="scroll-div hide-sect js-mobcat-content" style="display:none;">
                                    <ul>
                                        <li>@lang('Propietari') <span class="rgt-array">{{ $creator ? $creator->fullname() : ($author ? $author->fullname() : '') }}</span></li>
                                        <li>@lang('Validat per') <span class="rgt-array">{{ $document->validation_status == 'validat' && $document->user ? ($document->validator ? $document->validator->fullname() : $document->user->fullname()) : '' }}</span></li>
                                        <li>@lang('Data Publicació') <span class="rgt-array">{{ $document->post->created_at->format('d/m/Y') }}</span></li>
                                        @if (!$category->get_field('proveedor-sube'))
                                            <li>@lang('Data Màxima Validació/Caducitat') <span class="rgt-array">{{ $document->max_validation_date ? $document->max_validation_date->format('d/m/Y') : ($document->expiry_date ? $document->expiry_date->format('d/m/Y') : '' ) }}</span></li>
                                            <li>@lang('Data Validació') <span class="rgt-array">{{ $document->validation_date ? $document->validation_date->format('d/m/Y') : '' }}</span></li>
                                        @endif
                                        <li>@lang('Comentaris') <span class="rgt-array comments">
                                                <a href="{{ route('document.comment', ['doc' => $document->post_name, 'client' => request('client')]) }}">
                                                    <img src="{{ asset($_front.'images/edit-icon.svg') }}" alt="">
                                                </a>
                                                <a class="fancybox show js-show-comments" href="#popup-comments" data-document="{{ $document->post->post_name }}" data-doc="{{ $document->id }}" data-viewed-url="@if ($document->comments->isEmpty()){{ asset($_front.'images/eye-icon.svg') }}@else{{ asset($_front.'images/eye-icon-blue.svg') }}@endif">
                                                    @if ($document->areAllCommentsViewed())
                                                        @if ($document->comments->isEmpty())
                                                            <img src="{{ asset($_front.'images/eye-icon.svg') }}" alt="">
                                                        @else
                                                            <img src="{{ asset($_front.'images/eye-icon-blue.svg') }}" alt="">
                                                        @endif
                                                    @else
                                                        <img src="{{ asset($_front.'images/red-eye-fill.svg') }}" alt="">
                                                    @endif
                                                </a></span></li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach

        <div id="popup-div" class="popup">
            <img src="{{ asset($_front.'images/logo-array-plastics-mini.svg') }}" alt="ARRAY PLASTICS">
            <h3><img src="{{ asset($_front.'images/info-icon.svg') }}" alt=""> @lang('Més informació')</h3>
            <div class="content demo-y js-popup-description">
                <p></p>
            </div>
        </div>

        <div id="popup-comments" class="popup">
            <img src="{{ asset($_front.'images/logo-array-plastics-mini.svg') }}" alt="ARRAY PLASTICS">
            <h3><img src="{{ asset($_front.'images/info-icon.svg') }}" alt=""> @lang('Més informació')</h3>
            <div class="content demo-y js-popup-comment">
            </div>
        </div>
    </div>

    @include('Front::partials.delete-modal')
@endsection

@section('bottom_foot_scripts')
    <script>
        var commentsUrl = '{{ route('document.comments', ['client' => request('client'),'doc' => '::doc']) }}';
        var documentDownloadUrl = '{{ route('document.download') }}';
        var documentCommentsShowUrl = '{{ route('document.view-comments') }}';
        $(".btn-slide").click(function() {
            $(this).find("i").toggleClass("down");
            $(this).find("i").toggleClass("up");

            $(".trashed>td>div").slideToggle(500);
        })
    </script>
@endsection