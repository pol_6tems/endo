@extends('Front::layouts.html')

@php
    $user = auth()->user();
    $locale = null;

    if ($user) {
        $locale = $user->get_field('idioma');
    }

    if (!$locale) {
        $locale = request()->cookie('user_lang');
    }

    if (!$locale) {
        $lang = substr(request()->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
        $acceptLang = ['ca', 'es', 'en'];
        $locale = in_array($lang, $acceptLang) ? $lang : 'en';
    }


    if ($locale == 'ca') {
        $description = $item->description;
    } else {
        $description = $item->get_field('descripcio-' . $locale);
    }
@endphp

@section('head_metas')
    <meta name="robots" content="noindex,nofollow" />
@endsection

@section('body')
<div class="content-page">
    <div class="header">
        <a href="{{ route('index') }}">
            <img src="{{ asset($_front.'images/logo-array-plastics.svg') }}" alt="ARRAY PLASTICS">
        </a>
    </div>

    <div class="header bckg" style="background-image: url('{{ asset($_front. "images/2017_JMF2615.jpg") }}');">
        <div class="row">
            <h1>@lang($item->title)</h1>
        </div>
    </div>
    <div class="content">
        <div class="breadcrumbs">
            <div class="row">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route("index") }}">@Lang("Home")</a></li>
                    <li class="breadcrumb-item">@Lang($item->title)</li>
                </ul>
            </div>
        </div>
        <div class="row instructions-container">
            {!! strip_tags($description, "<br><p><h1><h2><h3><h4><h5><h6><img><b><ul><li><div><span>") !!}
        </div>
    </div>
</div>
@endsection