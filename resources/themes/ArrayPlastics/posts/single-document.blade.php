@extends('Front::layouts.login_main')

@section('extra_section_classes', auth()->user() ? 'aceptacio canvi' : '')

@section('content')
    @if (auth()->user())
        <div class="form">
            <form method="POST" action="{{ route('doc.comments.store', ['doc' => $item->post_name]) }}">
                @csrf

                <input type="hidden" name="post_id" value="{{ $item->id }}">
                <input type="hidden" name="redirect_to" value="{{ route('index', request('client') ? ['index' => '', 'client' => request('client')] : []) }}">
                <input type="hidden" name="client" value="{{ request('client') }}">

                <h3><img src="{{ asset($_front.'images/edit-icon-title.svg') }}" alt="">@lang('Afegir Comentari')</h3>
                <p>@lang('Completa les dades per afegir un comentari')</p>
                <ul>
                    <li>@lang('Document'): {{ $item->title }}</li>
                </ul>

                <textarea class="txt-box" name="comment" placeholder="@lang('Comentari')" required></textarea>
                <div class="clear"></div>
                <button type="submit" class="mt10">@lang('AFEGIR')</button>
            </form>
        </div>
    @endif
@endsection