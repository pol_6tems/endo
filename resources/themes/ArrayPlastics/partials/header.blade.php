@if (auth()->user())
    <header>
        <div class="row">
            <div class="head-left">
                <div class="logo"> <a href="{{ route('index', auth()->user()->role == 'gestor' && request('client') ? ['index' => '', 'client' => request('client')] : []) }}"><img src="{{ asset($_front.'images/logo-array-plastics.svg') }}" alt="ARRAY PLASTICS"></a> </div>
                <ul>
                    <li>@lang('Extranet<br> gestió documental')</li>
                    <li class="user-name"><a href="{{ route('user.edit') }}"><div><span>{{ substr(auth()->user()->name, 0, 1) }}</span> @lang('Benvingut :name', ['name' => auth()->user()->name])</div></a></li>
                </ul>
            </div>
            <div class="head-rgt">
                @if (isset($clients) && $clients->count())
                    <div class="descar">
                        <select class="js-change-client js-select2" data-route="{{ request()->url() }}">
                            @foreach($clients as $client)
                                <option value="{{ $client->id }}" @if (request('client') == $client->id )selected @else {{ !request('client') && auth()->user()->id == $client->id ? 'selected' : ''}} @endif>{{ $client->fullname() }}</option>
                            @endforeach
                        </select>
                        {{--<a href="javascript:void(0);">@lang('DESCARREGA EXCEL')</a>--}}
                    </div>
                @endif
                <div class="language-container">
                    <form method="POST" action="{{ route('user.update-language') }}">
                        @csrf

                        @if (auth()->user()->rol->level >= 50)
                            <input type="hidden" name="client" value="{{ request('client') }}">

                            @if (route_name('index'))
                                <input type="hidden" name="is_index" value="1">
                            @endif
                        @endif

                        <select class="js-lang-select" name="lang" data-route="{{ request()->url() }}">
                            @foreach($languages as $language)
                                <option value="{{ $language['code'] }}" @if ($language['code'] == $locale)selected @endif>{{ $language['name'] }}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
                <div class="instruccions">
                    <a href="{{ get_page_url("instruccions") }}" target="_blank"><img src="{{ asset($_front.'images/info-24px.svg') }}" alt="Instruccions"></a>
                </div>
                <div class="sortir">
                    <a href="javascript:void(0);" onclick="$('#logout-form').submit();">@lang('Sortir')</a>
                </div>
            </div>
        </div>
    </header>

    <form id="logout-form" action="{{ route('logout') }}" method="POST">
        @csrf
    </form>
@endif