<div id="delete-modal" class="popup">
    <h3>@lang('Eliminar documento')</h3>
    <div class="content">
        <p>
            @lang('¿Está seguro que desea eliminar el documento?')
        </p>

        <div class="modal-buttons">
            <form method="POST" action="{{ route('document.delete-validation-document') }}">
                @csrf
                <input type="hidden" class="js-input-user-document" name="user_document" value="">
                <input type="hidden" class="js-input-media" name="media" value="">

                <button type="button" class="js-close-fancy fancy-btn fancy-cancel">@lang('Cancelar')</button>
                <button type="submit" class="fancy-btn fancy-delete">@lang('Eliminar')</button>
            </form>
        </div>
    </div>
</div>