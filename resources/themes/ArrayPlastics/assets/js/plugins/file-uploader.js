(function(window, $) {
    var FileUploader = function() {
        this.init();
    };

    FileUploader.prototype = {
        init: function () {
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });

            this.bindings();
        },

        bindings: function () {
            var documentForm = $('#document-form');
            var me = this;

            var fileTextArea = $('.js-file-name');

            $('#File').fileupload({
                dataType: 'json',
                singleFileUploads: false,
                /*replaceFileInput: false,*/
                add: function (e, data) {
                    var totalSize = 0;
                    var fileListUl = $('#file-list');

                    $.each(data.originalFiles, function (index, file) {
                        if (typeof file['size'] !== 'undefined') {
                            totalSize += file['size'];
                        }
                    });

                    fileListUl.html('').hide();
                    fileTextArea.html(fileTextArea.data('default'));

                    $.each(data.files, function (index, file) {
                        var filename = file.name;
                        var filesize = me.bytesToSize(file.size);
                        var node = $('<li/>').append($('<div/>').html('<span>' + filename + ' - ' + filesize + '</span><span class="remove-file"><a href="javascript:void(0);">&times</a></span>')).attr('data-index',index);

                        node.find('a').click(function(e){
                            e.preventDefault();
                            var $self = $(this);
                            var $listItem = $self.parents('li');
                            var listIndex = $listItem.attr('data-index');
                            $listItem.remove();
                            $('#file-list li').attr('data-index',function(index){return index;});
                            data.files.splice(listIndex, 1);

                            if (!data.files.length) {
                                fileListUl.hide();
                                fileTextArea.html(fileTextArea.data('default'));
                            }

                            console.log(data.originalFiles);
                        });

                        fileListUl.append(node);
                    });

                    if (data.files.length) {
                        fileListUl.show();
                        fileTextArea.html(fileTextArea.data('full'));
                    }

                    if (totalSize > fileMaxSize) {
                        toastr.error(fileMaxSizeMsg);
                    }

                    data.context = documentForm.off('submit').on('submit', function (e) {
                        e.preventDefault();
                        var isOk = true;
                        var totalSize = 0;

                        $.each(data.originalFiles, function (index, file) {
                            if (typeof file['size'] !== 'undefined') {
                                totalSize += file['size'];
                            }
                        });

                        if (totalSize > fileMaxSize) {
                            isOk = false;
                            toastr.error(fileMaxSizeMsg);
                        }

                        if (isOk /*&& data.files.length*/) {
                            data.submit();
                        }
                    });
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);

                    $('.progress-block').show();
                    $('.submit-btn').hide();
                    $('#progress').css('width', progress + '%');
                    $('.progress-number').html(progress + '%');
                },
                done: function (e, data) {
                    $('.progress-block').hide();

                    var fileIdsInput = $('#file_ids');

                    $.each(data.result.files, function (index, file) {
                        if (fileIdsInput.val() != '') {
                            fileIdsInput.val(fileIdsInput.val() + ',');
                        }
                        fileIdsInput.val(fileIdsInput.val() + file.id);
                    });

                    /*$('#file_id').val(data.result.id);*/
                    documentForm.unbind('submit').submit();

                    $('#progress').css('width', 0);
                    $('.progress-number').html('');
                },
                error: function (data) {
                    console.log('error');
                    console.log(data);
                    $('.progress-block').hide();
                    $('.submit-btn').show();
                    $.each(data.errors.file, function(index, message) {
                        toastr.error(message);
                    });
                    $('#progress').css('width', 0);
                    $('.progress-number').html('');
                }
            });
        },

        bytesToSize: function (bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }
    };

    if (typeof self !== 'undefined') {
        self.FileUploader = FileUploader;
    }

    // Expose as a CJS module
    if (typeof exports === 'object') {
        module.exports = FileUploader;
    }
})(window, $);