@extends('Front::layouts.login_main')

@section('extra_login_class', 'recuperar')

@section('content')
	<div class="form">
		<form method="POST" action="{{ route('password.request') }}">
			@csrf

			<input type="hidden" name="token" value="{{ $token }}">

			<img src="{{ asset($_front.'images/logo-array-plastics.svg') }}" alt="ARRAY PLASTICS">
			<h3>@lang('Actualizació de contrasenya')</h3>
			<label>@lang('Email')</label>
			<input type="text" name="email" class="frm-ctrl" placeholder="ex. arrayplastics@email.com" required @if ($email)value="{{ $email }}" readonly @endif>
			<label>@lang('Paraula clau')</label>
			<div class="clear"></div>
			<div class="password-pad">
				<input type="password" name="password" class="frm-ctrl" placeholder="********" required><a class="js-view-pass" href="javascript:void(0);"></a>
			</div>
			<label>@lang('Paraula clau')</label>
			<div class="clear"></div>
			<div class="password-pad">
				<input type="password" name="password_confirmation" class="frm-ctrl" placeholder="********" required><a class="js-view-pass" href="javascript:void(0);"></a>
			</div>
			<button type="submit" class="mt10">@lang('ENVIAR')</button>
			<div class="clear"></div>
		</form>
	</div>
@endsection
