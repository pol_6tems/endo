<?php
/**
 * @title: Selection
 */
?>

@php
    // Post Actual
    $posts = array();
    $max = null;
    $min = null;
    
    // CUSTOM POSTS
    if ( isset($params) && isset($params->post_object) && $params->post_object != 'user' && $params->post_object != 'any') {
        
        $post_type = $params->post_object;

        if ($currentCP = \App\Models\CustomPost::where('id', $params->post_object)->first()) {
            $post_type = $currentCP->post_type;
        }

        if ($params->multiple) {
            $max = (isset($params->max_length) && $params->max_length) ? $params->max_length : '';
            $min = (isset($params->min_length) && $params->min_length) ? $params->min_length : '';
            if ($value && is_object($value)) {
                $value = array_map(function($k) { return $k['id']; }, $value->toArray());
            } else {
                $value = json_decode($value, true);
            }
        }

        if ( isset($params) && isset($params->parent_param) && $params->parent_param ) {
            $CF_remote = \App\Models\CustomField::find($params->parent_param);

            // Current Page Post Type
            $pt = isset(request()->post_type) ? request()->post_type : 'post';
            $cfs = \App\Models\CustomFieldGroup::get_cfs_by_post_type($pt);
        
            $CF_post_actual = collect($cfs)->filter(function($element) use ($CF_remote) {
                return $element->name == $CF_remote->name && $element->type == 'selection';
            })->first();

            if ( !empty($post) && $relacio = $post->get_field($CF_remote->name) ) {
                $posts = \App\Post::where('status', 'publish')->where('type', $post_type)->withTranslation()->get();
                // Buscar custom field name
                $field_name = '';
                // Custom Fields de Habitacions obtenim tots els que poden crear una relacio
                foreach(\App\Models\CustomFieldGroup::get_cfs_by_post_type($post_type) as $cf) {
                    // Agafem els parametres per buscar el post al que apunta
                    $params2 = json_decode($cf->params);
                    
                    // Si són de tipus seleccio "Apunten a un altre objecte"
                    if ( $cf->type == 'selection' && !$params2->multiple ) {
                        // Obtenim el custom post al que relaciona ()
                        $cp = App\Models\CustomPost::where('id', $params2->post_object)->first();
                        if (!is_numeric($relacio) && $cp->post_type == $relacio->type) {
                            $field_name = $cf->name;
                        }
                    }
                }

                if ($field_name != '') {
                    $aux = [];
                    foreach($posts as $i => $p) {
                        $p_aux = $p->get_field($field_name);

                        if ( !empty($p_aux->id) && $p_aux->id == $relacio->id) $aux[] = $posts[$i];
                    }
                    $posts = $aux;
                }
            } else $posts = [];
        } else {
            // Llista de Posts
            if ( isset($params) && isset($params->permision) && $params->permision == 'only_yours' && !Auth::user()->isAdmin()) {
                $posts = \App\Post::where('status', 'publish')->where('type', $post_type)->where('author_id', Auth::id())->withTranslation()->get();
            } else if ( isset($params) && isset($params->permision) && $params->permision == 'only_custom_user_id' && isset($custom_user_id) && !auth()->user()->isAdmin()) {
                $posts = \App\Post::where('status', 'publish')->where('type', $post_type)->where('author_id', $custom_user_id)->withTranslation()->get();
            } else {
                $posts = \App\Post::where('status', 'publish')->where('type', $post_type)->withTranslation()->get();
            }
        }

    } else if ( !empty($params->post_object) ) {
        if ( $params->post_object == 'user' && !empty($params->role) ) {
            $role_id = $params->role;
            if ($custom_field->name == 'responsable') {
                $role = \App\Models\Admin\Rol::find($role_id);

                $posts = \App\User::whereHas('rol', function($query) use ($role) {
                    $query->where('level', '>=', $role->level);
                })->get();
            } else {
                $posts = \App\User::whereHas('rol', function($query) use ($role_id) {
                    $query->where('id', $role_id);
                })->get();
            }

        } else if ( $params->post_object == 'any' ) {
            $posts = \App\Models\CustomPost::all();
            foreach ( $posts as &$aux ) $aux->title .= ' ('. $aux->post_type . ')';
        }
        else {
            $posts = \App\User::get();
            foreach ( $posts as &$aux ) $aux->title = $aux->fullname();
        }

        if ( $params->multiple ) {
            if (is_a($value, 'Illuminate\Database\Eloquent\Collection')) $value = $value->pluck("id")->all();
        }
    }

    if ($custom_field->name == 'validador' && !auth()->user()->isAdmin() && $params->permision == 'only_custom_user_id' && isset($custom_user_id)) {
        $posts = $posts->filter(function ($item) use ($custom_user_id) {
            $grupGestors = $item->get_field('grup-gestor');

            if ($grupGestors && $grupGestors->count()) {
                $grupGestor = $grupGestors->first();
                $grupAuthor = $grupGestor->get_field('gestor');
            }
            return (isset($grupAuthor) && $grupAuthor && $grupAuthor->id == $custom_user_id) || auth()->user()->id == $item->id;
        });
    }


    $required_title = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? '*' : '';
    $required = (isset($params->required) && $params->required && (isset($lang) && $lang == app()->getLocale())) ? 'required' : '';

    $id_element = 'cf' . $custom_field->id;
    if ($custom_field->parent_id) {
        $id_element .= '_k' . $k;
    }
@endphp

<div id="{{ $id_element }}" data-id="{{ $custom_field->id }}" class="custom-field selection-field {{ (isset($params) && isset($params->parent_param) && $params->parent_param) ? 'relation' : '' }}">
    <div class="flex-row pt-3">
        <label class="col-md-12">
            {{ __($title) }} {{ $required_title }}
            @if (Auth::user()->role == 'admin')
                (<button class="copy" type="button">{{ $custom_field->name }}</button>)
            @endif
            @if (isset($min) && $min != '') - Min: {{ $min }}@endif
            @if (isset($max) && $max != '') - Max: {{ $max }}@endif
            
            <a data-toggle="collapse" href="#cf_{{ $name }}"><i class="material-icons">keyboard_arrow_down</i></a>

            @if (isset($params->instructions_param) && $params->instructions_param)
                <span>{{ $params->instructions_param }}</span>
            @endif
        </label>
        <div class="col-md-12">
            <div id="cf_{{ $name }}">
                <div class="form-group">
                    @if( isset($params) && isset($params->multiple) && $params->multiple)
                        @if(isset($params) && isset($params->choices) && $params->choices != '')
                            @php($choices = explode("\r\n", $params->choices))
                            @foreach($choices as $k_multi => $choice)
                                @php ( $choice = explode(":", $choice) )
                                @if (count($choice) > 1)
                                <input type="hidden" name="{{ $name }}[{{ $choice[0] }}]" value="0" />
                                <div class="togglebutton">
                                    <label>
                                        <input
                                                type="checkbox"
                                                name="{{ $name }}[{{ $choice[0] }}]"
                                                value="{{ $choice[0] }}"
                                                {{ isset($value[$choice[0]]) && $value[$choice[0]] ? 'checked' : '' }} />

                                        <span class="toggle"></span>
                                        {{ $choice[1] }}
                                    </label>
                                </div>
                                @endif
                            @endforeach
                        @else
                            @php($posts = $posts->sortBy('order'))
                            @foreach ( $posts as $k_multi => $choice )
                                <input type="hidden" name="{{ $name }}[{{$k_multi}}]" value="0" />
                                <div class="togglebutton">
                                    <label>
                                        @php ( $choice_title = $choice->title )
                                        <input
                                            type="checkbox"
                                            name="{{ $name }}[{{$k_multi}}]"
                                            value="{{ $choice->id }}"
                                            {{ isset($value) && is_array($value) && in_array($choice->id, $value) ? 'checked' : '' }} />

                                        <span class="toggle"></span>
                                        {{ $choice_title }}
                                    </label>
                                </div>
                            @endforeach
                        @endif
                    @else
                        <select
                            name="{{ $name }}"
                            class="selectpicker"
                            data-style="btn btn-primary"
                            data-live-search="true"
                            {{ $required }}>
                            @if (!$required)
                                <option value="0">@lang('Ninguna')</option>
                            @endif

                            @if(isset($params) && isset($params->choices) && $params->choices != '')
                                @php($choices = explode("\r\n", $params->choices))
                                @foreach($choices as $choice)
                                    @php ( $choice = explode(":", $choice) )
                                    
                                    <option value="{{ $choice[0] }}" {{ ( /* !empty($value->id) && */ $value == $choice[0]) ? 'selected' : '' }}>{!! $choice[1] !!}</option>
                                @endforeach
                            @else
                                @if ($posts)
                                    @foreach($posts as $post)
                                        @php ( $choice_aux = $post )
                                        @if (is_a($value, 'Illuminate\Database\Eloquent\Collection'))
                                            @php ( $value = $value->first() )
                                        @endif

                                        <option value="{{ $post->id }}" {{ (!empty($value->id) && $value->id == $post->id) ? 'selected' : '' }}>{{ $choice_aux->title ?? $choice_aux->fullname() ?? $choice_aux->id }}</option>
                                    @endforeach
                                @endif
                            @endif
                        </select>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

{{-- PRIMERA IDEA PER INCLOURE SCRIPTS ESPECIFICS PE CADA CF --}}
@section("scripts-$id_element")
@if (isset($CF_post_actual) && $CF_post_actual)
<script>
    function {{ $id_element }}_obtenirDades(selects, parent) {
        $.ajax({
            url: "{{ route('ajax') }}",
            method: 'POST',
            data: {
                action: 'getParentChildrenData',
                parameters: {
                    post_type: "{{ $post_type }}",
                    parent: parent,
                    customField: "{{ $CF_remote->name }}"
                }
            },
            success: function(data) {
                var children = JSON.parse(data.result);
                var options = `<option value="0">@lang('No') {{ __($title) }}</option>`;
                children.forEach(function(e) {
                    options += `<option value="${e.id}">${e.title}</option>`
                });
                
                selects.html(options);
                selects.selectpicker('refresh');
            }
        });
    }
    
    $(document).on('repeater_add_field', function(e) {
        if ($("#cf"+e.detail.id).find('.relation').length) {
            var selects = $("#cf"+e.detail.id).find('.selectpicker');
            var parent = $("#cf{{ $CF_post_actual->id }}").find('.selectpicker');
            {{ $id_element }}_obtenirDades(selects, parent.val())
        }
    });

    $(function() {
        $("#cf{{ $CF_post_actual->id }}").find('.selectpicker').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            var id_new = $(this).children().get(clickedIndex).value;
            @if ($custom_field->parent_id)
                selects = $("#{{ $id_element }}").closest('.repeater-field').find('.selectpicker');
            @else
                selects = $("#{{ $id_element }}").find('.selectpicker');
            @endif
            {{ $id_element }}_obtenirDades(selects, id_new);
        });
    });
</script>
@endif

@if (isset($max) && is_numeric($max))
<script>
$("#{{ $id_element }} .togglebutton input[type='checkbox']").click(function(e) {
    $clicats = $(this).closest('.form-group').find('input[type="checkbox"]:checked').length;
    if ($clicats > {{ $max }}) {
        e.preventDefault();
    }
})
</script>
@endif

    @if (isset($min) && is_numeric($min))
        <script>
            var sel{{ $custom_field->id }} = $('#{{ $id_element }}');
            sel{{ $custom_field->id }}.closest('form').on('submit', function (e) {

                if ( $(this).find('input[type=hidden][name=status]').val() != 'draft' ) {

                    if ( $('#{{ $id_element }} input:checkbox:checked').length < {{ $min }} ) {
                        e.preventDefault();
                        sel{{ $custom_field->id }}.closest('.collapse').removeClass('collapse');
                        $("html, body").animate({ scrollTop: $('#{{ $id_element }}').offset().top }, 300);
                        $('#{{ $id_element }} label:first').css('color', 'red');
                    }

                }
            });
        </script>
    @endif
@endsection