@extends('Admin::layouts.admin')

@section('section-title')
    @lang('Resum validació')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">bubble_chart</i>
                    </div>
                    <h4 class="card-title">
                        @lang('Resum validació')
                    </h4>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>@lang('Nom')</th>
                            <th>@lang('Email')</th>
                            <th>@lang('Empresa')</th>
                            <th>@lang('Departament')</th>
                            <th>@lang('Rol')</th>
                            <th>@lang('En revisió')</th>
                            <th>@lang('No acceptats')</th>
                            <th>@lang('Pendents')</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($groupedDocs as $groupDocs)
                                <tr>
                                    <td><a href="{{ route('index', ['index' => '', 'client' => $groupDocs->first()->user->id]) }}" target="_blank">{{ $groupDocs->first()->user->fullname() }}</a></td>
                                    <td>{{ $groupDocs->first()->user->email }}</td>
                                    <td>{{ $groupDocs->first()->user->get_field('empresa') }}</td>
                                    <td>{{ $groupDocs->first()->user->get_field('departament') }}</td>
                                    <td>{{ $groupDocs->first()->user->role }}</td>
                                    <td>{{ $groupDocs->where('validation_status', 'revisio')->count() }}</td>
                                    <td>{{ $groupDocs->where('validation_status', 'no-acceptat')->count() }}</td>
                                    <td>{{ $groupDocs->where('validation_status', 'pendent')->count() }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">turned_in</i>
                    </div>
                    <h4 class="card-title">
                        @lang('Resum per categoria')
                    </h4>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>@lang('Nom')</th>
                            <th>@lang('En revisió')</th>
                            <th>@lang('No acceptats')</th>
                            <th>@lang('Pendents')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($groupedByCategoryDocs as $groupByCategoryDocs)
                            <tr>
                                <td>{{ $groupByCategoryDocs->first()->post->get_field('categoria')->title }}</td>
                                <td>{{ $groupByCategoryDocs->where('validation_status', 'revisio')->count() }}</td>
                                <td>{{ $groupByCategoryDocs->where('validation_status', 'no-acceptat')->count() }}</td>
                                <td>{{ $groupByCategoryDocs->where('validation_status', 'pendent')->count() }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">details</i>
                    </div>
                    <h4 class="card-title">
                        @lang('Detall documents')
                    </h4>
                </div>

                <div class="card-body">
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                        <tr>
                            <th>@lang('Document')</th>
                            <th>@lang('Categoria')</th>
                            <th>@lang('Nom usuari')</th>
                            <th>@lang('Empresa')</th>
                            <th>@lang('Departament')</th>
                            <th>@lang('Rol')</th>
                            <th>@lang('Estat')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($groupedDocs as $groupDocs)
                            @foreach($groupDocs as $doc)
                                <tr>
                                    <td><strong>{{ $doc->post->title }}</strong></td>
                                    <td>{{ ($doc->post->get_field('categoria') && !is_numeric($doc->post->get_field('categoria'))) ? $doc->post->get_field('categoria')->title : '' }}</td>
                                    <td><a href="{{ route('index', ['index' => '', 'client' => $doc->user->id]) }}" target="_blank">{{ $doc->user->fullname() }}</a></td>
                                    <td>{{ $doc->user->get_field('empresa') }}</td>
                                    <td>{{ $doc->user->get_field('departament') }}</td>
                                    <td>{{ $doc->user->role }}</td>
                                    <td>{{ __(ucfirst(str_replace('-', ' ', $doc->validation_status))) }}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@parent
<script>
var table = $('.table').DataTable({
    pagingType: "full_numbers",
    responsive: true,
    pageLength: 25,
    language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" }
});
</script>
@endsection