@extends('Admin::layouts.admin')

@section('section-title')
    @Lang(ucfirst($post_type_plural))
@endsection

@section('content')
<form id="PostForm" class="form-horizontal" action='{{ route($section_route . ".update", $group) }}' method="post">
    <input type="hidden" name="_method" value="PUT">
    @csrf
    <div class="row">
        <div class="col-lg-12">
            {{ mostrarErrors($errors) }}
            <div class="flex-row">
                <div class="col-md-12">
                    <article id="post{{ $group->id }}" data-post="{{ $group->id }}" class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">library_books</i>
                            </div>
                            <h4 class="card-title">
                                @Lang('Edit')
                                <a href='{{ route($section_route.".create") }}' class="btn btn-primary btn-sm">@Lang('Add')</a>
                            </h4>
                        </div>            
                        <div class="card-body">
                            <div class="flex-row">
                                <label class="col-sm-12 col-form-label">@Lang("Title")</label>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input name="title" type="text" class="form-control" value="{{ $group->title }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>

            <div class="flex-row">
                <div class="col-md-12">
                    <article class="card">
                        <div class="card-header card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">supervisor_account</i>
                            </div>
                            <h4 class="card-title">
                                @lang('Usuaris/Proveïdors')
                                <a href="#" class="btn btn-primary btn-sm js-add-user">@Lang('Add')</a>
                            </h4>
                        </div>            
                        <div class="card-body">
                            <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>@lang('Nom')</th>
                                        <th>@lang('Email')</th>
                                        <th>@lang('Empresa')</th>
                                        <th>@lang('Departament')</th>
                                        <th>@lang('Rol')</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($group->users as $client)
                                    <tr>
                                        <td><a href="{{ route('index', ['index' => '', 'client' => $client->id]) }}" target="_blank">{{ $client->fullname() }}</a></td>
                                        <td>{{ $client->email }}</td>
                                        <td>{{ $client->get_field('empresa') }}</td>
                                        <td>{{ $client->get_field('departament') }}</td>
                                        <td>{{ $client->role }}</td>
                                        <td class="td-actions text-right">
                                            @if (auth()->user()->rol->level >= 51)
                                                <button type="button" data-method="PUT" data-name="@Lang('Are you sure to give permissions of :group to :client?', ["group" => $group->title, "client" => $client->fullname()])" data-url="{{ route('admin.groups.user.authorize', [$group, $client]) }}" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm {{ ($client->pivot->gestor) ? 'btn-success' : '' }}" data-title="@Lang('Accept')">
                                                    <i class="material-icons">vpn_key</i>
                                                </button>
                                            @endif

                                            @if (auth()->user()->rol->level > $client->rol->level)
                                                <a href="{{ route("admin.users.edit", $client) }}" data-method="PUT" class="btn btn-just-icon btn-sm btn-success">
                                                    <i class="material-icons">edit</i>
                                                </a>
                                            @endif

                                            @if ($group->noEsGestor($client))
                                                <button type="button" data-method="DELETE"  data-name="@Lang('Are you sure to delete :name?', ['name' => $client->fullname()])" data-url="{{ route('admin.groups.user.remove', [$group, $client]) }}" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-danger remove" data-title="@lang('Delete')">
                                                    <i class="material-icons">close</i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </article>
                </div>
            </div>

            <div class="col-md-12">
                <!-- Post Meta -->
                <article class="card sticky bottom">
                    <!-- Footer -->
                    <div class="card-footer">
                        <div class="mr-2">
                            <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" name="previous" value="@Lang('Tornar')" onclick="window.location.href='{{ route('admin.groups.index') }}'">
                        </div>

                        <div class="ml-auto">
                            <input id="SaveBtnSubmit" type="submit" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="@Lang('Publish') {{ $post_type_title }}">
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </article>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="canviarEstatLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@Lang('Add user')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="flex-row">
                        <select class="selectpicker col-md-12" name="user" data-style="select-with-transition" title="@Lang('User')" data-size="7">
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->fullname() }} - {{ $user->rol->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
                    <button type="submit" id="guardaEstats" class="btn btn-primary ml-3">@Lang('Save')</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('styles')
<link href="{{asset($_admin.'js/summernote/summernote.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset($_admin.'js/summernote/plugin/codemirror/codemirror.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset($_admin.'js/summernote/plugin/codemirror/monokai.css')}}">
@endsection

@section('scripts')
<script>
    var post = @json($group);
    var lang = '{{ $language_code }}';
</script>
<script src="{{asset($_admin.'js/summernote/plugin/codemirror/codemirror.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/plugin/codemirror/xml.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/plugin/codemirror/formatting.js')}}"></script>

<script src="{{asset($_admin.'js/summernote/summernote.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/summernote-cleaner.js')}}"></script>
<script src="{{asset($_admin.'js/summernote.initialize.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/plugin/image-list/summernote-image-list.min.js')}}"></script>
<script src="{{asset($_admin.'js/clipboard.min.js')}}"></script>

@php( $summernote_lang = 'en-US' )
@if( \App::getLocale() == 'ca' )
    @php( $summernote_lang = 'ca-ES' )
@else
    @php( $summernote_lang = strtolower(\App::getLocale()) . '-' . strtoupper(\App::getLocale()) )
@endif

@if ( \App::getLocale() != 'en' )
<script src="{{asset($_admin.'js/summernote/lang/summernote-'.$summernote_lang.'.js')}}"></script>
@endif

<script>

    $('.js-add-user').on('click', function () {
        $('#addUser').modal('toggle');
    });
$('button.copy').click(function(e) {
    e.preventDefault()
});

/* Summernote de Descripcio */
$(document).ready(function() {
    var clipboard = new ClipboardJS('button.copy', {
        text: function(trigger) {
            return trigger.textContent;
        }
    });
    registerSummernote('.summernote', '@Lang('Leave a comment')', 400, '{{ $summernote_lang }}' );
});
</script>
<script>
$(".td-actions button").click(function() {
    event.preventDefault();
    var button = $(event.target).closest("button");

    swal({
        text: button.data("name"),
        //title: 'Are you sure?',
        //text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: button.data("title"),
        cancelButtonText: '@lang('Cancel')',
        buttonsStyling: false
    }).then(function(result) {
        if ( result.value ) {
            $.ajax({
                url: button.data("url"),
                method: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    '_method': button.data("method")
                },
                success: function(data) {
                    location.reload();
                },
                error: function(data) {
                    swal({
                        title: "@lang('Error')",
                        text: "@lang('Error saving data')",
                        type: 'error',
                        confirmButtonClass: "btn",
                        buttonsStyling: false
                    });
                }
            });
        }
    });
});

function save_draft() {
    var form_aux = $('#PostForm');
    form_aux.find('input[type=hidden][name=status]').val('draft');
    form_aux.submit();
}
function publish_draft() {
    $('#PostForm').find('input[type=hidden][name=status]').val('publish');
    $('#PostForm').find('#SaveBtnSubmit').click();
}
</script>

@includeIf('Admin::posts.media_modal')

@endsection