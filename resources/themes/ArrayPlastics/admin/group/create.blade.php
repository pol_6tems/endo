@extends('Admin::layouts.admin')

@section('section-title')
    @Lang(ucfirst($post_type_plural))
@endsection

@section('content')
<form id="PostForm" class="form-horizontal js-form" action='{{ route("admin.groups.store") }}' method="post">
    {{ csrf_field() }}
    <input type="hidden" name="status" value="publish" />
    <div class="row">
        <div class="col-md-12">
            <div class="flex-row">
                <div class="col-md-12">
                    <article class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">library_books</i>
                            </div>
                            <h4 class="card-title">
                                @Lang('Add new')
                            </h4>
                        </div>

                        <div class="card-body">
                            <div class="flex-row">
                                <label class="col-sm-12 col-form-label">@Lang("Title")</label>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input name="title" type="text" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                {{-- @php ( execute_actions('post_form_fields') ) --}}

                <div class="col-md-12">
                    <div class="card sticky bottom">
                        <div class="card-footer">

                            <div class="ml-auto">
                                <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd js-submit-btn" name="next" value="@Lang('Save')">
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('styles')
<link href="{{asset($_admin.'js/summernote/summernote.css')}}" rel="stylesheet">
@endsection

@section('scripts')
<script src="{{asset($_admin.'js/summernote/summernote.js')}}"></script>
<script> var imageListEndpoint = "{{ route('admin.get.images') }}"; </script>
<script src="{{asset($_admin.'js/summernote.initialize.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/plugin/image-list/summernote-image-list.min.js')}}"></script>
<script src="{{asset('js/stickyfill.min.js')}}"></script>
<script>
    var elements = document.querySelectorAll('.sticky');
    Stickyfill.add(elements);
</script>


@php( $summernote_lang = 'en-US' )
@if( \App::getLocale() == 'ca' )
    @php( $summernote_lang = 'ca-ES' )
@else
    @php( $summernote_lang = strtolower(\App::getLocale()) . '-' . strtoupper(\App::getLocale()) )
@endif

@if ( \App::getLocale() != 'en' )
<script src="{{asset($_admin.'js/summernote/lang/summernote-'.$summernote_lang.'.js')}}"></script>
@endif
<script>
/* Summernote de Descripcio */
$(document).ready(function() {
    registerSummernote('.summernote', '@Lang('Leave a comment')', 400, '{{ $summernote_lang }}' );
});

$('.custom_field_group_title').click(function(){
    var card = $(this).closest('.card');
    $(card).toggleClass('hidden');
    $(card).toggleClass('pb-5');
    $(card).find('.custom_field_group_fields').fadeToggle();
});
</script>

@includeIf('Admin::posts.media_modal')

{{-- GALLERY CUSTOM FIELD --}}
<script>
function delete_gallery_element(btn) {
    $(btn).closest('.fileinput[data-provides=fileinput]').remove();
}
$('.custom_field_gallery .add_image_to_gallery').click(function(){
    var id_button_gallery = $(this).attr('id');
    var parent_gallery = $(this).closest('.custom_field_gallery');
    var num_gallery_img = $(parent_gallery).find('.fileinput[data-provides=fileinput]').length;
    var clone = $(parent_gallery).find('#clone');
    var new_gallery_image = $(clone).clone();
    var new_name_gallery = id_button_gallery + '[' + num_gallery_img + ']';
    $(new_gallery_image).attr('id', '');
    $(new_gallery_image).find('input.media_input[type=hidden]').attr('name', new_name_gallery);
    $(new_gallery_image).insertBefore( $(clone) );
    $(new_gallery_image).fadeIn();
    var new_btn_file = $(new_gallery_image).find('.btn-file');
    show_media_modal(new_btn_file);
});


$('.js-submit-btn').on('click', function () {
    var form = $('.js-form');

    if (! form[0].checkValidity()) {
        var invalidInput = form.find('input:invalid').first();

        if (!invalidInput.is(':visible')) {
            var inputName = invalidInput.attr('name');
            var lang = inputName.match(/\[[a-z].\]/)[0];

            lang = lang.replace('[', '').replace(']', '');

            $('.nav-lang-' + lang).click();
        }
    }
});
</script>
{{-- end GALLERY CUSTOM FIELD --}}

@yield('scripts2')

@endsection