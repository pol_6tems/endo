@extends('Admin::layouts.admin')

@section('section-title')
    @Lang(ucfirst($post_type_plural))
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">library_books</i>
                    </div>
                    <h4 class="card-title">
                        @Lang(ucfirst($post_type_plural))
                        <a href='{{ route($section_route . ".create") }}' class="btn btn-primary btn-sm">@Lang('Add')</a>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="material-datatables">
                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                            <tr>
                                <th>@Lang('Title')</th>
                                <th width="15%">@Lang('Email')</th>
                                <th width="15%">@Lang('Empresa')</th>
                                <th width="15%">@Lang('Departament')</th>
                                <th width="15%">@Lang('Usuaris')</th>
                                {{--
                                <th width="15%">@Lang('Propietari')</th>
                                <th width="15%">@Lang('Empresa')</th>
                                <th width="15%">@Lang('Departament')</th>
                                <th width="15%">@Lang('Updated')</th>--}}
                                <th width="15%" class="disabled-sorting text-right">@Lang('Actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($groups as $key => $group)
                                @php($user = $group->gestors->first())
                                <tr>
                                    <td>{{ $group->title }}</td>
                                    <td>{{ $user->email ?? '' }}</td>
                                    <td>{{ $user ? $user->get_field("empresa") : '' }}</td>
                                    <td>{{ $user ? $user->get_field("departament") : '' }}</td>
                                    <td>{{ $group->users()->count() }}</td>
                                    <td class="td-actions text-right">
                                        <a href="{{ route($section_route . '.edit', $group) }}" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-success" title="@Lang('Edit')">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <button data-url="{{ route('admin.groups.destroy', $group) }}" data-name="@Lang('Are you sure to delete :name?', ['name' => $group->title])" type="button" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                                            <i class="material-icons">close</i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            <div class="loader">
                                <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                    <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                                </svg>
                            </div>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function getIDs() {
            var ids = [];
            $('.checkbox_delete').each(function () {
                if($(this).is(":checked")) {
                    ids.push($(this).val());
                }
            });
            $('#ids').val(ids.join());
        }

        $(".checkbox_all").click(function(){
            $('input.checkbox_delete').prop('checked', this.checked);
            getIDs();
        });

        $('.checkbox_delete').change(function() {
            getIDs();
        });
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            var table = $('.table').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [25, 50, -1],
                    [25, 50, "@Lang('All')"]
                ],
                responsive: true,
                language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" }
            });

            table.on( 'row-reorder', function ( e, diff, edit ) {
                var order = [];
                $('.table tbody tr').each(function (index, el) {
                    console.log($(this).data('post_id'));
                    order.push({post_id:$(this).data('post_id'), newPosition:index});
                });

                $.ajax({
                    url: "{{ route('ajax') }}",
                    method: 'POST',
                    data: {
                        parameters: JSON.stringify(order),
                        action: 'reorderRow',
                    },
                    success: function(data) {
                        $('.loader').fadeOut();
                        e.preventDefault();
                    },
                    error: function(data) {
                        swal({
                            title: "@Lang('Error')",
                            text: "@Lang('Error saving data')",
                            type: 'error',
                            confirmButtonClass: "btn",
                            buttonsStyling: false
                        });
                    }
                });
            });
            
            // Delete a record
            table.on('click', '.remove', function(e) {
                var $tr = $(this).closest('tr');
                var url = $(this).data('url');
                var title = $(this).data('name');
                swal({
                    text: title,
                    //title: 'Are you sure?',
                    //text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: '@Lang('Delete')',
                    cancelButtonText: '@Lang('Cancel')',
                    buttonsStyling: false
                }).then(function(result) {
                    if ( result.value ) {
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: {'_token': '{{csrf_token()}}', '_method': 'DELETE'},
                            success: function(data) {
                                swal({
                                    title: "@Lang('Deleted')",
                                    text: "@Lang('Item deleted succesfully.')",
                                    type: 'success',
                                    confirmButtonClass: "btn btn-success",
                                    buttonsStyling: false
                                });
                                table.row($tr).remove().draw();
                                e.preventDefault();
                            },
                            error: function(data) {
                                swal({
                                    title: "@Lang('Error')",
                                    text: "@Lang('Error saving data')",
                                    type: 'error',
                                    confirmButtonClass: "btn",
                                    buttonsStyling: false
                                });
                            }
                        });
                    }
                });
            });

            // RESTORE
            table.on('click', '.restore', function(e) {
                var $tr = $(this).closest('tr');
                var url = $(this).data('url');
                var title = $(this).data('name');
                swal({
                    text: title,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: '@Lang('Restore')',
                    cancelButtonText: '@Lang('Cancel')',
                    buttonsStyling: false
                }).then(function(result) {
                    if ( result.value ) {
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: {'_token': '{{csrf_token()}}', '_method': 'PUT'},
                            success: function(data) {
                                swal({
                                    title: "@Lang('Restored')",
                                    text: "@Lang('Item restored succesfully.')",
                                    type: 'success',
                                    confirmButtonClass: "btn btn-success",
                                    buttonsStyling: false
                                });
                                table.row($tr).remove().draw();
                                e.preventDefault();
                            },
                            error: function(data) {
                                swal({
                                    title: "@Lang('Error')",
                                    text: "@Lang('Error saving data')",
                                    type: 'error',
                                    confirmButtonClass: "btn",
                                    buttonsStyling: false
                                });
                            }
                        });
                    }
                });
            });

            table.on('click', '.switch_active', function(e) {
                var boto = $(this);
                var url = $(this).data('url');
                var title = $(this).data('name');
                var text = "@Lang('actived')";
                var id = $(this).data('id');
                var onlyone = $(this).data('onlyone');
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {'_token': '{{csrf_token()}}', 'id': id},
                    success: function(data) {
                        if ( data.active ) {
                            swal({
                                title: "{{ucfirst(__('actived'))}}",
                                text: title + " @Lang('actived')",
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            });
                            if ( onlyone ) {
                                $('.switch_active').removeClass('btn-success');
                                $('.switch_active').addClass('btn-danger');
                                $('.switch_active').find('i').html('clear');
                            }
                            boto.removeClass('btn-danger');
                            boto.addClass('btn-success');
                            boto.find('i').html('check');
                        } else {
                            swal({
                                title: "{{ucfirst(__('deactived'))}}",
                                text: title + " @Lang('deactived')",
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            });
                            boto.addClass('btn-danger');
                            boto.removeClass('btn-success');
                            boto.find('i').html('clear');
                        }
                        e.preventDefault();
                    },
                    error: function(data) {
                        swal({
                            title: "@Lang('Error')",
                            text: "@Lang('Error saving data')",
                            type: 'error',
                            confirmButtonClass: "btn",
                            buttonsStyling: false
                        });
                    }
                });
            });

            $('.table').on('click', '.show_img', function(e) {
                var boto = $(this);
                var img = $(this).data('img');
                var title = $(this).data('name');
                swal({
                    title: title,
                    width: '80%',
                    showCloseButton: true,
                    showCancelButton: false,
                    showConfirmButton: false,
                    html: '<img src="'+img+'" style="width: 100%;">'
                });
            });

            // DUPLICATE
            table.on('click', '.duplicate', function(e) {
                var $tr = $(this).closest('tr');
                var url = $(this).data('url');
                var title = $(this).data('name');
                swal({
                    text: title,
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: '@Lang('Duplicate')',
                    cancelButtonText: '@Lang('Cancel')',
                    buttonsStyling: false
                }).then(function(result) {
                    if ( result.value ) {
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: {'_token': '{{csrf_token()}}', '_method': 'PUT'},
                            success: function(data) {
                                swal({
                                    title: "@Lang('Duplicated')",
                                    text: "@Lang('Item duplicated succesfully.')",
                                    type: 'success',
                                    confirmButtonClass: "btn btn-success",
                                    buttonsStyling: false
                                }).then(function(result) {
                                    location.reload();
                                });
                            },
                            error: function(data) {
                                swal({
                                    title: "@Lang('Error')",
                                    text: "@Lang('Error saving data')",
                                    type: 'error',
                                    confirmButtonClass: "btn",
                                    buttonsStyling: false
                                });
                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection