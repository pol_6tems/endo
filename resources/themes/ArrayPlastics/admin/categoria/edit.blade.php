@extends('Admin::layouts.admin')

@section('section-title')
    @lang(ucfirst($post_type_plural))
@endsection

@section('content')
@php
    $params = ($params) ? json_decode($params) : null;
    $trobat = false;
    foreach ($cf_groups as $key => $cf_group) {
        if($cf_group->position == 'r-sidebar') {
            $trobat = true;
            break;
        }
    }
    
    // Revisem tots els parametres per saber si hem de reservar espai per la sidebar
    $sidebar =  in_array($post->type, ['post', 'page']) || ($params && isset($params->published) && $params->published ||
                $params && isset($params->author) && $params->author ||
                $params && isset($params->imatge) && $params->imatge) || $trobat;
@endphp
<form id="PostForm" class="form-horizontal" action='{{ route($section_route . ".update", $post->id) }}?post_type={{$post_type}}' method="post">
    <input type="hidden" name="_method" value="PUT">
    @csrf
    <input type="hidden" name="status" value="{{ $post->status }}" />
    <input type="hidden" name="type" value="{{ $post->type }}" />
    <div class="row">
        <div class="col-lg-12">
            {{ mostrarErrors($errors) }}
            <div class="flex-row">
                <div class="col-md-{{ ($sidebar) ? '8 mr-auto' : '12' }}">
                    <article id="post{{ $post->id }}" data-post="{{ $post->id }}" class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">library_books</i>
                            </div>
                            <h4 class="card-title">
                                @Lang('Edit')
                                <a href='{{ route($section_route.".create", ["post_type" => $post_type]) }}' class="btn btn-primary btn-sm">@Lang('Add')</a>
                            </h4>
                        </div>            
                        <div class="card-body">
                            @includeIf('Front::partials.edit-post-disclaimer')

                            @if (isset($subforms) && count($subforms))
                            <div class="toolbar">
                                <!-- Subforms -->
                                <ul class="nav nav-pills nav-pills-primary" role="tablist">
                                    <li class="nav-item">
                                        <a href="#general" data-toggle="tab" class="nav-link active">General</a>
                                    </li>
                                    @foreach($subforms as $key => $subform)
                                        @php($titol = explode('.', $subform['titol_tab'])[0])
                                        <li class="nav-item">
                                            <a href="#{{ $titol }}" data-toggle="tab" class="nav-link">{{$titol}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
            
                            <!-- Tab de General -->
                            <div class="tab-content">
                                <!-- General -->
                                <div id="general" class="tab-pane active">
                                    <!-- Tabs de Idiomes -->
                                    {{ mostrarIdiomes($_languages, $language_code, 'general') }}
                                    <div class="tab-content">
                                        @foreach($_languages as $language_item)
                                        <div id="general{{ $language_item->code }}" class="tab-pane {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                                            @includeIf('Admin::partials.general-form')
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <!-- Fi General -->
                            </div>

                            <div class="custom-field selection-field">
                                <div class="flex-row pt-3">
                                    <label class="col-md-12">@Lang("Groups")</label>
                                    <div class="col-md-12">
                                        <div>
                                            <div class="form-group">
                                                @foreach ( $groups as $k_multi => $choice )
                                                    <input type="hidden" name="groups[{{ $choice->id }}]" value="0" />
                                                    <div class="togglebutton">
                                                        <label>
                                                            @php ( $choice_title = $choice->title )
                                                            <input
                                                                type="checkbox"
                                                                name="groups[{{ $choice->id }}]"
                                                                value="1"
                                                                {{ $choice->categories->where("id", $post->id)->isNotEmpty() ? 'checked' : '' }} />

                                                            <span class="toggle"></span>
                                                            {{ $choice->title }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- Sidebar -->
                @if($sidebar)
                    @includeIf('Admin::partials.posts-sidebar')
                @endif

            <div class="col-md-12">
                {{-- CF GROUPS --}}
                @foreach ($cf_groups as $key => $cf_group)
                    @if($cf_group->position == 'bottom')
                    <article id="post{{ $post->id}}-cfg{{ $cf_group->id }}" data-post="{{ $post->id }}" class="card pm">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="flex-row" style="justify-content: space-between;">
                                <div class="header-left">
                                    <h4 class="card-title">{{ $cf_group->title }}</h4>
                                </div>
                                <div class="header-right">
                                    <a data-toggle="collapse" href="#cf_group{{ $cf_group->id }}"><i class="material-icons">keyboard_arrow_down</i></a>
                                    @if(Auth::user()->isAdmin())
                                    <a href="{{ route('admin.custom_fields.edit', $cf_group->id) }}?post_type={{$post_type}}" target="_blank" rel="tooltip">
                                        <i class="material-icons">settings</i>
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div id="cf_group{{ $cf_group->id }}" class="collapse">
                            <div class="card-body pb-5">
                                {{ mostrarIdiomes($_languages, $language_code, 'panel'.$cf_group->id) }}
                                <!-- Tab de General -->
                                <div class="tab-content">
                                    @foreach($_languages as $key => $language_item)
                                    <div id="panel{{ $cf_group->id }}{{ $language_item->code }}" class="tab-pane {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                                        <div class="custom_field_group_fields">
                                            @foreach ($cf_group->fields as $k => $custom_field)
                                                @php($viewParams = [
                                                    'title' => $custom_field->title,
                                                    'name' => "custom_fields[".$language_item->code."][".$custom_field->id . "]",
                                                    'value' => $post->get_field( $custom_field->name, $language_item->code ),
                                                    'params' => json_decode($custom_field->params),
                                                    'position' => $cf_group->position,
                                                    'custom_field' => $custom_field,
                                                    'order' => $k,
                                                    'lang' => $language_item->code,
                                                ])

                                                @if (View::exists('Front::admin.custom_fields.' . $custom_field->type))
                                                    @includeIf('Front::admin.custom_fields.' . $custom_field->type, $viewParams)
                                                @elseif (View::exists('Admin::default.custom_fields.' . $custom_field->type))
                                                    <!-- General -->
                                                    @includeIf('Admin::default.custom_fields.' . $custom_field->type, $viewParams)
                                                @else
                                                    @php($module = explode(".", $custom_field->type)[0])
                                                    @php($field = explode(".", $custom_field->type)[1])
                                                    @includeIf('Modules::'.$module.'.resources.views.custom_fields.'.$field, $viewParams)
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </article>
                    @endif
                @endforeach
            </div>

            @if ($documents && $documents->count())
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">details</i>
                            </div>
                            <h4 class="card-title">
                                @lang('Documents')
                            </h4>
                        </div>

                        <div class="card-body">
                            <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>@lang('Nom')</th>
                                    <th>@lang('Periode màxim de validació')</th>
                                    <th>@lang('Data caducitat')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($documents as $document)
                                    <tr>
                                        <td><strong>{{ $document->title }}</strong></td>
                                        <td>{{ $document->get_field('periode-maxim-validacio') ? __($document->get_field('periode-maxim-validacio')) : '' }}</td>
                                        <td>{{ $document->get_field('data-caducitat') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif

            @php ( execute_actions('post_form_fields', $post) )

            <div class="col-md-12">
                @if (isset($save_disclaimer) && $save_disclaimer)
                    <div>
                        {{ $save_disclaimer }}
                    </div>
                @endif

                <!-- Post Meta -->
                <article class="card sticky bottom">
                    <!-- Footer -->
                    <div class="card-footer">
                        <div class="mr-2">
                            <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" name="previous" value="@Lang('Tornar')" onclick="window.location.href='{{ route('admin.posts.index', ['post_type' => $post_type]) }}'">
                        </div>
                        @if (view()->exists("Front::posts.single-{$post->type}") || in_array($post_type, ['post', 'page']))
                            <div class="mr-auto">
                                <input type="button" class="btn btn-previous btn-fill btn-info" name="show" value="@lang('View')" onclick="window.open('{{ $post->get_url() }}')">
                            </div>
                        @endif
                        
                        <div class="ml-auto">
                            <input id="SaveBtnSubmit" type="submit" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="@Lang( ($post->status != 'draft' ? 'Update' : 'Publish') ) {{ $post_type_title }}" onclick="{{ $post->status == 'draft' ? 'publish_draft();' : '' }}">
                        </div>

                        @if ( $post->status != 'draft' )
                        <div class="ml-2">
                            <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" name="previous" value="@Lang('Draft')" onclick="save_draft();">
                        </div>
                        @endif

                        @if ( $can_aprove_pending )
                        <div class="ml-2">
                            <input type="button" class="btn btn-success btn-fill" name="aprove" value="@Lang('Aprove')" onclick="var form_aux = document.getElementById('PostForm');form_aux.action = '{{ route($section_route . ".aprove_pending", $post->id) }}?post_type={{$post_type}}';form_aux.submit();">
                        </div>
                        @endif

                        <div class="clearfix"></div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</form>
@endsection

@section('styles')
<link href="{{asset($_admin.'js/summernote/summernote.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset($_admin.'js/summernote/plugin/codemirror/codemirror.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset($_admin.'js/summernote/plugin/codemirror/monokai.css')}}">
@endsection

@section('scripts')
<script>
    var post = @json($post);
    var lang = '{{ $language_code }}';
</script>
<script src="{{asset($_admin.'js/summernote/plugin/codemirror/codemirror.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/plugin/codemirror/xml.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/plugin/codemirror/formatting.js')}}"></script>

<script src="{{asset($_admin.'js/summernote/summernote.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/summernote-cleaner.js')}}"></script>
<script src="{{asset($_admin.'js/summernote.initialize.js')}}"></script>
<script src="{{asset($_admin.'js/summernote/plugin/image-list/summernote-image-list.min.js')}}"></script>
<script src="{{asset($_admin.'js/clipboard.min.js')}}"></script>

@php( $summernote_lang = 'en-US' )
@if( \App::getLocale() == 'ca' )
    @php( $summernote_lang = 'ca-ES' )
@else
    @php( $summernote_lang = strtolower(\App::getLocale()) . '-' . strtoupper(\App::getLocale()) )
@endif

@if ( \App::getLocale() != 'en' )
<script src="{{asset($_admin.'js/summernote/lang/summernote-'.$summernote_lang.'.js')}}"></script>
@endif

<script>
$('button.copy').click(function(e) {
    e.preventDefault()
});

/* Summernote de Descripcio */
$(document).ready(function() {
    var clipboard = new ClipboardJS('button.copy', {
        text: function(trigger) {
            return trigger.textContent;
        }
    });
    registerSummernote('.summernote', '@Lang('Leave a comment')', 400, '{{ $summernote_lang }}' );
});
</script>

<script>
function save_draft() {
    var form_aux = $('#PostForm');
    form_aux.find('input[type=hidden][name=status]').val('draft');
    form_aux.submit();
}
function publish_draft() {
    $('#PostForm').find('input[type=hidden][name=status]').val('publish');
    $('#PostForm').find('#SaveBtnSubmit').click();
}
</script>

@includeIf('Admin::posts.media_modal')

{{-- Declarem tots els espais per els scripts dels CF --}}
@foreach ($cf_groups as $key => $cf_group)
    @foreach ($cf_group->fields as $custom_field)
        
        <!-- Scripts CF id: {{ $custom_field->id }}-->    
        @yield("scripts-cf$custom_field->id")

        @if ( $custom_field->type == 'repeater' )
            @if( $repeater_children = $post->get_field($custom_field->name) )
                @foreach ( $repeater_children as $nrc => $r_child_fields )
                    @foreach ( $r_child_fields as $r_child_field )
                        @php ( $id_element_nrc = 'cf' . $r_child_field['id'] . '_k' . $nrc )
                        <!-- Scripts CF id: {{ $custom_field->id }} repeater field id: {{ $r_child_field['id'] }} num: {{ $nrc }}-->
                        @yield("scripts-$id_element_nrc")
                    @endforeach
                @endforeach
            @endif
        @endif

    @endforeach
@endforeach

@endsection