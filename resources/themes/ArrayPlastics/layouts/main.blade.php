@extends('Front::layouts.html')

@section('body_classes', 'grey-bg')

@section('body')
    @include('Front::partials.header')

    <div class="content-area">
        <div class="row">
            @yield('content')
        </div>
    </div>
@endsection