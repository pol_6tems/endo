@php ( $comments = $params['comments'] )

<!-- COMENTARIOS -->
@if ( !empty($comments) && count($comments) > 0 )
    <ul class="comments">
        @foreach ($comments as $c)
        <li>
            <span style="font-size: 13px;">{{ $c->created_at->format('d/m/Y H:i:s') }} / @if ($c->user){{ $c->user->fullname() }}@else @lang('Anònim')@endif</span>
            <p>{!! nl2br($c->comment) !!}</p>
        </li>
        @endforeach
    </ul>
@endif
<!-- end COMENTARIOS -->