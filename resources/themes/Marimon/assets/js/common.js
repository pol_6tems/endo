// Input Box Placeholder
function fnremove(arg,val)	{
	if (arg.value == '') {arg.value = val}
}	
function fnshow(arg,val)	{
	if (arg.value == val) {arg.value = ''}
}
//DropDown Menu
ddsmoothmenu.ddsmoothmenu.init({
	mainmenuid: "smoothmenu",
	orientation: 'h',
	classname: 'ddsmoothmenu',
	//customtheme: ["#1c5a80", "#18374a"],
	method: 'toggle',
	contentsource: "markup"
});

$(document).ready(function () {
	if (typeof grecaptcha !== 'undefined') {
		submitCaptcha(false);
	}

	$(".leer-link").click(function () {
		$(".content-more").toggleClass("show");
		$(".leer-link").toggleClass("hide");
	});
	$(".leer-links").click(function () {
		$(".content-more1").toggleClass("show");
		$(".leer-links").toggleClass("hide");
	});
	$('#mobNav').mmenu();
	$(".select_box").selectbox();

	jsUpdateSize();	
	
	// Sticky Header		
	$("header").sticky({topSpacing:0});

	$(".fancybox").fancybox();

	$('#frm-popup .ok-btn').click(function(){
		$('.fancybox-overlay').hide();
		$('.fancybox-lock').addClass('scroll');
		$('.fancybox-lock body, .fancybox-lock').css("overflow", "auto");
	});

	$('.submit0-fancybox').on('click', function (e) {
		if (!$('#check1').is(':checked')) {
			e.preventDefault();
			$('.fancy-box-click').click();
		}
	});
});

$(window).resize(function(){	
	jsUpdateSize();
});

function jsUpdateSize()
{
	var width = $(window).width();
    var height = $(window).height();	
	if(width>=1023)
	{
		$('header').addClass("head-mobile");		
	}
	else{		
		$('header').removeClass("head-mobile");
	}
}

function changeTab(newTab) {
	var lis = $("ul.resp-tabs-list > li"),
		h2s = $(".resp-accordion"),
		divs = $(".resp-tab-content");

	lis.removeClass("resp-tab-active");
	h2s.removeClass("resp-tab-active");
	divs.removeClass("resp-tab-content-active").css('display', '');

	$('.resp-tab-item[aria-controls="tab_item-' + newTab + '"]').addClass('resp-tab-active');
	var currentAccordion = $('.resp-accordion[aria-controls="tab_item-' + newTab + '"]')
	currentAccordion.addClass('resp-tab-active');

	$('.resp-tab-content[aria-labelledby="tab_item-' + newTab + '"]').addClass('resp-tab-content-active');

	if (newTab == 0) {
		$('.js-return-button').show();
	} else {
		$('.js-return-button').hide();
	}

	$('html, body').animate({
		scrollTop: (currentAccordion.first().offset().top)
	}, 500);
}


function submitInputs(inputs) {
	$.each(inputs, function () {
		var name = $(this).attr('name');

		if (typeof name !== 'undefined') {
			if ($(this).attr('type') === 'radio' && !$(this).is(':checked')) {
				return true;
			}

			var val = $(this).val();
			var text = val;

			if ($(this).is('select')) {
				text = $(this).find('option:selected').text();
			}

			if ($(this).is(':radio')) {
				text = $(this).next('label:first').html();
			}

			if (name !== 'descripcion') {
				$('.resume-' + name).html(text);
			} else {
				var descriptionArray = text.split("\n");
				
				$('.resume-' + name + '1').html(descriptionArray[0]);
				
				var restDescription = '';

				if (descriptionArray.length > 1) {
					restDescription = descriptionArray[1];

					$.each(descriptionArray, function (index, element) {
						if (index <= 1) {
							return true;
						}
						if (element !== '') {
							restDescription = restDescription + "<br>" + element;
						}
					});
				}

				$('.resume-' + name + '2').html(restDescription);
			}

			if (name !== 'docs[]') {
				$('.form-' + name).val(val);
			} else {
				$('#resume-form').append('<input type="file" name="docs[]" value="' + val + '">');
			}
		}
	});
}


function submitCaptcha(submit) {
	grecaptcha.ready(function() {
		grecaptcha.execute('6Lc3P7AUAAAAADqtOuV-04Y8Ytsx9rie9WwUTC-k', {action: 'submit_compliance'}).then(function(token) {
			var form = $('#resume-form');
			form.prepend('<input type="hidden" name="captcha_token" value="' + token + '">');

			if (submit) {
				form.submit();
			}
		});
	});
}

var fileInputs = [{
	id: 'file-input0',
	index: 0,
	value: ''
}];

var examInput = $('.exam-input');
examInput.val('');

$('.exam-btn').on('click', function () {
	$.each(fileInputs, function (index, value) {
		var mIndex = index;
		var fIndex = value.index;

		if (value.value === '') {
			var input = $('#' + value.id);

			input.on('change', function () {
				var files = $(this).prop("files");
				var names = $.map(files, function(val) { return val.name; });

				var namesString = names.join(', ');

				var inputString = examInput.val();

				if (inputString !== '') {
					inputString += ', ';
				}
				examInput.val(inputString + namesString);

				var fileList = $('.file-list');

				$.each(names, function (index, value) {
					var fileElement = '<div class="form-img file-preview-' + fIndex + '"> <img src="' + fileList.data('imgs') +'" alt=""/>' +
						'<h4>' + fileList.data('titles') + '</h4>' +
						'<p>' + value + '</p></div>';

					fileList.append(fileElement);

					$('#file-list').append('<li class="file-preview" ><div class="file-remover" data-file-id="' + fIndex +'"></div>' + fileElement + '</li>')
				});

				if (names.length) {
					$('.js-files-step').show();
				} else {
					$('.js-files-step').hide();
				}

				fileInputs[mIndex]['value'] = namesString;

				var inputId = 'file-input' + (fIndex + 1);
				$('#resume-form').append('<input id="' + inputId + '" type="file" name="docs[]" accept=".gif, .jpg, .jpeg, .png, .pdf, .doc, .docx, .ppt, .pptx, .xls, .xlsx, .avi, .mov, .mp3, .mp4" style="display: none">')
				fileInputs.push({id: inputId, index:fIndex + 1, value: ''})
			});

			input.click();

			return false;
		}
	});
});

$(document).on('click', '.file-remover', function () {
	var me = $(this);
	var mIndex = me.data('file-id');

	$.each(fileInputs, function(index, value) {
		console.log(value);
		if (value.index == mIndex) {
			examInput.val(examInput.val().replace(value.value, ''));
			examInput.val(examInput.val().replace(', , ', ', '));

			$('.file-preview-' + mIndex).remove();
			me.parent('.file-preview').remove();
			$('#file-input' + mIndex).remove();

			fileInputs.splice(index, 1);
			return false;
		}
	});

	if (fileInputs.length <= 2) {
		examInput.val(examInput.val().replace(', ', ''));
	}
});

// Sticky Header		
$("header.head-mobile").sticky({topSpacing:0});

$('.dades-form').on('submit', function (e) {
	e.preventDefault();

	var inputs = $(this).find(':input');
	submitInputs(inputs);

	var current = $(this).data('current');

	if (typeof current !== 'undefined') {
		changeTab(current + 1);
	}
});

$('.volver-btn').on('click', function () {
	var current = $(this).data('current');

	if (typeof current !== 'undefined') {
		changeTab(current - 1);
	}
});

$('#resume-form').on('submit', function (e) {
	e.preventDefault();

	$(this).unbind('submit');

	var captchaInput = $('input[name="captcha_token"]');

	if (typeof captchaInput === 'undefined') {
		submitCaptcha(true);
	} else {
		$(this).submit();
	}

});
