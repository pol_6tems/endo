
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

/*require('./bootstrap');*/

require('./plugins/html5');
window.$ = window.jQuery = require('jquery');
window.ddsmoothmenu = require('./plugins/ddsmoothmenu');

require('./plugins/jquery.sticky');
require('./plugins/jquery.mmenu.min.all');
require('./plugins/jquery.fancybox');
require('./plugins/jquery.selectbox-0.2');
require('./plugins/easyResponsiveTabs');

require('./common');
