@extends('Front::layouts.form')

@section('form')
    <div id="form-details">
        <ul class="resp-tabs-list hor_1">
            <li>@lang('Step NUMBER', ['number' => 1])</li>
            <li>@lang('Step NUMBER', ['number' => 2])</li>
            <li>@lang('Step NUMBER', ['number' => 3])</li>
            <li>@lang('Step NUMBER', ['number' => 4])</li>
        </ul>
        <div class="resp-tabs-container hor_1">
            <!-- tab 1 -->
            @include('Front::partials.complaint-tab-id')

            <!-- tab 2 -->
            @include('Front::partials.complaint-tab-detail', ['isAnonymous' => 0])

            <!-- tab 3 -->
            @include('Front::partials.complaint-tab-docs', ['isAnonymous' => 0])

            <!-- tab 3 -->
            @include('Front::partials.complaint-tab-resume', ['isAnonymous' => 0])
        </div>
    </div>
@endsection