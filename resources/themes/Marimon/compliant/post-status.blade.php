@extends('Front::layouts.main')

@section('content')
    <section class="bienvenido vol min-ht">
        <div class="row1">
            <h4><a href="{{ $company->get_url() }}">@lang('Go back')</a></h4>
            <h2>@lang('Complaint Status')</h2>
            <div id="estado-details">
                <ul class="complain-status">
                    <li @if ($complaint->get_field('estado') == 'en_proceso')class="active-status" @endif>@lang('en_proceso')</li>
                    <li @if ($complaint->get_field('estado') == 'en_tramite')class="active-status" @endif>@lang('en_tramite')</li>
                    <li @if ($complaint->get_field('estado') == 'archivada')class="active-status" @endif>@lang('archivada')</li>
                </ul>
                <div class="resp-tabs-container hor_1">
                    @if ($complaint->get_field('estado') == 'en_proceso')
                        <!-- tab 1 -->
                        <div>
                            <h3>@lang('Complaint number', ['id' => $complaint->title])</h3>
                            @lang('complaint_status_in_progress')
                        </div>
                    @endif

                    @if ($complaint->get_field('estado') == 'en_tramite')
                        <!-- tab 2 -->
                        <div>
                            <h3>@lang('Complaint number', ['id' => $complaint->title])</h3>
                            @lang('complaint_status_processing', ['company_name' => $company->title])
                        </div>
                    @endif

                    @if ($complaint->get_field('estado') == 'archivada')
                        <!-- tab 3 -->
                        <div>
                            <h3>@lang('Complaint number', ['id' => $complaint->title])</h3>
                            <p>@lang($complaint->get_field('razon-archivada') . '_public', ['company_name' => $company->title])</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection