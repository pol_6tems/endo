@extends('Front::layouts.main')

@section('content')
    <section class="bienvenido inner-page min-ht small-page">
        <div class="row">
            <h4 class="title-center"><span class="top-tit">@if ($complaint->get_field('anonima')) @lang('submit0_anonymous') @else @lang('Submit a complaint') @endif</span></h4>
            <h2>@lang('Enviada')</h2>
            <div class="denuncia">
                <p>@lang('Le confirmamos que su denuncia ha sido registrada correctamente y le agradecemos la confianza depositada en el canal gestionado por MARIMÓN ABOGADOS. Transcurridos 5 días hábiles desde la recepción de la denuncia podrá consultar el estado de la misma en la sección Consulta el estado de tu denuncia')</p>
                <div class="den-bg">@lang('Su código de denuncia es:') <span>{{ $complaint->title }} </span></div>
                @if ($complaint->get_field('anonima'))
                    <div class="den-red">
                        <h4>@lang('ATENCIÓN')</h4>
                        <p>@lang('Debe anotar o memorizar el código de su denuncia. Necesitará su número de denuncia para poder acceder al apartado Consultar estado de su denuncia. <span>Si pierde su número de denuncia/consulta o su contraseña, tendrá que presentar un nuevo formulario.</span> Por motivos de seguridad y con el fin de proteger su anonimato, <span>no podemos recuperar su número de denuncia/consulta</span> ni su contraseña.')</p>
                    </div>
                @else
                    <div class="den-red">
                        <p>@lang('')</p>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection