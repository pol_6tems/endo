@extends('Front::layouts.main')

@section('content')
    <section class="bienvenido inner-page min-ht">
        <div class="row">
            <h4>
                <a href="{{ $company->get_url() }}">@lang('Go back')</a>
                <span class="top-tit">@lang('Submit a complaint')</span>
            </h4>

            <div class="pres-denu pres-denu-center">
                <p>
                    @if ($company->get_field('texto-presentar-denuncia'))
                        {!! str_replace('[company_name]', $company->title, $company->get_field('texto-presentar-denuncia')) !!}
                    @else
                        @lang('submit0_previous')
                    @endif
                </p>
            </div>

            <div class="siguiente">
                <div class="atencion-lst">
                    <a style="display:none;" href="#frm-popup" class="fancybox fancy-box-click"></a>
                    @if (!$complaintTypes || $complaintTypes->count() <= 1)
                        <ul>
                            <li><a href="{{ route('posts.submit-anonymous', $defaultRouteParams) }}"><span class="pre-ano"></span> <h3>@lang('submit0_anonymous')</h3></a></li>
                            <li><a href="{{ route('posts.submit-identified', $defaultRouteParams) }}" class="submit0-fancybox"><span class="pre-den"></span> <h3>@lang('submit0_identified')</h3></a></li>
                        </ul>
                    @elseif ($complaintTypes && $complaintTypes->count() > 1)
                        <div class="complaint-types">
                            <div class="types-title">
                                <img src="{{ asset($_front . 'images/icono-denuncia-anonima-blue.svg') }}" alt="@lang('Denuncia anónima')">
                                <h4>@lang('PRESENTAR DENUNCIA DE FORMA ANÓNIMA')</h4>
                            </div>
                            <ul>
                                @foreach($complaintTypes as $complaintType)
                                    {{--@if ($complaintType->id != $genericType->id)--}}
                                        <li>
                                            <a href="{{ route('posts.submit-anonymous', array_merge($defaultRouteParams, ['type' => $complaintType->id])) }}"><h3>{{ $complaintType->get_field('texto-boton') ?: $complaintType->title }}</h3></a>
                                            @if ($textBelow = $complaintType->get_field('texto-bajo-boton'))
                                                <span class="below-btn">{{ $textBelow }}</span>
                                            @endif
                                        </li>
                                    {{--@endif--}}
                                @endforeach
                            </ul>
                        </div><div class="types-divider"></div><div class="complaint-types">
                            <div class="types-title">
                                <img src="{{ asset($_front . 'images/icono-denuncia-blue.svg') }}" alt="@lang('Denuncia identificándose')">
                                <h4>@lang('PRESENTAR DENUNCIA IDENTIFICÁNDOSE')</h4>
                            </div>
                            <ul>
                                @foreach($complaintTypes as $complaintType)
                                    {{--@if ($complaintType->id != $genericType->id)--}}
                                        <li>
                                            <a href="{{ route('posts.submit-identified', array_merge($defaultRouteParams, ['type' => $complaintType->id])) }}" class="submit0-fancybox"><h3>{{ $complaintType->get_field('texto-boton') ?: $complaintType->title }}</h3></a>

                                            @if ($textBelow = $complaintType->get_field('texto-bajo-boton'))
                                                <span class="below-btn">{{ $textBelow }}</span>
                                            @endif
                                        </li>
                                    {{--@endif--}}
                                @endforeach
                            </ul>

                            <div class="frm-input checkbox click">
                                <input id="check1" type="checkbox">
                                <label for="check1">
                                    @lang('I have read and understood the') <span><a href="{{ route('posts.terms', ['locale' => app()->getLocale(), 'company' => $company->post_name]) }}" target="_blank">@lang('Terms and conditions')</a></span> @lang('of use of the complaint channel')
                                </label>
                            </div>
                        </div>
                    @endif
                </div>
                @if (!$complaintTypes || $complaintTypes->count() <= 1)
                    <div class="frm-input checkbox click">
                        <input id="check1" type="checkbox">
                        <label for="check1">
                            @lang('I have read and understood the') <span><a href="{{ route('posts.terms', ['locale' => app()->getLocale(), 'company' => $company->post_name]) }}" target="_blank">@lang('Terms and conditions')</a></span> @lang('of use of the complaint channel')
                        </label>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('popup')
    <!-- popup -->
    <div id="frm-popup">
        <div class="frm-popup-inn">
            <h2>@lang('You must read and accept the COMPANY NAME Terms and Conditions', ['company_name' => 'Marimón Abogados'])</h2>
            <input type="button" value="@lang('Ok')" class="ok-btn">
        </div>
    </div>
@endsection
