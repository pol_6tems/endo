@extends('Front::layouts.main')

@section('content')
    <section class="bienvenido inner-page min-ht small-page">
        <div class="row1">
            <h4><a href="{{ $company->get_url() }}">@lang('Go back')</a></h4>
            <h2>@lang('Consultar el estado de una denuncia')</h2>
            <div class="venido-form">
                <h3>@lang('Introduzca el código de referencia de la denuncia:')</h3>
                <p>@lang('Código de la denuncia (obligatorio)')</p>
                <form method="POST">
                    {{ csrf_field() }}
                    <input type="text" name="den" id="name" placeholder="DEN-XXXXXXXX" required>
                    <div class="clear"></div>
                    <button type="submit" class="consultar-but">@lang('CONSULTAR')</button>
                </form>
            </div>
        </div>
    </section>
@endsection