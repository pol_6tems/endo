<div>
    <h2>@lang('Adjuntar documentos')</h2>
    <p>@lang('Si dispone de documentación o archivos que fundamenten su notificación puede adjuntarlos en los formatos de archivo más comunes:') gif, jpg, jpeg, png, pdf, doc, docx, ppt, pptx, xls, xlsx, avi, mov, mp3, mp4.</p>

    <p class="attached-note">
        <strong>@lang('Nota sobre el envío de anexos:')</strong><br>
        @lang('Los archivos pueden contener datos personales ocultos que pongan en peligro su anonimato. Borre esos datos antes de enviar los archivos.')
    </p>
    <form id="dades-form" data-current="{{ 2 - $isAnonymous }}" class="form-box dades-form" method="POST">
        <div class="frm-input">
            <div class="frm-input-ctrl btn">
                <input class="form-control exam-input" type="text" disabled>
                <button class="exam-btn" type="button">@lang('EXAMINAR')</button>
            </div>
            <div class="clearfix-block"></div>
            <div class="file-list-container">
                <ul id="file-list">

                </ul>
            </div>
        </div>
        <div class="btns-chk">
            <button class="volver-btn" data-current="{{ 2 - $isAnonymous }}" type="button">@lang('Go back')</button>
            <button class="seg-btn">@lang('Next')</button>
        </div>
    </form>
</div>