<div>
    <h2>@lang('Personal information')</h2>
    <p>@lang('Es obligatorio rellenar todos lo campos para poder pasar al siguiente paso')</p>
    <form class="form-box dades-form" data-current="0" method="POST" action="javascript:void(0);">
        <div class="frm-lft">
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('Name')</label>
                    <input class="form-control" name="nombre" placeholder="@lang('Name')" type="text" required>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('Surname')</label>
                    <input class="form-control" name="apellidos" placeholder="@lang('Surname')" type="text" required>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('Phone number')</label>
                    <input class="form-control" name="telefono" placeholder="@lang('Phone number')" type="text" required>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>E-mail</label>
                    <input class="form-control" name="email" placeholder="E-mail" type="email" required>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('Relación con la entidad')</label>
                    <div class="frm-select">
                        <select class="select_box" name="relacion">
                            <?php
                                $params = json_decode($fields->where('name', 'relacion')->first()->params);
                                $choices = explode("\r\n", $params->choices);
                            ?>
                            @foreach($choices as $choice)
                                @php($choice = explode(':', $choice))
                                <option value="{{ $choice[0] }}">@lang($choice[0])</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="frm-rgt">
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('Address')</label>
                    <input class="form-control" name="direccion" placeholder="@lang('Address')" type="text" required>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>CP</label>
                    <input class="form-control" name="cp" placeholder="CP" type="text" required>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('City')</label>
                    <input class="form-control" name="ciudad" placeholder="@lang('City')" type="text" required>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('Country')</label>
                    <div class="frm-select">
                        <select class="select_box" name="pais">
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}">{{ $country->translate(app()->getLocale(), true)->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('Mejor medio para comunicarse con usted')</label>
                    <div class="frm-select">
                        <select class="select_box" name="comunicarse">
                            <?php
                            $params = json_decode($fields->where('name', 'comunicarse')->first()->params);
                            $choices = explode("\r\n", $params->choices);
                            ?>
                            @foreach($choices as $choice)
                                @php($choice = explode(':', $choice))
                                <option value="{{ $choice[0] }}">@lang($choice[1])</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-cont-btmbg">
            <h4>@lang('Pulsando ‘SIGUIENTE’ acepta la siguiente política de privacidad:')</h4>
            <p>@lang('submit1_next_disclaimer', ['company_name' => $company->title, 'contact_email' => $company->get_field('email-contacto')])</p>
        </div>
        <!--<div class="chk-box">
          <div class="frm-input checkbox">
            <input id="check1" type="checkbox">
            <label for="check1">Consiento de forma expresa el tratamiento de los datos de carácter personal que pueda proporcionar al utilizar el
              presente formulario conforme las finalidades expresamente recogidas en la Política de Privacidad</label>
          </div>
        </div>-->
        <button class="seg-btn">@lang('Next')</button>
    </form>
</div>