<header>
    <div class="mobile-head">
        <div class="row">
            @if (isset($company) && ($company->get_field('codigo-etico') || $company->get_field('protocolo-de-denuncia')))
                <a class="mob-download" href="#mobNav">@lang('Downloads')</a>
            @endif

            <div class="lang-mob">
                <ul>
                    @foreach ($_languages as $language)
                        <li><a @if ($language->code == $language_code)class="active" @endif href="@if ($language->code == $language_code)javascript:void(0);@else{{ (route_name() == 'posts' && $language->url) || !isset($company) ? $language->url : (request()->isMethod('get') ? route(route_name(), ['locale' => $language->code, 'company' => $company->post_name]) : $company->get_url($language->code)) }}@endif">{{ mb_strtoupper(mb_substr(__($language->name), 0, 3)) }}</a></li>
                    @endforeach
                </ul>
            </div>

        </div>

    </div>
    <div class="head-top">
        <div class="row">
            <div class="logo"><a href="{{ isset($company) ? $company->get_url() : 'https://marimon-abogados.com/' }}"><img src="{{ asset('Themes/Marimon/images/logo-marimon.jpg') }}" alt="Marimón Abogado" title="Marimón Abogado"></a></div>
            <!--Menu Starts-->
            <nav>
                <div id="smoothmenu" class="ddsmoothmenu">
                    <ul>
                        <li><a href="{{ isset($company) ? $company->get_url() : 'https://marimon-abogados.com/' }}">@lang('Home')</a></li>
                        @if (isset($company) && $code = $company->get_field('codigo-etico'))
                            <li><a class="download" href="{{ $code->get_thumbnail_url() }}" download>{{ $company->get_field('texto-codigo-etico') ?: __('Ethical code') }}</a></li>
                        @endif
                        @if (isset($company) && $protocol = $company->get_field('protocolo-de-denuncia'))
                            <li><a class="download" href="{{ $protocol->get_thumbnail_url() }}" download>{{ $company->get_field('texto-protocolo') ?: __('Complaints management protocol') }}</a></li>
                        @endif
                    </ul>
                </div>
            </nav>
            <div class="lang-selct">
                <ul>
                    @foreach ($_languages as $language)
                        @if ($company->get_url($language->code))
                            <li><a @if ($language->code == $language_code)class="active" @endif href="@if ($language->code == $language_code)javascript:void(0);@else{{ (route_name() == 'posts' && $language->url) || !isset($company) ? $language->url : (request()->isMethod('get') ? route(route_name(), ['locale' => $language->code, 'company' => $company->post_name]) : $company->get_url($language->code)) }}@endif">{{ mb_strtoupper(mb_substr(__($language->name), 0, 3)) }}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="cmp-logo"><a href="{{ isset($company) ? $company->get_url() : 'https://marimon-abogados.com/' }}">@if(isset($company) && $company->translate()->media)<img src="{{ $company->translate()->media->get_thumbnail_url() }}" alt="{{ $company->title }}" title="{{ $company->title }}">@endif</a></div>
        </div>
    </div>
</header>