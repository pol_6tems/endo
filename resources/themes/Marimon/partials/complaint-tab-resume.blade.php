<div>
    <h2>@lang('Revisar')</h2>
    <p>@lang('Revise la información proporcionada antes de presentar el formulario. Si desea modificar la información o complementarla, vuelva a la sección correspondiente.')</p>

    @if (!$isAnonymous)
        <div class="form-fill">
            <h3>@lang('Step NUMBER', ['number' => 1])</h3>
            <div class="frm-lft">
                <div class="form-update">
                    <h4>@lang('Name')</h4>
                    <p class="resume-nombre"></p>
                </div>
                <div class="form-update">
                    <h4>@lang('Surname')</h4>
                    <p class="resume-apellidos"></p>
                </div>
                <div class="form-update">
                    <h4>@lang('Phone number')</h4>
                    <p class="resume-telefono"></p>
                </div>
                <div class="form-update">
                    <h4>E-mail</h4>
                    <p class="resume-email"></p>
                </div>
                <div class="form-update">
                    <h4>@lang('Relación con la entidad')</h4>
                    <p class="resume-relacion"></p>
                </div>
            </div>
            <div class="frm-rgt">
                <div class="form-update">
                    <h4>@lang('Address')</h4>
                    <p class="resume-direccion"></p>
                </div>
                <div class="form-update">
                    <h4>CP</h4>
                    <p class="resume-cp"></p>
                </div>
                <div class="form-update">
                    <h4>@lang('City')</h4>
                    <p class="resume-ciudad"></p>
                </div>
                <div class="form-update">
                    <h4>@lang('Country')</h4>
                    <p class="resume-pais"></p>
                </div>
                <div class="form-update">
                    <h4>@lang('Mejor medio para comunicarse con usted')</h4>
                    <p class="resume-comunicarse"></p>
                </div>
            </div>
        </div>
    @endif

    <div class="form-fill">
        <h3>@lang('Step NUMBER', ['number' => 2 - $isAnonymous])</h3>
        <div class="form-fill-cont">
            <h1>@lang('¿Qué ha ocurrido?')</h1>
            <p class="resume-descripcion1"></p>
            <div class="content-more">
                <p class="resume-descripcion2"></p>
            </div>
            <a href="javascript:void(0);" class="leer-link">@lang('Read more')...</a>
            <a href="javascript:void(0);" class="leer-link hide">@lang('Read less')...</a>
        </div>
        <div class="frm-lft">
            <div class="form-update">
                <h4>@lang('¿Qué personas o cargos de la compañía se encuentran involucrados?')</h4>
                <p class="resume-involucrados"></p>
            </div>
            <div class="form-update">
                <h4>@lang('¿Dónde ha ocurrido?')</h4>
                <p class="resume-donde"></p>
            </div>
            <div class="form-update">
                <h4>@lang('¿Alguien más de la organización tiene conocimiento de los hechos?')</h4>
                <p class="resume-alguien-mas"></p>
            </div>
            <div class="form-update">
                <h4>@lang('¿Cuándo cree que se iniciaron los hechos?')</h4>
                <p class="resume-cuando"></p>
            </div>
            <div class="form-update">
                <h4>@lang('¿Durante cuánto tiempo han estado sucediendo estos hechos?')</h4>
                <p class="resume-cuanto-tiempo"></p>
            </div>
        </div>
        <div class="frm-rgt">
            <div class="form-update">
                <h4>@lang('¿Está sucediendo actualmente?')</h4>
                <p class="resume-actualmente"></p>
            </div>
            <div class="form-update">
                <h4>@lang('¿Cómo se dio cuenta de la presunta infracción?')</h4>
                <p class="resume-como"></p>
            </div>
            <div class="form-update">
                <h4>@lang('Suministre cualquier otro detalle que considere valioso para la investigación')</h4>
                <p class="resume-mas-detalle"></p>
            </div>
        </div>
    </div>
    <div class="form-fill">
        <div class="js-files-step" style="display: none">
            <h3>@lang('Step NUMBER', ['number' => 3 - $isAnonymous])</h3>
            <div class="file-list" data-titles="@lang('Archivo adjunto')" data-imgs="{{ asset('Themes/Marimon/images/icono-descarga-doc.svg') }}">
            </div>
        </div>
        <div class="form-cont-bg">
            <h4>@lang('Enviar formulario')</h4>
            <p>
                @if ($company->get_field('texto-enviar-denuncia'))
                    {!! str_replace('[company_name]', $company->title, $company->get_field('texto-enviar-denuncia')) !!}
                @else
                    @lang('Una vez haya enviado el formulario, éste será procesado de forma segura. Si quiere consultar nuestra respuesta a su denuncia, deberá hacerlo únicamente a través del apartado CONSULTAR ESTADO DE UNA DENUNCIA. Para acceder al apartado CONSULTAR ESTADO DE UNA DENUNCIA, situado en la página inicial de este sitio, introduzca el código de denuncia que se le mostrará una vez haya cursado su denuncia.')
                @endif
            </p>
        </div>
        <div class="btns-chk">
            <form id="resume-form" enctype="multipart/form-data" action="{{ route('posts.submit-complaint', ['locale' => app()->getLocale(), 'company' => $company->post_name]) }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="anonima" value="{{ $isAnonymous }}">
                <input type="hidden" name="estado" value="en_proceso">
                <input type="hidden" name="locale" value="{{ app()->getLocale() }}">

                {{-- Paso 1: identificar --}}
                <input type="hidden" class="form-nombre" name="nombre" value="">
                <input type="hidden" class="form-apellidos" name="apellidos" value="">
                <input type="hidden" class="form-telefono" name="telefono" value="">
                <input type="hidden" class="form-email" name="email" value="">
                <input type="hidden" class="form-relacion" name="relacion" value="">
                <input type="hidden" class="form-direccion" name="direccion" value="">
                <input type="hidden" class="form-cp" name="cp" value="">
                <input type="hidden" class="form-ciudad" name="ciudad" value="">
                <input type="hidden" class="form-pais" name="pais" value="">
                <input type="hidden" class="form-comunicarse" name="comunicarse" value="">

                {{-- Paso 2: detallar --}}
                <input type="hidden" class="form-descripcion" name="descripcion" value="">
                <input type="hidden" class="form-involucrados" name="involucrados" value="">
                <input type="hidden" class="form-donde" name="donde" value="">
                <input type="hidden" class="form-alguien-mas" name="alguien-mas" value="">
                <input type="hidden" class="form-cuando" name="cuando" value="">
                <input type="hidden" class="form-cuanto-tiempo" name="cuanto-tiempo" value="">
                <input type="hidden" class="form-actualmente" name="actualmente" value="">
                <input type="hidden" class="form-como" name="como" value="">
                <input type="hidden" class="form-mas-detalle" name="mas-detalle" value="">

                <input type="hidden" name="tipo-denuncia" value="{{ $tipoDenuncia }}">

                {{-- Paso 3: documentos --}}
                <input id="file-input0" type="file" name="docs[]" accept=".gif, .jpg, .jpeg, .png, .pdf, .doc, .docx, .ppt, .pptx, .xls, .xlsx, .avi, .mov, .mp3, .mp4" style="display: none">

                <button data-current="{{ 3 - $isAnonymous }}" class="volver-btn" type="button">@lang('Go back')</button>
                <button class="env-btn">@lang('Enviar formulario')</button>
            </form>
        </div>
    </div>
</div>