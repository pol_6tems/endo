<div>
    <h2>@lang('Descripción detallada')</h2>
    <p>@lang('Responda a las siguientes preguntas y proporcione tantos detalles como sea posible acerca del incidente que desea notificar')</p>
    <form id="dades-form" data-current="{{ 1 - $isAnonymous }}" class="form-box dades-form" method="POST">
        <div class="frm-input">
            <div class="frm-input-ctrl">
                <label>@lang('¿Qué ha ocurrido?')</label>
                <div class="frm-select">
                    <textarea class="txt-box" name="descripcion" placeholder="@lang('Describa aquí lo que le ha pasado')" required></textarea>
                </div>
            </div>
        </div>
        <div class="frm-lft">
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('¿Qué personas o cargos de la compañía se encuentran involucrados?')</label>
                    <input class="form-control" name="involucrados" type="text" required>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('¿Dónde ha ocurrido?')</label>
                </div>
                <div class="frm-input radio">
                    <?php
                        $params = json_decode($fields->where('name', 'donde')->first()->params);
                        $choices = explode("\r\n", $params->choices);
                    ?>
                    @foreach($choices as $key => $choice)
                        @php($choice = explode(':', $choice))
                            <input id="radio{{ $key }}_donde" name="donde" value="{{ $choice[0] }}" type="radio" {{ $key == 0 ? 'checked' : '' }}>
                            <label for="radio{{ $key }}_donde">@lang($choice[0], ['company_name' => $company->title])</label>
                    @endforeach
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('¿Alguien más de la organización tiene conocimiento de los hechos?')</label>
                </div>
                <div class="frm-input radio">
                    <?php
                    $params = json_decode($fields->where('name', 'alguien-mas')->first()->params);
                    $choices = explode("\r\n", $params->choices);
                    ?>
                    @foreach($choices as $key => $choice)
                        @php($choice = explode(':', $choice))
                        <input id="radio{{ $key }}_alguien-mas" name="alguien-mas" value="{{ $choice[0] }}" type="radio" {{ $key == 0 ? 'checked' : '' }}>
                        <label for="radio{{ $key }}_alguien-mas">@lang($choice[0])</label>
                    @endforeach
                </div>
            </div>
            <div class="frm-input cal-ico">
                <div class="frm-input-ctrl">
                    <label>@lang('¿Cuándo cree que se iniciaron los hechos?')</label>
                    <input type="text" name="cuando" class="datepicker" autocomplete="off" required/>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('¿Durante cuánto tiempo han estado sucediendo estos hechos?')</label>
                    <div class="frm-select">
                        <select class="select_box" name="cuanto-tiempo">
                            <?php
                                $params = json_decode($fields->where('name', 'cuanto-tiempo')->first()->params);
                                $choices = explode("\r\n", $params->choices);
                            ?>
                            @foreach($choices as $choice)
                                @php($choice = explode(':', $choice))
                                <option value="{{ $choice[0] }}">@lang($choice[0])</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="frm-rgt">
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('¿Está sucediendo actualmente?')</label>
                </div>
                <div class="frm-input radio">
                    <?php
                        $params = json_decode($fields->where('name', 'actualmente')->first()->params);
                        $choices = explode("\r\n", $params->choices);
                    ?>
                    @foreach($choices as $key => $choice)
                        @php($choice = explode(':', $choice))
                        <input id="radio{{ $key }}_actualmente" name="actualmente" value="{{ $choice[0] }}" type="radio" {{ $key == 0 ? 'checked' : '' }}>
                        <label for="radio{{ $key }}_actualmente">@lang($choice[0])</label>
                    @endforeach
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('¿Cómo se dio cuenta de la presunta infracción?')</label>
                    <div class="frm-select">
                        <select class="select_box" name="como">
                            <?php
                                $params = json_decode($fields->where('name', 'como')->first()->params);
                                $choices = explode("\r\n", $params->choices);
                            ?>
                            @foreach($choices as $choice)
                                @php($choice = explode(':', $choice))
                                <option value="{{ $choice[0] }}">@lang($choice[0])</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="frm-input">
                <div class="frm-input-ctrl">
                    <label>@lang('Suministre cualquier otro detalle que considere valioso para la investigación')</label>
                    <textarea class="txt-box1" name="mas-detalle" placeholder="@lang('Más detalle')"></textarea>
                </div>
            </div>
        </div>
        <div class="btns-chk">
            @if (!$isAnonymous)
                <button class="volver-btn" data-current="{{ 1 - $isAnonymous }}" type="button">@lang('Go back')</button>
            @endif
            <button class="seg-btn">@lang('Next')</button>
        </div>
    </form>
</div>