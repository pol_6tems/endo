@extends('Front::layouts.html')

@section('title', config('app.name') . (isset($company) ? ' - ' . $company->title : ''))

@section('body')
    <div id="page">
        @include('Front::partials.header')

        @yield('content')

        <footer>
            <div class="row">
                <div class="logo"><a href="{{ isset($company) ? $company->get_url() : 'https://marimon-abogados.com/' }}"><img src="{{ asset('Themes/Marimon/images/footer-logo.png') }}" alt="Marimón Abogado" title="Marimón Abogado"></a></div>
                {{--<ul>
                    <li><a href="javascript:void(0);">Términos de uso</a></li>
                  <li><a href="javascript:void(0);">Política de Privacidad</a></li>
                </ul>--}}
            </div>
        </footer>
    </div>

    @yield('popup')

    <div id="mobNav" class="mobNav">
        <figure class="mobi-logo"><a href="{{ isset($company) ? $company->get_url() : 'https://marimon-abogados.com/' }}" title="Marimón Abogado"><img src="{{ asset('Themes/Marimon/images/logo-marimon.jpg') }}" alt="Marimón Abogado"></a></figure>
        <ul>
            <li><a href="{{ isset($company) ? $company->get_url() : 'https://marimon-abogados.com/' }}">@lang('Home')</a></li>
            @if (isset($company) && $code = $company->get_field('codigo-etico'))
                <li><a href="{{ $code->get_thumbnail_url() }}">@lang('Ethical code')</a></li>
            @endif
            @if (isset($company) && $protocol = $company->get_field('protocolo-de-denuncia'))
                <li><a href="{{ $protocol->get_thumbnail_url() }}">@lang('Complaints management protocol')</a></li>
            @endif
            <li><a href="{{ isset($company) ? $company->get_url() : 'https://marimon-abogados.com/' }}" target="_blank">Marimón Abogados</a></li>
            <li class="mob-lang">
                @foreach ($_languages as $language)
                    <a @if ($language->code == $language_code)class="active" @endif href="@if ($language->code == $language_code)javascript:void(0);@else{{ (route_name() == 'posts' && $language->url) || !isset($company) ? $language->url : (request()->isMethod('get') ? route(route_name(), ['locale' => $language->code, 'company' => $company->post_name]) : $company->get_url($language->code)) }}@endif">{{ mb_strtoupper(mb_substr(__($language->name), 0, 3)) }}</a>
                @endforeach
            </li>
        </ul>
    </div>
@endsection