<script src="{{ asset($_front.'js/respond.js') }}"></script>

<script src="{{ mix('js/app.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>

<script>
    $(document).ready(function () {
        @if ($message = session()->get('message'))
        toastr.success('{!! $message !!}');
        @endif

        @if ($error = session()->get('error'))
        toastr.error('{!! $error !!}');
        @endif
    });
</script>

@yield('bottom_foot_scripts')