<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<meta name="robots" content="noindex, nofollow">

@if ( !empty($_config['app_icon']['file']) )
    <link rel="apple-touch-icon" sizes="76x76" href="{{ $_config['app_icon']['obj']->get_file_url() }}">
    <link rel="icon" type="image/png" href="{{ $_config['app_icon']['obj']->get_file_url() }}">
@endif

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield("title", config('app.name', 'Endo'))</title>

<link href="{{ mix('css/app.css', 'Themes/' . env('PROJECT_NAME')) }}" rel="stylesheet">

@yield('head_metas')