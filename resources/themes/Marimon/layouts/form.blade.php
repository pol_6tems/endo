@extends('Front::layouts.main')

@section('head_metas')
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
@endsection

@section('content')
    <section class="form-tabs">
        <div class="row">
            <h4><a class="js-return-button" href="{{ route('posts.access', ['locale' => app()->getLocale(), 'company' => $company->post_name]) }}">@lang('Go back')</a> <span>@lang($complaintTitle)</span></h4>

            @yield('form')
        </div>
    </section>
@endsection

@section('bottom_foot_scripts')
    <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=6Lc3P7AUAAAAADqtOuV-04Y8Ytsx9rie9WwUTC-k"></script>

    <script type="text/javascript">
        $(window).on('load', function() {
            $('#form-details').easyResponsiveTabs({
                type: 'default',
                width: 'auto',
                fit: true,
                tabidentify: 'hor_1'
            });

            $(".frm-input-ctrl.dropdown select").selectbox();

            $('ul.resp-tabs-list > li').off('click');
            $('.resp-tabs-container').find("[role=tab]").each(function () {
                $(this).off('click');
            });
        });

        $(function() {
            @if (app()->getLocale() != 'en')
                $.datepicker.regional['es'] = {
                    closeText: 'Cerrar',
                    prevText: '< Ant',
                    nextText: 'Sig >',
                    currentText: 'Hoy',
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                    weekHeader: 'Sm',
                    dateFormat: 'dd/mm/yy',
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: ''
                };
                $.datepicker.setDefaults($.datepicker.regional['es']);
            @endif

            $(".datepicker").datepicker({ dateFormat: 'dd/mm/yy', maxDate: new Date });
        });
    </script>
@endsection