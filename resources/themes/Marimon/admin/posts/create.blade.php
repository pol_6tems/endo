@extends('Admin::layouts.admin')

@section('section-title')
    @Lang(ucfirst($post_type_plural))
@endsection

@section('content')
    @php
        $params = ($params) ? json_decode($params) : null;

        $trobat = false;
        foreach ($cf_groups as $key => $cf_group) {
            if($cf_group->position == 'r-sidebar') {
                $trobat = true;
                break;
            }
        }

        // Revisem tots els parametres per saber si hem de reservar espai per la sidebar
        $sidebar = in_array($post_type, ['post', 'page']) ||
            (
                $params && isset($params->published) && $params->published ||
                $params && isset($params->author) && $params->author ||
                $params && isset($params->parent) && $params->parent ||
                $params && isset($params->imatge) && $params->imatge
            ) ||
            $trobat;
    @endphp
    <form class="form-horizontal js-form" action='{{ route("admin.posts.store", ["post_type" => $post_type]) }}' method="post">
        {{ csrf_field() }}
        <input type="hidden" name="status" value="publish" />
        <input type="hidden" name="type" value="{{ $post_type }}" />
        <div class="row">
            <div class="col-md-12">
                <div class="flex-row">
                    <div class="col-md-{{ ($sidebar) ? '8 mr-auto' : '12' }}">
                        <article class="card">
                            <div class="card-header card-header-primary card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">library_books</i>
                                </div>
                                <h4 class="card-title">
                                    @Lang('Add new')
                                </h4>
                            </div>

                            <div class="card-body">
                                @includeIf('Front::partials.edit-post-disclaimer')

                                @if ($errors->count() > 0)
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif

                                @if (count($_languages) > 2)
                                    <div class="toolbar">
                                        <ul class="nav nav-pills nav-pills-primary">
                                            @foreach($_languages as $language_item)
                                                <li class="nav-item {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                                                    <a class="nav-link {{ ($language_item->code == $language_code) ? 'active' : '' }}" data-toggle="tab" href="#{{$language_item->code}}">{{$language_item->code}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="tab-content">
                                    @foreach($_languages as $language_item)
                                        <div id="{{$language_item->code}}" class="tab-pane {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                                            <!-- Afegir Tabs Generals pe Post Type  -->
                                            <div class="flex-row">
                                                <label class="col-md-12 col-form-label">@Lang('Title')</label>
                                                <div class="col-md-12">
                                                    <div class="form-group has-default">
                                                        <input name="{{$language_item->code}}[title]" type="text" class="form-control" value="{{ old('title') }}" tabindex=1 {{ ($language_item->code == $language_code) ? 'required' : '' }}>
                                                    </div>
                                                </div>
                                            </div>
                                            @if ( in_array($post_type, ['post', 'page']) || ($params && isset($params) && $params->content) )
                                                <div class="flex-row {{ ($language_item->code != $language_code) ? 'summernote-not-first' : '' }}">
                                                    <label class="col-md-12 col-form-label">@Lang('Description')</label>
                                                    <div class="col-md-12">
                                                        <div class="form-group has-default">
                                                            <textarea id="description" name="{{$language_item->code}}[description]" class="form-control summernote" tabindex=2></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                        @endif
                                        <!-- End Pestanyes -->
                                        </div>
                                    @endforeach

                                    @foreach ($cf_groups as $key => $cf_group)
                                        @if($cf_group->position == 'main')

                                            @if (Auth::user()->isAdmin())
                                                <div class="flex-row mt-4" style="justify-content: flex-end;">
                                                    <div class="header-right">
                                                        <a href="{{ route('admin.custom_fields.edit', $cf_group->id) }}?post_type={{$post_type}}" target="_blank" rel="tooltip">
                                                            <i class="material-icons">settings</i>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif

                                            <div id="cf_group{{ $cf_group->id }}">
                                            {{ mostrarIdiomes($_languages, $language_code, 'panel'.$cf_group->id) }}
                                            <!-- Tab de General -->
                                                <div class="tab-content">
                                                    @foreach($_languages as $key => $language_item)
                                                        <div id="panel{{ $cf_group->id }}{{ $language_item->code }}" class="tab-pane {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                                                            <div class="custom_field_group_fields">
                                                            @foreach ($cf_group->fields as $k => $custom_field)
                                                                @php($viewParams = [
                                                                    'title' => $custom_field->title,
                                                                    'name' => "custom_fields[".$language_item->code."][".$custom_field->id . "]",
                                                                    'value' => isset($marimon_default[$language_item->code][$custom_field->name]) ? $marimon_default[$language_item->code][$custom_field->name] : "",
                                                                    'params' => json_decode($custom_field->params),
                                                                    'position' => $cf_group->position,
                                                                    'custom_field' => $custom_field,
                                                                    'order' => $k,
                                                                    'lang' => $language_item->code,
                                                                ])

                                                                @if (View::exists('Admin::default.custom_fields.' . $custom_field->type))
                                                                    <!-- General -->
                                                                        @includeIf('Admin::default.custom_fields.' . $custom_field->type, $viewParams)
                                                                    @else
                                                                        @php($module = explode(".", $custom_field->type)[0])
                                                                        @php($field = explode(".", $custom_field->type)[1])
                                                                        @includeIf('Modules::'.$module.'.resources.views.custom_fields.'.$field, $viewParams)
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach

                                </div>
                            </div>
                    </div>
                    </article>

                    @if( $sidebar )
                        @includeIf('Admin::partials.posts-sidebar')
                    @endif

                    @foreach ( $cf_groups as $key => $cf_group )
                        @if( $cf_group->position == 'bottom' )
                            <article id="post-cfg{{ $cf_group->id }}" class="card pm">
                                <div class="card-header card-header-primary card-header-icon">
                                    <div class="flex-row" style="justify-content: space-between;">
                                        <div class="header-left">
                                            <h4 class="card-title">{{ $cf_group->title }}</h4>
                                        </div>
                                        <div class="header-right">
                                            <a data-toggle="collapse" href="#cf_group{{ $cf_group->id }}"><i class="material-icons">keyboard_arrow_down</i></a>
                                            <a href="{{ route('admin.custom_fields.edit', $cf_group->id) }}?post_type={{$post_type}}" target="_blank" rel="tooltip">
                                                <i class="material-icons">settings</i>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div id="cf_group{{ $cf_group->id }}" class="collapse show">
                                    <div class="card-body pb-5">
                                    {{ mostrarIdiomes($_languages, $language_code, 'panel'.$cf_group->id) }}
                                    <!-- Tab de General -->
                                        <div class="tab-content">
                                            @foreach($_languages as $key => $language_item)
                                                <div id="panel{{ $cf_group->id }}{{ $language_item->code }}" class="tab-pane {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                                                    <div class="custom_field_group_fields">
                                                    @foreach ($cf_group->fields as $k => $custom_field)
                                                        @php($viewParams = [
                                                            'title' => $custom_field->title,
                                                            'name' => "custom_fields[".$language_item->code."][".$custom_field->id . "]",
                                                            'value' => isset($marimon_default[$language_item->code][$custom_field->name]) ? $marimon_default[$language_item->code][$custom_field->name] : "",
                                                            'params' => json_decode($custom_field->params),
                                                            'position' => $cf_group->position,
                                                            'custom_field' => $custom_field,
                                                            'order' => $k,
                                                            'lang' => $language_item->code,
                                                        ])

                                                        @if (View::exists('Admin::default.custom_fields.' . $custom_field->type))
                                                            <!-- General -->
                                                                @includeIf('Admin::default.custom_fields.' . $custom_field->type, $viewParams)
                                                            @else
                                                                @php($module = explode(".", $custom_field->type)[0])
                                                                @php($field = explode(".", $custom_field->type)[1])
                                                                @includeIf('Modules::'.$module.'.resources.views.custom_fields.'.$field, $viewParams)
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </article>
                        @endif
                    @endforeach

                    <div class="col-md-12">
                        @if (isset($save_disclaimer) && $save_disclaimer)
                            <div>
                                {{ $save_disclaimer }}
                            </div>
                        @endif

                        <div class="card sticky bottom">
                            <div class="card-footer">
                                <div class="mr-auto">
                                    <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" name="previous" value="@Lang('Tornar')" onclick="window.location.href='{{ route('admin.posts.index', ['post_type' => $post_type]) }}'">
                                </div>
                                <div class="ml-auto">
                                    <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd js-submit-btn" name="next" value="@Lang('Publish')">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('styles')
    <link href="{{asset($_admin.'js/summernote/summernote.css')}}" rel="stylesheet">
@endsection

@section('scripts')
    <script src="{{asset($_admin.'js/summernote/summernote.js')}}"></script>
    <script> var imageListEndpoint = "{{ route('admin.get.images') }}"; </script>
    <script src="{{asset($_admin.'js/summernote.initialize.js')}}"></script>
    <script src="{{asset($_admin.'js/summernote/plugin/image-list/summernote-image-list.min.js')}}"></script>
    <script src="{{asset('js/stickyfill.min.js')}}"></script>
    <script>
        var elements = document.querySelectorAll('.sticky');
        Stickyfill.add(elements);
    </script>


    @php( $summernote_lang = 'en-US' )
    @if( \App::getLocale() == 'ca' )
        @php( $summernote_lang = 'ca-ES' )
    @else
        @php( $summernote_lang = strtolower(\App::getLocale()) . '-' . strtoupper(\App::getLocale()) )
    @endif

    @if ( \App::getLocale() != 'en' )
        <script src="{{asset($_admin.'js/summernote/lang/summernote-'.$summernote_lang.'.js')}}"></script>
    @endif
    <script>
        /* Summernote de Descripcio */
        $(document).ready(function() {
            registerSummernote('.summernote', '@Lang('Leave a comment')', 400, '{{ $summernote_lang }}' );
        });

        $('.custom_field_group_title').click(function(){
            var card = $(this).closest('.card');
            $(card).toggleClass('hidden');
            $(card).toggleClass('pb-5');
            $(card).find('.custom_field_group_fields').fadeToggle();
        });
    </script>

    @includeIf('Admin::posts.media_modal')

    {{-- GALLERY CUSTOM FIELD --}}
    <script>
        function delete_gallery_element(btn) {
            $(btn).closest('.fileinput[data-provides=fileinput]').remove();
        }
        $('.custom_field_gallery .add_image_to_gallery').click(function(){
            var id_button_gallery = $(this).attr('id');
            var parent_gallery = $(this).closest('.custom_field_gallery');
            var num_gallery_img = $(parent_gallery).find('.fileinput[data-provides=fileinput]').length;
            var clone = $(parent_gallery).find('#clone');
            var new_gallery_image = $(clone).clone();
            var new_name_gallery = id_button_gallery + '[' + num_gallery_img + ']';
            $(new_gallery_image).attr('id', '');
            $(new_gallery_image).find('input.media_input[type=hidden]').attr('name', new_name_gallery);
            $(new_gallery_image).insertBefore( $(clone) );
            $(new_gallery_image).fadeIn();
            var new_btn_file = $(new_gallery_image).find('.btn-file');
            show_media_modal(new_btn_file);
        });


        $('.js-submit-btn').on('click', function () {
            var form = $('.js-form');

            if (! form[0].checkValidity()) {
                var invalidInput = form.find('input:invalid').first();

                if (!invalidInput.is(':visible')) {
                    var inputName = invalidInput.attr('name');
                    var lang = inputName.match(/\[[a-z].\]/)[0];

                    lang = lang.replace('[', '').replace(']', '');

                    $('.nav-lang-' + lang).click();
                }
            }
        });
    </script>
    {{-- end GALLERY CUSTOM FIELD --}}

    @yield('scripts2')

    {{-- Declarem tots els espais per els scripts dels CF --}}
    @foreach ($cf_groups as $key => $cf_group)
        @foreach ($cf_group->fields as $custom_field)
            <!-- Scripts CF id: {{ $custom_field->id }}-->
            @yield("scripts-cf$custom_field->id")
        @endforeach
    @endforeach

@endsection