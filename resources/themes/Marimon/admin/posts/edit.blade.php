@extends('Admin::layouts.admin')

@section('section-title')
    @lang(ucfirst($post_type_plural))
@endsection

@section('content')
    @php
        $params = ($params) ? json_decode($params) : null;

        $trobat = false;
        foreach ($cf_groups as $key => $cf_group) {
            if($cf_group->position == 'r-sidebar') {
                $trobat = true;
                break;
            }
        }

        // Revisem tots els parametres per saber si hem de reservar espai per la sidebar
        $sidebar =  in_array($post->type, ['post', 'page']) || ($params && isset($params->published) && $params->published ||
                    $params && isset($params->author) && $params->author ||
                    $params && isset($params->imatge) && $params->imatge) || $trobat;
    @endphp
    <div class="row">
        <div class="col-lg-12">
            <article id="post{{ $post->id }}" data-post="{{ $post->id }}" class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">library_books</i>
                    </div>
                    <h4 class="card-title">
                        @Lang('Status')
                    </h4>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action='{{ route('admin.denuncias.update', $post->id) }}?post_type={{$post_type}}' method="post">
                        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                        <input type="hidden" name="status" value="{{ $post->status }}" />
                        <input type="hidden" name="type" value="{{ $post->type }}" />

                        <div class="form-group">
                            <?php
                            $fields = $cf_groups->first()->fields;
                            $paramsSelect = json_decode($fields->where('name', 'estado')->first()->params);
                            $choices = explode("\r\n", $paramsSelect->choices);
                            ?>

                            <select name="estado"
                                    class="selectpicker complaint-status"
                                    data-style="btn btn-primary"
                                    data-live-search="true">
                                @foreach($choices as $choice)
                                    @php($choice = explode(':', $choice))
                                    <option value="{{ $choice[0] }}" @if ($post->get_field('estado') == $choice[0]) selected @endif>@lang($choice[0])</option>
                                @endforeach
                            </select>

                            <?php
                            $fields = $cf_groups->first()->fields;
                            $paramsSelect = json_decode($fields->where('name', 'razon-archivada')->first()->params);
                            $choices = explode("\r\n", $paramsSelect->choices);
                            ?>

                            <select name="razon-archivada"
                                    class="selectpicker archived-reason"
                                    data-style="btn btn-primary"
                                    data-live-search="true"
                                    @if ($post->get_field('estado') != 'archivada')style="display: none"@endif
                                     >
                                @foreach($choices as $choice)
                                    @php($choice = explode(':', $choice))
                                    <option value="{{ $choice[0] }}" @if ($post->get_field('razon-archivada') == $choice[0]) selected @endif>@lang($choice[0])</option>
                                @endforeach
                            </select>

                            <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd pull-right" name="next" value="@Lang('Update')">
                        </div>
                    </form>
                </div>
            </article>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <article id="post{{ $post->id}}-cfg{{ $cf_group->id }}" data-post="{{ $post->id }}" class="card pm">
                <div class="card-header card-header-primary card-header-icon">
                    <h4 class="card-title">@lang($cf_groups[0]->title)</h4>
                </div>

                <div class="card-body pb-5">
                    <div class="row">
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>@lang('Id'):</dt><dd>{{ $post->translate(app()->getLocale(), true)->title }}</dd>
                                <dt>@lang('Tipo denuncia'):</dt><dd>{{ $post->get_field('tipo-denuncia') ? $post->get_field('tipo-denuncia')->title : __('GENÉRICA') }}</dd>
                                <dt>@lang('Type'):</dt><dd>{{ $post->get_field('anonima') ? __('Anonima') : __('Registrada') }}</dd>
                                <dt>@lang('Created'):</dt><dd>{{ $post->created_at->format('d/m/Y H:m:s') }}</dd>
                                <dt>@lang('Updated'):</dt><dd>{{ $post->updated_at->format('d/m/Y H:m:s') }}</dd>
                                <dt>@lang('Company'):</dt><dd>
                                    @if ($post->parent)
                                        <a href="{{ route('admin.posts.index', ['post_type' => $post_type, 'parent' => $post->parent->id]) }}">
                                            {{ $post->parent->title }}
                                        </a>
                                    @else
                                        @lang('No existe')
                                    @endif
                                </dd>
                            </dl>
                        </div>
                        @if ($post->parent && $post->parent->translate()->media)
                            <div class="col-sm-3 col-sm-offset-3">
                                <img src="{{ $post->parent->translate()->media->get_thumbnail_url() }}" style="width: 100%; max-width: 128px">
                            </div>
                        @endif
                    </div>

                    @if (!$post->get_field('anonima'))
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <dl class="dl-horizontal">
                                    <dt>@lang('Name'):</dt><dd>{{ $post->get_field('nombre') }}</dd>
                                    <dt>@lang('Surname'):</dt><dd>{{ $post->get_field('apellidos') }}</dd>
                                    <dt>@lang('Phone number'):</dt><dd>{{ $post->get_field('telefono') }}</dd>
                                    <dt>Email:</dt><dd>{{ $post->get_field('email') }}</dd>
                                    <dt>@lang('Relación con la entidad'):</dt><dd>@lang($post->get_field('relacion'))</dd>
                                </dl>
                            </div>

                            <div class="col-md-6">
                                <dl class="dl-horizontal">
                                    <dt>@lang('Address'):</dt><dd>{{ $post->get_field('direccion') }}</dd>
                                    <dt>CP:</dt><dd>{{ $post->get_field('cp') }}</dd>
                                    <dt>@lang('City'):</dt><dd>{{ $post->get_field('ciudad') }}</dd>
                                    <dt>@lang('Country'):</dt><dd>{{ $post->get_field('pais')->translate(app()->getLocale(), true)->title }}</dd>
                                    <dt>@lang('Medio para comunicarse'):</dt><dd>@lang($post->get_field('comunicarse'))</dd>
                                </dl>
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <h5><strong>@lang('Description')</strong></h5>
                        </div>
                        <div class="col-md-12">
                            <p>{!! nl2br($post->get_field('descripcion')) !!}</p>
                        </div>

                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>@lang('Personas involucradas'):</dt><dd>{!! $post->get_field('involucrados') ?: '&nbsp;'  !!}</dd>
                                <dt>@lang('Donde'):</dt><dd>@lang($post->get_field('donde'), ['company_name' => $company ? $company->title : ''])</dd>
                                <dt>@lang('Alguien más'):</dt><dd>@lang($post->get_field('alguien-mas'))</dd>
                                <dt>@lang('Cuando se inició'):</dt><dd>{!! $post->get_field('cuando') ?: '&nbsp;'  !!}</dd>
                                <dt>@lang('Cuanto tiempo'):</dt><dd>@lang($post->get_field('cuanto-tiempo'))</dd>
                            </dl>
                        </div>

                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>@lang('Está sucediendo'):</dt><dd>@lang($post->get_field('actualmente'))</dd>
                                <dt>@lang('Como se dio cuenta'):</dt><dd>@lang($post->get_field('como'))</dd>
                                <dt>@lang('Otros detalles'):</dt><dd>{!! nl2br($post->get_field('mas-detalle')) !!}</dd>
                            </dl>
                        </div>
                    </div>

                    @if ($docs->count())
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h5><strong>@lang('Documentos')</strong></h5>
                            </div>

                            @foreach($docs as $doc)
                                <div class="col-md-6">
                                    <a href="{{ $doc->getUrl() }}" download>
                                        <div class="file_download">
                                            <span><i class="material-icons">save_alt</i> {{ $doc->filename . '.' . $doc->file_type }}</span>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </article>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var complaintStatusSelect = $('select.complaint-status');

            if (complaintStatusSelect.val() !== 'archivada') {
                $('.archived-reason').hide();
            }

            complaintStatusSelect.on('change', function () {
                if ($(this).val() === 'archivada') {
                    $('.archived-reason').show();
                } else {
                    $('.archived-reason').hide();
                }
            });
        });
    </script>
@endsection