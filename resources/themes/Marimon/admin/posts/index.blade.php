@extends('Admin::layouts.admin')

@section('section-title')
    @lang(ucfirst($post_type_plural))
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">library_books</i>
                    </div>
                    <h4 class="card-title">
                        @lang(ucfirst($post_type_plural))
                        {{--@if ( $status != 'trash')
                            <a href='{{ route($section_route . ".create", ["post_type" => $post_type]) }}' class="btn btn-primary btn-sm">@Lang('Add')</a>
                        @endif--}}
                    </h4>
                </div>
                <div class="card-body">
                    <div class="material-datatables">
                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                            <tr>
                                <th>@lang('Id')</th>
                                <th width="15%">@lang('Company')</th>
                                <th width="15%">@lang('Type')</th>
                                <th width="15%">@lang('Client')</th>
                                <th width="25%">@lang('Description')</th>
                                <th width="15%">@lang('Status')</th>
                                <th width="15%">@lang('Created at')</th>
                                <th width="10%" class="disabled-sorting text-right">@lang('Actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td>
                                        <a href="{{ route('admin.denuncias.edit', ['locale' => app()->getLocale(), 'id' => $item->id, 'post_type' => $post_type]) }}">
                                            {{ $item->translate(\App::getLocale(), true)->title }}
                                        </a>
                                        @if ( $item->id == $home_page_id )
                                            <i style="font-size: 11px;"> - @lang('Front')</i>
                                        @endif
                                    </td>

                                    <td>
                                        @if ($item->parent)
                                            <a href="{{ route('admin.posts.index', ['post_type' => $post_type, 'parent' => $item->parent->id]) }}">
                                                {{ $item->parent->title }}
                                            </a>
                                        @endif
                                    </td>

                                    <td>
                                        @php ($complaintType = $item->get_field('tipo-denuncia'))
                                        {{ $complaintType ? $complaintType->title : __('GENÉRICA') }}
                                    </td>

                                    <td>{{ $item->get_field('anonima') ? __('Anonima') : $item->get_field('nombre') . ' ' . $item->get_field('apellidos') }}</td>

                                    <td>
                                        {!! cut_text($item->get_field('descripcion')) !!}
                                    </td>

                                    <td>@lang($item->get_field('estado'))</td>
                                    <td>
                                        {{ $item->created_at->format('d/m/Y') . ' ' . $item->created_at->format('H:i') }}
                                    </td>

                                    <td class="td-actions text-right">
                                        <a href="{{ route('admin.denuncias.edit', ['locale' => app()->getLocale(), 'id' => $item->id, 'post_type' => $post_type]) }}" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-success" title="@lang('Edit')">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <button data-url="{{ route('admin.posts.destroy', $item->id) }}?post_type={{$post_type}}" data-name="@Lang('Are you sure to delete :name?', ['name' => $item->title])" type="button" data-toggle="tooltip" data-placement="top" class="btn btn-just-icon btn-sm btn-danger remove" title="@Lang('Delete')">
                                            <i class="material-icons">close</i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var table = $('.table').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [25, 50, -1],
                    [25, 50, "@lang('All')"]
                ],
                "order": [[ 5, "desc" ]],
                responsive: true,
                language: { "url": "{{ app()->getLocale() == 'es' ? asset('js/datatables/Spanish.json') : asset('js/datatables/Catalan.json') }}" }
            });

            table.on('click', '.switch_active', function(e) {
                var boto = $(this);
                var url = $(this).data('url');
                var title = $(this).data('name');
                var text = "@lang('actived')";
                var id = $(this).data('id');
                var onlyone = $(this).data('onlyone');
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {'_token': '{{csrf_token()}}', 'id': id},
                    success: function(data) {
                        if ( data.active ) {
                            swal({
                                title: "{{ucfirst(__('actived'))}}",
                                text: title + " @lang('actived')",
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            });
                            if ( onlyone ) {
                                $('.switch_active').removeClass('btn-success');
                                $('.switch_active').addClass('btn-danger');
                                $('.switch_active').find('i').html('clear');
                            }
                            boto.removeClass('btn-danger');
                            boto.addClass('btn-success');
                            boto.find('i').html('check');
                        } else {
                            swal({
                                title: "{{ucfirst(__('deactived'))}}",
                                text: title + " @lang('deactived')",
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            });
                            boto.addClass('btn-danger');
                            boto.removeClass('btn-success');
                            boto.find('i').html('clear');
                        }
                        e.preventDefault();
                    },
                    error: function(data) {
                        swal({
                            title: "@lang('Error')",
                            text: "@lang('Error saving data')",
                            type: 'error',
                            confirmButtonClass: "btn",
                            buttonsStyling: false
                        });
                    }
                });
            });

            // Delete a record
            table.on('click', '.remove', function(e) {
                var $tr = $(this).closest('tr');
                var url = $(this).data('url');
                var title = $(this).data('name');
                swal({
                    text: title,
                    //title: 'Are you sure?',
                    //text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: '@Lang('Delete')',
                    cancelButtonText: '@Lang('Cancel')',
                    buttonsStyling: false
                }).then(function(result) {
                    if ( result.value ) {
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: {'_token': '{{csrf_token()}}', '_method': 'DELETE'},
                            success: function(data) {
                                swal({
                                    title: "@Lang('Deleted')",
                                    text: "@Lang('Item deleted succesfully.')",
                                    type: 'success',
                                    confirmButtonClass: "btn btn-success",
                                    buttonsStyling: false
                                });
                                table.row($tr).remove().draw();
                                e.preventDefault();
                            },
                            error: function(data) {
                                swal({
                                    title: "@Lang('Error')",
                                    text: "@Lang('Error saving data')",
                                    type: 'error',
                                    confirmButtonClass: "btn",
                                    buttonsStyling: false
                                });
                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection