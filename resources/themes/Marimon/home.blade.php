@extends('Front::layouts.main')

@section('content')
	<section class="bienvenido bienvenido-home">
		<div class="row1">
			<div class="bienvenido-lft">
				<h2>
					@if ($company->get_field('texto-inicio-titulo'))
						{!! str_replace('[company_name]', $company->title, $company->get_field('texto-inicio-titulo')) !!}

						@if ($company->get_field('texto-inicio-subtitulo'))
							<span>{!! str_replace('[company_name]', $company->title, $company->get_field('texto-inicio-subtitulo')) !!}</span>
						@endif
					@else
						@lang('home_welcome', ['company_name' => $company->title])
					@endif
				</h2>
				@if ($company->get_field('texto-inicio-bienvenida'))
					{!! str_replace('[company_name]', $company->title, $company->get_field('texto-inicio-bienvenida')) !!}
				@else
					<p>@lang('home_integrity')</p>
					<h3>@lang('home_why', ['company_name' => $company->title])</h3>
					<h4>@lang('home_enforce')</h4>
					<p>@lang('home_channel_exists', ['company_name' => $company->title])</p>

					<h4>@lang('home_prevent')</h4>
					<p>@lang('home_secure_channel', ['company_name' => $company->title])</p>

					<div class="content-more">
						<h4>@lang('home_grant')</h4>
						<p>@lang('home_access', ['company_name' => $company->title])</p>
						<p>@lang('home_confidence')</p>
						<p>@lang('home_compromise')</p>
						<h3>@lang('home_who', ['company_name' => $company->title])</h3>
						<p>@lang('home_values')</p>
						<h3>@lang('home_who_use')</h3>
						<p>@lang('home_relation', ['company_name' => $company->title])</p>
					</div>
					<a href="javascript:void(0);" class="leer-link">@lang('Read more')...</a>
				@endif
			</div>

			<div class="bienvenido-rht">
				<div class="atencion">
					<p>
						@if ($company->get_field('texto-inicio-aviso'))
							{{ str_replace('[company_name]', $company->title, $company->get_field('texto-inicio-aviso')) }}
						@else
							@lang('home_disclaimer', ['company_name' => $company->title])
						@endif
					</p>
				</div>
				<div class="atencion-lst">
					<ul>
						<li><a href="{{ route('posts.access', ['locale' => app()->getLocale(), 'company' => $company->post_name]) }}"><span class="pre-den"></span> <h3>@lang('Submit a complaint')</h3></a></li>
						<!--<li><a href="javascript:void(0);"><span class="pre-ano"></span> PRESENTAR DENUNCIA ANÓNIMA</a></li>-->
						<li ><a href="{{ route('posts.get-status', ['locale' => app()->getLocale(), 'company' => $company->post_name]) }}"><span class="con-den"></span> <h3>@lang('See status of a complaint')</h3></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('bottom_foot_scripts')

@endsection
