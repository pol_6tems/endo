@extends('Admin::layouts.admin')

@section('content')

    <section class="bienvenido vol min-ht">
        <div class="row1">
            {{--<h4><a href="{{ $company->get_url() }}">@lang('Return')</a></h4>--}}
            <h2>Estado de la Denuncia</h2>
            <div id="estado-details">
                <ul class="complain-status">
                    <li @if ($item->get_field('estado') == 'en_proceso')class="active-status" @endif>EN PROCESO</li>
                    <li @if ($item->get_field('estado') == 'en_tramite')class="active-status" @endif>EN TRÁMITE</li>
                    <li @if ($item->get_field('estado') == 'archivada')class="active-status" @endif>ARCHIVADA</li>
                </ul>
                <div class="resp-tabs-container hor_1">
                @if ($item->get_field('estado') == 'en_proceso')
                    <!-- tab 1 -->
                        <div>
                            <h3>Denuncia nº {{ $item->title }}</h3>
                            <p>Plazo máximo de 5 días hábiles desde la recepción de la denuncia para su análisis con el objeto de determinar si es admitida para su tramitación y gestión.</p>
                        </div>
                @endif

                @if ($item->get_field('estado') == 'en_tramite')
                    <!-- tab 2 -->
                        <div>
                            <h3>Denuncia nº {{ $item->title }}</h3>
                            <p>La denuncia se enmarca en el ámbito objetivo del canal de denuncias.</p>
                            <p>Desde este momento, el Órgano de Control Interno de COMPANY NAME será el responsable de abrir una investigación interna en la entidad sobre los hechos denunciados.</p>
                            <div class="content-more">
                                <p>En todo caso, deberá ser informado por el órgano de Control Interno de la entidad (en el plazo máximo de 3 meses desde la presentación de su denuncia) sobre el estado de la investigación.</p>
                                <p>En caso de haber presentado una denuncia anónima no se le podrá informar sobre el estado de la investigación interna en COMPANY NAME.</p>
                            </div>
                            <a href="javascript:void(0);" class="leer-link">Leer más...</a>
                        </div>
                @endif

                @if ($item->get_field('estado') == 'archivada')
                    <!-- tab 3 -->
                        <div>
                            <h3>Denuncia nº {{ $item->title }}</h3>
                            <p>La denuncia carece manifiestamente de fundamento.</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection