@extends('Front::layouts.main')

@section('content')
    <section class="bienvenido inner-page">
        <div class="row1">
            <h2>{{ $item->title }}</h2>
            {!! $item->description !!}
        </div>
    </section>
@endsection