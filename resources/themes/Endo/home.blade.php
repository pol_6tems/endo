@extends('layouts.app')

@section('header')
<div class="title m-b-md flex-center" style="padding-top: 20vh;">
	Endo
</div>
@endsection

@section('content')
	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif
	<div style="text-align: center;">Front end layout</div>
@endsection
