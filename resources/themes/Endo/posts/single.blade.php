@extends('layouts.app')

@section('header')
<div class="title m-b-md flex-center" style="padding-top: 20vh;">
{{ $trans->title }}
</div>
@endsection

@section('content')

{!! $trans->description !!}

<img src="{{ $trans->get_thumbnail_url('medium_large') }}">

@endsection
