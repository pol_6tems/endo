<header>
    <div class="head-top">
        <div class="row">
            <div class="@if ($user)top-side-list @endif">
                @if ($user)
                    <div class="profile"><a href="{{ route('page.user', ['page' => 'user']) }}">@lang('User profile')</a></div>
                    <div class="logout"><a href="{{ route('logout') }}" onClick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('Logout')</a></div>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endif

                {{--<div class="lang-select">
                    <select class="select_box" onchange="window.location.href = this.value;">
                        @foreach ($_languages as $language)
                            <option value="{{ $language->url }}" {{ ($language->code == $language_code ? 'selected' : '') }}>@lang($language->name)</option>
                        @endforeach
                    </select>
                </div>--}}
            </div>
        </div>
    </div>
</header>