@extends('Front::layouts.html')

@section('body')
    @include('Front::partials.header')

    @yield('content')
@endsection