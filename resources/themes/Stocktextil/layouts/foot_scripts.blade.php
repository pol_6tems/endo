<script src="{{ asset($_front.'js/respond.js') }}"></script>

<script src="{{ mix('js/app.js', 'Themes/' . env('PROJECT_NAME')) }}"></script>

@yield('bottom_foot_scripts')