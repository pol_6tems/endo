<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('Front::layouts.head')
</head>

<body @if (route_name('login'))class="bg-img"@endif>
    @yield('body')

    @include('Front::layouts.foot_scripts')
</body>
</html>