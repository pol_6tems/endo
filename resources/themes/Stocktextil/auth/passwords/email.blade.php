@extends('layouts.app')

@section('header')
<h1>Reset Password</h1>
@endsection

@section('content')
<div class="col-md-8 col-md-offset-2">

	@if (session('status'))
		<div class="alert alert-success" role="alert">
			{{ session('status') }}
		</div>
	@endif

    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
		{{ csrf_field() }}

		<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
			<label for="email" class="col-md-4 control-label">E-Mail Address</label>

			<div class="col-md-6">
				<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

				@if ($errors->has('email'))
				<div class="alert alert-warning" role="alert">
					<span class="help-block">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
				</div>
				@endif
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				<button type="submit" class="btn btn-primary btn-raised">
					Send Password Reset Link
				</button>
			</div>
		</div>
	</form>
</div>
@endsection
