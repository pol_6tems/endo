@extends('Front::layouts.main')

@section('content')
	<section class="login-tab">
		<div class="row-login">
			<div class="login-sec">
				<!--<h1>Stock Textiles</h1>-->
				<div class="logo"><a href="{{ route('index') }}">{{ env('APP_NAME') }}</a></div>
				<h4>@lang('Login to access Stock Textile')</h4>

				<div id="login-details">
					<ul class="resp-tabs-list hor_1">
						<li>@lang("Already a user")</li>
						<li>@lang("Not registered")</li>
					</ul>
					<div class="resp-tabs-container hor_1">
						<!-- tab 1 -->
						<div>
							<form id="login-form" class="form-box login" method="POST" action="{{ route('login') }}">
								{{ csrf_field() }}
								<div class="frm-input">
									<div class="frm-input-ctrl">
										<input class="form-control @if ($errors->has('email') && !old('name'))with-error @endif" type="email" name="email" placeholder="@lang('Email')*" value="{{ old('email') }}" required>
										@if ($errors->has('email') && !old('name'))
											<div class="form-error">
												<span><strong>{{ $errors->first('email') }}</strong></span>
											</div>
										@endif
									</div>
									<div class="frm-input-ctrl">
										<input class="form-control @if ($errors->has('password') && !old('name'))with-error @endif" type="password" name="password" placeholder="@lang('Password')*" required>
										@if ($errors->has('password') && !old('name'))
											<div class="form-error">
												<span><strong>{{ $errors->first('password') }}</strong></span>
											</div>
										@endif
									</div>
									<input type="hidden" name="locale" value="en">
									{{--<div class="frm-input-ctrl dropdown">
										<select name="locale">
											@foreach ($_languages as $language)
												<option {{ ($language->code == $language_code ? 'selected' : '') }} value="{{$language->code}}">@lang($language->name)</option>
											@endforeach
										</select>
									</div>--}}
									<!--<button class="submit-btn">ENTRAR</button>-->
									<input type="submit" class="submit-btn" value="@lang('Enter')">
									<div class="chk-dt">
										<span>@lang('The fields marked with * are mandatory')</span>
										<a href="{{ route('password.request') }}"><span><strong>@lang('I forgot my password')</strong></span></a>
									</div>
								</div>
							</form>
						</div>
						<!-- tab 2 -->
						<div>
							<form id="login-reg-form" class="form-box reg-form" method="POST" action="{{ route('register') }}">
								{{ csrf_field() }}
								<div class="frm-input">
									<div class="frm-lft">
										<h2>@lang('User data')</h2>
										<div class="frm-input">
											<div class="frm-input-ctrl">
												<input class="form-control @if ($errors->has('name') && old('name'))with-error @endif" type="text" name="name" placeholder="@lang('Name')*" value="{{ old('name') }}" required>
												@if ($errors->has('name') && old('name'))
													<div class="form-error">
														<span><strong>{{ $errors->first('name') }}</strong></span>
													</div>
												@endif
											</div>
											<div class="frm-input-ctrl">
												<input class="form-control @if ($errors->has('lastname') && old('name'))with-error @endif" type="text" name="lastname" placeholder="@lang('Lastname')*" value="{{ old('lastname') }}">
												@if ($errors->has('lastname') && old('name'))
													<div class="form-error">
														<span><strong>{{ $errors->first('lastname') }}</strong></span>
													</div>
												@endif
											</div>
											<div class="frm-input-ctrl">
												<input class="form-control @if ($errors->has('phone_number') && old('name'))with-error @endif" type="text" name="phone_number" placeholder="@lang('Phone number')*" value="{{ old('phone_number') }}">
												@if ($errors->has('phone_number') && old('name'))
													<div class="form-error">
														<span><strong>{{ $errors->first('phone_number') }}</strong></span>
													</div>
												@endif
											</div>
											<div class="frm-input-ctrl">
												<input class="form-control @if ($errors->has('email') && old('name'))with-error @endif" type="email" name="email" placeholder="@lang('Email')*" value="{{ old('email') }}" required>
												@if ($errors->has('email') && old('name'))
													<div class="form-error">
														<span><strong>{{ $errors->first('email') }}</strong></span>
													</div>
												@endif
											</div>
											<div class="frm-input-ctrl">
												<input class="form-control @if ($errors->has('password') && old('name'))with-error @endif" type="password" name="password" placeholder="@lang('Password')*" required>
												@if ($errors->has('password') && old('name'))
													<div class="form-error">
														<span><strong>{{ $errors->first('password') }}</strong></span>
													</div>
												@endif
											</div>
											<div class="frm-input-ctrl">
												<input class="form-control" type="password" name="password_confirmation" placeholder="@lang('Confirm password')*" required>
											</div>
										</div>
									</div>
									<div class="frm-rgt">
										<h2>@lang('Company data')</h2>
										<div class="frm-input">
											<div class="frm-input-ctrl">
												<input class="form-control @if ($errors->has('company_name') && old('name'))with-error @endif" type="text" name="company_name" placeholder="@lang('Company name')*" value="{{ old('company_name') }}">
												@if ($errors->has('company_name') && old('name'))
													<div class="form-error">
														<span><strong>{{ $errors->first('company_name') }}</strong></span>
													</div>
												@endif
											</div>
											<div class="frm-input-ctrl">
												<input class="form-control @if ($errors->has('company_address') && old('name'))with-error @endif" type="text" name="company_address" placeholder="@lang('Address')*" value="{{ old('company_address') }}">
												@if ($errors->has('company_address') && old('name'))
													<div class="form-error">
														<span><strong>{{ $errors->first('company_address') }}</strong></span>
													</div>
												@endif
											</div>
											<div class="frm-input-ctrl">
												<input class="form-control @if ($errors->has('company_city') && old('name'))with-error @endif" type="text" name="company_city" placeholder="@lang('City')*" value="{{ old('company_city') }}">
												@if ($errors->has('company_city') && old('name'))
													<div class="form-error">
														<span><strong>{{ $errors->first('company_city') }}</strong></span>
													</div>
												@endif
											</div>
											<div class="frm-input-ctrl">
												<input class="form-control @if ($errors->has('company_country') && old('name'))with-error @endif" type="text" name="company_country" placeholder="@lang('Country')*" value="{{ old('company_country') }}">
												@if ($errors->has('company_country') && old('name'))
													<div class="form-error">
														<span><strong>{{ $errors->first('company_country') }}</strong></span>
													</div>
												@endif
											</div>
											<div class="frm-input-ctrl">
												<input class="form-control @if ($errors->has('company_nif') && old('name'))with-error @endif" type="text" name="company_nif" placeholder="NIF*" value="{{ old('company_nif') }}">
												@if ($errors->has('company_nif') && old('name'))
													<div class="form-error">
														<span><strong>{{ $errors->first('company_nif') }}</strong></span>
													</div>
												@endif
											</div>
											<input type="hidden" name="locale" value="en">
											{{--<div class="frm-input-ctrl dropdown">
												<select name="locale">
													@foreach ($_languages as $language)
														<option {{ ($language->code == $language_code ? 'selected' : '') }} value="{{$language->code}}">@lang($language->name)</option>
													@endforeach
												</select>
											</div>--}}
										</div>
									</div>

									<div class="chk-dt chk">
										<div class="frm-input checkbox">
											<input id="check1" type="checkbox" name="lopd">
											<label for="check1" class="lodp-checkbox"><span>@lang('I accept the Privacy Policy')*</span></label>
										</div>
										<div class="frm-input checkbox">
											<input id="check2" type="checkbox" name="promotional_info">
											<label for="check2"><span>@lang('I agree to receive promotional information')</span></label>
										</div>
										<!--<button class="submit-btn">ENTRAR</button>-->
										<input type="submit" class="submit-btn js-register-button" value="@lang('Enter')">
										<div class="chk-dt">
											<span>@lang('The fields marked with * are mandatory')</span>
										</div>
									</div>

								</div>
							</form>
						</div>

					</div>
				</div>

			</div>
		</div>
	</section>
@endsection
