@extends('Front::layouts.main')

@section('content')
    @php($user = auth()->user())
    <section>
        <div class="row">
            <div class="prod-list">
                <!--<h3>Stock Textiles</h3>-->
                <div class="logo"><a href="{{ route('index') }}">Stock Textiles</a></div>
                <div class="title-list">
                    <h1>@lang('User data')</h1>
                </div>
            </div>
            <div class="row-login">
                <div class="user-login">
                    <h1>@lang('Update your data')</h1>

                    <form class="form-box user" method="POST">
                        {{ csrf_field() }}

                        <div class="frm-input">
                            <div class="frm-input-ctrl">
                                <label>@lang('Name')</label>
                                <input class="form-control @if ($errors->has('name'))with-error @endif" type="text" name="name" value="{{ old('name') ?: $user->name }}">
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                                @if ($errors->has('name'))
                                    <div class="form-error">
                                        <span><strong>{{ $errors->first('name') }}</strong></span>
                                    </div>
                                @endif
                            </div>
                            <div class="frm-input-ctrl">
                                <label>@lang('Lastname')</label>
                                <input class="form-control @if ($errors->has('lastname'))with-error @endif" type="text" name="lastname" value="{{ old('lastname') ?: $user->lastname }}" required>
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                                @if ($errors->has('lastname'))
                                    <div class="form-error">
                                        <span><strong>{{ $errors->first('lastname') }}</strong></span>
                                    </div>
                                @endif
                            </div>
                            <div class="frm-input-ctrl">
                                <label>@lang('Phone number')</label>
                                <input class="form-control @if ($errors->has('phone_number'))with-error @endif" type="text" name="phone_number" value="{{ old('phone_number') ?: $user->get_field('phone_number') }}" required>
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                                @if ($errors->has('phone_number'))
                                    <div class="form-error">
                                        <span><strong>{{ $errors->first('phone_number') }}</strong></span>
                                    </div>
                                @endif
                            </div>
                            <div class="frm-input-ctrl">
                                <label>@lang('Email')</label>
                                <input class="form-control @if ($errors->has('email'))with-error @endif" type="text" name="email" value="{{ old('email') ?: $user->email }}" required>
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                                @if ($errors->has('email'))
                                    <div class="form-error">
                                        <span><strong>{{ $errors->first('email') }}</strong></span>
                                    </div>
                                @endif
                            </div>
                            <div class="frm-input-ctrl">
                                <label>@lang('Password')</label>
                                <input class="form-control @if ($errors->has('password'))with-error @endif" type="password" name="password" value="">
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                                @if ($errors->has('password'))
                                    <div class="form-error">
                                        <span><strong>{{ $errors->first('password') }}</strong></span>
                                    </div>
                                @endif
                            </div>
                            <div class="frm-input-ctrl">
                                <label>@lang('Confirm password')</label>
                                <input class="form-control" type="password" name="password_confirmation" value="">
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                            </div>
                            <div class="frm-input-ctrl">
                                <label>@lang('Company name')</label>
                                <input class="form-control @if ($errors->has('company_name'))with-error @endif" type="text" name="company_name" value="{{ old('company_name') ?: $user->get_field('company_name') }}" required>
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                                @if ($errors->has('company_name'))
                                    <div class="form-error">
                                        <span><strong>{{ $errors->first('company_name') }}</strong></span>
                                    </div>
                                @endif
                            </div>
                            <div class="frm-input-ctrl">
                                <label>@lang('Address')</label>
                                <input class="form-control @if ($errors->has('company_address'))with-error @endif" type="text" name="company_address" value="{{ old('company_address') ?:  $user->get_field('company_address') }}" required>
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                                @if ($errors->has('company_address'))
                                    <div class="form-error">
                                        <span><strong>{{ $errors->first('company_address') }}</strong></span>
                                    </div>
                                @endif
                            </div>
                            <div class="frm-input-ctrl">
                                <label>@lang('City')</label>
                                <input class="form-control @if ($errors->has('company_city'))with-error @endif" type="text" name="company_city" value="{{ old('company_city') ?: $user->get_field('company_city') }}" required>
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                                @if ($errors->has('company_city'))
                                    <div class="form-error">
                                        <span><strong>{{ $errors->first('company_city') }}</strong></span>
                                    </div>
                                @endif
                            </div>
                            <div class="frm-input-ctrl">
                                <label>@lang('Country')</label>
                                <input class="form-control @if ($errors->has('company_country'))with-error @endif" type="text" name="company_country" value="{{ old('company_country') ?: $user->get_field('company_country') }}" required>
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                                @if ($errors->has('company_country'))
                                    <div class="form-error">
                                        <span><strong>{{ $errors->first('company_country') }}</strong></span>
                                    </div>
                                @endif
                            </div>
                            <div class="frm-input-ctrl">
                                <label>NIF</label>
                                <input class="form-control @if ($errors->has('company_nif'))with-error @endif" type="text" name="company_nif" value="{{ old('company_nif') ?: $user->get_field('company_nif') }}" required>
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                                @if ($errors->has('company_nif'))
                                    <div class="form-error">
                                        <span><strong>{{ $errors->first('company_nif') }}</strong></span>
                                    </div>
                                @endif
                            </div>
                            <input type="hidden" name="locale" value="en">
                            {{--<div class="frm-input-ctrl dropdown">
                                <label>@lang('Language')</label>
                                <select name="locale">
                                    @foreach ($_languages as $language)
                                        <option {{ ($language->code == $language_code ? 'selected' : '') }} value="{{$language->code}}">@lang($language->name)</option>
                                    @endforeach
                                </select>
                                <img src="{{ asset('Themes/Stocktextil/images/edit-icon.svg') }}"/>
                            </div>--}}
                            <button class="submit-btn">@lang('Save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection