$(window).on('load', function() {
    // flexslider
    $('#prod-slider .flexslider').flexslider({
        animation:"fade",
        slideshowSpeed:5000,
        animationDuration: 3000,
        easing: "swing",
        directionNav: true,
        slideshow: false,
        controlNav: true,
        pausePlay: false
    });
});

$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        goToFirstSpeed :3000,
        loop:true,
        items:1,
        margin:1,
        autoplay:false,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        nav : true,
        dots : false
    });


    // set owl-carousel width equals to owl-wrapper width
    function owlWrapperWidth( selector ) {
        $(selector).each(function(){
            $(this).find('.owl-carousel').outerWidth( $(this).closest( selector ).innerWidth() );
        });
    }

    // trigger on start and resize
    owlWrapperWidth( '.owl-wrapper' );
    $('.list-view').click(function () {
        window.dispatchEvent(new Event('resize'))
    });
});


$(document).ready(function() {
    $('.ampliar').click(function() {
        if($('.amp').hasClass('fullscreen'))
        {
            $('html').css('overflow','inherit');
            $(".amp").removeClass('fullscreen');
            $('.flexslider').flexslider();
        }else{
            $('html').css('overflow','hidden');
            $(".amp").addClass('fullscreen');
            $('.flexslider').flexslider();
        }
    });
});