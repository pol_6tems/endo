// Input Box Placeholder
function fnremove(arg,val)	{
	if (arg.value == '') {arg.value = val}
}	
function fnshow(arg,val)	{
	if (arg.value == val) {arg.value = ''}
}

$(document).ready(function () {	
	
	$(".select_box").selectbox();
    
	$(".fancybox").fancybox();
	$(".fancybox-ampliar").fancybox();
	
 jsUpdateSize();
 
});

/* Grid & List View */
$('.list-view-btn').on("click", function () {
	$(".list-view-btn .grid-view").toggleClass("active");
	$(".list-view-btn .list-view").toggleClass("active");
 });
$('.list-view-btn .list-view').on("click", function () { 
	$('.prod-det-lst').toggleClass("list");
});

$('.list-view-btn .grid-view').on("click", function () {
	$('.prod-det-lst').removeClass("list");
});

function jsUpdateSize()
{
    var width = $(window).width();
    var height = $(window).height();
    if(width<=767)
    {
        $('.banner-home .flexslider .slides li').each(function()
        {
            var img = $('img', this).attr('src');
            $(this).css('background-image','url(' + img + ')');
            $(".banner-home").css('height',415);
            $(this).css('height',415);
        });
    }
    else{
        $('.banner-home .flexslider .slides li').each(function()
        {
            $(this).css('background-image','');
            $(".banner-home").css('height','auto');
            $(this).css('height','auto');
        });

    }

    if(width<=767)
    {
        $('.banner-home1 .flexslider .slides li').each(function()
        {
            var img = $('img', this).attr('src');
            $(this).css('background-image','url(' + img + ')');
            $(".banner-home1").css('height',530);
            $(this).css('height',530);
        });
    }
    else{
        $('.banner-home1 .flexslider .slides li').each(function()
        {
            $(this).css('background-image','');
            $(".banner-home1").css('height','auto');
            $(this).css('height','auto');
        });

    }
    if(width<=480)
    {
        $('.resp-banner .bann-img').each(function()
        {
            var img = $('img', this).attr('src');
            $(this).css('background-image','url(' + img + ')');
            $(".bann-img").css('height',200);
            $(this).css('height',200);
        });
    }
    else{
        $('.resp-banner .bann-img').each(function()
        {
            $(this).css('background-image','');
            $(".bann-img").css('height','auto');
            $(this).css('height','auto');
        });

    }
    if(width<=767)
    {
        $('.necesit .necesit-img').each(function()
        {
            var img = $('img', this).attr('src');
            $(this).css('background-image','url(' + img + ')');
            $(".necesit-img").css('height',260);
            $(this).css('height',260);
        });
    }
    else{
        $('.necesit .necesit-img').each(function()
        {
            $(this).css('background-image','');
            $(".necesit-img").css('height','auto');
            $(this).css('height','auto');
        });

    }
}