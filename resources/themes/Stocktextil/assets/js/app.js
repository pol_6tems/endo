
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

/*require('./plugins/bootstrap');*/

require('./plugins/html5');
window.$ = window.jQuery = require('jquery');
require('./plugins/jquery.selectbox-0.2');
require('./plugins/jquery.easyResponsiveTabs');
require('./plugins/jquery.fancybox');
require('./plugins/jquery.flexslider');
require('owl.carousel');
window.toastr = require('toastr');

require('./login');
require('./home');
require('./common');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */