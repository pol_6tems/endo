if (typeof $('#login-details') !== 'undefined') {
    $(window).on('load', function () {
        $('#login-details').easyResponsiveTabs({
            type: 'default',
            width: 'auto',
            fit: true,
            tabidentify: 'hor_1'
        });

        $(".frm-input-ctrl.dropdown select").selectbox();

        if ($('input[name="phone_number"]').val() !== '') {
            $('h2[aria-controls="hor_1_tab_item-1"]').click();
        }
    });
}

$('#login-reg-form').on('submit', function (e) {
    if (!$('#check1').is(':checked')) {
        e.preventDefault();
        $('.lodp-checkbox').addClass('lopd-red');
    }
});