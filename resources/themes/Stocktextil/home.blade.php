@extends('Front::layouts.main')

@section('content')
	<section>
		<div class="row">
			<div class="prod-list">
				<h3>Stock Textiles</h3>
				{{--<div class="logo"><a href="{{ route('index') }}">Stock Textiles</a></div>--}}

				<div class="title-list">
					<h1>@lang('Stocks list')</h1>
					<div class="list-view-btn">
						<div class="grid-view active"><a href="javascript:void(0);"><img src="{{ asset('Themes/Stocktextil/images/grid-view-icon.svg') }}"/></a></div>
						<div class="list-view"><a href="javascript:void(0);"><img src="{{ asset('Themes/Stocktextil/images/list-view-icon.svg') }}"/></a></div>
					</div>
				</div>

				<div class="prod-det-lst">
					<ul>
						@foreach($products as $key => $product)
							@php($trans = $product->translate(app()->getLocale()))

							<li>
								<div class="prod-img-lft">
									<div class="owl-carousel">
										@php($images = $product->get_field('images'))
										@if ($images)
											@foreach($images as $image)
												<div class="item">
													<img src="{{ $image->get_thumbnail_url('medium') }}" style="margin-right: 2px" />
												</div>
											@endforeach
										@endif
									</div>
								</div>

								<a href="#popup{{ $key }}" class="fancybox">
									<div class="prod-img-rgt">
										<h2>{{ $trans->title }}</h2>
										<p class="sml-para">{{ cut_text($trans->description, 87, '') }}</p>
										<p class="lrg-para">{{ cut_text($trans->description, 143, '…') }}</p>
										<span>{{--<strong>Laoreet dolore</strong> magna aliquam erat volutpat…--}}</span>
										<div class="date">{{ $product->get_field('date') }}</div>
									</div>
								</a>

								<div id="popup{{ $key }}" class="popup">
									<div class="pop-lft">
										<div id="prod-slider" class="amp">
											<div class="flexslider">
												@if ($images)
													<ul class="slides">
														@foreach($images as $image)
															<li><img src="{{ $image->get_thumbnail_url('medium_large') }}" /></li>
														@endforeach
													</ul>
												@endif
											</div>
											<div class="ampliar"></div>
										</div>
										{{--<div class="video-img">
											<img src="images/video-preview.jpg" />
										</div>--}}
									</div>
									<div class="pop-rgt">
										<h1>{{ $trans->title }}</h1>
										<span><strong>@lang('Material'):</strong> {{ $product->get_field('material') }}</span>
										{!! $trans->description !!}

										<h2>@lang('Ordering')</h2>
										<div class="real-lst">
											<ul>
												<li class="ph-ico"><strong>@lang('Call')</strong> <a href="tel:{{ $product->get_field('contact_phone') }}">{{ $product->get_field('contact_phone') }}</a></li>
												<li class="whatapp-ico"><strong>Whatsapp</strong></li>
												<li class="mail-ico"><strong>@lang('Or fill in the following form')</strong></li>
											</ul>
										</div>

										<form method="POST">
											{{ csrf_field() }}
											<input type="hidden" name="product" value="{{ $product->id }}">
											<textarea class="form-control" name="message" placeholder="{{ ucfirst(auth()->user()->name) }}, @lang('write here your order')" required></textarea>

											<div class="submit-pop">
												<!--<button class="submit-btn popup">ENVIAR</button>-->
												<input type="submit" class="submit-btn" value="ENVIAR">
											</div>
										</form>
									</div>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('bottom_foot_scripts')
	<script>
		$(document).ready(function () {
			@if ($message = session()->get('message'))
				toastr.success('{!! $message !!}');
			@endif

			@if ($error = session()->get('error'))
				toastr.error('{!! $error !!}');
			@endif
		});
	</script>
@endsection
