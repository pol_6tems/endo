@extends('Front::layouts.app')

@section("header")
@endsection

@section("content")
<section class="area-container">
    <div class="area-content">
        <h2>Plataforma de Targetes PASS</h2>
        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            @csrf
            <img src="{{ asset($_front."images/logo-login.png") }}" alt="formacio-i-treballll">
            <input type="text" name="email" class="frm-ctrl" placeholder="" value="email" onBlur="if (this.value == ''){this.value = 'email'; }" onFocus="if (this.value == 'email') {this.value = ''; }">
            <input type="password" name="password" class="frm-ctrl" placeholder="" value="Password" onBlur="if (this.value == ''){this.value = 'Password'; }" onFocus="if (this.value == 'Password') {this.value = ''; }">
                        
            <button type="submit" class="">ACCEDIR</button>
            <div class="clear"></div>
            <p class="mt25"><a href="{{ route('password.request') }}">Has oblidat el password?</a></p>
        </form>
    </div>
</section>

<footer class="log-foot">
    <div class="row">
        <h4><span>Fundació Formació i Treball 2017</span>  - Carrer Ramon Llull, 430-438 - Sant Adrià de Besòs (Barcelona)</h4>
        <ul>
            <li><a class="tel-ico" href="tel:(+34) 93 303 41 00">(+34) 93 303 41 00</a></li>
            <li><a class="mail-ico" href="mailto:fit@formacioitreball.org">fit@formacioitreball.org</a></li>
        </ul>
        <h3>Fundació Formació i Treball 2017</h3>
    </div>
</footer>
@endsection