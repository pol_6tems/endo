@extends('Front::layouts.app')

@section("header")
@endsection

@section("content")
<section class="area-container">
    <div class="area-content">
        <h2>@lang('Recuperar Contraseña')</h2>
        <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">

            <img src="{{ asset($_front."images/logo-login.png") }}" alt="formacio-i-treball">
            <input id="email" type="email" class="frm-ctrl" name="email" value="{{ $email or old('email') }}" required placeholder="@lang('E-mail')">
            <input id="password" type="password" class="frm-ctrl" name="password" required placeholder="@lang('Nueva Contraseña')">
            <input id="password-confirm" type="password" class="frm-ctrl" name="password_confirmation" placeholder="@lang('Confirmar Nueva Contraseña')">
            
            <button type="submit" class="">@lang('ACCEDIR')</button>
        </form>
    </div>
</section>

<footer class="log-foot">
    <div class="row">
        <h4><span>Fundació Formació i Treball 2017</span>  - Carrer Ramon Llull, 430-438 - Sant Adrià de Besòs (Barcelona)</h4>
        <ul>
            <li><a class="tel-ico" href="tel:(+34) 93 303 41 00">(+34) 93 303 41 00</a></li>
            <li><a class="mail-ico" href="mailto:fit@formacioitreball.org">fit@formacioitreball.org</a></li>
        </ul>
        <h3>Fundació Formació i Treball 2017</h3>
    </div>
</footer>
@endsection