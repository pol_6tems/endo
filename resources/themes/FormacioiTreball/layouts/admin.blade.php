<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <link rel="icon" type="image/x-icon" href="images/favicon.ico">
        <title>Formació i Treball</title>
        <!--CSS Start-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{ mix('admin/css/app.css', $_front) }}" rel="stylesheet">
    </head>
    <body>
        <div class="hole_div"> 
            <div id="page">  
                @yield('header', View::make('Front::admin.partials.header'))
                @yield('content')
                @includeIf("Front::admin.partials.footer")
            </div>
            @includeIf("Front::admin.partials.mobile")
        </div>
        <div id="new"></div>
        <script src="{{ mix('admin/js/app.js', $_front) }}"></script>
        @yield("scripts")
    </body>
</html>
