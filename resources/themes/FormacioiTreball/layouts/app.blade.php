<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="icon" type="image/x-icon" href="images/favicon.ico">
        <title>Formació i Treball | Calculadora</title>
        <!--CSS Start-->
        <link href="{{ mix('css/app.css', $_front) }}" rel="stylesheet">
    </head>
    <body>
        <div id="page">
            @yield('header', View::make('Front::partials.header'))
            @yield('content')
            @includeIf('Front::partials.footer')
        </div>
        <script src="{{ mix('js/app.js', $_front) }}"></script>
    </body>
</html>