<header>
    <!--Header Starts-->
    <div class="top-header">
        <div class="row">
            <div class="inner-top">
                <div class="logo"><a href="index.html"><img src="{{ asset($_front.'images/formacio-i-treball-logo-2.jpg') }}" /></a>
                </div>
                <div class="mob-logo"><a href="index.html"><img
                            src="{{ asset($_front.'images/formacio-i-treball-mob-logo.jpg') }}" /></a></div>
                <h1>Calculadora de productes Targeta Pass</h1>
                <div class="lang-select">
                    <select class="select_box">
                        <option>Català</option>
                        <option>English</option>
                        <option>Latin</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</header>