<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Formació i Treball</title>
        <!--CSS Start-->
        @includeIf('Front::pdfs.pass-styles')
    </head>
<body>
    <div id="page">
        <!--Header Starts-->
        <table style="table-layout: fixed;border-collapse: separate; border-spacing: 40px; width: 780px;margin-left: auto;margin-right: auto;">
            <tr>
                <td style="width: 324px; vertical-align: baseline;">
                    <table style="table-layout: fixed;border-collapse: collapse;margin: 0;padding: 0; margin-bottom: 40px; width: 324px">
                        <tr>
                            <td>
                                <h2 style="font-size:18px; line-height:25px; font-family: 'public_sansbold'; color:#282828;">{{ ucfirst($now->format('l')) }}, <span style="font-family: 'public_sansitalic';">{{ $now->format('d/m/Y') }}</span></h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2 style="font-size:18px; line-height:25px; font-family: 'public_sansbold'; color:#282828;">{{ $now->format('H:i') }} h</h2>
                            </td>
                        </tr>
                    </table>
                    <table style="table-layout: fixed;border-collapse: separate;margin: 0;padding: 0; border-spacing: 10px;">
                        <tr>
                            <td>
                                <a href="index.html">
                                    @if ($html)
                                        <img style="width: 218px; height: 128px;" src="{{ asset('Themes/FormacioiTreball/images/formacio-i-treball-logo.jpg') }}"/>
                                    @else
                                        <img style="width: 218px; height: 128px;" src="{{ public_path('Themes/FormacioiTreball/images/formacio-i-treball-logo.jpg') }}"/>
                                    @endif
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 324px; margin-top: 42px;" cellspacing="0" cellpadding="0">
                                    @if ($importe = $peticio->get_field('importe'))
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td bgcolor="#f3f3f3" style="width:250px; text-align:center; padding:30px 0; line-height:25px; border-radius:50px; display: inline-block;" align="center">
                                                        <a href="javascript:void(0)" target="_blank" style="font-size:32px; font-family: 'public_sansbold'; color:#282828; text-decoration: none;">{{ $importe }} &euro;</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    @endif
                                    @if ($aportacion = $peticio->get_field('aportacion'))
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td style="width:250px; text-align:center; padding:30px 0; line-height:25px; border-radius:50px; display: inline-block;" align="center">
                                                        <a href="javascript:void(0)" target="_blank" style="font-size:20px; font-family: 'public_sansbold'; color:#282828; text-decoration: none;">@Lang("Aporte: :aportacion", ["aportacion" => round($importe * $aportacion/100, 2)]) &euro;</a>
                                                        {{-- <small style="color:#353535; font-weight: 400;">({{ $aportacion }}&#37;)</small> --}}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    @endif
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 276px">
                    <table style="table-layout: fixed;border-collapse: collapse;margin: 0;padding: 0;width: 274px;">
                        <tr>
                            <td>
                                <img style="width: 274px; height: 274px;" src="data:image/png;base64,{{ $qrcode->generate() }}" />
                            </td>    
                        </tr>
                        @if ($caducidad = $peticio->get_field("data-caducidad"))
                        <tr>
                            <td>
                                <h5 style="font-size:16px; line-height:25px; font-family: 'public_sansbold'; text-align: right; color:#282828; padding-top:24px;">@Lang("Fecha de Caducidad - :fecha", ["fecha" => $caducidad])</h5>
                            </td>
                        </tr>
                        @endif
                        <tr>
                            <td style="text-align: right;">
                                <span>@Lang("Nombre Beneficiario - ")</span><span style="font-family: 'public_sansitalic';width:100%;">@Lang(":nombre", ["nombre" => $peticio->get_field("nombre-usuario")])</span>
                            </td>    
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="table-layout: fixed; border-collapse: collapse; width: 660px; margin-top: 40px; margin-bottom: 20px; margin-left: auto; margin-right: auto;">
            <tr>
                <td style="text-align: center;">
                    <img style="width: 530px; height: 103px;" src="data:image/png;base64,{{ $barcode->generate() }}" />
                </td>
            </tr>
        </table>
        <table style="table-layout: fixed; border-collapse: collapse; width: 660px; margin-left: auto; margin-right: auto;">
            <tr>
                <td width="600"><h1 style="width: 600px;font-size:28px; line-height:25px; font-family: 'public_sansbold'; color:#282828;">@Lang('Botigues Formació i Treball')</h1></td>
            </tr>
        </table>
        <table style="table-layout: fixed; border-collapse: collapse; width: 660px; margin-top: 30px; margin-left: auto; margin-right: auto;">
            @foreach($tiendas as $key => $tienda)
            <tr>
                <td style="width:600px; padding: 15px;{{ ($key % 2 == 0) ? 'background:#f3f3f3' : 'background:#ffffff' }}; ">
                    <table style="table-layout: fixed;border-collapse: collapse;margin: 0;width: 600px;">
                        <tr>
                            <td><h3 style="font-size:18px; line-height:27px; font-family: 'public_sanssemibold'; color:#282828;">{{ $tienda->get_field('direccion') }}</h3></td>
                        </tr>
                        <tr>
                            <td>
                                <h4 style="font-size:18px; line-height:27px; font-family: 'public_sanssemibold'; color:#282828;">@Lang('Horario'): <span style="font-family: 'public_sansregular';">{{ $tienda->get_field('horario') }}</span></h4>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</body>

</html>