<style>
@charset "utf-8";
/* CSS Document */

@font-face {
    font-family: 'public_sansextralight';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralight-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralight-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralight-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralight-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralight-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralight-webfont.svg') }}#public_sansextralight") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansextralight_italic';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralightitalic-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralightitalic-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralightitalic-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralightitalic-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralightitalic-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extralightitalic-webfont.svg') }}#public_sansextralight_italic") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansitalic';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-italic-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-italic-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-italic-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-italic-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-italic-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-italic-webfont.svg') }}#public_sansitalic") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sanslight';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-light-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-light-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-light-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-light-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-light-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-light-webfont.svg') }}#public_sanslight") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansextrabold_italic';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabolditalic-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabolditalic-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabolditalic-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabolditalic-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabolditalic-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabolditalic-webfont.svg') }}#public_sansextrabold_italic") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansmedium';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-medium-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-medium-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-medium-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-medium-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-medium-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-medium-webfont.svg') }}#public_sansmedium") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansmedium_italic';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-mediumitalic-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-mediumitalic-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-mediumitalic-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-mediumitalic-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-mediumitalic-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-mediumitalic-webfont.svg') }}#public_sansmedium_italic") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansregular';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-regular-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-regular-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-regular-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-regular-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-regular-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-regular-webfont.svg') }}#public_sansregular") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sanssemibold';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibold-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibold-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibold-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibold-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibold-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibold-webfont.svg') }}#public_sanssemibold") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sanslight_italic';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-lightitalic-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-lightitalic-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-lightitalic-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-lightitalic-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-lightitalic-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-lightitalic-webfont.svg') }}#public_sanslight_italic") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sanssemibold_italic';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibolditalic-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibolditalic-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibolditalic-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibolditalic-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibolditalic-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-semibolditalic-webfont.svg') }}#public_sanssemibold_italic") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansthin';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thin-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thin-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thin-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thin-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thin-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thin-webfont.svg') }}#public_sansthin") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansthin_italic';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thinitalic-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thinitalic-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thinitalic-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thinitalic-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thinitalic-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-thinitalic-webfont.svg') }}#public_sansthin_italic") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansblack_italic';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-blackitalic-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-blackitalic-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-blackitalic-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-blackitalic-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-blackitalic-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-blackitalic-webfont.svg') }}#public_sansblack_italic") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansbold';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bold-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bold-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bold-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bold-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bold-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bold-webfont.svg') }}#public_sansbold") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansbold_italic';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bolditalic-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bolditalic-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bolditalic-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bolditalic-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bolditalic-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-bolditalic-webfont.svg') }}#public_sansbold_italic") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansextrabold';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabold-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabold-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabold-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabold-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabold-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-extrabold-webfont.svg') }}#public_sansextrabold") format('svg');
    font-weight: normal;
    font-style: normal;

}
@font-face {
    font-family: 'public_sansblack';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-black-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-black-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-black-webfont.woff2') }}") format('woff2'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-black-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-black-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/publicsans-black-webfont.svg') }}#public_sansblack") format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'robotomedium';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/roboto-medium-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/roboto-medium-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/roboto-medium-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/roboto-medium-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/roboto-medium-webfont.svg') }}#robotomedium") format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'robotobold';
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/roboto-bold-webfont.eot') }}");
    src: url("{{ public_path('Themes/FormacioiTreball/fonts/roboto-bold-webfont.eot') }}#iefix") format('embedded-opentype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/roboto-bold-webfont.woff') }}") format('woff'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/roboto-bold-webfont.ttf') }}") format('truetype'),
         url("{{ public_path('Themes/FormacioiTreball/fonts/roboto-bold-webfont.svg') }}#robotobold") format('svg');
    font-weight: normal;
    font-style: normal;
}


/* CSS Document */

body { padding:0px; margin:0px; background:#ffffff; font-size:16px; color:#282828; line-height:20px; font-family: 'public_sansregular';}
article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section, audio, video, source { display: block; margin: 0; padding: 0; }
a, img { border: medium none; }
a { color: #282828; text-decoration: none; outline:none; }
div { margin: 0; padding: 0; }
p { font-size:16px; line-height:20px; padding:0px; margin:0px; color:#414141;} 
h1, h2, h3, h4, h5, h6 { font-weight:normal; padding:0px; margin:0px;  line-height: 30px; }
strong { font-family: 'public_sansbold';}
/*a:hover { color: #979796; }*/
ul, li { list-style:none;}
a:focus, :focus{ outline:none; }
ul,ol { list-style-type:none; padding:0px; margin:0px; }
.clearfix:after { content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0; } 
.clearfix { display: inline-block; }
html[xmlns] .clearfix { display: block; } 

* html .clearfix { height: 1%; }
* { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; }
*:before, *:after { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; }

.clear { clear: both; line-height: 1px; padding:0px; height:0px; font-size:1px; }
.anim { transition:all 1s ease; -moz-transition:all 1s ease; -webkit-transition:all 1s ease; -o-transition:all 1s ease; }
.anim2 { transition:all .5s ease; -moz-transition:all .5s ease; -webkit-transition:all .5s ease; -o-transition:all .5s ease; }
.row { width:1170px; margin:auto; padding:0px;}
.main-row { width:768px; margin:auto; padding:0px;}

/* header css */
header {width:100%; z-index: 999;}
.is-sticky header { box-shadow:1px 2px 3px #ccc;}
.top-section { width:100%;}
.top-lft { width: 50%;}
.top-lft h2 { font-size:18px; line-height:25px; font-family: 'public_sansbold'; color:#282828;}
.top-lft span {font-family: 'public_sansitalic';}
.top-lft img { margin-top:40px; width: auto;}
.top-rgt { width: 50%;}
.top-rgt img { width:auto;}
.top-btm { width: 100%; padding-top:42px;}
.top-btm img { width:auto;}
.dollar-btn { width:100%; margin-top:42px;}
.dollar-btn a { width:250px; text-align:center; background:#f3f3f3; padding:35px 0; font-size:32px; line-height:25px; border-radius:50px; font-family: 'public_sansbold'; color:#282828;}
.top-rgt h3 { font-size:18px; line-height:25px; font-family: 'public_sansbold'; text-align: right; color:#282828; padding-top:24px;}
.top-rgt span {font-family: 'public_sansitalic';width:100%;}

.cont-section {width:100%; padding-top:45px;}
.cont-section h1 { width:100%; font-size:34px; line-height:25px; font-family: 'public_sansbold'; color:#282828; padding-bottom:40px;}
.cont-section ul { width:100%;}
table tr.botigues { width:100%; background:#f3f3f3; padding: 15px 15px 15px 53px; position:relative;}
table tr.botigues::before { position:absolute; content:""; background:#282828; width:5px; height:5px; top:25px; left:15px; border-radius:3px;}
table tr.botigues:nth-child(2n+2) { background:#ffffff;}
.cont-section h3 { width:100%; font-size:18px; line-height:27px; font-family: 'public_sanssemibold'; color:#282828;}
.cont-section h4 { width:100%; font-size:18px; line-height:27px; font-family: 'public_sanssemibold'; color:#282828; padding-bottom:5px;}
.cont-section span { font-family: 'public_sansregular';}
</style>