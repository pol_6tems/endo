<div class="table-pad">
    <table class="tab-resp" width="100%" cellspacing="0" cellpadding="0" border="0">
        <thead>
            <tr>
                <th width="14%">@lang('FECHA/HORA')</th>
                <th width="10%">@lang('EXPEDIENTE')</th>
                <th width="20%">@lang('NOMBRE I APELLIDO')</th>
                <th width="10%">@lang('DNI')</th>
                <th width="20%">@lang('EMAIL')</th>
                <th width="13%">@lang('TELÉFONO')</th>
                <th width="6%">@lang('IMPORTE')</th>
                <th width="7%">@lang('&nbsp;')</th>
            </tr>
        </thead>
        <tbody id="prodTable">
            @foreach($peticiones as $peticio)
            <tr class="item category-1" data-nombre="{{ $peticio->get_field('nombre-usuario') }}" data-email="{{ $peticio->get_field('e-mail-usuario') }}">
                <td>{{ $peticio->created_at->format("d/m/Y · H:i") }}</td>
                <td>{{ $peticio->get_field('numero-expediente') }}</td>
                <td>{{ $peticio->get_field('nombre-usuario') }}</td>
                <td>{{ $peticio->get_field('dni-nie-passaporte') }}</td>
                <td><a href="mailto:{{ $peticio->get_field('e-mail-usuario') }}">{{ $peticio->get_field('e-mail-usuario') }}</a></td>
                <td>{{ $peticio->get_field('telefono') }}</td>
                <td>{{ $peticio->get_field('importe') }} &euro;</td>
                <td><a class="reenviar" href="{{ route("reenviar", $peticio->id) }}">@lang('Reenviar')</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{ $peticiones->appends($_GET)->links('Front::pagination.bootstrap-4') }}