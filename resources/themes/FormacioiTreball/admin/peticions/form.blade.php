<ul>
    <li>
        <label>@Lang("Expediente")</label>
        <input type="text" name="expediente" class="frm-ctrl" placeholder="@lang('Número de expediente')" value="{{ isset($peticion) ? $peticion->get_field("numero-expediente") : old("expediente") }}">
    </li>
    <li>
        <label>@Lang("Nombre")</label>
        <input type="text" name="nombre" class="frm-ctrl" placeholder="@lang('Nombre y Apellido')" value="{{ isset($peticion) ? $peticion->get_field("nombre-usuario") : old("nombre") }}">
    </li>
    <li>
        <label>@Lang("Edad")</label>
        <input type="text" name="edad" class="frm-ctrl" placeholder="@lang('Edad')" value="{{ isset($peticion) ? $peticion->get_field("edad") : old("edad") }}">
    </li>
    <li>
        <label>@Lang("Género")</label>
        <input type="text" name="genero" class="frm-ctrl" placeholder="@lang('Género')" value="{{ isset($peticion) ? $peticion->get_field("genero") : old("genero") }}">
    </li>
    <li>
        <label>@Lang("Familia")</label>
        <input type="text" name="familia" class="frm-ctrl" placeholder="@lang('Família')" value="{{ isset($peticion) ? $peticion->get_field("familia") : old("familia") }}">
    </li>
    <li>
        <label>@Lang("DNI / NIE / Passaporte")</label>
        <input type="text" name="dni" class="frm-ctrl" placeholder="@lang('DNI')" value="{{ isset($peticion) ? $peticion->get_field("dni-nie-passaporte") : old("dni") }}">
    </li>
    <li>
        <label>@Lang("Email")</label>
        <input type="text" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="frm-ctrl" placeholder="@lang('Email')" value="{{ isset($peticion) ? $peticion->get_field("e-mail-usuario") : old("email") }}">
    <li>
        <label>@lang('Fecha de Caducidad')</label>
        <input type="date" name="data-caducidad" class="frm-ctrl" value="{{ isset($peticion) ? $peticion->get_field("data-caducidad") : old("data-caducidad") }}">
    </li>
    <li>
        <label>@Lang("Teléfono")</label>
        <label class="prefix" for="telefono">+34</label>
        <input id="telefono" style="padding-left: 70px;" type="text" name="telefono" class="frm-ctrl" placeholder="@lang('Teléfono')" value="{{ isset($peticion) ? $peticion->get_field("telefono") : old("telefono") }}">
    </li>
    <li class="inline" style="display: flex;">
        <div>
            <label>@Lang("Importe FiT")</label>
            <input id="importe" type="number" name="importe" min="0" class="frm-ctrl impt-form" placeholder="Importe" value="{{ isset($peticion) ? $peticion->get_field("importe") : old("importe") ?? 0 }}">
            <label class="sufix">&euro;</label>
        </div>
        <div>
            <label>@Lang("Aportación")</label>
            <input id="aporte" type="number" name="aporte" min="0" max="100" class="frm-ctrl impt-form" style="margin-left:10px;width: 105px;" placeholder="Aporte" value="{{ isset($peticion) ? $peticion->get_field("aportacion") : old("aporte") ?? 0 }}">
            <label class="sufix">&#37;</label>
        </div>
        <div>
            <label>@Lang("Aporte")</label>
            <input id="aportacion" type="text" class="frm-ctrl impt-form" style="margin-left:10px;width: 120px" value="" readonly>
        </div>
    </li>
    <li>
        <button type="submit" class="">@lang('GENERAR PETICIÓN')</button>
    </li>
</ul>