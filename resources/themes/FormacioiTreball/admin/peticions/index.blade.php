@extends('Front::layouts.admin')

@section("content")
<section class="inner-cont alta-p mt35">
    <div class="row1">
        <div class="filter-list">
            <form action="{{ route("admin.peticion.index") }}" style="display: flex;">
                <input class="frm-ctrl" id="filter" name="query" type="text" value="{{ request()->query('query') }}" placeholder="@lang('Filtro')">
                <button type="submit"><i class="material-icons">search</i></button>
            </form>
        </div>

        <div class="table-list">
            <div class="table-pad">
                <table class="tab-resp table-peticions" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <thead>
                        <tr>
                            <th width="11%">@lang('FECHA/HORA')</th>
                            <th width="10%">@lang('EXPEDIENTE')</th>
                            <th width="20%">@lang('NOMBRE I APELLIDO')</th>
                            <th width="10%">@lang('DNI')</th>
                            <th width="20%">@lang('EMAIL')</th>
                            {{-- <th width="13%">@lang('TELÉFONO')</th> --}}
                            <th width="6%">@lang('IMPORTE')</th>
                            <th width="13%">@lang('TÉCNICO')</th>
                            <th width="20%">@lang('&nbsp;')</th>
                        </tr>
                    </thead>
                    <tbody id="prodTable">
                        @forelse($peticiones as $peticio)
                        <tr class="item" data-nombre="{{ $peticio->get_field('nombre-usuario') }}" data-email="{{ $peticio->get_field('e-mail-usuario') }}">
                            <td>{{ $peticio->created_at->format("d/m/Y · H:i") }}</td>
                            <td>{{ $peticio->get_field('numero-expediente') }}</td>
                            <td>{{ $peticio->get_field('nombre-usuario') }}</td>
                            <td>{{ $peticio->get_field('dni-nie-passaporte') }}</td>
                            <td><a href="mailto:{{ $peticio->get_field('e-mail-usuario') }}">{{ $peticio->get_field('e-mail-usuario') }}</a></td>
                            {{-- <td>{{ $peticio->get_field('telefono') }}</td> --}}
                            <td>{{ round($peticio->get_field('importe'), 2) }} &euro;</td>
                            <td class="avatar">
                                @if ($peticio->author)
                                    <img data-tippy-content="{{ $peticio->author->fullname() }}" src="{{ asset('storage/avatars/' . $peticio->author->avatar) }}" />
                                @endif
                            </td>
                            
                            <td>
                                <div class="accions">
                                    <a class="accio reenviar" data-tippy-content="@Lang("Reenviar mensaje")" href="{{ route("reenviar", $peticio->id) }}"><i class="material-icons">send</i></a>
                                    @if (auth()->user()->rol->level >= 50)
                                    <a class="accio editar" data-tippy-content="@Lang("Editar Petición")" href="{{ route("admin.peticion.edit", $peticio) }}"><i class="material-icons">create</i></a>
                                    <form action="{{ route("admin.peticion.destroy", $peticio) }}" method="POST">
                                        @csrf
                                        @method("DELETE")
                                        <a class="accio destroy" data-tippy-content="@Lang("Eliminar Petición")" href="javascript:void(0)"><i class="material-icons">clear</i></a>
                                    </form>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10" style="text-align: center;">@lang('No s\'ha trobat cap resultat')</td>
                        </tr>
                        @endforelse
                </tbody>
                </table>
            </div>	
            {{ $peticiones->appends($_GET)->links('Front::pagination.bootstrap-4') }}
        </div>
    </div>	
</section>
@endsection

@section("scripts")
    <script>
        $(".destroy").on("click", function(e) {
            var target = $(this);
            Swal.fire({
                title: '¿Estas seguro?',
                text: "Vas a eliminar la peticion",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, quiero borrarla'
            }).then((result) => {
                if (result.value) {
                    target.closest("form").submit();
                }
            })
        });

        $(".reenviar").click(function(e) {
            var nombre = $(this).closest(".item").data("nombre");
            var email = $(this).closest(".item").data("email");

            if (! confirm(`Quieres reenviar la petición a ${nombre} (${email})?`) ) {
                e.preventDefault();
            }
        });
    </script>
	@if (session('status'))
	<script>
    $.notify({
        title: '{{ session("title") }}',
        message: '{!! session("msg") !!}'
    },{
        placement: {
            from: "bottom"
        },
        type: 'minimalist',
        delay: 5000,
        icon_type: 'image',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} {{ session('status') }}" role="alert">' +
            '<span data-notify="title">{1}</span>' +
            '<span data-notify="message">{2}</span>' +
        '</div>'
    });
	</script>
	@endif
@endsection