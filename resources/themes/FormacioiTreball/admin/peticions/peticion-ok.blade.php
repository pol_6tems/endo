@extends('Front::layouts.admin')

@section("content")
<section class="inner-cont alta-p mt55">
    <div class="row1">
        <div class="form-list">
      		<h2>@lang('Su petición ha sido enviada!')</h2>
            <a href="{{ route("admin.peticion.index") }}" class="">@lang('VER PETICIONES ANTERIORES')</button>
        </div>
    </div>
</section>
@endsection

@section("scripts")
    @if ($errors->any())
		<script>
            $.notify({
                title: '{{ $error }}',
                message: '',
            },{
                placement: {
                    from: "bottom"
                },
                type: 'minimalist',
                delay: 5000,
                timer: 1000,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                allow_dismiss: true,
                icon_type: 'image',
                template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} error" role="alert">' +
                    '<span data-notify="title">{1}</span>' +
                    '<span data-notify="message">{2}</span>' +
                '</div>'
            });
		</script>
	@endif
@endsection