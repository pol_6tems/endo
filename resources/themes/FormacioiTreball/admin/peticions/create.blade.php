@extends('Front::layouts.admin')

@section('content')
<section class="inner-cont alta-p mt55">
    <div class="row1">
        <div class="form-list datos-de">
            <h2>@lang('Introduce los datos de petición')</h2>
            <form id="formulario" method="POST" action="{{ route("admin.peticion.store") }}">
                @csrf
                @include("Front::admin.peticions.form")
                <div class="clear"></div>
            </form>
        </div>
    </div>
</section>
@endsection

@section("scripts")
	<script>
	// importe
	// aporte
	// aportacion
	function updateAportacion() {
		var aporte = $("#aporte").val();
		var importe = $("#importe").val();
		if (aporte != "") {
			if (aporte != 0) {
				console.log("APORTE", aporte);
				console.log("IMPORTE", importe);
				$("#aportacion").val((importe * aporte / 100) + " €")
			} else {
				console.log("IMPORTE", importe);
				$("#aportacion").val(importe + " €")
			}
		}
	}

	$("#importe").keyup(function(e) {
		updateAportacion();
	});

	$("#aporte").keyup(function(e) {
		updateAportacion()
	});


	var required = ["nombre", "apellido", "dni", "importe"];
	$("#formulario").submit(function(e) {
		required.forEach(function(field) {
			var el = $(`[name=${field}]`);
			var label = $(`[for=${field}]`);
			if (el.val() == "") {
				if (label.length > 0) label.css("border-color", "#a21331");

				el.css("border", "2px solid #a21331")
				e.preventDefault();
			} else el.css("border", "2px solid #dadada;");
		});
	});
	</script>
	@if (session('status'))
	<script>
		$.notify({
			title: '{{ session("title") }}',
			message: '{!! session("msg") !!}'
		},{
			placement: {
				from: "bottom"
			},
			type: 'minimalist',
			delay: 5000,
			icon_type: 'image',
			template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} {{ session('status') }}" role="alert">' +
				'<span data-notify="title">{1}</span>' +
				'<span data-notify="message">{2}</span>' +
			'</div>'
		});
	</script>
	@endif

	@if ($errors->any())
		<script>
		@php($i=0)
		@foreach ($errors->getBag("default")->toArray() as $key => $error)
			setTimeout(function() {
				$.notify({
					title: '{{ $error["title"] }}',
					message: '{{ $error["msg"] }}',
				},{
					placement: {
						from: "bottom"
					},
					type: 'minimalist',
					delay: 5000,
					timer: 1000,
					animate: {
						enter: 'animated fadeInDown',
						exit: 'animated fadeOutUp'
					},
					allow_dismiss: true,
					icon_type: 'image',
					template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} error" role="alert">' +
						'<span data-notify="title">{1}</span>' +
						'<span data-notify="message">{2}</span>' +
					'</div>'
				});
			}, {{ 400 * ($i * 0.5) }});
			@php($i++)
		@endforeach
		</script>
	@endif
@endsection