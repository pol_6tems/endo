$(document).ready(function () {
    var win_hgt;
    $(window).resize(function () {
        win_hgt = $(window).height();
        $(".move #page").css({
            "height": win_hgt,
            "overflow": "hidden"
        });
    });

    $(document).ready(function () {

        win_hgt = $(window).height();
        $("#hie_menu").click(function () {
            $(".hole_div").addClass("move");
            $(this).hide();
            $("#new").show();
            $("#page").css({
                "height": win_hgt,
                "overflow": "hidden"
            });
        });

        $("#new").click(function () {
            $(".hole_div").removeClass("move");
            $("#hie_menu").show();
            $(this).hide();
            $("#page").css({
                "height": "auto",
                "overflow": "auto"
            });
        });

        $("#close-men").click(function () {
            $(".hole_div").removeClass("move");
            $("#hie_menu").show();
            $("#new").hide();
            $("#page").css({
                "height": "auto",
                "overflow": "auto"
            });
        });
    });
});

//Mobile Multi Level Menu		
$(".u-menu").vmenuModule({
    Speed: 200,
    autostart: true,
    autohide: true
});
$(".u-menu ul li ul").hide();

var lastScrollTop = 0;
$(window).scroll(function (event) {

    var st = $(this).scrollTop();
    var win_hgt = $(window).height();

    $('.animation').each(function () {
        var animation_div = $(this).offset().top + 300;
        if ((st + win_hgt) > animation_div) {
            $(this).addClass("start-animation");
        } else {
            $(this).removeClass("start-animation");
        }
    });
});

$(document).ready(function () {
    $(".select_box").selectbox();
    $("header").sticky({topSpacing:0});
});