$(document).ready(function () {
    $(".select_box").selectbox();

    $('.filter>ul>li').click(function () {
        if ($(this).hasClass('active')) {} else {
            $('.filter>ul>li').each(function () {
                $(this).removeClass('active');
            });
            $(this).addClass('active');
        }
    });

    $("header").sticky({
        topSpacing: 0
    });

    $(window).scroll(function () {
        var yellow_hgt = 0;
        var div_top = $("#get_po").offset().top;
        var win_top = $(window).scrollTop();
        var menu_hgt = $("header").height();
        yellow_hgt = $(".filter").height();
        var total_hgt = menu_hgt + yellow_hgt;

        if (div_top >= win_top + menu_hgt || win_top == 0) {
            $(".tb-amt").css({
                "position": "static",
                "top": menu_hgt
            });
            $(".tb-amt").removeClass('cnt-left');
        } else {
            $(".tb-amt").css({
                "position": "fixed",
                "top": menu_hgt
            });
            $(".tb-amt").addClass('cnt-left');
        }
    });

    /* quantity  script */
    jQuery(document).ready(function () {
        $('.qty').change(function () {
            var product = $(this).closest('.item');
            var total = product.data('price') * $(this).val();
            product.find('.total').html(total + ' &euro;');
            var item = $('.carro').find(`[data-id=${product.data('id')}]`);

            if ($(this).val() == 0) {
                if (item.length) {
                    item.remove();
                }
            } else {
                if (item.length) {
                    item.html(`
                        <td colspan="3">${product.data('product')} (${product.data('price')} &euro;) <span>x${$(this).val()}</span></td>
                        <td class="total" data-value="${total}">${total} &euro;</td>
                        <td>
                            <div class="close-btn"><img src="${close_btn_img}"></div>
                        </td>
                    `);
                } else {
                    $('.carro').append(`
                    <tr data-id="${product.data('id')}">
                        <td colspan="3">${product.data('product')} (${product.data('price')} &euro;) <span>x${$(this).val()}</span></td>
                        <td class="total" data-value="${total}">${total} &euro;</td>
                        <td>
                            <div class="close-btn"><img src="${close_btn_img}"></div>
                        </td>
                    </tr>`);
                }
            }

            var total_pass = 0;
            $('.carro.dsk .total').each(function (id, el) {
                total_pass += $(el).data('value');
            });
            $('.total-pass').data('total', total_pass);
            $('.total-pass').html(total_pass + ' &euro;');
            $('.mbl-qty').html(`(${$('.carro.mbl').children().length})`);
        });


        // This button will increment the value
        $('.qtyplus').click(function (e) {
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            fieldName = $(this).attr('field');
            // Get its current value
            var currentVal = parseInt($('input[name=' + fieldName + ']').val());
            // If is not undefined
            if (!isNaN(currentVal)) {
                // Increment
                $('input[name=' + fieldName + ']').val(currentVal + 1);
            } else {
                // Otherwise put a 0 there
                $('input[name=' + fieldName + ']').val(0);
            }
            $('input[name=' + fieldName + ']').change();
        });
        // This button will decrement the value till 0
        $(".qtyminus").click(function (e) {
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            fieldName = $(this).attr('field');
            // Get its current value
            var currentVal = parseInt($('input[name=' + fieldName + ']').val());
            // If it isn't undefined or its greater than 0
            if (!isNaN(currentVal) && currentVal > 0) {
                // Decrement one
                $('input[name=' + fieldName + ']').val(currentVal - 1);
            } else {
                // Otherwise put a 0 there
                $('input[name=' + fieldName + ']').val(0);
            }
            $('input[name=' + fieldName + ']').change();
        });
    });

    $(document).ready(function () {
        // easyResponsiveTabs
        $('#mob-amt').easyResponsiveTabs({
            type: 'default',
            width: 'auto',
            fit: true,
            closed: true,
            tabidentify: 'hor_1'
        });
    });
    // filter close
    $(document).on("click", ".close-btn img", function () {
        var row = $(this).closest('tr');
        $("#prodTable").find(`[data-id=${row.data('id')}] .qty`).val(0);
        $("#prodTable").find(`[data-id=${row.data('id')}] .qty`).change();
    });

    // filter table 
    $(document).ready(function () {
        
        $('#filterOptions li button').click(function () {
            // fetch the class of the clicked item
            var ourClass = $(this).attr('class');

            // reset the active class on all the buttons
            $('#filterOptions li').removeClass('active');
            // update the active state on our clicked button
            $(this).parent().addClass('active');

            if (ourClass == 'all') {
                // show all our items
                $('#prodTable').children('tr.item').slideDown();
            } else {
                // hide all elements that don't share ourClass
                $('#prodTable').children('tr:not(.' + ourClass + ')').slideUp();
                // show all elements that do share ourClass
                $('#prodTable').children('tr.' + ourClass).slideDown();
            }
            return false;
        });
    });
});