window.$ = window.jQuery = require('jquery');
import tippy from "./plugins/tippy-bundle.esm";
window.tippy = tippy;

import Swal from 'sweetalert2';
window.Swal = Swal;

require('./plugins/jquery.selectbox-0.2.js');
require('./plugins/easyResponsiveTabs.js');
require('./plugins/jquery.sticky.js');

require('./plugins/jquery.mmenu.min.all');
require('./plugins/bootstrap-notify');
require('./plugins/ddsmoothmenu');
require('./plugins/vmenuModule');

require('./pages/alta-peticio');
require('./pages/calculadora');
require('./pages/datos');
require('./plugins/common');

$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});

tippy('[data-tippy-content]');