@extends('Admin::layouts.admin')

@section('section-title')
    {{ $section_title }}
@endsection

@section('content')
<div class="row">
    <div class="col-md-{{ $width or '12' }}">
        <form id="form" class="form-horizontal" action="{{ route($route . '.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <article class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">{{ $section_icon }}</i>
                    </div>
                    <h4 class="card-title">@Lang('Add new')</h4>
                </div>

                <div class="card-body ">

                @if ($errors->count() > 0)
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                @if ( !empty($translable) && $translable )
                <div class="toolbar">
                    <ul class="nav nav-pills nav-pills-primary">
                        @foreach($_languages as $language_item)
                            <li class="nav-item {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                                <a class="nav-link {{ ($language_item->code == $language_code) ? 'active' : '' }}" data-toggle="tab" href="#{{$language_item->code}}">{{$language_item->code}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if ( !empty($translable) && $translable )
                <div class="tab-content">
                    @foreach($_languages as $language_item)
                        <div id="{{$language_item->code}}" class="tab-pane {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                        @foreach ( $rows as $key => $params )
                            @if ( empty($params['type']) )
                                @includeIf('Admin::default.subforms.text', ['is_create' => true, 'params' => $params, 'key' => $key, 'lang' => $language_item->code])
                            @elseif ( $params['type'] == 'subform' )
                                @includeIf($params['value'], ['is_create' => true, 'params' => $params, 'key' => $key, 'lang' => $language_item->code])
                            @else
                                @includeIf('Admin::default.subforms.' . $params['type'], ['is_create' => true, 'params' => $params, 'key' => $key, 'lang' => $language_item->code])
                            @endif
                        @endforeach
                        </div>
                    @endforeach
                </div>
                @else
                    @foreach ( $rows as $key => $params )
                        @if ( empty($params['type']) )
                            @includeIf('Admin::default.subforms.text', [
                                'is_create' => true,
                                'params' => $params,
                                'key' => $key
                            ])
                        @elseif ( $params['type'] == 'subform' || $params['type'] == 'subform_inline' )
                            @includeIf($params['value'], ['is_create' => true, 'params' => $params, 'key' => $key])
                        @else
                            @includeIf('Admin::default.subforms.' . $params['type'], [
                                'is_create' => true,
                                'params' => $params,
                                'key' => $key
                            ])
                        @endif
                    @endforeach
                @endif
                @if (isAdmin())
                    <div class="flex-row">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                    
                    <div class="flex-row">
                        <label class="col-sm-12 col-form-label">@Lang("Entitat")</label>
                        <div class="col-sm-12">
                            <select class="selectpicker" name="entitat">
                                @foreach($entitats as $entitat)
                                    <option value="{{ $entitat->id }}">{{ $entitat->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="flex-row">
                        <label class="col-sm-12 col-form-label">@Lang("Limite")</label>
                        <div class="col-sm-12">
                            <div class="form-group bmd-form-group">
                                <input id="limit" name="limit" type="number" class="form-control" value="0" tabindex="5">
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </article>

                @if (isset($cf_groups))
                {{-- CF GROUPS --}}
                @foreach ($cf_groups as $key => $cf_group)
                    @if($cf_group->position == 'bottom' && !$cf_group->fields->isEmpty())
                        <article id="item-cfg{{ $cf_group->id }}" class="card pm">
                            <div class="card-header card-header-primary card-header-icon">
                                <div class="flex-row" style="justify-content: space-between;">
                                    <div class="header-left">
                                        <h4 class="card-title">{{ $cf_group->title }}</h4>
                                    </div>
                                    <div class="header-right">
                                        <a data-toggle="collapse" href="#cf_group{{ $cf_group->id }}"><i class="material-icons">keyboard_arrow_down</i></a>
                                        <a href="{{ route('admin.custom_fields.edit', $cf_group->id) }}" target="_blank" rel="tooltip">
                                            <i class="material-icons">settings</i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div id="cf_group{{ $cf_group->id }}" class="collapse show">
                                <div class="card-body pb-5">
                                    <div class="custom_field_group_fields">
                                    @foreach ($cf_group->fields as $k => $custom_field)
                                        @php($viewParams = [
                                            'title' => $custom_field->title,
                                            'name' => "custom_fields[".$custom_field->id . "]",
                                            'value' => "",
                                            'params' => json_decode($custom_field->params),
                                            'position' => $cf_group->position,
                                            'custom_field' => $custom_field,
                                            'order' => $k
                                        ])

                                        @if (View::exists('Admin::default.custom_fields.' . $custom_field->type))
                                            <!-- General -->
                                                @includeIf('Admin::default.custom_fields.' . $custom_field->type, $viewParams)
                                            @else
                                                @php($module = explode(".", $custom_field->type)[0])
                                                @php($field = explode(".", $custom_field->type)[1])
                                                @includeIf('Modules::'.$module.'.resources.views.custom_fields.'.$field, $viewParams)
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </article>
                    @endif
                @endforeach
            @endif
            <article class="card sticky bottom">
                <div class="card-footer">
                    <div class="mr-auto">
                        <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" value="@Lang('Tornar')" onclick="window.location.href='{{ route($route . '.index') }}'">
                    </div>
                    <div class="ml-auto">
                        <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd" value="@Lang('Create')">
                    </div>
                    <div class="clearfix"></div>
                </div>
            </article>
        </form>
    </div>
</div>
@endsection

@section('styles')
<link href="{{asset($_admin.'js/summernote/summernote.css')}}" rel="stylesheet">
@endsection

@section('scripts')

<script src="{{asset($_admin.'js/summernote/summernote.js')}}"></script>
@php( $summernote_lang = 'en-US' )
@if( \App::getLocale() == 'ca' )
    @php( $summernote_lang = 'ca-ES' )
@else
    @php( $summernote_lang = strtolower(\App::getLocale()) . '-' . strtoupper(\App::getLocale()) )
@endif

@if ( \App::getLocale() != 'en' )
<script src="{{asset($_admin.'js/summernote/lang/summernote-'.$summernote_lang.'.js')}}"></script>
@endif
<script>
$(document).ready(function() {
    $('.summernote').summernote({
        height: '300',
        lang: '{{ $summernote_lang }}',
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        }
    });
    $('.note-popover').css('display', 'none');
});
</script>

@yield('scripts2')

@includeIf('Admin::posts.media_modal')

@yield('scripts-chat-buttons')

@endsection