@extends('Admin::layouts.admin')

@section('section-title')
    {{ $section_title }}
@endsection

@section('content')
@php( $disabled = isset($disabled) && $disabled )
@php( $title_card = isset($title_card) && !empty($title_card) ? $title_card : __('Edit') )
<form id="form" class="form-horizontal" action="{{ route($route . '.update', $item->id) }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="flex-row">
        <div class="col-md-{{ $width or '12' }}">
            <input type="hidden" name="_method" value="PUT">
            <article class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">{{ $section_icon }}</i>
                    </div>
                    <h4 class="card-title">
                        {{ $title_card }}
                        @if ( !$disabled && (!isset($can_create) || $can_create) )
                            <a href="{{ route($route . '.create') }}" class="btn btn-primary btn-sm">@Lang('Add new')</a>
                        @endif
                    </h4>
                </div>
                <div class="card-body ">
                    {{ mostrarErrors($errors) }}
                    
                    @if ( !empty($translable) && $translable )
                        {{ mostrarIdiomes($_languages, $language_code) }}
                    @endif

                    @if ( !empty($translable) && $translable )
                    <div class="tab-content">
                        @foreach($_languages as $language_item)
                            <div id="{{$language_item->code}}" class="tab-pane {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                            @foreach ( $rows as $key => $params )
                                @if ( empty($params['type']) )
                                    @includeIf('Admin::default.subforms.text', ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item, 'lang' => $language_item->code])
                                @elseif ( $params['type'] == 'subform' )
                                    @includeIf($params['value'], ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item, 'lang' => $language_item->code])
                                @else
                                    @includeIf('Admin::default.subforms.' . $params['type'], ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item, 'lang' => $language_item->code])
                                @endif
                            @endforeach
                            </div>
                        @endforeach
                    </div>
                    @else
                        @foreach ( $rows as $key => $params )
                            @if ( empty($params['type']) )
                                @includeIf('Admin::default.subforms.text', ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item])
                            @elseif ( $params['type'] == 'subform_inline' )
                                @includeIf($params['value'], ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item])
                            @elseif ( $params['type'] != 'subform' )
                                @if ( !isset($params['auth']) || $params['auth'] && Auth::user()->isAdmin())
                                    @includeIf('Admin::default.subforms.' . $params['type'], ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item])
                                @endif
                            @endif
                        @endforeach
                    @endif

                    @if (isAdmin())
                    <div class="flex-row">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                    
                    <div class="flex-row">
                        <label class="col-sm-12 col-form-label">@Lang("Entitat")</label>
                        <div class="col-sm-12">
                            <select class="selectpicker" name="entitat">
                                @foreach($entitats as $entitat)
                                    <option value="{{ $entitat->id }}" {{ $item->entitat == $entitat->id ? "selected" : "" }}>{{ $entitat->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="flex-row">
                        <label class="col-sm-12 col-form-label">@Lang("Limite")</label>
                        <div class="col-sm-12">
                            <div class="form-group bmd-form-group">
                                <input id="limit" name="limit" type="number" class="form-control" value="{{ $item->limit }}" tabindex="5">
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </article>

            <article class="card card-extra-fields">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">developer_board</i>
                    </div>
                    <h4 class="card-title">
                        @Lang('Fields')
                    </h4>
                </div>
                <div class="card-body">
                    @foreach ( $rows as $key => $params )
                        @if ( !empty($params['type']) && $params['type'] == 'subform' )
                            @includeIf($params['value'], ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item])         
                        @endif
                    @endforeach
                </div>

                <div class="card-footer">
                    <div class="ml-auto">
                        <button type="button" rel="tooltip" class="btn btn-sm btn-primary add_field">
                            @Lang('Add')
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </article>
            
            <!-- SUBFORM CARD -->
            @foreach ( $rows as $key => $params )
                @if ( !empty($params['type']) && $params['type'] == 'subform_card' )
                    @includeIf($params['value'], ['is_create' => false, 'params' => $params, 'key' => $key, 'item' => $item])         
                @endif
            @endforeach
            <!-- end SUBFORM CARD -->

            @if (isset($cf_groups))
                {{-- CF GROUPS --}}
                @foreach ($cf_groups as $key => $cf_group)
                    @if($cf_group->position == 'bottom' && !$cf_group->fields->isEmpty())
                        <article id="item{{ $item->id}}-cfg{{ $cf_group->id }}" data-item="{{ $item->id }}" class="card pm">
                            <div class="card-header card-header-primary card-header-icon">
                                <div class="flex-row" style="justify-content: space-between;">
                                    <div class="header-left">
                                        <h4 class="card-title">{{ $cf_group->title }}</h4>
                                    </div>
                                    <div class="header-right">
                                        <a data-toggle="collapse" href="#cf_group{{ $cf_group->id }}"><i class="material-icons">keyboard_arrow_down</i></a>
                                        <a href="{{ route('admin.custom_fields.edit', $cf_group->id) }}?item_type={{$item_type}}" target="_blank" rel="tooltip">
                                            <i class="material-icons">settings</i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div id="cf_group{{ $cf_group->id }}" class="collapse">
                                <div class="card-body pb-5">
                                    <div class="custom_field_group_fields">
                                    @foreach ($cf_group->fields as $k => $custom_field)
                                        @php($viewParams = [
                                            'title' => $custom_field->title,
                                            'name' => "custom_fields[".$custom_field->id . "]",
                                            'value' => $item->get_field( $custom_field->name ),
                                            'params' => json_decode($custom_field->params),
                                            'position' => $cf_group->position,
                                            'custom_field' => $custom_field,
                                            'order' => $k
                                        ])

                                        @if (View::exists('Admin::default.custom_fields.' . $custom_field->type))
                                            <!-- General -->
                                                @includeIf('Admin::default.custom_fields.' . $custom_field->type, $viewParams)
                                            @else
                                                @php($module = explode(".", $custom_field->type)[0])
                                                @php($field = explode(".", $custom_field->type)[1])
                                                @includeIf('Modules::'.$module.'.resources.views.custom_fields.'.$field, $viewParams)
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </article>
                    @endif
                @endforeach
            @endif
        </div>
    </div>

    @isset ( $subs )
    @foreach ( $subs as $subkey => $sub )
    <div class="flex-row">
        <div class="col-md-12">
            <article class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">{{ $sub['section_icon'] }}</i>
                    </div>
                    <h4 class="card-title">
                        {{ $sub['section_title'] }}
                        <a href="{{ route($sub['route'] . '.create', $item->id) }}" class="btn btn-primary btn-sm">@Lang('Add new')</a>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="toolbar">
                        <!--        Here you can write extra buttons/actions for the toolbar              -->
                    </div>
                    <div class="material-datatables">
                        <table id="datatables-{{ $subkey }}" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    @foreach ($sub['headers'] as $header => $params)
                                        <th
                                        @if ( array_key_exists('width', $params) )
                                            width="{{ $params['width'] }}"
                                        @endif
                                        @if ( array_key_exists('class', $params) )
                                            class="{{ $params['class'] }}"
                                        @endif
                                        >
                                            {{ $header }}
                                        </th>
                                    @endforeach
                                    <th width="10%" class="disabled-sorting text-right">@Lang('Actions')</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    @foreach ($sub['headers'] as $header => $params)
                                        <th
                                        @if ( array_key_exists('width', $params) )
                                            width="{{ $params['width'] }}"
                                        @endif
                                        @if ( array_key_exists('class', $params) )
                                            class="{{ $params['class'] }}"
                                        @endif
                                        >
                                            {{ $header }}
                                        </th>
                                    @endforeach
                                    <th width="10%" class="disabled-sorting text-right">@Lang('Actions')</th>
                                </tr>
                            </tfoot>
                            <tbody>
                            @forelse ($sub['items'] as $subitem)
                                <tr>
                                    @foreach ( $sub['rows'] as $params )
                                        @php ( $field = $params['value'] )
                                        @if ( !is_array($field) )
                                            @php ( $show_value = true )
                                            @php ( $name = $subitem->{$sub['field_name']} )
                                            @php ( $field_value = strpos($field, '()') !== false ? $subitem->{str_replace('()','',$field)}() : $subitem->{$field} )
                                        @else
                                            @php ( $field_value = $subitem )
                                            @foreach ( $field as $f_key => $f )
                                                @php ( $field_value = strpos($f, '()') !== false ? $field_value->{str_replace('()','',$f)}() : $field_value->{$f} )
                                            @endforeach
                                        @endif
                                        <td
                                        @if ( array_key_exists('width', $params) )
                                            width="{{ $params['width'] }}"
                                        @endif
                                        @if ( array_key_exists('class', $params) )
                                            class="{{ $params['class'] }}"
                                        @endif
                                        >
                                            @if ( array_key_exists('type', $params) )
                                                @if ( $params['type'] == 'icon' )
                                                    <i class="material-icons pr-2 admin-menu-icona" style="font-size: 2rem;">
                                                @elseif ( $params['type'] == 'img' )
                                                    @php ( $show_value = false )
                                                    <img class="show_img col-imatge col-10" src="data:image/jpeg;base64,{{ base64_encode( $field_value ) }}" 
                                                        data-img="data:image/jpeg;base64,{{ base64_encode( $field_value ) }}" 
                                                        data-name="{{ $name }}"
                                                    />
                                                @elseif ( $params['type'] == 'thumbnail' )
                                                    @php ( $show_value = false )
                                                    <img class="show_img col-imatge col-10" src="{{ $subitem->get_thumbnail_url('thumbnail') }}"
                                                        data-img="{{ $subitem->get_thumbnail_url('large') }}" 
                                                        data-name="{{ $name }}"
                                                        {{--style="width: 50px;object-fit: scale-down;"--}}
                                                    />
                                                @elseif ( $params['type'] == 'switch_active' )
                                                    @php ( $show_value = false )
                                                    <button data-url="{{ route($params['route']) }}" data-id="{{$subitem->id}}" data-name="{{$name}}" data-onlyone="{{$params['onlyone']}}" type="button" rel="tooltip" class="btn btn-link btn-just-icon switch_active btn-{{ $subitem->active ? 'success' : 'danger' }}">
                                                        <i class="material-icons">{{ $subitem->active ? 'check' : 'clear' }}</i>
                                                    </button>
                                                @elseif ( $params['type'] == 'count' )
                                                    @php ( $show_value = false )
                                                    {{ count($field_value) }}
                                                @elseif ( $params['type'] == 'date' )
                                                    @php ( $show_value = false )
                                                    @php ( $date_format = !empty($params['format']) ? $params['format'] : 'd/m/Y h:i' )
                                                    {{ $field_value->format( $date_format ) }}
                                                @endif
                                            @endif

                                            @if ( array_key_exists('type', $params) && $params['type'] == 'icon' )
                                                <i class="material-icons pr-2 admin-menu-icona" style="font-size: 2rem;">
                                            @endif
                                            
                                            @if ( $show_value )
                                                @if ( array_key_exists('translate', $params) && $params['translate'] )
                                                    {{ __($field_value) }}
                                                @else
                                                    {{ $field_value }}
                                                @endif
                                            @endif
                                            
                                            @if ( array_key_exists('type', $params) && $params['type'] == 'icon' )
                                                </i>
                                            @endif
                                        </td>
                                    @endforeach
                                    <td class="td-actions text-right">
                                        <button onclick="window.location='{{ route($sub['route'] . '.edit', $subitem->id) }}';" type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-success">
                                            <i class="material-icons">edit</i>
                                        </button>
                                        <button data-url="{{ route($sub['route'] . '.destroy', $subitem->id) }}" data-name="@Lang('Are you sure to delete :name?', ['name' => __($name)])" type="button" rel="tooltip" class="btn btn-just-icon btn-sm btn-danger remove">
                                            <i class="material-icons">close</i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="{{ count($sub['headers']) }}">@Lang('No entries found.')</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </article>
        </div>
    </div>
    @endforeach
    @endisset

    <article class="card sticky bottom">
        @if ( !$disabled )
        <div class="card-footer">
            <div class="mr-auto">
                <input type="button" class="btn btn-previous btn-fill btn-default btn-wd" value="@Lang('Tornar')" onclick="window.location.href='@if (!isset($can_create) || $can_create){{ route($route . '.index') }}@else{{ route('admin') }}@endif'">
            </div>
            <div class="ml-auto">
                <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd" value="@Lang('Update')">
            </div>
            <div class="clearfix"></div>
        </div>
        @endif
    </article>
</form>
@endsection

@section('styles')
<link href="{{asset($_admin.'js/summernote/summernote.css')}}" rel="stylesheet">
@yield('styles2')
@endsection


@section('scripts')
<script src="{{asset($_admin.'js/clipboard.min.js')}}"></script>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function() {

    // Si el card Campos (Fields) està buid, remove.
    if ( $('.card-extra-fields .card-body').html().trim() == "" ) {
        $('.card-extra-fields').remove();
    }

    new ClipboardJS('button.copy', {
        text: function(trigger) {
            return trigger.textContent;
        }
    });
@isset ( $subs )
    @foreach ( $subs as $subkey => $sub )
        @if ( !empty($sub['items']) && $sub['items']->count() > 0 )

        var table_{{$subkey}} = $('#datatables-{{$subkey}}').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "@Lang('All')"]
            ],
            responsive: true,
            /*language: {
                search: "_INPUT_",
                searchPlaceholder: "@Lang('Search')",
            }*/
            language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" }
        });

        //var table = $('#datatable').DataTable();

        // Delete a record
        table_{{$subkey}}.on('click', '.remove', function(e) {
            var $tr = $(this).closest('tr');
            var url = $(this).data('url');
            var title = $(this).data('name');
            swal({
                text: title,
                //title: 'Are you sure?',
                //text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonText: '@Lang('Delete')',
                cancelButtonText: '@Lang('Cancel')',
                buttonsStyling: false
            }).then(function(result) {
                if ( result.value ) {
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {'_token': '{{csrf_token()}}', '_method': 'DELETE'},
                        success: function(data) {
                            swal({
                                title: "@Lang('Deleted')",
                                text: "@Lang('Item deleted succesfully.')",
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            });
                            table_{{$subkey}}.row($tr).remove().draw();
                            e.preventDefault();
                        },
                        error: function(data) {
                            swal({
                                title: "@Lang('Error')",
                                text: "@Lang('Error saving data')",
                                type: 'error',
                                confirmButtonClass: "btn",
                                buttonsStyling: false
                            });
                        }
                    });
                }
            });
        });

        table_{{$subkey}}.on('click', '.switch_active', function(e) {
            var boto = $(this);
            var url = $(this).data('url');
            var title = $(this).data('name');
            var text = "@Lang('actived')";
            var id = $(this).data('id');
            var onlyone = $(this).data('onlyone');
            
            $.ajax({
                url: url,
                method: 'POST',
                data: {'_token': '{{csrf_token()}}', 'id': id},
                success: function(data) {
                    if ( data.active ) {
                        swal({
                            title: "{{ucfirst(__('actived'))}}",
                            text: title + " @Lang('actived')",
                            type: 'success',
                            confirmButtonClass: "btn btn-success",
                            buttonsStyling: false
                        });
                        if ( onlyone ) {
                            $('.switch_active').removeClass('btn-success');
                            $('.switch_active').addClass('btn-danger');
                            $('.switch_active').find('i').html('clear');
                        }
                        boto.removeClass('btn-danger');
                        boto.addClass('btn-success');
                        boto.find('i').html('check');
                    } else {
                        swal({
                            title: "{{ucfirst(__('deactived'))}}",
                            text: title + " @Lang('deactived')",
                            type: 'success',
                            confirmButtonClass: "btn btn-success",
                            buttonsStyling: false
                        });
                        boto.addClass('btn-danger');
                        boto.removeClass('btn-success');
                        boto.find('i').html('clear');
                    }
                    e.preventDefault();
                },
                error: function(data) {
                    swal({
                        title: "@Lang('Error')",
                        text: "@Lang('Error saving data')",
                        type: 'error',
                        confirmButtonClass: "btn",
                        buttonsStyling: false
                    });
                }
            });
        });

        table_{{$subkey}}.on('click', '.show_img', function(e) {
            var boto = $(this);
            var img = $(this).data('img');
            var title = $(this).data('name');
            swal({
                title: title,
                width: '80%',
                showCloseButton: true,
                showCancelButton: false,
                showConfirmButton: false,
                html: '<img src="'+img+'" style="width: 100%;">'
            });
        });

        @endif

    @endforeach
@endisset

});
</script>

<script src="{{asset($_admin.'js/summernote/summernote.js')}}"></script>
@php( $summernote_lang = 'en-US' )
@if( \App::getLocale() == 'ca' )
    @php( $summernote_lang = 'ca-ES' )
@else
    @php( $summernote_lang = strtolower(\App::getLocale()) . '-' . strtoupper(\App::getLocale()) )
@endif

@if ( \App::getLocale() != 'en' )
<script src="{{asset($_admin.'js/summernote/lang/summernote-'.$summernote_lang.'.js')}}"></script>
@endif
<script>
$(document).ready(function() {
    $('.summernote').summernote({
        height: '300',
        lang: '{{ $summernote_lang }}',
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        }
    });
    $('.note-popover').css('display', 'none');
});
</script>

@yield('scripts2')

@includeIf('Admin::posts.media_modal')

@yield('cf')

@yield('scripts-chat-buttons')

    @if (isset($cf_groups))
        @foreach ($cf_groups as $key => $cf_group)
            @foreach ($cf_group->fields as $custom_field)

                <!-- Scripts CF id: {{ $custom_field->id }}-->
                @yield("scripts-cf$custom_field->id")

                @if ( $custom_field->type == 'repeater' )
                    @if( $repeater_children = $item->get_field($custom_field->name) )
                        @foreach ( $repeater_children as $nrc => $r_child_fields )
                            @foreach ( $r_child_fields as $r_child_field )
                                @php ( $id_element_nrc = 'cf' . $r_child_field['id'] . '_k' . $nrc )
                                <!-- Scripts CF id: {{ $custom_field->id }} repeater field id: {{ $r_child_field['id'] }} num: {{ $nrc }}-->
                                @yield("scripts-$id_element_nrc")
                            @endforeach
                        @endforeach
                    @endif
                @endif

            @endforeach
        @endforeach
    @endif

@endsection