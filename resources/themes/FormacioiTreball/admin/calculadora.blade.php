@extends('Front::layouts.admin')

@section('content')
	<section class="inner-cont mt55">
		<div class="row">
			<h1>@lang('Productes Targeta Pass')</h1>
			<div class="filter">
				<h3>Filtre</h3>
				<div class="select-filter">
					<div class="select-box" id="filter-name">
						<a href="#" class="select-txt">@lang('Tots')</a>
						<a href="#" class="select-arw"></a>
					</div>
					<ul id="filterOptions">
						<li class="active"><button type="button" data-filter="all" class="all">Tots</button></li>
						@foreach($categories as $key => $categoria)
							<li><button type="button" class="{{ $categoria->post_name }}">{{ $categoria->title }}</button></li>
						@endforeach
					</ul>
				</div>
			</div>

			<div class="table-list">
				<div class="tb-lft">
					<table class="list-tab" width="100%" cellspacing="0" cellpadding="0" border="0">
						<thead>
							<tr>
								<th width="25%">@lang('NOM DEL PRODUCTE')</th>
								<th width="25%">@lang('PREU UNITARI')</th>
								<th width="30%">@lang('QUANTITAT')</th>
								<th width="20%">@lang('PREU TOTAL')</th>
							</tr>
							<tr>
								<th width="25%">@lang('NOM')</th>
								<th width="25%">@lang('PREU')</th>
								<th width="30%">@lang('QUAN.')</th>
								<th width="20%">@lang('TOTAL')</th>
							</tr>
						</thead>
						<tbody id="prodTable">
							@foreach($productes as $producte)
							@php($categoria = $producte->get_field('categoria'))
							@php($price = $producte->get_field('precio'))
							<tr class="item {{ $categoria->post_name }}" data-id="{{ $producte->id }}" data-product="{{ $producte->title }}" data-price="{{ $price }}">
								<td class="name">{{ $producte->title }}</td>
								<td class="price">{{ $price }} &euro;</td>
								<td>
									<div class="qty-chk">
										<input type='button' value='-' class='qtyminus' field='{{ $producte->post_name }}' />
										<input type='text' name='{{ $producte->post_name }}' value='0' class='qty' />
										<input type='button' value='+' class='qtyplus' field='{{ $producte->post_name }}' />
									</div>
								</td>
								<td class="total">0 &euro;</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="tb-rgt">
					<div class="amt-desk">
						<div id="get_po" style="float:left; width:100%;"></div>
						<div class="tb-amt">
							<div class="scroll">
								<table class="amt-tab" width="100%" cellspacing="0" cellpadding="0" border="0">
									<thead>
										<tr>
											<th>@lang('PRODUCTES SEL·LECCIONATS')</th>
										</tr>
									</thead>
									<tbody class="carro dsk"></tbody>
									<tfoot>
										<tr>
											<th>@lang('Total Targeta PASS')</th>
											<th class="total-pass">0 &euro;</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					<div class="amt-mob">
						<div id="mob-amt">
							<ul class="resp-tabs-list hor_1">
								<li>@lang('PRODUCTES SEL·LECCIONATS') <span class="mbl-qty value">(0)</span></li>
							</ul>
							<div class="resp-tabs-container hor_1">
								<!-- tab 1 -->
								<div>
									<table class="amt-tab" width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody class="carro mbl"></tbody>
									</table>
								</div>
							</div>
						</div>
						<table class="amt-tab" width="100%" cellspacing="0" cellpadding="0" border="0">
							<tfoot>
								<tr>
									<th>@lang('Total Targeta PASS')</th>
									<th class="total-pass">0 &euro;</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script>
		var close_btn_img = '{{ asset($_front.'images/close-btn.png') }}';
	</script>
@endsection
