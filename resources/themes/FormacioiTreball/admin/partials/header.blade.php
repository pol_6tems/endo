<header class="inner-page"> 
    <!--Header Starts-->
    <div class="top-header"> 
        <div class="row1">
            <div class="inner-top">  
                <div class="logo"><a href="{{ route("admin.peticion.index") }}"><img src="{{ asset($_front."images/formacio-i-treball-logo-2.jpg") }}"/></a></div>
                <div class="mob-logo"><a href="{{ route("admin.peticion.index") }}"><img src="{{ asset($_front."images/formacio-i-treball-mob-logo.jpg") }}"/></a></div>
                <!-- mobile nav starts-->
                <div class="m-menu"><a href="javascript:void(0);" id="hie_menu"> <span></span> <span></span> <span></span></a></div>
                <!-- mobile nav ends--> 
                <ul>
                    {{--
                    <li>
                        <div class="lang-select">
                            <select class="select_box">
                                <option value="{{ route(Route::currentRouteName(), ["locale" => "es"]) }}" {{ app()->getLocale() == "es" ? 'selected' : ''}}>@lang('Español')</option>
                                <option value="{{ route(Route::currentRouteName(), ["locale" => "ca"]) }}" {{ app()->getLocale() == "ca" ? 'selected' : ''}}>@lang('Català')</option>
                            </select>
                        </div>
                    </li>
                    --}}
                    <li>
                        <a class="usuario" data-tippy-content="@Lang("Saldo: :valor / :max", ["valor" => auth()->user()->limit, "max" => 1000])" href="{{ route("admin.tecnico.edit", auth()->user()) }}">@lang('Bienvenido :usuario!', ["usuario" => Auth::user()->fullname()])</a>
                    </li>
                    @if (isAdmin())
                    <li>
                        <a class="admin-b" href="{{ route("admin") }}">@lang('Panel de administración')</a>
                    </li>
                    @endif
                    <li>
                        <a class="salir-l" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            @Lang('Salir')
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="row1">	
            <div id="smoothmenu" class="ddsmoothmenu">
                <ul>
                    <li><a href="{{ route("admin.peticion.create") }}" class="{{ (url()->current() == route("admin.peticion.create")) ? "active" : "" }}">@lang('ALTA PETICIÓN')</a></li>
                    <li><a href="{{ route("admin.peticion.index") }}" class="{{ (url()->current() == route("admin.peticion.index")) ? "active" : "" }}">@lang('LISTADO DE PETICIONES')</a></li>
                    <li><a href="{{ route("calculadora") }}" class="{{ (url()->current() == route("calculadora")) ? "active" : "" }}">@lang('CALCULADORA')</a></li>
                    <li><a href="{{ route("admin.tecnico.edit", auth()->user()) }}" class="{{ (url()->current() == route("admin.tecnico.edit", auth()->user())) ? "active" : "" }}">@lang('DATOS DE USUARIO')</a></li>
                    @if (auth()->user()->rol->name == "cap-entitat")
                        <li><a href="{{ route("admin.tecnico.index") }}" class="{{ (url()->current() == route("admin.tecnico.index")) ? "active" : "" }}">@lang('TÉCNICOS')</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</header>