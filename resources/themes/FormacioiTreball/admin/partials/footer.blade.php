<footer>
    <div class="row1">
        <h4><span>Fundació Formació i Treball 2017</span>  - Carrer Ramon Llull, 430-438 - Sant Adrià de Besòs (Barcelona)</h4>
        <ul>
            <li><a class="tel-ico" href="tel:(+34) 93 303 41 00">(+34) 93 303 41 00</a></li>
            <li><a class="mail-ico" href="mailto:fit@formacioitreball.org">fit@formacioitreball.org</a></li>
        </ul>
        <h3>Fundació Formació i Treball 2017</h3>
    </div>
</footer>