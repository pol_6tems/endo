<!-- Mobile Navigation -->
<div id="mobNav" class="mobNav  mm-menu mm-horizontal mm-offcanvas mm-right mm-current mm-opened ">
    <div class="mm-list mm-panel mm-opened mm-current">
        <figure class="mobi-logo"> <a href="index.html" title=""><img src="{{ asset($_front."images/formacio-i-treball-mob-logo.jpg") }}" alt="" title=""></a> </figure>
        <div id="close-men"><a href="javascript:void(0);"><img src="{{ asset($_front."images/mb-close.png") }}" alt="" title=""></a></div>
        <div class="u-menu">
            <ul>
            <li><a href="javascript:void(0);" class="active">Alta Petición</a></li>
            <li><a href="javascript:void(0);">Listado De Peticiones</a></li>
            <li><a href="javascript:void(0);">Calculadora</a></li>
            <li><a href="javascript:void(0);">Datos De Usuario</a></li>
            <li><a href="javascript:void(0);">Bienvenido Oriol!</a></li>
            </ul>
        </div>
        <div class="lang">
            <ul>
            <li><a href="javascript:void(0);">Español</a></li>
            <li><a href="javascript:void(0);">Español</a></li>
            <li><a href="javascript:void(0);">Español</a></li>
            </ul>
        </div>
        <div class="client-link">
            <a href="javascript:void(0);" class="salir-l">Salir</a>
        </div>
    </div>
</div>
<!-- /Mobile Navigation --> 