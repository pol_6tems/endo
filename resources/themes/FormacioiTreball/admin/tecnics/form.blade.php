<li>
    <label>@lang('Nombre')</label>
    <input type="text" name="name" class="frm-ctrl" value="{{ (isset($user) && $user->name != "") ? $user->name : old('name') }}">
</li>
<li>
    <label>@lang('Apellido')</label>
    <input type="text" name="lastname" class="frm-ctrl" value="{{ (isset($user) && $user->lastname != "") ? $user->lastname : old('lastname') }}">
</li>
<li>
    <label>@lang('Teléfono')</label>
    <input type="text" name="custom_fields[22]" class="frm-ctrl" value="{{ (isset($user) && $user->get_field('telefono') != "") ? $user->get_field('telefono') : old('custom_fields.22') }}">
</li>
<li>
    <label>@lang('Email')</label>
    <input type="text" name="email" class="frm-ctrl" value="{{ (isset($user) && $user->email != "") ? $user->email : old('email') }}">
</li>