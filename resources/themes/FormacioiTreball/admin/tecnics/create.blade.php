@extends('Front::layouts.admin')

@section("content")
<section class="inner-cont alta-p mt55">
    <div class="row1">
        <div class="form-list datos-de">
      		<h2>@lang('Crear nuevo técnico')</h2>  		
            <form id="formulario" method="POST" enctype="multipart/form-data" action="{{ route("admin.tecnico.store") }}">
				@csrf
				<ul>
                    @includeIf("Front::admin.tecnics.form")
                    <li><button type="submit" class="">@lang('GUARDAR')</button></li>
                </ul>
                <div class="clear"></div>
            </form>
        </div>
    </div>	
</section>
@endsection

@section("scripts")
	<script>
	var required = ["name", "lastname", "email"];
	$("#formulario").submit(function(e) {
		required.forEach(function(field) {
			var el = $(`[name=${field}]`);
			var label = $(`[for=${field}]`);
			if (el.val() == "") {
				if (label.length > 0) label.css("border-color", "#a21331");

				el.css("border", "2px solid #a21331")
				e.preventDefault();
			} else el.css("border", "2px solid #dadada;");
		});
	});
	</script>
    @if ($errors->any())
		<script>
		@php($i=0)
		@foreach ($errors->all() as $key => $error)
			setTimeout(function() {
				$.notify({
					title: '{{ $error }}',
					message: '',
				},{
					placement: {
						from: "bottom"
					},
					type: 'minimalist',
					delay: 5000,
					timer: 1000,
					animate: {
						enter: 'animated fadeInDown',
						exit: 'animated fadeOutUp'
					},
					allow_dismiss: true,
					icon_type: 'image',
					template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} error" role="alert">' +
						'<span data-notify="title">{1}</span>' +
						'<span data-notify="message">{2}</span>' +
					'</div>'
				});
			}, {{ 400 * ($i * 0.5) }});
			@php($i++)
		@endforeach
		</script>
	@endif
@endsection