@extends('Front::layouts.admin')

@section("content")
<section class="inner-cont mt35">
    <div class="row1">
        <a class="btn btn-primary" href="{{ route("admin.tecnico.create") }}" style="float: right">@Lang("Crear nuevo técnico")</a>
    </div>
</section>
<section class="inner-cont alta-p mt35">
    <div class="row1">
        <div class="table-list">
            <div class="table-pad">
                <table class="tab-resp table-tecnics" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <thead>
                        <tr>
                            <th width="14%">@lang('NAME')</th>
                            <th width="10%">@lang('EMAIL')</th>
                            <th width="20%">@lang('LIMIT')</th>
                            {{-- <th width="10%">@lang('ENTITAT')</th> --}}
                            <th width="20%">@lang('NÚM. PETICIONS')</th>
                            <th width="4%"></th>
                        </tr>
                    </thead>
                    <tbody id="prodTable">
                        @forelse($users as $tecnic)
                        <tr class="item tecnics {{ ($tecnic->id == auth()->user()->id) ? "current" : "" }}" data-nombre="{{ $tecnic->fullname() }}">
                            <td>{{ $tecnic->fullname() }}</td>
                            <td><a href="mailto:{{ $tecnic->email }}">{{ $tecnic->email }}</a></td>
                            <td>{{ $tecnic->limit }} / 1000</td>
                            {{-- <td>{{ $tecnic->entitat }}</td> --}}
                            <td style="text-align:center;">{{ $tecnic->npeticions }}</td>
                            <td>
                                <div class="accions">
                                    <a href="{{ route("admin.tecnico.edit", $tecnic) }}" data-tippy-content="@Lang("Modificar Técnico")" class="accio editar"><i class="material-icons">create</i></a>
                                    <form action="{{ route("admin.tecnico.destroy", $tecnic) }}" method="POST">
                                        @csrf
                                        @method("DELETE")
                                        <a href="javascript:void(0)" data-tippy-content="@Lang("Eliminar Técnico")" class="accio destroy"><i class="material-icons">clear</i></a>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10" style="text-align: center;">@lang('No s\'ha trobat cap resultat')</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>	
        </div>
    </div>	
</section>
@endsection

@section("scripts")
    <script>
        $(".destroy").on("click", function(e) {
            var target = $(this);
            Swal.fire({
                title: '¿Estas seguro?',
                text: "Vas a eliminar la peticion",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, quiero borrarla'
            }).then((result) => {
                if (result.value) {
                    target.closest("form").submit();
                }
            })
        });
    </script>
    @if (session('status'))
	<script>
    $.notify({
        title: '{{ session("title") }}',
        message: '{!! session("msg") !!}'
    },{
        placement: {
            from: "bottom"
        },
        type: 'minimalist',
        delay: 5000,
        icon_type: 'image',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} {{ session('status') }}" role="alert">' +
            '<span data-notify="title">{1}</span>' +
            '<span data-notify="message">{2}</span>' +
        '</div>'
    });
	</script>
	@endif
@endsection