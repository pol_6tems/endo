@extends('Front::layouts.admin')

@section("content")
<section class="inner-cont alta-p mt55">
    <div class="row1">
        <div class="form-list datos-de">
      		<h2>@lang('Modificar sus datos de Acceso')</h2>  		
            <form method="POST" enctype="multipart/form-data" action="{{ route("admin.tecnico.update", $user) }}">
				@csrf
                @method("PUT")
				<ul>
                	@includeIf("Front::admin.tecnics.form")
					<li>
						<label>@lang('Password')</label>
						<input type="password" name="password" class="frm-ctrl">
					</li>
					<li>
						<label>@lang('Repita su password')</label>
						<input type="password" name="password_confirmation" class="frm-ctrl">
					</li>
					<li><button type="submit" class="">@lang('GUARDAR')</button></li>
				</ul>
                <div class="clear"></div>
            </form>
        </div>
    </div>
</section>
@endsection



@section("scripts")
	@if (session('status'))
	<script>
		$.notify({
			title: '{{ session("title") }}',
			message: '{!! session("msg") !!}'
		},{
			placement: {
				from: "bottom"
			},
			type: 'minimalist',
			delay: 5000,
			icon_type: 'image',
			template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} {{ session('status') }}" role="alert">' +
				'<span data-notify="title">{1}</span>' +
				'<span data-notify="message">{2}</span>' +
			'</div>'
		});
	</script>
	@endif
@endsection