@extends('layouts.admin')

@section('header')
<h1>
	{{ $item->fullname(true) }}
</h1>
@endsection

@section('content')
<form method="POST" action="{{route('admin.users.update', $item->id)}}">
	<input type="hidden" name="_method" value="PUT">
	{!! csrf_field() !!}
	
	<div class="form-group">
		<label for="name" class="bmd-label-floating">@Lang('Name')</label>
		<input type="text" class="form-control" id="name" name="name" value="{{ $item->name }}" required>
		<div class="text-danger">{{$errors->first('name')}}</div>
	</div>
	<div class="form-group">
		<label for="lastname" class="bmd-label-floating">@Lang('Lastname')</label>
		<input type="text" class="form-control" id="lastname" name="lastname" value="{{ $item->lastname }}" required>
		<div class="text-danger">{{$errors->first('lastname')}}</div>
	</div>
	<div class="form-group">
		<label for="email" class="bmd-label-floating">@Lang('Email')</label>
		<input type="email" class="form-control" id="email" name="email" value="{{ $item->email }}" required>
		<!--<span class="bmd-help">No compartiremos su mail con nadie.</span>-->
		<div class="text-danger">{{$errors->first('email')}}</div>
	</div>
	<div class="form-group">
		<label for="password" class="bmd-label-floating">@Lang('Password')</label>
		<input type="password" class="form-control" id="password" name="password">
		<div class="text-danger">{{$errors->first('password')}}</div>
	</div>
	<div class="form-group">
		<label for="password_confirmation" class="bmd-label-floating">@Lang('Confirm password')</label>
		<input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
	</div>
	<div class="form-group">
		<label for="role" class="bmd-label-floating">@Lang('Role')</label>
		<select class="form-control custom-select" name="role" data-live-search="true" title="@Lang('Role')" required>
			@foreach ($rols as $rol)
			<option value="{{$rol->name}}" {{$rol->name == $item->role ? 'selected' : ''}}>{{__($rol->name)}}</option>
			@endforeach
		</select>
	</div>
	<div class="form-group mt-5">
		<input onclick="window.location.href='{{ route('admin.users.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
		<input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
	</div>
</form>
@endsection