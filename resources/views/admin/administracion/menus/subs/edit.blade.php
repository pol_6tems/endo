@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>
    <i class="material-icons pr-2 admin-menu-icona">menu</i>
        @Lang('Edit'): {{__($item->name)}}
    </h1>
</div>

<div class="panel-body">

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.menus.subs.update', $item->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="menu_id" value="{{$item->menu_id}}"/>

        <div class="form-group">
            <label for="order" class="bmd-label-floating">@Lang('Order')</label>
            <input type="number" class="form-control" name="order" name="order" value="{{ $item->order }}" tabindex="1" min="0">
        </div>
        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $item->name }}" required tabindex="2">
        </div>
        <div class="card form-group">
            <h5 class="card-header bg-light">
                <strong>@Lang('Url')</strong>
            </h5>
            <div class="form-group">
                <div class="form-group">
                    <label for="url_type" class="bmd-label-floating" style="top: 0.4rem;">@Lang('Type')</label>
                    <select class="form-control custom-select" name="url_type" data-live-search="true" title="@Lang('Type')" required tabindex=3>
                        <option value="route" {{$item->url_type == 'route' ? 'selected' : ''}}>ROUTE</option>
                        <option value="url" {{$item->url_type == 'url' ? 'selected' : ''}}>URL</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="url" class="bmd-label-floating">@Lang('Url')</label>
                    <input type="text" class="form-control" id="url" name="url" value="{{ $item->url }}" required tabindex="4">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="controller" class="bmd-label-floating">@Lang('Controller')</label>
            <input type="text" class="form-control" id="controller" name="controller" value="{{ $item->controller }}" tabindex="5">
        </div>
        
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.menus.edit', $item->menu_id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
        </div>
             
        </div>
    </form>
</div>
@endsection