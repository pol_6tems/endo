@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>
        <i class="material-icons pr-2 admin-menu-icona">menu</i>
        @Lang('Submenu'): @Lang('Add new')
    </h1>
</div>

<div class="panel-body">
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.menus.subs.store') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="menu_id" value="{{$id}}"/>

        <div class="form-group">
            <label for="order" class="bmd-label-floating">@Lang('Order')</label>
            <input type="number" class="form-control" name="order" name="order" value="{{ old('order') }}" tabindex="1" min="0">
        </div>
        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required tabindex="2">
        </div>
        <div class="card form-group">
            <h5 class="card-header bg-light">
                <strong>@Lang('Url')</strong>
            </h5>
            <div class="form-group">
                <div class="form-group">
                    <label for="url_type" class="bmd-label-floating" style="top: 0.4rem;">@Lang('Type')</label>
                    <select class="form-control custom-select" name="url_type" data-live-search="true" title="@Lang('Type')" required tabindex=3>
                        <option value="route" selected>ROUTE</option>
                        <option value="url">URL</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="url" class="bmd-label-floating">@Lang('Url')</label>
                    <input type="text" class="form-control" id="url" name="url" value="{{ old('url') }}" required tabindex="4">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="controller" class="bmd-label-floating">@Lang('Controller')</label>
            <input type="text" class="form-control" id="controller" name="controller" value="{{ old('controller') }}" tabindex="5">
        </div>
        
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.menus.edit', $id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" tabindex="6"/>
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" tabindex="7"/>
        </div>
    </form>

</div>
@endsection