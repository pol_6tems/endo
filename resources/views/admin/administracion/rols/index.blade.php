@extends('layouts.admin')

@section('section-title')
    @Lang('Roles')
    <a href="{{ route('admin.rols.create') }}" class="btn btn-default btn-raised bg-white">@Lang('Add new')</a>
@endsection

@section('content')
<div class="card-noconflict p-3">
<div class="">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>@Lang('Name')</th>
                <th class="d-none d-sm-table-cell">@Lang('Title')</th>
                <th class="d-none d-sm-table-cell" width="10%">@Lang('Level')</th>
                <th class="d-none d-sm-table-cell" width="10%">@Lang('Users')</th>
                <th width="5%">@Lang('Actions')</th>
            </tr>
        </thead>
        <tbody>
            @forelse($items as $item)
            <tr>
                <td><a href="{{route('admin.rols.show', $item->id)}}">{{ $item->name }}</a></td>
                <td class="d-none d-sm-table-cell">{{ __($item->name) }}</td>
                <td class="d-none d-sm-table-cell">{{ $item->level }}</td>
                <td class="d-none d-sm-table-cell">{{ count($item->users) }}</td>

                <td class="cela-opcions">
                    <div class="btn-group">
                        <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </button>  
                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
                            <a class="dropdown-item" href="{{ route('admin.rols.edit', $item->id) }}">@Lang('Edit')</a>
                            <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('admin.rols.destroy', $item->id) }}" data-nombre="{{__($item->name)}}">@Lang('Delete')</button>
                        </div>
                    </div>
                </td>
            </tr>
            @empty
                <tr>
                    <td colspan="5">@Lang('No entries found.')</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>
    {{ $items->links() }}
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">Eliminar</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Eliminar</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection	

@section('scripts')
<script>
$('#myModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var url = button.data('url');
    var nombre = button.data('nombre');
    var modal = $(this);
    modal.find('form').attr('action', url);
    modal.find('.modal-body').html("@Lang('Are you sure to delete it?')");
});
$( "tbody" ).disableSelection();
$('tbody').sortable({
    update: function() {
        alert('ok');
    }
});
</script>
@endsection