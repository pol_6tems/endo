@extends('layouts.admin')

@section('section-title')
    @Lang('Roles')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Add new')</h4>
    
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.rols.store') }}" method="post">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required tabindex="1">
        </div>
        <div class="form-group">
            <label for="level" class="bmd-label-floating">@Lang('Level')</label>
            <input type="number" class="form-control" name="level" name="level" value="{{ old('level') }}" tabindex="2" min="0">
        </div>
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.rols.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
        </div>
    </form>

</div>
@endsection