@extends('layouts.admin')

@section('section-title')
    @Lang('Roles')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Edit')</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.rols.update', $item->id) }}" method="post">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        
        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $item->name }}" required tabindex="1">
        </div>
        <div class="form-group">
            <label for="level" class="bmd-label-floating">@Lang('Level')</label>
            <input type="number" class="form-control" name="level" name="level" value="{{ $item->level }}" tabindex="2" min="0">
        </div>
        <div class="form-group">
            <div class="grid permisos">
            @foreach ($controllers as $key => $mets)
                <div>
                <label>{{ $key }}</label>
                <div class="grid ml-4">
                    @foreach ($mets as $met)
                    <div class="checkbox mt-0 ml-1">
                        <label>
                            <input type="checkbox" name="permisos[{{$key}}][{{$met}}]"
                            @if ( !empty($item->get_permisos()[$key][$met]) && $item->get_permisos()[$key][$met] )
                                checked
                            @endif
                            />{{$met}}
                        </label>
                    </div>
                    @endforeach
                </div>
                </div>
            @endforeach
            </div>
        </div>

        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.rols.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
        </div>
             
        </div>
    </form>
</div>
@endsection