@extends('layouts.admin')

@section('section-title')
    @Lang(ucfirst($post_type_plural))
@endsection

@section('content')
<div class="card-noconflict p-3">

    <h4>@Lang('Edit')</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <ul class="nav nav-tabs mt-3">
        @foreach($_languages as $language_item)
            <li class="nav-item {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                <a class="nav-link {{ ($language_item->code == $language_code) ? 'active' : '' }}" data-toggle="tab" href="#{{$language_item->code}}">{{$language_item->code}}</a>
            </li>
        @endforeach
    </ul>
    
    <form action='{{ route("admin.posts.update", $post->id) }}?post_type={{$post_type}}' method="post">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        <input type="hidden" name="status" value="{{ $post->status }}" />
        <input type="hidden" name="type" value="{{ $post->type }}" />
        
        <div class="tab-content">
            @foreach($_languages as $language_item)
            <div id="{{$language_item->code}}" class="tab-pane fade {{ ($language_item->code == $language_code) ? 'active in show' : '' }}">
                
                <div class="form-group">
                    <label for="name" class="bmd-label-floating">@Lang('Title')</label>
                    <input type="text" class="form-control" name="{{$language_item->code}}[title]" value="{{ $post->translate($language_item->code)->title }}" tabindex=1 {{ ($language_item->code == $language_code) ? 'required' : '' }}/>
                </div>
                <div class="form-group">
                    <label for="post_name" class="bmd-label-floating">@Lang('URL')</label>
                    <input type="text" class="form-control" name="{{$language_item->code}}[post_name]" value="{{ $post->translate($language_item->code)->post_name }}" tabindex=2 {{ ($language_item->code == $language_code) ? 'required' : '' }}/>
                </div>
                <div class="form-group is-focused">
                    <label for="description" class="bmd-label-floating">@Lang('Description')</label>
                    <textarea id="description" name="{{$language_item->code}}[description]" class="form-control summernote" tabindex=3>{!! $post->translate($language_item->code)->description !!}</textarea>
                </div>
                <div class="form-group">
                    <label for="template" class="bmd-label-floating">@Lang('Template')</label>
                    <select class="form-control custom-select" name="template" data-live-search="true" title="@Lang('Template')">
                        @foreach ($templates as $template)
                        <option value="{{$template}}" {{$post->template == $template ? 'selected' : ''}}>
                            {{ ucfirst(strtolower($template)) }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4 col-sm-4 mb-5">
                    <h4 class="title">@Lang('Image')</h4>
                    <div class="fileinput {{$post->translate($language_item->code)->media != null && $post->translate($language_item->code)->media->has_thumbnail() ? 'fileinput-exists' : 'fileinput-new'}} text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                            <img src="{{ asset('images/image_placeholder.jpg') }}">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="">
                            @if ( $post->translate($language_item->code)->media != null && $post->translate($language_item->code)->media->has_thumbnail() )
                                <img src="{{ $post->translate($language_item->code)->media->get_thumbnail_url('medium') }}" alt="$post->translate($language_item->code)->media->alt">
                            @endif
                        </div>
                        <div>
                            <span class="btn btn-rose btn-round btn-file">
                            <span class="fileinput-new">@Lang('Select image')</span>
                            <span class="fileinput-exists">@Lang('Change')</span>
                            <input type="hidden"><input type="text" name="media_id" value="{{$post->translate($language_item->code)->media_id}}">
                            <div class="ripple-container"></div></span>
                            <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                                <i class="fa fa-times"></i> @Lang('Delete')
                            </a>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-5" style="clear:both;">
                    <input onclick="window.location.href='{{ route("admin.posts.index", ["post_type" => $post_type]) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
                    <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
                </div>
            </div>
            @endforeach
        </div>
    </form>

    </div>
@endsection

@section('styles')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<style>
    .ui-tooltip { display: none !important; }
</style>
@endsection

@section('scripts')
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<!--<script src="{{asset('js/plugins/jasny-bootstrap.min.js')}}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>  
<script>
$(document).ready(function() {
    $('.summernote').summernote({
        height: 300
    });
    $('.popover').hide();
    /*
    'fileinput-exists' : 'fileinput-new'
    */
    $('.fileinput[data-provides=fileinput] .fileinput-exists[data-dismiss=fileinput]').click(function(){
        $('.fileinput[data-provides=fileinput]').toggleClass('fileinput-exists');
        $('.fileinput[data-provides=fileinput]').toggleClass('fileinput-new');
        $('.fileinput[data-provides=fileinput] .fileinput-preview.fileinput-exists.thumbnail img').remove();
        $('.fileinput[data-provides=fileinput] input[type=text]').val(null);
    });
});
</script>
@endsection