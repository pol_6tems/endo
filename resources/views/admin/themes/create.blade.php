@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Add new')</h1>
</div>

<div class="panel-body">
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.themes.store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required tabindex="1">
            <span class="bmd-help">@Lang('The field has to be unique')</span>
        </div>
        <div class="form-group">
            <label for="path" class="bmd-label-floating">@Lang('Path')</label>
            <input type="text" class="form-control" id="path" name="path" value="{{ old('path') }}" required tabindex="2">
        </div>
        <div class="form-group">
            <label for="thumbnail" class="bmd-label-floating">@Lang('Thumbnail')</label>
            <input class="form-control-file" type="file" name="thumbnail" id="thumbnail" tabindex=3>
        </div>        
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.themes.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary"  tabindex="3"/>
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary"  tabindex="4" />
        </div>
    </form>
</div>
@endsection