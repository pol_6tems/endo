@extends('layouts.admin')

@section('section-title')
    @Lang('Modules')
@endsection

@section('content')
<div class="card-noconflict">
    <div class="panel-heading">
        <h4>@Lang('Edit'): {{ $item->name }}</h4>
    </div>

    <div class="panel-body">

        @if ($errors->count() > 0)
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <form action="{{ route('admin.modules.update', $item->id) }}" method="post">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="name" class="bmd-label-floating">@Lang('Name')</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $item->name }}" required tabindex="1">
            </div>
            <div class="form-group mt-5 mb-3">
                <input onclick="window.location.href='{{ route('admin.modules.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
                <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
            </div>
                
            </div>
        </form>
    </div>
</div>

<div class="card-noconflict">
    <div class="panel-heading">
        <h4>
            @Lang('Translations')
        </h4>
    </div>

    <div class="panel-body">
    <form class="form-inline" method="POST" action="{{ route('admin.modules.translations.create', $item->id) }}">
        {!! csrf_field() !!}
        <div class="form-group mr-4">
            <label for="key" class="bmd-label-floating">Key:</label>
            <input type="text" name="key" class="form-control" placeholder="">
        </div>
        <div class="form-group mr-4">
            <label for="value" class="bmd-label-floating">Value</label>
            <input type="text" name="value" class="form-control" placeholder="">
        </div>
        <span class="form-group bmd-form-group"> <!-- needed to match padding for floating labels -->
            <button type="submit" class="btn btn-default btn-raised">Add</button>
        </span>
    </form>

    <ul class="nav nav-tabs">
        <li class="nav-item active">
            <a class="nav-link active" data-toggle="tab" href="#traducciones">@Lang('Translations')</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#rutas">@Lang('Routes')</a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="traducciones" class="tab-pane active show">
            <table id="tabla-traducciones" class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Key</th>
                    @if($languages->count() > 0)
                        @foreach($languages as $language)
                            <th>{{ __($language->name) }}({{ $language->code }})</th>
                        @endforeach
                    @endif
                    <th width="80px;">@Lang('Action')</th>
                </tr>
                </thead>
                <tbody>
                    @if($columnsCount > 0)
                        @foreach($columns[0] as $columnKey => $columnValue)
                            @if ( substr( $columnKey, 0, 1 ) !== "/" )
                            <tr>
                                <td><a href="#" class="translate-key" data-title="Enter Key" data-type="text" data-pk="{{ $columnKey }}" data-url="{{ route('admin.modules.translations.update.json.key', $item->id) }}">{{ $columnKey }}</a></td>
                                @for($i=1; $i<=$columnsCount; ++$i)
                                <td><a href="#" data-title="@Lang('Enter Translate')" class="translate" data-code="{{ $columns[$i]['lang'] }}" data-type="textarea" data-pk="{{ $columnKey }}" data-url="{{ route('admin.modules.translations.update.json', $item->id) }}">{{ isset($columns[$i]['data'][$columnKey]) ? $columns[$i]['data'][$columnKey] : '' }}</a></td>
                                @endfor
                                <td><button data-action="{{ route('admin.modules.translations.destroy', [$item->id, base64_encode($columnKey)]) }}" class="btn btn-danger btn-xs remove-key">@Lang('Destroy')</button></td>
                            </tr>
                            @endif 
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>

        <div id="rutas" class="tab-pane fade">
            <table id="tabla-rutas" class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Key</th>
                    @if($languages->count() > 0)
                        @foreach($languages as $language)
                            <th>{{ __($language->name) }}({{ $language->code }})</th>
                        @endforeach
                    @endif
                    <th width="80px;">@Lang('Action')</th>
                </tr>
                </thead>
                <tbody>
                    @if($columnsCount > 0)
                        @foreach($columns[0] as $columnKey => $columnValue)
                            @if ( substr( $columnKey, 0, 1 ) === "/" )
                            <tr>
                                <td><a href="#" class="translate-key" data-title="Enter Key" data-type="text" data-pk="{{ $columnKey }}" data-url="{{ route('admin.modules.translations.update.json.key', $item->id) }}">{{ $columnKey }}</a></td>
                                @for($i=1; $i<=$columnsCount; ++$i)
                                <td><a href="#" data-title="@Lang('Enter Translate')" class="translate" data-code="{{ $columns[$i]['lang'] }}" data-type="textarea" data-pk="{{ $columnKey }}" data-url="{{ route('admin.modules.translations.update.json', $item->id) }}">{{ isset($columns[$i]['data'][$columnKey]) ? $columns[$i]['data'][$columnKey] : '' }}</a></td>
                                @endfor
                                <td><button data-action="{{ route('admin.modules.translations.destroy', [$item->id, base64_encode($columnKey)]) }}" class="btn btn-danger btn-xs remove-key">@Lang('Destroy')</button></td>
                            </tr>
                            @endif
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    </div>
</div>

@endsection

@section('styles')
<link href="{{asset('css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<style>
    .navbar {
        margin-bottom: 0;
        border-radius: 0px;
        border: none;
        min-height: inherit;
    }
    .bmd-layout-content a {
        color: #666;
    }
    .row { width: 100% !important; }
</style>
@endsection

@section('scripts')
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-editable.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.translate').editable({
        params: function(params) {
            params.code = $(this).editable().data('code');
            return params;
        }
    });


    $('.translate-key').editable({
        validate: function(value) {
            if($.trim(value) == '') {
                return 'Key is required';
            }
        }
    });


    $('body').on('click', '.remove-key', function(){
        var cObj = $(this);


        if ( confirm("@Lang('Are you sure to delete it?')") ) {
            $.ajax({
                url: cObj.data('action'),
                method: 'DELETE',
                success: function(data) {
                    cObj.parents("tr").remove();
                    $.snackbar({content: "@Lang('Translation deleted')", timeout: 3000});
                }
            });
        }
    });

    $(document).ready(function() {
        $('#tabla-traducciones').DataTable({
			"pageLength": 25,
			"language": { "url": "{{asset('js/datatables/Catalan.json')}}" }
        });
        $('#tabla-rutas').DataTable({
			"pageLength": 25,
			"language": { "url": "{{asset('js/datatables/Catalan.json')}}" }
		});
    });
</script>
@endsection