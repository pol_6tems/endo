@extends('layouts.admin')

@section('section-title')
	@Lang('Media')
@endsection

@section('content')
<div class="card-noconflict pl-3 pt-3">
	<div class="panel-heading">
		<h3>@Lang('Add new')</h3>
	</div>
	<div class="panel-body">
		<form method="POST" action="{{route('admin.media.store')}}" enctype="multipart/form-data">
			{!! csrf_field() !!}
			<div class="form-row">
				<div class="form-group col-md-12">
					<label for="title" class="bmd-label-floating">@Lang('Title')</label>
					<input type="text" class="form-control" name="title" value="{{ old('title') }}" required tabindex=1>
					<div class="text-danger">{{$errors->first('title')}}</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-5">
					<label for="legend" class="bmd-label-floating">@Lang('Legend')</label>
					<input type="text" class="form-control" name="legend" value="{{ old('legend') }}" tabindex=3>
					<div class="text-danger">{{$errors->first('legend')}}</div>
				</div>
				<div class="form-group col-md-5 col-md-offset-1">
					<label for="alt" class="bmd-label-floating">@Lang('Alternative text')</label>
					<input type="text" class="form-control" name="alt" value="{{ old('alt') }}" tabindex=4>
					<div class="text-danger">{{$errors->first('alt')}}</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<label for="description" class="bmd-label-floating">@Lang('Description')</label>
					<textarea class="form-control" name="description" rows=5 tabindex=5>{{ old('description') }}</textarea>
					<div class="text-danger">{{$errors->first('description')}}</div>
				</div>
			</div>
			<div class="form-group col-md-4 col-sm-4">
				<h4 class="title">@Lang('Thumbnail')</h4>
				<div class="fileinput fileinput-new text-center" data-provides="fileinput">
					<div class="fileinput-new thumbnail">
						<img src="{{ asset('images/image_placeholder.jpg') }}">
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="">
					</div>
					<div>
						<span class="btn btn-rose btn-round btn-file">
						<span class="fileinput-new">@Lang('Select image')</span>
						<span class="fileinput-exists">@Lang('Change')</span>
						<input type="hidden"><input type="file" name="file">
						<div class="ripple-container"></div></span>
						<a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
							<i class="fa fa-times"></i> @Lang('Delete')
						</a>
					</div>
				</div>
			</div>
			<div class="form-group col-md-12 mt-5 mb-3">
				<button type="button" class="btn btn-default" onclick="window.location.href='{{route('admin.media.index')}}'">@Lang('Cancel')</button>
				<button type="submit" class="btn btn-primary btn-raised">@Lang('Create')</button>
			</div>
		</form>
	</div>
</div>
@endsection

@section('footer')
@endsection

@section('styles')
@endsection

@section('scripts')
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{asset('js/plugins/jasny-bootstrap.min.js')}}"></script>
@endsection