@extends('layouts.admin')

@section('section-title')
    @Lang(ucfirst($post_type_plural))
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Edit')</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <ul class="nav nav-tabs">
        @foreach($_languages as $language_item)
            <li class="nav-item {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                <a class="nav-link {{ ($language_item->code == $language_code) ? 'active' : '' }}" data-toggle="tab" href="#{{$language_item->code}}">{{$language_item->code}}</a>
            </li>
        @endforeach
    </ul>
    
    <form action='{{ route("admin.$post_type_plural.update", $post->id) }}' method="post">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        <input type="hidden" name="status" value="{{ $post->status }}" />
        <input type="hidden" name="type" value="{{ $post->type }}" />
        
        <div class="tab-content">
            @foreach($_languages as $language_item)
            <div id="{{$language_item->code}}" class="tab-pane fade {{ ($language_item->code == $language_code) ? 'active in show' : '' }}">
                
                <div class="form-group">
                    <label for="name" class="bmd-label-floating">@Lang('Title')</label>
                    <input type="text" class="form-control" name="{{$language_item->code}}[title]" value="{{ $post->translate($language_item->code)->title }}" tabindex=1 {{ ($language_item->code == $language_code) ? 'required' : '' }}/>
                </div>
                <div class="form-group">
                    <label for="post_name" class="bmd-label-floating">@Lang('URL')</label>
                    <input type="text" class="form-control" name="{{$language_item->code}}[post_name]" value="{{ $post->translate($language_item->code)->post_name }}" tabindex=2 {{ ($language_item->code == $language_code) ? 'required' : '' }}/>
                </div>
                <div class="form-group is-focused">
                    <label for="description" class="bmd-label-floating">@Lang('Description')</label>
                    <textarea id="description" name="{{$language_item->code}}[description]" class="form-control summernote" tabindex=3>{!! $post->translate($language_item->code)->description !!}</textarea>
                </div>
                <div class="form-group">
                    <label for="template" class="bmd-label-floating">@Lang('Template')</label>
                    <select class="form-control custom-select" name="template" data-live-search="true" title="@Lang('Template')">
                        @foreach ($templates as $template)
                        <option value="{{$template}}" {{$post->template == $template ? 'selected' : ''}}>
                            {{ ucfirst(strtolower($template)) }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mt-5">
                    <input onclick="window.location.href='{{ route("admin.$post_type_plural.index") }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
                    <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
                </div>
                    
            </div>
                
            @endforeach
        </div>
    </form>

    </div>
@endsection

@section('styles')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>  
<script>
    $(document).ready(function() {
        $('.summernote').summernote({
            height: 300
        });
        $('.popover').hide();
    });
</script>
@endsection