<div class="form-group">
    <label for="type" class="bmd-label-floating">@Lang('Type')</label>
    <select onchange="show_type_form(this.value);" class="form-control custom-select" name="type" data-live-search="true" title="@Lang('Type')" required>
        @foreach ($tipus_fields as $tf_name => $tf)
        <option value="{{$tf_name}}">
            {{ ucfirst(strtolower($tf_name)) }}
        </option>
        @endforeach
    </select>
</div>
<div class="form-group camps">
    <label for="title" class="bmd-label-floating">@Lang('Title')</label>
    <input type="text" class="form-control" name="title" value="{{ old('title') }}" required>
</div>
<div class="form-group camps">
    <label for="name" class="bmd-label-floating">@Lang('Name')</label>
    <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
</div>
<div class="form-group camps">
    <label for="instructions" class="bmd-label-floating">@Lang('Instructions')</label>
    <input type="text" class="form-control" name="instructions" value="{{ old('instructions') }}">
</div>

<style>
    form#field_create .form-group.camps { display: none; }
</style>

<script>
    var $tipus_fields = {!! json_encode($tipus_fields) !!};
    function show_type_form(tipus) {
        $.each($tipus_fields[tipus].fields, function(name, type) {
            $('form#field_create .modal-body').append(`
                <div class="form-group camps camps-opcionals">
                    <label for="`+name+`" class="bmd-label-floating">`+name+`</label>
                    <input type="`+type+`" class="form-control" name="`+name+`">
                </div>
            `);
        });
        //$('form#field_create .form-group.camps').show("slide", { direction: "left" }, 500);
        $('form#field_create .form-group.camps').show("slide", { direction: "up" }, 500);
    }
</script>