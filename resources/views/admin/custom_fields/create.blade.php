@extends('layouts.admin')

@section('section-title')
    @Lang('Custom Fields') @Lang('Groups')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Add new')</h4>
    
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.custom_fields.store') }}" method="post">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="post_type" class="bmd-label-floating">@Lang('Type')</label>
            <input type="text" class="form-control" id="post_type" name="post_type" value="{{ old('post_type') }}" required tabindex=1>
        </div>
        <div class="form-group">
            <label for="title" class="bmd-label-floating">@Lang('Title')</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" required tabindex=2>
        </div>
        <div class="form-group">
            <label for="template" class="bmd-label-floating">@Lang('Template')</label>
            <select class="form-control custom-select" name="template" data-live-search="true" title="@Lang('Template')" tabindex=3>
                <option value="">@Lang('None')</option>    
                @foreach ($templates as $template)
                <option value="{{$template}}">
                    {{ ucfirst(strtolower($template)) }}
                </option>
                @endforeach
            </select>
        </div>
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.custom_fields.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
        </div>
    </form>

</div>
@endsection