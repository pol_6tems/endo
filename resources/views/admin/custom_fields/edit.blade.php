@extends('layouts.admin')

@section('section-title')
    @Lang('Custom Fields') @Lang('Groups')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Edit'): {{ $item->title }}</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.custom_fields.update', $item->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field("PUT")}}

        <div class="form-group">
            <label for="post_type" class="bmd-label-floating">@Lang('Type')</label>
            <input type="text" class="form-control" id="post_type" name="post_type" value="{{ $item->post_type }}" required tabindex=1>
        </div>
        <div class="form-group">
            <label for="title" class="bmd-label-floating">@Lang('Title')</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $item->title }}" required tabindex=2>
        </div>
        <div class="form-group">
            <label for="template" class="bmd-label-floating">@Lang('Template')</label>
            <select class="form-control custom-select" name="template" data-live-search="true" title="@Lang('Template')" tabindex=4>
                <option value="" {{ empty($item->template) ? 'selected' : '' }}>@Lang('None')</option>
                @foreach ($templates as $template)
                <option value="{{$template}}" {{ $template == $item->template ? 'selected' : '' }}>
                    {{ ucfirst(strtolower($template)) }}
                </option>
                @endforeach
            </select>
        </div>
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.custom_fields.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
        </div>
    </div>

    <div class="card-noconflict p-3 mt-5">
        <h4 class="mb-3">
            @Lang('Fields')
            <a onclick="add_field_form_row();return false;" href="#" class="btn btn-secondary btn-raised">
                @Lang('Add')
            </a>
        </h4>
        <div class="">
            <table id="custom_fields" class="table table-hover">
                <thead>
                    <tr>
                        <th width="5%">@Lang('Order')</th>
                        <th>@Lang('Title')</th>
                        <th width="30%">@Lang('Name')</th>
                        <th width="20%">@Lang('Type')</th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="field-form-row-0" style="display: none;" onclick="$(this).next().toggle();">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="field-form-row-1" class="field-form-row-header" style="display: none;">
                    <td colspan="4" class="field-form-row">
                        <div class="form-group">
                            <label for="type" class="bmd-label-floating">@Lang('Type')</label>
                            <select onchange="show_type_form(this.value);" class="form-control custom-select" name="type" data-live-search="true" title="@Lang('Type')" required>
                                @foreach ($tipus_fields as $tf_name => $tf)
                                <option value="{{$tf_name}}">
                                    {{ ucfirst(strtolower($tf_name)) }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group camps">
                            <label for="title" class="bmd-label-floating">@Lang('Title')</label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') }}" required>
                        </div>
                        <div class="form-group camps">
                            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                        </div>
                        <div class="form-group camps">
                            <label for="instructions" class="bmd-label-floating">@Lang('Instructions')</label>
                            <input type="text" class="form-control" name="instructions" value="{{ old('instructions') }}">
                        </div>
                    </td>
                    </tr>
                @foreach($item->fields as $subitem)
                    <tr>
                        <td>{{ $subitem->order }}</td>
                        <td>{{ $subitem->title }}</td>
                        <td>{{ $subitem->name }}</td>
                        <td>{{ $subitem->type }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </form>

</div>

<!-- Modal Field Create -->
<div class="modal fade bd-example-modal-lg" id="modal-field-create" style="background: #444;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">@Lang('Add') {{strtolower(__('Field'))}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="field_create">
                <div class="modal-body">
                    @include('admin.custom_fields.subform')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
                    <button type="submit" class="btn btn-primary">@Lang('Add')</button>
                </div>
            </form>
		</div>
	</div>
</div>
@endsection

@section('styles')
<style>
    .field-form-row-header:hover { cursor: default !important; }
    .field-form-row-header { background-color: transparent !important; }
</style>
@endsection

@section('scripts')
<script>
    var $fields = {!! json_encode($item->fields) !!};
    $('form#field_create').submit(function(e) {
        var values = {};
        $.each($(this).serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });
        if ( $fields.length == 0 ) $('table#custom_fields tbody tr').remove();
        values['order'] = $('table#custom_fields tbody tr').length;
        $fields.push(values);
        $('table#custom_fields tbody').append(`
        <tr>
            <td>`+values['order']+`</td>
            <td>`+values['title']+`</td>
            <td>`+values['name']+`</td>
            <td>`+values['type']+`</td>
            <td class="cela-opcions">
                
            </td>
        </tr>`);
        $('form#field_create input').val('');
        $('form#field_create .modal-body .camps-opcionals').remove();
        $('#modal-field-create').modal('hide');
        e.preventDefault();
        return false;
    });
    function add_field_form_row() {
        var copy0 = $('#field-form-row-0').clone();
        copy0.id = "";
        copy0.appendTo("table#custom_fields tbody");
        var copy1 = $('#field-form-row-1').clone();
        copy1.id = "";
        copy1.appendTo("table#custom_fields tbody");
        copy0.show();
        copy1.show();
        //copy0.show("slide", { direction: "up" }, 500);
        //copy1.show("slide", { direction: "up" }, 500);
    }
</script>
@endsection