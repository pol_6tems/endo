<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield("title", config('app.name', 'Endo'))</title>
	
	<link href="{{asset('css/bootstrap/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/bootstrap/css/bootstrap-editable.css')}}" rel="stylesheet" type="text/css">
	@yield("styles_before")
	
    <!-- Styles -->
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Material Design for Bootstrap CSS -->
	<link href="{{asset('js/snackbar/snackbar.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('js/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/bootstrap-material-design.min.css')}}" rel="stylesheet" type="text/css">
	
	@yield("styles")
	
	<link href="{{asset('css/endo-admin.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/material-dashboard.css')}}" rel="stylesheet" type="text/css">
	
	<style>
		.navbar-brand i, .navbar-brand span { vertical-align: middle; }
	</style>
	
	
</head>
<body>

	<div class="bmd-layout-container bmd-drawer-f-l border-bottom box-shadow bmd-drawer-in">
		<header class="bmd-layout-header minimized">
			<div class="navbar navbar-light bg-faded capsalera-menu">
				<button class="btn bmd-btn-fab bmd-btn-fab-sm navbar-toggler bg-white" type="button" onclick="minimize_sidebar();">
					<span class="sr-only">Toggle drawer</span>
					<i class="material-icons boto-menu-admin">menu</i>
				</button>
				<h3 class="ml-3" style="font-weight: lighter;">@yield('section-title')</h3>
				<a class="btn ml-md-auto" style="margin-bottom: 0;" href="{{route('index')}}">@Lang('Web')</a>
				<div class="btn-group selector-idiomas">
					<button class="btn dropdown-toggle" type="button" id="languages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						@Lang( $language_name )
					</button>
					<div class="dropdown-menu" aria-labelledby="languages">
						@foreach ($_languages as $language)
							<a class="dropdown-item" href="{{ $language->url }}">@Lang($language->name)</a>
						@endforeach
					</div>
				</div>
				<div class="dropdown pull-xs-right">
					<button class="btn bmd-btn-icon dropdown-toggle" type="button" id="lr1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="material-icons">person</i>
					</button>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="lr1" style="left: -128px;">
						<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
							@Lang('Logout')	
						</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							{{ csrf_field() }}
						</form>
					</div>
				</div>
			</div>
		</header>
		<div id="dw-s1" class="bmd-layout-drawer minimized" aria-expanded="true" aria-hidden="false" onmouseover="sidebar_hover(true);" onmouseout="sidebar_hover(true);">
			
			<div class="sidebar-logo">
				<div class="title-admin">
					<h5>{{ config('app.name', 'BATALLE') }}</h5>
				</div>
			</div>

			@include('partials.menu')
		</div>
		
		<main class="bmd-layout-content minimized">
			<!--
			<div class="header">
				<div class="container">
					@yield("header")
				</div>
			</div>
			-->

			<div class="container">
				{{--@if (Session::has('message'))
					<div class="alert alert-success alert-dismissible" role="alert">
						@lang(Session::get('message'))
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="alert alert-danger alert-dismissible" role="alert">
						@lang(Session::get('error'))
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif--}}
				<div class="content mb-5">
					@yield("content")
				</div>
			</div>


			<footer class="footer">
				<div class="container-fluid">
					<nav class="float-left">
						<ul>
							<li>
								<a href="http://www.6tems.com/es/index.html" target="_blank">
									6TEMS
								</a>
							</li>
							<li>
								<a href="http://www.6tems.com/es/index.html" target="_blank">
									@Lang('About Us')
								</a>
							</li>
						</ul>
					</nav>
					<div class="copyright float-right">
						©
						<script>
						document.write(new Date().getFullYear())
						</script>, @Lang('made by')&nbsp;
						<a href="http://www.6tems.com/es/index.html" target="_blank"><img style="width: 30px;" src="{{asset('images/6tems_round_50.png')}}"></a>.
					</div>
				</div>
			</footer>

		</main>

	</div>

	@yield("modals")
	
	<footer>
		@yield("footer")
	</footer>	
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.js') }}"></script>
    <script src="{{ asset('js/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-design.js') }}"></script>
	<script src="{{ asset('js/snackbar/snackbar.min.js') }}"></script>
	<script src="{{ asset('js/jquery-ui-1.12.1.js') }}"></script>
	<script>
	var is_minimized_sidebar = true;
	$(document).ready(function() {
		$('body').bootstrapMaterialDesign();
		$('select').selectpicker();
		@if (Session::has('message'))
			$.snackbar({content: "@Lang(Session::get('message'))", timeout: 3000});
		@endif
		@if (Session::has('error'))
			$.snackbar({content: "@Lang(Session::get('error'))", timeout: 3000});
		@endif
	});
	function minimize_sidebar(isover = false) {
		if ( !isover || (isover && $('.bmd-drawer-f-l > .bmd-layout-drawer').hasClass('minimized')) ) {
			$('.bmd-drawer-f-l > .bmd-layout-drawer').toggleClass('minimized');
			$('.bmd-drawer-in .bmd-layout-content').toggleClass('minimized');
			$('.bmd-drawer-in.bmd-drawer-f-l > .bmd-layout-header').toggleClass('minimized');
			is_minimized_sidebar = $('.bmd-drawer-f-l > .bmd-layout-drawer').hasClass('minimized');
		}
	}
	function sidebar_hover(isover = false) {
		if ( is_minimized_sidebar ) $('.bmd-drawer-f-l > .bmd-layout-drawer').toggleClass('minimized');
	}
	</script>
	
	@yield("scripts")
	
</body>
</html>
