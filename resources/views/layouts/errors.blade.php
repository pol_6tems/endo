<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield("title", config('app.name', 'Endo'))</title>

    <!-- Styles -->
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
	
	<!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <!-- Material Design for Bootstrap CSS -->
    <link href="{{asset('css/bootstrap-material-design.min.css')}}" rel="stylesheet" type="text/css">
	
	<link href="{{asset('css/endo.css')}}" rel="stylesheet" type="text/css">

	@yield('styles')
	
</head>
<body>

	<main class="col-12 col-md-12 py-md-3 pl-md-5" role="main">
	
        <div class="header">
			@yield("header")
		</div>

		<div class="content">
			@yield("content")
		</div>
		
    </main>
	
	
</body>
</html>
