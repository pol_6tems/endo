<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield("title", config('app.name', 'Endo'))</title>

    <!-- Styles -->
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
	
	<!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <!-- Material Design for Bootstrap CSS -->
    <link href="{{asset('css/bootstrap-material-design.min.css')}}" rel="stylesheet" type="text/css">
	
	<link href="{{asset('css/endo.css')}}" rel="stylesheet" type="text/css">

	@yield('styles')
	
</head>
<body>

	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a class="nav-link {{ Request::segment(1) == '' ? 'active' : '' }}" href="{{ route('index') }}">
				<strong>{{ config('app.name', 'Endo') }}</strong>
			</a>
		</li>
		
		@if ( Auth::check() )
			@if ( Auth::user()->role == 'admin' )
				<li class="nav-item">
					<a class="nav-link" href="{{route('admin')}}">Panel de Administrador</a>
				</li>
			@endif
		@endif
		
		@if (Auth::guest())
			<li class="nav-item"><a class="nav-link {{ Request::segment(1) == 'login' ? 'active' : '' }}" href="{{ route('login') }}">Login</a></li>
			<li class="nav-item"><a class="nav-link {{ Request::segment(1) == 'register' ? 'active' : '' }}" href="{{ route('register') }}">Register</a></li>
		@else
		
			<div class="btn-group" role="group" style="margin-left: auto;">
				<button class="btn dropdown-toggle" type="button" id="buttonMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					{{ Auth::user()->name }} <span class="caret"></span>
				</button>
				<div class="dropdown-menu" aria-labelledby="buttonMenu1">
					<a class="dropdown-item" href="{{ route('logout') }}"
						onclick="event.preventDefault();
						document.getElementById('logout-form').submit();">
						Logout
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</div>
			</div>
		@endif
	</ul>

    <main class="col-12 col-md-12 py-md-3 pl-md-5" role="main">
	
        <div class="header">
			@yield("header")
		</div>

		<div class="content">
			@yield("content")
		</div>
		
    </main>
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-design.js') }}"></script>
    <script src="{{ asset('js/jquery-ui-1.12.1.js') }}"></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	
	@yield('scripts')
	
</body>
</html>
