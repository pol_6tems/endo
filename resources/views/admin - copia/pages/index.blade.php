@extends('layouts.admin')

@section('section-title')
    @Lang(ucfirst($post_type_plural))
    <a href='{{ route("admin.$post_type_plural.create") }}' class="btn btn-default btn-raised bg-white">@Lang('Add')</a>
@endsection

@section('content')
<ul class="nav nav-tabs">
    <li class="nav-item active card-noconflict">
        <a class="nav-link active" data-toggle="tab" href="#posts" style="margin:0;">@Lang(ucfirst($post_type))</a>
    </li>
    <li class="nav-item card-noconflict">
        <a class="nav-link" data-toggle="tab" href="#trash" style="margin:0;">@Lang('Trash')</a>
    </li>
</ul>

<div class="tab-content">
    <div id="posts" class="tab-pane active show">
    <div class="card">
        <table class="table table-hover">
            <thead>
                <tr>
                    @if ( count($items) > 0 )
                        <th width="5%">
                            <div class="checkbox m-0 check_petit">
                                <label>
                                    <input type="checkbox" class="checkbox_all">
                                </label>
                            </div>
                        </th>
                    @endif
                    <th>@Lang('Title')</th>
                    <th width="30%">@Lang('URL')</th>
                    <th width="15%">@Lang('Template')</th>
                    <th width="15%">@Lang('Updated')</th>
                    <th width="5%">@Lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @forelse($items as $item)
                <tr>
                    <td>
                        <div class="checkbox m-0 check_petit">
                            <label>
                                <input type="checkbox" class="checkbox_delete" name="entries_to_delete[]" value="{{ $item->id }}" />
                            </label>
                        </div>
                    </td>
                    
                    <td>
                        <a href="{{ $item->get_url() }}" target="_blank">
                            {{ $item->title }}
                        </a>
                    </td>
                    <td>{{ $item->post_name }}</td>
                    <td>{{ $item->template }}</td>
                    <td>
                        {{ $item->updated_at->format('d/m/Y') . ' ' . $item->updated_at->format('h:i') }}
                    </td>

                    <td class="cela-opcions">
                        <div class="btn-group">
                            <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </button>  
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
                                <a class="dropdown-item" href='{{ route("admin.$post_type_plural.edit", $item->id) }}'>@Lang('Edit')</a>
                                <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-text="@Lang('Are you sure to delete it?')" data-url='{{ route("admin.$post_type_plural.destroy", $item->id) }}' data-nombre="{{$item->title}}" data-method="DELETE" data-titulo="@Lang('Delete')">@Lang('Delete')</button>
                            </div>
                        </div>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="6">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    
    {{ $items->links() }}

    @if ( count($items) > 0 )
        <form class="mt-5" action='{{ route("admin.$post_type_plural.mass_destroy") }}' method="post" onsubmit="return confirm('Are you sure?');">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="ids" id="ids" value="" />
            <input type="submit" value="@Lang('Delete selected')" class="btn btn-danger" />
        </form>
    @endif

    </div>

    <div id="trash" class="tab-pane fade">
    <div class="card">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>@Lang('Title')</th>
                    <th width="30%">@Lang('URL')</th>
                    <th width="15%">@Lang('Template')</th>
                    <th width="15%">@Lang('Deleted')</th>
                    <th width="5%">@Lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @forelse($items_trash as $item)
                <tr>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->post_name }}</td>
                    <td>{{ $item->template }}</td>
                    <td>
                        {{ $item->deleted_at->format('d/m/Y') . ' ' . $item->deleted_at->format('h:i') }}
                    </td>
                    <td class="cela-opcions">
                        <div class="btn-group">
                            <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </button>  
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
                                <a class="dropdown-item" href='{{ route("admin.$post_type_plural.edit", $item->id) }}'>@Lang('Edit')</a>
                                <button type="button" class="dropdown-item" data-toggle="modal" data-target="#myModal" data-text="@Lang('Are you sure to restore it?')" data-url='{{ route("admin.$post_type_plural.restore", $item->id) }}' data-method="PUT" data-nombre="{{$item->title}}" data-titulo="@Lang('Restore')">@Lang('Restore')</button>
                                <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-text="@Lang('Are you sure to delete it?')" data-url='{{ route("admin.$post_type_plural.destroy_permanent", $item->id) }}' data-nombre="{{$item->title}}" data-method="DELETE" data-titulo="@Lang('Delete permanently')">@Lang('Delete permanently')</button>
                            </div>
                        </div>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="6">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    {{ $items_trash->links() }}
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" id="_method" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Attention')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Accept')</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('styles')
<style>
 
</style>
@endsection

@section('scripts')
<script>
   $('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var url = button.data('url');
		var titulo = button.data('titulo');
		var nombre = button.data('nombre');
		var texto = button.data('text');
		var method = button.data('method');
		var modal = $(this);
		modal.find('form').attr('action', url);
		$('#_method').val(method);
		modal.find('.modal-body').html( texto );
		modal.find('.modal-title').html( titulo );
	});
</script>
<script>
    function getIDs() {
        var ids = [];
        $('.checkbox_delete').each(function () {
            if($(this).is(":checked")) {
                ids.push($(this).val());
            }
        });
        $('#ids').val(ids.join());
    }

    $(".checkbox_all").click(function(){
        $('input.checkbox_delete').prop('checked', this.checked);
        getIDs();
    });

    $('.checkbox_delete').change(function() {
        getIDs();
    });
</script>
@endsection