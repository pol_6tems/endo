@extends('layouts.admin')

@section('section-title')
    @Lang('Custom Posts')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Add new')</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <ul class="nav nav-tabs mt-3">
        @foreach($_languages as $language_item)
            <li class="nav-item {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                <a class="nav-link {{ ($language_item->code == $language_code) ? 'active' : '' }}" data-toggle="tab" href="#{{$language_item->code}}">{{$language_item->code}}</a>
            </li>
        @endforeach
    </ul>
    
    <form action="{{ route('admin.custom_posts.store') }}" method="post">
        {{ csrf_field() }}
        
        <div class="tab-content">
            @foreach($_languages as $language_item)
            <div id="{{$language_item->code}}" class="tab-pane fade {{ ($language_item->code == $language_code) ? 'active in show' : '' }}">
                
                <div class="form-group">
                    <label for="post_type" class="bmd-label-floating">@Lang('Type')</label>
                    <input type="text" class="form-control" name="{{$language_item->code}}[post_type]" value="{{ old('post_type') }}" required tabindex=1>
                </div>
                <div class="form-group">
                    <label for="title" class="bmd-label-floating">@Lang('Title')</label>
                    <input type="text" class="form-control" name="{{$language_item->code}}[title]" value="{{ old('title') }}" required tabindex=2>
                </div>
                <div class="form-group">
                    <label for="post_type_plural" class="bmd-label-floating">@Lang('Type plural')</label>
                    <input type="text" class="form-control" name="{{$language_item->code}}[post_type_plural]" value="{{ old('post_type_plural') }}" required tabindex=3>
                </div>
                <div class="form-group">
                    <label for="title_plural" class="bmd-label-floating">@Lang('Title plural')</label>
                    <input type="text" class="form-control" name="{{$language_item->code}}[title_plural]" value="{{ old('title_plural') }}" required tabindex=4>
                </div>
                <div class="form-group mt-5">
                    <input onclick="window.location.href='{{ route('admin.custom_posts.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
                    <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
                </div>
                    
            </div>
                
            @endforeach
        </div>
    </form>

    </div>
@endsection