@extends('layouts.admin')

@section('section-title')
	@Lang('Custom Posts')
	<a href="{{ route('admin.custom_posts.create') }}" class="btn btn-default btn-raised bg-white">
		@Lang('Add')
	</a>
@endsection

@section('content')
<div class="card" style="clear:both;">
<table id="taula" class="table table-hover table-sortable">
	<thead>
		<tr>
            @if ( count($items) > 0 )
                <th width="5%">
                    <div class="checkbox m-0 check_petit">
                        <label>
                            <input type="checkbox" class="checkbox_all">
                        </label>
                    </div>
                </th>
            @endif
			<th width="10%" class="sortable" data-sortby="post_type_plural" scope="col">@Lang('Type')</th>
			<th class="sortable" data-sortby="title_plural" scope="col">@Lang('Title')</th>
			<th width="15%" class="sortable" data-sortby="updated_at" scope="col">@Lang('Updated at')</th>
			<th width="5%" scope="col">@Lang('Actions')</th>
		</tr>
	</thead>
	<tbody>
	
	@forelse($items as $item)
		<tr>
            <td>
                <div class="checkbox m-0 check_petit">
                    <label>
                        <input type="checkbox" class="checkbox_delete" name="entries_to_delete[]" value="{{ $item->id }}" />
                    </label>
                </div>
            </td>
			<td>{{ $item->post_type }}</td>
			<td>
				{{ $item->title }}
			</td>
            <td>{{ $item->updated_at->format('d/m/Y') . ' ' . $item->updated_at->format('h:i') }}</td>
			<td class="cela-opcions">
				<div class="btn-group">
					<button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="material-icons">more_vert</i>
					</button>  
					<div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
						<a class="dropdown-item" href="{{route('admin.custom_posts.edit', $item->id)}}">@Lang('Edit')</a>
						<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-id="{{$item->id}}" data-nombre="{{$item->title}}" data-url="{{route('admin.custom_posts.destroy', $item->id)}}">@Lang('Delete')</button>
					</div>
				</div>
			</td>
		</tr>
	@empty
		<tr>
			<td colspan="5">@Lang('No entries found.')</td>
		</tr>
	@endforelse
	
	</tbody>
</table>
</div>
{{ $items->links() }}

@if ( count($items) > 0 )
	<form class="mt-3" action="{{ route('admin.custom_posts.mass_destroy') }}" method="post" onsubmit="return confirm('Are you sure?');">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="DELETE">
		<input type="hidden" name="ids" id="ids" value="" />
		<input type="submit" value="@Lang('Delete selected')" class="btn btn-danger" />
	</form>
@endif

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('footer')
@endsection

@section('styles')
@endsection

@section('scripts')
<script src="{{ asset('js/column-sortable.js') }}"></script>
<script>
	{{--var $sortby = '{{ $sortby }}';
	var $order = '{{ $order }}';
	var $search = '{{ $search }}';--}}
	
    $('.eliminar-item').click(function(e){
        e.preventDefault() // Don't post the form, unless confirmed
        if (confirm('¿Estás seguro de eliminarlo?')) {
            $(e.target).closest('form').submit();
        }
    });
	
	$('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var id = button.data('id');
		var nombre = button.data('nombre');
		var url = button.data('url');
		var modal = $(this);
		modal.find('form').attr('action', url);
		modal.find('.modal-body').html("¿Seguro que quieres eliminar a <strong>" + nombre + "?</strong>");
	});
</script>
@endsection