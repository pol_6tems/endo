@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Add new')</h1>
</div>

<div class="panel-body">
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.modules.store') }}" method="post">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required tabindex="1">
        </div>        
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.modules.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary"  tabindex="3"/>
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary"  tabindex="4" />
        </div>
    </form>
</div>
@endsection