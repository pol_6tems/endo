@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>
        @Lang('Themes')
        <a href="{{ route('admin.themes.create') }}" class="btn btn-default btn-raised">@Lang('Add new')</a>
    </h1>
</div>

<div class="panel-body">
<div class="card">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>@Lang('Name')</th>
                <th width="20%">@Lang('Path')</th>
                <th width="15%">@Lang('Thumbnail')</th>
                <th width="5%">@Lang('Active')</th>
                <th width="5%">@Lang('Actions')</th>
            </tr>
        </thead>
        <tbody>
            @forelse($items as $item)
            <tr>
                <td id="active-{{$item->id}}-name">{{ $item->name }}</td>
                <td>{{ $item->path }}</td>
                <td>
                    <img class="col-imatge col-10" src="data:image/jpeg;base64,{{ base64_encode( $item->thumbnail ) }}" 
                        data-toggle="modal" data-target="#modal-img" 
                        data-img="data:image/jpeg;base64,{{ base64_encode( $item->thumbnail ) }}" 
                        data-name="{{ $item->name }}"
                    />
                </td>
                <td>
                    <button class="btn bmd-btn-icon" type="button" onclick="switch_active({{$item->id}}, 'active-{{$item->id}}');">
                        <i id="active-{{$item->id}}" class="material-icons {{ $item->active ? 'active' : '' }}" style="color:{{ $item->active ? 'green' : 'red' }};transition: all 0.3s linear 0s;">
                            {{ $item->active ? 'check' : 'clear' }}
                        </i>
                    </button>
                </td>

                <td class="cela-opcions">
                    <div class="btn-group">
                        <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </button>  
                        <div class="dropdown-menu" aria-labelledby="opciones-{{ $item->id }}">
                            <a class="dropdown-item" href="{{ route('admin.themes.edit', $item->id) }}">@Lang('Edit')</a>
                            <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('admin.themes.destroy', $item->id) }}" data-nombre="{{__($item->name)}}">@Lang('Delete')</button>
                        </div>
                    </div>
                </td>
            </tr>
            @empty
                <tr>
                    <td colspan="5">@Lang('No entries found.')</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    
</div>
{{ $items->links() }}
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection	

@section('modals')
<!-- Modal -->
<div id="modal-img" style="background: #444;" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$('#myModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var url = button.data('url');
    var nombre = button.data('nombre');
    var modal = $(this);
    modal.find('form').attr('action', url);
    modal.find('.modal-body').html("@Lang('Are you sure to delete it?')");
});
$('.col-imatge').click(function() {
    var button = $(this);
    var nombre = button.data('name');
    var img = button.data('img');
    var modal = $('#modal-img');
    modal.find('.modal-title').html(nombre);
    modal.find('.modal-body').html("<img class='col-12' src='" + img + "'/>");
    modal.modal('show');
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function switch_active(id, e) {
    var icona = $('#' + e);
    if ( icona.hasClass('active') ) {
        $.snackbar({content: $('#' + e + '-name').html() + ' @Lang('already active')', timeout: 3000});
        return;
    }
    $.ajax({
        url: '{{ route('admin.themes.active') }}',
        method: 'POST',
        data: {'_token': '{{csrf_token()}}', 'id': id},
        success: function(data) {
            if ( data == 'OK' ) {
                $('i.active').html( 'clear' );
                $('i.active').css( 'color', 'red' );
                $('i.active').removeClass( 'active' );
                icona.html( 'check' );
                icona.css( 'color', 'green' );
                var msg = $('#' + e + '-name').html() + " @Lang('actived')";
                $.snackbar({content: msg, timeout: 3000});
                icona.toggleClass('active');
            } else $.snackbar({content: 'Error', timeout: 3000});
        }
    });
}
$( "tbody" ).disableSelection();
$('tbody').sortable({
    update: function() {
        alert('ok');
    }
});
</script>
@endsection