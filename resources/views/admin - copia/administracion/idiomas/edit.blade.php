@extends('layouts.admin')

@section('section-title')
    @Lang('Languages')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Edit')</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('idiomas.update', $item->id) }}" method="post">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $item->name }}" required tabindex="1">
        </div>
        <div class="form-group">
            <label for="code" class="bmd-label-floating">@Lang('Code')</label>
            <input type="text" class="form-control" id="code" name="code" value="{{ $item->code }}" required tabindex="2">
            <span class="bmd-help">@Lang('The code has to be unique')</span>
        </div>        
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('idiomas.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
        </div>
             
        </div>
    </form>
</div>
@endsection