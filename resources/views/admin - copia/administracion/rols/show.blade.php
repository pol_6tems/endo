@extends('layouts.admin')

@section('section-title')
    @Lang('Roles')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4 class="mb-5">{{__($item->name)}}: @Lang('Users')</h4>
    <table id="tabla" class="table table-hover">
        <thead>
            <tr>
                <th>@Lang('Name')</th>
                <th>@Lang('Email')</th>
            </tr>
        </thead>
        <tbody>
            @forelse($item->users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
            </tr>
            @empty
                <tr>
                    <td colspan="3">@Lang('No entries found.')</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection

@section('styles')
<link href="{{asset('css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<style>
    .navbar {
        margin-bottom: 0;
        border-radius: 0px;
        border: none;
        min-height: inherit;
    }
    .bmd-layout-content a {
        color: #666;
    }
    .row { width: 100% !important; }
</style>
@endsection

@section('scripts')
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-editable.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tabla').DataTable({
			"pageLength": 25,
			"language": { "url": "{{asset('js/datatables/Catalan.json')}}" }
        });
    });
</script>
@endsection