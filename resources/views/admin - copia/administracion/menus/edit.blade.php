@extends('layouts.admin')

@section('section-title')
    @Lang('Menus')
@endsection

@section('content')
<div class="card mb-5">
<div class="panel-heading ml-5 mt-3">
    <h4>
        @Lang('Edit'): {{__($item->name)}}
    </h4>
</div>

<div class="panel-body">

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.menus.update', $item->id) }}" method="post">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        
        <div class="form-group">
            <label for="order" class="bmd-label-floating">@Lang('Order')</label>
            <input type="number" class="form-control" name="order" name="order" value="{{ $item->order }}" tabindex="1" min="0">
        </div>
        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$item->name}}" required tabindex="2">
        </div>
        <div class="form-group">
            <label for="icon" class="bmd-label-floating">@Lang('Icon')</label>
            <input type="text" class="form-control" id="icon" name="icon" value="{{ $item->icon }}" tabindex="3">
            <span class="bmd-help"><a href="https://materializecss.com/icons.html">https://materializecss.com/icons.html</a></span>
        </div>
        <div class="form-group">
            <div class="switch">
                <label>
                    <input name="has_url" type="checkbox" onclick="show_url_form();" @if($item->has_url) checked @endif tabindex=4>
                    @Lang('Has link?')
                </label>
            </div>
        </div>
        <div class="form-group te_enllac">
            <label for="url_type" class="bmd-label-floating" style="top: 0.4rem;">@Lang('Type')</label>
            <select class="form-control custom-select" name="url_type" data-live-search="true" title="@Lang('Type')" tabindex=5>
                <option value="route" {{$item->url_type == 'route' ? 'selected' : ''}}>ROUTE</option>
                <option value="url" {{$item->url_type == 'url' ? 'selected' : ''}}>URL</option>
            </select>
        </div>
        <div class="form-group te_enllac">
            <label for="url" class="bmd-label-floating">@Lang('Url')</label>
            <input type="text" class="form-control" id="url" name="url" value="{{ $item->url }}" tabindex=6>
        </div>
        <div class="form-group te_enllac">
            <label for="controller" class="bmd-label-floating">@Lang('Controller')</label>
            <input type="text" class="form-control" id="controller" name="controller" value="{{ $item->controller }}" tabindex=7>
        </div>
        <div class="form-group mt-5 mb-1">
            <input onclick="window.location.href='{{ route('admin.menus.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
        </div>
            
        </div>
    </form>
</div>
</div>

<!-- MenuSub -->
<div class="panel-heading">
    <h2>
        @Lang('Submenus')
        <a href="{{ route('admin.menus.subs.create', $item->id) }}" class="btn btn-default btn-raised">@Lang('Add')</a>
    </h2>
</div>

<div class="panel-body">
<div class="card">
<table class="table table-hover">
        <thead>
            <tr>
                <th width="5%">#</th>
                <th>@Lang('Title')</th>
                <th class="d-none d-sm-table-cell">@Lang('Name')</th>
                <th class="d-none d-sm-table-cell">@Lang('Type')</th>
                <th class="d-none d-sm-table-cell">@Lang('URL')</th>
                <th class="d-none d-sm-table-cell">@Lang('Controller')</th>
                <th width="5%">@Lang('Actions')</th>
            </tr>
        </thead>
        <tbody>
            @forelse($subs as $sub)
            <tr>
                <td>{{ $sub->order }}</td>
                <td>{{ __($sub->name) }}</td>
                <td class="d-none d-sm-table-cell">{{ $sub->name }}</td>
                <td class="d-none d-sm-table-cell">{{ $sub->url_type }}</td>
                <td class="d-none d-sm-table-cell">{{ $sub->url }}</td>
                <td class="d-none d-sm-table-cell">{{ $sub->controller }}</td>
                <td class="cela-opcions">
                    <div class="btn-group">
                        <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $sub->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </button>  
                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $sub->id }}">
                            <a class="dropdown-item" href="{{ route('admin.menus.subs.edit', $sub->id) }}">@Lang('Edit')</a>
                            <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('admin.menus.subs.destroy', $sub->id) }}" data-nombre="@Lang('Submenu'): {{ __($sub->name) }} - @Lang('Email'): {{ $sub->name }}">@Lang('Delete')</button>
                        </div>
                    </div>
                </td>
                
            </tr>
            @empty
                <tr>
                    <td colspan="7">@Lang('No entries found.')</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>
    {{ $subs->links() }}
</div>
<!-- end MenuSub -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('styles')
<style>
    .te_enllac { display:none; }
</style>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    @if ($item->has_url) show_url_form(); @endif
});
$('#myModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var url = button.data('url');
    var nombre = button.data('nombre');
    var modal = $(this);
    modal.find('form').attr('action', url);
    modal.find('.modal-body').html("@Lang('Are you sure to delete it?')");
});
$( "tbody" ).disableSelection();
$('tbody').sortable({
    update: function() {
        alert('ok');
    }
});
function show_url_form() {
    $('.te_enllac').toggle("slow");
}
</script>
@endsection