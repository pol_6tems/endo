@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>
        <i class="material-icons pr-2 admin-menu-icona">menu</i>
        @Lang('Menu'): @Lang('Add new')
    </h1>
</div>

<div class="panel-body">
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.menus.store') }}" method="post">
        {{ csrf_field() }}
        
        <div class="form-group">
            <label for="order" class="bmd-label-floating">@Lang('Order')</label>
            <input type="number" class="form-control" name="order" name="order" value="{{ old('order') }}" tabindex="1" min="0">
        </div>
        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required tabindex="2">
        </div>
        <div class="form-group">
            <label for="icon" class="bmd-label-floating">@Lang('Icon')</label>
            <input type="text" class="form-control" id="icon" name="icon" value="{{ old('icon') }}" tabindex="3">
            <span class="bmd-help"><a href="https://materializecss.com/icons.html">https://materializecss.com/icons.html</a></span>
        </div>
        <div class="form-group">
            <div class="switch">
                <label>
                    <input name="has_url" type="checkbox" onclick="show_url_form();" tabindex=4>
                    @Lang('Has link?')
                </label>
            </div>
        </div>
        <div class="form-group te_enllac">
            <label for="url_type" class="bmd-label-floating" style="top: 0.4rem;">@Lang('Type')</label>
            <select class="form-control custom-select" name="url_type" data-live-search="true" title="@Lang('Type')" tabindex=5>
                <option value="route" selected>ROUTE</option>
                <option value="url">URL</option>
            </select>
        </div>
        <div class="form-group te_enllac">
            <label for="url" class="bmd-label-floating">@Lang('Url')</label>
            <input type="text" class="form-control" id="url" name="url" value="{{ old('url') }}" tabindex=6>
        </div>
        <div class="form-group te_enllac">
            <label for="controller" class="bmd-label-floating">@Lang('Controller')</label>
            <input type="text" class="form-control" id="controller" name="controller" value="{{ old('controller') }}" tabindex=7>
        </div>
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.menus.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
        </div>
        
    </form>

</div>
@endsection

@section('styles')
<style>
    .te_enllac { display:none; }
</style>
@endsection

@section('scripts')
<script>
    function show_url_form() {
        $('.te_enllac').toggle("slow");
    }
</script>
@endsection