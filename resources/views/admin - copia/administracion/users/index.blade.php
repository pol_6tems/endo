@extends('layouts.admin')

@section('section-title')
	@Lang('Users')
	<a href="{{ route('admin.users.create') }}" class="btn btn-default btn-raised bg-white">
		@Lang('Add')
	</a>
@endsection

@section('content')
<div class="card-noconflict p-3 col-md-5 mb-3">
<form class="form-inline card-noconflict p-3" method="GET" action="{{ route('admin.users.index') }}">
	{!! csrf_field() !!}
	<div class="form-group mr-4">
		<input type="hidden" name="sortby" value="{{$sortby}}">
		<input type="hidden" name="order" value="{{$order}}">
		<label for="search" class="bmd-label-floating">@Lang('Search'):</label>
		<input type="text" name="search" class="form-control" placeholder="" value="{{$search}}">
	</div>
	<span class="form-group bmd-form-group"> <!-- needed to match padding for floating labels -->
		<button type="submit" class="btn btn-default btn-raised">@Lang('Search')</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button type="button" onclick="window.location.href='{{route('admin.users.index')}}'" class="btn btn-secondary btn-raised">@Lang('Reset')</button>
	</span>
</form>
</div>

<div class="card" style="clear:both;">
<table id="taula" class="table table-hover table-sortable">
	<thead>
		<tr>
			<th width="5%" scope="col">#</th>
			<th scope="col">@Lang('Name')</th>
			<th class="d-none d-sm-table-cell" scope="col">@Lang('Email')</th>
			<th class="d-none d-sm-table-cell" scope="col">@Lang('Created at')</th>
			<th class="d-none d-sm-table-cell" scope="col">@Lang('Role')</th>
			<th width="5%" scope="col">@Lang('Actions')</th>
		</tr>
	</thead>
	<tbody>
	
	@foreach($items as $item)
		@if ( Auth::user()->role != 'admin' && Auth::user()->id != $item->id && ($item->role == 'admin' || $item->role == 'editor') )
			@continue
		@endif
		<tr>
			<th scope="row">{{ $item->id }}</th>
			<td>{{ $item->fullname(true) }}</td>
			<td class="d-none d-sm-table-cell">{{ $item->email }}</td>
			<td class="d-none d-sm-table-cell">{{ $item->created_at }}</td>
			<td class="d-none d-sm-table-cell">{{ $item->role }}</td>
			<td class="cela-opcions">
				<div class="btn-group">
					<button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="material-icons">more_vert</i>
					</button>  
					<div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
						<a class="dropdown-item" href="{{route('admin.users.edit', $item->id)}}">@Lang('Edit')</a>
						@if ( $item->id > 1 && Auth::user()->id != $item->id )
							<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{route('admin.users.destroy', $item->id)}}" data-id="{{$item->id}}" data-nombre="{{$item->fullname(true)}}">@Lang('Delete')</button>
						@endif
					</div>
				</div>
			</td>
		</tr>
	@endforeach
	
	</tbody>
</table>
</div>
{{ $items->links() }}

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('footer')
@endsection

@section('styles')
@endsection

@section('scripts')
<script>
    $('.eliminar-item').click(function(e){
        e.preventDefault() // Don't post the form, unless confirmed
        if (confirm('¿Estás seguro de eliminarlo?')) {
            $(e.target).closest('form').submit();
        }
    });
	$('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var id = button.data('id');
		var nombre = button.data('nombre');
		var url = button.data('url');
		var modal = $(this);
		modal.find('form').attr('action', url);
		modal.find('.modal-body').html("¿Seguro que quieres eliminar a <strong>" + nombre + "?</strong>");
	});
</script>
@endsection