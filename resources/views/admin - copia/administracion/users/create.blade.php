@extends('layouts.admin')

@section('section-title')
	@Lang('Users')
@endsection

@section('content')
<div class="card-noconflict p-3">
	<h4>@Lang('Add new')</h4>

<form method="POST" action="{{route('admin.users.store')}}">
    {!! csrf_field() !!}
	<div class="form-group">
		<label for="name" class="bmd-label-floating">@Lang('Name')</label>
		<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
		<div class="text-danger">{{$errors->first('name')}}</div>
	</div>
	<div class="form-group">
		<label for="lastname" class="bmd-label-floating">@Lang('Lastname')</label>
		<input type="text" class="form-control" id="lastname" name="lastname" value="{{ old('lastname') }}" required>
		<div class="text-danger">{{$errors->first('lastname')}}</div>
	</div>
	<div class="form-group">
		<label for="email" class="bmd-label-floating">@Lang('Email')</label>
		<input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
		<!--<span class="bmd-help">No compartiremos su mail con nadie.</span>-->
		<div class="text-danger">{{$errors->first('email')}}</div>
	</div>
	<div class="form-group">
		<label for="password" class="bmd-label-floating">@Lang('Password')</label>
		<input type="password" class="form-control" id="password" name="password" required>
		<div class="text-danger">{{$errors->first('password')}}</div>
	</div>
	<div class="form-group">
		<label for="password_confirmation" class="bmd-label-floating">@Lang('Confirm password')</label>
		<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
	</div>
	<div class="form-group">
		<label for="role" class="bmd-label-floating">@Lang('Role')</label>
		<select class="form-control custom-select" name="role" data-live-search="true" title="@Lang('Role')" required>
			@foreach ($rols as $rol)
			<option value="{{$rol->name}}" {{$rol->name == 'user' ? 'selected' : ''}}>{{__($rol->name)}}</option>
			@endforeach
		</select>
	</div>
	<div class="form-group mt-5 mb-3">
		<button type="button" class="btn btn-default" onclick="window.location.href='{{route('admin.users.index')}}'">@Lang('Cancel')</button>
		<button type="submit" class="btn btn-primary btn-raised">@Lang('Create')</button>
	</div>
</form>
</div>
@endsection

@section('footer')
@endsection

@section('styles')
<style>
	.dropdown-menu.show {
		overflow: inherit !important;
    	top: 35px !important;
	}
</style>
@endsection

@section('scripts')
@endsection