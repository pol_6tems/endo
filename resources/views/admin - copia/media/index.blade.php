@extends('layouts.admin')

@section('section-title')
	@Lang('Media')
	<a href="{{ route('admin.media.create') }}" class="btn btn-default btn-raised bg-white ml-3">@Lang('Add')</a>
@endsection

@section('content')

<div class="card-noconflict pl-3 mb-3 col-md-4">
<form class="form-inline" method="GET" action="{{ route('admin.calendario.empleados.index') }}">
    {!! csrf_field() !!}
    <div class="form-group mr-4">
		<input type="hidden" name="sortby" value="{{$sortby}}">
		<input type="hidden" name="order" value="{{$order}}">
        <label for="search" class="bmd-label-floating">@Lang('Search'):</label>
        <input type="text" name="search" class="form-control" placeholder="" value="{{$search}}">
    </div>
    <span class="form-group bmd-form-group"> <!-- needed to match padding for floating labels -->
	<button type="submit" class="btn btn-default btn-raised">@Lang('Search')</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button type="button" onclick="window.location.href='{{route('admin.calendario.empleados.index')}}'" class="btn btn-secondary btn-raised">@Lang('Reset')</button>
    </span>
</form>
</div>

<div class="card" style="clear: both;">
<table id="taula" class="table table-hover table-striped table-sortable">
	<thead>
		<tr>
			@if ( count($items) > 0 )
				<th width="5%"><input type="checkbox" class="checkbox_all"></th>
			@endif
			<th width="10%" class="sortable" data-sortby="id" scope="col">@Lang('Id')</th>
			<th class="sortable" data-sortby="title" scope="col">@Lang('Title')</th>
			<th width="20%" class="sortable" data-sortby="file_name" scope="col">@Lang('File')</th>
			<th width="20%" class="sortable" scope="col">@Lang('Thumbnail')</th>
			<th width="10%" class="sortable" data-sortby="updated_at" scope="col">@Lang('Updated at')</th>
			<th width="5%" scope="col">@Lang('Actions')</th>
		</tr>
	</thead>
	<tbody>
	
	@forelse($items as $item)
		<tr>
			<td><input type="checkbox" class="checkbox_delete" name="entries_to_delete[]" value="{{ $item->id }}" /></td>
			<td>{{ $item->id }}</td>
			<td>
				<a href="{{ $item->get_thumbnail_url() }}" target="_blank">
					{{ $item->title }}
				</a>
			</td>
			<td>{{ $item->file_name }}</td>
			<td>
					<img class="col-imatge" src="{{ $item->get_thumbnail_url('thumbnail') }}"
						data-toggle="modal" data-target="#modal-img" 
                        data-img="{{ $item->get_thumbnail_url('large') }}" 
						data-name="{{ $item->title }}"
						style="width: 50px;object-fit: scale-down;"
					/>
			</td>
			<td>{{ $item->updated_at->format('d/m/Y h:i') }}</td>
			<td class="cela-opcions">
				<div class="btn-group">
					<button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="material-icons">more_vert</i>
					</button>  
					<div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
						<a class="dropdown-item" href="{{route('admin.media.edit', $item->id)}}">@Lang('Edit')</a>
						<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-id="{{$item->id}}" data-nombre="{{$item->title}}" data-url="{{route('admin.media.destroy', $item->id)}}">@Lang('Delete')</button>
					</div>
				</div>
			</td>
		</tr>
	@empty
		<tr>
			<td class="bg-white" colspan="8">@Lang('No entries found.')</td>
		</tr>
	@endforelse
	
	</tbody>
</table>
</div>
{{ $links }}

@if ( count($items) > 0 )
	<form class="mt-3" action="{{ route('admin.media.mass_destroy') }}" method="post" onsubmit="return confirm('@Lang('Are you sure?')');">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="DELETE">
		<input type="hidden" name="ids" id="ids" value="" />
		<input type="submit" value="@Lang('Delete selected')" class="btn btn-danger" />
	</form>
@endif
@endsection

@section('modals')
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="modal-img" style="background: #444;" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/column-sortable.js') }}"></script>
<script>
	var $sortby = '{{ $sortby }}';
	var $order = '{{ $order }}';
	var $search = '{{ $search }}';
	
    $('.eliminar-item').click(function(e){
        e.preventDefault() // Don't post the form, unless confirmed
        if (confirm('@Lang('¿Estás seguro de eliminarlo?')')) {
            $(e.target).closest('form').submit();
        }
    });
	
	$('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var id = button.data('id');
		var nombre = button.data('nombre');
		var url = button.data('url');
		var modal = $(this);
		modal.find('form').attr('action', url);
		modal.find('.modal-body').html("¿Seguro que quieres eliminar a <strong>" + nombre + "?</strong>");
	});

	$('.col-imatge').click(function() {
		var button = $(this);
		var nombre = button.data('name');
		var img = button.data('img');
		var modal = $('#modal-img');
		modal.find('.modal-title').html(nombre);
		modal.find('.modal-body').html("<img class='col-12' src='" + img + "'/>");
		modal.modal('show');
	});
</script>
@endsection