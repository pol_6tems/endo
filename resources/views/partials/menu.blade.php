@foreach ( \App\Http\Controllers\Admin\AdminController::getMenu() as $menu )
    <header class="pb-0">
        <a class="navbar-brand pl-0 @if($menu->is_active) active @else collapsed @endif" data-toggle="collapse" href="{{ $menu->submenus->count() > 0 ? '#' . $menu->name : $menu->href }}" role="button" aria-expanded="{{$menu->is_active?'true':false}}" aria-controls="{{$menu->name}}" @if (!empty($menu->href)) onclick="window.location.href='{{$menu->href}}';" @endif>
            <i class="material-icons pr-2">{{$menu->icon}}</i><span>{{__($menu->name)}}</span>
            @if ( $menu->submenus->count() > 0 )<b class="caret"></b>@endif
        </a>
    </header>
    @if ( $menu->submenus->count() > 0 )
        <div class="collapse @if($menu->is_active) in show @endif" id="{{$menu->name}}" aria-expanded="{{$menu->is_active?'true':false}}">
            <ul class="list-group mt-2">
                @foreach ( $menu->submenus as $sub )
                    <li>
                        <a class="list-group-item minimized pt-2 pb-2
							@if ( $sub->is_active )
                                active
                            @endif
                                "
                           href="@if ( $sub->url_type == 'route' ) {{ route($sub->url) }} @else {{ $sub->url }} @endif">
                            @php($words = explode(' ', strtoupper(__($sub->name))))
                            @php($w_txt = '')
                            @foreach ($words as $word)
                                @php($w_txt .= substr($word, 0, 1))
                            @endforeach
                            {{ $w_txt }}
                        </a>
                        <a class="list-group-item pt-2 pb-2
							@if ( $sub->is_active )
                                active
                            @endif
                                "
                           href="@if ( $sub->url_type == 'route' ) {{ route($sub->url) }} @else {{ $sub->url }} @endif">
                            {{__($sub->name)}}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
@endforeach