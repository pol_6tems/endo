@extends('layouts.app')

@section('header')
<div class="title m-b-md flex-center" style="padding-top: 20vh;">
	Dashboard
</div>
@endsection

@section('content')
	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif
	<div style="text-align: center;">Front end layout</div>


    {{ route('index') }}
    <br/>
    {{ route('index', ['locale' => 'ca']) }}
    <br/>
    {{ route('index', ['locale' => 'es']) }}
    <br/>
    {{ route('index', ['locale' => 'en']) }}
    <br><br>
    {{ route('home') }}
    <br/>
    {{ route('home', ['locale' => 'ca']) }}
    <br/>
    {{ route('home', ['locale' => 'es']) }}
    <br/>
    {{ route('home', ['locale' => 'en']) }}
@endsection
