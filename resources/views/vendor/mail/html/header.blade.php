{{-- ORIGINAL
<tr>
    <td class="header">
        <a href="{{ $url }}">
            {{ $slot }}
        </a>
    </td>
</tr>
--}}

@php ( $img_email = \App\Models\Configuracion::get_config('img_email') )
@if ( empty($img_email) )
    @php ( $img_email = asset('images/email_logo_250.jpg') )
@endif
<tr>
    <td class="header">
        <a href="{{ route('index') }}">
            <img width="250px" src="{{ $img_email }}">
        </a>
    </td>
</tr>