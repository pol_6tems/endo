@if ($paginator->hasPages())
<div class="pagination">
    <ul>
        {{-- Previous Page Link --}}
        <li class="first"><a href="{{ $paginator->previousPageUrl() }}">@lang('Página anterior')</a></li>

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li><a class="active" href="{{ $url }}">{{ $page }}</a></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        <li class="last"><a href="{{ $paginator->nextPageUrl() }}">@lang('Página siguiente')</a></li>
    </ul>
</div>
@endif
