<?php

return [
    'email' => 'Email',
    'failed'   => 'Aquestes credencials no concorden amb els nostres registres.',
    'log_out' => 'Sortir',
    'login' => 'Iniciar sessió',
    'logout' => 'Tancar sessió',
    'password' => 'Contrasenya',
    'confirm_password' => 'Confirmar contrasenya',
    'recover_pwd' => 'Ha oblidat la contrasenya?',
    'remember_me' => 'Recordar-me',
    'throttle' => 'Heu superat el nombre màxim d\'intents d\'accés. Per favor, torna a intentar-ho en :seconds segons.',
    'welcome_to' => 'Benvinguts a :app_name'
];