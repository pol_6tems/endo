<?php

namespace App\Providers;

use View;
use App\Theme;
use App\Module;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\LengthAwarePaginator;

class AppServiceProvider extends ServiceProvider
{    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // FIX Error SQL (Syntax error or access violation: 1071 Specified key was too long; max key length is 767 bytes)
        \Schema::defaultStringLength(191);

        // Carreguem els helpers perque estiguin disponibles als templates blade
        $this->loadHelpers();
        $this->loadModules();
        $this->loadThemes();

        if (!isProEnv() && env('GENERATE_FRONT_TRANSLATIONS')) {
            $this->app->register(__NAMESPACE__ . '\\TranslationServiceProvider');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ( \Schema::hasTable('modules') ) {
            foreach (glob(app_path('Modules/*'), GLOB_ONLYDIR) as $dirname) {
                $mod = \DB::table('modules')->select('*')->where('name', basename($dirname))->where('active', 1)->exists();
                if ( $mod && file_exists("$dirname/Console/Kernel.php") ) {
                    $commands = app('App\Modules\\' . basename($dirname) . '\Console\Kernel')->getCommands();
                    $this->commands($commands);
                }
            }
        }
    }

    /**
     * Load helpers.
     */
    protected function loadThemes()
    {
        if ( \Schema::hasTable('themes') ) {
            // Themes - Front
            if ( $front_theme_path = Theme::where([['active', true], ['admin', false]])->pluck('path')->first() ) {
                View::addNamespace('Front', base_path('resources/themes/'.$front_theme_path));
                config(['front_theme' => 'Themes/' . $front_theme_path . '/']);
            }

            // Themes - Admin
            if ( $admin_theme_path = Theme::where([['active', true], ['admin', true]])->pluck('path')->first() ) {
                View::addNamespace('Admin', base_path('resources/themes/'.$admin_theme_path));
                config(['admin_theme' => 'Themes/' . $admin_theme_path . '/']);
            }

            // Themes - Modules
            View::addNamespace('Modules', app_path('Modules/'));
        }
    }

    /**
     * Load helpers.
     */
    protected function loadHelpers()
    {
        foreach (glob(app_path('Helpers/*.php')) as $filename) {
            require_once $filename;
        }
    }

    /**
     * Load Modules.
     */
    protected function loadModules()
    {
        // https://laravel.com/docs/5.8/packages >> REVISAR
        if ( \Schema::hasTable('modules') ) {
            foreach (glob(app_path('Modules/*'), GLOB_ONLYDIR) as $dirname) {
                if ( \App\Module::where([['name', basename($dirname)], ['active', true]])->exists() ) {

                    // Importem les traduccions
                    $this->loadJSONTranslationsFrom("$dirname/resources/lang");
                    
                    foreach (glob($dirname . '/Helpers/*.php') as $filename) {
                        require_once $filename;
                    }

                    // Carreguem només els providers dels fills del "provider" actual
                    $currentProvider = explode("\\", get_class());
                    $currentProvider = array_pop($currentProvider);
                    
                    foreach(rglob($dirname . "/Providers/$currentProvider.php") as $provider) {
                        $className = get_className($provider);
                        if (class_exists($className)) {
                            $this->app->register($className);
                        }
                    }

                    // Carreguem les migracions
                    $this->loadMigrationsFrom("$dirname/database/migrations");

                    // Afegim la localització dels recursos
                    View::addLocation("$dirname/resources/views");
                    $this->loadViewsFrom($dirname.'/resources/views', basename($dirname));
                }
            }
        }
    }
}
