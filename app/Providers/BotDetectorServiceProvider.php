<?php


namespace App\Providers;


use App\Detectors\BotDetector;
use Illuminate\Support\ServiceProvider;

class BotDetectorServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;


    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('BotDetector', function ($app) {
            return new BotDetector(config('botDetector'), request());
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['BotDetector'];
    }
}