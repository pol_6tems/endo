<?php


namespace App\Providers;


use App\Services\PostsHelper;
use Illuminate\Support\ServiceProvider;

class PostsHelperServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;


    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('PostsHelper', function ($app) {
            return new PostsHelper();
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['PostsHelper'];
    }
}