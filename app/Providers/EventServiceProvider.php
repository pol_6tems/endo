<?php

namespace App\Providers;

use App\Module;
use App\Theme;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        if ( Schema::hasTable('themes') ) {
            $front_theme_path = Theme::where([['active', true], ['admin', false]])->pluck('path')->first();

            $className = "\App\Modules\\$front_theme_path\Providers\\EventServiceProvider";

            if (class_exists($className)) {
                $this->app->register(
                    $className
                );
            }
        }

        if ( \Schema::hasTable('modules') ) {
            if ($module = Module::where('active', true)->first()) {
                $front_module_path = $module->name;

                $className = "\App\Modules\\$front_module_path\Providers\\ComposerServiceProvider";

                if (class_exists($className)) {
                    $this->app->register(
                        $className
                    );
                }
            }
        }

        //
    }
}
