<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /*$cont = new \App\Modules\CalendarioVacaciones\Controllers\Admin\AdminController();
        $pendings = $cont->widget_last_requests([
            'limit' => -1,
            'status' => 'pending',
            'name' => 'last_pendings',
            'title' => __('Last pending requests'),
            'icon' => 'date_range',
            'icon_class' => 'info',
            'headers' => [__('Name'), __('Email'), __('Level'), __('Date')],
        ]);
        $cont->add_notification([
            'msg' => __('You have :number pending requests', ['number' => count($pendings->rows)]),
            'href' => route('admin.vacaciones.index', ['locale' => \App::getLocale()]),
        ]);
        $mensajes_calendario = CalendarioMensaje::where('visto', false)->get();
        $cont->add_notification([
            'msg' => __('You have :number unread messages (calendar)', ['number' => $mensajes_calendario->count()]),
            'href' => route('admin.calendario.mensajes.index', ['locale' => \App::getLocale()]),
        ]);
        $mensajes = Mensaje::where('visto', false)->get();
        $cont->add_notification([
            'msg' => __('You have :number unread messages', ['number' => $mensajes->count()]),
            'href' => route('admin.mensajes.index', ['locale' => \App::getLocale()]),
        ]);
        dd( $cont->notifications );
        session(['notifications' => $cont->notifications]);*/
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
