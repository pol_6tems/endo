<?php


namespace App\Providers;


use App\Http\Composers\Admin\SidebarComposer;
use App\Module;
use App\Theme;
use Illuminate\Support\ServiceProvider;
use Schema;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     * 
     * @return void
     */
    public function boot()
    {
        view()->composer(['Admin::partials.sidebar', 'Admin::partials.menu'], SidebarComposer::class);
        $this->registerModulesServiceProviders();
    }

    public function registerModulesServiceProviders() {
        if ( \Schema::hasTable('modules') ) {
            if ($theme = Theme::where([['active', true], ['admin', false]])->first()) {
                $front_theme_path = $theme->path;

                $className = "\App\Modules\\$front_theme_path\Providers\\ComposerServiceProvider";

                if (!class_exists($className)) {
                    $front_theme_path = $theme->name;
                    $className = "\App\Modules\\$front_theme_path\Providers\\ComposerServiceProvider";
                }
                
                if (class_exists($className)) {
                    $this->app->register(
                        $className
                    );
                }
            }

            if ($module = Module::where('active', true)->first()) {
                $front_module_path = $module->name;

                $className = "\App\Modules\\$front_module_path\Providers\\ComposerServiceProvider";

                if (class_exists($className)) {
                    $this->app->register(
                        $className
                    );
                }
            }
        }
    }


    /**
     * Register the service provider
     * 
     * @return void
     */
    public function register()
    {
        //
    }
}