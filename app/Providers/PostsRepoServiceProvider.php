<?php


namespace App\Providers;


use App\Repositories\PostsRepository;
use Illuminate\Support\ServiceProvider;

class PostsRepoServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;


    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('PostsRepo', function ($app) {
            return new PostsRepository();
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['PostsRepo'];
    }
}