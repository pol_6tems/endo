<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
//use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteModulesServiceProvider extends ServiceProvider
{

    public function boot() {
        if ( \Schema::hasTable('modules') ) {
            foreach (glob(app_path('Modules/*'), GLOB_ONLYDIR) as $dirname) {
                if ( \App\Module::where([['name', basename($dirname)], ['active', true]])->exists() ) {
                    
                    //Load Web Routes
                    Route::prefix('{locale}')
                        ->middleware(['web'])
                        ->namespace('App\Modules\\' . basename($dirname) . '\Controllers')
                        ->group(function () use ($dirname) {
                            require_once "$dirname/routes.php";
                        }
                    );

                    // Load API Routes
                    Route::prefix('api')
                        ->middleware(['api', 'locale'])
                        ->namespace('App\Modules\\' . basename($dirname) . '\Controllers')
                        ->group(function () use ($dirname) {
                            if (file_exists("$dirname/api.php")) require_once "$dirname/api.php";
                        }
                    );


                }
            }
        }
    }

}
