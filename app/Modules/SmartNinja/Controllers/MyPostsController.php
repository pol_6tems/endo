<?php

namespace App\Modules\SmartNinja\Controllers;

use View;
use App\Post;
use App\Models\CustomPost;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\PostsController;

class MyPostsController extends PostsController
{
    protected $api;

    protected function is_single($post_name) {
        // Busquem primer si hi ha una pàgina i a continuació si hi ha un Post
        $page = Post::publish()
                        ->ofType('page')
                        ->whereTranslation('post_name', $post_name)
                        ->first();

        if ( !empty($page) ) return $page;
        else {
            $posts = Post::publish()
                            ->whereTranslation('post_name', $post_name, app()->getLocale())
                            ->get();

			$segments = request()->segments();
			
			$trobat = false;
			foreach($posts as $post) {
				// Mirem si dins de la url hi ha el plural del post_type que ha de tenir el post name
				foreach(array_reverse($segments) as $segment) {
					$cp = $post->customPostTranslations->first();
					if ($segment == $cp->post_type_plural) {
						$trobat = true;
						break;
					}
				}
				if ($trobat) return $post;
			}
			
			return $posts->first();
        }
    }

    protected function is_archive($post_name) {
        return CustomPost::whereTranslation('post_type_plural', $post_name, \App::getLocale())->first();
    }

    /**
     * Coincideix amb totes les rutes.
     * Itera per totes les rutes començant pel final i va buscant si coincideix amb algun template.
     * 
     * Finalitza quan es troba una pàgina, que en mostra o bé el template o bé la pàgina de defecte
     */
    public function process_routes($locale) {
        $params = collect(request()->segments());
        $params->forget(0); // Treure primer segment (lang)
        $data = [];
        
        return $this->renderView($params, $data);
    }

    /**
     * Funció que retorna la vista corresponent segons els parametres d'entrada.
     * Només necessitem saber el primer element (Distingir si és un CP Plural (Archive) | Post Name (Single) | Page Name (Template))
     *
     * @param Array $params
     * @param array $data
     * @return View
     */
    private function renderView($params, $data = []) {
        $routes = ['Admin::errors.404'];

        // Check si hi ha un curs multi domain
        $curs = get_posts(array (
            'post_type' => 'curso',
            'metas' => array(
                array('dominio', request()->getHttpHost())
            )
        ))->first();

        if($curs == null) {
            // Si no, assignem la pàgina de portada
            $item = Post::where('id', '1')->get()->first();
            $trans  = $item->getTranslation();
            $routes = [ "Front::posts.templates.{$item->template}", 'Front::posts.page' ];
            $data   = array_merge($data, [ 'item' => $item, 'page' => $item, 'trans' => $trans ]);
        } else {
            $item   = $curs;
            $trans  = $curs->getTranslation();
            $routes = [ "Front::posts.single-{$curs->type}", "Front::posts.single" ];
            $data   = array_merge($data, [ 'item' => $curs, 'trans' => $trans ]);
        }

        return view()->first($routes, $data);
    }

    public function show($item, $trans = null) {
        $routes = ['Admin::errors.404'];
        $data = [];
        $lang = app()->getLocale();

        if ( empty($trans) ) $trans = $item->translate($lang);

        global_item($item);

        if ( $item->type == 'page' ) {
            $trans  = $item->getTranslation();
            $routes = [ "Front::posts.templates.{$item->template}", 'Front::posts.page' ];
            $data   = [ "item" => $item, "page" => $item, "trans" => $trans ];
        } else {
            $trans  = $item->getTranslation();
            $routes = [ "Front::posts.single-{$item->type}", "Front::posts.single" ];
            $data   = [ "item" => $item, "trans" => $trans ];
        }
        
        if ($this->api) return $item;

        return view()->first($routes, $data);
    }
}