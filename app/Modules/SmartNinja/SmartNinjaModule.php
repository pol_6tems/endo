<?php

namespace App\Modules\SmartNinja;

use App\Support\Module;

class SmartNinjaModule extends Module {

    const MODULE_NAME = 'Smart Ninja';
    const MODULE_ROUTE_ADMIN = 'admin.smart';
    const MODULE_ROUTE = 'smart';
    
}