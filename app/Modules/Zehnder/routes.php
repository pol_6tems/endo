<?php


Route::get('/{index?}', 'HomeController@index')->name('index');

Route::get('/home', function ($locale) {
    return redirect()->route('index');
});

Route::get('/{brand}', 'HomeController@brand')->name('brand');

Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    Route::resource("posts", "Admin\MyPostsController", ["as" => "admin"]);
});