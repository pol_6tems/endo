<?php


namespace App\Modules\Zehnder\Controllers\Admin;


use App\Http\Controllers\Admin\PostsController;

class MyPostsController extends PostsController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = parent::index();
        return view('Front::admin.posts.index', $view->getData());
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->role != 'admin' && request('post_type') == 'page') {
            abort(403);
        }

        return parent::create();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $view = parent::edit($locale, $id);
        return view('Front::admin.posts.edit', $view->getData());
    }
}