<?php


namespace App\Modules\Zehnder\Controllers;


use App\Post;
use App\Models\PostMeta;
use App\Models\CustomPost;
use App\Models\CustomFieldGroup;
use App\Http\Controllers\PostsController;

class HomeController extends PostsController
{


    public function index()
    {
        $brands = get_posts(['post_type' => 'marca'])->sortBy('order');
        $home = $this->is_single('home');

        $videos = get_posts(['post_type' => 'videos'])->sortBy('order');

        return view('Front::home', compact('brands', 'home', 'videos'));
    }
    
    
    public function brand($locale, $brandName)
    {
        $brands = get_posts(['post_type' => 'marca'])->sortBy('order');

        $videos = get_posts(['post_type' => 'videos'])->sortBy('order');

        $brand = null;
        $video = null;

        if (strpos($brandName, 'videos-') !== 0) {
            foreach ($brands as $brandItem) {
                if ($brandItem->translate()->post_name == $brandName) {
                    $brand = $brandItem;
                }
            }
        } else {
            $brandName = str_replace_first('videos-', '', $brandName);

            foreach ($videos as $videoItem) {
                if ($videoItem->translate()->post_name == $brandName) {
                    $video = $videoItem;
                }
            }
        }

        if (!isset($brand) && !isset($video)) {
            abort(404);
        }

        $categoryFieldId = $this->getCustomFieldParentId('categoria', 'marca');
        $subcategoryFieldId = $this->getCustomFieldParentId('subcategoria', 'categoria');
        $categoryProductFieldId = $this->getCustomFieldParentId('producto', 'categoria');
        $subcategoryProductFieldId = $this->getCustomFieldParentId('producto', 'subcategoria');

        foreach ($brands as $brandItem) {
            $categoryMetas = PostMeta::with(['post.metas.customField'])
                ->where('value', $brandItem->id)
                ->where('custom_field_id', $categoryFieldId)
                ->get();

            $categories = $categoryMetas->map(function ($categoryMeta) {
                return $categoryMeta->post;
            });

            $categoryIds = $categories->filter(function ($category) {
                return $category instanceof Post;
            })->map(function ($category) {
                if(!is_object($category)) {
                    dd($category);
                }
                return $category->id;
            })->all();

            $subcategoryMetas = PostMeta::with(['post.metas.customField'])
                ->whereIn('value', $categoryIds)
                ->where('custom_field_id', $subcategoryFieldId)
                ->get();

            $subcategories = $subcategoryMetas->map(function ($subcategoryMeta) {
                return $subcategoryMeta->post;
            })->filter(function($subcategory) {
                return $subcategory;
            })->values();

            $categoryProductMetas = PostMeta::with(['post.metas.customField'])
                ->whereIn('value', $categoryIds)
                ->where('custom_field_id', $categoryProductFieldId)
                ->get();

            $categoryProducts = $categoryProductMetas->map(function ($categoryProductMeta) {
                return $categoryProductMeta->post;
            });


            $subcategoryIds = $subcategories->map(function ($subcategory) {
                return $subcategory->id;
            });


            $subcategoryProductMetas = PostMeta::with(['post.metas.customField'])
                ->whereIn('value', $subcategoryIds)
                ->where('custom_field_id', $subcategoryProductFieldId)
                ->get();

            $subcategoryProducts = $subcategoryProductMetas->map(function ($subcategoryProductMeta) {
                return $subcategoryProductMeta->post;
            });

            foreach ($categories as $category) {
                foreach ($subcategories as $subcategory) {
                    $subcatProducts = $subcategoryProducts->filter(function ($subcategoryProduct) use ($subcategory) {
                        return $subcategoryProduct && $subcategoryProduct->get_field('subcategoria')->id == $subcategory->id;
                    });

                    $subcategory->products = $subcatProducts->sortBy('order');
                }

                $catSubcategories = $subcategories->filter(function ($subcategory) use ($category) {
                    $subcategoryParent = $subcategory->get_field('categoria');
                    return $subcategory && $category && $subcategoryParent && $subcategoryParent->id == $category->id;
                });

                $catProducts = $categoryProducts->filter(function ($categoryProduct) use ($category) {
                    return $categoryProduct && $category && $categoryProduct->get_field('categoria')->id == $category->id;
                });

                if ($category) {
                    $category->subcategories = $catSubcategories->sortBy('order');
                    $category->products = $catProducts->sortBy('order');
                }
            }

            $brandItem->categories = $categories->sortBy('order');
        }

        
        return view('Front::category', compact(
            'brands',
            'brand',
            'video',
            'videos'
        ));
    }


    private function getCustomFieldParentId($post_type, $fieldName)
    {
        $cf_group = CustomFieldGroup::with(['fields'])->where('post_type', $post_type)->first();

        foreach ($cf_group->fields as $field) {
            if ($field->name == $fieldName) {
                return $field->id;
            }
        }
    }
}