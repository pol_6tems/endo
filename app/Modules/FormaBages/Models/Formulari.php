<?php

namespace App\Modules\FormaBages\Models;

use Illuminate\Database\Eloquent\Model;

class Formulari extends Model
{
    protected $fillable = ['nombre', 'email', 'poblacion', 'organitzacio', 'telefono', 'observaciones', 'promo'];
}
