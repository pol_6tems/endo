<?php


namespace App\Modules\FormaBages\Crawlers;


use DiDom\Document;

class AppEnsenyamentCrawler extends BaseCrawler
{

    private $cookie;

    private $csv;

    public function getCursos()
    {
        $dom = $this->getSpecialDom();

        $javaxViewState = $this->getJavaxViewStat(null, $dom);

        $search0 = $this->search0($javaxViewState);
        $javaxViewState = $this->getJavaxViewStat(null, $search0);

        $this->csv = $this->getCsv($javaxViewState);
        $page = 1;

        $activate = $this->activateFamily($javaxViewState);
        $javaxViewState = $this->getJavaxViewStat($javaxViewState, $activate);

        $cursos = [];

        do {
            echo 'Page: ' . $page . PHP_EOL;

            $domList = $this->getListDom($javaxViewState, $page);
            $javaxViewState = $this->getJavaxViewStat($javaxViewState, $domList);

            $rows = $domList->find('tr.taulaRow');

            echo 'Rows: ' . count($rows) . PHP_EOL;

            foreach ($rows as $key => $row) {
                $cursDom = $this->getRowDom($javaxViewState, $key);

                $javaxViewState = $this->getJavaxViewStat($javaxViewState, $cursDom);

                $tr = $cursDom->find('tr.rf-edt-r-sel.rf-edt-r-act.taulaRow');

                if (count($tr)) {
                    $cursRow = $tr[0];
                    $cursCols = $cursRow->find('td');

                    $curs = [];
                    $cursLinks = null;

                    foreach ($cursCols as $key => $cursCol) {
                        $info = $cursCol->find('.rf-edt-c-cnt')[0];

                        if ($key == 0) {
                            $curs['nom'] = trim(explode("\n", strip_tags($info->innerHtml()))[0]);

                            $cursLinks = $info->find('a');

                            continue;
                        }

                        if ($key == 1) {
                            $curs['familia'] = trim($info->innerHtml());
                            continue;
                        }

                        if ($key == 2) {
                            $curs['nom_centre'] = trim($info->innerHtml());
                        }

                        if ($key == 3) {
                            $curs['municipi_centre'] = trim($info->innerHtml());
                        }

                        if ($key == 4) {
                            $curs['hores'] = trim(str_replace(['horas', '.'], '', $info->innerHtml()));
                            continue;
                        }

                        if ($key == 5) {
                            $type = trim($info->innerHtml());

                            if (!in_array($type, ['CFPM', 'CFPS'])) {
                                echo 'No type: ' . $type . PHP_EOL;
                                $curs = [];
                                break;
                            }
                        }
                    }

                    if (count($curs)) {
                        $curs['formabages_tipus2'] = 'App ensenyament';

                        $csvCurs = $this->csv->where('Nom curs', $curs['nom'])
                            ->where('Municipi', $curs['municipi_centre'])
                            ->where('Família', $curs['familia'])
                            ->first();

                        if ($csvCurs) {
                            $curs['area'] = $csvCurs['Àrea'];
                            $curs['correu_electronic'] = $csvCurs['Correu electrònic'];
                        }

                        foreach ($cursLinks as $link) {
                            if ($link->getAttribute('href') != '#') {
                                $cursLink = $link->getAttribute('href');

                                $moreInfoCurs = $this->getCursQueEstudiarUrl($cursLink);

                                if (count($moreInfoCurs)) {
                                    $cursos[] = array_merge($curs, $moreInfoCurs);
                                }
                            }
                        }
                    }
                }
            }

            $page = 'next';
        } while (count($rows) == 14);

        return $cursos;
    }


    private function getCursQueEstudiarUrl($url)
    {
        echo 'Getting extra course info' . PHP_EOL;
        $cursDom = $this->getSpecialDom('http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/' . $url);

        $mainInfoItems = $cursDom->find('table#panelInfo tr');

        $curs = [];

        foreach ($mainInfoItems as $mainInfoItem) {
            $label = trim($mainInfoItem->find('td.detallLabel label')[0]->innerHtml());
            $cleanValue = trim($mainInfoItem->find('td.detallValue')[0]->innerHtml());
            $linkValue = $mainInfoItem->find('a');

            if ($label == 'Tipo de formación:') {
                $curs['tipus'] = $cleanValue;
                continue;
            }

            if (count($linkValue) && !isset($curs['url'])) {
                $url = $linkValue[0]->getAttribute('href');

                parse_str(parse_url($url)['query'], $queryString);
                $id = $queryString['p_id'];

                // Avoid wrong urls?
                if ($id < 15000) {
                    $curs['url'] = $url;
                }
            }
        }

        $offerInfoItems = $cursDom->find('.rf-dt tbody td.rf-dt-c');

        foreach ($offerInfoItems as $key => $offerInfoItem) {
            if ($key == 1) {
                $curs['modalitat'] = trim($offerInfoItem->innerHtml());
                continue;
            }

            if ($key == 3) {
                $curs['data_inici'] = trim($offerInfoItem->innerHtml());
            }
        }

        return $curs;
    }


    private function getSpecialDom($url = 'http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/index.xhtml')
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'timeout'  => 30,
            'cookies' => true,
            'headers' => [
                "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                "Accept-Encoding" => "gzip, deflate",
                "Accept-Language" => "es-ES,es;q=0.9,ca;q=0.8,en;q=0.7,gl;q=0.6",
                "Cache-Control" => "no-cache",
                "Connection" => "keep-alive",
                "Cookie" => isset($this->cookie) ? $this->cookie : "",
                "Host" => "aplicacions.ensenyament.gencat.cat",
                "Pragma" => "no-cache",
                "Upgrade-Insecure-Requests" => "1",
                "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36"
            ]
        ]);

        try {
            $response = $client->request('GET', $url);

            $cookieJar = $client->getConfig('cookies');
            $cookieJar->toArray();

            $sessionCookie = $cookieJar->getCookieByName('JSESSIONID');

            if ($sessionCookie) {
                $this->cookie = 'JSESSIONID=' . $sessionCookie->getValue();
            }

            $bigIpCookie = $cookieJar->getCookieByName('BIGipServerPOOL_ENS_H02e.1_INTERNET_PROHPM_80');

            if ($bigIpCookie) {
                $this->cookie = $sessionCookie ? $this->cookie . '; BIGipServerPOOL_ENS_H02e.1_INTERNET_PROHPM_80=' . $bigIpCookie->getValue() : $bigIpCookie->getValue();
            }

            return new Document((string)$response->getBody()->getContents(), false);
        } catch (\Exception $e) {
            echo $url . PHP_EOL;

            throw $e;
        }
    }


    private function getListDom($javaxViewState, $page)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'timeout'  => 30,
            'headers' => [
                "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                "Accept-Encoding" => "gzip, deflate",
                "Accept-Language" => "es-ES,es;q=0.9,ca;q=0.8,en;q=0.7,gl;q=0.6",
                "Cache-Control" => "no-cache",
                "Connection" => "keep-alive",
                "Cookie" => isset($this->cookie) ? $this->cookie : "JSESSIONID=0yoOf8OGOBkKkpWF7YihveFRxS8epqQ3gPH5XavLeQEM4vQjMVxj!-746984544",
                "Content-type" => "application/x-www-form-urlencoded;charset=UTF-8",
                "Host" => "aplicacions.ensenyament.gencat.cat",
                "Pragma" => "no-cache",
                "Upgrade-Insecure-Requests" => "1",
                "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
                "Referer" => "http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/index.xhtml"
            ]
        ]);

        try {
            $response = $client->request('POST', 'http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/mapContent.xhtml', [
                    'form_params' => [
                        'resultPanel' => 'resultPanel',
                        'resultPanel:geoserverQuery' => '(ID_PROVINCIA=\'1\' and ID_COMARCA=\'7\') and (ID_TIPUS=\'CFPM\' or ID_TIPUS=\'CFPS\')',
                        'resultPanel:tabPanel-value' => 'llistat',
                        'resultPanel:table:wi' => '',
                        'resultPanel:table:si' => '|||',
                        'javax.faces.ViewState' => $javaxViewState,
                        'javax.faces.source' => 'resultPanel:table:datascroller',
                        'javax.faces.partial.event' => 'rich:datascroller:onscroll',
                        'javax.faces.partial.execute' => 'resultPanel:table:datascroller @component',
                        'javax.faces.partial.render' => '@component',
                        'resultPanel:table:datascroller:page' => $page,
                        'org.richfaces.ajax.component' => 'resultPanel:table:datascroller',
                        'resultPanel:table:datascroller' => 'resultPanel:table:datascroller',
                        'AJAX:EVENTS_COUNT' => '1',
                        'javax.faces.partial.ajax:' => 'true'
                    ]]);

            return new Document((string)$response->getBody()->getContents(), false);
        } catch (\Exception $e) {
            throw $e;
        }
    }


    private function getRowDom($javaxViewState, $key)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'timeout'  => 30,
            'headers' => [
                "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                "Accept-Encoding" => "gzip, deflate",
                "Accept-Language" => "es-ES,es;q=0.9,ca;q=0.8,en;q=0.7,gl;q=0.6",
                "Cache-Control" => "no-cache",
                "Connection" => "keep-alive",
                "Cookie" => isset($this->cookie) ? $this->cookie : "JSESSIONID=0yoOf8OGOBkKkpWF7YihveFRxS8epqQ3gPH5XavLeQEM4vQjMVxj!-746984544",
                "Content-type" => "application/x-www-form-urlencoded;charset=UTF-8",
                "Host" => "aplicacions.ensenyament.gencat.cat",
                "Pragma" => "no-cache",
                "Upgrade-Insecure-Requests" => "1",
                "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
                "Referer" => "http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/index.xhtml"
            ]
        ]);
        try {
            $response = $client->request('POST', 'http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/mapContent.xhtml', [
                'form_params' => [
                    'resultPanel' => 'resultPanel',
                    'resultPanel:geoserverQuery' => '(ID_PROVINCIA=\'1\' and ID_COMARCA=\'7\') and (ID_TIPUS=\'CFPM\' or ID_TIPUS=\'CFPS\')',
                    'resultPanel:tabPanel-value' => 'llistat',
                    'resultPanel:table:wi' => '',
                    'resultPanel:table:si' => "$key,$key|$key||x",
                    'javax.faces.ViewState' => $javaxViewState,
                    'javax.faces.source' => 'resultPanel:table',
                    'javax.faces.partial.event' => 'click',
                    'javax.faces.partial.execute' => 'resultPanel:table @component',
                    'javax.faces.partial.render' => '@component',
                    'javax.faces.behavior.event' => 'selectionchange',
                    'org.richfaces.ajax.component' => 'resultPanel:table',
                    'AJAX:EVENTS_COUNT' => '1',
                    'javax.faces.partial.ajax:' => 'true'
                ]]);

            return new Document((string)$response->getBody()->getContents(), false);
        } catch (\Exception $e) {
            throw $e;
        }
    }


    private function activateFamily($javaxViewState){
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'timeout'  => 30,
            'headers' => [
                "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                "Accept-Encoding" => "gzip, deflate",
                "Accept-Language" => "es-ES,es;q=0.9,ca;q=0.8,en;q=0.7,gl;q=0.6",
                "Cache-Control" => "no-cache",
                "Connection" => "keep-alive",
                "Cookie" => isset($this->cookie) ? $this->cookie : "JSESSIONID=0yoOf8OGOBkKkpWF7YihveFRxS8epqQ3gPH5XavLeQEM4vQjMVxj!-746984544",
                "Content-type" => "application/x-www-form-urlencoded;charset=UTF-8",
                "Host" => "aplicacions.ensenyament.gencat.cat",
                "Pragma" => "no-cache",
                "Upgrade-Insecure-Requests" => "1",
                "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
                "Referer" => "http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/index.xhtml"
            ]
        ]);
        try {
            $response = $client->request('POST', 'http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/mapContent.xhtml', [
                'form_params' => [
                    'j_idt99' => 'j_idt99',
                    'j_idt99:j_idt101' => 'on',
                    'j_idt99:j_idt103' => 'on',
                    'j_idt99:j_idt105' => 'on',
                    'j_idt99:j_idt107' => 'on',
                    'j_idt99:j_idt109' => 'on',
                    'javax.faces.ViewState' => $javaxViewState,
                    'javax.faces.source' => 'j_idt99:j_idt101',
                    'javax.faces.partial.event' => 'click',
                    'javax.faces.partial.execute' => 'j_idt99:j_idt101 @component',
                    'javax.faces.partial.render' => '@component',
                    'javax.faces.behavior.event' => 'valueChange',
                    'org.richfaces.ajax.component' => 'j_idt99:j_idt101',
                    'AJAX:EVENTS_COUNT' => '1',
                    'javax.faces.partial.ajax:' => 'true'
                ]]);

            return new Document((string)$response->getBody()->getContents(), false);
        } catch (\Exception $e) {
            throw $e;
        }
    }


    private function search0($javaxViewState)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'timeout' => 30,
            'headers' => [
                "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                "Accept-Encoding" => "gzip, deflate",
                "Accept-Language" => "es-ES,es;q=0.9,ca;q=0.8,en;q=0.7,gl;q=0.6",
                "Cache-Control" => "no-cache",
                "Connection" => "keep-alive",
                "Cookie" => isset($this->cookie) ? $this->cookie : "",
                "Content-type" => "application/x-www-form-urlencoded;charset=UTF-8",
                "Host" => "aplicacions.ensenyament.gencat.cat",
                "Pragma" => "no-cache",
                "Upgrade-Insecure-Requests" => "1",
                "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
                "Referer" => "http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/index.xhtml"
            ]
        ]);
        try {
            $response = $client->request('POST', 'http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/index.xhtml', [
                'form_params' => [
                    'j_idt15' => 'j_idt15',
                    'j_idt15:families' => 'All',
                    'j_idt15:j_idt27' => '7',
                    'j_idt15:j_idt32' => 'All',
                    'j_idt15:j_idt64' => ['CFPM', 'CFPS'],
                    'j_idt15:j_idt86' => 'Buscar',
                    'javax.faces.ViewState' => $javaxViewState
                ]]);

            return new Document((string)$response->getBody()->getContents(), false);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    private function getJavaxViewStat($javaxViewState, $dom)
    {
        $newJavaxViewStat = count($dom->find('input[name="javax.faces.ViewState"]')) ? $dom->find('input[name="javax.faces.ViewState"]')[0]->getAttribute('value') : null;

        if ($newJavaxViewStat) {
            return $newJavaxViewStat;
        }

        return $javaxViewState;
    }


    private function getCsv($javaxViewState)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'timeout' => 30,
            'headers' => [
                "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                "Accept-Encoding" => "gzip, deflate",
                "Accept-Language" => "es-ES,es;q=0.9,ca;q=0.8,en;q=0.7,gl;q=0.6",
                "Cache-Control" => "no-cache",
                "Connection" => "keep-alive",
                "Cookie" => isset($this->cookie) ? $this->cookie : "",
                "Content-type" => "application/x-www-form-urlencoded;charset=UTF-8",
                "Host" => "aplicacions.ensenyament.gencat.cat",
                "Pragma" => "no-cache",
                "Upgrade-Insecure-Requests" => "1",
                "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
                "Referer" => "http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/index.xhtml"
            ]
        ]);
        try {
            $response = $client->request('POST', 'http://aplicacions.ensenyament.gencat.cat/cat_prof/AppJava/views/mapContent.xhtml', [
                'form_params' => [
                    'j_idt94' => 'j_idt94',
                    'j_idt94:exporta' => 'Exporta',
                    'javax.faces.ViewState' => $javaxViewState
                ]]);

            $csvData = (string)$response->getBody()->getContents();

            $csvLines = explode(PHP_EOL, $csvData);

            $csvCols = [];

            $csvParsed = collect();

            foreach ($csvLines as $key => $csvLine) {
                $array = str_getcsv($csvLine, ';');

                if ($key == 0) {
                    $csvCols = $array;
                    continue;
                }

                $csvRow = [];

                foreach ($array as $keyRow => $item) {
                    if ($item != '') {
                        $csvRow[$csvCols[$keyRow]] = $item;
                    }
                }

                $csvParsed->push(collect($csvRow));
            }

            $csvParsed = $csvParsed->unique();

            return $csvParsed;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}