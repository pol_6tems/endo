<?php


namespace App\Modules\FormaBages\Crawlers;


use DiDom\Document;

class EnsenyamentCrawler extends BaseCrawler
{

    public function getCursos()
    {
        $dom = $this->getDom('http://ensenyament.gencat.cat/ca/arees-actuacio/fp/cercador/filtra-per-comarca/index.html?fitxer=da222541-0a00-11ea-b112-005056924a59');
        /*$this->randomSleep();*/

        $tableRows = $dom->find('#demo tbody tr');

        $cursos = [];

        $cols = [
            'familia',
            'area',
            'nom',
            'nom_centre',
            'municipi_centre',
            'tipus',
            'hores',
            'data_inici',
            'modalitat',
            'url'
        ];

        foreach ($tableRows as $tableRow) {
            $rowCols = $tableRow->find('td');

            $curs = [];

            foreach ($cols as $key => $col) {
                $curs[$col] = str_replace('&amp;', '&', trim($rowCols[$key]->innerHtml()));

                if ($col == 'tipus' && in_array($curs[$col], ['Formació ocupacional', 'Formació Contínua'])) {
                    $curs = [];
                    break;
                }
            }

            if (count($curs)) {
                $curs['formabages_tipus2'] = 'Ensenyament';

                $cursos[] = $curs;
            }
        }

        return $cursos;
    }
}