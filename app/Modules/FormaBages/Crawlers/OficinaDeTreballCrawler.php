<?php


namespace App\Modules\FormaBages\Crawlers;


use DiDom\Document;

class OficinaDeTreballCrawler extends BaseCrawler
{

    private $cookie;


    public function getCursos()
    {
        $this->getSpecialDom();

        $domScripts = $this->getListDom()->find('#contenedorCercadors script');

        foreach ($domScripts as $domScript) {
            $str = $domScript->innerHtml();

            preg_match("/var xmlResultats='(.*?)';/", $str, $matches);

            if (count($matches)) {
                $cursosXml = html_entity_decode($matches[1]);

                if (strpos($cursosXml, '</cursos></formacio>') === false) {
                    $cursosXml .= '</cursos></formacio>';
                }
                break;
            }
        }

        $cursos = [];

        $municipis = [
            '08113' => 'Manresa',
            '08192' => 'Santpedor',
            '08141' => 'Navàs',
            '08218' => 'Sant Joan de Vilatorrada'
        ];

        if (isset($cursosXml)) {
            $xml = new \SimpleXMLElement($cursosXml);

            $xmlCursos = $xml->cursos;

            foreach ($xmlCursos->curs as $xmlCurs) {
                $attrs = $xmlCurs->attributes();

                if (isset($municipis[(string)$attrs['mun']])) {
                    $curs = [
                        'nom' => (string)$attrs['des'],
                        'url' => 'https://www.oficinadetreball.gencat.cat/socfuncions/DetallCurs.do?idCurs=' . (string)$attrs['id'],
                        'municipi_centre' => $municipis[(string)$attrs['mun']],
                        'hores' => (string)$attrs['dur']
                    ];

                    $cursos[] = array_merge($curs, $this->getCurs($curs['url']));
                }
            }

        }

        return $cursos;
    }


    public function getCurs($url)
    {
        echo 'Getting extra course info' . PHP_EOL;

        $document = $this->getDom($url);
        /*$this->randomSleep();*/

        $temari = $this->getTemari($document);
        $centre = $this->getCentre($document);

        if (count($temari) > 0 && strpos($temari[count($temari) - 1], "Horari:") >= 0) {
            $horari = array_pop($temari);
            $horari = explode("Di", $horari); // Tots els dies començen per Di
            array_shift($horari);
            $horari = array_map(function ($value) {
                return "Di" . $value;
            }, $horari);
        }

        $dades = $this->getDades($document);

        if ($centre) {
            return array_merge($centre, $dades, [
                'formabages_tipus' => 'crawler',
                'temari' => $temari,
                'horari' => isset($horari) ? $horari : []
            ]);
        }
    }


    private function getTemari(Document $document) {
        $temaris = $document->find('.temari li');
        $itemsTemari = array();
        foreach($temaris as $temari) {
            $content = trim($temari->text(), " .");
            if ($content != "Temari:" && strlen($content) > 0) {
                $itemsTemari[] = $content;
            }
        }

        return $itemsTemari;
    }


    private function getCentre(Document $document)
    {
        $data = $document->find('.infoDadesDelCentre p');

        foreach ($data as $datum) {
            if (strpos($datum, '<label>Dades Centre:</label>') === false) {
                continue;
            }

            $datum = str_replace('<label>Dades Centre:</label>', '', $datum);

            $datumArray = explode('<br>', $datum);

            $returnArray = [];

            foreach ($datumArray as $item) {
                $text = trim(strip_tags($item));
                if ($text == '') {
                    continue;
                }

                if (!isset($returnArray['nom_centre'])) {
                    $returnArray['nom_centre'] = $text;
                    continue;
                }

                if (!isset($returnArray['adreca_centre'])) {
                    $returnArray['adreca_centre'] = $text;
                    continue;
                }

                if (!isset($returnArray['telefon_centre'])) {
                    $returnArray['telefon_centre'] = $text;
                    continue;
                }

                if (!isset($returnArray['correu_electronic'])) {
                    $returnArray['correu_electronic'] = $text;
                    continue;
                }
            }

            return $returnArray;
        }
    }


    private function getDades(Document $document)
    {
        $listItems = $document->find('.boxholder .box ul li');

        $data = [];

        foreach ($listItems as $listItem) {
            $innerHtml = $listItem->innerHtml();
            if (strpos($innerHtml, '<label>Tipus:</label>') !== false) {
                $data['tipus'] = trim(strip_tags(str_replace('<label>Tipus:</label>', '', $innerHtml)));

                $data['tipus'] = str_replace('Formació continua', 'Formació contínua', $data['tipus']);
                continue;
            }

            if (strpos($innerHtml, '<label>Acció formativa:</label>') !== false) {
                $data['area'] = trim(strip_tags(str_replace('<label>Acció formativa:</label>', '', $innerHtml)));
                continue;
            }

            if (strpos($innerHtml, '<label>Família professional:</label>') !== false) {
                $data['familia'] = ucfirst(mb_strtolower(trim(strip_tags(str_replace('<label>Família professional:</label>', '', $innerHtml)))));
                continue;
            }

            if (strpos($innerHtml, '<label>Modalitat:</label>') !== false) {
                $data['modalitat'] = ucfirst(mb_strtolower(trim(strip_tags(str_replace('<label>Modalitat:</label>', '', $innerHtml)))));
                continue;
            }

            if (strpos($innerHtml, '<label>Data inici del curs - Data final del curs:</label>') !== false) {
                preg_match_all('/\d{2}\/\d{2}\/\d{4}/', $innerHtml, $matches);

                if (isset($matches[0][0])) {
                    $data['data_inici'] = $matches[0][0];
                }

                if (isset($matches[0][1])) {
                    $data['data_fi'] = $matches[0][1];
                }

                continue;
            }

            if (strpos($innerHtml, '<label>Estat curs:</label>') !== false) {
                $data['estat_curs'] = trim(strip_tags(str_replace('<label>Estat curs:</label>', '', $innerHtml)));
                continue;
            }

            if (strpos($innerHtml, '<label>CIFO:</label>') !== false) {
                $data['cifo'] = trim(strip_tags(str_replace('<label>CIFO:</label>', '', $innerHtml)));
                continue;
            }
        }

        return $data;
    }


    private function getSpecialDom($url = 'https://www.oficinadetreball.gencat.cat/socfuncions/CercarCursos.do')
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'timeout'  => 30,
            'cookies' => true,
            'headers' => [
                "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Accept-Encoding" => "gzip, deflate, br",
                "Accept-Language" => "es-ES,es;q=0.9,ca;q=0.8,en;q=0.7,gl;q=0.6",
                "Cache-Control" => "no-cache",
                "Connection" => "keep-alive",
                "Cookie" => isset($this->cookie) ? $this->cookie : "",
                "Host" => "www.oficinadetreball.gencat.cat",
                "Pragma" => "no-cache",
                "Upgrade-Insecure-Requests" => "1",
                "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36"
            ]
        ]);

        try {
            $response = $client->request('GET', $url);

            $cookieJar = $client->getConfig('cookies');
            $cookieJar->toArray();

            $sessionCookie = $cookieJar->getCookieByName('JSESSIONID');

            if ($sessionCookie) {
                $this->cookie = 'JSESSIONID=' . $sessionCookie->getValue();
            }

            $bigIpCookie = $cookieJar->getCookieByName('BIGipServerPOOL_ENS_H02e.1_INTERNET_PROHPM_80');

            if ($bigIpCookie) {
                $this->cookie = $sessionCookie ? $this->cookie . '; BIGipServerPOOL_ENS_H02e.1_INTERNET_PROHPM_80=' . $bigIpCookie->getValue() : $bigIpCookie->getValue();
            }

            return new Document((string)$response->getBody()->getContents(), false);
        } catch (\Exception $e) {
            echo $url . PHP_EOL;

            throw $e;
        }
    }


    private function getListDom()
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'timeout'  => 30,
            'headers' => [
                "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "Accept-Encoding" => "gzip, deflate, br",
                "Accept-Language" => "es-ES,es;q=0.9,ca;q=0.8,en;q=0.7,gl;q=0.6",
                "Cache-Control" => "no-cache",
                "Connection" => "keep-alive",
                "Cookie" => isset($this->cookie) ? $this->cookie : "JSESSIONID=32FA7E699852976B54821F3E15BD3271.LSOCSTX02",
                "Content-type" => "application/x-www-form-urlencoded",
                "Host" => "www.oficinadetreball.gencat.cat",
                "Origin" => "https://www.oficinadetreball.gencat.cat",
                "Pragma" => "no-cache",
                "Upgrade-Insecure-Requests" => "1",
                "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
                "Referer" => "https://www.oficinadetreball.gencat.cat/socfuncions/CercarCursos.do",
                "Sec-Fetch-Mode" => "navigate",
                "Sec-Fetch-Site" => "same-origin",
                "Sec-Fetch-User" => "?1"
            ]
        ]);

        try {
            $response = $client->request('POST', 'https://www.oficinadetreball.gencat.cat/socfuncions/CercarCursos.do', [
                'form_params' => [
                    "idServei" => "",
                    "idioma" => "ca",
                    "idiomaCerca" => "ca",
                    "tipusCurs" => "",
                    "tipusCursDummy" => "",
                    "cod_prov" => "08",
                    "cod_com" => "0907",
                    "cod_muni" => "",
                    "xmlCursos" => "",
                    "palabra" => "",
                    "localitat" => "Bages (Comarca)",
                    "areaConeix" => "",
                    "botoCercaEstudisSubmit" => "Enviar"
                ]]);

            return new Document((string)$response->getBody()->getContents(), false);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}