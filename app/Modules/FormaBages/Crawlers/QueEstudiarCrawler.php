<?php


namespace App\Modules\FormaBages\Crawlers;


use DiDom\Document;

class QueEstudiarCrawler extends BaseCrawler
{
    const BASE_URL = 'http://queestudiar.gencat.cat';


    public function getCursos()
    {
        $mainUrl = 'http://queestudiar.gencat.cat/ca/estudis/cercador-estudis/cercador/index.html';
        $this->randomSleep();

        $p = 1;

        $courses = [];

        do {
            echo 'p: ' . $p . PHP_EOL;

            $url = $mainUrl;

            if ($p > 1) {
                $url .= '?p_numpag=' . $p;
            }

            $document = $this->getDom($url);

            $coursesLinks = $document->find('.CEDU_vertical .titol');

            foreach ($coursesLinks as $coursesLink) {
                $href = $coursesLink->find('a')[0]->attributes(['href'])['href'];
                $title = trim($coursesLink->find('titol_web')[0]->innerHtml());

                $courses[] = [
                    'url' => $href,
                    'nom' => $title
                ];
            }

            $p++;
        } while (count($coursesLinks));

        return $courses;
    }


    public function getCurs($url, $title, $fp = false)
    {
        /*echo 'Getting Course Data' . PHP_EOL;*/

        if (!isset(parse_url($url)['host'])) {
            $url = self::BASE_URL . $url;
        }

        $coursePage = $this->getDom($url);
        $this->randomSleep();

        $notNeededTypes = [
            'Educació infantil (0-3 anys)',
            'Educació infantil (3-6 anys)',
            'Educació primària (6-12 anys)',
            'Educació secundària obligatòria (12-16 anys)',
            'Batxillerat',
            'Formació professional'
        ];

        $courseDetails = [
            'nom' => $title,
            'formabages_tipus' => 'crawler'
        ];

        $details = $coursePage->find('ul.CEDU_nostyle li');

        $tipus0 = null;

        foreach ($details as $key => $detail) {
            $detailInner = $detail->innerHtml();

            if ($key == 0) {
                $tipus0 = json_decode(str_replace('\u00a0', '', json_encode(trim(strip_tags(str_replace(['Ensenyament:', '&nbsp;'], '', $detailInner))))));

                if (!$fp && in_array($tipus0, $notNeededTypes)) {
                    return [];
                }
            }

            if ($key == 1 && strpos($detailInner, 'Grau:') !== false) {
                $tipus = json_decode(str_replace('\u00a0', '', json_encode(trim(str_replace(['<strong>Grau:</strong>', '&nbsp;'], '', $detailInner)))));

                if (strpos($tipus, 'Grau mitjà') !== false && $tipus0 == 'Formació professional') {
                    $courseDetails['tipus'] = 'Cicles d\'FP de grau mitjà';
                }

                if (strpos($tipus, 'Grau superior') !== false && $tipus0 == 'Formació professional') {
                    $courseDetails['tipus'] = 'Cicles d\'FP de grau superior';
                }
            }

            if (strpos($detailInner, 'Família professional:') !== false) {
                $courseDetails['familia'] = ucfirst(json_decode(str_replace('\u00a0', '', json_encode(trim(str_replace(['<strong>Família professional:</strong>', '&nbsp;'], '', $detailInner))))));
            }

            if (strpos($detailInner, 'Modalitat/Família:') !== false) {
                $courseDetails['familia'] = ucfirst(json_decode(str_replace('\u00a0', '', json_encode(trim(str_replace(['<strong>Modalitat/Família:</strong>', '&nbsp;'], '', $detailInner))))));

                if ($courseDetails['familia'] == 'Instruments de la música antiga, excepte orgue') {
                    $courseDetails['familia'] = 'Instruments de la música antiga';
                }
            }

            if (strpos($detailInner, 'Entorn:') !== false) {
                $courseDetails['area'] = ucfirst(json_decode(str_replace('\u00a0', '', json_encode(trim(str_replace(['<strong>Entorn:</strong>', '&nbsp;'], '', $detailInner))))));
            }

            if (isset($tipus0) && $tipus0 == 'Educació d\'adults' && strpos($detailInner, 'Formació:') !== false) {
                $courseDetails['subtipus'] = ucfirst(json_decode(str_replace('\u00a0', '', json_encode(trim(str_replace(['<strong>Formació:</strong>', '&nbsp;'], '', $detailInner))))));
            }
        }

        if (!isset($courseDetails['tipus']) && $tipus0) {
            $courseDetails['tipus'] = ucfirst(($tipus0 ? $tipus0: ''));
        }

        if ((!isset($courseDetails['area']) || !$courseDetails['area']) && $tipus0) {
            $courseDetails['area'] = ucfirst($tipus0);
        }

        if (!isset($courseDetails['tipus']) || !$courseDetails['tipus']) {
            return [];
        }

        $description = $coursePage->find('#div_01 .bulletsGenerals');
        $courseDetails['introduccio'] = trim(preg_replace('/\s+/', ' ', strip_tags($description[0]->innerHtml(), '<br>')));
        $temaris = $coursePage->find('#div_02 ul li');

        $courseDetails['temari'] = [];

        $hores = 0;

        foreach ($temaris as $item) {
            $temari = trim(str_replace('&nbsp', '', strip_tags($item->innerHtml())));
            $courseDetails['temari'][] = $temari;

            preg_match('/\((.*?)\)/', $temari, $match);

            if (count($match)) {
                $temps = trim(str_replace(['(', ')', 'hores', '.', '&nbsp'], '', $match[1]));

                if ($temps) {
                    try {
                        $hores += $temps;
                    } catch (\Exception $e) {
                        $hores = 0;
                        break;
                    }
                }
            }
        }

        if ($hores) {
            $courseDetails['hores'] = $hores;
        }

        $requisits = $coursePage->find('#div_03 .FW_bColCos ul li');

        $courseDetails['requisits'] = [];

        foreach ($requisits as $item) {
            $courseDetails['requisits'][] = trim(str_replace('&nbsp', '', strip_tags($item->innerHtml())));
        }


        $courseDetails['tramits'] = trim(str_replace('&nbsp', '', strip_tags($coursePage->find('#div_04 .bulletsGenerals')[0]->innerHtml(), '<p><br><strong><ul><li>')));

        $courseDetails['continuitat'] = trim(str_replace('&nbsp', '', strip_tags($coursePage->find('#div_05 .FW_bColCos.dotted')[0]->innerHtml(), '<p><br><h3><a><strong><ul><li>')));

        $schools = $this->getCitiesAndSchools($coursePage);

        $schoolsArray = [];

        foreach ($schools as $school) {
            if (count($school)) {
                $schoolsArray[] = array_merge($courseDetails, $school);
            }
        }

        return $schoolsArray;
    }


    private function getCitiesAndSchools(Document $coursePage)
    {
        /*echo 'Getting Cities and Schools' . PHP_EOL;*/

        $addressForm = $coursePage->find('form[name="pregunta"]');

        if (!count($addressForm)) {
            return [];
        }

        $addressForm = $addressForm[0];

        $citiesUrl = $addressForm->find('input[name="direccion"]')[0]->getAttribute('value');

        $citiesOptions = $addressForm->find('option');

        $bagesQuery = null;

        foreach ($citiesOptions as $citiesOption) {
            $citiesQueryString = $citiesOption->attributes(['id'])['id'];
            parse_str(str_replace('?', '', $citiesQueryString), $citiesQuery);

            if (isset($citiesQuery['p_comarca']) && $citiesQuery['p_comarca'] == '07') {
                $bagesQuery = $citiesQueryString;
                break;
            }
        }

        if ($bagesQuery) {
            $citiesPage = $this->getDom(self::BASE_URL . $citiesUrl . $bagesQuery);
            $this->randomSleep();

            $citiesLinks = $citiesPage->find('ul.FW_sLlistaComu li.FW_clear a');

            $citiesAndSchools = [];

            foreach ($citiesLinks as $cityLink) {
                $schoolsUrl = $cityLink->attributes(['href'])['href'];
                $city = $cityLink->innerHtml();

                foreach ($this->getSchools($schoolsUrl, $city) as $school) {
                    $citiesAndSchools[] = array_merge(['municipi_centre' => $city], $school);
                }
            }

            return $citiesAndSchools;
        }

        return [];
    }


    private function getSchools($url, $city)
    {
        /*echo 'Getting Schools' . PHP_EOL;*/

        $schoolsPage = $this->getDom(self::BASE_URL . $url);
        $this->randomSleep();

        $schoolsUls = $schoolsPage->find('#center_1_1 ul.CEDU_horitzontal');

        foreach ($schoolsUls as $schoolUl) {
            $schoolLis = $schoolUl->find('li');

            $schoolData = [];
            foreach ($schoolLis as $key => $schoolLi) {
                $links = $schoolLi->find('a');
                $liInner = $schoolLi->innerHtml();

                if ($key == 0) {
                    preg_match('#\((.*?)\)#', $liInner, $match);

                    $schoolData['nom_centre'] = $schoolLi->find('strong')[0]->innerHtml();
                    $schoolData['id_centre'] = $match[1];

                    continue;
                }

                if ($key == 1) {
                    $schoolData['adreca_centre'] = $liInner;
                    continue;
                }

                if ($key == 2) {
                    $schoolData['codi_postal'] = trim(str_replace([$city, '&#13;'], '', $liInner));
                    continue;
                }

                if ($key == 3 && strpos($liInner, 'Tel.') === 0) {
                    $schoolData['telefon_centre'] = trim(str_replace(['Tel.', '&#13;'], '', $liInner));
                    continue;
                }

                if (count($links) && !isset($schoolData['pagina_web'])) {
                    $schoolData['web_centre'] = $links[0]->attributes(['href'])['href'];
                    continue;
                }

                if (strpos($liInner, 'Horari:') !== false) {
                    $schoolData['modalitat'] = trim(str_replace(['Horari:', '&#13;'], '', $liInner));
                }
            }

            yield $schoolData;
        }
    }
}