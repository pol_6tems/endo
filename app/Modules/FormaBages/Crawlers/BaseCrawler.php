<?php


namespace App\Modules\FormaBages\Crawlers;


use DiDom\Document;
use GuzzleHttp\Client;

abstract class BaseCrawler
{

    protected function randomSleep()
    {
        usleep(20000);
        return;
        try {
            $sleep = random_int(2, 5);
            /*echo 'Random sleep ' . $sleep . PHP_EOL;*/
            sleep($sleep);

        } catch (\Exception $e) {
            /*echo 'Sleep 1' . PHP_EOL;*/
            sleep(1);
        }
    }


    protected function getDom($url, $retry = 0)
    {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
            'timeout'  => 30,

        ]);
        try {
            $response = $client->request('GET', $url);

            return new Document((string)$response->getBody()->getContents(), false);
        } catch (\Exception $e) {
            echo $url . PHP_EOL;

            if ($retry < 3) {
                try {
                    $sleep = random_int(2, 5);
                    /*echo 'Random sleep ' . $sleep . PHP_EOL;*/
                    sleep($sleep);

                } catch (\Exception $e) {
                    /*echo 'Sleep 1' . PHP_EOL;*/
                    sleep(1);
                }

                echo 'Retry get dom' . PHP_EOL;
                return $this->getDom($url, $retry + 1);
            } else {
                throw $e;
            }
        }
    }
}