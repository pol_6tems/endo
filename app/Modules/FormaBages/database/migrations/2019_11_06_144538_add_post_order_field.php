<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostOrderField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('posts', 'order') ) {
            Schema::table('posts', function (Blueprint $table) {
                $table->integer('order')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('posts', 'order') ) {
            Schema::table('posts', function (Blueprint $table) {
                $table->dropColumn('order');
            });
        }
    }
}
