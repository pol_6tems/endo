<?php

namespace App\Modules\FormaBages\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Modules\FormaBages\Models\Formulari;

class FormSubmited extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Formulari $form)
    {
        $this->form = $form;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try {
            return $this->from(config("mail.from.address"))
            ->subject("Formulari enviat")
            ->view('mail')
                ->with([
                    'email' => $this->form->email,
                    'promo' => $this->form->promo,
                    'nombre' => $this->form->nombre,
                    'telefono' => $this->form->telefono,
                    'poblacion' => $this->form->poblacion,
                    'organitzacio' => $this->form->organitzacio,
                    'observaciones' => $this->form->observaciones,
                ]);
        } catch (\Exception $e) {
            dump($e->getMessage());
        }
    }
}
