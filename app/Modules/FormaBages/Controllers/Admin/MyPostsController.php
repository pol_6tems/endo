<?php

namespace App\Modules\FormaBages\Controllers\Admin;

use App\Post;
use App\Models\PostMeta;
use App\Models\CustomField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Admin\PostsController;
use Jenssegers\Date\Date;

class MyPostsController extends PostsController
{

    public function index() {
        $view = parent::index();
        if ($this->post_type == "recurs") {
            $recursos = $view->getData()['items'];
            $recursos = $recursos->sortBy("order")->values();

            return view("Front::admin.index-recursos", [
                "items" => $recursos,
                "status" => $view->getData()['status'],
                "num_trashed" => $view->getData()['num_trashed'],
                "num_pending" => $view->getData()['num_pending'],
                "num_draft" => $view->getData()['num_draft'],
                "home_page_id" => $view->getData()['home_page_id']
            ]);
        }

        if ($this->post_type == "curso" && isset($view->getData()['status']) && $view->getData()['status'] == 'publish') {
            $items = $view->getData()['items'];

            $items = $items->each(function ($item) {
                $item->admin_status = 'publicat';

                $type = $item->get_field('tipus-formacio');

                if ($type && is_object($type) && ($type->post_name == 'formacio-continua' || $type->post_name == 'formacio-ocupacional')) {
                    $fi = $item->get_field('data-final-del-curs');

                    if ($fi) {
                        if (is_integer($item->get_field('id'))) {
                            $item->admin_status = Date::createFromFormat('m/d/Y', $fi)->timestamp >= Date::now()->timestamp ? $item->admin_status : 'caducat';
                        } else {
                            $item->admin_status = Date::createFromFormat('d/m/Y', $fi)->timestamp >= Date::now()->timestamp ? $item->admin_status : 'caducat';
                        }
                    }
                }
            });

            return view('Front::admin.cursos-index', [
                'items' => $items,
                'status' => $view->getData()['status'],
                'num_trashed' => $view->getData()['num_trashed'],
                'num_pending' => $view->getData()['num_pending'],
                'num_draft' => $view->getData()['num_draft'],
                'home_page_id' => $view->getData()['home_page_id'],
            ]);
        }
        return $view;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $view = parent::edit($locale, $id);
        if (is_a($view, 'Illuminate\View\View')) return view('Front::admin.edit', $view->getData());
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $result = parent::store($request);
        Artisan::call('cache:clear');
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $post = Post::findOrFail($id);
        $data = $request->all();
        $post = $this->updatePost($post, $data);
        Artisan::call('cache:clear');

        return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $this->post_type)->with(['message' => __('Post updated successfully')]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = parent::create();
        return view('Front::admin.create', $view->getData());
    }
}
