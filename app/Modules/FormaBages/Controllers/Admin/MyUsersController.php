<?php


namespace App\Modules\FormaBages\Controllers\Admin;


use App\Http\Controllers\Admin\UsersController;
use App\Models\Admin\Rol;
use App\Models\CustomFieldGroup;
use App\Modules\FormaBages\Requests\AdminCreateUserRequest;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Artisan;

class MyUsersController extends UsersController
{
    use ResetsPasswords;


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $level = auth()->user()->rol->level;
        $rols = Rol::where('level', '<=', $level)->get()->sortby('name');

        $cf_groups = CustomFieldGroup::with(['fields'])->where('post_type', $this->section_type)->orderBy('order')->get();

        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'type' => $this->section_type,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'lastname', 'title' => __('Lastname'), 'required' => true],
                ['value' => 'email', 'title' => __('Email'), 'required' => true, 'type' => 'email'],
                ['value' => 'password', 'title' => __('Password'), 'required' => false, 'type' => 'password'],
                ['value' => 'password_confirmation', 'title' => __('Confirm password'), 'required' => false, 'type' => 'password'],
                ['value' => 'role', 'title' => __('Role'), 'type' => 'select', 'required' => true,
                    'datasource' => $rols, 'sel_value' => 'name', 'sel_title' => ['name'], 'sel_search' => true
                ],
            ],
            'cf_groups' => $cf_groups,
        ));
    }



    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function customStore(AdminCreateUserRequest $request) {
        //Si la validación no es correcta redireccionar al formulario con los errores
        $createParams = [
            "name" => $request->name,
            "lastname" => $request->lastname,
            "email" => $request->email,
            "remember_token" => str_random(100),
            "role" => $request->role
        ];

        $hasPwd = false;

        if (isset($request->password) && $request->password && isset($request->password_confirmation)
            && $request->password_confirmation && $request->password == $request->password_confirmation) {
            $hasPwd = true;
            $createParams['password'] = bcrypt($request->password);
        } else {
            $createParams['password'] = bcrypt(str_random(10));
        }

        $user = User::create($createParams);

        if (!$hasPwd) {
            $this->broker()->sendResetLink(['email' => $user->email]);
        }

        if (isset($request->custom_fields)) {
            $this->save_custom_fields(User::find($user->id), $request->custom_fields);
        }

        if ($user) {
            Artisan::call('cache:clear');

            execute_actions('save_user', $user); // Hook
            return redirect()->route('admin.users.edit', ['id' => $user->id])->with('message', __('User added successfully'));
        } else {
            return redirect()->back()->with('error', __('An error occurred while saving the data'));
        }
    }
}