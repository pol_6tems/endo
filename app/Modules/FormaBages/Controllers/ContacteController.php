<?php

namespace App\Modules\FormaBages\Controllers;

use Newsletter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Modules\FormaBages\Models\Formulari;
use App\Modules\FormaBages\Mail\FormSubmited;
use App\Modules\FormaBages\Requests\FormulariRequest;

class ContacteController extends Controller
{
    public function formulari(FormulariRequest $request) {
        $data = request()->all();
        $data['promo'] = (isset(request()->promo)) ? 1 : 0;
        if ($form = Formulari::create($data)) {
            
            Mail::to("formabages@ccbages.cat")->send(new FormSubmited($form));
            $email = $data['email'];

            if (env('MAILCHIMP_APIKEY') != "" && env('MAILCHIMP_LIST_ID') != "") {
                if ( $data['promo'] == 1 && !Newsletter::isSubscribed($email) ) {
                    Newsletter::subscribe($email);
                }
            }

            return redirect()->back()->with(["status" => "success"]);
        }
    }
}
