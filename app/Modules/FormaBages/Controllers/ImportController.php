<?php

namespace App\Modules\FormaBages\Controllers;

use App\Post;
use stdClass;
use App\Language;
use DiDom\Document;
use App\Models\PostMeta;
use App\Models\CustomField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Artisan;
use Maatwebsite\Excel\Excel as ExcelFormat;
use App\Modules\FormaBages\Imports\CursImport;
use App\Modules\FormaBages\Repositories\ImportRepository;

class ImportController extends Controller
{
    protected $cursos;
    protected $especialitats;

    public const MUNICIPIS_BAGES = [
        'Aguilar de Segarra',
        'Artés',
        'Avinyó',
        'Balsareny',
        'Callús',
        'Cardona',
        'Castellbell i el Vilar',
        'Castellfollit del Boix',
        'Castellgalí',
        'Castellnou de Bages',
        'Fonollosa',
        'Gaià',
        'Manresa',
        'Marganell',
        'Monistrol de Montserrat',
        'Mura',
        'Navarcles',
        'Navàs',
        'Pont de Vilomara i Rocafort',
        'Rajadell',
        'Sallent',
        'Sant Feliu Sasserra',
        'Sant Fruitós de Bages',
        'Sant Joan de Vilatorrada',
        'Sant Mateu de Bages',
        'Sant Salvador de Guardiola',
        'Sant Vicenç de Castellet',
        'Santpedor',
        'Súria',
        'Talamanca',
    ];

    protected $added = 0;

    public function import(ImportRepository $repo) {
        foreach($this->cursos as $curs) {            
            // Poblacio Centre
            if ($poblacio = $repo->insertPoblacio($curs['municipi_centre'])) {
                
                // Centre
                $centre = $repo->insertCentre($curs['nom_centre'], $poblacio, [
                    'adreca' => $curs['adreca_centre'],
                    'telefon' => $curs['telefon_centre'],
                    'pagina_web' => $curs['web_centre'],
                ]);

                // Inserim el curs
                $curs = $repo->insertCurs($curs, $centre);
            }
        }

        Excel::import(new CursImport, storage_path('csv/cursos_url.csv'));

        Artisan::call('cache:clear');

        return view('Front::admin.import');
    }

    public function importCSV() {
        // Excel::import(new CursImport, storage_path('csv/formabages-estudiar-catalunya.csv'));
        Excel::import(new CursImport, storage_path('csv/cursos_url.csv'));
    }
}
