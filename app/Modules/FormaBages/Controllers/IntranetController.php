<?php


namespace App\Modules\FormaBages\Controllers;


use App\Http\Controllers\Admin\PostsController;
use App\Http\Controllers\Controller;
use App\Models\CustomField;
use App\Models\Mensaje;
use App\Models\PostMeta;
use App\Modules\FormaBages\Requests\UserPasswordRequest;
use App\Modules\FormaBages\Requests\UserUpdateRequest;
use App\Notifications\EmailNotification;
use App\Post;
use App\User;
use stdClass;

class IntranetController extends Controller
{

    public function index()
    {
        $user = auth()->user();

        $courses = Post::with(['metas.customField', 'translations'])
            ->where('type', 'curso')
            ->where('author_id', $user->id)
            ->whereIn('status', ['publish', 'pending'])
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('Front::intranet.index', compact('courses'));
    }


    public function create()
    {
        $types = get_posts('tipo-formacion')->sortBy('title');
        $subtypes = get_posts('subtipus-formacio')->sortBy('title');
        $families = get_posts('familia-formativa')->sortBy('post_name');
        $centres = get_posts('centre');
        $modalitats = CustomField::where('name', 'modalitat')->first();
        $modalitats = explode("\r\n", json_decode($modalitats->params, true)['choices']);

        $temariCF = CustomField::find(83);
        $horariCF = CustomField::find(85);
        $requisitsCF = CustomField::find(202);

        $user = auth()->user();

        return view('Front::intranet.edit', compact(
            'types',
            'centres',
            'modalitats',
            'temariCF',
            'horariCF',
            'requisitsCF',
            'families',
            'user',
            'subtypes'
        ));
    }


    public function store()
    {
        $data = request()->all();

        $cursData = [
            'status' => 'pending',
            'author_id' => auth()->id(),
            'parent_id' => 0,
            'type' => 'curso'
        ];

        $title = $data['title'];
        $sanitize_title = str_slug($title, '-');

        foreach ($this->_languages as $lang) {
            $aux = $sanitize_title;
            $i = 1;
            while (Post::whereTranslation('post_name', $aux, $lang->code)->exists()) {
                $aux = $sanitize_title . '-' . $i;
                $i++;
            }

            $sanitize_title = $aux;
            $cursData[$lang->code]['post_name'] = $sanitize_title;
            $cursData[$lang->code]['title'] = $title;
            $cursData[$lang->code]['media_id'] = ( !empty($data['media_id']) ) ? $data['media_id'] : null;

            if (!isset($data['template']) || isset($data[$lang->code]['template'])) {
                $cursData['template'] = isset($data[$lang->code]['template']) ? $data[$lang->code]['template'] : null;
            } else {
                $cursData[$lang->code]['template'] = $data['template'] ?? $data[$lang->code]['template'];
            }
        }

        $cfData = $data;
        unset($cfData['_token']);
        unset($cfData['title']);

        if (isset($cfData['data-no-definitiva']) && $cfData['data-no-definitiva']) {
            $cfData['data-no-definitiva'] = true;
        }

        if (isset($cfData['tipus-formacio-proposta']) && $cfData['tipus-formacio-proposta']) {
            $typeProposal = $cfData['tipus-formacio-proposta'];
            unset($cfData['tipus-formacio-proposta']);
        }

        if (isset($cfData['subtipus-formacio-proposta']) && $cfData['subtipus-formacio-proposta']) {
            $subtypeProposal = $cfData['subtipus-formacio-proposta'];
            unset($cfData['subtipus-formacio-proposta']);
        }

        if (isset($cfData['familia-professional-proposta']) && $cfData['familia-professional-proposta']) {
            $familyProposal = $cfData['familia-professional-proposta'];
            unset($cfData['familia-professional-proposta']);
        }

        if (isset($cfData['observacions']) && $cfData['observacions']) {
            $observations = $cfData['observacions'];
            unset($cfData['observacions']);
        }

        $curs = Post::create($cursData);

        foreach ($cfData as $key => $cfDatum) {
            if ($key == 'custom_fields') {
                foreach ($this->_languages as $lang) {
                    app(PostsController::class)->save_custom_fields($curs, $cfDatum, $lang->code);
                }
            } else {
                $curs->set_field($key, $cfDatum);
            }
        }

        if (isProEnv()) {
            $this->notifyNewCourse($curs, isset($typeProposal) ? $typeProposal : null, isset($familyProposal) ? $familyProposal : null, isset($observations) ? $observations : null, isset($subtypeProposal) ? $subtypeProposal : null);
        }


        return redirect(route('intranet.edit', ['id' => $curs->id]))->with(['success' => __('Curs creat correctament')]);
    }


    public function edit($locale, $id)
    {
        $course = Post::findOrFail($id);

        $user = auth()->user();

        if ($course->author_id != $user->id) {
            abort(403);
        }

        $types = get_posts('tipo-formacion')->sortBy('title');
        $subtypes = get_posts('subtipus-formacio')->sortBy('title');
        $families = get_posts('familia-formativa')->sortBy('post_name');
        $centres = get_posts('centre');
        $modalitats = CustomField::where('name', 'modalitat')->first();
        $modalitats = explode("\r\n", json_decode($modalitats->params, true)['choices']);

        $temariCF = CustomField::find(83);
        $horariCF = CustomField::find(85);
        $requisitsCF = CustomField::find(202);

        return view('Front::intranet.edit', compact(
            'course',
            'types',
            'centres',
            'modalitats',
            'temariCF',
            'horariCF',
            'requisitsCF',
            'families',
            'user',
            'subtypes'
        ));
    }


    public function update($locale, $id)
    {
        $course = Post::findOrFail($id);

        $user = auth()->user();

        if ($course->author_id != $user->id) {
            abort(403);
        }

        $data = request()->all();

        $cursData = [];

        $title = $data['title'];
        $sanitize_title = str_slug($title, '-');

        foreach ($this->_languages as $lang) {
            $aux = $sanitize_title;
            $i = 1;
            while (Post::whereTranslation('post_name', $aux, $lang->code)->where('id', '<>', $course->id)->where('pending_id', '<>', $course->pending_id)->exists()) {
                $aux = $sanitize_title . '-' . $i;
                $i++;
            }

            $sanitize_title = $aux;
            $cursData[$lang->code]['post_name'] = $sanitize_title;
            $cursData[$lang->code]['title'] = $title;
            $cursData[$lang->code]['media_id'] = ( !empty($data['media_id']) ) ? $data['media_id'] : null;

            if (!isset($data['template']) || isset($data[$lang->code]['template'])) {
                $cursData['template'] = isset($data[$lang->code]['template']) ? $data[$lang->code]['template'] : null;
            } else {
                $cursData[$lang->code]['template'] = $data['template'] ?? $data[$lang->code]['template'];
            }
        }

        $cfData = $data;
        unset($cfData['_token']);
        unset($cfData['title']);
        unset($cfData['_method']);

        $course->update($cursData);

        if (isset($cfData['data-no-definitiva']) && $cfData['data-no-definitiva']) {
            $cfData['data-no-definitiva'] = true;
        } else {
            $cfData['data-no-definitiva'] = false;
        }

        foreach ($cfData as $key => $cfDatum) {
            if ($key == 'custom_fields') {
                foreach ($this->_languages as $lang) {
                    app(PostsController::class)->save_custom_fields($course, $cfDatum, $lang->code);
                }
            } else {
                $course->set_field($key, $cfDatum);
            }
        }

        return redirect(route('intranet.edit', ['id' => $course->id]))->with(['success' => __('Curs actualitzat correctament')]);
    }


    public function destroy($locale, $id)
    {
        $course = Post::findOrFail($id);

        $user = auth()->user();

        if ($course->author_id != $user->id) {
            abort(403);
        }

        app(PostsController::class)->destroy($locale, $id);

        return redirect('intranet.index')->with(['success' => __('Curs eliminat correctament')]);
    }


    public function duplicate($locale, $id)
    {
        $course = Post::findOrFail($id);

        $user = auth()->user();

        if ($course->author_id != $user->id) {
            abort(403);
        }

        $new = $this->realDuplicate($id);

        $new->update(['status' => 'pending']);

        if (isProEnv()) {
            $this->notifyNewCourse($new);
        }

        return redirect('intranet.index')->with(['success' => __(':name duplicated successfully', ['name' => __(ucfirst('Curs'))])]);
    }


    public function user()
    {
        return view('Front::intranet.user');
    }


    public function postUser(UserUpdateRequest $request, $locale)
    {
        $user = auth()->user();

        $user->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        return redirect()->route('intranet.user')->with('success', __('Dades actualitzades correctament'));
    }


    public function postUserPwd(UserPasswordRequest $request, $locale)
    {
        $user = auth()->user();

        $user->update([
            'password' => bcrypt($request->password)
        ]);

        return redirect()->route('intranet.user')->with('success', __('Dades actualitzades correctament'));
    }


    public function resources()
    {
        $docs = get_posts('recurs-intranet');
        return view('Front::intranet.resources', compact('docs'));
    }

    private function realDuplicate($id)
    {
        $post = Post::find($id);
        $data = [
            'type' => $post->type,
            'status' => $post->status,
            'template' => $post->template,
            'author_id' => auth()->id(),
        ];
        foreach ($this->_languages as $lang) {
            $trans = $post->getTranslation($lang->code);
            $exists = true;
            $i = 1;
            $name = $trans->post_name . "-$i";
            while ($exists) {
                if (!Post::whereTranslation('post_name', $name, $lang->code)->exists()) {
                    $exists = false;
                } else {
                    $i++;
                    $name = $trans->post_name . "-$i";
                }
            }
            $data[$lang->code] = [
                'title' => $trans->title . ' (copy)',
                'description' => $trans->description,
                'post_name' => $name,
                'media_id' => $trans->media_id,
            ];
        }

        $new = Post::create($data);

        // Duplicate post_meta
        $custom_fields = PostMeta::where('post_id', $id)->get();
        foreach ($custom_fields as $field) {
            $new_field = [
                'post_id' => $new->id,
                'custom_field_id' => $field->custom_field_id,
                'value' => $field->getOriginal('value'),
                'file' => $field->file,
                'locale' => $field->locale,
                'parent_id' => $field->parent_id
            ];

            if (isset($field->order)) {
                $new_field['order'] = $field->order;
            }

            PostMeta::create($new_field);
        }

        return $new;
    }


    private function notifyNewCourse($curs, $typeProposal = null, $familyProposal = null, $observations = null, $subtypeProposal = null)
    {
        $from = User::where('email', 'pol@6tems.com')->first();
        $to = User::where('email', 'formabages@ccbages.cat')->first();

        /*$to = User::where('email', 'sergi.solsona@6tems.com')->first();*/

        if ($from && $to) {
            $msgParams = [
                'centre' => auth()->user()->fullname(),
                'Títol del curs' => $curs->title
            ];

            if (isset($typeProposal) && $typeProposal) {
                $msgParams['Proposta tipus de formació'] = $typeProposal;
            }

            if (isset($subtypeProposal) && $subtypeProposal) {
                $msgParams['Proposta subtipus de formació'] = $subtypeProposal;
            }

            if (isset($familyProposal) && $familyProposal) {
                $msgParams['Proposta família professional'] = $familyProposal;
            }

            if (isset($observations) && $observations) {
                $msgParams['Observacions'] = $observations;
            }

            $mensaje = new Mensaje();
            $mensaje->user_id = 0;
            $mensaje->mensaje = '';
            $mensaje->fitxer = '';
            $mensaje->email = '';
            $mensaje->name = '';
            $mensaje->type = 'email';
            $mensaje->params = json_encode($msgParams);

            $params = new stdClass();
            $params->usuario = $to;
            $params->mensaje = $mensaje;
            $params->from = $from;
            $params->email_id = 1;

            $notificacion = new EmailNotification($params);
            if ($notificacion->check_email()) {
                $to->notify($notificacion);
            }

            $narcisTo = User::where('email', 'narcis@6tems.com')->first();

            $params->usuario = $narcisTo;
            $notificacion = new EmailNotification($params);
            if ($notificacion->check_email()) {
                $narcisTo->notify($notificacion);
            }
        }
    }
}