<?php

namespace App\Modules\FormaBages\Imports;

use App\Post;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use App\Modules\FormaBages\Controllers\ImportController;
use App\Modules\FormaBages\Repositories\ImportRepository;

class CursImport implements ToCollection, WithCustomCsvSettings, WithHeadingRow
{
    use Importable;

	protected $output;

    function withOutput($output) {
        $this->output = $output;
		return $this;
    }
	
    public function collection(Collection $rows)
    {
        $repo = new ImportRepository();
        $cursos = $repo->getCursosJSON();
		
		$bar = $this->output->createProgressBar(count($cursos) + count($rows));
        $bar->start();

        foreach ($rows as $row) {
            $curs = $repo->importRow($row);
			$bar->advance();
        }
		
        // Import from -> https://www.oficinadetreball.gencat.cat/opendata/recursos/ofertaCursos.json
        foreach($cursos as $curs) {
            // Poblacio Centre
            if ($poblacio = $repo->insertPoblacio($curs['municipi_centre'])) {
                
                // Centre
                $centre = $repo->insertCentre($curs['nom_centre'], $poblacio, [
                    'adreca' => $curs['adreca_centre'],
                    'telefon' => $curs['telefon_centre'],
                    'pagina_web' => $curs['web_centre'],
                ]);

                // Inserim el curs
                $curs = $repo->insertCurs($curs, $centre);
            }
			$bar->advance();
        }
		
		$bar->finish();
    }
    
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }
}