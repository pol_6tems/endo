<?php


use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;

function getDayAndMonthCat($date, $format = 'd/m/Y') {
    if (! is_null($date) && $date != '') {
        if (validateValue($date, "date_format:$format")) {
            $date = Date::createFromFormat($format, $date);
            if(app()->getLocale() == 'ca') {
                if(isVowel($date->format('F')[0])) {
                    return $date->format('d \d\'F \d\e Y');
                }
            }
            return $date->format('d \d\e F \d\e Y');
        } else if (validateValue($date, "date_format:m/d/Y")) {
            $date = Date::createFromFormat("m/d/Y", $date);
            if(app()->getLocale() == 'ca') {
                if(isVowel($date->format('F')[0])) {
                    return $date->format('d \d\'F \d\e Y');
                }
            }
            return $date->format('d \d\e F \d\e Y');
        }
    }
    
    return null;
}


function countCourses($custom_field, $categoria) {
    $cursos = collect();
    $aux = array("metas" => [], "metas_or" => []);

    foreach(request()->all() as $key => $f) {
        if ($key != "page") {
            $items = $f;

            if (is_string($items)) {
                $items = explode(',', $items);
            }
            if ($items) {
                foreach ($items as $item) {
                    $post = \App\Post::whereTranslation("post_name", $item)->first();
                    if ($post) {
                        $aux["metas"][] = array($key, $post->id);
                        if (!in_array($key, $aux["metas_or"])) {
                            array_push($aux["metas_or"], $key);
                        }
                    }
                }
            }
            
        }
    }

    if (is_string($categoria)) {
        $cursos = get_posts([
            'post_type' => 'curso',
            'metas' => [ ['modalitat', $categoria] ],
            'with' => ['metas.customField']
        ], false);

        $cursos = filterOldCourses($cursos);
        return $cursos->count();
    } else {
        if ($custom_field == "localitzacio" || $custom_field == "centre") {
            return get_posts([
                'post_type' => 'centre',
                'metas' => [ ['poblacion', $categoria->id] ],
                'with' => ['metas.customField']
            ], false)->reduce(function($carry, $item) use ($custom_field, $categoria) {
                $posts = get_posts([
                    'post_type' => 'curso',
                    'metas' => [ ['centre', $item->id] ],
                    'with' => ['metas.customField']
                ], false);

                // $posts = currentFiltersFilter($posts, $custom_field, $categoria);

                return $carry + filterOldCourses($posts)->count();
            });
        } else {
            /*if ($custom_field == 'tipus-formacio' && $categoria->post_name == 'ensenyaments-didiomes') {
                $cursos = \App\Post::select(DB::raw('posts.*'))
                    ->withTranslation()
                    ->with(['metas.customField'])
                    ->ofType('curso')
                    ->ofStatus('publish')
                    ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
                    ->where(function ($q) {
                        $q->where('post_translations.post_name', 'like', '%angles%')
                            ->orWhere('post_translations.post_name', 'like', '%catala%')
                            ->orWhere('post_translations.post_name', 'like', '%castella%')
                            ->orWhere('post_translations.post_name', 'like', '%frances%')
                            ->orWhere('post_translations.post_name', 'like', '%alemany%')
                            ->orWhere('post_translations.post_name', 'like', '%italia%')
                            ->orWhere('post_translations.post_name', 'like', '%rus%')
                            ->orWhere('post_translations.post_name', 'like', '%xines%')
                            ->orWhere('post_translations.post_name', 'like', '%japones%')
                            ->orWhere('post_translations.post_name', 'like', '%ucraines%')
                            ->orWhere('post_translations.post_name', 'like', '%suiss%')
                            ->orWhere('post_translations.post_name', 'like', '%portugues%')
                            ->orWhere('post_translations.post_name', 'like', '%grec%')
                            ->orWhere('post_translations.post_name', 'like', '%polones%')
                            ->orWhere('post_translations.post_name', 'like', '%suec%')
                            ->orWhere('post_translations.post_name', 'like', '%romanes%')
                            ->orWhere('post_translations.post_name', 'like', '%marroqui%');
                    })->get();
            } else {*/
                $cursos = get_posts([
                    'post_type' => 'curso',
                    'metas' => [[$custom_field, $categoria->id]],
                    'with' => ['metas.customField']
                ], false);
            /*}*/

            // $cursos = currentFiltersFilter($cursos, $custom_field, $categoria);
            return filterOldCourses($cursos)->count();
        }
    }
}

function currentFiltersFilter($cursos, $custom_field, $categoria) {
    $aux = collect();
    foreach(request()->all() as $key => $f) {
        $aux[$key] = explode(",", $f);
    }

    $aux = $aux->filter(function($value, $key) use ($custom_field) {
        return $key != $custom_field;
    });
    
    $current_cat = (is_string($categoria)) ? $categoria : $categoria->type;

    $cf = \App\Models\CustomField::where("name", $custom_field)->first();
    if ($cf && $params = json_decode($cf->params)) {
        if (isset($params->post_object)) {
            $cp = \App\Models\CustomPost::find($params->post_object);
            $custom_field = $cp->post_type;
        }
    }
    
    if ($aux->count() > 0 ) {
        $cursos = $cursos->filter(function($item) use ($aux) {
            return $aux->every(function($value, $key) use ($item) {
                $field = $item->get_field($key);
                return in_array($field->post_name, $value);
            });
        });
    }

    return $cursos;
}

function filterOldCourses($cursos) {
    /*return $cursos;*/
    return $cursos->filter(function($curs) {
        $type = $curs->get_field('tipus-formacio');
        $creationType = $curs->get_field('tipus');

        if (($type && is_object($type) && ($type->post_name == 'formacio-continua' || $type->post_name == 'formacio-ocupacional')) || $creationType != 'crawler') {
            $fi = $curs->get_field('data-final-del-curs');

            if ($fi) {
                if (is_integer($curs->get_field('id'))) {
                    return Date::createFromFormat('m/d/Y', $fi)->timestamp >= Date::now()->timestamp;
                }
                return Date::createFromFormat('d/m/Y', $fi)->timestamp >= Date::now()->timestamp;
            }
        }

        return true;
    });
}

function print_course_date($item, $data, $short = true) {
    if ($short) {
        if (validateValue($data, "date_format:d/m/Y")) {
            $date = Date::createFromFormat("d/m/Y", $data);
            return $item->get_field('data-no-definitiva') ? ucfirst($date->format('F')) . " " . __("(data per confirmar)") : $date->format('d/m/Y');
        }
    } else {
        if (validateValue($data, "date_format:d/m/Y")) {
            $date = Date::createFromFormat("d/m/Y", $data);
            return $item->get_field('data-no-definitiva') ? ucfirst($date->format('F')) . " " . __("(data per confirmar)") : getDayAndMonthCat($data);
        }
    }
    return $data;
}


function centre_title($title)
{
    $title = $title == mb_strtoupper($title) ? ucwords(mb_strtolower($title), " -\t\r\n\f\v") : $title;

    $title = str_replace('Emi-Manresa', 'EMI-Manresa', $title);

    return $title;
}