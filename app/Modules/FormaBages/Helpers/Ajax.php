<?php

use App\Post;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;

function ajax_no_priv_loadCursos() {}
function loadCursos($parameters) {
    $query = ['post_type' => 'curso'];
    $currentPage = isset($parameters['page']) ? $parameters['page'] : 1;
    $order = isset($parameters['order']) ? $parameters['order'] : 'default';
    $perPage = 10;

    Paginator::currentPageResolver(function () use ($currentPage) {
        return $currentPage;
    });

    if ( !empty($parameters['metas_or']) ) {
        $query['metas_or'] = $parameters['metas_or'];
    }

    $query['with'] = ['metas.customField'];
    
    if ( !empty($parameters['metas']) ) {
        $filtered = $posts = collect();

        $pMetas = $parameters['metas'];

        $filteredGroups = [];
        $prevMeta = null;

        $addLanguages = false;

        foreach($pMetas as $i => $meta) {
            $metas = [];
            $metas_or = [];

            $custom_field = $meta[0];
            $value = $meta[1];
            if ($custom_field == 'localitzacio') {
                $centres = get_posts([
                    'post_type' => 'centre',
                    'metas' => [ ['poblacion', $value] ],
                    'with' => ['metas.customField']
                ])->pluck("id");

                $metas_or[] = 'centre';
                foreach($centres as $centre) {
                    $metas[] = [ 'centre', $centre ];
                }
                unset($parameters['metas'][$i]);
            } else if ($custom_field == 'familia-formativa') {
                $metas_or[] = 'familia-formativa';
                $metas[] = [ 'familia-formativa', $value ];

                unset($parameters['metas'][$i]);
            }

            if ($custom_field == 'tipus-formacio') {
                $tipusFormacio = \App\Post::find($value);

                if ($tipusFormacio->post_name == 'ensenyaments-didiomes') {
                    //$addLanguages = true;
                }
            }

            if (count($metas) > 0) {
                $aux = get_posts([
                    'post_type' => 'curso',
                    'metas' => $metas,
                    'metas_or' => $metas_or,
                    'with' => ['metas.customField']
                ]);
                $aux = filterOldCourses($aux);

                if (!array_key_exists($custom_field, $filteredGroups)) {
                    $filteredGroups[$custom_field] = $aux;
                } else {
                    $filteredGroups[$custom_field] = $filteredGroups[$custom_field]->merge($aux);
                }
            }
        }

        if (count($filteredGroups)) {
            foreach ($filteredGroups as $filteredGroup) {
                if (!$filtered->count()) {
                    $filtered = $filteredGroup;
                } else {
                    $filtered = $filtered->intersect($filteredGroup);
                }
            }
        }

        $query['metas'] = $parameters['metas'];
        $cursos = get_posts($query);

        if ($addLanguages) {
            $langCursos = \App\Post::select(DB::raw('posts.*'))
                ->withTranslation()
                ->with(['metas.customField'])
                ->ofType('curso')
                ->ofStatus('publish')
                ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
                ->where(function ($q) {
                    $q->where('post_translations.post_name', 'like', '%angles%')
                        ->orWhere('post_translations.post_name', 'like', '%catala%')
                        ->orWhere('post_translations.post_name', 'like', '%castella%')
                        ->orWhere('post_translations.post_name', 'like', '%frances%')
                        ->orWhere('post_translations.post_name', 'like', '%alemany%')
                        ->orWhere('post_translations.post_name', 'like', '%italia%')
                        ->orWhere('post_translations.post_name', 'like', '%rus%')
                        ->orWhere('post_translations.post_name', 'like', '%xines%')
                        ->orWhere('post_translations.post_name', 'like', '%japones%')
                        ->orWhere('post_translations.post_name', 'like', '%ucraines%')
                        ->orWhere('post_translations.post_name', 'like', '%suiss%')
                        ->orWhere('post_translations.post_name', 'like', '%portugues%')
                        ->orWhere('post_translations.post_name', 'like', '%grec%')
                        ->orWhere('post_translations.post_name', 'like', '%polones%')
                        ->orWhere('post_translations.post_name', 'like', '%suec%')
                        ->orWhere('post_translations.post_name', 'like', '%romanes%')
                        ->orWhere('post_translations.post_name', 'like', '%marroqui%');
                })->get();

            $cursos = $cursos->merge($langCursos);
            $cursos->unique();
        }

        $filtered = $filtered->unique();

        $cursos = $filtered->count() ? $filtered->intersect($cursos) : $cursos;
    } else {
        $cursos = get_posts($query);
    }
    
    $cursos = filterOldCourses($cursos);

    // Ordenar per Nom
    if ($order == 'duracio') {
        $cursos = $cursos->sortByDesc(function ($curs, $key) use ($order) {
            $int = 0;

            $duration = $curs->get_field('duracio');

            if ($duration) {
                $int = (int)filter_var($duration, FILTER_SANITIZE_NUMBER_INT);
            }

            return $int . '_' . $curs->title;
        }, SORT_NATURAL|SORT_FLAG_CASE);
    } else {
        $cursos = $cursos->sortBy(function ($curs, $key) use ($order) {
            switch ($order) {
                case 'data_inici':
                    $dataInici = $curs->get_field('data-inici-del-curs');
                    $date = Carbon::now()->addCentury();

                    if ($dataInici) {
                        $date = Carbon::createFromFormat('d/m/Y', $dataInici);
                    }

                    return $date->getTimestamp() . '_' . $curs->title;
                    break;
                case 'alphabetical':
                    return $curs->title;
                    break;
                default:
                    $type = $curs->get_field('tipus-formacio');
                    $order = $type && is_object($type) ? $type->order : 9999;

                    return $order . '_' . $curs->title;
            }
        }, SORT_NATURAL | SORT_FLAG_CASE);
    }

    if ($query = $parameters["search_query"]) {
        $cursos = $cursos->filter(function($element) use ($query) {
            $cifo = $element->get_field('cifo');
            $estat = $element->get_field('estat-curs');
            $organizador = $element->get_field('organizador');
            $id = $element->get_field('identificador-del-curs');
            $centre = $element->get_field('centre');

            $result =   like_match("%$query%", $element->title) ||
                        like_match("%$query%", $element->post_name) ||
                        like_match("%$query%", $element->description) ||
                        like_match("%$query%", $cifo) ||
                        like_match("%$query%", $estat) ||
                        like_match("%$query%", $organizador) ||
                        like_match("%$query%", $id) ||
                        ($centre && like_match("%$query%", $centre->title));

            return $result;
        });
    }

    $filterGroups = cacheable(\App\Modules\FormaBages\Repositories\FiltersRepository::class, 10)->getFilters(true);
    $cfs = [
        'tipos_formacion' => 'tipus-formacio',
        'subtipus-formacio' => 'subtipus-formacio',
        'familia_formativa' => 'familia-formativa',
        'arees_formacio' => 'area-formativa',
        'poblacions' => 'centre',
        'modalitats' => 'modalitat'
    ];

    $nonFilteredCfs = [];

    if (isset($parameters['metas_or']) && $parameters['metas_or'] && count($parameters['metas_or']) == 1) {
        $requestCf = $parameters['metas_or'][0];

        if ($requestCf == 'localitzacio') {
            $requestCf = 'centre';
        }

        foreach ($cfs as $key => $cf) {
            if ($cf == $requestCf) {
                $nonFilteredCfs[$key] = $cf;
                unset($cfs[$key]);
                break;
            }
        }
    }

    $filterCounts = [];

    foreach ($filterGroups as $key => $filterGroup) {
        if (!isset($cfs[$key])) {
            if (count($nonFilteredCfs) == 1 && isset($nonFilteredCfs[$key])) {
                foreach ($filterGroup as $filter) {
                    if (is_object($filter)) {
                        $filterCounts[$filter->id] = isset($filter->count_courses) ? $filter->count_courses : countCourses($nonFilteredCfs[$key], $filter);
                    } else {
                        $filterCounts[$filter] = 0;
                    }
                }
            }

            continue;
        }

        foreach ($filterGroup as $filter) {
            if (is_object($filter)) {
                $filterCounts[$filter->id] = 0;
            } else {
                $filterCounts[$filter] = 0;
            }
        }
    }

    $tipusIdiomes = Post::publish()
        ->ofType('tipo-formacion')
        ->whereTranslation('post_name', 'ensenyaments-didiomes')
        ->first();

    foreach ($cursos as $curs) {
        foreach ($cfs as $cf) {
            $cursFilter = $curs->get_field($cf);

            if ($cursFilter && $cf == 'centre') {
                $cursFilter = $cursFilter->get_field('poblacion');
            }

            if ($cursFilter) {
                if (is_object($cursFilter) && isset($filterCounts[$cursFilter->id])) {
                    $filterCounts[$cursFilter->id] = $filterCounts[$cursFilter->id] + 1;

                    /*if ($cursFilter->type == 'tipo-formacion' && $cursFilter->id != $tipusIdiomes->id && (
                        strpos($curs->post_name, 'catala') !== false
                            || strpos($curs->post_name, 'castella') !== false
                            || strpos($curs->post_name, 'angles') !== false
                            || strpos($curs->post_name, 'frances') !== false
                            || strpos($curs->post_name, 'alemany') !== false
                            || strpos($curs->post_name, 'italia') !== false
                            || strpos($curs->post_name, 'rus') !== false
                            || strpos($curs->post_name, 'xines') !== false
                            || strpos($curs->post_name, 'japones') !== false
                            || strpos($curs->post_name, 'ucraines') !== false
                            || strpos($curs->post_name, 'suiss') !== false
                            || strpos($curs->post_name, 'portugues') !== false
                            || strpos($curs->post_name, 'grec') !== false
                            || strpos($curs->post_name, 'polones') !== false
                            || strpos($curs->post_name, 'suec') !== false
                            || strpos($curs->post_name, 'romanes') !== false
                            || strpos($curs->post_name, 'marroqui') !== false
                        )) {
                        $filterCounts[$tipusIdiomes->id] = $filterCounts[$tipusIdiomes->id] + 1;
                    }*/
                } else if (!is_object($cursFilter) && isset($filterCounts[$cursFilter])) {
                    $filterCounts[$cursFilter] = $filterCounts[$cursFilter] + 1;
                }
            }
        }
    }
    
    $sliced = $cursos->slice($perPage * ($currentPage - 1), $perPage);
    $cursos = new LengthAwarePaginator($sliced, $cursos->count(), $perPage, $currentPage, [
        'path' => get_archive_link("curso")
    ]);

    if (isset($parameters["format"]) && $parameters["format"] == "json") {
        return $cursos;
    }

    try {
        return [
            'filter_counts' => $filterCounts,
            'view' => view('Front::ajax.curs', compact('cursos'))->render()
        ];
    } catch (Throwable $e) {
        if (!isProEnv()) {
            throw $e;
        }
    }
}

function ajax_no_priv_loadNoticias() {}
function loadNoticias($parameters) {
    $query = ['post_type' => 'noticia'];
    $currentPage = isset($parameters['page']) ? $parameters['page'] : 1;
    $perPage = 10;

    Paginator::currentPageResolver(function () use ($currentPage) {
        return $currentPage;
    });

    if ( !empty($parameters['orwhere']) ) {
        $query['orwhere'] = $parameters['orwhere'];
    }

    if ( !empty($parameters['metas']) ) {
        $query['metas'] = $parameters['metas'];
    }
    
    if ( !empty($parameters['metas_or']) ) {
        $query['metas_or'] = $parameters['metas_or'];
    }

    $noticies = get_posts($query);

    // Ordenar per Data
    $noticies = $noticies->sortByDesc(function($noticia, $key) {
        $date = $noticia->get_field('data-publicacio');
        if ($date) {
            return Date::createFromFormat('d/m/Y', $date)->timestamp;
        }
    });
    
    $sliced = $noticies->slice($perPage * ($currentPage - 1), $perPage);
    $noticies = new LengthAwarePaginator($sliced, $noticies->count(), $perPage, $currentPage, [
        'path' => get_archive_link("noticia")
    ]);

    return view('Front::ajax.noticies', compact('noticies'));
}



function ajax_no_priv_loadAgenda() {}
function loadAgenda($parameters) {
    $query = ['post_type' => 'agenda'];
    $metas_cursos = [];
    $metas_or_cursos = [];

    $currentPage = isset($parameters['page']) ? $parameters['page'] : 1;
    $perPage = 10;

    Paginator::currentPageResolver(function () use ($currentPage) {
        return $currentPage;
    });

    if ( !empty($parameters['orwhere']) ) {
        $metas_or_cursos = $query['orwhere'] = $parameters['orwhere'];
    }

    if ( !empty($parameters['metas']) ) {
        $query['metas'] = $parameters['metas'];
        foreach($query['metas'] as $meta) {
            if ($meta[0] == "publicacio") {
                $metas_cursos[] = ['data-inici-del-curs', $meta[1]];
            }
        }
    }
    
    if ( !empty($parameters['metas_or']) ) {
        $query['metas_or'] = $parameters['metas_or'];
    }

    $cursos = get_posts([
        "post_type" => "curso",
        "metas" => $metas_cursos,
        "orwhere" => $metas_or_cursos,
    ]);

    $agendas = get_posts($query);
    $agendas = $agendas->merge($cursos);

    if (! isset($query['metas']) ) {
        $agendas = $agendas->filter(function($activitat) {
            if ($activitat->type == "curso") $date = $activitat->get_field('data-inici-del-curs');
            else $date = $activitat->get_field('publicacio');

            if ($date) {
                return Date::createFromFormat('d/m/Y', $date)->timestamp > Date::now()->timestamp;
            } else {
                return Carbon::now()->addYears(10)->timestamp;
            }
        });
    }

    // Ordenar per Data
    $agendas = $agendas->sortBy(function($activitat, $key) {
        if ($activitat->type == "curso") $date = $activitat->get_field('data-inici-del-curs');
        else $date = $activitat->get_field('publicacio');

        if ($date) {
            return Date::createFromFormat('d/m/Y', $date)->timestamp;
        } else {
            return Carbon::now()->addYears(10)->timestamp;
        }
    });
    
    $sliced = $agendas->slice($perPage * ($currentPage - 1), $perPage);
    $agendas = new LengthAwarePaginator($sliced, $agendas->count(), $perPage, $currentPage, [
        'path' => get_archive_link("agenda")
    ]);
    
    return view('Front::ajax.agenda', compact('agendas'));
}