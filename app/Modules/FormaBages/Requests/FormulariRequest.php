<?php

namespace App\Modules\FormaBages\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormulariRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:255',
            'email' => 'required',
            'rgpd' => 'required',
        ];
    }

    public function messages()
{
    return [
        'nombre.required' => __('El nombre es obligatorio'),
        'email.required'  => __('El correo electrónico es obligatorio'),
        'rgpd.required'  => __('Debe aceptar las normas de uso y privacidad'),
    ];
}
}
