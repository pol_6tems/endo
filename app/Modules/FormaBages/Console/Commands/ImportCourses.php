<?php

namespace App\Modules\FormaBages\Console\Commands;

use App\Models\PostMeta;
use App\Modules\FormaBages\Crawlers\AppEnsenyamentCrawler;
use App\Modules\FormaBages\Crawlers\EnsenyamentCrawler;
use App\Modules\FormaBages\Crawlers\OficinaDeTreballCrawler;
use App\Modules\FormaBages\Crawlers\QueEstudiarCrawler;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use App\Modules\FormaBages\Repositories\ImportRepository;
use Jenssegers\Date\Date;

class ImportCourses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:courses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importar cursos';


    /**
     * @var ImportRepository
     */
    private $repo;

    /**
     * @var string
     */
    private $fp_ini_date;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    private function init()
    {
        $this->repo = app(ImportRepository::class);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();

        $this->removeOldCourses();

        $this->output->title('Starting import');

        $this->getSocCourses();

        $this->getQueEstudiarCourses();

        $tipus = $this->repo->insertTipoFormacio('Altres');
        $this->repo->insertFamiliaFormativa('Altres', $tipus);

        $this->output->success('Import successful');

        Artisan::call('cache:clear');
    }


    private function removeOldCourses()
    {
        $this->info('Removing old courses');

        $courses = get_posts('curso');

        foreach ($courses as $course) {
            $type = $course->get_field('tipus');

            if ($type != 'crawler') {
                continue;
            }

            $endDate = $course->get_field('data-final-del-curs');

            if ($endDate) {
                continue;
            }

            $type = $course->get_field('tipus-formacio');

            if ($type && ($type->post_name == 'formacio-continua' || $type->post_name == 'formacio-ocupacional')) {
                continue;
            }

            $id = $course->id;

            $course->forceDelete();
            PostMeta::where('post_id', $id)->delete();
        }
    }


    private function getSocCourses()
    {
        $this->info('Importing Ensenyament App courses..');

        $cursosApp = app(AppEnsenyamentCrawler::class)->getCursos();

        $this->info('Importing Ensenyament courses..');
        $cursos = app(EnsenyamentCrawler::class)->getCursos();

        $soc = app(OficinaDeTreballCrawler::class);

        $this->info('Importing SOC courses..');
        $cursosSoc = $soc->getCursos();

        $cursos = array_merge($cursosApp, $cursos, $cursosSoc);

        $queEstudiar = app(QueEstudiarCrawler::class);

        $bar = $this->output->createProgressBar(count($cursos));
        $bar->start();

        // Import from -> https://www.oficinadetreball.gencat.cat/opendata/recursos/ofertaCursos.json
        foreach($cursos as $curs) {
            if (!isset($curs['municipi_centre']) && !isProEnv()) {
                dd($curs);
            }
            // Poblacio Centre
            if ($poblacio = $this->repo->insertPoblacio($curs['municipi_centre'])) {
                unset($area);
                unset($familia);

                // Nova categorització
                [$tipus, $subtipus] = $this->getTypeAndSubtype($curs);

                /*$tipus = $this->repo->insertTipoFormacio($curs['tipus']);*/

                // Familia Formativa
                $familia = $this->getFamiliaFormativa($curs, $tipus);

                // Area
                /*if (isset($curs['area'])) {
                    $area = $this->repo->insertAreaFormativa($curs['area'], $familia);
                }*/

                $title = $curs['nom'];

                /*if (isset($tipus)) {*/
                    $parsedUrl = parse_url($curs['url']);

                    if ($parsedUrl['host'] == 'queestudiar.gencat.cat') {
                        $moreCourses = $queEstudiar->getCurs($curs['url'], $curs['nom'], true);

                        if (!$moreCourses) {
                            echo PHP_EOL;
                            $this->info('No more info');
                            $this->info($curs['nom']);
                            $this->info($curs['url']);
                            $this->warn(' Crawler: ' . isset($curs['formabages_tipus2']) ? $curs['formabages_tipus2'] : 'Not set..');
                            $bar->advance();
                            continue;
                        }

                        $trobatCentre = false;

                        foreach ($moreCourses as $moreCourse) {
                            if (isset($moreCourse['familia']) && $moreCourse['familia'] &&
                                (!isset($familia) || !$familia || $moreCourse['familia'] != $curs['familia'])) {
                                $familia = $this->getFamiliaFormativa($moreCourse, $tipus);
                            }

                            if (isset($curs['nom_centre']) && (strpos($curs['nom_centre'], $moreCourse['nom_centre']) !== false || strpos($moreCourse['nom_centre'], $curs['nom_centre']) !== false)) {
                                $trobatCentre = true;
                            }

                            $this->fp_ini_date = $curs['data_inici'];

                            $centre = $this->repo->insertCentre($moreCourse['nom_centre'], $poblacio, [
                                'adreca' => $moreCourse['adreca_centre'],
                                'telefon' => $moreCourse['telefon_centre'],
                                'pagina_web' => $moreCourse['web_centre'],
                                'correu_electronic' => isset($curs['correu_electronic']) && $curs['correu_electronic'] ? $curs['correu_electronic'] : (isset($moreCourse['correu_electronic']) && $moreCourse['correu_electronic'] ? $moreCourse['correu_electronic'] : ''),
                            ]);

                            if (isset($centre)) {
                                if (isset($curs['data_inici'])) {
                                    $date_ini = Date::createFromFormat(strlen($curs['data_inici']) == 8 ? "d/m/y" : 'd/m/Y', $curs['data_inici']);
                                }

                                if (isset($moreCourse['data_fi'])) {
                                    $date_fi = Date::createFromFormat(strlen($curs['data_fi']) == 8 ? "d/m/y" : 'd/m/Y', $moreCourse['data_fi']);
                                }

                                parse_str($parsedUrl['query'], $queryString);
                                $id = $queryString['p_id'];

                                $cursObj = $this->repo->fillPost("curso", $title, $title . '-' . $id . '-' . $centre->id, [
                                    'duracio' => $curs['hores'] . 'h',
                                    'data-final-del-curs' => isset($date_fi) ? $date_fi->format("d/m/Y") : null,
                                    'data-inici-del-curs' => isset($date_ini) ? $date_ini->format("d/m/Y") : null,
                                    'modalitat' => str_replace('Di&uuml;rn', 'Diürn', ucfirst(strtolower(mb_convert_encoding($curs['modalitat'], 'HTML-ENTITIES', 'UTF-8')))),
                                    'familia-formativa' => isset($familia) && $familia ? $familia->id : null,
                                    'tipus-formacio' => $tipus ? $tipus->id : null,
                                    'subtipus-formacio' => isset($subtipus) && $subtipus ? $subtipus->id : null,
                                    'centre' => $centre->id,
                                    'tipus' => $moreCourse['formabages_tipus'],
                                    'text-tramits' => $moreCourse['tramits'],
                                    'text-continuitat' => $moreCourse['continuitat'],
                                    'introduccio' => isset($moreCourse['introduccio']) ? $moreCourse['introduccio'] : null
                                ]);

                                if ($cursObj->created_at == $cursObj->updated_at) {
                                    $this->repo->fillRepeater($cursObj, 83, 84, $moreCourse['temari']);
                                    $this->repo->fillRepeater($cursObj, 202, 203, $moreCourse['requisits']);
                                }
                            }
                        }

                        if (!$trobatCentre) {
                            $this->warn('Centre ' . $curs['nom_centre'] . ' no trobat per al curs ' . $curs['nom'] . ' crawler: ' . (isset($curs['formabages_tipus2']) ? $curs['formabages_tipus2'] : 'Not set..'));
                        }
                    }

                    if ($parsedUrl['host'] == 'www.oficinadetreball.gencat.cat') {
                        $extraData = $soc->getCurs($curs['url']);

                        $centre = $this->repo->insertCentre($extraData['nom_centre'], $poblacio, [
                            'adreca' => $extraData['adreca_centre'],
                            'telefon' => $extraData['telefon_centre'],
                            'correu_electronic' => $extraData['correu_electronic'],
                        ]);

                        if (isset($centre)) {
                            if (isset($curs['data_inici'])) {
                                $date_ini = Date::createFromFormat(strlen($curs['data_inici']) == 8 ? "d/m/y" : 'd/m/Y', $curs['data_inici']);
                            }

                            if (isset($extraData['data_fi'])) {
                                $date_fi = Date::createFromFormat(strlen($curs['data_fi']) == 8 ? "d/m/y" : 'd/m/Y', $extraData['data_fi']);
                            }

                            parse_str($parsedUrl['query'], $queryString);
                            $id = $queryString['idCurs'];

                            $cursObj = $this->repo->fillPost("curso", $title, $title . '-' . $id, [
                                'duracio' => $curs['hores'] . 'h',
                                'cifo' => isset($extraData['cifo']) ? $extraData['cifo'] : null,
                                'estat-curs' => isset($extraData['estat_curs']) ? $extraData['estat_curs'] : null,
                                'data-final-del-curs' => isset($date_fi) ? $date_fi->format("d/m/Y") : null,
                                'data-inici-del-curs' => isset($date_ini) ? $date_ini->format("d/m/Y") : null,
                                'modalitat' => ucfirst(strtolower(mb_convert_encoding($curs['modalitat'], 'HTML-ENTITIES', 'UTF-8'))),
                                'familia-formativa' => isset($familia) ? $familia->id : null,
                                'tipus-formacio' => $tipus->id,
                                'centre' => $centre->id,
                                'tipus' => $extraData['formabages_tipus']
                            ]);

                            if ($cursObj->created_at == $cursObj->updated_at) {
                                $this->repo->fillRepeater($cursObj, 85, 86, $extraData['horari']);
                                $this->repo->fillRepeater($cursObj, 83, 84, $extraData['temari']);
                            }
                        }
                    }
                /*}*/
            }

            $bar->advance();
        }

        $bar->finish();

        echo PHP_EOL;
        $this->info('Soc Success 🍺');
    }


    private function getQueEstudiarCourses()
    {
        $this->info('Importing "Que estudiar" courses..');

        $queEstudiar = app(QueEstudiarCrawler::class);

        $courses = $queEstudiar->getCursos();

        $bar = $this->output->createProgressBar(count($courses));
        $bar->start();

        foreach ($courses as $course) {
            $cursos = $queEstudiar->getCurs($course['url'], $course['nom']);

            foreach ($cursos as $curs) {
                if ($poblacio = $this->repo->insertPoblacio($curs['municipi_centre'])) {
                    unset($familia);
                    /*unset($area);*/

                    [$tipus, $subtipus] = $this->getTypeAndSubtype($curs);

                    /*$tipus = $this->repo->insertTipoFormacio($curs['tipus']);*/

                    // Familia Formativa
                    $familia = $this->getFamiliaFormativa($curs, $tipus);

                    // Area
                    /*if (isset($curs['area']) && isset($familia)) {
                        $area = $this->repo->insertAreaFormativa($curs['area'], $familia);
                    }*/

                    $title = $curs['nom'];

                    /*if (isset($tipus)) {*/
                        $parsedUrl = parse_url($course['url']);

                        $centre = $this->repo->insertCentre($curs['nom_centre'], $poblacio, [
                            'adreca' => $curs['adreca_centre'],
                            'telefon' => $curs['telefon_centre'],
                            'pagina_web' => $curs['web_centre'],
                        ]);

                        if (isset($centre)) {
                            if (isset($curs['data_inici'])) {
                                $date_ini = Date::createFromFormat(strlen($curs['data_inici']) == 8 ? "d/m/y" : 'd/m/Y', $curs['data_inici']);
                            } else if (isset($this->fp_ini_date)) {
                                $date_ini = Date::createFromFormat(strlen($this->fp_ini_date) == 8 ? "d/m/y" : 'd/m/Y', $this->fp_ini_date);
                            }

                            if (isset($curs['data_fi'])) {
                                $date_fi = Date::createFromFormat(strlen($curs['data_fi']) == 8 ? "d/m/y" : 'd/m/Y', $curs['data_fi']);
                            }

                            parse_str($parsedUrl['query'], $queryString);
                            $id = $queryString['p_id'];

                            $cursObj = $this->repo->fillPost("curso", $title, $title . '-' . $id . '-' . $centre->id, [
                                'duracio' => isset($curs['hores']) ? $curs['hores'] . 'h' : null,
                                'data-final-del-curs' => isset($date_fi) ? $date_fi->format("d/m/Y") : null,
                                'data-inici-del-curs' => isset($date_ini) ? $date_ini->format("d/m/Y") : null,
                                'modalitat' => str_replace('Di&uuml;rn', 'Diürn', ucfirst(strtolower(mb_convert_encoding($curs['modalitat'], 'HTML-ENTITIES', 'UTF-8')))),
                                'familia-formativa' => isset($familia) && $familia ? $familia->id : null,
                                'tipus-formacio' => $tipus ? $tipus->id : null,
                                'subtipus-formacio' => isset($subtipus) && $subtipus ? $subtipus->id : null,
                                'centre' => $centre->id,
                                'tipus' => $curs['formabages_tipus'],
                                'text-tramits' => $curs['tramits'],
                                'text-continuitat' => $curs['continuitat'],
                                'introduccio' => isset($curs['introduccio']) ? $curs['introduccio'] : null
                            ]);

                            if ($cursObj->created_at == $cursObj->updated_at) {
                                $this->repo->fillRepeater($cursObj, 83, 84, $curs['temari']);
                                $this->repo->fillRepeater($cursObj, 202, 203, $curs['requisits']);
                            }
                        }
                    /*}*/
                }
            }

            $bar->advance();
        }

        $bar->finish();

        echo PHP_EOL;
        $this->info('"Que estudiar" Success 🍺');
    }

    private function getTypeAndSubtype($curs)
    {
        if (isset($curs['url'])) {
            $parsedUrl = parse_url($curs['url']);
        }

        foreach (['arts escèniques', 'arts plàstiques i disseny'] as $art) {
            if (mb_strpos(mb_strtolower($curs['tipus']), $art) !== false) {
                $curs['tipus'] = 'Ensenyaments artístics';
            }
        }

        foreach (['dansa', 'música'] as $music) {
            if (mb_strpos(mb_strtolower($curs['tipus']), $music) !== false) {
                $curs['tipus'] = 'Música i Dansa';
            }
        }

        if (in_array(mb_strtolower($curs['tipus']), ['idiomes a les eoi'])) {
            $curs['tipus'] = 'Ensenyaments d’Idiomes';
        }

        if (in_array(mb_strtolower($curs['tipus']), ['cicles d\'fp de grau superior', 'cicles d\'fp de grau mitjà'])) {
            $curs['subtipus'] = $curs['tipus'];
            $curs['tipus'] = 'Formació Professional Reglada';
        }

        if (in_array(mb_strtolower($curs['tipus']), ['ensenyaments artístics', 'música i dansa', 'ensenyaments d’idiomes', 'ensenyaments esportius'])) {
            $curs['subtipus'] = 'Ensenyaments en Règim Especial';
            $curs['tipus'] = 'Formació Professional Reglada';
        }

        if (in_array(mb_strtolower($curs['tipus']), ['cursos per accedir a cicles'])) {
            $curs['subtipus'] = $curs['tipus'];
            $curs['tipus'] = 'Itineraris Formatius per a Joves';
        }

        if (in_array(mb_strtolower($curs['tipus']), ['formació ocupacional']) && isset($parsedUrl) && $parsedUrl['host'] == 'www.oficinadetreball.gencat.cat') {
            $curs['subtipus'] = 'FOAP (SOC)';
        }

        if (in_array(mb_strtolower($curs['tipus']), ['educació d\'adults'])) {
            $curs['tipus'] = 'Escola d\'Adults';
            if (isset($curs['subtipus'])) {
                if (strpos(mb_strtolower($curs['subtipus']), 'llengua') !== false) {
                    $curs['subtipus'] = 'Llengües';
                }

                if (mb_strtolower($curs['subtipus']) == 'educació secundària per a les persones adultes') {
                    $curs['subtipus'] = 'Graduat/da en ESO';
                }

                if (in_array(mb_strtolower($curs['subtipus']), ['preparació per a les proves d\'accés']) || $curs['nom'] == 'Curs específic per a l\'accés als cicles de grau mitjà') {
                    $curs['subtipus'] = 'Preparació per accés a cicles formatius';
                }

                if (mb_strtolower($curs['subtipus']) == 'informàtica') {
                    $curs['subtipus'] = 'Informàtica i noves tecnologies';
                }

                if (mb_strtolower($curs['subtipus']) == 'formació bàsica') {
                    $curs['subtipus'] = 'Cicle de formació instrumental';
                }
            }
        }

        $allowedTypes = [
            'Estudis Universitaris',
            'Formació Professional Reglada',
            'Itineraris Formatius per a Joves',
            'Formació Ocupacional',
            'Formació Contínua',
            'Escola d\'Adults',
            'Formació d\'acollida per persones estrangeres',
            'Formació on-line'
        ];

        foreach ($allowedTypes as $key => $allowedType) {
            $allowedTypes[$key] = mb_strtolower($allowedType);
        }

        if (in_array(mb_strtolower($curs['tipus']), $allowedTypes)) {
            $tipus = $this->repo->insertTipoFormacio($curs['tipus']);

            if (isset($curs['subtipus'])) {
                $subtipus = $this->repo->insertSubtipoFormacio($curs['subtipus'], $tipus->id);
            }
        }

        return [isset($tipus) ? $tipus : null, isset($subtipus) ? $subtipus : null];
    }

    private function getFamiliaFormativa($curs, $tipus)
    {
        foreach (['català', 'castellà', 'anglès', 'francès', 'alemany', 'italià', 'rus', 'llengua catalana', 'llengua castellana', 'xinès', 'japonès', 'ucraïnès', 'suïss', 'portuguès', 'grec', 'polonès', 'suec', 'romanès', 'marroqui', 'marroquí'] as $language) {
            if (mb_strpos(mb_strtolower($curs['nom']), $language) !== false) {
                $curs['familia'] = 'Idiomes';
            }
        }

        // Familia Formativa
        if (isset($curs['familia'])) {
            if (mb_strtolower($curs['familia']) == mb_strtolower('Hoteleria i turisme') || mb_strtolower($curs['familia']) == mb_strtolower('Hosteleria i turisme')) {
                $curs['familia'] = 'Hoteleria i turisme';
            }

            if (in_array(mb_strtolower($curs['familia']), ['àrea de cant', 'instruments de la música antiga', 'instruments de la música moderna i jazz', 'instruments de l\'orquestra', 'instruments polifònics', 'instruments tradicionals']) || $curs['tipus'] == 'Música i Dansa' || $curs['tipus'] == 'Ensenyaments artístics') {
                $curs['familia'] = 'Activitats Artístiques';
            }

            if (in_array(mb_strtolower($curs['familia']), ['comerç i màrqueting', 'màrqueting i logística'])) {
                $curs['familia'] = 'Comerç, Màrqueting i Logística';
            }

            if (in_array(mb_strtolower($curs['familia']), ['comunicació gràfica i audiovisual'])) {
                $curs['familia'] = 'Informàtica i comunicacions';
            }

            if (in_array(mb_strtolower($curs['familia']), ['instal.lació i manteniment'])) {
                $curs['familia'] = 'Instal·lació i manteniment';
            }

            $allowedFamilies = [
                'Cienciès de la Salut',
                'Cienciès socials',
                'Enginyeria',
                'Recursos Naturals i Medi Ambient',
                'Acollida',
                'Activitats Artístiques',
                'Activitats Físiques i Esportives',
                'Administració i Gestió',
                'Agrària',
                'Comerç, Màrqueting i Logística',
                'Disseny gràfic i tècnic',
                'Edificació i Obra civil',
                'Educació/CIFE',
                'Electricitat i electrònica',
                'Fabricació mecànica',
                'Fusta, moble i suro',
                'Gestió neteja',
                'Habilitats Personals',
                'Hoteleria i turisme',
                'Idiomes',
                'Imatge personal',
                'Imatge i so',
                'Indústries alimentàries',
                'Indústries extractives',
                'Informàtica i comunicacions',
                'Instal·lació i manteniment',
                'Qualitat i medi ambient',
                'Prevenció i Riscos Laborals (PRL)',
                'Sanitat',
                'Serveis socioculturals i a la comunitat',
                'Tèxtil, confecció i pell',
                'Transport i manteniment de vehicles'
            ];

            foreach ($allowedFamilies as $key => $allowedFamily) {
                $allowedFamilies[$key] = mb_strtolower($allowedFamily);
            }

            if (in_array(mb_strtolower($curs['familia']), $allowedFamilies)) {
                return $this->repo->insertFamiliaFormativa($curs['familia'], $tipus);
            }
        }
    }
}