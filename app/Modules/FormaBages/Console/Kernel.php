<?php
namespace App\Modules\FormaBages\Console;

use Illuminate\Console\Scheduling\Schedule;
use App\Modules\FormaBages\Console\Commands\ImportCourses;

class Kernel
{

    /**
     * The Artisan commands provided by your module.
     *
     * @var array
     */
    protected $commands = [
        ImportCourses::class,
    ];


    public function getCommands()
    {
        return $this->commands;
    }

    /**
     * Define the module's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule)
    {
        // $schedule->command('send:review-form')->dailyAt('18:00');

        $schedule->command('import:courses')->cron('0 1 * * 1,5');
    }
}