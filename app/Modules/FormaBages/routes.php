<?php

Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    Route::resource("posts", "Admin\MyPostsController", ["as" => "admin", 'except' => ['show']]);
    Route::post("insert", "ImportController@insert")->name('admin.insert');
    Route::get("import", "ImportController@import")->name('admin.import');
    Route::get("importcsv", "ImportController@importCSV")->name('admin.importcsv');

    Route::resource('users', "Admin\MyUsersController", ["as" => "admin"])->except([
        'store'
    ]);

    Route::post('/users', 'Admin\MyUsersController@customStore')->name('admin.users.store');
});

Route::post("contactar", "ContacteController@formulari")->name('contactar');
/*Route::get("search", "SearchController@search")->name('search');*/

Route::get("cursos-{formacio}", "CursosController@search")->name('cursos.list');

Route::group(['middleware' => ['auth']] , function () {
    Route::get('intranet/user', "IntranetController@user")->name('intranet.user');
    Route::post('intranet/user', "IntranetController@postUser");
    Route::post('intranet/user/pwd', "IntranetController@postUserPwd")->name('intranet.user.pwd');
    Route::get('intranet/resources', 'IntranetController@resources')->name('intranet.resources');
    Route::resource('intranet', 'IntranetController');
    Route::put("intranet/{id}/duplicate", "IntranetController@duplicate")->name("intranet.duplicate");

});