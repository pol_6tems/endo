<?php

namespace App\Modules\FormaBages;

use App\Support\Module;

class FormaBagesModule extends Module
{
    const MODULE_NAME = 'FormaBages';
    const MODULE_ROUTE_ADMIN = 'admin.formabages.index';
    const MODULE_ROUTE = 'formabages';
}
