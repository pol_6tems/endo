<?php


namespace App\Modules\FormaBages\Providers;


use Illuminate\Support\ServiceProvider;
use App\Modules\FormaBages\Composers\FiltersComposer;
use App\Modules\FormaBages\Composers\BuscadorComposer;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('Front::partials.buscador', BuscadorComposer::class);
        view()->composer('Front::partials.filters', FiltersComposer::class);
    }


    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {

    }
}