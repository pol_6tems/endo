@extends('Front::layouts.base')

@section('header')
    @includeIf('Front::partials.inner-header')
@endsection

@section('content')
<!--Inner Page Starts-->
<section class="inner-page">
    <div class="bread-cum">
        <div class="row">
            <ul>
                <li><a href="{{ get_page_url('home') }}">@lang('Inici')</a></li>
                <li>@lang('Search')</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <h1 class="page-title">@lang('Búsqueda')</h1>
        <div class="right-content">
            <ul>
                @forelse($result as $curs)
                <li>
                    <div class="title">
                        <h1><a href="{{ $curs->get_url() }}">{{ $curs->title }}</a></h1>
                        <span>
                            <a href="javascript:void(0);"><strong>Formació ocupacional</strong></a>
                            <a href="javascript:void(0);">Transport i manteniment de vehícles</a>
                        </span>
                    </div>
                    <div class="values">
                        @if ($duracio = $curs->get_field('duracio'))
                            <span class="time">{{ $duracio }}</span>
                        @endif
                        @if ($localitzacio = $curs->get_field('localitzacio'))
                            <br><span class="loc">{{ $localitzacio->title }}</span>
                        @endif
                        @if ($data_inici = $curs->get_field('data-inici-del-curs'))
                            <br><span class="cdate">@lang('Inici:') {{ getDayAndMonthCat($data_inici) }}</span>
                        @endif
                        @if ($data_final = $curs->get_field('data-final-del-curs'))
                            <span>@lang('Fi:') {{ getDayAndMonthCat($data_final) }}</span>
                        @endif
                        <div class="mlink"><a href="{{ $curs->get_url() }}" class="pink-btn">@lang('Veure curs')</a></div>
                    </div>
                </li>
                @empty
                <li>
                    <div class="no-result">
                        <div class="simg"><img src="{{ asset($_front.'images/not-found-big.png') }}" alt="" border="0"></div>
                        <h3>@lang('No s’ha trobat cap curs')</h3>
                        @lang('Contacta’ns i diga’ns quin curs estas buscant')
                        <div class="link-btn">
                            <a href="{{ get_page_url('contacte') }}">@lang('Contactar')</a>
                            <a href="javascript:void(0);">@lang('Nova cerca')</a>
                        </div>
                    </div>
                </li>
                @endforelse
            </ul>
        </div>
    </div>
</section>
<!--Inner Page Ends-->
@endsection