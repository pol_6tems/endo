<html>
    <head>
        <style>
            @font-face {
                font-family: 'Barlow';
                font-style: normal;
                font-weight: 300;
                src: url('{{ storage_path('fonts/barlow-v3-latin-300.ttf') }}') format('truetype');
            }
            /* barlow-regular - latin */
            @font-face {
                font-family: 'Barlow';
                font-style: normal;
                font-weight: normal;
                src: url('{{ storage_path('fonts/barlow-v3-latin-regular.ttf') }}') format('truetype');
            }
            /* barlow-600 - latin */
            @font-face {
                font-family: 'Barlow';
                font-style: normal;
                font-weight: bold;
                src: url('{{ storage_path('fonts/barlow-v3-latin-600.ttf') }}') format('truetype');
            }
            /* barlow-500 - latin */
            @font-face {
                font-family: 'Barlow';
                font-style: normal;
                font-weight: 500;
                src: url('{{ storage_path('fonts/barlow-v3-latin-500.ttf') }}') format('truetype');
            }
            /* barlow-700 - latin */
            @font-face {
                font-family: 'Barlow';
                font-style: normal;
                font-weight: 700;
                src: url('{{ storage_path('fonts/barlow-v3-latin-700.ttf') }}') format('truetype');
            }

            * {
                font-family: Barlow, sans-serif;
            }
        </style>
    </head>
    <body bgcolor="#f3f3f3">
    <table width="600" align="center" valign="mi" style="border: 1px solid #dcdcdc;">
        <tr>
            <td>
                <table width="600" cellpadding="20" align="center" bgcolor="#f8f8f8">
                    <tr>
                        <td style="text-align: center;">
                            <img src="{{ asset($_front."images/logo.png") }}" width="144" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td></td></tr>
        <tr>
            <td>
                <table width="600" cellpadding="10" align="center" bgcolor="#fff">
                    <tr>
                        <td><strong>@lang('Nom:')</strong></td>
                        <td>{{ $nombre }}</td>
                    </tr>
                    <tr>
                        <td><strong>@lang('Email:')</strong></td>
                        <td>{{ $email }}</td>
                    </tr>
                    <tr>
                        <td><strong>@lang('Població:')</strong></td>
                        <td>{{ $poblacion }}</td>
                    </tr>
                    <tr>
                        <td><strong>@lang('Organització:')</strong></td>
                        <td>{{ $organitzacio }}</td>
                    </tr>
                    <tr>
                        <td><strong>@lang('Teléfono:')</strong></td>
                        <td>{{ $telefono }}</td>
                    </tr>
                    <tr><td><strong>@lang('Observaciones:')</strong></td></tr>
                    <tr><td>{{ $observaciones }}</td></tr>
                    <tr>
                        <td><strong>@lang('Promo:')</strong></td>
                        <td>{{ is_null($promo) ? 'No':'Si' }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </body>
</html>