<?php


namespace App\Modules\FormaBages\Repositories;


class FiltersRepository
{

    public function getFilters($ajax = false)
    {
        $tipos_formacion = get_posts('tipo-formacion');
        $subtipos_formacion = get_posts(['post_type' => 'subtipus-formacio', 'with' => ['metas.customField']]);
        $arees_formacio = get_posts('area-formacion');
        $familia_formativa = get_posts('familia-formativa');
        $poblacions = get_posts('poblacion');
        /*$modalitats = ['presencial' => 'Presencial', 'online' => 'Online', 'diurn' => 'Diürn'];*/

        $subtipos_formacion = $subtipos_formacion->filter(function ($subtipo) {
            $subtipo->count_courses = countCourses('subtipus-formacio', $subtipo);
            return $subtipo->count_courses;
        });

        $return_tipos_formacion = collect();

        foreach ($tipos_formacion as $item) {
            $item->count_courses = countCourses("tipus-formacio", $item);
            $item->subtipus_formacio = $subtipos_formacion->filter(function ($subtipo) use ($item) {
                $parentTipus = $subtipo->get_raw_field('tipus-formacio');
                return $parentTipus && $parentTipus == $item->id;
            });

            if ($item->count_courses) {
                $return_tipos_formacion->push($item);
            }
        }
        /*$tipos_formacion = $tipos_formacion->filter(function($item) {
            return countCourses("tipus-formacio", $item);
        });*/

        $return_familia_formativa = collect();

        foreach ($familia_formativa as $item) {
            $item->count_courses = countCourses("familia-formativa", $item);

            if ($item->count_courses) {
                $return_familia_formativa->push($item);
            }
        }
        /*$familia_formativa = $familia_formativa->filter(function($item) {
            return countCourses("familia-formativa", $item);
        });*/

        $return_arees_formacio = collect();

        foreach ($arees_formacio as $item) {
            $item->count_courses = countCourses("area-formativa", $item);

            if ($item->count_courses) {
                $return_arees_formacio->push($item);
            }
        }
        /*$arees_formacio = $arees_formacio->filter(function($item) {
            return countCourses("area-formativa", $item);
        });*/

        $return_poblacions = collect();

        foreach ($poblacions as $item) {
            $item->count_courses = countCourses("localitzacio", $item);

            if ($item->count_courses) {
                $return_poblacions->push($item);
            }
        }
        /*$poblacions = $poblacions->filter(function($item) {
            return countCourses("localitzacio", $item);
        });*/

        /*$modalitats = array_filter($modalitats, function($item) {
            return countCourses("modalitat", $item);
        });*/

        $return = [
            'tipos_formacion' => $return_tipos_formacion->sortBy('order'),
            'arees_formacio' => $return_arees_formacio->sortBy('post_name'),
            'familia_formativa' => $return_familia_formativa->sortBy('post_name'),
            'poblacions' => $return_poblacions->sortBy('post_name'),
            /*'modalitats' => $modalitats,*/
        ];

        if ($ajax) {
            $return['subtipus-formacio'] = $subtipos_formacion;
        }

        return $return;
    }


    public function getFamiliesFormatives()
    {
        $familia_formativa = get_posts('familia-formativa');

        return $familia_formativa->filter(function($item) {
            return countCourses("familia-formativa", $item);
        });
    }

    function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
}