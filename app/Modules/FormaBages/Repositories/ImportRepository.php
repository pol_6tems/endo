<?php


namespace App\Modules\FormaBages\Repositories;

use App\Post;
use stdClass;
use App\Language;
use DiDom\Document;
use App\Models\PostMeta;
use Jenssegers\Date\Date;

class ImportRepository
{
    protected $languages;
    protected $count = 0;

    protected const MUNICIPIS_BAGES = [
        'Aguilar de Segarra',
        'Artés',
        'Avinyó',
        'Balsareny',
        'Callús',
        'Cardona',
        'Castellbell i el Vilar',
        'Castellfollit del Boix',
        'Castellgalí',
        'Castellnou de Bages',
        'Fonollosa',
        'Gaià',
        'Manresa',
        'Marganell',
        'Monistrol de Montserrat',
        'Mura',
        'Navarcles',
        'Navàs',
        'Pont de Vilomara i Rocafort',
        'Rajadell',
        'Sallent',
        'Sant Feliu Sasserra',
        'Sant Fruitós de Bages',
        'Sant Joan de Vilatorrada',
        'Sant Mateu de Bages',
        'Sant Salvador de Guardiola',
        'Sant Vicenç de Castellet',
        'Santpedor',
        'Súria',
        'Talamanca',
    ];

    private $cursos;

    private $especialitats;


    public function __construct() {
        $this->languages = Language::where('active', true)->get();
    }


    public function getCursosJSON() {
        if (!isset($this->cursos)) {
            $this->cursos = getJSON('https://www.oficinadetreball.gencat.cat/opendata/recursos/ofertaCursos.json', true);
        }

        return $this->cursos;
    }


    public function getEspecialitatsJSON() {
        if (!isset($this->especialitats)) {
            $this->especialitats = getJSON('https://www.oficinadetreball.gencat.cat/opendata/recursos/especialitatsFormatives.json', true);
        }

        return $this->especialitats;
    }


    public function getCount() {
        return $this->count;
    }


    public function insertTipoFormacio($title) {
        return $this->fillPost("tipo-formacion", $title);
    }


    public function insertSubtipoFormacio($title, $tipusId) {
        return $this->fillPost("subtipus-formacio", $title, null, [
            'tipus-formacio' => $tipusId,
        ]);
    }


    public function insertFamiliaFormativa($title, $tipus) {
        $exceptions = array("Fusta, moble i suro", "Tèxtil, confecció i pell", "Comerç, Màrqueting i Logística");
        $replacements = array("INDÚSTRIA ALIMENTÀRIA" => "Indústries alimentàries");
		$familiaobj = null;
		$families = [$title];
		if (! in_array($title, $exceptions) ) {
            $families = explode(", ", $title);
		}
		
		// Aixo pot provocar errors, només es retorna l'última familia creada
		foreach($families as $familia) {
			if ($familia != "") {
                if (array_key_exists($familia, $replacements)) $familia = $replacements[$familia];
                
				$familiaobj = $this->fillPost("familia-formativa", $familia, null, [
					'tipus' => $tipus ? $tipus->id : null,
				]);
			}
		}
        return $familiaobj;
    }

    public function insertAreaFormativa($title, $familia, $extra_data = array()) {
        $arees = [$title];
        if (substr_count($title, "i") > 1) {
            $arees = explode(", ", $title);
        }
		
		// Aixo pot provocar errors, només es retorna l'última familia creada
		foreach($arees as $area) {
			if ($area != "") {
				$area = $this->fillPost("area-formacion", $area, null, [ 'familia' => $familia->id ]);
			}
        }
        
        if ($extra_data) {
            foreach($this->languages as $lang) {
                foreach($extra_data as $cf => $value) {
                    $area->set_field($cf, $value, $lang->code);
                }
            }
        }

        return $area;
    }

    public function insertPoblacio($title) {
        if ( isset($title) ) {
            $municipis_bages = array();
            foreach(self::MUNICIPIS_BAGES as $municipi) {
                $municipis_bages[] = str_slug($municipi);
            }

            if (in_array(str_slug($title), $municipis_bages)) {
                return $this->fillPost("poblacion", $title);
            }
        }

        return null;
    }

    public function insertCentre($title, $poblacio, $data) {
        if ( isset($title) ) {
            $web = (isset($data['pagina_web'])) ? $data['pagina_web'] : "";
            $email = (isset($data['correu_electronic'])) ? $data['correu_electronic'] : "";

            if (! isset($data['correu_electronic']) && isset($data['pagina_web'])) {
                if (strpos($web, "@")) {
                    $email = $web;
                }
            }

            if (isset($data['pagina_web'])) {
                if (strpos($web, "@")) {
                    $aux = explode("@", $web);
                    $web = $aux[1];
                }
            }
            
            return $this->fillPost("centre", $title, null, [
                'id' => (isset($data['id_centre'])) ? strval($data['id_centre']) : "",
                'direccion' => (isset($data['adreca'])) ? $data['adreca'] : "",
                'codi_postal' => (isset($data['codi_postal'])) ? strval($data['codi_postal']) : "",
                'telefono' => (isset($data['telefon'])) ? strval($data['telefon']) : "",
                'email' => $email,
                'poblacion' => $poblacio->id,
                'url' => $web,
            ]);
        } return null;
    }

    public function insertCurs($curs, $centre) {
        // Especialitat
        $especialitat = $this->getEspecialitat($curs['especialitat_formativa']);
        // Tipo Formacion
        $tipus = $this->insertTipoFormacio($curs['tipus']);

        
        // Familia Formativa
        if ($tipus) {
            $familia = $this->insertFamiliaFormativa($especialitat['familia'], $tipus);
        }

        // Area
        if ($familia) {
            $area = $this->insertAreaFormativa($especialitat['area'], $familia);
        }
        
        if (isset($area) && isset($tipus) && isset($centre)) {
            $title = $curs['especialitat_formativa'];
            $date_ini = Date::createFromFormat("m/d/Y", $curs['data_inici']);
            $date_fi = Date::createFromFormat("m/d/Y", $curs['data_fi']);

            $curs = $this->fillPost("curso", $title, $title . '-' . $curs['curs'], [
                'id' => $curs['curs'],
                'duracio' => $curs['hores'] . 'h',
                'cifo' => $curs['cifo'],
                'estat-curs' => $curs['estat_curs'],
                'identificador-del-curs' => $curs['identificador_curs'],
                'data-final-del-curs' => $date_fi->format("d/m/Y"),
                'data-inici-del-curs' => $date_ini->format("d/m/Y"),
                'modalitat' => ucfirst(strtolower($curs['modalitat'])),
                'area-formativa' => $area->id,
                'tipus-formacio' => $tipus->id,
                'centre' => $centre->id,
                'tipus' => 'json',
            ]);

            return $this->fillCurs($curs);
        }
        return null;
    }

    public function getEspecialitat($title) {
        $current = array_filter($this->especialitats, function($value) use ($title) {
            return isset($value['descripcio']) && strtolower($value['descripcio']) == strtolower($title);
        });

        return array_shift($current);
    }

    public function importRow($row) {
        if ($poblacio = $this->insertPoblacio($row['municipi'])) {
            // Centre
            $centre = $this->insertCentre($row['nom_centre'], $poblacio, $row);

            // Tipo Formacion
            $tipus = $this->insertTipoFormacio($row['tipus']);
            
            // Familia Formativa
            if ($tipus) {
                $familia = $this->insertFamiliaFormativa($row['familia'], $tipus);
            }

            // Area
            if ($familia) {
                $area = $this->insertAreaFormativa($row['area'], $familia);
            }
            
            if (isset($area) && isset($tipus) && isset($centre)) {
                $title = $row['nom_curs'];
                $id = getURLparam($row["url_fitxa"], "cursId");

                $curs = $this->fillPost("curso", $title, $title . '-' . $id, [
                    'id' => $id,
                    'duracio' => strval($row['durada']) . 'h',
                    'data-inici-del-curs' => $row['data_dinici'],
                    'preinscripcio' => $row['preinscripcio'],
                    'modalitat' => ucfirst(strtolower($row['modalitat'])),
                    'area-formativa' => $area->id,
                    'tipus-formacio' => $tipus->id,
                    'centre' => $centre->id,
                    'tipus' => 'csv',
                ]);

                return $this->fillCurs($curs);
            }
        }

        return null;
    }

    private function getTemari($document) {
        $temaris = $document->find('.temari li');
        $itemsTemari = array();
        foreach($temaris as $temari) {
            $content = trim($temari->text(), " .");
            if ($content != "Temari:" && strlen($content) > 0) {
                $itemsTemari[] = $content;
            }
        }

        return $itemsTemari;
    }

    private function getCertificacions($document) {
        // Certificats Professionals
        $certificats = $document->find('.b_uc');
        
        $certificacions = array();
        // Obtenir Unitats de Competència
        if (count($certificats) > 0) {
            foreach($certificats[0]->children() as $unitat_comp) {
                $uc = new stdClass();
                $uc->title = $unitat_comp->find('span')[0]->text();
                $moduls_formatius = $unitat_comp->find('.b_mf > li');

                if (count($moduls_formatius) > 0) {
                    foreach($moduls_formatius as $modul_formatiu) {
                        $span = $modul_formatiu->find('span')[0];
                        $mf = new stdClass();
                        $mf->title = $span->firstChild()->text();
                        $mf->hours = $span->lastChild()->text();
                        $mf->hours = trim($mf->hours, '(');
                        $mf->hours = trim($mf->hours, ' h.)');

                        $unitats_formatives = $modul_formatiu->find('.b_uf > li');
                        if ( count($unitats_formatives) > 0 ) {
                            foreach($unitats_formatives as $unitat_formativa) {
                                $span = $unitat_formativa->find('span')[0];
                                $uf = new stdClass();
                                $uf->title = $span->firstChild()->text();
                                $uf->hours = $span->lastChild()->text();
                                $uf->hours = trim($uf->hours, '(');
                                $uf->hours = trim($uf->hours, ' h.)');

                                $mf->unitats_formatives[] = $uf;
                            }
                        }

                        $uc->moduls_formatius[] = $mf;
                    }
                }
                $certificacions[] = $uc;
            }
        }        
        return $certificacions;
    }

    public function fillCurs($curs) {
        if ($curs) {
            try {
                $document = new Document("https://www.oficinadetreball.gencat.cat/socfuncions/DetallCurs.do?idCurs=" . $curs->get_field('id_curs'), true);
                $temari = $this->getTemari($document);
                $certificacions = $this->getCertificacions($document);

                if (count($temari) > 0 && strpos($temari[count($temari) - 1], "Horari:") >= 0) {
                    $horari = array_pop($temari);
                    $horari = explode("Di", $horari); // Tots els dies començen per Di
                    array_shift($horari);
                    $horari = array_map(function ($value) {
                        return "Di" . $value;
                    }, $horari);

                    $this->fillRepeater($curs, 85, 86, $horari);
                }
                $this->fillRepeater($curs, 83, 84, $temari);
            } catch (\Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }
        }

        return $curs;
    }

    public function fillPost($post_type, $title, $post_name = null, $custom_fields = array()) {
        if (is_null($post_name)) {
            $post_name = str_slug($title);
        } else {
            $post_name = str_slug($post_name);
        }

        if (isset($title)) {
            $post = Post::where([
                'type' => $post_type,
                'status' => 'publish',
                'author_id' => 1,
            ])
            ->whereTranslation('post_name', $post_name)
            ->first();

            if (is_null($post)) {
                $post = Post::create([
                    'type' => $post_type,
                    'status' => 'publish',
                    'author_id' => 1,
                ]);

                foreach($this->languages as $lang) {
                    $lang = $lang->code;
                    $post->{"title:$lang"} = $title;
                    $post->{"post_name:$lang"} = $post_name;
                    
                    if (is_array($custom_fields)) {
                        foreach($custom_fields as $cf => $value) {
                            if ($value) $post->set_field($cf, $value, $lang);
                        }
                    }
                }
                $post->save();
            } elseif ($post_type == 'centre' && !$post->get_field('email') && isset($custom_fields['email']) && $custom_fields['email']) {
                foreach($this->languages as $lang) {
                    $post->set_field('email', $custom_fields['email'], $lang->code);
                }
            } elseif ($post_type == 'curso' && $post->get_field('data-inici-del-curs') != $custom_fields['data-inici-del-curs']) {
                $post->set_field('data-inici-del-curs', $custom_fields['data-inici-del-curs']);
            } elseif ($post_type == 'curso' && isset($custom_fields['subtipus-formacio']) && (!$post->get_field('subtipus-formacio') || $post->get_field('subtipus-formacio')->id != $custom_fields['subtipus-formacio'])) {
                $post->set_field('subtipus-formacio', $custom_fields['subtipus-formacio']);
            } elseif ($post_type == 'subtipus-formacio' && isset($custom_fields['tipus-formacio']) && (!$post->get_field('tipus-formacio') || $post->get_field('tipus-formacio')->id != $custom_fields['tipus-formacio'])) {
                $post->set_field('tipus-formacio', $custom_fields['tipus-formacio']);
            } elseif ($post_type == 'curso' && isset($custom_fields['familia-formativa']) && (!$post->get_field('familia-formativa') || ((is_numeric($post->get_field('familia-formativa')) && $post->get_field('familia-formativa') == $custom_fields['familia-formativa']) || (is_object($post->get_field('familia-formativa')) && $post->get_field('familia-formativa')->id != $custom_fields['familia-formativa'])))) {
                $post->set_field('familia-formativa', $custom_fields['familia-formativa']);
            }
            return $post;
        }
        return null;
    }

    public function fillRepeater($post, $repeater_cf, $value_cf, $array) {
        foreach($this->languages as $lang) {
            $repeater = PostMeta::firstOrCreate([
                'post_id' => $post->id,
                'custom_field_id' => $repeater_cf, // Custom Field Temari,
                'value' => null,
                'locale' => $lang->code,
                'order' => 0,
            ]);
            
            foreach($array as $order => $item) {
                $meta = PostMeta::updateOrcreate([
                    'post_id' => $post->id,
                    'custom_field_id' => $value_cf,
                    'value' => $item,
                    'locale' => $lang->code,
                    'order' => $order,
                    'parent_id' => $repeater->custom_field_id
                ]);
            }
        }
    }
}