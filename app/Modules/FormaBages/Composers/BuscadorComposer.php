<?php


namespace App\Modules\FormaBages\Composers;


use Carbon\Carbon;
use Illuminate\View\View;
use Jenssegers\Date\Date;

class BuscadorComposer
{

    public function compose(View $view)
    {
        $tipus = get_posts('tipo-formacion')->sortBy('order');

        $view->with([
            'tipus' => $tipus,
            'search_query' => request()->search_query,
            'search_formacio' => request()->search_formacio,
        ]);
    }
}