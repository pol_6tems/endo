<?php


namespace App\Modules\FormaBages\Composers;

use App\Modules\FormaBages\Repositories\FiltersRepository;
use Illuminate\View\View;


class FiltersComposer
{

    public function compose(View $view)
    {
        ini_set('max_execution_time', 180); //3 minutes

        /*$view->with(app(FiltersRepository::class)->getFilters());*/
        $view->with(cacheable(FiltersRepository::class, 10)->getFilters());
    }
}