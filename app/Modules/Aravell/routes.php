<?php

Route::group(['middleware' => ['auth']], function () {
    Route::get('/{index?}', 'HomeController@index')->name('index');

    Route::get('/green-fee/availability', 'ReservesController@availability')->name('green-fee.availability');
    Route::get('/green-fee/all-hours-availability', 'ReservesController@allHoursAvailability')->name('green-fee.all-availability');

    Route::resource('green-fee', 'ReservesController')->except([
        'index'
    ]);

    Route::post('/green-fee/pay', 'ReservesController@pay')->name('green-fee.pay');
    Route::post('/green-fee/payPost', 'ReservesController@payPost')->name('green-fee.payPost');
    Route::get('/green-fee/{id}/confirm', 'ReservesController@confirm')->name('green-fee.confirm');
    Route::get('/green-fee/{id}/update-confirm', 'ReservesController@updateConfirm')->name('green-fee.update-confirm');
    Route::get('/green-fee/{id}/pre-delete', 'ReservesController@preDestroy')->name('green-fee.pre-delete');
    Route::get('/green-fee/{id}/delete-confirm', 'ReservesController@destroyConfirm')->name('green-fee.delete-confirm');


    Route::resource('practicas', 'ReservesController')->except([
        'index'
    ]);

    Route::post('/practicas/pay', 'ReservesController@pay')->name('practicas.pay');
    Route::post('/practicas/payPost', 'ReservesController@payPost')->name('practicas.payPost');

    Route::resource('restaurant', 'ReservesController')->except([
        'index'
    ]);

    Route::post('/restaurant/pay', 'ReservesController@pay')->name('restaurant.pay');
    Route::post('/restaurant/payPost', 'ReservesController@payPost')->name('restaurant.payPost');
    Route::get('/restaurant/{id}/confirm', 'ReservesController@confirm')->name('restaurant.confirm');
    Route::get('/restaurant/{id}/update-confirm', 'ReservesController@updateConfirm')->name('restaurant.update-confirm');
    Route::get('/restaurant/{id}/pre-delete', 'ReservesController@preDestroy')->name('restaurant.pre-delete');
    Route::get('/restaurant/{id}/delete-confirm', 'ReservesController@destroyConfirm')->name('restaurant.delete-confirm');

    Route::get('reserves', 'ReservesController@index')->name('reserves.index');
    Route::get('/reserves/player-price', 'ReservesController@playerPrice')->name('reserves.player-price');

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@edit')->name('user.edit');
        Route::post('/', 'UserController@postEdit');
        Route::get('/confirm', 'UserController@editConfirm')->name('user.edit-confirm');

        Route::get('/privacity', 'UserController@privacity')->name('user.privacity');
        Route::post('/privacity', 'UserController@postPrivacity');
        Route::get('/privacity-confirm', 'UserController@privacityConfirm')->name('user.privacity-confirm');

        Route::get('/wallet', 'UserController@wallet')->name('user.wallet');
        Route::post('/wallet', 'UserController@postAddWallet');
        Route::get('/wallet/add-confirm', 'UserController@addWalletConfirm')->name('user.wallet-add-confirm');
        Route::get('/wallet/history', 'UserController@walletHistory')->name('user.wallet-history');
    });

    Route::get('communication', 'CommunicationController@index')->name('communication');
    Route::get('communication/chat', 'CommunicationController@chat')->name('communication.chat');
    Route::redirect('communication/chat2', 'communication/chat', 301)->name('perfil.mensajes');
    Route::post('communication/send-message', 'CommunicationController@sendMessage')->name('communication.send-message');
});

Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    // Posts
    Route::put("posts/{id}/aprove_pending", "Admin\PostsController@aprove_pending")->name("admin.posts.aprove_pending");
    Route::resource('posts', "Admin\PostsController", ["as" => "admin"]);

    Route::resource('users', "Admin\UsersController", ["as" => "admin"]);
    Route::post('/users/{id}/charge-wallet', 'Admin\UsersController@chargeWallet')->name('admin.users.charge-wallet');

    Route::resource('purchases', 'Admin\PurchasesController', ['as' => 'admin'])->except([
        'create', 'store', 'update', 'destroy'
    ]);

    Route::get('calendar', "Admin\GreenFeeCalendarController@index")->name('admin.green-fee-calendar');
    Route::get('day', "Admin\GreenFeeCalendarController@day")->name('admin.green-fee-calendar.day');
    Route::post('price', "Admin\GreenFeeCalendarController@postPrice")->name('admin.green-fee-calendar.price');
    Route::post('open', "Admin\GreenFeeCalendarController@postOpen")->name('admin.green-fee-calendar.open');
    Route::post('clone', "Admin\GreenFeeCalendarController@postClone")->name('admin.green-fee-calendar.clone');

    Route::get('tariffs', "Admin\TariffsController@index")->name('admin.tariffs');
    Route::get('tariffs/create', "Admin\TariffsController@create")->name('admin.tariffs.create');
    Route::post('tariffs/store', "Admin\TariffsController@store")->name('admin.tariffs.store');
    Route::post('tariffs/update', "Admin\TariffsController@update")->name('admin.tariffs.update');
    Route::post('tariffs/price', "Admin\TariffsController@updatePrice")->name('admin.tariffs.price');
    Route::delete('tariffs/delete', 'Admin\TariffsController@delete')->name('admin.tariffs.destroy');


    Route::resource('tables', 'Admin\TablesController', ['as' => 'admin'])->except([
        'create', 'store', 'destroy'
    ]);

    Route::resource('messaging', 'Admin\WhatsappController', ['as' => 'admin'])->except([
        'create', 'store', 'destroy'
    ]);
});