<?php


namespace App\Modules\Aravell\Observers;


use App\Models\CustomFieldGroup;
use App\Models\UserMeta;
use App\User;

class UserObserver
{

    public function creating(User $user)
    {
        if (!$user->role) {
            $user->role = 'No soci';
        }
    }


    public function created(User $user)
    {
        $data = ['saldo' => 0];

        $cf_groups = CustomFieldGroup::where('post_type', 'user')->orderBy('order')->get();

        foreach ($cf_groups as $cf_group) {
            foreach ($cf_group->fields as $field) {
                if (array_key_exists($field->name, $data)) {
                    $userMeta = UserMeta::firstOrNew([
                        'user_id' => $user->id,
                        'custom_field_id' => $field->id,
                        'post_id' => 'user'
                    ]);

                    if (!$userMeta->exists) {
                        $userMeta->value = $data[$field->name];
                        $userMeta->save();
                    }
                }
            }
        }
    }


    public function saving(User $user)
    {
    }


    public function updating(User $user)
    {
    }


    public function saved(User $user)
    {
    }
}