<?php


namespace App\Modules\Aravell\Services;


use App\Modules\Aravell\Models\AravellNotification;
use App\User;
use Carbon\Carbon;

class NotificationsService
{

    public function getHistory(User $user, $limit = null, $onlyPast = false, $onlyFuture = false)
    {
        // TODO: paginate?
        $query = AravellNotification::with(['post'])
            ->where('user_id', $user->id);

        if ($onlyPast || !$onlyFuture) {
            $query = $query->orderBy('created_at', 'desc');
        } else {
            $query = $query->orderBy('created_at', 'asc');
        }

        if ($onlyPast) {
            $query = $query->where(function ($q) {
                $q->where(function ($q2){
                        $q2->where('seen', 1)
                            ->whereNull('until');
                    })
                    ->orWhere('until', '<', Carbon::now());
            });
        }

        if ($onlyFuture) {
            $query = $query->where(function ($q) {
                $q->where('seen', 0)
                    ->orWhere('until', '>=', Carbon::now());
            });
        }

        if ($limit) {
            $query = $query->limit($limit);
        }

        return $query->get();
    }


    public function updateSeenNotifications(User $user)
    {
        AravellNotification::where('user_id', $user->id)
            ->where('seen', 0)
            ->update(['seen' => 1]);
    }
}