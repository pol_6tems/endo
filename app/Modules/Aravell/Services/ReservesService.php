<?php


namespace App\Modules\Aravell\Services;


use App\Models\PostMeta;
use App\Post;
use App\PostTranslation;
use App\User;
use Carbon\Carbon;

class ReservesService
{

    public function getHistory(User $user, $limit = null, $onlyPast = false, $onlyFuture = false)
    {
        // TODO: paginate?
        $query = Post::with(['metas.customField', 'translations'])
            ->whereIn('type', ['reserva-green-fee', 'reserva-practicas', 'reserva-restaurant'])
            ->where('author_id', $user->id)
            ->where('status', 'publish')
            ->orderBy('created_at', 'desc');

        if ($limit && !($onlyPast || $onlyFuture)) {
            $query = $query->limit($limit);
        }

        $reserves = $query->get();

        if ($onlyPast || $onlyFuture) {
            $now = Carbon::now();
            $reserves = $reserves->filter(function ($reserve) use ($now, $onlyPast, $onlyFuture) {
                $horaField = 'hora-sortida';

                if ($reserve->type == 'reserva-restaurant') {
                    $horaField = 'hora';
                }

                $reserve->time = Carbon::createFromFormat('d/m/Y H:i', $reserve->get_field('data') . ' ' . $reserve->get_field($horaField));
                if ($onlyPast) {
                    return $reserve->time <= $now;
                } else {
                    return $reserve->time > $now;
                }
            });

            if ($limit) {
                $reserves = $reserves->take($limit);
            }

            if ($onlyPast) {
                $reserves = $reserves->sortByDesc('time');
            } else {
                $reserves = $reserves->sortBy('time');
            }
        }

        $reserveTypes = [
            'reserva-green-fee' => [
                'route' => 'green-fee.show',
                'type_name' => __('Green Fee'),
                'img' => 'histo-ico-2.svg',
                'listImg' => '1.svg',
                'notifImg' => 'noti-icon-1.svg'
            ],

            'reserva-practicas' => [
                'route' => 'practicas.show',
                'type_name' => __('Campo de prácticas'),
                'img' => 'mob-menu-icon-practice.svg',
                'listImg' => '3.svg',
                'notifImg' => 'noti-icon-3.svg'
            ],

            'reserva-restaurant' => [
                'route' => 'restaurant.show',
                'type_name' => __('Restaurante'),
                'img' => 'histo-ico-1.svg',
                'listImg' => '2.svg',
                'notifImg' => 'noti-icon-2.svg'
            ]
        ];

        $reserves->each(function ($reserve) use ($reserveTypes) {
            $reserve->route = $reserveTypes[$reserve->type]['route'];
            $reserve->type_name = $reserveTypes[$reserve->type]['type_name'];
            $reserve->img = $reserveTypes[$reserve->type]['img'];
            $reserve->listImg = $reserveTypes[$reserve->type]['listImg'];
            $reserve->notifImg = $reserveTypes[$reserve->type]['notifImg'];
        });

        return $reserves;
    }


    public function getReserveNumber($type)
    {
        $letra = $this->get_letra_from_type($type);

        $libre = false;
        while(!$libre) {
            $six_digit_random_number = mt_rand(100000, 999999);
            $numero = $letra . $six_digit_random_number;
            $libre = !PostTranslation::where('post_name', $numero)->exists();

            if ($libre) {
                return $numero;
            }
        }
    }


    public function getHorasSalida($type)
    {
        // TODO: filter used slots!

        return Post::select('posts.*')
            ->with(['metas.customField', 'translations'])
            ->join('post_meta', 'post_meta.post_id', '=', 'posts.id')
            ->join('custom_fields', 'custom_fields.id', '=', 'post_meta.custom_field_id')
            ->where('custom_fields.name', 'hora')
            ->where('posts.type', 'hora-sortida')
            ->where('posts.status', 'publish')
            ->orderBy('post_meta.value', 'asc')
            ->get();
    }


    public function getHorasRestaurant()
    {
        // TODO: filter used slots!

        return Post::select('posts.*')
            ->with(['metas.customField', 'translations'])
            ->join('post_meta', 'post_meta.post_id', '=', 'posts.id')
            ->join('custom_fields', 'custom_fields.id', '=', 'post_meta.custom_field_id')
            ->where('custom_fields.name', 'hora')
            ->where('posts.type', 'hora-restaurant')
            ->where('posts.status', 'publish')
            ->orderBy('post_meta.value', 'asc')
            ->get();
    }


    private function get_letra_from_type($type)
    {
        if ($type == 'green-fee') {
            return 'RGF';
        }

        if ($type == 'practicas') {
            return 'RP';
        }

        if ($type == 'restaurant') {
            return 'RR';
        }

        if ($type == 'recarrega-wallet') {
            return 'RW';
        }

        return 'R';
    }
}