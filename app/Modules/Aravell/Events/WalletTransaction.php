<?php


namespace App\Modules\Aravell\Events;


use App\User;
use Illuminate\Queue\SerializesModels;

class WalletTransaction
{

    use SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @var int
     */
    public $amount;

    /**
     * @var string
     */
    public $comments;


    /**
     * WalletTransaction constructor.
     * @param User $user
     * @param int $amount
     * @param string $comments
     */
    public function __construct(User $user, int $amount, string $comments)
    {
        $this->user = $user;
        $this->amount = $amount;
        $this->comments = $comments;
    }

}