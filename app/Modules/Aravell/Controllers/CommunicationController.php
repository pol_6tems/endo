<?php


namespace App\Modules\Aravell\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Mensaje;
use App\Modules\Aravell\Services\NotificationsService;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommunicationController extends Controller
{

    public function index()
    {
        $user = auth()->user();
        $user->load(['metas.customField']);

        $notificationsService = app(NotificationsService::class);

        $historyNotifications = $notificationsService->getHistory($user, 6, true);
        $newNotifications = $notificationsService->getHistory($user, null, false, true);

        $notificationsService->updateSeenNotifications($user);

        return view('Front::communication.index', compact('historyNotifications', 'newNotifications'));
    }

    public function chat() {
        $user = auth()->user();
        $orgUser = User::where('email', 'organitzador@aravellgolfclub.com')->first();

        $user_avatar_default = asset('storage/avatars/user-endo.jpg');
        $my_user_avatar = $user_avatar_default;
        $avatar_pub = public_path('storage/avatars/' . $user->avatar);
        if (file_exists($avatar_pub)) {
            $my_user_avatar = asset('storage/avatars/' . $user->avatar);
        }

        $mensajes = Mensaje::select(DB::raw('distinct mensajes.*'))
            ->where(function ($query) use ($user, $orgUser) {
                $query->where('chat', 'like', '%' . $orgUser->id . '_' . $user->id . '%')
                    ->orWhere('chat', 'like', '%' . $user->id . '_' . $orgUser->id . '%');
            })
            ->orderBy('id', 'asc')
            ->get();



        if (!$mensajes->count()) {
            $firstMessage = new Mensaje([
                'user_id' => $orgUser->id,
                'mensaje' => __('¡Hola! ¿En qué puedo ayudarte?'),
                'visto' => 0,
                'chat' => $orgUser->id . '_' . $user->id . '_',
                'type' => 'chat',
                'to' => $user->id
            ]);

            $firstMessage->created_at = Carbon::now();
            $firstMessage->updated_at = Carbon::now();

            $mensajes = collect([$firstMessage]);
        }

        $last_mensaje = $mensajes->last();

        /* USUARIOS */
        $chat_users = $last_mensaje->get_users();

        $user_id = 0;
        $user_name = [];
        $user_email = [];
        $user_avatar = $user_avatar_default;

        foreach ( $chat_users as $chat_user ) {
            if ( is_int($chat_user) ) {
                $chatUser = User::find($chat_user);

                if ( empty($chatUser) ) continue;

                $user_email[] = $chatUser->email;
                $user_name[] = $chatUser->name;
                if ( count($chat_users) == 1 )  {
                    $user_name = [$chatUser->fullname()];
                    if ( !empty($chatUser->avatar) ) {
                        $avatar = asset('storage/avatars/' . $chatUser->avatar);
                        $avatar_pub = public_path('storage/avatars/' . $chatUser->avatar);
                        if ( file_exists($avatar_pub) ) $user_avatar = $avatar;
                    }
                }
            } else {
                $parts = explode('##', $chat_user);
                $user_id = $parts[1];
                $user_name[] = $parts[0];
                $user_email[] = $parts[1];
                $user_avatar = $user_avatar_default;
            }
        }
        /* end USUARIOS */

        $chat = new \stdClass();
        $chat->chat = str_replace('.', '-', str_replace('@', '-', $last_mensaje->chat));
        $chat->chat_noencode = $last_mensaje->chat;
        $chat->type = $last_mensaje->type;
        $chat->mensajes = $mensajes;
        $chat->last_mensaje = $last_mensaje;
        $chat->user_name = !empty($user_name) ? implode(', ', $user_name) : Auth::user()->fullname();
        $chat->user_email = count($chat_users) == 1 ? reset($user_email) : '';
        $chat->user_id = $user_id;
        $chat->user_avatar = !empty($user_name) ? $user_avatar : $my_user_avatar;

        Mensaje::where('to', $user->id)
            ->where('visto', 0)
            ->update(['visto' => 1]);

        return view('Front::communication.chat', compact('chat', 'user_avatar_default', 'orgUser', 'user'));
    }


    public function sendMessage()
    {
        $user = auth()->user();
        $orgUser = User::where('email', 'organitzador@aravellgolfclub.com')->first();

        $data = request()->all();

        $mensajes = Mensaje::select(DB::raw('distinct mensajes.*'))
            ->where(function ($query) use ($user, $orgUser) {
                $query->where(function ($query2) use ($user, $orgUser) {
                    $query2->where('user_id', $user->id)
                        ->where('to', $orgUser->id);
                })->orWhere(function ($query3) use ($user, $orgUser) {
                    $query3->where('user_id', $orgUser)
                        ->where('to', $user->id);
                });
            })
            ->orderBy('id', 'asc')
            ->get();

        if (!$mensajes->count()) {
            Mensaje::create([
                'user_id' => $orgUser->id,
                'mensaje' => __('¡Hola! ¿En qué puedo ayudarte?'),
                'visto' => 1,
                'chat' => $data['chat_id'],
                'type' => 'chat',
                'fitxer' => '',
                'to' => $user->id
            ]);
        }

        Mensaje::create([
            'user_id' => $user->id,
            'mensaje' => $data['message'],
            'visto' => 0,
            'chat' => $data['chat_id'],
            'type' => 'chat',
            'fitxer' => '',
            'to' => $orgUser->id,
        ]);

        return response()->json([
            'status' => 'OK'
        ]);
    }


    public static function notification($parameters, $name = null)
    {

    }
}