<?php


namespace App\Modules\Aravell\Controllers\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Modules\Aravell\Models\GreenFeeCalendarDay;
use App\Modules\Aravell\Models\GreenFeeCalendarHour;
use App\Modules\Aravell\Models\GreenFeeCalendarPrice;
use App\Modules\Aravell\Models\Tariff;
use App\Modules\Aravell\Repositories\CalendarRepository;
use App\Post;
use Carbon\Carbon;

class GreenFeeCalendarController extends AdminController
{

    public function index()
    {
        $startDate = request('start_date');

        $startDate = $startDate ? Carbon::createFromFormat('d/m/Y', $startDate) : Carbon::parse($startDate);

        $startDate->startOfWeek();
        $endDate = clone $startDate;
        $endDate->endOfWeek();

        $days = app(CalendarRepository::class)->getCalendarBetweenDates($startDate, $endDate);

        $dates = $this->generateDateRange($startDate, $endDate);

        $prevDate = clone $startDate;
        $prevDate->subWeek()->startOfWeek();

        $nextDate = clone $endDate;
        $nextDate->addDay()->startOfWeek();

        $categories = Post::ofType('categoria-socio')
            ->ofStatus('publish')
            ->withTranslation()
            ->get();

        $tariffs = Tariff::with(['tariffCategories.category'])->get();

        return view('Front::admin.green-fee-calendar', [
            'start_date' => $startDate,
            'end_date' => $endDate,
            'days' => $days,
            'dates' => $dates,
            'prev_date' => $prevDate,
            'next_date' => $nextDate,
            'categories' => $categories,
            'tariffs' => $tariffs
        ]);
    }


    public function day()
    {
        $day = null;
        $id = request('id');

        if ($id) {
            $day = GreenFeeCalendarDay::find($id);
        }

        if (!$id || !$day) {
            $date = request('date');

            if (!$date) {
                abort(400);
            }

            $date = Carbon::createFromFormat('d/m/Y', $date)->startOfDay();

            $day = GreenFeeCalendarDay::where('date', $date)->first();
        }

        if ($day) {
            $date = $day->date;
        }

        $categories = Post::ofType('categoria-socio')
            ->ofStatus('publish')
            ->withTranslation()
            ->get();

        $tariffs = Tariff::with(['tariffCategories.category'])->get();

        return view('Front::admin.green-fee-calendar-day', [
            'date' => $date,
            'day' => $day,
            'categories' => $categories,
            'tariffs' => $tariffs
        ]);
    }


    public function postPrice()
    {
        $id = request('id');
        $hour = request('hour');
        $category = request('category');
        $tariff = request('tariff');

        if ($id) {
            $day = GreenFeeCalendarDay::find($id);
        }

        if (!$id || !$day) {
            $date = request('date');

            if (!$date) {
                abort(400);
            }

            $date = Carbon::createFromFormat('d/m/Y', $date)->startOfDay();

            $day = GreenFeeCalendarDay::firstOrCreate([
                'date' => $date
            ]);
        }

        $dayHour = $day->hours->filter(function ($dayHour) use ($hour) {
            return $dayHour->hour == sprintf("%02s", $hour) . ':00:00';
        })->first();

        if (!$dayHour) {
            $dayHour = GreenFeeCalendarHour::create([
                'green_fee_calendar_day_id' => $day->id,
                'hour' => $hour . ':00:00',
                'tariff_id' => $tariff ? $tariff : null,
                'is_open' => $tariff ? 1 : 0
            ]);
        }

        if ($category) {
            $categoryPrice = $dayHour->prices->where('post_id', $category)->first();

            if (!$categoryPrice && $tariff) {
                GreenFeeCalendarPrice::create([
                    'green_fee_calendar_hour_id' => $dayHour->id,
                    'post_id' => $category,
                    'tariff_id' => $tariff
                ]);
            } else {
                if ($tariff) {
                    $categoryPrice->update(['tariff_id' => $tariff]);
                } else {
                    $categoryPrice->delete();
                }
            }
        } else {
            $dayHour->update([
                'tariff_id' => $tariff ? $tariff : null,
                'is_open' => $tariff ? 1 : 0
            ]);
        }
    }


    public function postOpen()
    {
        $id = request('id');

        if ($id) {
            $day = GreenFeeCalendarDay::find($id);
        }

        if (!$id || !$day) {
            $date = request('date');

            if (!$date) {
                abort(400);
            }

            $date = Carbon::createFromFormat('d/m/Y', $date)->startOfDay();

            $day = GreenFeeCalendarDay::firstOrCreate([
                'date' => $date
            ]);
        }

        $day->update([
            'is_open' => request('is_open', 1)
        ]);
    }


    public function postClone()
    {
        $day = null;
        $type = request('type');
        $id = request('id');
        $startDate = request('start_date');
        $endDate = request('end_date');

        if ($id) {
            $day = GreenFeeCalendarDay::find($id);
        }

        if (!$id || !$day) {
            $date = request('date');

            if (!$date) {
                abort(400);
            }

            $date = Carbon::createFromFormat('d/m/Y', $date)->startOfDay();

            if ($type == 'week') {
                $date->startOfWeek();
            }

            $day = GreenFeeCalendarDay::where('date', $date)->first();
        }

        if (!$day || !$type || !$startDate) {
            abort(400);
        }

        $startDate = Carbon::createFromFormat('d/m/Y', $startDate)->startOfDay();
        $rangeToClone = [$startDate];

        if ($type == 'day') {
            if ($endDate) {
                $endDate = Carbon::createFromFormat('d/m/Y', $endDate)->startOfDay();
                $rangeToClone = $this->generateDateRange($startDate, $endDate);
            }

            foreach ($rangeToClone as $cloneDate) {
                $cloneDay = GreenFeeCalendarDay::firstOrCreate([
                    'date' => $cloneDate
                ]);

                $cloneDay->update(['is_open' => $day->is_open]);

                foreach ($day->hours as $hour) {
                    $cloneHour = GreenFeeCalendarHour::where('green_fee_calendar_day_id', $cloneDay->id)
                        ->where('hour', $hour->hour)
                        ->first();

                    if (!$cloneHour) {
                        $cloneHour = GreenFeeCalendarHour::create([
                            'green_fee_calendar_day_id' => $cloneDay->id,
                            'hour' => $hour->hour,
                            'is_open' => $hour->is_open,
                            'tariff_id' => $hour->tariff_id
                        ]);
                    } else {
                        $cloneHour->update([
                            'is_open' => $hour->is_open,
                            'tariff_id' => $hour->tariff_id
                        ]);
                    }

                    foreach ($hour->prices as $price) {
                        $clonePrice = $cloneHour->prices->where('post_id', $price->post_id)->first();

                        if (!$clonePrice) {
                            GreenFeeCalendarPrice::create([
                                'green_fee_calendar_hour_id' => $cloneHour->id,
                                'post_id' => $price->post_id,
                                'tariff_id' => $price->tariff_id
                            ]);
                        } else {
                            $clonePrice->update(['tariff_id' => $price->tariff_id]);
                        }
                    }
                }
            }
        }

        if ($type == 'week') {
            if (!$endDate) {
                return redirect()->back()->with('error', __('Fecha final requerida'));
            }

            $originalEndDate = clone $date;
            $originalEndDate->endOfWeek();

            $originalRange = $this->generateDateRange($date, $originalEndDate);

            $endDate = Carbon::createFromFormat('d/m/Y', $endDate)->startOfDay();
            $rangeToClone = $this->generateWeekDateRange($startDate, $endDate);

            foreach ($rangeToClone as $weekToClone) {
                $weekDayToClone = clone $weekToClone;

                foreach ($originalRange as $originalDay) {
                    $originalDayObj = GreenFeeCalendarDay::where('date', $originalDay)->first();

                    if ($originalDayObj) {
                        $cloneDay = GreenFeeCalendarDay::firstOrCreate([
                            'date' => $weekDayToClone
                        ]);

                        $cloneDay->update(['is_open' => $originalDayObj->is_open]);

                        foreach ($originalDayObj->hours as $hour) {
                            $cloneHour = GreenFeeCalendarHour::where('green_fee_calendar_day_id', $cloneDay->id)
                                ->where('hour', $hour->hour)
                                ->first();

                            if (!$cloneHour) {
                                $cloneHour = GreenFeeCalendarHour::create([
                                    'green_fee_calendar_day_id' => $cloneDay->id,
                                    'hour' => $hour->hour,
                                    'is_open' => $hour->is_open,
                                    'tariff_id' => $hour->tariff_id
                                ]);
                            } else {
                                $cloneHour->update([
                                    'is_open' => $hour->is_open,
                                    'tariff_id' => $hour->tariff_id
                                ]);
                            }

                            foreach ($hour->prices as $price) {
                                $clonePrice = $cloneHour->prices->where('post_id', $price->post_id)->first();

                                if (!$clonePrice) {
                                    GreenFeeCalendarPrice::create([
                                        'green_fee_calendar_hour_id' => $cloneHour->id,
                                        'post_id' => $price->post_id,
                                        'tariff_id' => $price->tariff_id
                                    ]);
                                } else {
                                    $clonePrice->update(['tariff_id' => $price->tariff_id]);
                                }
                            }
                        }
                    }

                    $weekDayToClone->addDay();
                }
            }

            return redirect()->route('admin.green-fee-calendar', ['start_date' => $date->format('d/m/Y')])->with('message', __('Semana clonado correctamente'));
        }

        return redirect()->route('admin.green-fee-calendar.day', ['id' => $day->id])->with('message', __('Día clonado correctamente'));
    }


    private function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for($date = $start_date->copy(); $date->lte($end_date); $date->addDay()) {
            $dates[] = clone $date;
        }

        return $dates;
    }


    private function generateWeekDateRange(Carbon $start_date, Carbon $end_date)
    {
        $start_date->startOfWeek();
        $end_date->startOfWeek();

        $dates = [];

        for($date = $start_date->copy(); $date->lte($end_date); $date->addWeek()) {
            $dates[] = clone $date;
        }

        return $dates;
    }
}