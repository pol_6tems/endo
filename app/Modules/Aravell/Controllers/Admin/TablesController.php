<?php


namespace App\Modules\Aravell\Controllers\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Models\CustomField;
use App\Models\UserMeta;
use App\Modules\Aravell\Models\AravellNotification;
use App\User;
use Illuminate\Support\Facades\DB;

class TablesController extends AdminController
{

    protected $post_type;
    protected $post_type_plural;
    protected $section_title;
    protected $section_route;
    protected $purchase_item;
    protected $admin_view_path;


    public function index()
    {
        $users = User::select(DB::raw('users.*'))
            ->join('user_meta', 'user_meta.user_id', '=', 'users.id')
            ->join('custom_fields', 'custom_fields.id', '=', 'user_meta.custom_field_id')
            ->where('user_meta.value', '<>', 2)
            ->where('custom_fields.name', 'categoria')
            ->get();

        return view('Front::admin.tables.index', compact('users'));
    }


    public function edit($locale, $id)
    {
        $user = User::findOrFail($id);

        return View('Front::admin.tables.edit', compact('user'));
    }


    public function update($locale, $id)
    {
        $user = User::findOrFail($id);
        $amount = request('amount');
        $concept = request('concept');

        if (!$user || !$amount || !$concept) {
            abort(500);
        }

        $amount = -$amount;

        $customField = CustomField::select(DB::raw('custom_fields.*'))
            ->join('custom_field_groups', 'custom_field_groups.id', '=', 'custom_fields.cfg_id')
            ->where('custom_field_groups.post_type', 'user')
            ->where('custom_fields.name', 'saldo')
            ->first();

        $userMeta = UserMeta::firstOrNew([
            'user_id' => $user->id,
            'post_id' => 'user',
            'custom_field_id' => $customField->id
        ]);

        if (!$userMeta->exists) {
            $userMeta->value = 0;
        }

        $userMeta->value += $amount;
        $userMeta->save();

        event(new \App\Modules\Aravell\Events\WalletTransaction($user, $amount, $concept));

        AravellNotification::create([
            'user_id' => $user->id,
            'title' => 'Cargo Restaurante',
            'subtitle' => $concept,
            'route_name' => 'user.wallet',
            'image_url' => 'left-menu-icon-wallet.svg',
            'small_image_url' => 'home-icon-wallet.svg'
        ]);

        return redirect()->route('admin.tables.edit', ['id' => $user->id])->with('message', __('Cobro realizado correctamente'));
    }


    public function callAction($method, $parameters)
    {
        view()->share([
            'section_title' => $this->section_title,
            'post_type_plural' => $this->post_type_plural,
            'section_route' => $this->section_route
        ]);

        return parent::callAction($method, $parameters);
    }
}