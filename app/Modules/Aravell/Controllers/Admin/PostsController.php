<?php


namespace App\Modules\Aravell\Controllers\Admin;


use App\Models\CustomFieldGroup;
use App\Models\Mensaje;
use App\Modules\Aravell\Models\AravellNotification;
use App\Modules\Aravell\Models\PostLog;
use App\Notifications\EmailNotification;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use stdClass;

class PostsController extends \App\Http\Controllers\Admin\PostsController
{

    public function update(Request $request, $locale, $id)
    {
        $post = Post::findOrFail($id);

        if (in_array($post->type, ['reserva-green-fee', 'reserva-practicas', 'reserva-restaurant'])) {
            $post->metas;

            PostLog::create([
                'user_id' => auth()->user()->id,
                'post_id' => $post->id,
                'action' => 'update',
                'previous_state' => json_encode($post)
            ]);
        }

        $return = parent::update($request, $locale, $id);

        if ($post->type == 'reserva-restaurant') {
            $cf_groups = CustomFieldGroup::where('post_type', 'reserva-restaurant')->orderBy('order')->get();
            $estadoCf = $cf_groups->filter(function ($cf_group) {
                return $cf_group && $cf_group->fields->where('name', 'estado')->first();
            })->first()->fields->where('name', 'estado')->first();
            $data = $request->all();

            if ($data['custom_fields'][app()->getLocale()][$estadoCf->id] == 'no_valida') {
                $post->status = 'trash';
                $post->update();
                $post->delete();
            }
            
            if ($estadoCf && isset($data['custom_fields'][app()->getLocale()][$estadoCf->id]) && $post->get_field('estado') != $data['custom_fields'][app()->getLocale()][$estadoCf->id]) {
                $subtitle = 'Reserva para :date aceptada';
                $routeName = 'restaurant.show';

                if ($data['custom_fields'][app()->getLocale()][$estadoCf->id] != 'valida') {
                    $subtitle = 'Reserva para :date no aceptada';
                    $routeName = 'communication';
                }

                AravellNotification::create([
                    'user_id' => $post->author_id,
                    'post_id' => $post->id,
                    'title' => 'Restaurant',
                    'subtitle' => $subtitle,
                    'route_name' => $routeName,
                    'image_url' => 'noti-icon-2.svg',
                    'small_image_url' => 'histo-ico-1.svg'
                ]);

                if (isProEnv()) {
                    $user = $post->author;

                    $mensaje = new Mensaje();
                    $mensaje->user_id = 0;
                    $mensaje->mensaje = '';
                    $mensaje->fitxer = '';
                    $mensaje->email = '';
                    $mensaje->name = '';
                    $mensaje->type = 'email';
                    $mensaje->params = json_encode([
                        'message' => $data['custom_fields'][app()->getLocale()][$estadoCf->id] != 'valida' ? 'Reserva para el restaurante no aceptada.' : 'Reserva para el restaurante aceptada.'
                    ]);

                    $from = User::where('email', 'organitzador@aravellgolfclub.com')->first();

                    $params = new stdClass();
                    $params->usuario = $user;
                    $params->mensaje = $mensaje;
                    $params->from = $from;
                    $params->email_id = 3;

                    $notificacion = new EmailNotification($params);
                    if ($notificacion->check_email()) {
                        $user->notify($notificacion);
                    }
                }
            }
        }

        return $return;
    }


    public function aprove_pending($locale, $id) {
        $post = Post::findOrFail($id);
        $data = request()->all();

        if (in_array($post->type, ['reserva-green-fee', 'reserva-practicas', 'reserva-restaurant'])) {
            $post->metas;

            PostLog::create([
                'user_id' => auth()->user()->id,
                'post_id' => $post->id,
                'action' => 'update',
                'previous_state' => json_encode($post)
            ]);
        }

        if ($post->type == 'reserva-restaurant') {
            $cf_groups = CustomFieldGroup::where('post_type', 'reserva-restaurant')->orderBy('order')->get();
            $estadoCf = $cf_groups->filter(function ($cf_group) {
                return $cf_group && $cf_group->fields->where('name', 'estado')->first();
            })->first()->fields->where('name', 'estado')->first();
        }

        if ($post->type == 'reserva-restaurant' && $data['custom_fields'][app()->getLocale()][$estadoCf->id] == 'no_valida') {
            $return = parent::update(request(), $locale, $id);

            $post->status = 'trash';
            $post->update();
            $post->delete();
        } else {
            $return = parent::aprove_pending($locale, $id);
        }

        if ($post->type == 'reserva-restaurant') {
            if ($estadoCf && isset($data['custom_fields'][app()->getLocale()][$estadoCf->id]) && $post->get_field('estado') != $data['custom_fields'][app()->getLocale()][$estadoCf->id]) {
                $subtitle = 'Reserva para :date aceptada';
                $routeName = 'restaurant.show';

                if ($data['custom_fields'][app()->getLocale()][$estadoCf->id] != 'valida') {
                    $subtitle = 'Reserva para :date no aceptada';
                    $routeName = 'communication';
                }

                AravellNotification::create([
                    'user_id' => $post->author_id,
                    'post_id' => $post->id,
                    'title' => 'Restaurant',
                    'subtitle' => $subtitle,
                    'route_name' => $routeName,
                    'image_url' => 'noti-icon-2.svg',
                    'small_image_url' => 'histo-ico-1.svg'
                ]);

                if (isProEnv()) {
                    $user = $post->author;

                    $mensaje = new Mensaje();
                    $mensaje->user_id = 0;
                    $mensaje->mensaje = '';
                    $mensaje->fitxer = '';
                    $mensaje->email = '';
                    $mensaje->name = '';
                    $mensaje->type = 'email';
                    $mensaje->params = json_encode([
                        'message' => $data['custom_fields'][app()->getLocale()][$estadoCf->id] != 'valida' ? 'Reserva para el restaurante no aceptada.' : 'Reserva para el restaurante aceptada.'
                    ]);

                    $from = User::where('email', 'organitzador@aravellgolfclub.com')->first();

                    $params = new stdClass();
                    $params->usuario = $user;
                    $params->mensaje = $mensaje;
                    $params->from = $from;
                    $params->email_id = 3;

                    $notificacion = new EmailNotification($params);
                    if ($notificacion->check_email()) {
                        $user->notify($notificacion);
                    }
                }
            }
        }

        return $return;
    }
}