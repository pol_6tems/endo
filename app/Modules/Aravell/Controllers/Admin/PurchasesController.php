<?php


namespace App\Modules\Aravell\Controllers\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Modules\Aravell\Models\Purchase;
use App\Modules\Aravell\Repositories\PurchasesRepository;

class PurchasesController extends AdminController
{

    protected $post_type;
    protected $post_type_plural;
    protected $section_title;
    protected $section_route;
    protected $purchase_item;
    protected $admin_view_path;

    public function index()
    {

        $search = request('search');
        $purchases = app(PurchasesRepository::class)->getPurchases($search);

        if ($search) {
            $purchases->appends(['search' => $search]);
        }

        return view('Front::admin.purchases.index', compact('purchases'));
    }


    public function show($locale, $id)
    {
        $purchase = Purchase::findOrFail($id);

        return view('Front::admin.purchases.show', compact('purchase'));
    }



    public function callAction($method, $parameters)
    {
        view()->share([
            'section_title' => $this->section_title,
            'post_type_plural' => $this->post_type_plural,
            'section_route' => $this->section_route
        ]);

        return parent::callAction($method, $parameters);
    }
}