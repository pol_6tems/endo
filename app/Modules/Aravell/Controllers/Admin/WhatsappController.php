<?php


namespace App\Modules\Aravell\Controllers\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\User;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class WhatsappController extends AdminController
{
    public function index()
    {
        $users = User::with(['metas.customField'])->get();

        $users = $users->filter(function ($user) {
            return $user->get_field('telefon-1') || $user->get_field('telefon-2');
        });

        return view('Front::admin.whatsapp.index', compact('users'));
    }


    public function edit($locale, $id)
    {
        $user = User::findOrFail($id);

        return View('Front::admin.whatsapp.edit', compact('user'));
    }


    public function update($locale, $id)
    {
        $user = User::findOrFail($id);

        $twilioSid = env("TWILIO_SID");
        $twilioToken = env("TWILIO_TOKEN");

        if ($twilioSid && $twilioToken) {
            $phone = $user->get_field('telefon-1');

            if (!$phone) {
                $phone = $user->get_field('telefon-2');
            }

            if ($phone && $twilioSid && $twilioToken) {
                try {
                    $twilio = new Client($twilioSid, $twilioToken);

                    $message = $twilio->messages
                        ->create("whatsapp:+34" . $phone, // to
                            [
                                "from" => "whatsapp:+14155238886",
                                "body" => __('Your reserve code is 1234567'),
                            ]
                        );
                } catch (TwilioException $e) {
                    dd($e->getMessage());
                    return redirect()->route('admin.messaging.edit', ['id' => $user->id])->with('error', __('Error enviando mensaje'));
                }
            }
        } else {
            return redirect()->route('admin.messaging.edit', ['id' => $user->id])->with('error', __('Imposible enviar mensaje'));
        }

        return redirect()->route('admin.messaging.edit', ['id' => $user->id])->with('message', __('Mensaje enviado correctamente'));
    }
}