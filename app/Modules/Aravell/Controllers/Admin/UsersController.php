<?php


namespace App\Modules\Aravell\Controllers\Admin;


use App\Http\Controllers\Admin\UsersController as AdminUsersController;
use App\Models\CustomField;
use App\Models\UserMeta;
use App\Modules\Aravell\Models\AravellNotification;
use App\Modules\Aravell\Models\WalletTransaction;
use App\User;
use Illuminate\Support\Facades\DB;

class UsersController extends AdminUsersController
{

    public function edit($locale, User $user)
    {
        if (is_numeric($user)) {
            $user = User::findOrFail($user);
        }

        $view = parent::edit($locale, $user);
        $data = $view->getData();

        $data['transactions'] = WalletTransaction::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return View('Front::admin.users.edit', $data);
    }


    public function chargeWallet($locale, $id)
    {
        $amount = request('value');
        $concept = request('comments');

        $user = User::find($id);

        if (!$user || !$amount || !$concept) {
            abort(500);
        }

        $customField = CustomField::select(DB::raw('custom_fields.*'))
            ->join('custom_field_groups', 'custom_field_groups.id', '=', 'custom_fields.cfg_id')
            ->where('custom_field_groups.post_type', 'user')
            ->where('custom_fields.name', 'saldo')
            ->first();

        $userMeta = UserMeta::firstOrNew([
            'user_id' => $user->id,
            'post_id' => 'user',
            'custom_field_id' => $customField->id
        ]);

        if (!$userMeta->exists) {
            $userMeta->value = 0;
        }

        $userMeta->value += $amount;
        $userMeta->save();

        event(new \App\Modules\Aravell\Events\WalletTransaction($user, $amount, $concept));

        AravellNotification::create([
            'user_id' => $user->id,
            'title' => 'Recarga de Wallet',
            'subtitle' => $concept,
            'route_name' => 'user.wallet',
            'image_url' => 'left-menu-icon-wallet.svg',
            'small_image_url' => 'home-icon-wallet.svg'
        ]);
    }
}