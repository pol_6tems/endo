<?php


namespace App\Modules\Aravell\Controllers\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Modules\Aravell\Models\Tariff;
use App\Modules\Aravell\Models\TariffsCategory;
use App\Post;

class TariffsController extends AdminController
{

    public function index()
    {
        $tariffs = Tariff::with(['tariffCategories.category'])->get();

        $categories = Post::ofType('categoria-socio')
            ->ofStatus('publish')
            ->withTranslation()
            ->get();

        return view('Front::admin.tariffs', [
            'tariffs' => $tariffs,
            'categories' => $categories
        ]);
    }


    public function create()
    {
        return view('Front::admin.tariffs-create');
    }


    public function store()
    {
        $name = request('name');

        $tariff = Tariff::where('name', $name)->first();

        if ($tariff) {
            return redirect()->back()->with('error', __('Ya existe tarifa con este nombre'));
        }

        Tariff::create(['name' => $name]);

        return redirect()->route('admin.tariffs')->with('message', __(':name added successfully', ['name' => 'Tarifa']));
    }


    public function update()
    {
        $id = request('id');
        $name = request('name');

        if (!$id || !$name) {
            abort(404);
        }

        $tariff = Tariff::findOrFail($id);

        $tariff->name = $name;
        $tariff->save();
    }


    public function updatePrice()
    {
        $id = request('id');
        $categoryId = request('category');
        $price = request('price');

        if (!$id || !$categoryId || is_null($price)) {
            abort(404);
        }

        $tariff = Tariff::findOrFail($id);

        $tariffCategory = $tariff->tariffCategories->where('category_id', $categoryId)->first();

        if (!$tariffCategory) {
            $tariffCategory = TariffsCategory::firstOrNew([
                'tariff_id' => $tariff->id,
                'category_id' => $categoryId
            ]);
        }

        $tariffCategory->price = $price;

        $tariffCategory->save();
    }


    public function delete()
    {
        $id = request('id');

        if (!$id) {
            abort(404);
        }

        $tariff = Tariff::findOrFail($id);

        foreach ($tariff->tariffCategories as $tariffCategory) {
            $tariffCategory->delete();
        }

        $tariff->delete();
    }
}