<?php


namespace App\Modules\Aravell\Controllers;


use App\Http\Controllers\Controller;
use App\Models\CustomField;
use App\Models\CustomFieldGroup;
use App\Models\PostMeta;
use App\Models\UserMeta;
use App\Modules\Aravell\Models\AravellNotification;
use App\Modules\Aravell\Models\Purchase;
use App\Modules\Aravell\Models\WalletTransaction;
use App\Modules\Aravell\Requests\UserPasswordRequest;
use App\Modules\Aravell\Requests\UserUpdateRequest;
use App\Modules\Aravell\Services\ReservesService;
use App\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function edit()
    {
        $user = auth()->user();

        $user->load(['metas.customField']);

        $birthday = $user->get_field('data-naixement');

        if ($birthday) {
            $birthday = Carbon::createFromFormat('d/m/Y', $birthday);
        }

        return view('Front::user.edit', compact('user', 'birthday'));
    }


    public function postEdit(UserUpdateRequest $request)
    {
        $user = auth()->user();

        $data = $request->all();

        if (isset($data['birth_day']) && $data['birth_day'] && isset($data['birth_month']) && $data['birth_month'] &&
            isset($data['birth_year']) && $data['birth_year']) {

            $data['data-naixement'] = $data['birth_day'] . '/' . $data['birth_month'] . '/' . $data['birth_year'];
            unset($data['birth_day']);
            unset($data['birth_month']);
            unset($data['birth_year']);
        }

        $update = [
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'email' => $data['email']
        ];

        if ($request->hasFile('avatar')) {
            $avatar = $request->avatar;
            Storage::put('public/avatars', $avatar);
            $update['avatar'] = $avatar->hashName();
        }

        $user->update($update);

        $cf_groups = CustomFieldGroup::where('post_type', 'user')->orderBy('order')->get();

        foreach ($cf_groups as $cf_group) {
            foreach ($cf_group->fields as $field) {
                if (array_key_exists($field->name, $data)) {
                    $userMeta = UserMeta::firstOrNew([
                        'user_id' => $user->id,
                        'custom_field_id' => $field->id,
                        'post_id' => 'user'
                    ]);

                    $userMeta->value = $data[$field->name];
                    $userMeta->save();
                }
            }
        }

        return redirect()->route('user.edit-confirm');
    }


    public function editConfirm()
    {
        return view('Front::user.edit-confirm');
    }


    public function privacity()
    {
        return view('Front::user.privacity');
    }

    public function postPrivacity(UserPasswordRequest $request)
    {
        $user = auth()->user();

        $user->update([
            'password' => bcrypt($request->password)
        ]);

        return redirect()->route('user.privacity-confirm');
    }


    public function privacityConfirm()
    {
        return view('Front::user.privacity-confirm');
    }


    public function wallet()
    {
        $user = auth()->user();

        $userCategory = $user->get_field('categoria');

                if (!$userCategory || $userCategory->id == 2) {
            return redirect()->route('index');
        }

        $user->load(['metas.customField']);

        return view('Front::user.wallet', compact('user'));
    }


    public function postAddWallet()
    {
        $user = auth()->user();

        $userCategory = $user->get_field('categoria');

                if (!$userCategory || $userCategory->id == 2) {
            return redirect()->route('index')->with('error', __('No permitido'));
        }

        $amount = request()->input('amount');

        if (!is_numeric($amount) || $amount <= 0) {
            return redirect()->back()->with('warning', __('Cantidad mínima 1€'));
        }

        // TODO: redsys transaction

        $reserve_number = app(ReservesService::class)->getReserveNumber('recarrega-wallet');

        $charge = Post::create([
            'type' => 'recarrega-wallet',
            'status' => 'publish',
            'author_id' => $user->id,
            app()->getLocale() => [
                'title' => $reserve_number,
                'post_name' => strtolower($reserve_number)
            ]
        ]);

        $data = ['valor' => $amount];

        $cf_groups = CustomFieldGroup::where('post_type', 'recarrega-wallet')->orderBy('order')->get();
        foreach ($cf_groups as $cf_group) {
            foreach ($cf_group->fields as $field) {
                if (array_key_exists($field->name, $data)) {
                    $postMeta = PostMeta::firstOrNew([
                        'custom_field_id' => $field->id,
                        'post_id' => $charge->id,
                        'locale' => app()->getLocale()
                    ]);

                    $postMeta->value = $data[$field->name];
                    $postMeta->save();
                }
            }
        }

        Purchase::create([
            'status' => 1,
            'order_id' => $charge->title,
            'method' => 'redsys?',
            'external_order_id' => $charge->title,
            'post_id' => $charge->id,
            'user_id' => $user->id,
            'base_amount' => $amount,
            'total_amount' => $amount
        ]);

        $customField = CustomField::select(DB::raw('custom_fields.*'))
            ->join('custom_field_groups', 'custom_field_groups.id', '=', 'custom_fields.cfg_id')
            ->where('custom_field_groups.post_type', 'user')
            ->where('custom_fields.name', 'saldo')
            ->first();

        $userMeta = UserMeta::firstOrNew([
            'user_id' => $user->id,
            'post_id' => 'user',
            'custom_field_id' => $customField->id
        ]);

        if (!$userMeta->exists) {
            $userMeta->value = 0;
        }

        $userMeta->value += $amount;
        $userMeta->save();

        event(new \App\Modules\Aravell\Events\WalletTransaction($user, $amount, __('Recarga wallet')));

        AravellNotification::create([
            'user_id' => $user->id,
            'title' => 'Recarga de Wallet',
            'subtitle' => '',
            'route_name' => 'user.wallet',
            'image_url' => 'left-menu-icon-wallet.svg',
            'small_image_url' => 'home-icon-wallet.svg'
        ]);

        return redirect()->route('user.wallet-add-confirm', ['amount' => $amount]);
    }


    public function addWalletConfirm()
    {
        if (auth()->user()->role == 'No soci') {
            return redirect()->route('index');
        }

        $amount = request('amount');

        return view('Front::user.wallet-add-confirm', compact('amount'));
    }


    public function walletHistory()
    {
        $user = auth()->user();

        $userCategory = $user->get_field('categoria');

                if (!$userCategory || $userCategory->id == 2) {
            return redirect()->route('index');
        }

        // TODO: paginate?
        $transactions = WalletTransaction::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('Front::user.wallet-history', compact('user', 'transactions'));
    }
}