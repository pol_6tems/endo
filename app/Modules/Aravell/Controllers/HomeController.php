<?php


namespace App\Modules\Aravell\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Mensaje;
use App\Modules\Aravell\Models\AravellNotification;
use App\Modules\Aravell\Models\WalletTransaction;
use App\Modules\Aravell\Services\NotificationsService;
use Carbon\Carbon;

class HomeController extends Controller
{

    public function index()
    {

        $user = auth()->user();

        $user->load(['metas.customField']);

        $notificationsService = app(NotificationsService::class);

        $historyNotifications = $notificationsService->getHistory($user, 6, true);
        $newNotifications = $notificationsService->getHistory($user, null, false, true);

        $messages = Mensaje::where('type', 'chat')
            ->where('visto', false)
            ->where('to', $user->id)
            ->count();

        if ($messages) {
            $messagesNotification = new AravellNotification([
                'user_id' => $user->id,
                'title' => 'Chat',
                'subtitle' => 'Tienes mensajes sin leer',
                'route_name' => 'communication.chat',
                'image_url' => 'left-menu-icon-notifications-active.svg',
                'small_image_url' => 'header-notification-active.svg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            $newNotifications->push($messagesNotification);
        }

        $notificationsService->updateSeenNotifications($user);

        $walletTransactions = null;

        $userCategory = $user->get_field('categoria');

        // 2 == public category
        if ($userCategory && $userCategory->id != 2) {
            $walletTransactions = WalletTransaction::where('user_id', $user->id)
                ->orderBy('created_at', 'desc')
                ->limit(4)
                ->get();
        }

        return View('Front::home', compact('historyNotifications', 'user', 'walletTransactions', 'newNotifications'));
    }
}