<?php


namespace App\Modules\Aravell\Controllers;


use App\Http\Controllers\Controller;
use App\Models\CustomField;
use App\Models\CustomFieldGroup;
use App\Models\Mensaje;
use App\Models\PostMeta;
use App\Models\UserMeta;
use App\Modules\Aravell\Models\GreenFeeCalendarDay;
use App\Modules\Aravell\Models\PostLog;
use App\Modules\Aravell\Models\Purchase;
use App\Modules\Aravell\Services\ReservesService;
use App\Notifications\EmailNotification;
use App\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use stdClass;

class ReservesController extends Controller
{

    public function index()
    {
        $user = auth()->user();

        $user->load(['metas.customField']);

        // TODO: paginate?
        $reserves = app(ReservesService::class)->getHistory($user);

        $now = Carbon::now();

        $futureReserves = $reserves->filter(function ($reserve) use ($now) {
            if (!isset($reserve->time)) {
                $horaField = 'hora-sortida';

                if ($reserve->type == 'reserva-restaurant') {
                    $horaField = 'hora';
                }

                $reserve->time = Carbon::createFromFormat('d/m/Y H:i', $reserve->get_field('data') . ' ' . $reserve->get_field($horaField));
            }

            return $reserve->time >= $now;
        })->sortBy('time');

        $pastReserves = $reserves->filter(function ($reserve) use ($now) {
            if (!isset($reserve->time)) {
                $horaField = 'hora-sortida';

                if ($reserve->type == 'reserva-restaurant') {
                    $horaField = 'hora';
                }

                $reserve->time = Carbon::createFromFormat('d/m/Y H:i', $reserve->get_field('data') . ' ' . $reserve->get_field($horaField));
            }

            return $reserve->time < $now;
        })->sortByDesc('time');

        return view('Front::reserves.index', compact('futureReserves', 'pastReserves', 'user'));
    }


    public function create($locale)
    {
        $user = auth()->user();

        $user->load(['metas.customField']);

        $type = $this->getType($locale, 'create');

        $priceLoggedPlayer = $user->get_field('categoria') ? $user->get_field('categoria')->get_field('precio-salida') : null;

        $normalUserCategory = Post::select(DB::raw('posts.*'))
            ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
            ->where('posts.type', 'categoria-socio')
            ->where('post_translations.locale', app()->getLocale())
            ->where('post_translations.post_name', 'publico')
            ->first();

        if (!$normalUserCategory) {
            abort(500);
        }

        $normalPrice = $normalUserCategory->get_field('precio-salida');

        if (!$priceLoggedPlayer) {
            $priceLoggedPlayer = $normalPrice;
        }

        $params = [
            'type' => $type,
            'price_logged_player' => $priceLoggedPlayer,
            'normal_price' => $normalPrice,
            'type_logged_player' =>  $user->get_field('categoria') ? $user->get_field('categoria')->id : $normalUserCategory->id,
            'type_normal_price' => $normalUserCategory->id
        ];

        if ($type == 'green-fee') {
            $extras = Post::with(['metas.customField', 'translations'])
                ->where('type', 'extra')
                ->get();

            $params['extras'] = $extras;

            $params['horas_salida'] = app(ReservesService::class)->getHorasSalida($type);

            $availability = $this->checkGreenFeeAvailability(Carbon::tomorrow(), $params['horas_salida']->first()->get_field('hora') . ':00');

            if ($availability) {
                $availabilityPrice = $availability->where('post_id', $user->get_field('categoria') ? $user->get_field('categoria')->id : $normalUserCategory->id)->first();

                if ($availabilityPrice) {
                    $params['price_logged_player'] = $availabilityPrice->price;
                }

                $availabilityNormalPrice = $availability->where('post_id', $normalUserCategory->id)->first();

                if ($availabilityNormalPrice) {
                    $params['normal_price'] = $availabilityNormalPrice->price;
                }
            }
        } else {
            $params['horas_restaurant'] = app(ReservesService::class)->getHorasRestaurant();
        }

        return view('Front::' . $type . '.create', $params);
    }


    /**
     * Show resume
     *
     * @param $locale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store($locale)
    {
        $user = auth()->user();

        $user->load(['metas.customField']);

        $type = $this->getType($locale, 'store');

        $params = [
            'type' => $type
        ];

        $reserveId = request('reserve_id');

        if ($reserveId) {
            $reserve = Post::where('id', $reserveId)->where('type', 'reserva-' . $type)->where('author_id', $user->id)->first();
        }

        if (!isset($reserve)) {
            $data = request()->all();
            $reserveTimeField = 'hora-sortida';

            if ($type == 'restaurant') {
                $reserveTimeField = 'hora';
            }

            $reserveTime = Carbon::createFromFormat('d/m/Y H:i:s', $data['data'] . ' ' . $data[$reserveTimeField] . ':00');

            if ($reserveTime < Carbon::now()->addMinutes(15)) {
                return redirect()->back()->withInput()->with('error', __('Franja horaria no disponible'));
            }

            $availability = $this->checkGreenFeeAvailability($reserveTime, $reserveTime->format('H:i:s'));

            if ($type != 'restaurant') {
                if (!$availability) {
                    return redirect()->back()->withInput()->with('error', __('Fecha/Hora salida no disponibles'));
                }
            }

            $extras = null;
            $players = null;

            if (isset($data['extras'])) {
                $extras = $data['extras'];
                unset($data['extras']);
            }

            if (isset($data['jugadors'])) {
                $players = $data['jugadors'];
                unset($data['jugadors']);
            }

            if ($type == 'restaurant') {
                $data['estado'] = 'pendiente';
            }

            $normalUserCategory = Post::select(DB::raw('posts.*'))
                ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
                ->where('posts.type', 'categoria-socio')
                ->where('post_translations.locale', app()->getLocale())
                ->where('post_translations.post_name', 'publico')
                ->first();

            if (!$normalUserCategory) {
                abort(500);
            }

            $availabilityNormalPrice = $availability ? $availability->where('post_id', $normalUserCategory->id)->first() : null;

            $normalPrice = $availabilityNormalPrice ? $availabilityNormalPrice->price : $normalUserCategory->get_field('precio-salida');
            $normalType = $normalUserCategory->id;

            // Avoid html editions
            if ($type == 'green-fee') {
                $cfLlicencia = CustomField::select(DB::raw('custom_fields.*'))
                    ->join('custom_field_groups', 'custom_field_groups.id', '=', 'custom_fields.cfg_id')
                    ->where('custom_fields.name', 'llicencia')
                    ->where('custom_field_groups.post_type', 'user')
                    ->where('custom_field_groups.title', 'Dades Aravell')
                    ->first();

                $preu = 0;

                $licenses = [];

                foreach ($players as $key => $player) {
                    if (array_first($players) == $player) {
                        $playerType = $user->get_field('categoria') ? $user->get_field('categoria')->id : $normalType;

                        $availabilityPrice = $availability->where('post_id', $user->get_field('categoria') ? $user->get_field('categoria')->id : $normalUserCategory->id)->first();

                        $playerPreu = $availabilityPrice ? $availabilityPrice->price : $normalPrice;

                    } else {
                        $playerPreu = $normalPrice;
                        $playerType = $normalType;

                        if (isset($player['llicencia']) && $player['llicencia'] && !in_array($player['llicencia'], $licenses)) {
                            $userPlayer = $this->getPlayerByLicense($player['llicencia'], $cfLlicencia->id);

                            if ($userPlayer) {
                                $availabilityPrice = $availability->where('post_id', $userPlayer->get_field('categoria') ? $userPlayer->get_field('categoria')->id : $normalUserCategory->id)->first();
                                $playerPreu = $availabilityPrice ? $availabilityPrice->price : $normalPrice;
                                $playerType = $availabilityPrice ? $availabilityPrice->post_id : $normalType;
                                $licenses[] = $player['llicencia'];
                            }
                        }
                    }

                    $players[$key]['preu'] = $playerPreu;
                    $players[$key]['type'] = $playerType;
                    $preu += $playerPreu;
                }

                $data['preu'] = $preu;
            }

            $reserve_number = app(ReservesService::class)->getReserveNumber($type);

            $reserve = Post::create([
                'type' => 'reserva-' . $type,
                'status' => 'draft',
                'author_id' => $user->id,
                app()->getLocale() => [
                    'title' => $reserve_number,
                    'post_name' => strtolower($reserve_number)
                ]
            ]);

            $this->updateReserve($reserve, $data, $extras, $players, $normalPrice, $normalType, $type);
        }

        $params['reserve'] = $reserve;
        $params['reserve_total'] = $reserve->get_field('preu');
        $extras = $reserve->get_field('extres');

        if ($extras && $params['reserve_total']) {
            foreach ($extras as $extra) {
                $params['reserve_total'] += ($extra['extra']['value']->get_field('preu') * $extra['quantitat']['value']);
            }
        }

        return view('Front::' . $type . '.resume', $params);
    }


    /**
     * Show pay methods
     *
     * @param $locale
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pay($locale)
    {
        $user = auth()->user();

        $user->load(['metas.customField']);

        $type = $this->getType($locale, 'pay');

        $params = [
            'type' => $type,
            'user' => auth()->user()
        ];

        $reserveId = request('reserve_id');

        if (!$reserveId) {
            abort(404);
        }

        $reserve = Post::where('id', $reserveId)->where('type', 'reserva-' . $type)->where('author_id', $user->id)->first();

        if (!$reserve) {
            abort(403);
        }

        if ($type == 'restaurant') {
            $reserve->update([
                'status' => 'pending'
            ]);

            if (isProEnv()) {
                $orgUser = User::where('email', 'organitzador@aravellgolfclub.com')->first();

                $reserveTime = Carbon::createFromFormat('d/m/Y H:i', $reserve->get_field('data') . ' ' . $reserve->get_field('hora'));

                $mensaje = new Mensaje();
                $mensaje->user_id = 0;
                $mensaje->mensaje = '';
                $mensaje->fitxer = '';
                $mensaje->email = '';
                $mensaje->name = '';
                $mensaje->type = 'email';
                $mensaje->params = json_encode([
                    'message' => __('Id: ') . $reserve->title . PHP_EOL
                        . __('Cliente: ') . $user->email . PHP_EOL
                        . __('Fecha: ') . $reserveTime->format('d/m/Y H:i:s') . PHP_EOL
                        . __('Adultos: ') . $reserve->get_field('adults') . PHP_EOL
                        . __('Niños: ') . $reserve->get_field('nens')
                ]);

                $from = User::where('email', 'sergi.solsona@6tems.com')->first();

                $params = new stdClass();
                $params->usuario = $orgUser;
                $params->mensaje = $mensaje;
                $params->from = $from;
                $params->email_id = 8;

                $notificacion = new EmailNotification($params);
                if ($notificacion->check_email()) {
                    $orgUser->notify($notificacion);
                }
            }

            return redirect()->route($type . '.confirm', ['id' => $reserve->id]);
        }

        $params['reserve'] = $reserve;
        $params['reserve_total'] = $reserve->get_field('preu');
        $extras = $reserve->get_field('extres');

        if ($extras && $params['reserve_total']) {
            foreach ($extras as $extra) {
                $params['reserve_total'] += ($extra['extra']['value']->get_field('preu') * $extra['quantitat']['value']);
            }
        }

        return view('Front::' . $type . '.pay', $params);
    }


    public function payPost($locale)
    {
        // TODO here: payment with wallet
        // TODO here: needs purchase row?
        // TODO: maybe move to a private method?
        // TODO: create new endpoints for card payments (redsys?)

        $user = auth()->user();
        $type = $this->getType($locale, 'payPost');

        $reserveId = request('reserve_id');

        if (!$reserveId) {
            abort(404);
        }

        $reserve = Post::where('id', $reserveId)->where('type', 'reserva-' . $type)->where('author_id', $user->id)->first();

        if (!$reserve) {
            abort(403);
        }

        if ($reserve->status == 'publish') {
            return redirect()->route($type . '.confirm', ['id' => $reserve->id]);
        }

        $amount = $reserve->get_field('preu');

        $extras = $reserve->get_field('extres');

        if ($extras && $amount) {
            foreach ($extras as $extra) {
                $amount += ($extra['extra']['value']->get_field('preu') * $extra['quantitat']['value']);
            }
        }

        /*if (!$user->get_field('saldo') || $user->get_field('saldo') < $amount) {
            abort(403, 'Insufficient balance');
        }*/

        $customField = CustomField::select('custom_fields.*')
            ->join('custom_field_groups', 'custom_field_groups.id', '=', 'custom_fields.cfg_id')
            ->where('custom_fields.name', 'saldo')
            ->where('custom_field_groups.post_type', 'user')
            ->first();

        if ($customField) {
            unset($reserve->preu);
            unset($reserve->extres);

            $userSaldo = UserMeta::where('user_id', $user->id)
                ->where('post_id', 'user')
                ->where('custom_field_id', $customField->id)
                ->first();

            Purchase::create([
                'status' => 1,
                'order_id' => $reserve->title,
                'method' => 'wallet',
                'external_order_id' => $reserve->title,
                'post_id' => $reserve->id,
                'user_id' => $user->id,
                'base_amount' => $amount,
                'total_amount' => $amount
            ]);

            $userSaldo->value -= $amount;
            $userSaldo->save();

            $reserve->status = 'publish';
            $reserve->save();

            $reserve->set_field('preu-total', $amount);

            event(new \App\Modules\Aravell\Events\WalletTransaction($user, -$amount, __('Reserva :type :id', ['type' => 'Green Fee', 'id' => $reserve->title])));

            if (isProEnv()) {
                $mensaje = new Mensaje();
                $mensaje->user_id = 0;
                $mensaje->mensaje = '';
                $mensaje->fitxer = '';
                $mensaje->email = '';
                $mensaje->name = '';
                $mensaje->type = 'email';
                $mensaje->params = json_encode([
                    'reserve_id' => $reserve->id
                ]);

                $from = User::where('email', 'organitzador@aravellgolfclub.com')->first();

                $params = new stdClass();
                $params->usuario = $user;
                $params->mensaje = $mensaje;
                $params->from = $from;
                $params->email_id = 4;

                $notificacion = new EmailNotification($params);
                if ($notificacion->check_email()) {
                    $user->notify($notificacion);
                }

                $orgUser = User::where('email', 'organitzador@aravellgolfclub.com')->first();

                $reserveTime = Carbon::createFromFormat('d/m/Y H:i', $reserve->get_field('data') . ' ' . $reserve->get_field('hora-sortida'));

                $mensaje = new Mensaje();
                $mensaje->user_id = 0;
                $mensaje->mensaje = '';
                $mensaje->fitxer = '';
                $mensaje->email = '';
                $mensaje->name = '';
                $mensaje->type = 'email';
                $mensaje->params = json_encode([
                    'message' => __('Id: ') . $reserve->title . PHP_EOL
                        . __('Cliente: ') . $user->email . PHP_EOL
                        . __('Fecha: ') . $reserveTime->format('d/m/Y H:i:s')
                ]);

                $from = User::where('email', 'sergi.solsona@6tems.com')->first();

                $params = new stdClass();
                $params->usuario = $orgUser;
                $params->mensaje = $mensaje;
                $params->from = $from;
                $params->email_id = 7;

                $notificacion = new EmailNotification($params);
                if ($notificacion->check_email()) {
                    $orgUser->notify($notificacion);
                }
            }
            return redirect()->route('green-fee.confirm', ['id' => $reserve->id]);
        }

        abort(500);
    }


    public function confirm($locale, $id)
    {
        $user = auth()->user();

        $user->load(['metas.customField']);

        $type = $this->getType($locale, $id . '/confirm');

        $reserve = Post::where('id', $id)->where('type', 'reserva-' . $type)->where('author_id', $user->id)->first();

        if (!$reserve) {
            abort(403);
        }

        return view('Front::' . $type . '.confirm', compact('reserve'));
    }


    public function show($locale, $id)
    {
        $user = auth()->user();

        $user->load(['metas.customField']);

        $type = $this->getType($locale, $id);

        $reserve = Post::findOrFail($id);

        if ($reserve->author_id != $user->id) {
            abort(403);
        }

        $reserve_total = $reserve->get_field('preu');
        $extras = $reserve->get_field('extres');

        if ($extras && $reserve_total) {
            foreach ($extras as $extra) {
                $reserve_total += ($extra['extra']['value']->get_field('preu') * $extra['quantitat']['value']);
            }
        }

        $previousRoute = app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName();

        $isAfterConfirm = strpos($previousRoute, '.confirm') !== false;

        return view('Front::' . $type . '.show', compact('reserve', 'isAfterConfirm', 'reserve_total'));
    }


    public function edit($locale, $id)
    {
        $user = auth()->user();

        $user->load(['metas.customField']);

        $type = $this->getType($locale, $id . '/edit');

        $reserve = Post::findOrFail($id);

        $horaField = 'hora-sortida';

        if ($reserve->type == 'reserva-restaurant') {
            $horaField = 'hora';
        }

        $reserve->time = Carbon::createFromFormat('d/m/Y H:i', $reserve->get_field('data') . ' ' . $reserve->get_field($horaField));

        if ($reserve->author_id != $user->id || $reserve->time < Carbon::now()) {
            abort(403);
        }

        $reserve_total = $reserve->get_field('preu');
        $extras = $reserve->get_field('extres');

        if ($extras && $reserve_total) {
            foreach ($extras as $extra) {
                $reserve_total += ($extra['extra']['value']->get_field('preu') * $extra['quantitat']['value']);
            }
        }

        $priceLoggedPlayer = $user->get_field('categoria') ? $user->get_field('categoria')->get_field('precio-salida') : null;

        $normalUserCategory = Post::select(DB::raw('posts.*'))
            ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
            ->where('posts.type', 'categoria-socio')
            ->where('post_translations.locale', app()->getLocale())
            ->where('post_translations.post_name', 'publico')
            ->first();

        if (!$normalUserCategory) {
            abort(500);
        }

        $normalPrice = $normalUserCategory->get_field('precio-salida');

        if (!$priceLoggedPlayer) {
            $priceLoggedPlayer = $normalPrice;
        }

        $params = [
            'type' => $type,
            'reserve' => $reserve,
            'reserve_total' => $reserve_total,
            'price_logged_player' => $priceLoggedPlayer,
            'normal_price' => $normalPrice,
            'type_logged_player' =>  $user->get_field('categoria') ? $user->get_field('categoria')->id : $normalUserCategory->id,
            'type_normal_price' => $normalUserCategory->id
        ];

        if ($type == 'green-fee') {
            $extras = Post::with(['metas.customField', 'translations'])
                ->where('type', 'extra')
                ->get();

            $params['extras'] = $extras;

            $rExtras = $reserve->get_field('extres');
            $reserveExtras = [];
            foreach ($rExtras as $rExtra) {
                $reserveExtras[$rExtra['extra']['value']->id] = $rExtra['quantitat']['value'];
            }

            $params['reserve_extras'] = $reserveExtras;
            $params['horas_salida'] = app(ReservesService::class)->getHorasSalida($type);

            $availability = $this->checkGreenFeeAvailability(Carbon::createFromFormat('d/m/Y', $reserve->get_field('data')), $reserve->get_field('hora-sortida') . ':00', $reserve->id);

            if ($availability) {
                $availabilityPrice = $availability->where('post_id', $user->get_field('categoria') ? $user->get_field('categoria')->id : $normalUserCategory->id)->first();

                if ($availabilityPrice) {
                    $params['price_logged_player'] = $availabilityPrice->price;
                }

                $availabilityNormalPrice = $availability->where('post_id', $normalUserCategory->id)->first();

                if ($availabilityNormalPrice) {
                    $params['normal_price'] = $availabilityNormalPrice->price;
                }
            }
        } else {
            $params['horas_restaurant'] = app(ReservesService::class)->getHorasRestaurant();
        }

        return view('Front::' . $type . '.edit', $params);
    }


    public function update($locale, $id)
    {
        $user = auth()->user();
        $type = $this->getType($locale, $id);

        $reserve = Post::find($id);

        $data = request()->all();
        $reserveTimeField = 'hora-sortida';

        if ($type == 'restaurant') {
            $reserveTimeField = 'hora';
        }

        $reserveTime = Carbon::createFromFormat('d/m/Y H:i', $data['data'] . ' ' . $data[$reserveTimeField]);

        if ($reserveTime < Carbon::now()->addMinutes(15)) {
            return redirect()->back()->withInput()->with('error', __('Franja horaria no disponible'));
        }

        $availability = $this->checkGreenFeeAvailability($reserveTime, $reserveTime->format('H:i:s'), $reserve->id);

        if ($type == 'restaurant') {
            $data['estado'] = 'pendiente';
        } else {
            if (!$availability) {
                return redirect()->back()->withInput()->with('error', __('Fecha/Hora salida no disponibles'));
            }
        }

        // Save current status
        PostLog::create([
            'user_id' => auth()->user()->id,
            'post_id' => $reserve->id,
            'action' => 'update',
            'previous_state' => json_encode($reserve)
        ]);

        if ($type == 'restaurant') {
            $reserve->set_field(request()->all(), false);

            $reserve->update(['status' => 'pending']);

            if (isProEnv()) {
                $orgUser = User::where('email', 'organitzador@aravellgolfclub.com')->first();

                $mensaje = new Mensaje();
                $mensaje->user_id = 0;
                $mensaje->mensaje = '';
                $mensaje->fitxer = '';
                $mensaje->email = '';
                $mensaje->name = '';
                $mensaje->type = 'email';
                $mensaje->params = json_encode([
                    'message' => __('Id: ') . $reserve->title . PHP_EOL
                        . __('Cliente: ') . $user->email . PHP_EOL
                        . __('Fecha: ') . $reserveTime->format('d/m/Y H:i') . PHP_EOL
                        . __('Adultos: ') . $reserve->get_field('adults') . PHP_EOL
                        . __('Niños: ') . $reserve->get_field('nens')
                ]);

                $from = User::where('email', 'sergi.solsona@6tems.com')->first();

                $params = new stdClass();
                $params->usuario = $orgUser;
                $params->mensaje = $mensaje;
                $params->from = $from;
                $params->email_id = 8;

                $notificacion = new EmailNotification($params);
                if ($notificacion->check_email()) {
                    $orgUser->notify($notificacion);
                }
            }

            return redirect()->route($type . '.update-confirm', ['id' => $reserve->id]);
        }

        $extras = null;
        $players = null;

        if (isset($data['extras'])) {
            $extras = $data['extras'];
            unset($data['extras']);
        }

        if (isset($data['jugadors'])) {
            $players = $data['jugadors'];
            unset($data['jugadors']);
        }

        $normalUserCategory = Post::select(DB::raw('posts.*'))
            ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
            ->where('posts.type', 'categoria-socio')
            ->where('post_translations.locale', app()->getLocale())
            ->where('post_translations.post_name', 'publico')
            ->first();

        if (!$normalUserCategory) {
            abort(500);
        }

        $availabilityNormalPrice = $availability ? $availability->where('post_id', $normalUserCategory->id)->first() : null;

        $normalPrice = $availabilityNormalPrice ? $availabilityNormalPrice->price : $normalUserCategory->get_field('precio-salida');
        $normalType = $normalUserCategory->id;

        if ($type == 'green-fee') {
            // Avoid html editions
            $cfLlicencia = CustomField::select(DB::raw('custom_fields.*'))
                ->join('custom_field_groups', 'custom_field_groups.id', '=', 'custom_fields.cfg_id')
                ->where('custom_fields.name', 'llicencia')
                ->where('custom_field_groups.post_type', 'user')
                ->where('custom_field_groups.title', 'Dades Aravell')
                ->first();

            $preu = 0;

            $licenses = [];

            foreach ($players as $key => $player) {
                if (array_first($players) == $player) {
                    $playerType = $user->get_field('categoria') ? $user->get_field('categoria')->id : $normalType;

                    $availabilityPrice = $availability->where('post_id', $user->get_field('categoria') ? $user->get_field('categoria')->id : $normalUserCategory->id)->first();

                    $playerPreu = $availabilityPrice ? $availabilityPrice->price : $normalPrice;

                } else {
                    $playerPreu = $normalPrice;
                    $playerType = $normalType;

                    if (isset($player['llicencia']) && $player['llicencia'] && !in_array($player['llicencia'], $licenses)) {
                        $userPlayer = $this->getPlayerByLicense($player['llicencia'], $cfLlicencia->id);

                        if ($userPlayer) {
                            $availabilityPrice = $availability->where('post_id', $userPlayer->get_field('categoria') ? $userPlayer->get_field('categoria')->id : $normalUserCategory->id)->first();
                            $playerPreu = $availabilityPrice ? $availabilityPrice->price : $normalPrice;
                            $playerType = $availabilityPrice ? $availabilityPrice->post_id : $normalType;
                            $licenses[] = $player['llicencia'];
                        }
                    }
                }

                $players[$key]['preu'] = $playerPreu;
                $players[$key]['type'] = $playerType;
                $preu += $playerPreu;
            }

            $data['preu'] = $preu;

            $currentAmount = $reserve->get_field('preu-total');
            $newAmount = $data['preu'];

            if ($extras) {
                foreach ($extras as $extraId => $extraQuantity) {
                    $extraObj = Post::find($extraId);

                    if ($extraObj) {
                        $newAmount += $extraObj->get_field('preu') * $extraQuantity;
                    }
                }
            }

            $this->updateReserve($reserve, $data, $extras, $players, $normalPrice, $normalType, $type);

            if ($currentAmount != $newAmount) {
                $userCategory = $user->get_field('categoria');

                $transactionAmount = $newAmount - $currentAmount;

                if (!$userCategory || $userCategory->id == 2) {

                    if ($transactionAmount < 0) {
                        dd('TODO: enviar emails a user + admin (falta definir admin)');

                        if (isProEnv()) {
                            $orgUser = User::where('email', 'organitzador@aravellgolfclub.com')->first();

                            $mensaje = new Mensaje();
                            $mensaje->user_id = 0;
                            $mensaje->mensaje = '';
                            $mensaje->fitxer = '';
                            $mensaje->email = '';
                            $mensaje->name = '';
                            $mensaje->type = 'email';
                            $mensaje->params = json_encode([
                                'reserve_id' => $reserve->id,
                            ]);

                            $from = $orgUser;

                            $params = new stdClass();
                            $params->usuario = $user;
                            $params->mensaje = $mensaje;
                            $params->from = $from;
                            $params->email_id = 5;

                            $notificacion = new EmailNotification($params);
                            if ($notificacion->check_email()) {
                                $user->notify($notificacion);
                            }

                            $mensaje = new Mensaje();
                            $mensaje->user_id = 0;
                            $mensaje->mensaje = '';
                            $mensaje->fitxer = '';
                            $mensaje->email = '';
                            $mensaje->name = '';
                            $mensaje->type = 'email';
                            $mensaje->params = json_encode([
                                'reserve_title' => $reserve->title,
                                'reserve_id' => $reserve->id,
                                'transaction_amount' => -$transactionAmount,
                                'user_name' => $user->fullname(),
                                'user_email' => $user->email
                            ]);

                            $from = User::where('email', 'sergi.solsona@6tems.com')->first();

                            $params = new stdClass();
                            $params->usuario = $orgUser;
                            $params->mensaje = $mensaje;
                            $params->from = $from;
                            $params->email_id = 6;

                            $notificacion = new EmailNotification($params);
                            if ($notificacion->check_email()) {
                                $user->notify($notificacion);
                            }

                        }
                    } else {
                        dd('TODO: pasarela de preu redsys per a diferencia');
                    }
                } else {

                    $customField = CustomField::select('custom_fields.*')
                        ->join('custom_field_groups', 'custom_field_groups.id', '=', 'custom_fields.cfg_id')
                        ->where('custom_fields.name', 'saldo')
                        ->where('custom_field_groups.post_type', 'user')
                        ->first();

                    if ($customField) {
                        $userSaldo = UserMeta::where('user_id', $user->id)
                            ->where('post_id', 'user')
                            ->where('custom_field_id', $customField->id)
                            ->first();


                        $orderId = $reserve->title;
                        $aux = $orderId;
                        $i = 1;

                        while (Purchase::where('order_id', $aux)->exists()) {
                            $aux = $orderId . '-' . $i;
                            $i++;
                        }

                        $orderId = $aux;

                        Purchase::create([
                            'status' => 1,
                            'order_id' => $orderId,
                            'method' => 'wallet',
                            'external_order_id' => $orderId,
                            'post_id' => $reserve->id,
                            'user_id' => $user->id,
                            'base_amount' => $transactionAmount,
                            'total_amount' => $transactionAmount
                        ]);

                        $userSaldo->value -= $transactionAmount;
                        $userSaldo->save();

                        unset($reserve->preu);
                        unset($reserve->extres);
                        unset($reserve->preu_total);

                        $reserve->status = 'publish';
                        $reserve->save();

                        $reserve->set_field('preu-total', $newAmount);

                        event(new \App\Modules\Aravell\Events\WalletTransaction($user, -$transactionAmount, __('Modificació reserva :type :id', ['type' => 'Green Fee', 'id' => $reserve->title])));
                    }
                }

                return redirect()->route('green-fee.update-confirm', ['id' => $reserve->id]);
            }
        }

        return redirect()->route($type . '.update-confirm', ['id' => $reserve->id]);
    }


    public function updateConfirm($locale, $id)
    {
        $user = auth()->user();

        $user->load(['metas.customField']);

        $type = $this->getType($locale, $id . '/update-confirm');

        $reserve = Post::findOrFail($id);

        if ($reserve->author_id != $user->id) {
            abort(403);
        }

        $params = [
            'reserve' => $reserve
        ];

        if ($type == 'green-fee') {
            $params['reserve_total'] = $reserve->get_field('preu');
            $extras = $reserve->get_field('extres');

            if ($extras && $params['reserve_total']) {
                foreach ($extras as $extra) {
                    $params['reserve_total'] += ($extra['extra']['value']->get_field('preu') * $extra['quantitat']['value']);
                }
            }
        }

        return view('Front::' . $type . '.update-confirm', $params);
    }



    public function preDestroy($locale, $id)
    {
        $user = auth()->user();

        $user->load(['metas.customField']);

        $type = $this->getType($locale, $id . '/pre-delete');

        $reserve = Post::findOrFail($id);

        if ($reserve->author_id != $user->id) {
            abort(403);
        }

        $reserve_total = $reserve->get_field('preu');
        $extras = $reserve->get_field('extres');

        if ($extras && $reserve_total) {
            foreach ($extras as $extra) {
                $reserve_total += ($extra['extra']['value']->get_field('preu') * $extra['quantitat']['value']);
            }
        }

        return view('Front::' . $type . '.pre-delete', compact('reserve', 'reserve_total'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $type = $this->getType($locale, $id);

        $reserve = Post::findOrFail($id);

        $user = auth()->user();

        if ($type == 'green-fee') {
            $user->load(['metas.customField']);

            $userCategory = $user->get_field('categoria');

            $reserveAmount = $reserve->get_field('preu-total');

            if (!$userCategory || $userCategory->id == 2) {
                dd('TODO: enviar emails a user + admin (falta definir admin)');

                if (isProEnv()) {
                    $orgUser = User::where('email', 'organitzador@aravellgolfclub.com')->first();

                    $mensaje = new Mensaje();
                    $mensaje->user_id = 0;
                    $mensaje->mensaje = '';
                    $mensaje->fitxer = '';
                    $mensaje->email = '';
                    $mensaje->name = '';
                    $mensaje->type = 'email';
                    $mensaje->params = json_encode([
                        'reserve_id' => $reserve->id,
                    ]);

                    $from = $orgUser;

                    $params = new stdClass();
                    $params->usuario = $user;
                    $params->mensaje = $mensaje;
                    $params->from = $from;
                    $params->email_id = 5;

                    $notificacion = new EmailNotification($params);
                    if ($notificacion->check_email()) {
                        $user->notify($notificacion);
                    }

                    $mensaje = new Mensaje();
                    $mensaje->user_id = 0;
                    $mensaje->mensaje = '';
                    $mensaje->fitxer = '';
                    $mensaje->email = '';
                    $mensaje->name = '';
                    $mensaje->type = 'email';
                    $mensaje->params = json_encode([
                        'reserve_title' => $reserve->title,
                        'reserve_id' => $reserve->id,
                        'transaction_amount' => -$reserveAmount,
                        'user_name' => $user->fullname(),
                        'user_email' => $user->email
                    ]);

                    $from = User::where('email', 'sergi.solsona@6tems.com')->first();

                    $params = new stdClass();
                    $params->usuario = $orgUser;
                    $params->mensaje = $mensaje;
                    $params->from = $from;
                    $params->email_id = 6;

                    $notificacion = new EmailNotification($params);
                    if ($notificacion->check_email()) {
                        $user->notify($notificacion);
                    }

                }
            } else {
                $customField = CustomField::select('custom_fields.*')
                    ->join('custom_field_groups', 'custom_field_groups.id', '=', 'custom_fields.cfg_id')
                    ->where('custom_fields.name', 'saldo')
                    ->where('custom_field_groups.post_type', 'user')
                    ->first();

                if ($customField) {
                    $userSaldo = UserMeta::where('user_id', $user->id)
                        ->where('post_id', 'user')
                        ->where('custom_field_id', $customField->id)
                        ->first();


                    $orderId = $reserve->title;
                    $aux = $orderId;
                    $i = 1;

                    while (Purchase::where('order_id', $aux)->exists()) {
                        $aux = $orderId . '-' . $i;
                        $i++;
                    }

                    $orderId = $aux;

                    Purchase::create([
                        'status' => 1,
                        'order_id' => $orderId,
                        'method' => 'wallet',
                        'external_order_id' => $orderId,
                        'post_id' => $reserve->id,
                        'user_id' => $user->id,
                        'base_amount' => -$reserveAmount,
                        'total_amount' => -$reserveAmount
                    ]);

                    $userSaldo->value += $reserveAmount;
                    $userSaldo->save();

                    event(new \App\Modules\Aravell\Events\WalletTransaction($user, $reserveAmount, __('Cancelación reserva :type :id', ['type' => 'Green Fee', 'id' => $reserve->title])));
                }
            }
        } else {
            // TODO: advice admin.
        }

        unset($reserve->preu);
        unset($reserve->extres);
        unset($reserve->preu_total);

        $reserve->update([
            'status' => 'trash'
        ]);

        $reserve->delete();

        return redirect()->route($type . '.delete-confirm', ['id' => $reserve->id]);
    }


    public function destroyConfirm($locale, $id)
    {
        $user = auth()->user();

        $user->load(['metas.customField']);
        $type = $this->getType($locale, $id . '/delete-confirm');

        $reserve = Post::withTrashed()->findOrFail($id);

        if ($reserve->author_id != $user->id) {
            abort(403);
        }

        if ($reserve->status != 'trash') {
            return redirect()->route($type . '.show', ['id' => $reserve->id]);
        }

        $user->load(['metas.customField']);

        $userCategory = $user->get_field('categoria');

        $hasWallet = !$userCategory || $userCategory->id == 2 ? false : true;

        return view('Front::reserves.delete-confirm', compact('reserve', 'hasWallet'));
    }


    public function updateReserve($reserve, $data, $extras, $players, $normalPrice, $normalType, $type)
    {
        $cf_groups = CustomFieldGroup::where('post_type', 'reserva-' . $type)->orderBy('order')->get();
        $extrasFg = null;
        $extraF = null;
        $quantitatExtraF = null;

        $jugadorsFg = null;
        $jugadorNomF = null;
        $jugadorCognomF = null;
        $jugadorLlicenciaF = null;
        $jugadorPreu = null;
        $jugadorType = null;

        foreach ($cf_groups as $cf_group) {
            foreach ($cf_group->fields as $field) {
                if ($field->name == 'extres') {
                    $extrasFg = $field;

                    $extraF = CustomField::where('name', 'extra')->where('parent_id', $extrasFg->id)->first();
                    $quantitatExtraF = CustomField::where('name', 'quantitat')->where('parent_id', $extrasFg->id)->first();
                }

                if ($field->name == 'jugadors') {
                    $jugadorsFg = $field;

                    $jugadorNomF = CustomField::where('name', 'nom')->where('parent_id', $jugadorsFg->id)->first();
                    $jugadorCognomF = CustomField::where('name', 'cognom')->where('parent_id', $jugadorsFg->id)->first();
                    $jugadorLlicenciaF = CustomField::where('name', 'llicencia')->where('parent_id', $jugadorsFg->id)->first();
                    $jugadorPreu = CustomField::where('name', 'preu')->where('parent_id', $jugadorsFg->id)->first();
                    $jugadorType = CustomField::where('name', 'type')->where('parent_id', $jugadorsFg->id)->first();
                }

                if (array_key_exists($field->name, $data)) {
                    $postMeta = PostMeta::firstOrNew([
                        'custom_field_id' => $field->id,
                        'post_id' => $reserve->id,
                        'locale' => app()->getLocale()
                    ]);

                    $postMeta->value = $data[$field->name];
                    $postMeta->save();
                }
            }
        }

        if ($extras && $extrasFg) {
            PostMeta::firstOrCreate([
                'custom_field_id' => $extrasFg->id,
                'post_id' => $reserve->id,
                'locale' => app()->getLocale()
            ]);

            PostMeta::whereIn('custom_field_id', [$extraF->id, $quantitatExtraF->id])
                ->where('post_id', $reserve->id)
                ->where('parent_id', $extrasFg->id)
                ->where('locale', app()->getLocale())
                ->delete();

            $order = 0;
            foreach ($extras as $id => $quantity) {
                PostMeta::create([
                    'custom_field_id' => $extraF->id,
                    'post_id' => $reserve->id,
                    'value' => $id,
                    'parent_id' => $extrasFg->id,
                    'order' => $order,
                    'locale' => app()->getLocale()
                ]);

                PostMeta::create([
                    'custom_field_id' => $quantitatExtraF->id,
                    'post_id' => $reserve->id,
                    'value' => $quantity,
                    'parent_id' => $extrasFg->id,
                    'order' => $order,
                    'locale' => app()->getLocale()
                ]);

                $order++;
            }
        }

        if ($players && $jugadorsFg) {
            PostMeta::firstOrCreate([
                'custom_field_id' => $jugadorsFg->id,
                'post_id' => $reserve->id,
                'locale' => app()->getLocale()
            ]);

            PostMeta::whereIn('custom_field_id', [$jugadorNomF->id, $jugadorCognomF->id, $jugadorLlicenciaF->id])
                ->where('post_id', $reserve->id)
                ->where('parent_id', $jugadorsFg->id)
                ->where('locale', app()->getLocale())
                ->delete();

            foreach ($players as $order => $item) {
                PostMeta::create([
                    'custom_field_id' => $jugadorNomF->id,
                    'post_id' => $reserve->id,
                    'value' => isset($item['nom']) ? $item['nom'] : '',
                    'parent_id' => $jugadorsFg->id,
                    'order' => $order,
                    'locale' => app()->getLocale()
                ]);

                PostMeta::create([
                    'custom_field_id' => $jugadorCognomF->id,
                    'post_id' => $reserve->id,
                    'value' => isset($item['cognom']) ? $item['cognom'] : '',
                    'parent_id' => $jugadorsFg->id,
                    'order' => $order,
                    'locale' => app()->getLocale()
                ]);

                PostMeta::create([
                    'custom_field_id' => $jugadorLlicenciaF->id,
                    'post_id' => $reserve->id,
                    'value' => isset($item['llicencia']) ? $item['llicencia'] : '',
                    'parent_id' => $jugadorsFg->id,
                    'order' => $order,
                    'locale' => app()->getLocale()
                ]);

                PostMeta::create([
                    'custom_field_id' => $jugadorPreu->id,
                    'post_id' => $reserve->id,
                    'value' => isset($item['preu']) ? $item['preu'] : $normalPrice,
                    'parent_id' => $jugadorsFg->id,
                    'order' => $order,
                    'locale' => app()->getLocale()
                ]);

                PostMeta::create([
                    'custom_field_id' => $jugadorType->id,
                    'post_id' => $reserve->id,
                    'value' => isset($item['type']) ? $item['type'] : $normalType,
                    'parent_id' => $jugadorsFg->id,
                    'order' => $order,
                    'locale' => app()->getLocale()
                ]);
            }
        }
    }


    public function playerPrice()
    {
        $license = request('license');

        $normalUserCategory = Post::select(DB::raw('posts.*'))
            ->join('post_translations', 'post_translations.post_id', '=', 'posts.id')
            ->where('posts.type', 'categoria-socio')
            ->where('post_translations.locale', app()->getLocale())
            ->where('post_translations.post_name', 'publico')
            ->first();

        if (!$normalUserCategory) {
            abort(500);
        }

        $userPlayer = $this->getPlayerByLicense($license);

        $playerPreu = $normalUserCategory->get_field('precio-salida');
        $playerType = $normalUserCategory->id;

        if ($userPlayer) {
            $playerPreu = $userPlayer->get_field('categoria') ? $userPlayer->get_field('categoria')->get_field('precio-salida') : $playerPreu;
            $playerType = $userPlayer->get_field('categoria') ? $userPlayer->get_field('categoria')->id : $playerType;
        }

        return response()->json(['price' => $playerPreu, 'type' => $playerType]);
    }


    public function availability()
    {
        $date = request('date');
        $hour = request('hour');
        $reserveId = request('reserve_id');

        if (!$hour || !$date) {
            abort(400);
        }
        $date = Carbon::createFromFormat('d/m/Y', $date);
        $hour .= ':00';

        $availability = $this->checkGreenFeeAvailability($date, $hour, $reserveId);

        if (!$availability) {
            return response()->json(['available' => 0]);
        }

        return response()->json([
            'available' => 1,
            'prices' => json_encode($availability)
        ]);
    }


    public function allHoursAvailability()
    {
        $date = request('date');
        $reserveId = request('reserve_id');

        if (!$date) {
            abort(400);
        }

        $date = Carbon::createFromFormat('d/m/Y', $date);

        $departureHours = app(ReservesService::class)->getHorasSalida('green-fee');

        $return = [];

        $date->startOfDay();

        $purchasesQ = Purchase::select(DB::raw('distinct purchases.*'))
            ->with(['post'])
            ->join('posts', 'posts.id', '=', 'purchases.post_id')
            ->join('post_meta', 'post_meta.post_id', '=', 'posts.id')
            ->join('custom_fields', 'custom_fields.id', '=', 'post_meta.custom_field_id')
            ->where('custom_fields.name', 'data')
            ->where('post_meta.value', $date->format('d/m/Y'))
            ->where('posts.type', 'reserva-green-fee');

        if ($reserveId) {
            $purchasesQ = $purchasesQ->where('purchases.post_id', '<>', $reserveId);
        }

        $purchases = $purchasesQ->get();

        $day = GreenFeeCalendarDay::where('date', $date)->with('hours.tariff.tariffCategories', 'hours.prices.category', 'hours.prices.tariff.tariffCategories')->first();

        $categories = Post::ofType('categoria-socio')
            ->ofStatus('publish')
            ->withTranslation()
            ->get();

        foreach ($departureHours as $departureHour) {
            $hour = $departureHour->get_field('hora');

            $availability = $this->checkGreenFeeAvailability($date, $hour . ':00', $reserveId, $purchases, $day, $categories);
            $return[] = [
                'hour' => $hour,
                'available' => $availability ? 1 : 0
            ];
        }
        return response()->json(['availabilities' => $return]);
    }


    private function getType($locale, $id = null)
    {
        $path = request()->path();
        $type = str_replace($locale . '/', '', $path);
        $type = str_replace('/' . $id, '', $type);

        return trim($type, '/');
    }


    private function getPlayerByLicense($license, $cfLlicenciaId = null)
    {
        if (!$cfLlicenciaId) {
            $cfLlicenciaId = CustomField::select(DB::raw('custom_fields.*'))
                ->join('custom_field_groups', 'custom_field_groups.id', '=', 'custom_fields.cfg_id')
                ->where('custom_fields.name', 'llicencia')
                ->where('custom_field_groups.post_type', 'user')
                ->where('custom_field_groups.title', 'Dades Aravell')
                ->first()->id;
        }

        return User::select(DB::raw('users.*'))
            ->join('user_meta', 'user_meta.user_id', '=', 'users.id')
            ->where('user_meta.custom_field_id', $cfLlicenciaId)
            ->where('user_meta.value', $license)
            ->first();
    }


    private function checkGreenFeeAvailability(Carbon $date, $hour, $reserveId = null, $purchases = null, $day = null, $categories = null)
    {
        $date->startOfDay();

        if (!$purchases) {
            $purchasesQ = Purchase::select(DB::raw('distinct purchases.*'))
                ->with(['post'])
                ->join('posts', 'posts.id', '=', 'purchases.post_id')
                ->join('post_meta', 'post_meta.post_id', '=', 'posts.id')
                ->join('custom_fields', 'custom_fields.id', '=', 'post_meta.custom_field_id')
                ->where('custom_fields.name', 'data')
                ->where('post_meta.value', $date->format('d/m/Y'))
                ->where('posts.type', 'reserva-green-fee');

            if ($reserveId) {
                $purchasesQ = $purchasesQ->where('purchases.post_id', '<>', $reserveId);
            }

            $purchases = $purchasesQ->get();
        }

        foreach ($purchases as $purchase) {
            if ($purchase->post->get_field('hora-sortida') == str_replace_first(':00', '', $hour)) {
                if ($purchase->status == Purchase::PURCHASE_STATUS_PAYED) {

                    return false;
                }

                // Time for someone buying (trying to avoid race conditions..)
                if (Carbon::now()->subMinutes(15) <= $purchase->updated_at) {
                    return false;
                }
            }
        }

        if (!$day) {
            $day = GreenFeeCalendarDay::where('date', $date)->with('hours.tariff.tariffCategories', 'hours.prices.category', 'hours.prices.tariff.tariffCategories')->first();
        }

        if (!$day || !$day->is_open) {
            return false;
        }

        $dayHour = $day->hours->filter(function ($dayHour) use ($hour) {
            return Carbon::createFromFormat('H:i:s', $dayHour->hour) <= Carbon::createFromFormat('H:i:s', $hour);
        })->filter(function ($dayHour) use ($hour) {
            return Carbon::createFromFormat('H:i:s', $dayHour->hour)->addHour() > Carbon::createFromFormat('H:i:s', $hour);
        })->first();

        if (!$dayHour || !$dayHour->is_open) {
            return false;
        }

        $prices = collect();

        foreach ($dayHour->prices as $price) {
            $tariffCategory = $price->tariff->tariffCategories->where('category_id', $price->category->id)->first();

            $prices->push(json_decode(json_encode([
                'category_name' => $price->category->post_name,
                'price' => $tariffCategory ? $tariffCategory->price : 0,
                'post_id' => $price->category->id
            ]), false));
        }

        if (!$categories) {
            $categories = Post::ofType('categoria-socio')
                ->ofStatus('publish')
                ->withTranslation()
                ->get();
        }

        foreach ($categories as $category) {
            if (!$prices->where('category_name', $category->post_name)->first()) {
                $auxDayHour = $dayHour;
                $tariffCategory = $auxDayHour->tariff->tariffCategories->where('category_id', $category->id)->first();

                $prices->push(json_decode(json_encode([
                    'category_name' => $category->post_name,
                    'price' => $tariffCategory ? $tariffCategory->price : 0,
                    'post_id' => $category->id
                ]), false));
            }
        }

        return $prices;
    }
}