<?php


namespace App\Modules\Aravell\Repositories;


use App\Modules\Aravell\Models\Purchase;
use Illuminate\Support\Facades\DB;

class PurchasesRepository
{


    public function getPurchases($search = null, $itemsPerPage = 50)
    {
        $query = Purchase::whereNotNull('post_id')
            ->with(['post.metas.customField', 'user', 'post.customPostTranslations'])
            ->orderBy('purchases.id', 'desc');

        if ($search) {
            $query = $query->select(DB::raw('distinct purchases.*'))
                ->join('users', 'users.id', '=', 'purchases.user_id')
                ->join('posts', 'posts.id', '=', 'purchases.post_id')
                ->join('custom_post_translations', 'custom_post_translations.post_type', '=', 'posts.type')
                ->where('custom_post_translations.locale', app()->getLocale())
                ->where(function ($query) use ($search) {
                    $query->where('purchases.total_amount', 'like', '%' . $search . '%')
                        ->orWhere('purchases.order_id', 'like', '%' . $search . '%')
                        ->orWhere('purchases.comments', 'like', '%' . $search . '%')
                        ->orWhereRaw('CONCAT(users.name, " ", users.lastname) like "%?%"', [$search])
                        ->orWhere('custom_post_translations.title', 'like', '%' . $search . '%');
                });
        }

        return $query->paginate($itemsPerPage);
    }
}