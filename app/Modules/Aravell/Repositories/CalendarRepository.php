<?php


namespace App\Modules\Aravell\Repositories;


use App\Modules\Aravell\Models\GreenFeeCalendarDay;
use Carbon\Carbon;

class CalendarRepository
{

    public function getCalendarBetweenDates(Carbon $startDate, Carbon $endDate)
    {
        return GreenFeeCalendarDay::whereBetween('date', [$startDate->startOfDay(), $endDate->endOfDay()])
            ->with(['hours.prices.category'])
            ->get();
    }
}