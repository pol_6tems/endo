<?php


namespace App\Modules\Aravell\Models;


use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    public function tariffCategories()
    {
        return $this->hasMany(TariffsCategory::class);
    }
}