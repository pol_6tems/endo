<?php


namespace App\Modules\Aravell\Models;


use Illuminate\Database\Eloquent\Model;

class GreenFeeCalendarHour extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    public function day()
    {
        return $this->belongsTo(GreenFeeCalendarDay::class);
    }

    public function tariff()
    {
        return $this->belongsTo(Tariff::class);
    }


    public function prices()
    {
        return $this->hasMany(GreenFeeCalendarPrice::class);
    }
}