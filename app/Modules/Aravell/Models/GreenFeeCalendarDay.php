<?php


namespace App\Modules\Aravell\Models;


use Illuminate\Database\Eloquent\Model;

class GreenFeeCalendarDay extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dates = ['date'];


    public function hours()
    {
        return $this->hasMany(GreenFeeCalendarHour::class);
    }
}