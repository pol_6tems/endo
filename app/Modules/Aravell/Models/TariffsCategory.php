<?php


namespace App\Modules\Aravell\Models;


use App\Post;
use Illuminate\Database\Eloquent\Model;

class TariffsCategory extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    public function tariff()
    {
        return $this->belongsTo(Tariff::class);
    }


    public function category()
    {
        return $this->belongsTo(Post::class, 'category_id');
    }


    public function getPriceAttribute($value)
    {
        return $value / 100;
    }


    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }
}