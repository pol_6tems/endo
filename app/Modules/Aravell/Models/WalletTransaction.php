<?php

namespace App\Modules\Aravell\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WalletTransaction extends Model
{
    protected $guarded = [];

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date);
    }


    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at', 'desc')->get();
    }


    public function getAmountAttribute($value)
    {
        return $value / 100;
    }


    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = $value * 100;
    }
}
