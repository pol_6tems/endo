<?php


namespace App\Modules\Aravell\Models;


use App\Post;
use Illuminate\Database\Eloquent\Model;

/**
 * Calendar Tariff exceptions
 * Class GreenFeeCalendarPrice
 * @package App\Modules\Aravell\Models
 */
class GreenFeeCalendarPrice extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    public function hour()
    {
        return $this->belongsTo(GreenFeeCalendarHour::class);
    }


    public function category()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }


    public function tariff()
    {
        return $this->belongsTo(Tariff::class);
    }
}