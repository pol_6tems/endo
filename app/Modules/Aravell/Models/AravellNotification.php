<?php


namespace App\Modules\Aravell\Models;


use App\Post;
use App\User;
use Illuminate\Database\Eloquent\Model;

class AravellNotification extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dates = ['until'];


    public function post()
    {
        return $this->belongsTo(Post::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}