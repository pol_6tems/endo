<?php

namespace App\Modules\Aravell\Models;

use App\Post;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;

class Purchase extends Model
{
    use SoftDeletes;

    const PURCHASE_STATUS_PENDING = 0;
    const PURCHASE_STATUS_PAYED = 1;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dates = ['deleted_at'];


    public function post()
    {
        return $this->belongsTo(Post::class)->withTrashed();
    }
    
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /*public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }*/
    
    
    public function getCreatedAtAttribute($value)
    {
        return Date::parse($value);
    }


    public function getBaseAmountAttribute($value)
    {
        return $value / 100;
    }


    public function setBaseAmountAttribute($value)
    {
        $this->attributes['base_amount'] = $value * 100;
    }


    public function getCouponDiscountAmountAttribute($value)
    {
        return $value / 100;
    }


    public function setCouponDiscountAmountAttribute($value)
    {
        $this->attributes['coupon_discount_amount'] = $value * 100;
    }


    public function getTotalAmountAttribute($value)
    {
        return $value / 100;
    }


    public function setTotalAmountAttribute($value)
    {
        $this->attributes['total_amount'] = $value * 100;
    }
}
