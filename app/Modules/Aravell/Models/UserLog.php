<?php


namespace App\Modules\Aravell\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date);
    }
}