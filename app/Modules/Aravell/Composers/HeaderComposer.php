<?php


namespace App\Modules\Aravell\Composers;


use App\Modules\ArrayPlastics\Repositories\GroupsRepository;
use Illuminate\View\View;

class HeaderComposer
{

    public function compose(View $view)
    {
        $user = auth()->user();

        $userCategory = $user->get_field('categoria');

        // 2 == public category
        $view->with([
            'isPartner' => $userCategory && $userCategory->id != 2,
            'user' => $user
        ]);
    }
}