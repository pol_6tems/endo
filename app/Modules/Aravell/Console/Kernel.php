<?php
/**
 * Created by PhpStorm.
 * User: sergisolsona
 * Date: 12/06/2019
 * Time: 11:58
 */

namespace App\Modules\Aravell\Console;


use App\Modules\Aravell\Console\Commands\CreateDepartureHours;
use App\Modules\Aravell\Console\Commands\ImportSocis;
use App\Modules\Aravell\Console\Commands\SendReserveNotifications;
use Illuminate\Console\Scheduling\Schedule;

class Kernel
{

    /**
     * The Artisan commands provided by your module.
     *
     * @var array
     */
    protected $commands = [
        SendReserveNotifications::class,
        CreateDepartureHours::class,
        ImportSocis::class
    ];


    public function getCommands()
    {
        return $this->commands;
    }


    /**
     * Define the module's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule)
    {
        $schedule->command('send:reserve-notifications')->everyThirtyMinutes();
    }
}