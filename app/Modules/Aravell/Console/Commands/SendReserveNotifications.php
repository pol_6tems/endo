<?php

namespace App\Modules\Aravell\Console\Commands;

use App\Language;
use App\Models\Mensaje;
use App\Modules\Aravell\Models\AravellNotification;
use App\Notifications\EmailNotification;
use App\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use stdClass;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class SendReserveNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:reserve-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reserve notifications one day before';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();

        $reserves = Post::select(DB::raw('distinct posts.*'))
            ->join('post_meta', 'post_meta.post_id', '=', 'posts.id')
            ->whereIn('type', ['reserva-green-fee', 'reserva-restaurant'])
            ->where('status', 'publish')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('aravell_notifications')
                    ->whereRaw('aravell_notifications.post_id = posts.id');
            })
            ->get();

        $languages = Language::where('active', true)->get();

        $from = User::where('email', 'organitzador@aravellgolfclub.com')->first();

        $twilioSid = env("TWILIO_SID");
        $twilioToken = env("TWILIO_TOKEN");

        if ($twilioSid && $twilioToken) {
            $twilio = new Client($twilioSid, $twilioToken);
        }

        foreach ($reserves as $reserve) {
            $langCode = null;
            $hour = null;

            foreach ($languages as $language) {
                $hour = $reserve->get_field('hora-sortida', $language->code);

                if ($reserve->type == 'reserva-restaurant') {
                    $hour = $reserve->get_field('hora', $language->code);
                }

                if ($hour) {
                    $langCode = $language->code;
                    break;
                }
            }

            $date = Carbon::createFromFormat('d/m/Y H:i', $reserve->get_field('data', $langCode) . ' ' . $hour);

            if (!$hour || ($reserve->type == 'reserva-restaurant' && $reserve->get_field('estado', $langCode) != 'valida') ||
                $now > $date || $now->diffInHours($date) > 24) {
                continue;
            }

            $title = 'Green Fee';
            $imageUrl = 'noti-icon-1.svg';
            $smallImageUrl = 'histo-ico-2.svg';

            if ($reserve->type == 'reserva-restaurant') {
                $title = 'Restaurant';
                $imageUrl = 'noti-icon-2.svg';
                $smallImageUrl = 'histo-ico-1.svg';
            }

            AravellNotification::create([
                'post_id' => $reserve->id,
                'user_id' => $reserve->author_id,
                'until' => $date,
                'title' => $title,
                'subtitle' => 'Reserva para :date a las :hour',
                'route_name' => str_replace('reserva-', '', $reserve->type) . '.show',
                'image_url' => $imageUrl,
                'small_image_url' => $smallImageUrl
            ]);

            $user = $reserve->author;

            if (isProEnv()) {
                $mensaje = new Mensaje();
                $mensaje->user_id = 0;
                $mensaje->mensaje = '';
                $mensaje->fitxer = '';
                $mensaje->email = '';
                $mensaje->name = '';
                $mensaje->type = 'email';
                $mensaje->params = json_encode([
                ]);

                $params = new stdClass();
                $params->usuario = $user;
                $params->mensaje = $mensaje;
                $params->from = $from;
                $params->email_id = 2;

                $notificacion = new EmailNotification($params);
                if ($notificacion->check_email()) {
                    $user->notify($notificacion);
                }
            }

            $phone = $user->get_field('telefon-1');

            if (!$phone) {
                $phone = $user->get_field('telefon-2');
            }

            if ($phone && $twilioSid && $twilioToken) {
                $this->info('Sending Whatsapp..');

                try {
                    $message = $twilio->messages
                        ->create("whatsapp:+34" . $phone, // to
                            [
                                "from" => "whatsapp:+14155238886",
                                "body" => __('Te recordamos que mañana tienes una reserva'),
                            ]
                        );
                } catch (TwilioException $e) {
                    dd($e->getMessage());
                }
            }

            sleep(1);
        }

        $this->info('Done!');
    }
}
