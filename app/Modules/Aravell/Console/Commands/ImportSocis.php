<?php

namespace App\Modules\Aravell\Console\Commands;

use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportSocis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:socis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Socis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $socis = DB::table('import_socis')
            ->select('*')
            ->get();

        foreach ($socis as $soci) {
            $email = $soci->EMAIL;

            if (!$email) {
                $this->warn('Soci without email. Continue..');
                continue;
            }

            $fullnameArray = explode(',', $soci->NOMBRE);

            $name = isset($fullnameArray[1]) ? trim($fullnameArray[1]) : trim($fullnameArray[0]);
            $lastname = isset($fullnameArray[1]) ? trim($fullnameArray[0]) : '';

            $user = User::where('email', $email)->first();

            if (!$user) {
                $this->info('Creating ' . $email);

                $llicencia = $soci->LICENCIA;
                $birthDate = Carbon::createFromFormat('d/m/Y', $soci->FNACIMIENTO);
                $phone1 = $soci->TELEFONO1;
                $phone2 = $soci->TELEFONO2;
                $address = $soci->DIRECCION1;
                $city = $soci->POBLACION1;
                $country = $soci->PAIS1;
                $handicap = floatval(str_replace(',', '.', $soci->HEX));

                $user = User::create([
                    'name' => $name,
                    'lastname' => $lastname,
                    'email' => $email,
                    'role' => 'Soci',
                    'password' => bcrypt(str_random(10))
                ]);

                $user->set_field([
                    'categoria' => 46, // SOCIO
                    'llicencia' => $llicencia,
                    'data-naixement' => $birthDate->format('d/m/Y'),
                    'telefon-1' => $phone1,
                    'telefon-2' => $phone2,
                    'poblacio' => $city,
                    'adreca' => $address,
                    'pais' => $country,
                    'handicap' => $handicap
                ], null);
            } else {
                $this->info('User ' . $email . ' already exists');
            }
        }

        $this->info('Done!');
    }
}
