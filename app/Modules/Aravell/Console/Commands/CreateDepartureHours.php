<?php

namespace App\Modules\Aravell\Console\Commands;

use App\Models\CustomField;
use App\Models\PostMeta;
use App\Post;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateDepartureHours extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:departure-hours';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Green Fee departure hours (every 10 minutes between 8h and 20h)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = Carbon::today();
        $startTime->hour = 8;

        $endTime = Carbon::today();
        $endTime->hour = 20;

        $cfHoraSortida = CustomField::select(DB::raw('custom_fields.*'))
            ->join('custom_field_groups', 'custom_field_groups.id', '=', 'custom_fields.cfg_id')
            ->where('custom_field_groups.post_type', 'hora-sortida')
            ->first();

        while ($startTime < $endTime) {
            $oldDepartureHour = Post::select(DB::raw('posts.*'))
                ->join('post_meta', 'post_meta.post_id', '=', 'posts.id')
                ->withTrashed()
                ->where('posts.type', 'hora-sortida')
                ->where('post_meta.custom_field_id', $cfHoraSortida->id)
                ->where('post_meta.value', $startTime->format('H:i'))
                ->first();

            if (!$oldDepartureHour) {
                $departure = Post::create([
                    'type' => 'hora-sortida',
                    'status' => 'publish',
                    'parent_id' => 0,
                    'author_id' => 1,
                    'pending_id' => 0,
                    'es' => [
                        'title' => $startTime->format('H:i'),
                        'description' => '',
                        'post_name' => $startTime->format('H:i')
                    ]
                ]);

                PostMeta::create([
                    'post_id' => $departure->id,
                    'custom_field_id' => $cfHoraSortida->id,
                    'value' => $startTime->format('H:i'),
                    'locale' => 'es'
                ]);

                $this->info('Created ' . $startTime->format('H:i'));
            } elseif ($oldDepartureHour->status != 'publish') {
                $oldDepartureHour->status = 'publish';
                $oldDepartureHour->deleted_at = null;

                $oldDepartureHour->save();

                $this->info('Updated ' . $startTime->format('H:i'));
            }

            $startTime->addMinutes(10);
        }
    }
}
