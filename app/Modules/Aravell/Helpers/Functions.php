<?php

use Carbon\Carbon;

if (! function_exists('print_chat_date')) {

    /**
     *
     * @param Carbon $date
     * @return String
     */
    function print_chat_date(Carbon $date) {
        $date->startOfDay();

        if (Carbon::today() == $date) {
            return __('Hoy');
        }

        if (Carbon::yesterday() == $date) {
            return __('Ayer');
        }

        return $date->format('d/m/Y');
    }
}