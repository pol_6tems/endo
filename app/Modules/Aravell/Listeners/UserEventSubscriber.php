<?php

namespace App\Modules\Aravell\Listeners;

use App\Modules\Aravell\Events\WalletTransaction;
use App\Modules\Aravell\Models\AravellNotification;
use App\Modules\Aravell\Models\UserLog;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;

class UserEventSubscriber
{
    /**
     * Handle user login events.
     * @param Login $event
     */
    public function onUserLogin(Login $event)
    {
        UserLog::create([
            'user_id' => $event->user->id,
            'action' => 'login'
        ]);
    }

    /**
     * Handle user logout events.
     * @param Logout $event
     */
    public function onUserLogout(Logout $event)
    {
        UserLog::create([
            'user_id' => $event->user->id,
            'action' => 'logout'
        ]);
    }


    public function onWalletTransaction(WalletTransaction $transaction)
    {
        \App\Modules\Aravell\Models\WalletTransaction::create([
            'user_id' => $transaction->user->id,
            'amount' => $transaction->amount,
            'comments' => $transaction->comments
        ]);

        if ($transaction->amount < 0 && $transaction->user->get_field('saldo') < 0) {
            AravellNotification::create([
                'user_id' => $transaction->user->id,
                'title' => 'Saldo negativo en Wallet',
                'subtitle' => 'Recuerde recargar su wallet',
                'route_name' => 'user.wallet',
                'image_url' => 'left-menu-icon-wallet.svg',
                'small_image_url' => 'home-icon-wallet.svg'
            ]);
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            self::class . '@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            self::class . '@onUserLogout'
        );

        $events->listen(
            WalletTransaction::class,
            self::class . '@onWalletTransaction'
        );
    }

}