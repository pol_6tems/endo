<?php


namespace App\Modules\Aravell\Providers;


use App\Modules\Aravell\Observers\UserObserver;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
    }


    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {

    }
}