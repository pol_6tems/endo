<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGreenFeeCalendarPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('green_fee_calendar_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('green_fee_calendar_hour_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->integer('tariff_id')->unsigned();
            $table->timestamps();

            $table->unique(['green_fee_calendar_hour_id', 'post_id'], 'green_fee_calendar_hour_price_unique');

            $table->foreign('green_fee_calendar_hour_id')->references('id')->on('green_fee_calendar_hours');
            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('tariff_id')->references('id')->on('tariffs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('green_fee_calendar_prices');
    }
}
