<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGreenFeeCalendarHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('green_fee_calendar_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('green_fee_calendar_day_id')->unsigned();
            $table->time('hour');
            $table->boolean('is_open')->default(0);
            $table->integer('tariff_id')->unsigned()->nullable();
            $table->timestamps();

            $table->unique(['green_fee_calendar_day_id', 'hour'], 'green_fee_calendar_day_hours_unique');

            $table->foreign('green_fee_calendar_day_id')->references('id')->on('green_fee_calendar_days');
            $table->foreign('tariff_id')->references('id')->on('tariffs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('green_fee_calendar_hours');
    }
}
