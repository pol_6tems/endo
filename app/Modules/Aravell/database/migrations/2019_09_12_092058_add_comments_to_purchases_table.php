<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentsToPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->string('comments')->default('')->after('total_amount');
            if ($this->foreignExists('purchases', 'purchases_post_id_foreign')) {
                $table->dropForeign('purchases_post_id_foreign');
            }

            if ($this->foreignExists('purchases', 'purchases_post_id_unique')) {
                $table->dropUnique('purchases_post_id_unique');
            }

            $table->foreign('post_id')->references('id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->dropColumn('comments');
        });
    }


    private function foreignExists($tableName, $key)
    {
        return DB::select(
            DB::raw(
                "SHOW KEYS
        FROM $tableName
        WHERE Key_name='$key'"
            )
        );
    }
}
