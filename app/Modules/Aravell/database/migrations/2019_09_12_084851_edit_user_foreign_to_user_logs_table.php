<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUserForeignToUserLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_logs', function (Blueprint $table) {
            if ($this->foreignExists('user_logs', 'user_logs_user_id_foreign')) {
                $table->dropForeign('user_logs_user_id_foreign');

                if ($this->foreignExists('user_logs', 'user_logs_user_id_foreign')) {
                    $table->dropIndex('user_logs_user_id_foreign');
                }
            }

            $table->integer('user_id')->unsigned()->nullable()->change();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Do nothing
    }


    private function foreignExists($tableName, $key)
    {
        return DB::select(
            DB::raw(
                "SHOW KEYS
        FROM $tableName
        WHERE Key_name='$key'"
            )
        );
    }
}
