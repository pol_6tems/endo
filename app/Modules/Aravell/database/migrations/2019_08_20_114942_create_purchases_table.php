<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('status')->default(0);
            $table->string('order_id', 16)->unique();
            $table->string('external_order_id')->unique()->nullable();
            $table->integer('post_id')->unsigned()->unique();
            $table->integer('coupon_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('base_amount')->nullable();
            $table->integer('coupon_discount_amount')->nullable();
            $table->integer('total_amount')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('coupon_id');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('coupon_id')->references('id')->on('coupons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
