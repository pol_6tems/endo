<?php

Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    Route::resource('comments', "AdminCommentsController", ['as' => 'admin']);
});

Route::resource('comments', "CommentsController");