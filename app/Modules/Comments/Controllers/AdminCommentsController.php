<?php

namespace App\Modules\Comments\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\Comments\Models\Comment;
use App\User;
use App\Post;

class AdminCommentsController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Comments');
        $this->section_icon = 'chat_bubble';
        $this->section_route = 'admin.comments';
    }

    protected function get_form_fields() {

        $users = User::get();
        $posts = Post::where('status', 'publish')->withTranslation()->get();
        
        return array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'post_id', 'title' => __('Post'), 'type' => 'select', 'required' => true,
                    'datasource' => $posts,
                    'sel_value' => 'id',
                    'sel_title' => ['title'],
                    'sel_search' => true,
                    'with_post_type' => true,
                ],
                ['value' => 'user_id', 'title' => __('User'), 'type' => 'select', 'required' => true,
                    'datasource' => $users,
                    'sel_value' => 'id',
                    'sel_title' => ['fullname()'],
                    'sel_search' => true,
                    'with_post_type' => false,
                ],
                ['value' => 'comment', 'title' => __('Comment'), 'type' => 'textarea', 'rows' => 10],
            ], 
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = Comment::withAll()->get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'has_duplicate' => false,
            'headers' => [
                __('#') => [ 'width' => '5%' ],
                __('User') => [ 'width' => '15%' ],
                __('Post') => [ 'width' => '20%' ],
                __('Comment') => [],
                __('Updated at') => [ 'width' => '15%' ],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => ['user', 'fullname()']],
                ['value' => ['post', 'title'] ],
                ['value' => 'comment', 'class' => 'textarea'],
                ['value' => 'updated_at', 'type' => 'date'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {        
        return View('Admin::default.create', $this->get_form_fields());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();
        Comment::create($data);
        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name added successfully', ['name' => __('Comment')]) ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id) {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id) {
        $item = Comment::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');

        return View('Admin::default.edit', array_merge(
            ['item' => $item], 
            $this->get_form_fields()
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id) {
        $item = Comment::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');

        $data = $request->all();
        $item->update($data);
        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name updated successfully', ['name' => __('Comment')]) ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id) {
        $item = Comment::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');
        
        $item->delete();
        
        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name deleted successfully', ['name' => __('Comment')]) ]);
    }

}
