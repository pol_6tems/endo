<?php

namespace App\Modules\Comments\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Comments\Models\Comment;
use App\Post;
use Auth;

class CommentsController extends Controller
{

    public static function show_comments($post_id) {
        $comments = self::get_comments($post_id);
        $post = Post::find($post_id);
        
        return [
            'view' => 'comments.single-comments',
            'params' => [
                'post_id' => $post_id,
                'post_url' => $post->get_url(),
                'comments' => $comments,
            ]
        ];
    }

    public static function get_comments($post_id) {
        $comments = Comment::where('post_id', $post_id)->withAll()->orderby('updated_at', 'ASC')->get();

        return $comments;
    }

    public function store(Request $request) {
        $data = request()->all();
        
        $data['user_id'] = Auth::user()->id;
        
        if ( !empty($data['comment']) && !empty($data['post_id']) ) {
            Comment::create($data);
        }

        return !empty($data['redirect_to']) ? redirect($data['redirect_to']) : abort(404);
    }

}
