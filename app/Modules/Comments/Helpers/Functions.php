<?php

use App\Modules\Comments\Controllers\CommentsController;

function show_comments($post_id) {
    return CommentsController::show_comments($post_id);
}

function get_comments($post_id) {
    return CommentsController::get_comments($post_id);
}