<?php

namespace App\Modules\Comments;

use Artisan;
use Exception;
use App\Models\Admin\AdminMenu;

class CommentsModule {
    
    const MODULE_NAME = 'Comments';
    const MODULE_ROUTE_ADMIN = 'admin.comments';
    const MODULE_ROUTE = 'comments';

    public static function get_path() {
        return 'Modules\\' . self::MODULE_NAME . '\\';
    }

    // INSTALL
    public static function install() {
        try {
            Artisan::call('migrate', array(
                '--path' => 'app/Modules/' . self::MODULE_NAME . '/migrations', 
                '--force' => true,
            ));

            self::make_menu();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // DESTROY
    public static function reset() {
        try {
            Artisan::call('migrate:reset', array(
                '--path' => 'app/Modules/' . self::MODULE_NAME . '/migrations', 
            ));
            
            self::remove_menu();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // RESET
    public static function refresh() {
        try {
            Artisan::call('migrate:refresh', array(
                '--path' => 'app/Modules/' . self::MODULE_NAME . '/migrations', 
            ));

            self::remove_menu();

            self::make_menu();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function make_menu() {
        if ( !AdminMenu::where('url', self::MODULE_ROUTE_ADMIN . '.index')->exists() ) {
            AdminMenu::create([
                'name' => 'Comments',
                'icon' => 'chat_bubble',
                'has_url' => true,
                'url_type' => 'route',
                'url' => self::MODULE_ROUTE_ADMIN . '.index',
                'controller' => self::get_path() . 'Controllers\\Admin\\Admin' . self::MODULE_NAME . 'Controller',
                'order' => 800,
                'parameters' => NULL,
            ]);
        }
    }

    public static function remove_menu() {
        return AdminMenu::where('url', self::MODULE_ROUTE_ADMIN . '.index')->delete();
    }

}