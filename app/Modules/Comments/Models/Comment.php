<?php

namespace App\Modules\Comments\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Post;

class Comment extends Model {

    protected $fillable = ['post_id', 'user_id', 'comment', 'created_at'];

    public function scopeWithAll($query) {
        $query->with('user', 'post');
    }

    public function post() {
        return $this->belongsTo(Post::class, 'post_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
