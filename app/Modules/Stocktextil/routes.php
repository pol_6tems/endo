<?php

Route::group(['middleware' => ['auth']], function () {
    Route::get('/{index?}', "HomeController@index")->name('index');
    Route::post('/{index?}', "HomeController@contact");

    Route::get('/{page}', "\App\Http\Controllers\PostsController@process_routes")->name('page.user');
    Route::post('/{page}', "UserController@update")->name('page.user');
});

Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    Route::resource('posts', "Admin\MyPostsController", ["as" => "admin"]);
});