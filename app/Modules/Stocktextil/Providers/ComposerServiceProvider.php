<?php


namespace App\Modules\Stocktextil\Providers;


use App\Modules\Stocktextil\Composers\HeaderComposer;
use App\Theme;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('Front::partials.header', HeaderComposer::class);
    }


    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {

    }
}