<?php


namespace App\Modules\Stocktextil\Controllers;


use App\Http\Controllers\Controller;
use App\Models\CustomFieldGroup;
use App\Models\UserMeta;
use App\Modules\Stocktextil\Requests\UserRequest;

class UserController extends Controller
{
    protected $section_type = 'user';
    
    public function update(UserRequest $request)
    {
        $user = auth()->user();

        $update = [
            'name' => $request->name,
            'lastname' => $request->lastname
        ];

        if (!empty($request->password)) {
            if (strlen($request->password) < 6) {
                return redirect()->back()->withErrors(['password' => trans('validation.min.string', ['attribute' => 'password', 'min' => 6])]);
            }

            if ($request->password != $request->password_confirmation) {
                return redirect()->back()->withErrors(['password' => trans('validation.confirmed', ['attribute' => 'password'])]);
            }

            $update['password'] = bcrypt($request->password);
        }

        $user->update($update);

        $cf_groups = CustomFieldGroup::where('post_type', $this->section_type)->orderBy('order')->get();

        $data = $request->all();

        foreach ($cf_groups as $cf_group) {
            foreach ($cf_group->fields as $field) {
                if (array_key_exists($field->name, $request->all())) {
                    $userMeta = UserMeta::firstOrNew([
                        'user_id' => $user->id,
                        'custom_field_id' => $field->id,
                        'post_id' => $this->section_type
                    ]);

                    $userMeta->value = $data[$field->name];
                    $userMeta->save();
                }
            }
        }

        $locale = $request->get('locale', app()->getLocale());

        return redirect()->route('index', ['locale' => $locale])->with('message', 'Updated');
    }
}