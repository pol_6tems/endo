<?php


namespace App\Modules\Stocktextil\Controllers\Admin;


use App\Http\Controllers\Admin\PostsController;
use App\Models\Configuracion;
use App\Post;

class MyPostsController extends PostsController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parent = (request()->parent) ? request()->parent : null;
        $status = (request()->status) ? request()->status : $this->def_status;

        $home_page = Configuracion::get_config('home_page');
        $home_page_id = !empty($home_page) ? $home_page->id : 0;

        $builder = \App\Modules\ArrayPlastics\Models\Post::ofType($this->post_type)
            ->ofStatus($status)
            ->withTranslation();

        if ( !$status ) {
            // Revisar
            // Todo: Revisar-ne l'us
            if ( $parent ) {
                $parent_id = $parent;
                $parent = Post::ofType($this->post_type)->where('id', $parent_id)->get();
                if ($parent) {
                    $builder = $parent->merge(Post::ofType($this->post_type)->ofType($this->post_type)->OfParent($parent_id)->get());
                } else {
                    $builder = $builder->OfParent($parent_id);
                }
            } else {
                $builder = $builder->with(['parent', 'customPostTranslations.customPost.translations']);
            }
        }

        // View::share ( 'post_types', $post_types );

        $trashedQuery = Post::onlyTrashed()->ofType($this->post_type);
        $pendingQuery = Post::ofType($this->post_type)->pending();
        $draftQuery = Post::ofType($this->post_type)->draft();

        $author = auth()->user();

        if ($author->rol->level < 90) {
            $builder = $builder->where('author_id', $author->id);
            $trashedQuery = $trashedQuery->where('author_id', $author->id);
            $pendingQuery = $pendingQuery->where('author_id', $author->id);
            $draftQuery = $draftQuery->where('author_id', $author->id);
        }

        // View::share ( 'post_types', $post_types );

        $items = $builder->orderByDesc('created_at')->get();

        $num_trashed = $trashedQuery->count();
        $num_pending = $pendingQuery->count();
        $num_draft = $draftQuery->count();

        return view('Admin::posts.index', compact('items', 'status', 'num_trashed', 'num_pending', 'num_draft', 'home_page_id'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $author = auth()->user();

        $post = Post::withTrashed()->findOrFail($id);

        if ($post->author_id != $author->id) {
            abort(403);
        }

        return parent::edit($locale, $id);
    }
}