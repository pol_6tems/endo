<?php


namespace App\Modules\Stocktextil\Controllers;


use App\Http\Controllers\PostsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends PostsController
{

    public function index()
    {
        $products = get_posts(['post_type' => 'product']);

        return view('Front::home', compact('products'));
    }


    public function contact(Request $request)
    {
        $params = $request->all();

        $products = get_posts(['post_type' => 'product', 'id' => $params['product']]);

        if ($products->count()) {
            $product = $products->first();

            $contactEmail = $product->get_field('contact_email');
            $contactName = $product->get_field('contact_name');

            Mail::raw($params['message'], function ($message) use ($contactEmail, $contactName) {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to($contactEmail, $contactName);
                $message->subject('Contacte');
            });

            return redirect()->route('index')->with('message', __('Order sent successfully'));
        }

        return redirect()->back()->with('error', __('An error occurred while sending order'));
    }
}