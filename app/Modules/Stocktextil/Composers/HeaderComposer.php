<?php


namespace App\Modules\Stocktextil\Composers;


use Illuminate\View\View;

class HeaderComposer
{

    public function compose(View $view)
    {

        $view->with([
            'user' => auth()->user()
        ]);
    }
}