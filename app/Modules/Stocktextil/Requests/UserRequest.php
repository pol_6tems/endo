<?php


namespace App\Modules\Stocktextil\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|' . Rule::unique('users', 'email')->ignore(auth()->user()->id),
            'lastname' => 'string|max:255',
            'phone_number' => 'string|max:255',
            'company_name' => 'string|max:255',
            'company_address' => 'string|max:255',
            'company_city' => 'string|max:255',
            'company_country' => 'string|max:255',
            'company_nif' => 'string|max:255',
        ];
    }
}