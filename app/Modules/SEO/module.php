<?php

namespace App\Modules\SEO;

use Artisan;
use Exception;
use App\Models\Admin\AdminMenu;

class SEOModule {
    
    const MODULE_NAME = 'SEO';
    const MODULE_ROUTE_ADMIN = 'admin.seo';
    const MODULE_ROUTE = 'seo';

    public static function get_path() {
        return 'Modules\\' . self::MODULE_NAME . '\\';
    }

    // INSTALL
    public static function install() {
        try {
            Artisan::call('migrate', array(
                '--path' => 'app/Modules/' . self::MODULE_NAME . '/migrations', 
                '--force' => true,
            ));

            self::make_menu();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // DESTROY
    public static function reset() {
        try {
            Artisan::call('migrate:reset', array(
                '--path' => 'app/Modules/' . self::MODULE_NAME . '/migrations', 
            ));
            
            self::remove_menu();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // RESET
    public static function refresh() {
        try {
            Artisan::call('migrate:refresh', array(
                '--path' => 'app/Modules/' . self::MODULE_NAME . '/migrations', 
            ));

            self::remove_menu();

            self::make_menu();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function make_menu() {
        if ( !AdminMenu::where('url', self::MODULE_ROUTE_ADMIN . '.index')->exists() ) {
            AdminMenu::create([
                'name' => 'SEO',
                'icon' => 'search',
                'has_url' => true,
                'url_type' => 'route',
                'url' => self::MODULE_ROUTE_ADMIN . '.index',
                'controller' => self::get_path() . 'Controllers\\Admin\\Admin' . self::MODULE_NAME . 'Controller',
                'order' => 800,
                'parameters' => NULL,
            ]);
        }
    }

    public static function remove_menu() {
        return AdminMenu::where('url', self::MODULE_ROUTE_ADMIN . '.index')->delete();
    }

}