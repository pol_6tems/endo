@php
$metas = $params['metas'];
if ( $metas->no_app_name ) $meta_title = $metas->title;
else $meta_title = (!empty($metas->title) ? $metas->title . ' - ' : '') . ucwords($metas->app_name);
@endphp

<meta name="robots" content="@yield('meta_robots', $metas->robots)" />

<title>@yield('title', $meta_title)</title>

<meta name="description" content="@yield('meta_desc', $metas->description)">

<meta name="keywords" content="{{ $metas->keywords }}" />
<meta property="image" content="{{ $metas->image }}" />

<meta property="og:url" content="{{ $metas->url }}" />
<meta property="og:title" content="@yield('title', $meta_title)" />
<meta property="og:description" content="@yield('meta_desc', $metas->description)" />
<meta property="og:image" content="{{ $metas->image }}" />
<meta property="og:type" content="website" />

<meta property="og:site_name" content="{{ $metas->app_name }}"/>
<meta name="twitter:description" content="{{ $metas->description }}"/>
<meta name="twitter:title" content="{{ $meta_title }}"/>
<meta name="twitter:card" content="summary_large_image"/>

@yield('extra_metas')

{{--<meta property="fb:app_id" content="453941658026880" />--}}