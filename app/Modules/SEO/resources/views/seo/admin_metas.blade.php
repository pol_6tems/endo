@php
$post = $params['post'];
$meta_fields = $params['meta_fields'];
@endphp

@if ( Auth::user()->role == 'admin' )

<div class="col-md-12">
    <article id="seo_metas" data-post="{{ $post->id }}" class="card pm">
        <div class="card-header card-header-primary card-header-icon">
            <div class="flex-row" style="justify-content: space-between;">
                <div class="header-left">
                    <h4 class="card-title">@Lang('SEO')</h4>
                </div>
                <div class="header-right">
                    <a data-toggle="collapse" href="#seo_metas_fields"><i class="material-icons">keyboard_arrow_down</i></a>
                </div>
            </div>
        </div>

        <div id="seo_metas_fields" class="collapse">
            <div class="card-body pb-5">
                {{-- mostrarIdiomes($_languages, $language_code, 'panel_seo_metas_fields') --}}
                <!-- Tab de General -->
                <div class="tab-content">
                    @foreach($_languages as $key => $language_item)
                    <div id="panel_seo_metas_fields{{ $language_item->code }}" class="tab-pane {{ ($language_item->code == $language_code) ? 'active' : '' }}">
                        <div class="custom_field_group_fields">
                            @foreach ($meta_fields as $k => $custom_field)

                                @php ( $value = $post->get_field( $custom_field->name, $language_item->code ) )
                                @if ( $custom_field->type == 'media' )
                                    @php ( $value = \App\Models\Media::where('id', $value)->first() )
                                @endif

                                @php($viewParams = [
                                    'title' => $custom_field->title,
                                    'name' => "custom_fields[".$language_item->code."][".$custom_field->id . "]",
                                    'value' => $value,
                                    'params' => json_decode($custom_field->params),
                                    'position' => 'bottom',
                                    'custom_field' => $custom_field,
                                    'order' => $k,
                                    'lang' => $language_item->code,
                                ])

                                @if (View::exists('Admin::default.custom_fields.' . $custom_field->type))
                                    @includeIf('Admin::default.custom_fields.' . $custom_field->type, $viewParams)
                                @endif
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </article>
</div>

@endif