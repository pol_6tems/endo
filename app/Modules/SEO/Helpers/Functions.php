<?php

use App\Modules\SEO\Controllers\SEOController;

function show_seo_metas($post) {
    global $_item;
    if ( empty($post) ) $post = $_item;
    return SEOController::show_seo_metas($post);
}

function get_seo_metas($post) {
    global $_item;
    if ( empty($post) ) $post = $_item;
    return SEOController::get_seo_metas($post);
}

function show_seo_metas_post_form_fields($post) {
    global $_item;
    if ( empty($post) ) $post = $_item;
    return SEOController::show_seo_metas_post_form_fields($post);
}

add_action('get_header', 'show_seo_metas');
add_action('post_form_fields', 'show_seo_metas_post_form_fields');