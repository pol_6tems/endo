<?php

namespace App\Modules\SEO\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\SEO\Models\SEO;
use App\Models\CustomPost;

class AdminSEOController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('SEO');
        $this->section_icon = 'search';
        $this->section_route = 'admin.seo';
    }

    protected function get_form_fields() {
        return array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'post_type', 'title' => __('Post Type'), 'type' => 'select', 'required' => true,
                    'datasource' => $this->get_custom_posts(),
                    'sel_value' => 'post_type',
                    'sel_title' => ['post_type'],
                    'sel_search' => true,
                    'with_post_type' => false,
                ],
                ['value' => 'maximo', 'title' => __('Max'), 'type' => 'number'],
                ['value' => 'icono_full_id', 'title' => __('Icon') . ' ' . __('Full'), 'type' => 'media', 'media' => 'icono_full'],
                ['value' => 'icono_empty_id', 'title' => __('Icon') . ' ' . __('Empty'), 'type' => 'media', 'media' => 'icono_empty'],
            ], 
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = SEO::get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'has_duplicate' => true,
            'headers' => [
                __('#') => [ 'width' => '10%' ],
                __('Name') => [],
                __('Post Type') => [],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => 'name'],
                ['value' => 'post_type'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {        
        return View('Admin::default.create', $this->get_form_fields());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();
        Rating::create($data);
        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name added successfully', ['name' => __('Rating')]) ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id) {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id) {
        $item = SEO::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');

        return View('Admin::default.edit', array_merge(
            ['item' => $item], 
            $this->get_form_fields()
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id) {
        $item = SEO::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');

        $data = $request->all();
        $item->update($data);
        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name updated successfully', ['name' => __('SEO')]) ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id) {
        $item = SEO::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');
        
        $item->delete();
        
        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name deleted successfully', ['name' => __('SEO')]) ]);
    }

    protected function get_custom_posts() {
        $cps = new Collection();
        
        $cp = new CustomPost();
        $cp->post_type = 'post';
        $cps->push($cp);
        $cp = new CustomPost();
        $cp->post_type = 'page';
        $cps->push($cp);

        $cps_aux = CustomPost::withTranslation(function($q){
            $q->orderBy('post_type');
        })->get();
        $cps = $cps->merge($cps_aux);
        return $cps;
    }

    public function duplicate($locale, $id) {
        $item = SEO::find($id);
        
        $data = [
            'name' => $item->name . ' (copy)',
            'post_type' => $item->post_type,
            'params' => $item->params,
            'maximo' => $item->maximo,
            'icono_full_id' => $item->icono_full_id,
            'icono_empty_id' => $item->icono_empty_id,
        ];

        $new = SEO::create($data);

        if ( !empty($new) ) {
            $msg = __(':name duplicated successfully', ['name' => __('SEO')]);
        } else {
            $msg = __('Error saving data');
        }
        return redirect()->route('admin.seo.index')->with([ 'message' => $msg ]);
    }

}
