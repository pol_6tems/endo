<?php

namespace App\Modules\SEO\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Modules\SEO\Models\SEO;
use App\Post;
use \App\Models\Media;
use \App\Models\User;
class SEOController extends Controller
{

    public static function show_seo_metas($post) {
        $metas = self::get_seo_metas($post);

        return [
            'view' => 'seo.metas',
            'params' => [
                'metas' => $metas,
            ]
        ];
    }

    public static function get_seo_metas($post) {
        $metas = new \stdClass();
        $metas->title = '';
        $metas->description = '';
        $metas->url = '';
        $metas->keywords = '';
        $metas->image = null;
        $metas->robots = 'index, follow';
        $metas->no_app_name = false;

        $configs = self::get_configurations();
        if ( empty($configs['app_name']['value']) ) $metas->app_name = config('app.name', 'Endo');
        else $metas->app_name = $configs['app_name']['value'];
        
        if ( !is_object($post) ) $post = Post::find($post);

        if ( empty($post) ) return $metas;

        $t_post = $post->translate(true);

        $meta_title = $post->get_field('meta_title');
        $meta_description = $post->get_field('meta_description');
        $meta_keywords = $post->get_field('meta_keywords');
        $meta_url = $post->get_field('meta_url');
        $meta_image = $post->get_field('meta_image');
        $meta_robots = $post->get_field('meta_robots');

        $metas->no_app_name = !empty($meta_title);
        $metas->title = !empty($meta_title) ? $meta_title : $t_post->title;
        $metas->description = !empty($meta_description) ? $meta_description : get_excerpt(strip_tags($t_post->description), 40);
        $metas->url = !empty($meta_url) ? $meta_url : $post->get_url();
        $metas->keywords = !empty($meta_keywords) ? $meta_keywords : $t_post->keywords;
        if ( !empty($meta_robots) ) $metas->robots = $meta_robots;

        if ( !empty($meta_image) && !empty($meta_image) ) {
            $value = Media::where('id', $meta_image)->first();
            if ( !empty($value) ) $metas->image = $value->get_thumbnail_url('medium_large');
        }
        else $metas->image = !empty($post->media()) ? $t_post->media->get_thumbnail_url('medium_large') : null;

        return $metas;

    }

    public static function show_seo_metas_post_form_fields($post) {
        $meta_fields = self::get_seo_metas_post_form_fields($post);
            return [
            'view' => 'seo.admin_metas',
            'params' => [
                'post' => $post,
                'meta_fields' => $meta_fields,
            ]
        ];
    }

    public static function get_seo_metas_post_form_fields($post) {

        $cfs = array();
        // Title
        $cf = new \stdClass();
        $cf->id = 'meta_title';
        $cf->title = __('Meta Title');
        $cf->name = 'meta_title';
        $cf->type = 'text';
        $cf->params = json_encode(array());
        $cfs[] = $cf;
        // Description
        $cf = new \stdClass();
        $cf->id = 'meta_description';
        $cf->title = __('Meta Description');
        $cf->name = 'meta_description';
        $cf->type = 'text';
        $cf->params = json_encode(array());
        $cfs[] = $cf;
        // Keywords
        $cf = new \stdClass();
        $cf->id = 'meta_keywords';
        $cf->title = __('Meta Keywords');
        $cf->name = 'meta_keywords';
        $cf->type = 'text';
        $cf->params = json_encode(array());
        $cfs[] = $cf;
        // Image
        $cf = new \stdClass();
        $cf->id = 'meta_image';
        $cf->title = __('Meta Image');
        $cf->name = 'meta_image';
        $cf->type = 'media';
        $cf->params = json_encode(array());
        $cfs[] = $cf;
        // Robots
        $cf = new \stdClass();
        $cf->id = 'meta_robots';
        $cf->title = __('Meta Robots');
        $cf->name = 'meta_robots';
        $cf->type = 'text';
        $cf->params = json_encode(array());
        $cfs[] = $cf;
        // URL
        $cf = new \stdClass();
        $cf->id = 'meta_url';
        $cf->title = __('Meta URL');
        $cf->name = 'meta_url';
        $cf->type = 'text';
        $cf->params = json_encode(array());
        //$cfs[] = $cf;
        
        return $cfs;
    }

}
