<?php

use App\Post;
use Twilio\Rest\Client;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use CodeItNow\BarcodeBundle\Utils\QrCode;
use Maatwebsite\Excel\Excel as CSVFormat;
use App\Modules\FormacioiTreball\Mail\OrderShipped;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use App\Modules\FormacioiTreball\Exports\PostsExport;

function generateBarcodeNumber() {
    $number = mt_rand(1000000000, 9999999999); // better than rand()

    // call the same function if the barcode exists already
    if (barcodeNumberExists($number)) {
        return generateBarcodeNumber();
    }

    // otherwise, it's valid and can be used
    return $number;
}
    
function barcodeNumberExists($number) {
    // query the database and return a boolean
    // for instance, it might look like this in Laravel
    $peticiones = App\Post::with('metas')->ofType('peticion')->get();
    $peticiones = $peticiones->filter(function($peticion) use ($number) {
        $metas = $peticion->metas;
        return $metas->firstWhere('value', $number);
    });

    return !$peticiones->isEmpty();
}


function generatePass($identificador) {
    $peticio = get_posts([
        "post_type" => "peticion",
        "metas" => [ ["identificador-de-peticion", $identificador] ]
    ])->first();
    
    $qrCode = new QrCode();
    $qrCode
        ->setText($identificador)
        ->setSize(274)
        ->setErrorCorrection('high')
        ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
        ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
        ->setImageType(QrCode::IMAGE_TYPE_PNG);

    $barCode = new BarcodeGenerator();
    $barCode->setText(strval($identificador));
    $barCode->setType(BarcodeGenerator::Code128);
    $barCode->setScale(3);
    // $barCode->setThickness(25);
    $barCode->setFormat("PNG");
    $barCode->setFontSize(10);

    $pdf = PDF::setOptions([
        'isHtml5ParserEnabled' => true,
        'isRemoteEnabled' => true
    ]);

    $data = [
        'now' => Date::now(),
        'html' => false,
        'qrcode' => $qrCode,
        'barcode' => $barCode,
        'peticio' => $peticio,
        'tiendas' => Post::ofType('tienda')->get(),
    ];

    try {
        return $pdf->loadView('Front::pdfs.pass', $data);
    } catch(Exception $e) {
        if (is_null($peticio)) return __("La petición no existe");
        else return $e->getMessage();
    }
}

function notifyUser($dades) {
    // $link = route("targeta_pass", ["hash" => endoEncrypt([ "identificador" => $dades->{"identificador"}, "email" => $dades->{'e-mail-usuario'} ]) ]);

    $pdf = generatePass($dades->{"identificador"});
    if (!is_string($pdf)) {
        $emails = [];
        if ($dades->{'e-mail-usuario'}) {
            $emails[] = $dades->{'e-mail-usuario'};
        }

        if ($dades->{'email-tecnico'}) {
            $emails[] = $dades->{'email-tecnico'};
        }

        if (count($emails) > 0) {
            Mail::to($emails)->send(new OrderShipped($dades,  $pdf));
        }

        Excel::store(new PostsExport, 'public/FormacioiTreball/pass_list.csv', 'local', CSVFormat::CSV);
        
        /*if ($dades->{'telefono'}) {
            $twilio = new Client(env("TWILIO_SID"), env("TWILIO_TOKEN")); 
            $message = $twilio->messages
                    ->create("whatsapp:+34".$dades->{"telefono"}, // to 
                            array(
                                "from" => "whatsapp:+14155238886",
                                "body" => $link,
                            )
                    );
        }*/
    }
}

if (! function_exists('isAdmin')) {

    /**
     * Helper per saber si s'es admin o no
     */
    function isAdmin() {
        return !is_null(auth()->user()) && (auth()->user()->isAdmin() || auth()->user()->role == "subadmin");
    }
}