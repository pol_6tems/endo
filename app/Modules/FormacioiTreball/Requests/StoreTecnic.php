<?php

namespace App\Modules\FormacioiTreball\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTecnic extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "lastname" => "required",
            "email" => "required|email:rfc,dns|unique:users,email",
        ];
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('El nombre es obligatorio'),
            'lastname.required'  => __('El apellido es obligatorio'),
            'email.required'  => __('El email es obligatorio'),
            'email.unique'  => __('Parece que ya existe un usuario con este email'),
        ];
    }
}
