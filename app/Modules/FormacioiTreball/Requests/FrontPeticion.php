<?php

namespace App\Modules\FormacioiTreball\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FrontPeticion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // "expediente" => "required",
            "nombre" => "required",
            // "apellido" => "required",
            "dni" => "required",
            // "email" => "required",
            // "telefono" => "required",
            "importe" => "required",
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'expediente.required' => __('El número de expediente es obligatorio'),
            'nombre.required'  => __('El nombre es obligatorio'),
            'apellido.required'  => __('El apellido es obligatorio'),
            'dni.required'  => __('El DNI es obligatorio'),
            'email.required'  => __('El email es obligatorio'),
            'telefono.required'  => __('El teléfono es obligatorio'),
            'importe.required'  => __('El importe es obligatorio'),
        ];
    }
}
