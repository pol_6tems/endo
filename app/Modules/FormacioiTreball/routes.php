<?php

Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    // Posts new send route
    Route::get('posts/{post_id}/send', "Admin\MyPostsController@send")->name('admin.posts.send');
    Route::get('posts/csv', "Admin\MyPostsController@csv")->name('admin.posts.csv');
    
    Route::resource('users', 'Admin\MyUsersController', ['as' => 'admin']);
    Route::resource('posts', "Admin\MyPostsController", ["as" => "admin", 'except' => ['show']]);
});

Route::group(['middleware' => ['rols', 'auth']], function () {
    Route::resource('peticion', 'Admin\PeticionController', ['as' => 'admin']);
    Route::resource('tecnico', 'Admin\TecnicsController', ['as' => 'admin']);
    
    Route::get('reenviar/{post}', 'Admin\PeticionController@reenviar')->name('reenviar');
    Route::get('calculadora', 'Admin\MyAdminController@calculadora')->name('calculadora');
});

Route::get('/targeta_pass/{hash}', 'Admin\MyAdminController@targeta_pass')->name('targeta_pass');


// Password Reset Routes...
Route::get('password/reset', 'Auth\MyForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\MyForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\MyResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\MyResetPasswordController@reset')->name('password.update');

Route::get('/{index?}', 'MyHomeController@index')->name('index');