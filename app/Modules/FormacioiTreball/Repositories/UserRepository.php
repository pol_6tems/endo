<?php


namespace App\Modules\FormacioiTreball\Repositories;

use App\User;
use App\Repositories\PostsRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class UserRepository
{
    protected $postsRepo;

    public function __construct(PostsRepository $repo)
    {
        $this->postsRepo = $repo;
    }
    
    public function find($id) {
        return User::find($id);
    }

    public function getUserByEmail($email) {
        return User::where("email", $email)->first();
    }

    public function getPeticions(User $user)
    {
        return $this->postsRepo->getPosts([
            "post_type" => "peticion",
            "where" => [
                ["users.id", "=", $user->id]
            ]
        ]);
    }

    public function getCurrentEntityUsers() {
        $that = $this;
        $repo = $this->postsRepo;
        $users = User::get();
        if (auth()->user()->rol->level < 90) {
            $users = $users->filter(function($f) {
                return !$f->isAdmin() && $f->rol != "subadmin" &&
                        $f->entitat == auth()->user()->entitat;
            });
        }

        return $users->map(function($u) use($repo, $that) {
            $u->entitat = $repo->find($u->entitat);
            $u->npeticions = count($that->getPeticions($u));
            return $u;
        });
    }
}