<?php
/**
 * File which describes the API definition
 */


// GET Routes
Route::get("pass/list", "ApiController@list");
Route::get("pass/{peticio}", "ApiController@show");

// POST Routes
Route::post("pass", "ApiController@pass");
Route::post("pass/{peticio}", "ApiController@pass_patch");
Route::post("pass/{peticio}/spend", "ApiController@pass_gastat");
Route::get("pass/{peticio}/valid", "ApiController@pass_valid");


// Callback if no mached route
Route::get('/{any}', function ($any) {
    return json_encode([
        "success" => false,
        "message" => "Not a valid method"
    ]);  
})->where('any', '.*');