<?php

namespace App\Modules\FormacioiTreball\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Exception;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    protected $json;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($json, $pdf)
    {
        $this->json = $json;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try {
            if (!is_string($this->pdf)) {
                return $this->subject("Targeta Pass - Formació i Treball")
                    ->view('mail')
                    ->with([
                        'email' => $this->json->{"e-mail-usuario"},
                        'dni' => $this->json->{"dni-nie-passaporte"},
                        'nombre' => $this->json->{"nombre-usuario"},
                        'telefono' => $this->json->{"telefono"},
                        'importe' => $this->json->{"importe"} . " €",
                    ])
                    ->attachData($this->pdf->output(), 'targeta-pass.pdf', [
                        'mime' => 'application/pdf',
                    ]);
            }
            return null;
        } catch(Exception $e) {
            Log::channel('targetes_pass')->error('Pass card generation failed: ' . $e->getMessage());
        }        
    }
}
