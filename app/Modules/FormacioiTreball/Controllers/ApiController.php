<?php

namespace App\Modules\FormacioiTreball\Controllers;

use App\Post;
use App\User;
use stdClass;
use Jenssegers\Date\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Excel as CSVFormat;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\FormacioiTreball\Exports\PostsExport;

class ApiController extends AdminController
{
    protected $user;
    private $errors = [];
    public const PASS_FIELDS = [
        'ID_Donant' => 'id-donant',
        'ID_Entitat' => 'identificador-entidad',
        'Entitat' => 'nombre-entidad',
        'N_Expediente' => 'numero-expediente',
        'Persona' => 'nombre-usuario',
        'DNI' => 'dni-nie-passaporte',
        'Email' => 'e-mail-usuario',
        'Telefono' => 'telefono',
        'Data_Caducitat' => 'data-caducidad',
        'Tecnic_Caritas' => 'identificador-tecnico',
        'N_Tecnic' => 'nombre-tecnico',
        'Subtipus_Seu' => 'subtipo-sede',
        'Mail_Tecnic_caritas' => 'email-tecnico',
        'Tipus_Servei' => 'tipo-de-servicio',
        'Import' => 'importe',
        'Edad' => 'edad',
        'Genero' => 'genero',
        'Familia' => 'familia',
        'Aportacion' => 'aportacion',
    ];

    public const REQUIRED = [
        'id-donant',
        'identificador-entidad',
        'nombre-entidad',
        'numero-expediente',
        'nombre-usuario',
        'dni-nie-passaporte',
        'e-mail-usuario',
        'telefono',
        'data-caducidad',
        'identificador-tecnico',
        'nombre-tecnico',
        'subtipo-sede',
        'email-tecnico',
        'tipo-de-servicio',
        'importe',
    ];

    public function __construct() {
        // Check Valid Token
        $token = request()->bearerToken();
        $this->user = User::where('api_token', $token)->first();
        
        if ( is_null($this->user) ) {
            echo json_encode([
                'success' => false,
                'message' => "Not Authorized",
            ]); exit(1);
        }

        Log::info(sprintf("[%s]:auth:Login del user [%s] %s", request()->ip(), $this->user->id, $this->user->fullname()));
    }

    public function list(Request $request) {        
        $peticions = Post::ofType('peticion')->whereTranslation("locale", "es")->get();
        $peticions = $peticions->map(function($p) {
            return $this->fillPeticion($p);
        });

        Log::info(sprintf("[%s]:list:S'ha fet una petició per mostrar la llista de peticions", $request->ip()));

        return $peticions;
    }

    public function show(Post $peticio, Request $request) {
        Log::info(sprintf("[%s]:show:S'ha fet una petició per mostrar la petició", $request->ip(), $peticio->id ));
        return json_encode($this->fillPeticion($peticio));
    }

    public function pass(Request $request) {
        if ($json = $this->verificar_JSON()) {
            // Guardem el JSON a la DB linkada amb ENDO
            $post = $this->storeJSON($json);
            // Enviem el Correu al usuari

            Log::info(sprintf("[%s]:store:Creat una petició amb id [%d]. DATA: %s", $request->ip(), $post->id, \json_encode($json) ));

            notifyUser($json);
            
            return response()->json([
                'success' => true,
                'message' => [
                    'pass_id' => $post->id,
                ],
            ]);
        }

        Log::error(sprintf("[%s]:store:Error al crear una peticio. ERRORS: %s DATA: %s", $request->ip(), join(',', $this->errors), \json_encode($json)));

        return response()->json([
            'success' => false,
            'message' => join('\\n', $this->errors),
        ]);
    }

    public function pass_patch(Post $peticio, Request $request) {
        if ($peticio) {
            if (isset($request->field)) {
                $field = $request->field;
                if (array_key_exists($field, self::PASS_FIELDS)) {
                    if (isset($request->value)) {
                        $peticio->set_field(self::PASS_FIELDS[$field], $request->value);

                        Log::info(sprintf("[%s]:update:S'ha actualitzat la peticio amb id [%d]", $request->ip(), $peticio->id));
                        
                        Excel::store(new PostsExport, 'public/FormacioiTreball/pass_list.csv', 'local', CSVFormat::CSV);
                        
                        return response()->json([
                            'success' => true,
                            'message' => $peticio->id,
                        ]);
                    } else $this->errors[] = __('Missing \'value\' variable');
                } else $this->errors[] = __('Field :field doesn\'t exists', ['field' => $field]);
            } else $this->errors[] = __('Missing \'field\' variable');
        } else $this->errors[] = __('Pass order doesn\'t exist');

        Log::error(sprintf("[%s]:update:Error a la petició amb id [%d]. ERRORS: %s", $request->ip(), $peticio->id, join(',', $this->errors)));

        return response()->json([
            'success' => false,
            'message' => join('\\n', $this->errors),
        ]);
    }


    /**
     * Mostra la vista del panell d'usuari
     *
     * @return void
     */
    public function pass_gastat(Post $peticio, Request $request) {
        if ($peticio) {
            if (isset($request->value)) {
                if (floatval($request->value) > 0) {
                    $current = $peticio->get_field('importe-gastado');
                    $new = $current + floatval($request->value);
                    $peticio->set_field('importe-gastado', $new);

                    Log::info(sprintf("[%s]:spend:S'ha modificat el camp gastat de la petició amb id [%d]", $request->ip(), $peticio->id));
                
                    Excel::store(new PostsExport, 'public/FormacioiTreball/pass_list.csv', 'local', CSVFormat::CSV);
                    
                    return response()->json([
                        'success' => true,
                        'message' => $peticio->id,
                    ]);
                } else $this->errors[] = __('Value must be a float number');
            } else $this->errors[] = __('Missing \'value\' variable');
        } else $this->errors[] = __('Pass order doesn\'t exist');

        Log::error(sprintf("[%s]:spend:Error a la petició amb id [%d]. ERRORS: %s", $request->ip(), $peticio->id, join(',', $this->errors)));

        return response()->json([
            'success' => false,
            'message' => join('\\n', $this->errors),
        ]);
    }


    public function pass_valid($peticio, Request $request) {
		$peticio = Post::find($peticio);
        if ($peticio) {
            $caducitat = $peticio->get_field("data-caducidad");
            if ($caducitat != "" && $caducitat) {
                if (Date::createFromFormat("d/m/Y", $caducitat) > Date::now()) {
                    Log::info(sprintf("[%s]:valid:S'ha fet una petició per saber la validesa de la petició", $request->ip(), $peticio->id ));
                    return response()->json([
                        'success' => true,
                        'message' => "This pass card is valid",
                    ]);
                } else $this->errors[] = __("This pass card has expired");
            } else {
                return response()->json([
                    'success' => true,
                    'message' => "This pass card doesn't have expiration date",
                ]);
            }
        } else $this->errors[] = __('Pass order doesn\'t exist');

        Log::error(sprintf("[%s]:valid:Error a la petició amb id [%d]. ERRORS: %s", $request->ip(), $peticio->id, join(',', $this->errors)));
		
		return response()->json([
            'success' => false,
            'message' => join('\\n', $this->errors),
        ]);
    }

    private function fillPeticion($peticion) {
        $fields = [
            "identificador-de-peticion", "id-donant", "identificador-entidad", "nombre-entidad",
            "numero-expediente", "nombre-usuario", "dni-nie-passaporte", "e-mail-usuario",
            "telefono", "numero-utilitzaciones", "data-caducidad", "identificador-tecnico",
            "email-tecnico", "subtipo-sede", "sub-subtipo-sede", "tipo-de-servicio",
            "importe", "importe-gastado" ];
        
        $aux = new \stdClass();
        $aux->id = $peticion->id;
        foreach($fields as $field) {
            $aux->{$field} = $peticion->get_field($field);
        }
        return $aux;
    }

    /**
     * Retorna un objecte conformat pel JSON esperat, en cas d'error retorna fals
     * i omplena la variable global d'errors
     *
     * @return void
     */
    private function verificar_JSON() {
        try {
            $data = request()->all();
            $json = new stdClass();
            
            $json->identificador = generateBarcodeNumber();
            foreach(self::PASS_FIELDS as $key => $field) {
                if (!in_array($field, self::REQUIRED)) $json->{$field} = $data[$key] ?? '';
				else $json->{$field} = $data[$key];
            }

            return $json;
        } catch (\Exception $e) {
            $field_missing = str_replace("Undefined index: ", "", $e->getMessage());
            $this->errors[] = __('Falta el campo: :field', ['field' => $field_missing]);
            return false;
        }
    }

    private function storeJSON($json)
    {
        $title = $json->{'identificador-tecnico'} . ' - ' . $json->{'nombre-entidad'} . ' - ' . Date::now()->format('d-m-Y H:i');
        $post_name = str_slug($title);

        $post = Post::create([
            'type' => 'peticion',
            'status' => 'publish',
            'author_id' => $this->user->id,
            'es' => [
                'title' => $title,
                'description' => '',
                'post_name' => $post_name,
            ],
            'ca' => [
                'title' => $title,
                'description' => '',
                'post_name' => $post_name,
            ],
        ]);

        $post->set_field('identificador-de-peticion', $json->identificador);
        foreach(self::PASS_FIELDS as $field) {
            $post->set_field($field, $json->{$field});
        }

        return $post;
    }
}
