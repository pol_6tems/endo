<?php

namespace App\Modules\FormacioiTreball\Controllers\Admin;

use App\Facades\BotDetector;
use App\Http\Controllers\Admin\AdminController;

class MyAdminController extends AdminController
{
    protected $users;

    /**
     * Mostra la Vista de la Calculadora
     *
     * @return void
     */
    public function calculadora()
    {
        $categories = get_posts('categoria');
        $productes = get_posts('producto');

        return view('Front::admin.calculadora', compact("categories", "productes"));
    }

    /**
     * Renderitzar amb l'enllaç enviat al movil
     *
     * @param [type] $locale
     * @param [type] $hash
     * @return void
     */
    public function targeta_pass($locale, $hash)
    {
        $data = endoDecrypt($hash);
        $pdf = generatePass($data['identificador']);
        $link = route("targeta_pass", ["hash" => endoEncrypt([ "identificador" => $data['identificador'], "email" => $data['email'] ]) ]);

        if (!BotDetector::isBot()) {
            return $pdf->stream();
        } else {
            return view("Front::pdfs.opengraf", compact("link"));
        }
    }
}
