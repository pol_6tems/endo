<?php

namespace App\Modules\FormacioiTreball\Controllers\Admin;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Admin\UsersController;

class MyUsersController extends UsersController
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = parent::create();
        $entitats = get_posts("entidad");
        return view("Front::admin.users.create", array_merge($view->getData(), [
            "entitats" => $entitats
        ]));
    }

    public function show($locale, $id)
    {
        return redirect(route("admin.users.index"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, User $user)
    {
        $view = parent::edit($locale, $user);
        $entitats = get_posts("entidad");

        if (isAdmin() || $user->entitat == auth()->user()->entitat) {
            return View('Front::admin.users.edit', array_merge($view->getData(), [
                "entitats" => $entitats,
            ]));
        }

        return redirect()->back()->with([
            "status" => "error",
            "title" => __("No tienes permiso"),
            "msg" => __("No tienes permisos para modificar este técnico")
        ]);
    }

    protected function fillUser(User $user, Request $request) {
        // Prevent losing session for different reference on current user
        if ($user->id == auth()->user()->id) {
            $user = auth()->user();
        }

        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->limit = $request->limit;
        $user->entitat = $request->entitat;
        $user->role = (isset($request->role)) ? $request->role : $user->rol->name;

        if ( !empty($request->password)) {
            if ($request->password != $request->password_confirmation) {
                return redirect()->back()->with('error', trans('validation.confirmed', ['attribute' => 'password']));
            }

            $user->password = bcrypt($request->password);
        }
        
        $user->update();

        if (isset($request->custom_fields)) {
            $this->save_custom_fields($user, $request->custom_fields);
        }

        return $user;
    }

    /**
     * Mostra la vista del panell d'usuari
     *
     * @return void
     */
    public function front()
    {
        return view('Front::admin.users.front-edit');
    }
}
