<?php

namespace App\Modules\FormacioiTreball\Controllers\Admin;

use App\Post;
use stdClass;
use App\Models\CustomPost;
use App\Models\Configuracion;
use App\Models\CustomFieldGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Admin\PostsController;
use App\Modules\FormacioiTreball\Mail\OrderShipped;
use App\Modules\FormacioiTreball\Exports\PostsExport;
use App\Modules\FormacioiTreball\Controllers\ApiController;

class MyPostsController extends PostsController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parent = (request()->parent) ? request()->parent : null;
        $status = (request()->status) ? request()->status : $this->def_status;

        $home_page = Configuracion::get_config('home_page');
        $home_page_id = !empty($home_page) ? $home_page->id : 0;

        $builder = Post::ofType($this->post_type)
                        ->ofStatus($status)
                        ->withTranslation();
        
        if ( !$status ) {
            $items = $builder->with(['parent', 'customPostTranslations.customPost.translations']);
        }

        $items = $builder->orderByDesc('created_at')->get();
        $num_trashed = Post::onlyTrashed()->ofType($this->post_type)->count();
        $num_pending = Post::ofType($this->post_type)->pending()->count();
        $num_draft = Post::ofType($this->post_type)->draft()->count();

        if ($this->post_type == 'peticion') return view('Front::posts.index', compact('items', 'status', 'num_trashed', 'num_pending', 'num_draft', 'home_page_id'));
        else return view('Admin::posts.index', compact('items', 'status', 'num_trashed', 'num_pending', 'num_draft', 'home_page_id'));
    }

    public function edit($locale, $id)
    {
        $post = Post::withTrashed()->findOrFail($id);
        $templates = $this->get_templates();
        $cf_groups = CustomFieldGroup::where('post_type', $this->post_type);

        // Si el paràmetre de tipus es diferent al del post, redirigim al post amb el tipus correcte (EVITAR RENDERITZAR CUSTOM FIELDS QUE NO CORRESPONEN)
        if ($this->post_type != $post->type) return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $post->type);

        if ( !empty($post->template) && $post->type == 'page' ) {
            $cf_groups = $cf_groups->where('template', $post->template)
                ->orWhere('template', null)->where('post_type', $this->post_type);
        }
        $cf_groups = $cf_groups->orderBy('order')->get();
        
        $params = null;
        if ( !in_array($this->post_type, ['post', 'page']) ) {
            $cp = CustomPost::whereTranslation('post_type', $this->post_type)->first();
            if ( !empty($cp) ) $params = $cp->params;
        }

        // Check if User can aprove pending post
        $can_aprove_pending = $post->status == 'pending' && Auth::user()->rol->level >= Post::ROL_LEVEL_LIMIT_REVISIO;

        if ($this->post_type == 'peticion') {
            return view('Front::posts.edit', compact('post', 'templates', 'cf_groups', 'params', 'can_aprove_pending'));
        } else {
            return view('Admin::posts.edit', compact('post', 'templates', 'cf_groups', 'params', 'can_aprove_pending'));
        }
    }

    public function send($lang, $id) {
        $post = Post::find($id);
        $json = new stdClass();

        
        $json->identificador = $post->get_field('identificador-de-peticion');
        foreach(ApiController::PASS_FIELDS as $field) {
            $json->{$field} = $post->get_field($field);
        }
        
        $pdf = generatePass($json->{"identificador"});
        if (!is_string($pdf)) {
            if ($json->{'e-mail-usuario'}) {
                Mail::to($json->{'e-mail-usuario'})->send(new OrderShipped($json, $pdf));
            }
        }
        
        return redirect(route($this->section_route . '.edit', $id));
    }

    public function csv($lang) {
        Excel::store(new PostsExport, 'public/FormacioiTreball/pass_list.csv', 'local', \Maatwebsite\Excel\Excel::CSV);
        return  redirect(route($this->section_route . '.index', ['post_type' => $this->post_type]));
    }
}
