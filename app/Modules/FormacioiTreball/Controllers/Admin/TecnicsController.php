<?php

namespace App\Modules\FormacioiTreball\Controllers\Admin;

use App\Post;
use App\User;
use App\Models\UserMeta;
use App\Models\CustomField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\FormacioiTreball\Requests\StoreTecnic;
use App\Modules\FormacioiTreball\Repositories\UserRepository;

class TecnicsController extends AdminController
{
    public function __construct(UserRepository $user)
    {
        parent::__construct();
        $this->users = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->users->getCurrentEntityUsers();
        return view("Front::admin.tecnics.index", compact("users"));
    }

    public function show() {
        return redirect(route("admin.peticion.create"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $entidades = get_posts("entidad");
        return view("Front::admin.tecnics.create", compact("entidades"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTecnic $request)
    {
        $data = $request->all();
        $password = str_random(8);
        
        $user = User::create([
            "name" => $data["name"],
            "lastname" => $data["lastname"],
            "email" => $data["email"],
            "password" => $password,
            "entitat" => auth()->user()->entitat,
            "role" => "tecnico",
        ]);

        $contacts = [[ "email" => $data["email"], "name" => $data["name"] ]];
        $text = "Su contraseña es: $password \n Deberia cambiarla por una que solo conozca usted en la mayor brevedad possible";

        Mail::raw($text, function ($message) use ($contacts) {
            $message->from(config('mail.from.address'), config('mail.from.name'));
            $message->subject('Formació i Treball - Nuevo Usuario');
            foreach ($contacts as $contact) {
                $message->to($contact['email'], $contact['name']);
            }
        });

        if (isset($request->custom_fields)) {
            $this->save_custom_fields($user, $request->custom_fields);
        }

        return redirect(route("admin.tecnico.index"))->with([
            "status" => "success",
            "title" => __("Técnico creado"),
            "msg" => __("Se ha creado un nuevo técnico"),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, User $tecnico)
    {
        $user = $tecnico;
        $entidades = get_posts("entidad");

        return view("Front::admin.tecnics.edit", compact("user", "entidades"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, User $tecnico)
    {
        $password = $request->password;
        $confirm = $request->password_confirmation;
        
        if (auth()->user()->rol->level < 50) {
            if ($password == "" || $password != $confirm) {
                return redirect()->back()->withInput(Input::all())->with([
                    "status" => "error",
                    "title" => __("Perfil no actualizado"),
                    "msg" => __("Ha habido un error, las contraseñas no coinciden o esta vacía"),
                ]);
            } else {
                if (!Hash::check($password, $tecnico->password)) {
                    return redirect()->back()->withInput(Input::all())->with([
                        "status" => "error",
                        "title" => __("Perfil no actualizado"),
                        "msg" => __("Ha habido un error, la contraseña es incorrecta"),
                    ]);
                }
            }
        }

        if ($tecnico->entitat == auth()->user()->entitat) {
            $this->fillUser($tecnico, $request);
            $ruta = route("admin.tecnico.index");
            if (auth()->user()->rol->name == "tecnico") $ruta = route("admin.tecnico.edit", $tecnico);
            return redirect($ruta)->withInput(Input::all())->with([
                "status" => "success",
                "title" => __("Perfil actualizado"),
                "msg" => __("Se ha actualizado el usuario <strong>:user</strong>", ["user" => $tecnico->fullname()])
            ]);
        } else {
            return redirect()->back()->withInput(Input::all())->with([
                "status" => "error",
                "title" => __("Error"),
                "msg" => __("No tienes permisos para modificar tecnicos que no son de tu entidad")
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, User $tecnico)
    {
        // Passen totes les peticions a ser filles del administrador de la entitat
        $peticions = $this->users->getPeticions($tecnico);
        $peticions = $peticions->each(function($p) {
            $p->author_id = auth()->user()->id;
            $p->update();
        });

        // Eliminem la petició
        $tecnico->delete();

        return redirect(route("admin.tecnico.index"))->with([
            "status" => "success",
            "title" => __("Técnico eliminado"),
            "msg" => __("Has eliminado un tecnico, en caso de ser un error <strong>pongase en contacto con Formacio i Treball</strong>"),
        ]);
    }

    protected function save_custom_fields(User $user, $fields = [], $is_child = false)
    {
        if ($is_child) {
            foreach ($fields as $idx => $subfields) {
                $this->save_custom_fields($user, $subfields);
            }
        } else {
            $order = -1;

            foreach ($fields as $field_id => $field) {
                if ($field_id == 'order') {
                    $order = $field;
                    continue;
                }

                $field_value = empty($field['fields']) ? $field : null;

                if (is_array($field_value)) {
                    $field_value = json_encode($field_value);
                }

                $meta = [
                    'user_id' => $user->id,
                    'custom_field_id' => $field_id,
                    'value' => $field_value,
                    'post_id' => "user"
                ];

                $where = [['user_id', $user->id], ['custom_field_id', $field_id]];

                // Parent_id
                $f_aux = CustomField::find($field_id);
                if (!empty($f_aux->parent_id)) {
                    $meta['parent_id'] = $f_aux->parent_id;
                    $where[] = ['parent_id', $f_aux->parent_id];
                }
                // Order
                if ($order >= 0) {
                    $meta['order'] = $order;
                    $where[] = ['order', $order];
                }


                // Update/Create
                if (UserMeta::where($where)->exists()) {
                    $user_meta = UserMeta::where($where);
                    $user_meta->update($meta);
                } else {
                    UserMeta::create($meta);
                }

                // Save SubFields
                if (!empty($field['fields'])) {
                    $this->save_custom_fields($user, $field['fields'], true);
                }
            }
        }
    }

    protected function fillUser(User $user, Request $request) {
        // Prevent losing session for different reference on current user
        if ($user->id == auth()->user()->id) {
            $user = auth()->user();
        }

        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->role = (isset($request->role)) ? $request->role : $user->rol->name;

        if ( !empty($request->password)) {
            if ($request->password != $request->password_confirmation) {
                return redirect()->back()->with('error', trans('validation.confirmed', ['attribute' => 'password']));
            }

            $user->password = bcrypt($request->password);
        }
        
        $user->update();

        if (isset($request->custom_fields)) {
            $this->save_custom_fields($user, $request->custom_fields);
        }

        return $user;
    }
}
