<?php

namespace App\Modules\FormacioiTreball\Controllers\Admin;

use App\Post;
use Jenssegers\Date\Date;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\FormacioiTreball\Requests\FrontPeticion;

class PeticionController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = request()->all();
        $metas = [];
        $query = [
            'post_type' => 'peticion',
            'orderBy' => [ "col" => "created_at", "dir" => "asc" ],
            'with' => ["author"]
        ];

        if ( isset($data["query"]) ) {
            $text = $data["query"];
            $metas = [
                'metas_or' => ['identificador-de-peticion', 'numero-expediente', 'nombre-usuario', 'dni-nie-passaporte', 'e-mail-usuario', 'telefono'],
                'metas' => [
                    ['telefono', 'LIKE', "%$text%"],
                    ['e-mail-usuario', 'LIKE', "%$text%"],
                    ['nombre-usuario', 'LIKE', "%$text%"],
                    ['numero-expediente', 'LIKE', "%$text%"],
                    ['dni-nie-passaporte', 'LIKE', "%$text%"],
                ],
            ];
        }

        if (auth()->user()->rol->name == "tecnico") {
            $metas["where"] = [['users.id', "=", auth()->user()->id]];
        }

        $peticiones = get_posts(array_merge($query, $metas));


        if ( isset($data["query"]) ) {
            $text = $data["query"];
            $peticiones = $peticiones->merge(get_posts(array_merge($query, [
                "where" => [ ['users.name', 'LIKE', "%$text%"] ]
            ])));
        }
        
        if (!isAdmin(auth()->user())) {
            $peticiones = $peticiones->filter(function($e) {
                return $e->author && $e->author->entitat && $e->author->entitat == auth()->user()->entitat;
            });
        }

        $peticiones = $peticiones->sortByDesc("created_at")->paginate(15);

        
        return view('Front::admin.peticions.index', compact('peticiones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Front::admin.peticions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FrontPeticion $request)
    {
        $title = auth()->user()->fullname() . " - " . Date::now()->format("d/m/y H:i");
        $current = auth()->user();

        if ($current->limit - intval($request->importe) >= 0)
        {
            $post = Post::create([
                "type" => "peticion",
                "status" => "publish",
                "author_id" => auth()->id(),
                "es" => [
                    "title" => $title,
                    "post_name" => str_slug($title),
                ],
                'ca' => [
                    'title' => $title,
                    'post_name' => str_slug($title),
                ],
            ]);

            $json = $this->requestToJson($request);
            $post = $this->setJsonToPost($post, $json);

            if (!isAdmin()) {
                $current->limit -= intval($json->{"importe"});
                $current->update();
            }

            notifyUser($json);

            return view("Front::admin.peticions.peticion-ok")->with([
                "status" => "success",
                "title" => __("Petició realitzada"),
                "msg" => __("Has realitzat correctament una nova petició"),
            ]);
        }

        return back()->withInput()->withErrors([[
            "status" => "error",
            "title" => __("No tienes saldo suficiente"),
            "msg" => __("Vaya, parece que se ha agotado tu saldo"),
        ]]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view("Front::admin.peticions.show");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, Post $peticion)
    {
        return view("Front::admin.peticions.edit", compact("peticion"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FrontPeticion $request, $locale, Post $peticion)
    {
        $json = $this->requestToJson($request);
        $peticion = $this->setJsonToPost($peticion, $json);

        return redirect()->back()->with([
            "status" => "success",
            "title" => __("Petición actualizada"),
            "msg" => __("Se ha actualizado la petición correctamente"),
        ]);        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, Post $peticion)
    {
        $peticion->delete();

        return redirect()->back()->with([
            "status" => "success",
            "title" => __("Petición eliminada"),
            "msg" => __("Has eliminado una petición, en caso de ser un error <strong>pongase en contacto con Formacio i Treball</strong>"),
        ]);
    }

    public function reenviar($locale, Post $post)
    {
    
        $json = $this->getPostToJson($post);
        notifyUser($json);

        return redirect()->back()->with([
            "status" => "success",
            "title" => __("Email Reenviado"),
            "msg" => __("Email reenviado a <strong>:user</strong>", ["user" => $json->{"e-mail-usuario"}]),
        ]);
    }

    private function requestToJson($request)
    {
        $data = $request->all();

        $json = new \stdClass();
        $json->{"identificador"} = generateBarcodeNumber();
        $json->{"numero-expediente"} = $data["expediente"];
        $json->{"nombre-usuario"} = $data["nombre"];
        $json->{"dni-nie-passaporte"} = $data["dni"];
        $json->{"e-mail-usuario"} = $data["email"];
        $json->{"telefono"} = $data["telefono"];
        $json->{"importe"} = $data["importe"];
        $json->{"edad"} = $data["edad"];
        $json->{"genero"} = $data["genero"];
        $json->{"familia"} = $data["familia"];
        $json->{"aportacion"} = $data["aporte"];
        $json->{"data-caducidad"} = Date::parse($data["data-caducidad"])->format("d/m/Y");
        $json->{"identificador-tecnico"} = auth()->user()->fullname();
        $json->{"email-tecnico"} = auth()->user()->email;

        return $json;
    }

    private function getPostToJson($post)
    {
        $json = new \stdClass();
        $json->{"identificador"} = $post->get_field("identificador-de-peticion");
        $json->{"numero-expediente"} = $post->get_field("numero-expediente");
        $json->{"nombre-usuario"} = $post->get_field("nombre-usuario");
        $json->{"dni-nie-passaporte"} = $post->get_field("dni-nie-passaporte");
        $json->{"e-mail-usuario"} = $post->get_field("e-mail-usuario");
        $json->{"telefono"} = $post->get_field("telefono");
        $json->{"importe"} = $post->get_field("importe");
        $json->{"aportacion"} = $post->get_field("aportacion");
        $json->{"data-caducidad"} = $post->get_field("data-caducidad");

        $json->{"identificador-tecnico"} = $post->get_field("identificador-tecnico");
        $json->{"email-tecnico"} = $post->get_field("email-tecnico");

        return $json;
    }

    private function setJsonToPost($post, $json)
    {
        $post->set_field("identificador-de-peticion", $json->{"identificador"});
        $post->set_field("numero-expediente", $json->{"numero-expediente"});
        $post->set_field("nombre-usuario", $json->{"nombre-usuario"});
        $post->set_field("dni-nie-passaporte", $json->{"dni-nie-passaporte"});
        $post->set_field("e-mail-usuario", $json->{"e-mail-usuario"});
        $post->set_field("telefono", $json->{"telefono"});
        $post->set_field("importe", $json->{"importe"});
        $post->set_field("familia", $json->{"familia"});
        $post->set_field("genero", $json->{"genero"});
        $post->set_field("edad", $json->{"edad"});
        $post->set_field("aportacion", $json->{"aportacion"});
        $post->set_field("data-caducidad", $json->{"data-caducidad"});

        $post->set_field("identificador-tecnico", $json->{"identificador-tecnico"});
        $post->set_field("email-tecnico", $json->{"email-tecnico"});

        return $post;
    }
}
