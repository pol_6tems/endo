<?php

namespace App\Modules\FormacioiTreball\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;

class MyHomeController extends HomeController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) return redirect(route("admin.peticion.create"));
        else return redirect(route("login"));
    }
}
