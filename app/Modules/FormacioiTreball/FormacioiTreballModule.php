<?php

namespace App\Modules\FormacioiTreball;

use App\Support\Module;

class FormacioiTreballModule extends Module {

    const MODULE_NAME = 'Formacio i Treball';
    const MODULE_ROUTE_ADMIN = 'admin.formacio';
    const MODULE_ROUTE = 'formacio';
    
}