<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLimitToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('users', 'limit') ) {
            Schema::table('users', function ($table) {
                $table->integer('limit')->after('email')->default(1000);
            });
        }

        if ( !Schema::hasColumn('users', 'entitat') ) {
            Schema::table('users', function ($table) {
                $table->bigInteger('entitat')->after('email')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('users', 'limit') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('limit');
            });
        }

        if ( Schema::hasColumn('users', 'entitat') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('entitat');
            });
        }
    }
}
