<?php

namespace App\Modules\FormacioiTreball\Exports;

use App\Post;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class PostsExport implements FromCollection, WithColumnFormatting, WithMapping, WithCustomCsvSettings, WithHeadings, ShouldAutoSize
{
    public function collection()
    {
        return Post::ofType('peticion')->get();
    }

    public function map($post): array
    {
        $importe = $post->get_field('importe');
        $aportacion = $post->get_field('aportacion');
        $aporte = round($importe * $aportacion/100, 2);
        
        $caducidad = $post->get_field('data-caducidad');
		try {
			$caducidad = \Carbon\Carbon::createFromFormat("d/m/Y", $caducidad)->format("d/m/Y");	
		} catch(\Exception $e) {
			if ($caducidad != "") {
				$caducidad = \Carbon\Carbon::createFromFormat("d/m/Y H:i:s", $caducidad)->format("d/m/Y");
			}
		}

        return [
            $post->get_field('identificador-de-peticion'),
            $post->get_field('importe'),
            // str_replace(".", ",", $aportacion) . "%",
            $post->get_field('id-donant'), // Donant
            $post->get_field('numero-expediente'), // Expedient
            
            $post->get_field('nombre-usuario'), // Nom + Cognoms Usuari
            $post->get_field('dni-nie-passaporte'), // DNI
            $post->get_field('e-mail-usuario'), // Email
            $post->get_field('telefono'), // Teléfono
            $post->created_at->format("d/m/Y"), // Inici Validesa
            $caducidad, // Fi Validesa
            str_replace(".", ",", $aporte),
        ];
    }
    
    public function columnFormats(): array
    {
        return [
            // 'B' => NumberFormat::FORMAT_CURRENCY_EUR,
            // 'I' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            // 'J' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function getCsvSettings(): array
    {
        return [
            'enclosure' => '',
            'delimiter' => ';',
            'use_bom' => true,
        ];
    }

    public function headings(): array
    {
        return [
            "Identificador",
            "Importe",
            // "Aporte %",
            "Donant",
            "Expediente",
            "Usuario",
            "DNI",
            "Email",
            "Teléfono",
            "Inicio Validez",
            "Caducidad",
            "Aportación",
        ];
    }
}