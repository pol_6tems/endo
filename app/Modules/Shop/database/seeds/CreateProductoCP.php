<?php

namespace App\Modules\Shop\database\seeds;

use App\Models\CustomPost;
use App\Models\CustomField;
use Illuminate\Database\Seeder;
use App\Models\CustomFieldGroup;

class CreateProductoCP extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (! CustomPost::whereTranslation('post_type', 'compra-producto')->exists()) {
            CustomPost::create([
                'params' => json_encode('{"imatge":"0","content":"0","author":"0","parent":"0","published":"0"}'),
                'permissions' => json_encode('{"index":"1","create":"1","store":"1","edit":"1","update":"1","destroy":"1","massDestroy":"1","restore":"1","destroy_permanent":"1","get_templates":"1","duplicate":"1"}}'),
                'es' => [
                    'title' => 'Compra Producto',
                    'title_plural' => 'Compra Productos',
                    'post_type' => 'compra-producto',
                    'post_type_plural' => 'compra-producto',
                ]
            ]);
        } else {
            $this->command->info("CP: Compra Producto ya existe");
        }


        if (! CustomPost::whereTranslation('post_type', 'producto')->exists()) {
            CustomPost::create([
                'params' => json_encode('{"imatge":"1","content":"1","author":"0","parent":"0","published":"0"}'),
                'permissions' => json_encode('{"callAction":"0","index":"0","create":"0","store":"0","edit":"0","update":"0","destroy":"0","massDestroy":"0","restore":"0","destroy_permanent":"0","get_templates":"0","duplicate":"0","aprove_pending":"0"}}'),
                'es' => [
                    'title' => 'Producto',
                    'title_plural' => 'Productos',
                    'post_type' => 'producto',
                    'post_type_plural' => 'productos',
                ]
            ]);
        } else {
            $this->command->info("CP: Producto ya existe");
        }

        $cfg_compra = CustomFieldGroup::firstOrCreate([
            'title' => 'Camps',
            'post_type' => 'compra-producto',
            'position' => 'main',
            'order' => 0,
        ]);

        $cfg_producto = CustomFieldGroup::firstOrCreate([
            'title' => 'Propiedades',
            'post_type' => 'producto',
            'position' => 'r-sidebar',
            'order' => 0,
        ]);


        if (! CustomField::where(['cfg_id' => $cfg_producto->id, 'name' => 'precio'])->exists()) {
            CustomField::create([
                'cfg_id' => $cfg_producto->id,
                'title' => 'Precio',
                'name' => 'precio',
                'type' => 'text',
                'params' => json_encode('{"required":"1","placeholder":null,"min_length":null,"max_length":null,"input_type":null}'),
                'order' => 0,
            ]);
        } else {
            $this->command->info("CF: Propiedades ya existe");
        }

        if (! CustomField::where(['cfg_id' => $cfg_producto->id, 'name' => 'subscription'])->exists()) {
            CustomField::create([
                'cfg_id' => $cfg_producto->id,
                'title' => 'Subscription',
                'name' => 'subscription',
                'type' => 'boolean',
                'params' => json_encode('{"required":"0","role":"1","boolean_rule":"487"}'),
                'order' => 1,
            ]);
        } else {
            $this->command->info("CF: Subscription ya existe");
        }

        if (! CustomField::where(['cfg_id' => $cfg_producto->id, 'name' => 'producto'])->exists()) {
            CustomField::create([
                'cfg_id' => $cfg_producto->id,
                'title' => 'Producto',
                'name' => 'producto',
                'type' => 'selection',
                'params' => json_encode('{"required":"0","post_object":"any","role":"1","multiple":"0","choices":null,"max_length":null,"min_length":null,"parent_param":"0","permision":"no_restriction","instructions_param":null}'),
                'order' => 3,
            ]);
        } else {
            $this->command->info("CF: Producto ya existe");
        }

        if (! CustomField::where(['cfg_id' => $cfg_producto->id, 'name' => 'valor'])->exists()) {
            CustomField::create([
                'cfg_id' => $cfg_producto->id,
                'title' => 'Valor',
                'name' => 'valor',
                'type' => 'text',
                'params' => json_encode('{"required":"1","placeholder":null,"min_length":null,"max_length":null,"input_type":null}'),
                'order' => 4,
            ]);
        } else {
            $this->command->info("CF: Valor ya existe");
        }

        if (! CustomField::where(['cfg_id' => $cfg_producto->id, 'name' => 'function'])->exists()) {
            CustomField::create([
                'cfg_id' => $cfg_producto->id,
                'title' => 'Function',
                'name' => 'function',
                'type' => 'Shop.function',
                'params' => json_encode('{}'),
                'order' => 5,
            ]);
        } else {
            $this->command->info("CF: Function ya existe");
        }

        if (! CustomField::where(['cfg_id' => $cfg_compra->id, 'name' => 'duration'])->exists()) {
            CustomField::create([
                'cfg_id' => $cfg_compra->id,
                'title' => 'Duration',
                'name' => 'duration',
                'type' => 'selection',
                'params' => json_encode('{"required":"0","post_object":"any","role":"1","multiple":"0","choices":"P1Y: 1 A\u00f1o\r\nP6M: 6 Meses","max_length":null,"min_length":null,"parent_param":"0","permision":"no_restriction","instructions_param":"Ninguna => Indefinido"}'),
                'order' => 0,
            ]);
        } else {
            $this->command->info("CF: Duration ya existe");
        }

        if (! CustomField::where(['cfg_id' => $cfg_compra->id, 'name' => 'post'])->exists()) {
            CustomField::create([
                'cfg_id' => $cfg_compra->id,
                'title' => 'Post',
                'name' => 'post',
                'type' => 'selection',
                'params' => json_encode('{"required":"1","post_object":"7","role":"1","multiple":"0","choices":null,"max_length":null,"min_length":null,"parent_param":"0","permision":"no_restriction","instructions_param":null}'),
                'order' => 1,
            ]);
        } else {
            $this->command->info("CF: Post ya existe");
        }

        if (! CustomField::where(['cfg_id' => $cfg_compra->id, 'name' => 'compra'])->exists()) {
            CustomField::create([
                'cfg_id' => $cfg_compra->id,
                'title' => 'Compra',
                'name' => 'compra',
                'type' => 'selection',
                'params' => json_encode('{"required":"1","post_object":"68","role":"1","multiple":"0","choices":null,"max_length":null,"min_length":null,"parent_param":"0","permision":"no_restriction","instructions_param":null}'),
                'order' => 2,
            ]);
        } else {
            $this->command->info("CF: Compra ya existe");
        }

        if (! CustomField::where(['cfg_id' => $cfg_compra->id, 'name' => 'valor'])->exists()) {
            CustomField::create([
                'cfg_id' => $cfg_compra->id,
                'title' => 'Valor',
                'name' => 'valor',
                'type' => 'number',
                'params' => json_encode('{"required":"1","default_value":null,"role":"1","min_length":null,"max_length":null}'),
                'order' => 3,
            ]);
        } else {
            $this->command->info("CF: Valor ya existe");
        }
    }
}
