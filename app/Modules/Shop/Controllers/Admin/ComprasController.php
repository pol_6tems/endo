<?php

namespace App\Modules\Shop\Controllers\Admin;

use App\Post;
use Illuminate\Support\Facades\DB;
use App\Modules\EncuentraTuRetiro\Models\Purchase;
use App\Modules\EncuentraTuRetiro\Controllers\Admin\PurchasesController;

class ComprasController extends PurchasesController
{

    const POST_TYPE = 'compra-producto';
    const POST_TYPE_PLURAL = 'Compras';
    const SECTION_ROUTE = 'admin.compras';
    const SECTION_TITLE = 'Compras';


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $query = Purchase::select(DB::raw('purchases.*'))
            ->join('posts', 'posts.id', '=', 'purchases.post_id')
            ->where('posts.type', $this->purchase_item)
            ->orderBy('created_at', 'DESC')
            ->with('post.author', 'user');


        $trashedQuery = clone $query;

        $trashed = $trashedQuery->onlyTrashed()->get();

        if (request('status') == 'trash') {
            $purchases = $trashed;
        } else {
            $purchases = $query->paginate(15);
        }

        $user = auth()->user();

        if ($user->rol->level < Post::ROL_LEVEL_LIMIT_REVISIO) {
            $purchases = $purchases->filter(function ($purchase) use ($user) {
                return $purchase->post->author_id == $user->id;
            });

            $trashed = $trashed->filter(function ($purchase) use ($user) {
                return $purchase->post->author_id == $user->id;
            });
        }
        
        return view($this->admin_view_path . 'index', [
            'items' => $purchases,
            'num_trashed' => $trashed->count()
        ]);
    }

    public function callAction($method, $parameters)
    {
        $this->post_type = self::POST_TYPE;
        $this->post_type_plural = self::POST_TYPE_PLURAL;
        $this->section_route = self::SECTION_ROUTE;
        $this->section_title = self::SECTION_TITLE;

        $this->purchase_item = 'producto';
        $this->admin_view_path = 'admin.compras-';

        return parent::callAction($method, $parameters);
    }
}