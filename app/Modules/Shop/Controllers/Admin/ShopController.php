<?php

namespace App\Modules\Shop\Controllers\Admin;

use App\Post;
use App\User;
use DateInterval;
use Carbon\Carbon;
use Sermepa\Tpv\Tpv;
use App\Models\CustomPost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Modules\Shop\Models\Subscription;
use App\Modules\Shop\Service\PaymentGateway;
use App\Http\Controllers\Admin\PostsController;
use App\Modules\EncuentraTuRetiro\Models\Purchase;

class ShopController extends PostsController
{
    protected $post_type = 'producto';
    protected $post_type_plural = 'productos';
    protected $section_route = 'admin.products';
    protected $def_status = 'publish';
    protected $subforms = [];
    protected $steps = [
        'Selección del producto',
        'Resumen y Confirmación de los datos de facturación',
        'Confirmación de la Reserva'
    ];

    public function __construct() {
        parent::__construct();
        
        View::share('post_type_title', 'Producto');
        View::share('steps', $this->steps);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function shop() {
        $products = Post::where('type', 'producto')->get();

        return view('admin.index', compact('products'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, Post $product) {
        $currentUser = Auth::user();
        $step = 0;

        if ($object = $product->get_field('producto')) {
            $cp = CustomPost::find($object->id);
            $objects = Post::ofType($cp->post_type)
                                ->publish()
                                ->owner(auth()->user())
                                ->get();
            
            return view('admin.show', compact('product', 'cp', 'objects', 'step'));
        }

        return view('admin.show', compact('product', 'step'));
    }

    public function buy($locale, Post $product) {
        $data = request()->all();
        $object = null;
        $step = 1;

        if (isset($data['related_object'])) {
            $object = Post::find($data['related_object']);
        }

        return view('admin.show', compact('product', 'step', 'object'));
    }

    public function pay($locale, Post $offer, PaymentGateway $paymentGateway) {
        $data = request()->all();
        $amount = intval($offer->get_field('precio'));

        if (array_key_exists('related_object', $data)) {
            $related_object = Post::find($data['related_object']);
        }

        try {
            $orderId = array_key_exists('external_order_id', $data) ? $data['external_order_id'] : time();
            $paymentGateway->setTPV($offer, $related_object, $orderId, $amount);

            $paymentGateway->run();
        } catch (\Sermepa\Tpv\TpvException $e) {
            echo $e->getMessage();
        }
    }


    /**
     * TODO?
     *
     * @param $locale
     * @param $product_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function formOK($locale, Post $offer, Post $product)
    {
        $data = $this->getOrderData();

        if (!$offer->id) {
            abort(404);
        }

        $user = auth()->user();

        if (!$data || $data['user_id'] != $user->id) {
            abort(403);
        }

        $purchase = $this->confirmBuy($data, $user, $offer, $product);

        return redirect()->route('admin.products.buyed', [
            'locale' => $locale,
            'offer' => $offer->id,
            'product' => $product->id,
            'order_id' => $purchase->order_id,
        ]);
    }

    /**
     * TODO?
     *
     * @param $locale
     * @param $product_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function formKO($locale, $product_id)
    {
        return redirect(route('admin.products.show', $product_id))->with('compra', false);
    }

    private function confirmBuy($data, User $user, Post $offer, Post $product)
    {
        $user = $user ?? User::find($data['user_id']); 
        
        $purchase = Purchase::firstOrNew([
            'external_order_id' => $data['order_id'],
            'user_id' => $user->id,
            'post_id' => $offer->id,
        ]);
        
        // Ja existia i s'ha completat
        if ($purchase->isConfirmed()) {
            return $purchase;
        }

        $total = $offer->get_field('precio');
        $updateParams = [
            'status' => Purchase::RESERVATION_STATUS_CONFIRMED,
            'mensaje' => '',
            'acompanantes' => '',
            'cancelacion' => '',
            'total' => $total,
            'payed' => $total,
            'left' => 0,
            'dharmas_spent' => 0,
            'method' => $data['method'],
        ];

        unset($data['method']);
        unset($data['user_id']);
        unset($data['order_id']);
        unset($data['product_id']);

        $updateParams['extra_data'] = json_encode($data);

        if ($purchase->exists) {
            $purchase->update($updateParams);
        } else {
            $purchase = Purchase::create(array_merge([
                'external_order_id' => $purchase->external_order_id,
                'user_id' => $user->id,
                'post_id' => $purchase->post_id
            ], $updateParams));
        }

        if ($offer->get_field('subscription')) {
            $duration = $offer->get_field('duration');
            if (is_numeric($duration)) $duration = "P99Y";
            
            $subscription = Subscription::firstOrNew([
                ['type' => $offer->type,'post_id' => $product->id],
                [
                    'purchase_id' => $purchase->id,
                    'user_id' => $user->id,
                    'duration' => $duration,
                    'start_at' => new Carbon(),
                    'end_at' => (new Carbon())->add(new DateInterval($duration))
                ]
            ]);

            if (! $subscription->exists ) {
                $end = new Carbon($subscription->end_at);
                $subscription->end_at = $end->add(new DateInterval($duration));
            }
            $subscription->save();
        } else {
            // Altre Tipus de compra amb Valor
            $slug = str_slug($offer->post_name . '-' . $product->post_name);
            if (! $post = Post::whereTranslation('post_name', $slug)->first()) {
                $post = Post::create([
                    'type' => 'compra-producto',
                    'status' => 'publish',
                    'es' => [
                        'title' => $offer->title . ' - ' . $product->title,
                        'post_name' => $slug,
                    ],
                    'parent_id' => 0,
                    'author_id' => $user->id,
                ]);
    
                $post->set_field('compra', $offer->id);
                $post->set_field('post', $product->id);
            }

            if (is_null($post->get_field('valor'))) $post->set_field('valor', $offer->get_field('valor'));
            else $post->set_field('valor', (intval($post->get_field('valor')) + intval($offer->get_field('valor'))));
        }

        if (isProEnv()) {
            try {
                // $this->purchaseEmails($purchase);
            } catch (\Exception $e) {
                // Do nothing
            }
        }

        return $purchase;
    }

    /**
     * @return mixed
     */
    private function getOrderData() {
        try {
            $redsys = new Tpv();
            $key = env('REDSYS_KEY');

            $parameters = $redsys->getMerchantParameters(request('Ds_MerchantParameters'));
            $DsResponse = $parameters["Ds_Response"];
            $DsResponse += 0;
            $orderToken = request('orderToken');

            if ($redsys->check($key, request()->all()) && $DsResponse <= 99 && $orderToken) {
                //acciones a realizar si es correcto, por ejemplo validar una reserva, mandar un mail de OK, guardar en bbdd o contactar con mensajería para preparar un pedido
                return endoDecrypt($orderToken);
            }
        } catch (\Sermepa\Tpv\TpvException $e) {
            abort(400, $e->getMessage());
        }
    }

    public function comprado($locale, $offer, $product)
    {
        $offer = Post::find($offer);
        $product = Post::find($product);

        $orderId = request('order_id');
        $externalOrderId = request('external_order_id');
        
        if ((!$orderId && !$externalOrderId) || !$product->id) {
            abort(400);
        }
        
        $query = Purchase::where('user_id', auth()->user()->id)->where('post_id', $offer->id);
        $query = ($orderId) ? $query->where('order_id', $orderId) : $query->where('external_order_id', $externalOrderId);
        $purchase = $query->first();

        if (!$purchase) {
            abort(400);
        }

        if ($offer->get_field('subscription')) {
            $purchase->order_id = get_letra_from_type('subscription') . $purchase->order_id;
        } else {
            $purchase->order_id = get_letra_from_type('buyable') . $purchase->order_id;
        }

        $purchase->save();
        
        $product->set_field('destacado', 1);
        
        $step = 2;
        return view('admin.show', compact('offer', 'product', 'step'));
    }

    public function compras() {
        $items = Subscription::with(['post'])->where('user_id', Auth::user()->id)->get();
        $status = 'publish';
        $num_trashed = 0;
        $num_pending = 0;
        $num_draft = 0;

        $post_type_plural = 'Compras';

        return view('admin.list', compact('items', 'status', 'num_trashed', 'num_pending', 'num_draft', 'post_type_plural'));
    }
}
