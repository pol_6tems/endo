<?php
Route::any('products/buy/{offer}/noti', 'Admin\ShopController@notification')->name('admin.products.buy.noti');

Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    Route::get('products/buy/{offer}-{product}/ok', 'Admin\ShopController@formOK')->name('admin.products.buy.ok');
    Route::get('products/buy/{offer}-{product}/ko', 'Admin\ShopController@formKO')->name('admin.products.buy.ko');
    Route::get('products/buyed/{offer}-{product}', 'Admin\ShopController@comprado')->name('admin.products.buyed');
    Route::get('shop', "Admin\ShopController@shop")->name('admin.products.shop');
    Route::get('products/compras', "Admin\ShopController@compras")->name('admin.products.compras');

    Route::post('products/buy/{offer}', "Admin\ShopController@pay", ['as' => 'admin'])->name('admin.products.pay');
    Route::post('products/confirm/{product}', "Admin\ShopController@buy", ['as' => 'admin'])->name('admin.products.confirm');
    Route::resource('products', "Admin\ShopController", ['as' => 'admin']);

    Route::resource('compras', 'Admin\ComprasController', ["as" => "admin"]);
    Route::put("compras/{id}/restore", "Admin\ComprasController@restore")->name("admin.compras.restore");
});