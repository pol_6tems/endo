<?php


namespace App\Modules\Shop\Providers;


use App\Post;
use Illuminate\Support\ServiceProvider;
use App\Modules\Shop\Observers\PostObserver;
use App\Modules\Shop\Service\PaymentGateway;
use Sermepa\Tpv\Tpv;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        Post::observe(PostObserver::class);

        $this->app->singleton(PaymentGateway::class, function ($app) {
            return new PaymentGateway(new Tpv());
        });
    }


    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {

    }
}