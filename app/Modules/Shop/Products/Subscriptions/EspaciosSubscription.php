<?php

namespace App\Modules\Shop\Products\Subscriptions;

class EspaciosSubscription extends Subscription {
    protected $id = "espacio"; // Id
    protected $title = "Subscripción de Espacios"; // Id
    protected $related_object = "espacio"; // Id
}