<?php

namespace App\Modules\Shop\Models;

use App\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\EncuentraTuRetiro\Models\Purchase;

class Subscription extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $guarded = [];

    public function post() {
        return $this->belongsTo(Post::class);
    }

    public function purchase() {
        return $this->belongsTo(Purchase::class, 'purchase_id', 'external_order_id');
    }
}
