<?php

namespace App\Modules\Shop;

use App\Support\Module;

class ShopModule extends Module {

    const MODULE_NAME = 'Shop';
    const MODULE_ROUTE_ADMIN = 'admin.shop';
    const MODULE_ROUTE = 'shop';
    
}