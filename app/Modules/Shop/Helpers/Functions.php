<?php

use App\Post;
use App\Modules\Shop\Controllers\FunctionsController;


add_action('after_locale', 'load');
function load() {
    $productos = Post::ofType('producto')->get();
    foreach($productos as $producto) {
        if ($function = $producto->get_field('function')) {
            FunctionsController::$function();
        }
    }
}


function aplicarDestacados($retiros) {
    $compres = Post::with('metas')->ofType('compra-producto')->get();
    
    $retiros = $retiros->map(function($value, $key) use ($compres) {
        foreach($compres as $compra) {
            $post = $compra->get_field('post');

            if ($post->id == $value->id) {
                $post_meta = $compra->metas->firstWhere('custom_field_id', 505);
                if ($post_meta) {
                    if ($post_meta->value > 0) {
                        $post_meta->value -= 1;
                        $post_meta->save();
                        return;
                    } else {
                        $value->set_field('destacado', 0);
                    }
                }
            }
        }
    });
}
