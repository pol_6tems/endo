<?php

namespace App\Modules\Shop\Observers;

use App\Post;

class PostObserver
{

    public function retrieved(Post $post) {
        if ($post->type == 'producto') {
            return true;
        }
    }
}