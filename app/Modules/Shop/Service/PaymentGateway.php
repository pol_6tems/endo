<?php

namespace App\Modules\Shop\Service;

use App\Post;
use stdClass;
use Sermepa\Tpv\Tpv;

class PaymentGateway
{
    protected $tpv;

    protected $key;
    protected $env;
    protected $term;

    protected $product;
    protected $targeta;
    protected $token;

    public function __construct(Tpv $tpv)
    {
        $this->tpv = $tpv;
        $this->tpv->setMerchantcode('185006673');
        $this->tpv->setVersion('HMAC_SHA256_V1');
        $this->tpv->setTradeName('ENCUENTRA TU RETIRO');
        $this->tpv->setTitular('SERGI ARRIBAS TORRAS');

        $this->key = env('REDSYS_KEY');
        $this->env = env('REDSYS_ENV');
        $this->term = env('REDSYS_TERMINAL');
    }

    public function setTargeta($number, $expiracion, $cvv) {
        $targeta = new stdClass();
        $targeta->number = $number;
        $targeta->expiracion = $expiracion;
        $targeta->cvv = $cvv;
        
        // Redsys pago por web service
        if ($targeta->number && $targeta->expiracion && $targeta->cvv) {
            $this->tpv->setPan($targeta->number); //Número de la tarjeta
            $this->tpv->setExpiryDate($targeta->expiracion); //AAMM (año y mes)
            $this->tpv->setCVV2($targeta->cvv); //CVV2 de la tarjeta
        }

        $this->targeta = $targeta;
    }

    public function setTPV(Post $offer, Post $product, $orderId, $amount = 0) {
        $this->offer = $offer;
        $this->product = $product;

        $this->tpv->setAmount($amount);
        $this->tpv->setOrder($orderId);

        // No funciona
        $this->tpv->setCurrency(978);

        $this->tpv->setTransactiontype('0');
        $this->tpv->setTerminal($this->term);
        $this->tpv->setMethod('C'); // Solo pago con tarjeta, no mostramos iupay

        $this->token = endoEncrypt([
            'user_id' => auth()->user()->id,
            'order_id' => $orderId,
            'product_id' => $offer->id,
            'user_currency' => userCurrency(true),
            'method' => 'redsys',
            'value' => $offer->get_field('valor'),
        ]);

        $this->tpv->setNotification(route('admin.products.buy.noti', ['offer' => $offer->id, 'product' => $product->id, 'orderToken' => $this->token]));
        $this->tpv->setUrlOk(route('admin.products.buy.ok', ['offer' => $offer->id, 'product' => $product->id, 'orderToken' => $this->token]));
        $this->tpv->setUrlKo(route('admin.products.buy.ko', ['offer' => $offer->id, 'product' => $product->id])); //Url KO
    }

    public function getToken() {
        return $this->token;
    }


    public function setNotification($url) {
        $this->tpv->setNotification($url);
    }

    public function setUrlOk($url) {
        $this->tpv->setUrlOk($url);
    }

    public function setUrlKo($url) {
        $this->tpv->setUrlKo($url);
    }

    public function run() {
        $this->tpv->setEnvironment($this->env); //Entorno test
        $signature = $this->tpv->generateMerchantSignature($this->key); //
        $this->tpv->setMerchantSignature($signature); //
        $this->tpv->executeRedirection();
    }
}