<div class="flex-row">
    <article class="card">
        <div class="card-header">
            <span class="purchase-check"><i class="material-icons">done</i></span>
            <h4 class="card-title ml-5">@Lang('Purchase Completed')</h4>
        </div>
        <div class="card-body">
            <div class="success-panel">
                <p>
                    @if ($product->get_field('subscription'))
                        @php($interval = ($product->get_field('duration') != "0") ? new DateInterval($product->get_field('duration')) : __('Forever'))

                        @Lang('Gracias, el :object_type  <strong>:object_title</strong> ha iniciado una subscripcion :subscription_long', [
                            'object_type' => strtolower($offer->customPostTranslations->first()->title),
                            'object_title' => $offer->title,
                            'subscription_long' => ($interval instanceof DateInterval) ? $interval->format('de %y año/s, %m mes/es y %d dia/s') : $interval
                        ])
                    @else
                        @Lang('Gracias, has comprado correctamente el :object_type  <strong>:object_title</strong>', [ 
                            'object_type' => strtolower($offer->customPostTranslations->first()->title),
                            'object_title' => $offer->title
                        ])
                    @endif
                </p>
            </div>
            <a class="btn" href="{{ route('admin.products.shop') }}">Volver</a>
        </div>
    </article>            
</div>
