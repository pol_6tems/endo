<form action="{{ route('admin.products.confirm', $product->id) }}" method="POST">
        @csrf
        <div class="flex-row">
            <div class="col-md-4">
                @if($product->media())
                    <div class="product-image">
                        <img src="{{ $product->media()->get_thumbnail_url() }}" alt="">
                    </div>
                @endif
            </div>
            <div class="col-md-8 content">
                <article class="card">
                    <div class="card-header">
                        <h1 class="card-title">{{ $product->title }}</h1>
                        @if ($preu = $product->get_field('precio'))
                        <h2 class="card-price">@Lang('Price'): <span>{{ $preu }} €</span></h2>
                            @if ($product->get_field('subscription'))
                                @php($interval = ($product->get_field('duration') != "0") ? new DateInterval($product->get_field('duration')) : __('Forever'))
                                
                                <h2 class="card-price">@Lang('Type'): <span>@Lang('Subscription')</span></h2>
                                <h2 class="card-price">@Lang('Duration'): <span> {{ ($interval instanceof DateInterval) ? $interval->format('%y año/s, %m mes/es y %d dia/s') : $interval }} </span></h2>
                            @endif
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="product-description">
                            {!! $product->description !!}
                        </div>
                        <div class="properties">
                            @isset ($objects)
                                <h3>@Lang('Select the :object', ['object' => strtolower($cp->title)])</h3>
                                <select name="related_object" class="selectpicker" data-live-search="true">
                                    @foreach ($objects as $object)
                                        <option value="{{ $object->id }}">{{ $object->title }}</option>
                                    @endforeach
                                </select>
                            @endisset
                        </div>
                        <div class="actions">
                            <input type="submit" class="btn btn-primary" value="@Lang('Buy')" />
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </form>