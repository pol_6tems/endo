@php($user = Auth::user())
<form action="{{ route('admin.products.pay', $product->id) }}" method="POST">
    @csrf
    <div class="flex-row">
        <div class="col-md-8">
            <article class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        @Lang('Billing Data')
                        <a style="float: right;" class="btn btn-primary btn-sm" href="{{ route('admin.users.edit', $user->id)}}">Editar</a>
                    </h4>
                </div>
                <div class="card-body facturacion">
                    <div class="row">
                        <div class="facturacion-title col-md-12">
                            <p>@Lang('For security reasons you are not allowed to modify your personal data on this page. Please, if you need to modify your personal data do it on your personal profile.')</p>
                            <h4>@Lang('Personal Information')</h4>
                            <hr>
                        </div>
                        <div class="input-field col-md-6">
                            <label for="name">@Lang('Name')</label>
                            <input type="text" class="form-control readonly" name="name" id="name" value="{{ $user->name }}" required />
                        </div>
                        <div class="input-field col-md-6">
                            <label for="apellido">@Lang('Lastname')</label>
                            <input type="text" class="form-control readonly" name="apellido" id="apellido" value="{{ $user->lastname }}" required />
                        </div>
                        <div class="input-field col-md-6">
                            <label for="email">@Lang('Email')</label>
                            <input type="text" class="form-control readonly" name="email" id="email" value="{{ $user->email }}" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="facturacion-title col-md-12">
                            <h4>@Lang('Fiscal Information')</h4>
                            <hr>
                        </div>
                        <div class="input-field col-md-6">
                            <label for="nombre-fiscal">@Lang('Fiscal Name')</label>
                            <input type="text" class="form-control readonly" name="nombre_fiscal" id="nombre-fiscal" value="{{ $user->get_field('nombre-fiscal') }}" required />
                        </div>
                        <div class="input-field col-md-6">
                            <label for="direccion-fiscal">@Lang('Fiscal Address')</label>
                            <input type="text" class="form-control readonly" name=direccion_fiscal" id="direccion-fiscal" value="{{ $user->get_field('direccion-fiscal') }}" required />
                        </div>
                        <div class="input-field col-md-6">
                            <label for="localidad-fiscal">@Lang('Fiscal Location')</label>
                            <input type="text" class="form-control readonly" name="" id="localidad_fiscal" value="{{ $user->get_field('localidad-fiscal') }}" required />
                        </div>
                        <div class="input-field col-md-6">
                            <label for="pais-fiscal">@Lang('Fiscal Country')</label>
                            <input type="text" class="form-control readonly" name=localidad_fiscal"" id="pais-fiscal" value="{{ $user->get_field('pais-fiscal') }}" required />
                        </div>
                        <div class="input-field col-md-6">
                            <label for="num-id-fiscal">@Lang('Fiscal ID')</label>
                            <input type="text" class="form-control readonly" name="num_id_fiscal" id="num-id-fiscal" value="{{ $user->get_field('numero-identificacion') }}" required />
                        </div>
                    </div>
                </div>
            </article>
            @isset($object)
            <input type="hidden" name="related_object" value="{{ $object->id }}">
            <article class="card">
                <div class="card-header">
                    <div class="card-title">{{ $object->customPostTranslations->first()->title }}</div>
                </div>
                <div class="card-body">
                    <div class="object">
                        <div class="obj-lft">
                            @if ($object->media())
                                <img src="{{ $object->media()->get_thumbnail_url("medium") }}" alt="">
                            @endif
                        </div>
                        <div class="obj-rgt">
                            <h3>{{ $object->title }}</h3>
                            <p>{{ get_excerpt($object->description, 15) }}</p>
                            <a href="{{ route('admin.posts.edit', ['post' => $object->id, 'post_type' => $object->type]) }}" class="btn btn-primary btn-sm" target="_blank">@Lang('Check')</a>
                        </div>
                    </div>
                </div>
            </article>
            @endisset
        </div>
        <div class="col-md-4 content">
            <article class="card">
                @if($product->media())
                    <img class="card-img-top" src="{{ $product->media()->get_thumbnail_url() }}" alt="">
                @endif
                <div class="card-header">
                    <h1 class="card-title">{{ $product->title }}</h1>
                </div>
                <div class="card-body">
                    <div class="product-description">
                        {!! $product->description !!}
                    </div>
                    <hr>
                    @if ($preu = $product->get_field('precio'))
                        <h2 class="card-price detail">@Lang('Total'): <span>{{ $preu }} €</span></h2>
                    @endif
                    <hr>
                </div>
                <div class="card-footer">
                    <div class="checkbox">
                        <div class="form-check">
                            <input id="rgpd" class="form-check-input" type="checkbox" value="" required>
                            <label for="rgpd" class="form-check-label">
                                @lang('Acepto los <a target="_blank" href=":url">Terminos y Condiciones de Servicio</a>', ['url' => get_page_url('terminos-y-condiciones-generales-de-contratacion')])
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <input type="submit" class="btn btn-primary" value="@Lang('Buy')" />
                    </div>
                </div>
            </article>
        </div>
    </div>
</form>
@section('scripts')
<script src="{{asset('Modules/Shop/js/creditly.js') }}"></script>
<script>
$(".readonly").keydown(function(e){
    e.preventDefault();
});

$(document).ready(function(){
	$('.toggle-btn').click(function(){
		var getId = $(this).parents('.divider').attr('id');
        var divider = $('#'+getId);
        divider.toggleClass('open');

        var openTxt = $(this).data('open-txt');
        var closeTxt = $(this).data('closed-txt');

        if (typeof openTxt !== 'undefined' && typeof closeTxt !== 'undefined') {
            $(this).html(divider.hasClass('open') ? openTxt : closeTxt)
        }
	});
	//card function	
	// For the blue theme
	var blueThemeCreditly = Creditly.initialize(
	'.expiration-month-and-year',
	'.credit-card-number',
	'.security-code'
	);

	$(".creditly-blue-theme-submit").click(function(e) {
	    e.preventDefault();
	    var output = blueThemeCreditly.validate();
	});
	
	//card Input
	$('.credit-card-number').keyup(function(){
		var getValue = $('.credit-card-number').val();					
		var splitText = getValue.split(" ");
		console.log(splitText);
		var getlength= 0;
		if(splitText.length > 0 || splitText.length != undefined)
		{
			getlength = splitText.length;
		}
		console.log(getlength);
		$('.card-num span').empty();
		for(var i=0; i<=getlength; i++)
		{
			console.log(splitText[i])
			$('.card-num span:nth-child('+(i+1)+')').text(splitText[i]);
		}				
	});
	
	//Changing month
	$('.expiration-month-and-year').keyup(function(){
		var getValue = $(this).val();		
		$('.month-number').empty();								
		$('.month-number').text(getValue);								
	});
});
</script>
@endsection