@extends('Admin::layouts.admin')

@section('section-title')
    @lang($section_title)
@endsection

@section('content')
    @if ($item->status != \App\Modules\EncuentraTuRetiro\Models\Purchase::RESERVATION_STATUS_CONFIRMED)
        <div class="row">
            <div class="col-lg-12">
                <article class="card">
                    <div class="card-header card-header-primary card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">library_books</i>
                        </div>
                        <h4 class="card-title">
                            @lang('Status')
                        </h4>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal" action='{{ route($section_route . '.update', $item->id) }}' method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">

                            <div class="form-group">
                                <select name="status"
                                        class="selectpicker complaint-status"
                                        data-style="btn btn-primary"
                                        data-live-search="true">
                                    @foreach($statuses as $key => $status)
                                        <option value="{{ $key }}" @if ($item->status == $key) selected @endif>{{ $status }}</option>
                                    @endforeach
                                </select>
                                <input type="submit" class="btn btn-next btn-fill btn-primary btn-wd pull-right" name="next" value="@lang('Update')" />
                            </div>
                        </form>
                    </div>
                </article>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <article class="card pm">
                <div class="card-header card-header-primary card-header-icon">
                    <h4 class="card-title">@lang('Detalles')</h4>
                </div>

                <div class="card-body pb-5">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>@lang('Reserva')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Id'):</dt><dd>{{ $item->order_id ? $item->order_id : 'N/A' }}</dd>

                                @if ($item->status == \App\Modules\EncuentraTuRetiro\Models\Purchase::RESERVATION_STATUS_CONFIRMED)
                                    <dt>@lang('Id pago')</dt><dd>{{ $item->external_order_id }}</dd>
                                @endif

                                <dt>@lang('Status'):</dt><dd>{!! $item->present()->statusColored !!}</dd>
                                <dt>@lang('Created'):</dt><dd>{{ $item->created_at->format('d/m/Y H:m:s') }}</dd>
                                <dt>@lang('Updated'):</dt><dd>{{ $item->updated_at->format('d/m/Y H:m:s') }}</dd>
                            </dl>
                        </div>

                        @if ($item->post->type == 'producto')
                            @php($interval = new DateInterval($item->post->get_field('duration')))
                            @php($avui = new Date())
                            <div class="col-md-6">
                                <h5>@lang('Producto')</h5>
                                <dl class="dl-horizontal">
                                    @if ($item->post->get_field('subscription'))
                                        @php($related = $item->subscription->post)
                                        <dt>@lang('Tipo'):</dt><dd>{{ ($item->post->get_field('subscription')) ? 'Suscripción':'Producto' }}</dd>
                                        <dt>@lang('Producto Suscrito'):</dt>
                                            <dd><a href="{{ route('admin.posts.edit', ['post_type' => $related->type, 'post' => $related->id]) }}">{{ $related->title }}</a></dd>
                                    @endif
                                    <dt>@lang('Producto'):</dt><dd><a href="{{ $item->post->get_url() }}" target="_blank">{{ $item->post->title }}</a> </dd>
                                    <dt>@lang('Finalización'):</dt><dd>{{ $avui->add($interval)->diffForHumans() }}</dd>
                                </dl>
                            </div>
                        @else
                            <div class="col-md-6">
                                <h5>@lang('Retiro')</h5>
                                <dl class="dl-horizontal">
                                    <dt>@lang('Retiro'):</dt><dd><a href="{{ $item->post->get_url() }}" target="_blank">{{ $item->post->title }}</a> </dd>
                                    @if ($item->habitacio)
                                    <dt>@lang('Habitacion'):</dt><dd>{{ $item->habitacio->title }}</dd>
                                    @endif
                                    <dt>@lang('Organizador')</dt><dd>{{ $item->post->author->fullname() }}</dd>
                                </dl>
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5>@lang('Cliente')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Name'):</dt><dd>{{ $item->user->fullname() }}</dd>
                                <dt>@lang('Email'):</dt><dd>{{ $item->user->email }}</dd>
                                <dt>@lang('Fechas'):</dt><dd>{{ $item->present()->dates }}</dd>
                                <dt>@lang('Plazas'):</dt><dd>{{ $item->present()->people }}</dd>
                                <dt>@lang('Teléfono'):</dt><dd>{!! $item->present()->extraPhone !!}</dd>
                                <dt>@lang('Edad'):</dt><dd>{!! $item->present()->extraAge !!}</dd>
                                <dt>@lang('Ciudad'):</dt><dd>{!! $item->present()->extraCity !!}</dd>
                                <dt>@lang('País'):</dt><dd>{!! $item->present()->extraCountry !!}</dd>
                            </dl>
                        </div>

                        <div class="col-md-6">
                            <h5>@lang('Pago')</h5>
                            <dl class="dl-horizontal">
                                <dt>@lang('Método')</dt><dd>{{ $item->method ? ucfirst($item->method): 'Redsys' }}</dd>
                                <dt>@lang('Depósito'):</dt><dd>{{ $item->present()->payedCurrency }}</dd>
                                <dt>@lang('Total')</dt><dd>{{ $item->present()->totalCurrency }}</dd>
                            </dl>
                        </div>
                    </div>

                </div>
            </article>
        </div>
    </div>
@endsection