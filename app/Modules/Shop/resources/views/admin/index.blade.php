@extends('Admin::layouts.admin')

@section('styles')
<link href="{{asset('Modules/Shop/css/shop.css')}}" rel="stylesheet">
@endsection

@section('section-title')
    @Lang('Shop')
@endsection

@section('content')
    <div style="text-align: right;">
        <a href="{{ route('admin.products.create') }}" class="btn btn-primary btn-sm">@Lang('Add')</a>
    </div>
    <div class="row">
        @foreach ($products as $product)
            <div class="col-md-4">
                <div class="rotating-card-container">
                    <div class="card card-rotate card-background">
                        <div class="front front-background" style="background-image:url('{{ $product->media() ? $product->media()->get_thumbnail_url("medium") : '' }}');">
                            <div class="card-body">
                                <h6 class="card-category">{{ ucfirst($product->get_field('producto')->post_type) }}</h6>
                                <h3 class="card-title">{{ $product->title }}</h3>
                                <p class="card-description">
                                        {{ get_excerpt($product->description, 60) }}
                                </p>
                            </div>
                        </div>
            
                        <div class="back back-background" style="background-image: url('{{ $product->media() ? $product->media()->get_thumbnail_url("medium") : '' }}');">
                            <div class="card-body">
                                <h5 class="card-title">
                                    {{ $product->title }}
                                </h5>
                                <p class="card-description">{{ get_excerpt($product->description, 10) }}</p>
                                <div class="footer justify-content-center">
                                    <a href="{{ route('admin.products.show', $product->id) }}" class="btn btn-primary">@Lang('Comprar')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection