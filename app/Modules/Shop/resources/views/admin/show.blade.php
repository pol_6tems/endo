@extends('Admin::layouts.admin')

@section('styles')
<link href="{{asset('Modules/Shop/css/shop.css')}}" rel="stylesheet">
@endsection

@section('section-title')
    @Lang('Shop')
@endsection

@section('content')
<div class="wizard">
    <ul>
        @foreach ($steps as $key => $pas)
            <li {{ ($key == $step) ? 'class=active' : '' }}>{{ $pas }}</li>
        @endforeach
    </ul>
</div>

@if ($step == 0) 
    @includeIf('partials.product-show', ['product' => $product, 'objects' => $objects])
@elseif($step == 1)
    @includeIf('partials.product-detail', ['product' => $product, 'object' => $object])
@elseif($step == 2)
    @includeIf('partials.product-buyed', ['product' => $product])
@endif
@endsection