@extends('Admin::layouts.admin')

@section('styles')
<link href="{{asset('Modules/Shop/css/shop.css')}}" rel="stylesheet">
@endsection

@section('section-title')
    @Lang(ucfirst($post_type_plural))
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">library_books</i>
                </div>
                <h4 class="card-title">
                    @Lang(ucfirst($post_type_plural))
                </h4>
            </div>
            <div class="card-body">
                <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th width="10%">@Lang('Order ID')</th>
                                <th width="15%">@Lang('Product')</th>
                                <th width="15%">@Lang('Title')</th>
                                <th width="10%">@Lang('Payed')</th>
                                <th width="12%">@Lang('Status')</th>
                                <th width="12%">@Lang('Active')</th>
                                <th width="12%">@Lang('Start Time')</th>
                                <th width="12%">@Lang('End Time')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $key => $item)
                            @php($now = (new \Carbon\Carbon()))
                            @php($start = (new \Carbon\Carbon($item->start_at)))
                            @php($end = (new \Carbon\Carbon($item->end_at)))
                            @php($active = ($start <= $now && $end >= $now) ? 'subs_active' : 'subs_no_active')
                            <tr data-post_id="{{ $item->id }}">
                                <td>{{ $item->purchase->order_id }}</td>
                                <td>{{ $item->purchase->post->title }}</td>
                                <td>{{ $item->post->title }}</td>
                                <td>{{ $item->purchase->payed }} €</td>
                                <td><div class="{{ $item->purchase->status }}">{{ $item->purchase->status }}</div></td>
                                <td><div class="{{ $active }}">{{ ($active == 'subs_active') ? __('Active') : __('No Active') }}</div></td> 
                                <td>{{ $start->format('d-m-Y') }}</td>
                                <td>{{ $end->format('d-m-Y') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
function getIDs() {
    var ids = [];
    $('.checkbox_delete').each(function () {
        if($(this).is(":checked")) {
            ids.push($(this).val());
        }
    });
    $('#ids').val(ids.join());
}

$(".checkbox_all").click(function(){
    $('input.checkbox_delete').prop('checked', this.checked);
    getIDs();
});

$('.checkbox_delete').change(function() {
    getIDs();
});
</script>
<script>
$(document).ready(function() {
    var table = $('.table').DataTable({
        "pagingType": "full_numbers",
        @if ($items->first() && !is_null($items->first()->order))
        "rowReorder": true,
        "columnDefs": [
            { targets: 0, visible: false }
        ],
        @endif
        "lengthMenu": [
            [25, 50, -1],
            [25, 50, "@Lang('All')"]
        ],
        responsive: true,
        language: { "url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}" }
    });
});
</script>
@endsection