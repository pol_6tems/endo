<?php
/**
 * @title: Function
 */
?>

@php
    $class = 'App\Modules\Shop\Controllers\FunctionsController';
    $methods = get_class_methods(new $class);
@endphp

<div id="cf{{ $custom_field->id }}" data-id="{{ $custom_field->id }}" class="mt-4 m-auto custom-field text-field">
    <div class="flex-row pt-3">
        <label class="col-md-12">
            {{ __($title) }}
            @if (Auth::user()->role == 'admin')
                (<button class="copy" type="button">{{ $custom_field->name }}</button>)
            @endif
        </label>
        <div class="col-md-12">
            <div class="form-group">
                <select name="{{ $name }}" class="selectpicker" data-style="btn btn-primary" data-live-search="true">
                    @foreach($methods as $method)
                        <option value="{{ $method }}">{{ $method }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>