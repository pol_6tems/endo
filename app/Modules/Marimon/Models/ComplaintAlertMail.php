<?php


namespace App\Modules\Marimon\Models;


use Illuminate\Database\Eloquent\Model;

class ComplaintAlertMail extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}