<?php


namespace App\Modules\Marimon\Models;


use Illuminate\Database\Eloquent\Model;

class ComplaintDoc extends Model
{

    const MEDIA_PATH = 'storage/media';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    
    
    public function getUrl()
    {
        $path = self::MEDIA_PATH . '/' . $this->file_path . '/' .$this->filename . '.' . $this->file_type;
        $url = asset($path);
        $real_path = public_path($path);

        if ( file_exists($real_path) ) return $url;
        else return '';
    }
}