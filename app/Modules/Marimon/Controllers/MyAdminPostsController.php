<?php


namespace App\Modules\Marimon\Controllers;


use App\Http\Controllers\Admin\PostsController;
use App\Models\CustomField;
use App\Models\CustomFieldGroup;
use App\Models\CustomPost;
use App\Modules\Marimon\Models\ComplaintDoc;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MyAdminPostsController extends PostsController
{

    public function index()
    {
        $view = parent::index();

        if (request('post_type') == 'canal-denuncias') {
            return view('Front::admin.posts.companies', $view->getData());
        }

        if (request('post_type') != 'complaint') {
            return $view;
        }

        return view('Front::admin.posts.index', $view->getData());
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = parent::create();

        if (request('post_type') != 'canal-denuncias') {
            return $view;
        }
        $channelData = ['marimon_default' => [
            'en' => [
                'texto-inicio-titulo' => __('home_welcome', ['company_name' => '[company_name]'], 'en'),
                'texto-inicio-bienvenida' => "<p>" . __('home_integrity', [], 'en') . "</p>
					<h3>" . __('home_why', ['company_name' => '[company_name]'], 'en') . "</h3>
					<h4>" . __('home_enforce', [], 'en') . "</h4>
					<p>" . __('home_channel_exists', ['company_name' => '[company_name]'], 'en') . "</p>

					<h4>" . __('home_prevent', [], 'en') . "</h4>
					<p>" . __('home_secure_channel', ['company_name' => '[company_name]'], 'en') . "</p>

					<div class=\"content-more\">
						<h4>" . __('home_grant', [], 'en') . "</h4>
						<p>" . __('home_access', ['company_name' => '[company_name]'], 'en') . "</p>
						<p>" . __('home_confidence', [], 'en') . "</p>
						<p>" . __('home_compromise', [], 'en') . "</p>
						<h3>" . __('home_who', ['company_name' => '[company_name]'], 'en') . "</h3>
						<p>" . __('home_values', [], 'en') . "</p>
						<h3>" . __('home_who_use', [], 'en') . "</h3>
						<p>" . __('home_relation', ['company_name' => '[company_name]'], 'en') . "</p>
					</div>
					<a href=\"javascript:void(0);\" class=\"leer-link\">" . __('Read more', [], 'en') . "...</a>",
                'texto-inicio-aviso' => __('home_disclaimer', ['company_name' => '[company_name]'], 'en'),
                'texto-presentar-denuncia' => __('submit0_previous', [], 'en'),
                'texto-enviar-denuncia' => __('Una vez haya enviado el formulario, éste será procesado de forma segura. Si quiere consultar nuestra respuesta a su denuncia, deberá hacerlo únicamente a través del apartado CONSULTAR ESTADO DE UNA DENUNCIA. Para acceder al apartado CONSULTAR ESTADO DE UNA DENUNCIA, situado en la página inicial de este sitio, introduzca el código de denuncia que se le mostrará una vez haya cursado su denuncia.', [], 'en')
            ],
            'es' => [
                'texto-inicio-titulo' => __('home_welcome', ['company_name' => '[company_name]'], 'es'),
                'texto-inicio-bienvenida' => "<p>" . __('home_integrity', [], 'es') . "</p>
					<h3>" . __('home_why', ['company_name' => '[company_name]'], 'es') . "</h3>
					<h4>" . __('home_enforce', [], 'es') . "</h4>
					<p>" . __('home_channel_exists', ['company_name' => '[company_name]'], 'es') . "</p>

					<h4>" . __('home_prevent', [], 'es') . "</h4>
					<p>" . __('home_secure_channel', ['company_name' => '[company_name]'], 'es') . "</p>

					<div class=\"content-more\">
						<h4>" . __('home_grant', [], 'es') . "</h4>
						<p>" . __('home_access', ['company_name' => '[company_name]'], 'es') . "</p>
						<p>" . __('home_confidence', [], 'es') . "</p>
						<p>" . __('home_compromise', [], 'es') . "</p>
						<h3>" . __('home_who', ['company_name' => '[company_name]'], 'es') . "</h3>
						<p>" . __('home_values', [], 'es') . "</p>
						<h3>" . __('home_who_use', [], 'es') . "</h3>
						<p>" . __('home_relation', ['company_name' => '[company_name]'], 'es') . "</p>
					</div>
					<a href=\"javascript:void(0);\" class=\"leer-link\">" . __('Read more', [], 'es') . "...</a>",
                'texto-inicio-aviso' => __('home_disclaimer', ['company_name' => '[company_name]']), 'es',
                'texto-presentar-denuncia' => __('submit0_previous'), 'es',
                'texto-enviar-denuncia' => __('Una vez haya enviado el formulario, éste será procesado de forma segura. Si quiere consultar nuestra respuesta a su denuncia, deberá hacerlo únicamente a través del apartado CONSULTAR ESTADO DE UNA DENUNCIA. Para acceder al apartado CONSULTAR ESTADO DE UNA DENUNCIA, situado en la página inicial de este sitio, introduzca el código de denuncia que se le mostrará una vez haya cursado su denuncia.', [], 'es')
            ]
        ]];

        return view('Front::admin.posts.create', array_merge($view->getData(), $channelData));
    }


    public function edit($locale, $id)
    {
        $view = parent::edit($locale, $id);

        if (request('post_type') != 'complaint') {
            return $view;
        }

        $docs = ComplaintDoc::where('post_id', $view->getData()['post']->id)->get();

        $company = Post::find($view->getData()['post']->parent_id);

        return view('Front::admin.posts.edit', array_merge($view->getData(), ['docs' => $docs, 'company' => $company]));
    }


    public function update(Request $request, $locale, $id)
    {
        if (request('post_type') != 'complaint') {
            return parent::update($request, $locale, $id);
        }

        $post = Post::find($id);

        $currentStatus = $post->get_field('estado');
        $currentReason = $post->get_field('razon-archivada');
        $newStatus = $request->get('estado');
        $newReason = $request->get('razon-archivada');

        if ($post && $currentStatus != $newStatus) {
            $inProgressDate = $post->get_field('fecha-en-tramite');

            $newInProgressDate = null;
            $newArchivedDate = null;

            if ($newStatus != 'en_proceso') {
                $newInProgressDate = $inProgressDate ?: Carbon::now()->toDateTimeString();
            }

            if ($newStatus == 'archivada') {
                $newArchivedDate = Carbon::now()->toDateTimeString();
            }

            $cfg = CustomFieldGroup::where('post_type', $this->post_type)->first();

            $cfs = CustomField::where('cfg_id', $cfg->id)
                ->get();

            foreach ($this->_languages as $lang) {
                $this->save_custom_fields($post, [$cfs->where('name', 'estado')->first()->id => $newStatus], $lang->code);
                $this->save_custom_fields($post, [$cfs->where('name', 'razon-archivada')->first()->id => $newReason], $lang->code);
                $this->save_custom_fields($post, [$cfs->where('name', 'fecha-en-tramite')->first()->id => $newInProgressDate], $lang->code);
                $this->save_custom_fields($post, [$cfs->where('name', 'fecha-archivada')->first()->id => $newArchivedDate], $lang->code);
            }

            unset($post->estado);
            unset($post->razon_archivada);
            unset($post->fecha_en_tramite);
            $post->touch();

            if ($newStatus == 'en_tramite' && $currentStatus == 'en_proceso' && !$post->get_field('anonima')) {
                $this->notifyClient($post);
            }
        } elseif ($post && $currentReason != $newReason) {
            $cfg = CustomFieldGroup::where('post_type', $this->post_type)->first();

            $cfs = CustomField::where('cfg_id', $cfg->id)
                ->get();

            foreach ($this->_languages as $lang) {
                $this->save_custom_fields($post, [$cfs->where('name', 'razon-archivada')->first()->id => $newReason], $lang->code);
            }

            unset($post->estado);
            unset($post->razon_archivada);
            unset($post->fecha_en_tramite);
            $post->touch();
        }

        return redirect(route('admin.denuncias.edit', $post->id) . '?post_type=' . $this->post_type)->with(['message' => __('Post updated successfully')]);
    }


    public function notifyClient($complaint)
    {
        $contactEmail = $complaint->get_field('email');
        $contactName = $complaint->get_field('nombre') . ' ' . $complaint->get_field('apellidos');

        Mail::raw(preg_replace('/\<br(\s*)?\/?\>/i', "\n", __('compalint_archived_email',[], $complaint->get_field('locale'))), function ($message) use ($contactEmail, $contactName) {
            $message->from(config('mail.from.address'), config('mail.from.name'));
            $message->to($contactEmail, $contactName);
            $message->subject('Marimón Abogados');
        });
    }
}