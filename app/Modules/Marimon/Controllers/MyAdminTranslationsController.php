<?php


namespace App\Modules\Marimon\Controllers;


use App\Http\Controllers\Admin\AdminController;
use App\Module;
use DB;
use File;
use Illuminate\Http\Request;

class MyAdminTranslationsController extends AdminController
{

    public function index()
    {
        $item = Module::where('name', 'Marimon')->first();
        $languages = DB::table('languages')->where('active', true)->get();
        $columns = [];
        $columnsCount = $languages->count();
        if ($languages->count() > 0) {
            foreach ($languages as $key => $language) {
                if ($key == 0) $columns[$key] = $this->openJSONFile($language->code, $item->name);
                $columns[++$key] = [
                    'data' => $this->openJSONFile($language->code, $item->name),
                    'lang' => $language->code
                ];
            }
        }
        return View('Front::admin.languages', compact('item', 'languages', 'columns', 'columnsCount'));
    }


    public function update(Request $request, $locale, $id)
    {
        $data = $request->all();
        $item = Module::where('name', 'Marimon')->first();
        $item->update($data);
        return redirect()->route('admin.modules.edit', $id)->with(['message' => __(':name updated successfully', ['name' => __('Module')])]);
    }

    public function destroy($locale, $id)
    {
        $language = Module::findOrFail($id);
        if ($language->delete()) {
            return response()->json(['success' => 'Done!']);
        }
        return response()->json(['error' => 'Done!'], 401);
    }

    public function active(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $item = Module::where('name', 'Marimon')->first();
        $item->active = !$item->active;
        $item->update();
        return response()->json(['success' => 'Done!', 'active' => $item->active]);
    }

    /* Translations */
    public function transStore(Request $request, $locale)
    {
        $module = Module::where('name', 'Marimon')->first();
        $this->validate($request, [
            'key' => 'required',
            'value' => 'required',
        ]);

        $data = $this->openJSONFile('en', $module->name);
        $data[$request->key] = $request->value;
        //$data[$request->key] = addcslashes($request->value, '"\\/');

        array_walk_recursive($data, function (&$item, $key) {
            $item = addcslashes($item, '"\\/');
        });

        $this->saveJSONFile('en', $data, $module->name);

        return redirect()->route('admin.traducciones.index');
    }

    public function transUpdate(Request $request, $locale)
    {
        $module = Module::where('name', 'Marimon')->first();
        $data = $this->openJSONFile($request->code, $module->name);
        $data[$request->pk] = $request->value;

        array_walk_recursive($data, function (&$item, $key) {
            $item = addcslashes($item, '"\\/');
        });

        $this->saveJSONFile($request->code, $data, $module->name);
        return response()->json(['success' => 'Done!']);
    }

    public function transUpdateKey(Request $request, $locale)
    {
        $module = Module::where('name', 'Marimon')->first();
        $languages = DB::table('languages')->get();
        if ($languages->count() > 0) {
            foreach ($languages as $language) {
                $data = $this->openJSONFile($language->code, $module->name);
                if (isset($data[$request->pk])) {
                    $data[$request->value] = $data[$request->pk];
                    unset($data[$request->pk]);
                    $this->saveJSONFile($language->code, $data, $module->name);
                }
            }
        }
        return response()->json(['success' => 'Done!']);
    }

    public function transDestroy($locale, $key)
    {
        $module = Module::where('name', 'Marimon')->first();
        $key = base64_decode($key);
        $languages = DB::table('languages')->get();
        if ($languages->count() > 0) {
            foreach ($languages as $language) {
                $data = $this->openJSONFile($language->code, $module->name);
                unset($data[$key]);
                $this->saveJSONFile($language->code, $data, $module->name);
            }
        }
        return response()->json(['success' => $key]);
    }


    private function openJSONFile($code, $module_name)
    {
        $jsonString = [];
        $file_path = app_path("Modules/$module_name/resources/lang/$code.json");
        if (File::exists($file_path)) {
            $jsonString = file_get_contents($file_path);
            $jsonString = json_decode($jsonString, true);
        }
        return $jsonString;
    }

    private function saveJSONFile($code, $data, $module_name)
    {
        $file_path = app_path("Modules/$module_name/resources/lang/$code.json");
        ksort($data);
        $jsonData = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        $jsonData = str_replace('\n', '<br>', $jsonData);
        file_put_contents($file_path, stripslashes($jsonData));
    }
}