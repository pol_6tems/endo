<?php


namespace App\Modules\Marimon\Controllers;


use App\Http\Controllers\PostsController;
use App\Language;
use App\Models\CustomFieldGroup;
use App\Models\Opcion;
use App\Models\PostMeta;
use App\Modules\Marimon\Models\ComplaintDoc;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Storage;

class CompanyController extends PostsController
{

    public function index($locale, $company = '')
    {
        return view('Front::home', compact('company'));
    }


    public function terms($locale, $company)
    {
        $item = $this->is_single('terminos-y-condiciones');

        return view('Front::posts.templates.terminos-y-condiciones', compact('company', 'item'));
    }


    public function access($locale, $company)
    {
        $terms = $this->is_single('terminos-y-condiciones');

        $complaintTypes = $company->get_field('tipo-denuncia');

        if (!$complaintTypes) {
            $langs = Language::where('active', true)->get();

            foreach ($langs as $langItem) {
                $auxComplaintTypes = $company->get_field('tipo-denuncia', $langItem->code);

                if ($auxComplaintTypes) {
                    $complaintTypes = $auxComplaintTypes;
                    break;
                }
            }
        }
        $genericType = $this->is_single('generica');
        $singleType = $complaintTypes && $complaintTypes->count() == 1 && $complaintTypes->first()->id != $genericType->id ? $complaintTypes->first() : null;

        $defaultRouteParams = [
            'locale' => app()->getLocale(),
            'company' => $company->post_name
        ];

        if ($singleType) {
            $defaultRouteParams['type'] = $singleType->id;
        }
        
        return view('Front::compliant.access', compact('company', 'terms', 'complaintTypes', 'defaultRouteParams', 'genericType'));
    }


    public function anonymous($locale, $company)
    {
        return view('Front::compliant.anonymous', $this->getComplaintVars($company, 'submit0_anonymous'));
    }


    public function identified($locale, $company)
    {
        return view('Front::compliant.identified', $this->getComplaintVars($company));
    }


    public function successComplaint($locale, $company)
    {
        $denId = request('den');

        $complaint = $this->is_single($denId);

        if (!$complaint || $company->id != $complaint->parent_id) {
            return redirect($company->get_url());
        }

        return view('Front::compliant.success', compact('company', 'complaint'));
    }


    public function postComplaint(Request $request, $locale, $company)
    {
        $data = request()->all();

        $captchaToken = request('captcha_token');

        if (!$captchaToken || !$this->validateCaptcha($captchaToken)) {
            abort(403);
        }

        $denCode = $this->getComplaintDen($company);

        $postData = [
            'type' => 'complaint',
            'parent_id' => $company->id,
            'status' => 'publish',
            'author_id' => 0,
        ];

        foreach ($this->_languages as $lang) {
            $postData[$lang->code] = [
                'title' => $denCode,
                'post_name' => $denCode
            ];
        }

        $post = Post::create($postData);

        $cf_groups = CustomFieldGroup::where('post_type', 'complaint')->orderBy('order')->get();

        foreach ($this->_languages as $lang) {
            foreach ($cf_groups as $cf_group) {
                foreach ($cf_group->fields as $field) {
                    if (array_key_exists($field->name, $data)) {
                        PostMeta::create([
                            'post_id' => $post->id,
                            'custom_field_id' => $field->id,
                            'value' => $data[$field->name],
                            'locale' => $lang->code
                        ]);
                    }
                }
            }
        }

        if (isset($request->docs)) {
            $this->storeDocs($request->docs, $post, $denCode);
        }

        if (isProEnv()) {
            $this->sendEmails($post);
        }
        
        return redirect()->route('posts.submit-complaint-success', ['locale' => $locale, 'company' => $company->post_name, 'den' => $denCode]);
    }


    public function getComplaintStatus($locale, $company)
    {
        return view('Front::compliant.get-status', compact('company'));
    }
    
    
    public function postComplaintStatus($locale, $company)
    {
        $denId = request('den');

        $complaint = $this->is_single($denId);

        if (!$complaint || $company->id != $complaint->parent_id) {
            return redirect($company->get_url());
        }

        return view('Front::compliant.post-status', compact('company', 'complaint'));
    }


    public function callAction($method, $parameters)
    {
        if (!array_key_exists('company', $parameters)) {
            abort(403);
        }

        $companyName = $parameters['company'];

        $company = $this->is_single($companyName);

        if (!$company) {
            $company = Post::where('status', 'publish')->whereTranslation('post_name', $companyName)->first();
        }

        if (!$company) {
            abort(403);
        }

        $parameters['company'] = $company;

        return parent::callAction($method, $parameters);
    }


    private function getComplaintVars($company, $complaintTitle = 'Submit a complaint')
    {
        $cf_groups = CustomFieldGroup::where('post_type', 'complaint')->first();
        $fields = $cf_groups->fields;

        $countries = Post::where('type', 'country')->withTranslation()->get()->sortBy('title', SORT_NATURAL|SORT_FLAG_CASE);

        $tipoDenuncia = request('type');

        if (!$tipoDenuncia) {
            $genericType = $this->is_single('generica');
            $tipoDenuncia = $genericType->id;
        }

        return compact('company', 'complaintTitle', 'fields', 'countries', 'tipoDenuncia');
    }


    private function getComplaintDen($company)
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $denCode = 'DEN-' . substr(str_shuffle($permitted_chars), 0, 8);

        $compliant = $this->is_single($denCode);

        if ($compliant && $compliant->parent_id == $company->id) {
            return $this->getComplaintDen($company);
        }

        return $denCode;
    }


    private function storeDocs($docs, $complaint, $denCode)
    {
        $nowFormated = Carbon::now()->format('Y/m');

        foreach ($docs as $doc) {
            // Generar Fitxer
            $file_type = $doc->getClientOriginalExtension();
            $file_name = $denCode . '-' . str_replace('.' . $doc->getClientOriginalExtension(), '', $doc->getClientOriginalName());
            $filePath = 'public/media/' . $nowFormated . '/';

            $i = 0;
            $file_path_aux = $filePath . $file_name . '.' . $file_type;
            while (Storage::exists($file_path_aux)) {
                $i++;
                $file_path_aux = $filePath . $file_name . '-' . $i . '.' . $file_type;
            }

            $file_name = $file_name . ($i > 0 ? '-' . $i : '');
            if ($stored_file = $doc->storeAs($filePath, $file_name . '.' . $file_type)) {
                ComplaintDoc::create([
                    'post_id' => $complaint->id,
                    'filename' => $file_name,
                    'file_type' => $file_type,
                    'file_path' => $nowFormated
                ]);
            }
        }
    }

    private function sendEmails(Post $complaint)
    {
        $denCode = $complaint->translate(app()->getLocale(), true)->title;
        $isAnonymous = $complaint->get_field('anonima');

        $genericType = $this->is_single('generica');
        $complaintType = $complaint->get_field('tipo-denuncia');

        if (!$isAnonymous) {
            $contactEmail = $complaint->get_field('email');
            $contactName = $complaint->get_field('nombre') . ' ' . $complaint->get_field('apellidos');

            $confirmTextKey = 'complaint_confirm_email';

            if ($complaintType->post_name == 'acoso') {
                $confirmTextKey = 'complaint_confirm_email_acoso';
            }

            if ($complaintType->post_name == 'blanqueo') {
                $confirmTextKey = 'complaint_confirm_email_blanqueo';
            }

            Mail::raw(preg_replace('/\<br(\s*)?\/?\>/i', "\n", __($confirmTextKey, ['den_code' => $denCode, 'complaint_date' => $complaint->created_at->format('Y/m/d H:i:s')])), function ($message) use ($contactEmail, $contactName) {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to($contactEmail, $contactName);
                $message->subject(__('Código de recepción denuncia'));
            });
        }

        $emailsOption = Opcion::where('key', 'complaint_alert_emails')->first();

        if (!$emailsOption) {
            return;
        }

        $contacts = json_decode($emailsOption->value, true);

        if (!count($contacts)) {
            return;
        }

        $typeText = ($complaintType && $complaintType->id != $genericType->id ? 'de ' : '') . ($complaintType ? $complaintType->title : __('GENÉRICA'));

        $text = 'Nueva denuncia ' . $typeText . ' ' . $complaint->title . ' de la empresa ' . $complaint->parent->title . ' con fecha de envío ' . $complaint->created_at->format('Y/m/d H:i:s');
        
        Mail::raw($text, function ($message) use ($contacts) {
            $message->from(config('mail.from.address'), config('mail.from.name'));
            $message->subject('Alerta - Marimón Abogados');
            foreach ($contacts as $contact) {
                $message->to($contact['email'], $contact['name']);
            }
        });
    }


    private function validateCaptcha($captcha) {
        $secretKey = "6Lc3P7AUAAAAAB1ITUDrVcfJteqalOarYrDOwRFb";

        // post request to server
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = ['secret' => $secretKey, 'response' => $captcha];

        $options = [
            'http' => [
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            ]
        ];

        $context  = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        $responseKeys = json_decode($response,true);

        return $responseKeys["success"];
    }
}