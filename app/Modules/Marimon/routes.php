<?php

Route::get('/{index?}', function () {
    return redirect()->route('admin');
})->name('index');

Route::get('/', function () {
    return redirect()->route('admin');
});

Route::get('canal-denuncias/{company?}', "CompanyController@index")->name('posts');

Route::get('canal-denuncias/{company}/terminos-y-condiciones', "CompanyController@terms")->name('posts.terms');

Route::get('canal-denuncias/{company}/presentar-denuncia', "CompanyController@access")->name('posts.access');

Route::get('canal-denuncias/{company}/presentar-denuncia/anonima', "CompanyController@anonymous")->name('posts.submit-anonymous');
Route::get('canal-denuncias/{company}/presentar-denuncia/identificado', "CompanyController@identified")->name('posts.submit-identified');

Route::post('canal-denuncias/{company}/presentar-denuncia', "CompanyController@postComplaint")->name('posts.submit-complaint');

Route::get('canal-denuncias/{company}/presentar-denuncia/success', "CompanyController@successComplaint")->name('posts.submit-complaint-success');

Route::get('canal-denuncias/{company}/consultar-denuncia', "CompanyController@getComplaintStatus")->name('posts.get-status');

Route::post('canal-denuncias/{company}/consultar-denuncia', "CompanyController@postComplaintStatus");

// ADMIN
Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    Route::resource("denuncias", "MyAdminPostsController", ["as" => "admin"]);
    Route::resource("posts", "MyAdminPostsController", ["as" => "admin"]);
    
    Route::resource("traducciones", "MyAdminTranslationsController", ["as" => "admin"]);
    Route::post('traducciones/update', 'MyAdminTranslationsController@transUpdate')->name('admin.traducciones.update.json');
    Route::post('traducciones/updateKey', 'MyAdminTranslationsController@transUpdateKey')->name('admin.traducciones.update.json.key');
    Route::delete('traducciones/destroy/{key}', 'MyAdminTranslationsController@transDestroy')->name('admin.traducciones.destroy');
    Route::post('traducciones/create', 'MyAdminTranslationsController@transStore')->name('admin.traducciones.create');
});