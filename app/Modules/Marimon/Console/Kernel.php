<?php
/**
 * Created by PhpStorm.
 * User: sergisolsona
 * Date: 12/06/2019
 * Time: 11:58
 */

namespace App\Modules\Marimon\Console;

use App\Modules\Marimon\Console\Commands\SendComplaintAlerts;
use Illuminate\Console\Scheduling\Schedule;
use App\Modules\Marimon\Console\Commands\Inspire;

class Kernel
{

    /**
     * The Artisan commands provided by your module.
     *
     * @var array
     */
    protected $commands = [
        // Inspire::class
        SendComplaintAlerts::class
    ];


    public function getCommands()
    {
        return $this->commands;
    }


    /**
     * Define the module's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule)
    {
        /*$schedule->command('inspire')
            ->hourly();*/

        $schedule->command('send:complain-alerts')
            ->weekdays()->everyThirtyMinutes()->between('7:00', '20:00');
    }
}