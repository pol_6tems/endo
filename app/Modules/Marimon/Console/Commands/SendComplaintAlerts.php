<?php

namespace App\Modules\Marimon\Console\Commands;

use App\Models\Opcion;
use App\Modules\Marimon\Models\ComplaintAlertMail;
use App\Post;
use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Facades\Mail;

class SendComplaintAlerts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:complain-alerts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send complaint alerts to Marimón before 5 days after creation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now()->subDays(3);

        $emailsOption = Opcion::where('key', 'complaint_alert_emails')->first();

        if (!$emailsOption) {
            $this->error('Please, setup complaint_alert_emails option');
            return;
        }

        $contacts = json_decode($emailsOption->value, true);

        if (!count($contacts)) {
            $this->error('No emails to send alerts');
            return;
        }

        $complaints = Post::select(DB::raw('distinct posts.*'))
            /*->with(['metas.customField'])*/
            ->join('post_meta', 'posts.id', '=', 'post_meta.post_id')
            ->join('custom_fields', 'post_meta.custom_field_id', '=', 'custom_fields.id')
            ->where('posts.type', 'complaint')
            ->where('status', 'publish')
            ->where('posts.created_at', '<', $now)
            ->where('custom_fields.name', 'estado')
            ->where('post_meta.value', 'en_proceso')
            ->whereRaw('(select count(*) from complaint_alert_mails cam where cam.post_id = posts.id) = 0')
            ->get();

        foreach ($complaints as $complaint) {
            $alertDate = $this->getAlertDate($complaint->created_at);

            if ($alertDate <= Carbon::now() && $complaint->created_at >= Carbon::now()->subWeek()) {
                $text = 'Quedan menos de 48h para que finalice el plazo de 5 días laborables para la denuncia ' . $complaint->title . ' de la empresa ' . $complaint->parent->title;

                Mail::raw($text, function ($message) use ($contacts) {
                    $message->from(config('mail.from.address'), config('mail.from.name'));
                    $message->subject('Alerta - Marimón Abogados');
                    foreach ($contacts as $contact) {
                        $message->to($contact['email'], $contact['name']);
                    }
                });

                ComplaintAlertMail::create([
                    'post_id' => $complaint->id
                ]);
            }
        }

        $this->info('Done!');
    }


    private function getAlertDate(Carbon $created_at)
    {
        for($i = 0; $i < 3; $i++)
        {
            $created_at = $created_at->addDay();

            while ($created_at->isWeekend()) {
                $created_at = $created_at->addDay();
            }
        }

        return $created_at;
    }
}
