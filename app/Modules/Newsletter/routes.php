<?php

Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    Route::get('newsletter', "AdminNewsletterController@index")->name('admin.newsletter.index');
});

//Route::get('newsletter-test', "NewsletterController@test")->name('newsletter.test');
Route::post('newsletter-subscribe', "NewsletterController@subscribe")->name('newsletter.subscribe');

Route::get('clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});