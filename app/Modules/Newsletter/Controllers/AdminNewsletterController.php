<?php

namespace App\Modules\Newsletter\Controllers;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Opcion;

class AdminNewsletterController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Newsletter');
        $this->section_icon = 'star_rate';
        $this->section_route = 'admin.newsletter';
    }

    public function index() {
        $opcion = Opcion::where('key', 'newsletter_api_key')->first();
        if ( empty($opcion) ) {
            Opcion::create([
                'key' => 'newsletter_api_key',
                'value' => '7451132b69f2ee18de3c696baa24ed9f-us14',
                'descripcion' => 'NewsletterModule: Mailchimp Api Key - Conta 6TEMS',
            ]);
        }
        $opcion = Opcion::where('key', 'newsletter_list_id')->first();
        if ( empty($opcion) ) {
            Opcion::create([
                'key' => 'newsletter_list_id',
                'value' => '17e30db445',
                'descripcion' => 'NewsletterModule: Mailchimp List Id - Conta 6TEMS Llista Prova',
            ]);
        }
        $opcion = Opcion::where('key', 'newsletter_cron_config')->first();
        if ( empty($opcion) ) {
            Opcion::create([
                'key' => 'newsletter_cron_config',
                'value' => '30 1 1,15 * *',
                'descripcion' => 'NewsletterModule: Configuració Cron - Cada 2 setmanes (dies 1 i 15) a les 1:30 AM',
            ]);
        }
        $opcion = Opcion::where('key', 'newsletter_reply_to')->first();
        if ( empty($opcion) ) {
            Opcion::create([
                'key' => 'newsletter_reply_to',
                'value' => 'pol@6tems.com',
                'descripcion' => 'NewsletterModule: Email Reply to per Mailchimp',
            ]);
        }

        $items = Opcion::where('key', 'like', 'newsletter%')->get();
        
        return View("newsletter.admin.index", compact('items'));
    }

}
