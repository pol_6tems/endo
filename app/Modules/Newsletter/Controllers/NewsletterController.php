<?php

namespace App\Modules\Newsletter\Controllers;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use URL;
use App\Models\Opcion;
use Illuminate\Http\Request;
use App\User;
use App\Notifications\EmailNotification;
use App\Models\Mensaje;

class NewsletterController extends Controller
{
    public static function test()
    {
        $res = self::crear_plantilla();
        echo $res['plantilla'];
    }

    public static function get_opcion($key)
    {
        $opcion = Opcion::where('key', $key)->first();
        if (empty($opcion->value)) return false;
        else return $opcion->value;
    }

    public static function send_newsletter()
    {
        $res = self::crear_plantilla();
        $ok = $res['ok'];
        $plantilla = $res['plantilla'];
        $title = $res['title'];

        if (!$ok) return;

        // Mailchimp List id
        $list_id = self::get_opcion('newsletter_list_id');
        if ($list_id == FALSE) return;

        // Mailchimp Reply to
        $reply_to = self::get_opcion('newsletter_reply_to');
        if ($reply_to == FALSE) return;

        // Enviar template
        $action = "/templates";
        $args = json_encode(array(
            'name' => $title,
            'html' => $plantilla
        ));
        $res_template = self::mailchimp_form_send($action, $args);
        // end Enviar template

        if (FALSE == $res_template) return false;

        $plantilla_id = $res_template->id;

        // Crear campanya
        $action = "/campaigns";
        $args = json_encode(array(
            'type' => 'regular',
            'recipients' => array('list_id' => $list_id),
            'settings'  => array(
                'title' => $title,
                'from_name' => config('app.name'),
                'template_id' => $plantilla_id,
                'subject_line' => $title,
                'reply_to' => $reply_to
            )
        ));
        $res_campanya = self::mailchimp_form_send($action, $args);
        // end Crear campanya

        if (FALSE == $res_campanya) return false;

        $campanya_id = $res_campanya->id;

        // Enviar campanya
        $action = "/campaigns/{$campanya_id}/actions/send";
        $args = array();
        $res_campanya_send = self::mailchimp_form_send($action, $args);
        // end Enviar campanya
    }

    public static function mailchimp_form_send($action, $args, $method = 'POST')
    {
        $mailchimp_apiKey = self::get_opcion('newsletter_api_key');
        if (empty($mailchimp_apiKey)) return false;

        $list_id = self::get_opcion('newsletter_list_id');
        if (empty($list_id)) return false;

        $dataCenter = substr($mailchimp_apiKey, strpos($mailchimp_apiKey, '-') + 1);
        $url = "https://{$dataCenter}.api.mailchimp.com/3.0{$action}";

        $auth = base64_encode('PRCadmin:' . $mailchimp_apiKey);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Authorization: Basic ' . $auth
            )
        );
        //curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/3.0');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POST, true);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $json = json_decode($result);
        curl_close($ch);

        /*
        dump($args, "MAILCHIMP FIELDS");
        dump($url, "MAILCHIMP URL");
        dump($result, "RESULT MAILCHIMP SEND");
        dump($httpCode, "HTTPCODE MAILCHIMP SEND");
        */

        if ($httpCode == 200) {
            return $json;
        } else {
            return false;
        }
    }

    public static function subscribe(Request $request)
    {
        $data = $request->all();

        if (empty(($data['email']))) return 'false';

        $email = $data['email'];

        $list_id = self::get_opcion('newsletter_list_id');
        if (empty($list_id)) return 'false';

        $memberID = md5(strtolower($email));
        $action = "/lists/{$list_id}/members/{$memberID}";
        $json = json_encode(array(
            'email_address' => $email,
            'status' => 'subscribed'
        ));
        $result = self::mailchimp_form_send($action, $json, 'PUT');

        if ($result !== FALSE) {
            self::send_mail_new_subscriber($email);
            return 'true';
        } else return 'false';
    }

    public static function send_mail_new_subscriber($email)
    {
        // Mail: Avis nou subscriptor newsletter (admin)
        $from_email = config("mail.from.address");
        $to = User::where('email', 'formabages@ccbages.cat')->first();

        if ($from_email && $to) {
            $mensaje = new Mensaje();
            $mensaje->user_id = 0;
            $mensaje->mensaje = '';
            $mensaje->fitxer = '';
            $mensaje->email = '';
            $mensaje->name = '';
            $mensaje->type = 'email';
            $mensaje->params = json_encode(array(
                'email' => $email
            ));

            $params = new \stdClass();
            $params->usuario = $to;
            $params->mensaje = $mensaje;
            $params->email_id = 2;

            $notificacion = new EmailNotification($params);
            if ($notificacion->check_email()) {
                $to->notify($notificacion);
            }
        }

        // Mail: Avis nou subscriptor newsletter (usuari)
        $from_email = config("mail.from.address");
        $to = new User();
        $to->name = '';
        $to->email = $email;

        if ($from_email && $to) {
            $mensaje = new Mensaje();
            $mensaje->user_id = 0;
            $mensaje->mensaje = '';
            $mensaje->fitxer = '';
            $mensaje->email = '';
            $mensaje->name = '';
            $mensaje->type = 'email';
            $mensaje->params = null;

            $params = new \stdClass();
            $params->usuario = $to;
            $params->mensaje = $mensaje;
            $params->email_id = 3;

            $notificacion = new EmailNotification($params);
            if ($notificacion->check_email()) {
                $to->notify($notificacion);
            }
        }
    }

    public static function crear_plantilla()
    {
        setlocale(LC_TIME, 'ca_ES');
        \Carbon\Carbon::setLocale('ca');

        $ok = true;

        $date = \Carbon\Carbon::now();
        $fecha = $date->formatLocalized('%B %Y');
        $title = "Newsletter $fecha";

        $theme = config('front_theme');
        $template_url = URL::asset("{$theme}") . "/";

        $cursos_page = get_archive_link('curso');

        $cursos =  Post::ofType('curso')
            ->ofStatus('publish')
            ->withTranslation()
            ->orderByDesc('created_at')
            ->get();

        $cursos = $cursos->filter(function ($curs) {
            $startDate = $curs->get_field('data-inici-del-curs');

            if ($startDate) {
                $startDate = \Carbon\Carbon::createFromFormat('d/m/Y', $startDate);
            }

            return $startDate && $startDate > \Carbon\Carbon::now();
        })->take(10);

        $tipus_formacions = Post::ofType('tipo-formacion')
            ->ofStatus('publish')
            ->join('post_translations as t', 't.post_id', '=', 'posts.id')
            ->where('locale', 'ca')
            ->orderBy('t.title', 'asc')
            ->with('translations')
            ->get();

        $tipus_formacions = Post::ofType('tipo-formacion')
            ->ofStatus('publish')
            ->join('post_translations as t', 't.post_id', '=', 'posts.id')
            ->where('locale', 'ca')
            ->orderBy('t.title', 'asc')
            ->with('translations')
            ->get();

        $poblacions_query = Post::ofType('poblacion')
            ->ofStatus('publish')
            ->join('post_translations as t', 't.post_id', '=', 'posts.id')
            ->where('locale', 'ca')
            ->orderBy('t.title', 'asc')
            ->with('translations')
            ->get();

        $news = Post::select(DB::raw('posts.*'))
            ->ofType('noticia')
            ->ofStatus('publish')
            ->with(['metas'])
            ->join('post_translations as t', 't.post_id', '=', 'posts.id')
            ->where('locale', 'ca')
            ->with('translations')
            ->get();

        $lastNews = $news->sortByDesc(function($noticia) {
            $date = $noticia->get_field('data-publicacio');
            if ($date) {
                return \Carbon\Carbon::createFromFormat('d/m/Y', $date);
            }
        })->take(4)->each(function ($noticia) {
            $date = $noticia->get_field('data-publicacio');

            $noticia->url = $noticia->get_url();
            $noticia->body = get_excerpt(strip_tags(str_replace('</p>', '</p> ', $noticia->description)), 30);

            if ($date) {
                $noticia->data_publicacio = getDayAndMonthCat($date, 'd/m/Y');
            }
        });

        // Loop Categories
        $categories = array();
        foreach ($tipus_formacions as $tf_obj) {
            $cursos_count = Post::ofType('curso')
                ->ofStatus('publish')
                ->leftJoin('post_meta', 'post_meta.post_id', '=', 'posts.id')
                ->leftJoin('custom_fields', 'post_meta.custom_field_id', '=', 'custom_fields.id')
                ->where('custom_fields.name', 'tipus-formacio')
                ->where('post_meta.value', $tf_obj->post_id)
                ->count();

            $categories[$tf_obj->post_id]['title'] = $tf_obj->title;
            $categories[$tf_obj->post_id]['url'] = "{$cursos_page}?tipus-formacio={$tf_obj->post_name}";
            $categories[$tf_obj->post_id]['count'] = $cursos_count;
            $categories[$tf_obj->post_id]['ultims_cursos'] = array();
        }
        // end Loop Categories

        // Loop Poblacions
        $poblacions = array();
        foreach ($poblacions_query as $p_obj) {
            $poblacions[$p_obj->post_id]['title'] = $p_obj->title;
            $poblacions[$p_obj->post_id]['url'] = "{$cursos_page}?localitzacio={$p_obj->post_name}";
        }
        // end Loop Poblacions

        // Loop Últims cursos
        foreach ($cursos as $element) {
            $tipus_formacio = $element->get_field('tipus-formacio');
            $duracio = $element->get_field('duracio');
            $poblacio = null;
            $centre = $element->get_field('centre');
            if (!empty($centre)) {
                $poblacio_obj = $centre->get_field('poblacion');
                if (!empty($poblacio_obj)) $poblacio = $poblacio_obj->title;
            }

            $new_curs = new \stdClass();
            $new_curs->id = $element->id;
            $new_curs->title = $element->title;
            $new_curs->poblacio = $poblacio;
            $new_curs->duracio = $duracio;
            $new_curs->url = $element->get_url();

            $categories[$tipus_formacio->id]['ultims_cursos'][$new_curs->id] = $new_curs;
        }
        // end Loop Últims cursos

        $html = "<html>
        <head>
        </head>
        <body>
        
        <style>
            @font-face {
                font-family: barlowregular;
                src: url({$template_url}fonts/barlow-regular-webfont.eot);
                src: url({$template_url}fonts/barlow-regular-webfont.eot?#iefix) format('embedded-opentype'), url({$template_url}fonts/barlow-regular-webfont.woff) format('woff'), url({$template_url}fonts/barlow-regular-webfont.ttf) format('truetype'), url({$template_url}fonts/barlow-regular-webfont.svg#barlowregular) format('svg');
                font-weight: 400;
                font-style: normal
            }
            
            @font-face {
                font-family: barlowbold;
                src: url({$template_url}fonts/barlow-bold-webfont.eot);
                src: url({$template_url}fonts/barlow-bold-webfont.eot?#iefix) format('embedded-opentype'), url({$template_url}fonts/barlow-bold-webfont.woff) format('woff'), url({$template_url}fonts/barlow-bold-webfont.ttf) format('truetype'), url({$template_url}fonts/barlow-bold-webfont.svg#barlowbold) format('svg');
                font-weight: 400;
                font-style: normal
            }

            html, body {
                font-family: barlowregular;
                font-weight: normal;
                font-size: 16px;
            }

            table {
                width: 600px;
            }

            #header {
                font-family: barlowbold;
                background: #fc6262;
                color: #fff;
                padding: 10px 20px;
                font-size: 22px;
                font-weight: bold;
            }
            #header .data {
                text-align: right;
                font-size: 14px;
                font-weight: lighter;
            }

            #coneix,
            #poblacions,
            #categories,
            #ultims-cursos,
            #noticies  {
                background: #f8f8f8;
                color: #000;
                padding: 15px 20px;
            }
            
            #coneix .header,
            #poblacions .header,
            #categories .header,
            #ultims-cursos .header,
            #noticies .header {
                font-family: barlowbold;
                color: #fc6262;
                font-weight: bold;
                border-bottom: solid 1px #fc6262;
            }
            
            #noticies .header {
                margin-bottom: 8px;
            }
            
            #noticies .noticia {
                font-size: 14px;
            }
            
            #ultims-cursos .header-categoria,
             #noticies .noticia .noticia-header {
                font-family: barlowbold;
                color: #000;
                font-weight: bold;
                padding: 15px 0px;
            }
            
            #noticies .noticia .noticia-header {
                font-size: 16px;
                margin-bottom: 8px;
                padding: 8px 0px;
            }
            
            #noticies .noticia .noticia-date {
                margin-bottom: 8px;
            }
            
            #noticies .noticia a {
                color: #fc6262;
            }

            #ultims-cursos .veure-categoria {
                text-align: center;
            }

            a.boto:link,
            #noticies .noticia a:link {
                text-decoration: none;
            }
            
            a.boto:visited,
            #noticies .noticia a:visited {
                text-decoration: none;
            }
            
            a.boto:hover,
            #noticies .noticia a:visited {
                text-decoration: none;
            }
            
            a.boto:active,
            #noticies .noticia a:active {
                text-decoration: none;
            }
            .boto {
                border: 1px solid #fc6262;
                color: #fc6262;
                background: transparent;
                padding: 5px;
                width: 150px;
                display: inline-block;
                text-align: center;
                margin: 0 auto;
                transition: all .5s ease;
                -moz-transition: all .5s ease;
                -webkit-transition: all .5s ease;
                -o-transition: all .5s ease;
            }
            .boto:hover {
                background: #fc6262;
                color: #fff;
                cursor: pointer;
            }

            #coneix table,
            #poblacions table,
            #categories table,
            #ultims-cursos table,
            #noticies table {
                width: 100%;
                margin-bottom: 10px
            }
            #coneix table tr,
            #poblacions table tr,
            #categories table tr,
            #ultims-cursos table tr,
            #noticies table tr {
                padding-bottom: 5px
            }
            
            #coneix table tr td,
            #ultims-cursos table tr td,
            #noticies table tr td {
                padding: 10px;
                background: #fff;
                border-bottom: 4px solid #f8f8f8;
            }
            
            #noticies table tr td {
                border-bottom: 20px solid #f8f8f8;
            }

            #ultims-cursos .curs {
                font-size: 13px;
            }

            #ultims-cursos .curs .nom {
                width: 45%;
            }
            #ultims-cursos .curs .poblacio {
                width: 30%;
            }
            #ultims-cursos .curs .duracio {
                width: 20%;
            }
            #ultims-cursos .curs .veure,
            #noticies .noticia .veure {
                width: 5%;
            }
            #ultims-cursos .curs .veure a.boto,
            #noticies .noticia .veure a.boto {
                width: 75px;
            }
            #ultims-cursos .curs .icona {
                width: 14px;
                height: 14px;
                margin-right: 5px;
            }

            #categories .boto,
            #poblacions .boto {
                color: #000;
                border: none;
                font-weight: bold;
                width: auto;
            }

            #categories a.boto:hover,
            #poblacions a.boto:hover {
                text-decoration: underline;
                color: #000;
                background: #fff;
            }

            #coneix .text {
                width: 60%;
                padding: 10px 15px;
            }
            #coneix .imatge {
                width: 40%;
            }
            #coneix .text p {
                font-weight: 100;
                font-size: 14px;
                line-height: 23px;
            }
            #coneix .imatge img {
                width: auto;
            }
            #coneix .veure {
                padding-top: 20px;
                text-align: center;
            }
        </style>

        <center>

        <table id='header' style='padding: 10px 20px;'>
            <tr>
                <td>
                    formaBAGES
                </td>
                <td class='data'>
                    $title
                </td>
            </tr>
        </table>
        ";


        // Últims cursos
        $html .= "
        <table id='ultims-cursos' style='padding: 15px 20px;'>
            <tr>
                <td class='header'>
                    Últims cursos publicats
                </td>
            </tr>
        ";

        foreach ($categories as $cat) :
            if (empty($cat['ultims_cursos'])) continue;

            $html .= "
            <tr>
                <td class='header-categoria' style='padding: 15px 20px;'>
                    {$cat['title']}
                </td>
            </tr>
            ";

            $html .= "
            <tr>
            <td>
            <table cellspacing=0>
            ";

            foreach ($cat['ultims_cursos'] as $curs) :
                $html .= "
                <tr class='curs'>
                    <td class='nom'>{$curs->title}</td>
                    <td class='poblacio'>
                ";

                if (!empty($curs->poblacio)) {
                    $html .= "<img class='icona' width='14px' height='14px' src='{$template_url}images/location.png'>&nbsp;&nbsp;{$curs->poblacio}";
                }

                $html .= "
                    </td>
                    <td class='duracio'>
                ";

                if (!empty($curs->duracio)) {
                    $html .= "<img class='icona' width='14px' height='14px' src='{$template_url}images/time.png'>&nbsp;&nbsp;" . str_replace('hores', 'h', $curs->duracio);
                }

                $html .= "
                    </td>
                    <td class='veure'>
                        <a class='boto' style='padding: 5px;' href='{$curs->url}' target='_blank'>Veure</a>
                    </td>
                </tr>
                ";
            endforeach;

            $html .= "
            </table>
            </td>
            </tr>
            ";

            $html .= "
            <tr>
                <td class='veure-categoria'>
                    <a class='boto' style='padding: 5px;' href='{$cat['url']}' target='_blank'>Veure més</a>
                </td>
            </tr>
            ";
        endforeach;

        $html .= "
        </table>
        ";
        // end Últims cursos

        // Categories
        $html .= "
        <table id='categories' style='padding: 10px;'>
            <tr>
                <td class='header' style='padding: 10px;'>
                    Consulta totes les categories i descobreix els cursos que millor s'adapten a tu
                </td>
            </tr>
            <tr>
                <td style='padding: 10px;'>
        ";

        $cont = 1;
        foreach ($categories as $cat) :
            if ($cat['count'] <= 0) continue;
            $html .= "<a class='boto' style='display:inline-block;padding: 5px;' href='{$cat['url']}' target='_blank'>{$cat['title']} ({$cat['count']})</a>";
            if ($cont < count($categories)) $html .= "&nbsp;&nbsp;&middot;&nbsp;&nbsp;";
            $cont++;
        endforeach;

        $html .= "
        </td></tr>
        </table>
        ";
        // end Categories

        // Poblacions
        $html .= "
        <table id='poblacions' style='padding: 10px;'>
            <tr>
                <td class='header' style='padding: 10px;'>
                    Trobem els cursos que es realitzen aprop teu
                </td>
            </tr>
            <tr>
                <td style='padding: 10px;'>
        ";

        $cont = 1;
        foreach ($poblacions as $pob) :
            $html .= "<a class='boto' style='display:inline-block;padding: 5px;' href='{$pob['url']}' target='_blank'>{$pob['title']}</a>";
            if ($cont < count($poblacions)) $html .= "&nbsp;&nbsp;&middot;&nbsp;&nbsp;";
            $cont++;
        endforeach;

        $html .= "
        </td></tr>
        </table>
        ";
        // end Poblacions

        // Coneix formaBages
        $html .= "
        <table id='coneix' style='padding: 10px;'>
            <tr>
                <td class='header' style='padding: 10px;' colspan=2>
                    Coneix tota l'oferta formativa del Bages
                </td>
            </tr>
            <tr>
                <td class='text' style='padding: 10px 15px;'>
                    <p style='line-height: 23px;'>
                        FormaBAGES és la plataforma online impulsada pel Consell Comarcal del Bages i més de 35 entitats de formació, administracions, entitats i agrupacions vinculades amb la comarca, que té l'objectiu de potenciar la difusió d'una part molt rellevant de l'oferta formativa de la comarca.
                    </p>
                    <p style='line-height: 23px;'>
                        A FormaBAGES hi trobaràs tots els cursos de Formació Professional, OCupacional i Continua, Ensenyaments Artístics, Esportius i d'Idiomes, Música i Dansa, Programes d'Inserció, Itineraris Específics, Cursos d'accés i Educació per a Adults.
                    </p>
                    <p style='line-height: 23px;'>
                        Consulta la plataforma i orienta el teu futur professional amb FormaBAGES!
                    </p>
                </td>
                <td class='imatge' style='padding: 10px;'>
                    <img src='{$template_url}images/newsletter_image.png'>
                </td>
            </tr>
            <tr>
                <td class='veure' colspan=2 style='padding: 10px;padding-top: 20px;'>
                    <a class='boto' style='padding: 5px;' href='" . URL::to('/') . "' target='_blank'>Coneix FormaBAGES</a>
                </td>
            </tr>
        </table>
        ";
        // end Coneix formaBages


        // Noticies
        $html .= "
        <table id='noticies' style='padding: 10px;'>
            <tr>
                <td class='header' style='padding: 10px;'>
                    Últimes notícies
                </td>
            </tr>
        ";

        $html .= "
            <tr>
            <td>
            <table cellspacing=0>
            ";

        foreach ($lastNews as $lastNew) :
            $html .= "
                <tr class='noticia'>
                    <td>
                        <div class='noticia-header'>
                            {$lastNew->title}
                        </div>
                        ";
            if ($lastNew->data_publicacio) {
                $html .= "<div class='noticia-date'>
                            {$lastNew->data_publicacio}
                        </div>
                        ";
            }

            $html .=    "<p>
                            {$lastNew->body}
                        </p>
                        <a href='{$lastNew->url}' target='_blank'>Més informació &nbsp;&nbsp;<img class='icona' width='12px' height='6px' src='{$template_url}images/arrow-01.png'></a>
                    </td>
                </tr>
                ";
        endforeach;

        $html .= "
            </table>
            </td>
            </tr>
        </table>
            ";
        // end Noticies

        $html .= "
        </center>

        </body>
        </html>";

        return array('ok' => $ok, 'plantilla' => $html, 'title' => $title);
    }

}
