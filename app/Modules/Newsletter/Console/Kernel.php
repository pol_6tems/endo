<?php

namespace App\Modules\Newsletter\Console;


use Illuminate\Console\Scheduling\Schedule;
use App\Modules\Newsletter\Controllers\NewsletterController;
use App\Modules\Newsletter\Console\Commands\SendNewsletter;

class Kernel
{

    /**
     * The Artisan commands provided by your module.
     *
     * @var array
     */
    protected $commands = [
        SendNewsletter::class
    ];


    public function getCommands()
    {
        return $this->commands;
    }


    /**
     * Define the module's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule)
    {
        //$newsletter_cron = NewsletterController::get_opcion("newsletter_cron_config");
        //if ($newsletter_cron == FALSE) $newsletter_cron = "30 11 1,15 * *";

        //$schedule->command('send:newsletter')->cron($newsletter_cron);

        /*$schedule->command('send:newsletter')->weekly()->mondays()->when(function () {
            return date('W') % 2;
        })->at("00:00");*/

        $schedule->command('send:newsletter')->cron('0 8 1,15 * *');

        //$schedule->command('send:newsletter')->everyThirtyMinutes();
    }
}