<?php

namespace App\Modules\Newsletter\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Newsletter\Controllers\NewsletterController;

class SendNewsletter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:newsletter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar Newsletter cada 2 setmanes o estipulat a les opcions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        NewsletterController::send_newsletter();
    }
}
