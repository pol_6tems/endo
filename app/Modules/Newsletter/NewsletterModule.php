<?php

namespace App\Modules\Newsletter;

use Exception;
use App\Models\Admin\AdminMenu;
use App\Models\Admin\AdminMenuSub;

class NewsletterModule {

    const MODULE_NAME = 'Newsletter';
    const MODULE_ROUTE_ADMIN = 'admin.newsletter';
    const MODULE_ROUTE = 'newsletter';

    public static function get_path() {
        return 'Modules\\' . self::MODULE_NAME . '\\';
    }

    // INSTALL
    public static function install() {
        try {

            self::make_menu();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // DESTROY
    public static function reset() {
        try {

            self::remove_menu();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // RESET
    public static function refresh() {
        try {

            self::remove_menu();

            self::make_menu();

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function make_menu() {
        if ( !AdminMenuSub::where('url', self::MODULE_ROUTE_ADMIN . '.index')->exists() ) {
            AdminMenuSub::create([
                'menu_id' => 1,
                'name' => 'Newsletter',
                'icon' => 'email',
                'url_type' => 'route',
                'url' => self::MODULE_ROUTE_ADMIN . '.index',
                'controller' => self::get_path() . 'Controllers\\Admin\\Admin' . self::MODULE_NAME . 'Controller',
                'order' => 80,
                'parameters' => NULL,
            ]);
        }
    }

    public static function remove_menu() {
        return AdminMenuSub::where('url', self::MODULE_ROUTE_ADMIN . '.index')->delete();
    }
    
}