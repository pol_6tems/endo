@extends('Admin::layouts.admin')

@section('section-title')
@Lang('Newsletter')
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-primary card-header-icon">
				<div class="card-icon">
					<i class="material-icons">ballot</i>
				</div>
				<h4 class="card-title">
					@Lang('Options')
				</h4>
			</div>
			<div class="card-body">
				<div class="toolbar">
					<?php /*<button type="button" class="btn btn-primary btn-sm" onClick="window.location.href = '{{route('newsletter.test')}}';">@Lang('Test')</button>*/ ?>
				</div>
				<div class="material-datatables">
					<table id="tabla-traducciones" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
						<thead>
							<tr>
								<th>@Lang('Key')</th>
								<th>@Lang('Value')</th>
								<th>@Lang('Description')</th>
							</tr>
						</thead>
						<tbody>
							@foreach($items as $item)
							<tr>
								<td><a href="#" class="translate-key" data-title="" data-type="text" data-pk="{{ $item->key }}" data-url="{{ route('admin.opciones.update_key') }}">{{ $item->key }}</a></td>
								<td><a href="#" data-title="" class="translate value" data-code="{{ $item->value }}" data-type="textarea" data-pk="{{ $item->key }}" data-url="{{ route('admin.opciones.update_value') }}">{{ $item->value }}</a></td>
								<td><a href="#" data-title="" class="translate" data-code="{{ $item->descripcion }}" data-type="textarea" data-pk="{{ $item->key }}" data-url="{{ route('admin.opciones.update_desc') }}">{{ $item->descripcion }}</a></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('styles')
<link href="{{asset($_admin . 'css/bootstrap-editable.css')}}" rel="stylesheet" />
@endsection

@section('scripts')
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-editable.min.js')}}"></script>
<script type="text/javascript">
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});


	$('.translate').editable({
		params: function(params) {
			params.code = $(this).editable().data('code');
			return params;
		}
	});


	$('.translate-key').editable({
		validate: function(value) {
			if ($.trim(value) == '') {
				return 'Key is required';
			}
		}
	});
</script>
<script>
	$(document).ready(function() {
		$('.table').DataTable({
			"pagingType": "full_numbers",
			"lengthMenu": [
				[10, 25, 50, -1],
				[10, 25, 50, "@Lang('All')"]
			],
			responsive: true,
			language: {
				"url": "{{asset($datatableLangFile ?: 'js/datatables/Spanish.json')}}"
			}
		});

		var table = $('.table').DataTable();
	});
</script>
@endsection