<?php

namespace App\Modules\Newsletter;

class NewsletterModuleBasic {

    const MODULE_NAME = 'Newsletter';
    const MODULE_ROUTE_ADMIN = 'admin.newsletter';
    const MODULE_ROUTE = 'newsletter';    
}