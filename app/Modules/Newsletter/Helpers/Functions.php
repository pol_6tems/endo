<?php

use App\Modules\Newsletter\Controllers\NewsletterController;

function send_newsletter() {
    return NewsletterController::send_newsletter();
}