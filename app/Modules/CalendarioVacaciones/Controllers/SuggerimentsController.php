<?php

namespace App\Modules\CalendarioVacaciones\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Modules\CalendarioVacaciones\Models\User;
use App\Modules\CalendarioVacaciones\Models\Suggeriment;

class SuggerimentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();

        $item = new Suggeriment();
        $item->user_id = $user->id;
        $item->subject = $data['subject'];
        $item->message = $data['message'];

        if ( $id = $item->save() ) {
            return response()->json(['success'=>'Done!']);
        }
        return response()->json(['error'=>'Done!'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
