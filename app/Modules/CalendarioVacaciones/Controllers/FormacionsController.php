<?php

namespace App\Modules\CalendarioVacaciones\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Auth;

use App\Modules\CalendarioVacaciones\Models\User;
use App\Modules\CalendarioVacaciones\Models\Formacio;

class FormacionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $id = Auth::id();
        $data = $request->all();
        DB::enableQueryLog();

        $existeix = DB::table('formacions_usuaris')->where([
            ['formacio_id', intval($data['id'])],
            ['user_id', $id],
        ])->get();

        if (count($existeix) < 1) {

            $object = array();
            $object['user_id'] = $id;
            $object['formacio_id'] = $data['id'];
            $object['vista'] = '1';
            $object['created_at'] = Carbon::now();
            $object['updated_at'] = Carbon::now();

            DB::table('formacions_usuaris')->where('formacio_id', $data['id']);

            $result = DB::table('formacions_usuaris')->insert($object);

            return response()->json(['success' => true, 'result' => $result]);
        } else {
            return response()->json(['error'=>'Done!', 'result' => $existeix], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
