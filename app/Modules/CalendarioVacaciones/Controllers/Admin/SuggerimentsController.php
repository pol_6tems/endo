<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\CalendarioVacaciones\Models\Suggeriment;
use App\Modules\CalendarioVacaciones\Models\User;

class SuggerimentsController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Suggestions box');
        $this->section_icon = 'inbox';
        $this->section_route = 'admin.suggeriments';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Suggeriment::get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'subject',
            'headers' => [
                __('User') => [],
                __('Subject') => [],
                __('Sent') => [ 'width' => '15%' ],
            ],
            'rows' => [
                ['value' => ['user', 'email']],
                ['value' => 'subject'],
                ['value' => 'created_at', 'type' => 'date'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort('404');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = Suggeriment::findOrFail($id);
        return View('Admin::default.edit', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'item' => $item,
            'route' => $this->section_route,
            'field_name' => 'subject',
            'disabled' => true,
            'title_card' => $item->user->fullname(true) . ' (' . $item->user->email . ')',
            'rows' => [
                //['value' => ['user', 'email'], 'title' => __('User'), 'disabled' => true],
                ['value' => 'subject', 'title' => __('Subject'), 'disabled' => true],
                ['value' => 'message', 'title' => __('Message'), 'disabled' => true, 'type' => 'textarea', 'rows' => 10],
                ['value' => 'created_at', 'title' => __('Sent'), 'type' => 'date', 'disabled' => true],
            ],
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $item = Suggeriment::findOrFail($id);
        $item->delete();
        
        return redirect()->route($this->section_route . '.index')->with(['message' => __('Suggestion sent to trash')]);
    }

    public function massDestroy(Request $request)
    {
        $items = explode(',', $request->input('ids'));
        foreach ($items as $id) {
            $item = Suggeriment::findOrFail($id);
            $item->delete();
        }
        return redirect()->route($this->section_route . '.index')->with(['message' => __('Suggestion sent to trash')]);
    }

}
