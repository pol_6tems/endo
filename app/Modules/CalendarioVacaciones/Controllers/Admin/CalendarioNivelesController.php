<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivel;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivelYear;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivelWeek;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivelEncarregat;
use App\Modules\CalendarioVacaciones\Models\User;

class CalendarioNivelesController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Vacations') . ' - ' . __('Levels');
        $this->section_icon = 'date_range';
        $this->section_route = 'admin.calendario.niveles';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = CalendarioNivel::get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'headers' => [
                __('#') => [ 'width' => '10%' ],
                __('Name') => [],
                __('Selection') => [ 'width' => '15%' ],
                __('Blocked per request') => [ 'width' => '20%' ],
                __('Limit') => [ 'width' => '10%' ],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => 'name'],
                ['value' => 'sel_dia_o_semana', 'translate' => true],
                ['value' => 'no_bloquejat', 'type' => 'yes_no'],
                ['value' => 'max_limit'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'sel_dia_o_semana', 'title' => __('Selection'), 'type' => 'select', 
                    'choices' => [
                        ['value' => 'dia', 'title' => __('Day')],
                        ['value' => 'semana', 'title' => __('Week')],
                    ]
                ],
                ['value' => 'no_bloquejat', 'title' => __('Blocked per request'), 'type' => 'select', 
                    'choices' => [
                        ['value' => 1, 'title' => __('Yes')],
                        ['value' => 0, 'title' => __('No')],
                    ]
                ],
                ['value' => 'max_limit', 'title' => __('Limit'), 'type' => 'number'],
            ],
            
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        CalendarioNivel::create($data);
        return redirect()->route('admin.calendario.niveles.index')->with(['message' => __('Level added successfully')]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = CalendarioNivel::findOrFail($id);
        $years = CalendarioNivelYear::where('calendario_nivel_id', "=", $id)->orderBy('year', 'DESC')->get();
        $weeks = CalendarioNivelWeek::where('calendario_nivel_id', "=", $id)->orderBy('year', 'DESC')->orderBy('week', 'DESC')->get();
        $encarregats = CalendarioNivelEncarregat::where('calendario_nivel_id', "=", $id)->orderBy('user_id', 'ASC')->get();
        
        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'sel_dia_o_semana', 'title' => __('Selection'), 'type' => 'select', 
                    'choices' => [
                        ['value' => 'dia', 'title' => __('Day')],
                        ['value' => 'semana', 'title' => __('Week')],
                    ]
                ],
                ['value' => 'no_bloquejat', 'title' => __('Blocked per request'), 'type' => 'select', 
                    'choices' => [
                        ['value' => 1, 'title' => __('Yes')],
                        ['value' => 0, 'title' => __('No')],
                    ]
                ],
                ['value' => 'max_limit', 'title' => __('Limit'), 'type' => 'number'],
            ],
            'subs' => [
                [
                    'section_title' => __('Personalized holiday pending days'),
                    'section_icon' => $this->section_icon,
                    'items' => $years,
                    'route' => $this->section_route . '.years',
                    'field_name' => 'year',
                    'headers' => [
                        __('Year') => [],
                        __('Days') => [],
                    ],
                    'rows' => [
                        ['value' => 'year'],
                        ['value' => 'days'],
                    ],
                ],
                [
                    'section_title' => __('Employee limit per week'),
                    'section_icon' => $this->section_icon,
                    'items' => $weeks,
                    'route' => $this->section_route . '.weeks',
                    'field_name' => 'year',
                    'headers' => [
                        __('Year') => [],
                        __('Week') => [],
                        __('Limit') => [],
                    ],
                    'rows' => [
                        ['value' => 'year'],
                        ['value' => 'week'],
                        ['value' => 'max_limit'],
                    ],
                ],
                [
                    'section_title' => __('Managers'),
                    'section_icon' => $this->section_icon,
                    'items' => $encarregats,
                    'route' => $this->section_route . '.encarregat',
                    'field_name' => 'user->fullname()',
                    'headers' => [
                        __('Id') => ['width' => '10%'],
                        __('Manager') => [],
                        __('Email') => [],
                        __('Role') => ['width' => '15%'],
                    ],
                    'rows' => [
                        ['value' => 'user_id'],
                        ['value' => ['user', 'fullname()'] ],
                        ['value' => ['user', 'email'] ],
                        ['value' => ['user', 'role'], 'translate' => true],
                    ],
                ]
            ]
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $item = CalendarioNivel::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('admin.calendario.niveles.index')->with(['message' => __('Level updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $item = CalendarioNivel::findOrFail($id);
        $item->status = 'trash';
        $item->update();
        $item->delete();
        
        return redirect()->route('admin.calendario.niveles.index')->with(['message' => __('Level sent to trash')]);
    }

    public function massDestroy(Request $request)
    {
        $items = explode(',', $request->input('ids'));
        foreach ($items as $id) {
            $item = CalendarioNivel::findOrFail($id);
            $item->status = 'trash';
            $item->update();
            $item->delete();
        }
        return redirect()->route('admin.calendario.niveles.index')->with(['message' => __('Levels sent to trash')]);
    }

    /* CalendarioNivelYear */
    public function years_create($locale, $id)
    {
        return View('Admin::default.subcreate', array(
            'section_title' => __('Personalized holiday pending days'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.years',
            'route_parent' => $this->section_route,
            'rows' => [
                ['value' => 'calendario_nivel_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'year', 'title' => __('Year'), 'type' => 'number', 'required' => true],
                ['value' => 'days', 'title' => __('Days'), 'type' => 'number', 'required' => true],
            ],
        ));
    }

    public function years_store(Request $request)
    {
        $data = $request->all();
        CalendarioNivelYear::create($data);
        return redirect()->route('admin.calendario.niveles.edit', $data['calendario_nivel_id'])->with(['message' => __('Year added successfully')]);
    }

    public function years_edit($locale, $id)
    {
        $item = CalendarioNivelYear::findOrFail($id);
        return View('Admin::default.subedit', array(
            'item' => $item,
            'id_parent' => $item->calendario_nivel_id,
            'section_title' => __('Personalized holiday pending days'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.years',
            'route_parent' => $this->section_route,
            'rows' => [
                ['value' => 'calendario_nivel_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'year', 'title' => __('Year'), 'type' => 'number', 'required' => true],
                ['value' => 'days', 'title' => __('Days'), 'type' => 'number', 'required' => true],
            ],
        ));
    }

    public function years_update(Request $request, $locale, $id)
    {
        $item = CalendarioNivelYear::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('admin.calendario.niveles.edit', $item->calendario_nivel_id)->with(['message' => __('Year updated successfully')]);
    }

    public function years_destroy($locale, $id)
    {
        $item = CalendarioNivelYear::findOrFail($id);
        $item->delete();
        return redirect()->back()->with(['message' => __('Year deleted successfully')]);
    }
    /* end CalendarioNivelYear */

    /* CalendarioNivelWeek */
    public function weeks_create($locale, $id)
    {
        return View('Admin::default.subcreate', array(
            'section_title' => __('Employee limit per week'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.weeks',
            'route_parent' => $this->section_route,
            'rows' => [
                ['value' => 'calendario_nivel_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'year', 'title' => __('Year'), 'type' => 'number', 'required' => true],
                ['value' => 'week', 'title' => __('Week'), 'type' => 'number', 'required' => true],
                ['value' => 'max_limit', 'title' => __('Limit'), 'type' => 'number', 'required' => true],
            ],
        ));
    }

    public function weeks_store(Request $request)
    {
        $data = $request->all();
        CalendarioNivelWeek::create($data);
        return redirect()->route('admin.calendario.niveles.edit', $data['calendario_nivel_id'])->with(['message' => __('Week added successfully')]);
    }

    public function weeks_edit($locale, $id)
    {
        $item = CalendarioNivelWeek::findOrFail($id);
        return View('Admin::default.subedit', array(
            'item' => $item,
            'id_parent' => $item->calendario_nivel_id,
            'section_title' => __('Employee limit per week'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.weeks',
            'route_parent' => $this->section_route,
            'rows' => [
                ['value' => 'calendario_nivel_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'year', 'title' => __('Year'), 'type' => 'number', 'required' => true],
                ['value' => 'week', 'title' => __('Week'), 'type' => 'number', 'required' => true],
                ['value' => 'max_limit', 'title' => __('Limit'), 'type' => 'number', 'required' => true],
            ],
        ));
    }

    public function weeks_update(Request $request, $locale, $id)
    {
        $item = CalendarioNivelWeek::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('admin.calendario.niveles.edit', $item->calendario_nivel_id)->with(['message' => __('Week updated successfully')]);
    }

    public function weeks_destroy($locale, $id)
    {
        $item = CalendarioNivelWeek::findOrFail($id);
        $item->delete();
        return redirect()->back()->with(['message' => __('Week deleted successfully')]);
    }
    /* end CalendarioNivelWeek */
    
    /* CalendarioNivelEncarregat */
    public function encarregat_create($locale, $id)
    {
        $users = User::get()->sortby('id');
        return View('Admin::default.subcreate', array(
            'section_title' => __('Managers'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.encarregat',
            'route_parent' => $this->section_route,
            'rows' => [
                ['value' => 'calendario_nivel_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'user_id', 'title' => __('User'), 'type' => 'select', 'required' => true, 'sel_search' => true,
                    'datasource' => $users, 'sel_value' => 'id', 'sel_title' => ['fullname()', 'email']
                ],
            ],
        ));
    }

    public function encarregat_store(Request $request)
    {
        $data = $request->all();
        CalendarioNivelEncarregat::create($data);
        return redirect()->route('admin.calendario.niveles.edit', $data['calendario_nivel_id'])->with(['message' => __('Manager added successfully')]);
    }

    public function encarregat_edit($locale, $id)
    {
        $item = CalendarioNivelEncarregat::findOrFail($id);
        $users = User::get()->sortby('id');
        return View('calendario-vacaciones.admin.calendario.nivel.encarregat.edit', compact('item', 'users'));
    }

    public function encarregat_update(Request $request, $locale, $id)
    {
        $item = CalendarioNivelEncarregat::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('admin.calendario.niveles.edit', $item->calendario_nivel_id)->with(['message' => __('Manager updated successfully')]);
    }

    public function encarregat_destroy($locale, $id)
    {
        $item = CalendarioNivelEncarregat::findOrFail($id);
        $item->delete();
        return redirect()->back()->with(['message' => __('Manager deleted successfully')]);
    }
    /* end CalendarioNivelEncarregat */
}
