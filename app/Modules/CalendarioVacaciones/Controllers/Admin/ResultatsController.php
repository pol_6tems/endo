<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Admin\AdminController;
use App\Models\UserMeta;
use App\Post;
use Auth;

class ResultatsController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Surveys results');
        $this->section_icon = 'format_list_numbered';
        $this->section_route = 'admin.resultats';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post_type = Input::get('post_type');
        if ( empty($post_type) ) return View('Admin::errors.404');
        $items = Post::where('type', $post_type)->get()->all();
        foreach ( $items as $key => $item ) {
            $items[$key]->meta_count = UserMeta::where('post_id', $item->id)->count();
        }
        usort($items, function($a, $b) {
            return strcmp($a->meta_count, $b->meta_count);
        });
        return View('Admin::default.index', array(
            'readonly' => true,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => ['translate()', 'title'],
            'headers' => [
                __('#') => ['width' => '10%'],
                __('Title') => [],
                __('Results') => ['width' => '15%'],
            ],
            'rows' => [
                ['value' => 'id', 'name' => 'id'],
                ['value' => ['translate()', 'title'], 'name' => 'title'],
                ['value' => 'meta_count', 'name' => 'meta_count'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        $item = Post::findOrFail($id);
        return View('calendario-vacaciones.admin.resultats.show', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'item' => $item,
            'route' => $this->section_route,
            'toolbar' => [
                '<input type="button" class="btn btn-previous btn-fill btn-default btn-sm" name="previous" value="'.__('Tornar').'" onclick="window.location.href=\''.route($this->section_route . '.index', ['post_type='.$item->type]).'\'">',
            ],
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
