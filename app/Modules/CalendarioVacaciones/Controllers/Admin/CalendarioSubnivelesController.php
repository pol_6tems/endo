<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivel;
use App\Modules\CalendarioVacaciones\Models\CalendarioSubnivel;
use App\Modules\CalendarioVacaciones\Models\CalendarioSubnivelYear;
use App\Modules\CalendarioVacaciones\Models\CalendarioSubnivelWeek;

class CalendarioSubnivelesController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Vacations') . ' - ' . __('Sublevels');
        $this->section_icon = 'date_range';
        $this->section_route = 'admin.calendario.subniveles';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = CalendarioSubnivel::paginate(20);
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'headers' => [
                __('#') => [ 'width' => '10%' ],
                __('Name') => [],
                __('Level') => [],
                __('Selection') => [ 'width' => '15%' ],
                __('Blocked per request') => [ 'width' => '20%' ],
                __('Limit') => [ 'width' => '10%' ],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => 'name'],
                ['value' => ['nivel', 'name']],
                ['value' => 'sel_dia_o_semana', 'translate' => true],
                ['value' => 'no_bloquejat', 'type' => 'yes_no'],
                ['value' => 'max_limit'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $niveles = CalendarioNivel::get();
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'calendario_nivel_id', 'title' => __('Level'), 'type' => 'select', 'required' => true,
                    'datasource' => $niveles, 'sel_value' => 'id', 'sel_title' => ['name'], 'sel_search' => true
                ],
                ['value' => 'sel_dia_o_semana', 'title' => __('Selection'), 'type' => 'select', 
                    'choices' => [
                        ['value' => 'dia', 'title' => __('Day')],
                        ['value' => 'semana', 'title' => __('Week')],
                    ]
                ],
                ['value' => 'no_bloquejat', 'title' => __('Blocked per request'), 'type' => 'select', 
                    'choices' => [
                        ['value' => 1, 'title' => __('Yes')],
                        ['value' => 0, 'title' => __('No')],
                    ]
                ],
                ['value' => 'max_limit', 'title' => __('Limit'), 'type' => 'number'],
            ],
            
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        CalendarioSubnivel::create($data);
        return redirect()->route('admin.calendario.subniveles.index')->with(['message' => __('Level added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = CalendarioSubnivel::findOrFail($id);
        $niveles = CalendarioNivel::get();
        $years = CalendarioSubnivelYear::where('calendario_subnivel_id', "=", $id)->orderBy('year', 'DESC')->paginate(20);
        $weeks = CalendarioSubnivelWeek::where('calendario_subnivel_id', "=", $id)->orderBy('year', 'DESC')->orderBy('week', 'DESC')->paginate(20);
        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'calendario_nivel_id', 'title' => __('Level'), 'type' => 'select', 'required' => true,
                    'datasource' => $niveles, 'sel_value' => 'id', 'sel_title' => ['name'], 'sel_search' => true
                ],
                ['value' => 'sel_dia_o_semana', 'title' => __('Selection'), 'type' => 'select', 
                    'choices' => [
                        ['value' => 'dia', 'title' => __('Day')],
                        ['value' => 'semana', 'title' => __('Week')],
                    ]
                ],
                ['value' => 'no_bloquejat', 'title' => __('Blocked per request'), 'type' => 'select', 
                    'choices' => [
                        ['value' => 1, 'title' => __('Yes')],
                        ['value' => 0, 'title' => __('No')],
                    ]
                ],
                ['value' => 'max_limit', 'title' => __('Limit'), 'type' => 'number'],
            ],
            'subs' => [
                [
                    'section_title' => __('Personalized holiday pending days'),
                    'section_icon' => $this->section_icon,
                    'items' => $years,
                    'route' => $this->section_route . '.years',
                    'field_name' => 'year',
                    'headers' => [
                        __('Year') => [],
                        __('Days') => [],
                    ],
                    'rows' => [
                        ['value' => 'year'],
                        ['value' => 'days'],
                    ],
                ],
                [
                    'section_title' => __('Employee limit per week'),
                    'section_icon' => $this->section_icon,
                    'items' => $weeks,
                    'route' => $this->section_route . '.weeks',
                    'field_name' => 'year',
                    'headers' => [
                        __('Year') => [],
                        __('Week') => [],
                        __('Limit') => [],
                    ],
                    'rows' => [
                        ['value' => 'year'],
                        ['value' => 'week'],
                        ['value' => 'max_limit'],
                    ],
                ]
            ]
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $item = CalendarioSubnivel::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('admin.calendario.subniveles.index')->with(['message' => __('Level updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $item = CalendarioSubnivel::findOrFail($id);
        $item->status = 'trash';
        $item->update();
        $item->delete();
        
        return redirect()->route('admin.calendario.subniveles.index')->with(['message' => __('Level sent to trash')]);
    }

    public function massDestroy(Request $request)
    {
        $items = explode(',', $request->input('ids'));
        foreach ($items as $id) {
            $item = CalendarioSubnivel::findOrFail($id);
            $item->status = 'trash';
            $item->update();
            $item->delete();
        }
        return redirect()->route('admin.calendario.subniveles.index')->with(['message' => __('Levels sent to trash')]);
    }

    /* CalendarioSubnivelYear */
    public function years_create($locale, $id)
    {
        return View('Admin::default.subcreate', array(
            'section_title' => __('Personalized holiday pending days'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.years',
            'route_parent' => $this->section_route,
            'rows' => [
                ['value' => 'calendario_subnivel_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'year', 'title' => __('Year'), 'type' => 'number', 'required' => true],
                ['value' => 'days', 'title' => __('Days'), 'type' => 'number', 'required' => true],
            ],
        ));
    }

    public function years_store(Request $request)
    {
        $data = $request->all();
        CalendarioSubnivelYear::create($data);
        return redirect()->route('admin.calendario.subniveles.edit', $data['calendario_subnivel_id'])->with(['message' => __('Year added successfully')]);
    }

    public function years_edit($locale, $id)
    {
        $item = CalendarioSubnivelYear::findOrFail($id);
        return View('Admin::default.subedit', array(
            'item' => $item,
            'id_parent' => $item->calendario_subnivel_id,
            'section_title' => __('Personalized holiday pending days'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.years',
            'route_parent' => $this->section_route,
            'rows' => [
                ['value' => 'calendario_subnivel_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'year', 'title' => __('Year'), 'type' => 'number', 'required' => true],
                ['value' => 'days', 'title' => __('Days'), 'type' => 'number', 'required' => true],
            ],
        ));
    }

    public function years_update(Request $request, $locale, $id)
    {
        $item = CalendarioSubnivelYear::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('admin.calendario.subniveles.edit', $item->calendario_subnivel_id)->with(['message' => __('Year updated successfully')]);
    }

    public function years_destroy($locale, $id)
    {
        $item = CalendarioSubnivelYear::findOrFail($id);
        $item->delete();
        return redirect()->back()->with(['message' => __('Year deleted successfully')]);
    }
    /* end CalendarioSubnivelYear */

    /* CalendarioSubnivelWeek */
    public function weeks_create($locale, $id)
    {
        return View('Admin::default.subcreate', array(
            'section_title' => __('Employee limit per week'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.weeks',
            'route_parent' => $this->section_route,
            'rows' => [
                ['value' => 'calendario_subnivel_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'year', 'title' => __('Year'), 'type' => 'number', 'required' => true],
                ['value' => 'week', 'title' => __('Week'), 'type' => 'number', 'required' => true],
                ['value' => 'max_limit', 'title' => __('Limit'), 'type' => 'number', 'required' => true],
            ],
        ));
    }

    public function weeks_store(Request $request)
    {
        $data = $request->all();
        CalendarioSubnivelWeek::create($data);
        return redirect()->route('admin.calendario.subniveles.edit', $data['calendario_subnivel_id'])->with(['message' => __('Week added successfully')]);
    }

    public function weeks_edit($locale, $id)
    {
        $item = CalendarioSubnivelWeek::findOrFail($id);
        return View('Admin::default.subedit', array(
            'item' => $item,
            'id_parent' => $item->calendario_subnivel_id,
            'section_title' => __('Employee limit per week'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.weeks',
            'route_parent' => $this->section_route,
            'rows' => [
                ['value' => 'calendario_subnivel_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'year', 'title' => __('Year'), 'type' => 'number', 'required' => true],
                ['value' => 'week', 'title' => __('Week'), 'type' => 'number', 'required' => true],
                ['value' => 'max_limit', 'title' => __('Limit'), 'type' => 'number', 'required' => true],
            ],
        ));
    }

    public function weeks_update(Request $request, $locale, $id)
    {
        $item = CalendarioSubnivelWeek::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('admin.calendario.subniveles.edit', $item->calendario_subnivel_id)->with(['message' => __('Week updated successfully')]);
    }

    public function weeks_destroy($locale, $id)
    {
        $item = CalendarioSubnivelWeek::findOrFail($id);
        $item->delete();
        return redirect()->back()->with(['message' => __('Week deleted successfully')]);
    }
    /* end CalendarioSubnivelWeek */
}
