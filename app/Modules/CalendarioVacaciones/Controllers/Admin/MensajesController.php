<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Admin;

use Illuminate\Http\Request;
use App\Notifications\NuevoMensajeNotification;
use App\Http\Controllers\Admin\MensajesController as BaseMensajesController;
use App\Models\Mensaje;
use Auth;
use DB;
use App\Modules\CalendarioVacaciones\Models\User;

class MensajesController extends BaseMensajesController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Contact') . ' ' . __('Messages');
        $this->section_icon = 'message';
        $this->section_route = 'admin.mensajes';
    }

    public function index()
    {
        $users = User::whereHas(
            'contactar_mensajes', function ($query) {
                $query->whereNotNull('mensaje');
            }
        )
        ->leftJoin('mensajes', 'users.id', '=', 'mensajes.user_id')
        ->groupBy(['users.id', 'users.name', 'users.email', 'users.avatar'])
        ->orderByRaw('max(mensajes.updated_at) desc')
        ->select(['users.id', 'users.name', 'users.email', 'users.avatar'])
        ->get();
        
        return view('calendario-vacaciones.admin.messaging', array(
            'users' => $users,
            //'items' => $items,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'user_class' => new User(),
            'user_messages' => 'contactar_mensajes',
        ));
    }
}
