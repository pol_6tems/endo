<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController as AdminControllerBase;
use App\Modules\CalendarioVacaciones\Models\CalendarioVacacion;
use App\Modules\CalendarioVacaciones\Models\User;
use App\Modules\CalendarioVacaciones\Models\CalendarioMensaje;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivelEncarregat;
use App\Models\Mensaje;
use Auth;

class AdminController extends AdminControllerBase
{
    public function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $widgets = [];
        $widgets[] = $this->widget_last_requests([
            'status' => 'pending',
            'name' => 'last_pendings',
            'title' => __('Last pending requests'),
            'icon' => 'date_range',
            'icon_class' => 'info',
            'headers' => [__('Name'), __('Email'), __('Level'), __('Date')],
        ]);
        $widgets[] = $this->widget_last_requests([
            'status' => 'approved',
            'name' => 'last_approveds',
            'title' => __('Last approved requests'),
            'icon' => 'date_range',
            'icon_class' => 'success',
            'headers' => [__('Name'), __('Email'), __('Level'), __('Date')],
        ]);
        $widgets[] = $this->widget_last_requests([
            'status' => 'denied',
            'name' => 'last_denieds',
            'title' => __('Last denied requests'),
            'icon' => 'date_range',
            'icon_class' => 'yellow',
            'headers' => [__('Name'), __('Email'), __('Level'), __('Date')],
        ]);
        return View('Admin::index', compact('widgets'));
    }

    public static function widget_last_requests($params) {
        if ( empty(Auth::user()) ) return array();

        $es_admin = Auth::user()->rol->level > 90;
        $nivells_encarregats = array();
        $r = CalendarioNivelEncarregat::where('user_id', '=', Auth::user()->id)->get();
        if ( !$r->isEmpty() ) foreach ( $r as $r1 ) $nivells_encarregats[] = $r1->calendario_nivel_id;
        else if ( Auth::user()->rol->level < 90 ) return array();

        $widget = new \stdClass();
        $widget->status = !empty($params['status']) ? $params['status'] : '';
        $widget->name = !empty($params['name']) ? $params['name'] : '';
        $widget->title = !empty($params['title']) ? $params['title'] : '';
        $widget->icon = !empty($params['icon']) ? $params['icon'] : '';
        $widget->icon_class = !empty($params['icon_class']) ? $params['icon_class'] : '';
        $widget->headers = !empty($params['headers']) ? $params['headers'] : [];
        $widget->rows = [];
        $items = \DB::table("calendario_vacaciones")
        ->select( \DB::raw("user_id, date_format(start, '%Y-%m') AS mes, MIN(start) AS min_start, MAX(end) AS max_end, MAX(updated_at) as last_updated") )
        ->where( 'status', $widget->status )
        ->groupBy(['user_id', 'mes'])
        ->orderByRaw("last_updated DESC, min_start ASC, user_id ASC")
        ->limit( ( isset($params['limit']) ? $params['limit'] : 10 ) )
        ->get();

        foreach ( $items as $item ) {
            $es_encarregat = false;
            $user = User::find($item->user_id);
            if ( $user == null || !$user->exists() ) continue;
            if ( $user->subnivel != null && $user->subnivel->exists() ) {
                $es_encarregat = in_array($user->subnivel->id, $nivells_encarregats) || $es_admin;
                $nivel = $user->subnivel->name;
            } else if ( $user->nivel != null && $user->nivel->exists() ) {
                $es_encarregat = in_array($user->nivel->id, $nivells_encarregats) || $es_admin;
                $nivel = $user->nivel->name;
            } else $nivel = '-';
            
            if ( !$es_encarregat ) continue;

            $start = \Carbon\Carbon::parse($item->min_start)->format('d/m/Y');
            $end = \Carbon\Carbon::parse($item->max_end)->format('d/m/Y');
            $dates = $start != $end ? $start.' - '.$end : $start;
            $widget->rows[] = [
                $user->fullname(true),
                $user->email,
                $nivel,
                $dates,
            ];
        }
        return $widget;
    }

    public static function get_notificacio($param) {
        if ( $param == 'pending' ) {
            $pendings = self::widget_last_requests([
                'limit' => -1,
                'status' => 'pending',
                'name' => 'last_pendings',
                'title' => __('Last pending requests'),
                'icon' => 'date_range',
                'icon_class' => 'info',
                'headers' => [__('Name'), __('Email'), __('Level'), __('Date')],
            ]);
            if ( empty($pendings) ) return null;
            return [
                'msg' => __('You have :number pending requests', ['number' => count($pendings->rows)]),
                'href' => route('admin.vacaciones.index', ['locale' => \App::getLocale()]),
            ];
        } else if ( $param == 'calendario_messages' ) {
            $mensajes_calendario = CalendarioMensaje::where('visto', false)->get();
            if ( empty($mensajes_calendario) ) return null;
            return [
                'msg' => __('You have :number unread messages (calendar)', ['number' => $mensajes_calendario->count()]),
                'href' => route('admin.calendario.mensajes.index', ['locale' => \App::getLocale()]),
            ];
        } else if ( $param == 'contact_messages' ) {
            $mensajes = Mensaje::where('visto', false)->get();
            if ( empty($mensajes) ) return null;
            return [
                'msg' => __('You have :number unread messages', ['number' => $mensajes->count()]),
                'href' => route('admin.mensajes.index', ['locale' => \App::getLocale()]),
            ];
        } else {
            return null;
        }
    }

}
