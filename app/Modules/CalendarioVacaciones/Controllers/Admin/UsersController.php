<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\CalendarioVacaciones\Models\User;
use Validator;
use Auth;
use App\Models\Admin\Rol;
use App\Modules\CalendarioVacaciones\Models\Empleado;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivel;
use App\Modules\CalendarioVacaciones\Models\CalendarioSubnivel;
use App\Modules\CalendarioVacaciones\Models\CalendarioUserYear;

class UsersController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Users');
        $this->section_icon = 'person_pin';
        $this->section_route = 'admin.users';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = User::get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'email',
            'toolbar_header' => [
                '<a target="_blank" href="' . route('admin.users.exportar') . '" class="btn btn-default btn-sm">' . __('Export') . '</a>',
            ],
            'headers' => [
                __('#') => [ 'width' => '5%' ],
                __('Name') => [],
                __('Email') => [],
                __('ID card') => [],
                __('Created at') => [ 'width' => '15%' ],
                __('Role') => [],
                __('Level') => [],
                __('Sublevel') => [],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => ['fullname()']],
                ['value' => 'email'],
                ['value' => 'dni'],
                ['value' => 'created_at', 'type' => 'date'],
                ['value' => 'role', 'translate' => true],
                ['value' => ['nivel', 'name']],
                ['value' => ['subnivel', 'name']],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empleados = Empleado::whereNull('user_id')->get()->sortBy('nombre');
        $niveles = CalendarioNivel::get()->sortby('name');
        $subniveles = CalendarioSubnivel::get()->sortby('name');
        $level = Auth::user()->rol->level;
        $rols = Rol::where('level', '<=', $level)->get()->sortby('name');
        return View('calendario-vacaciones.admin.administracion.users.create', compact(
            'empleados', 'niveles', 'subniveles', 'rols'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if ( $request->isMethod('post') ) {
			//Roles de validación
			$rules = [
				'name' => 'required|min:3',
				'email' => 'required|email|max:255|unique:users,email',
				'password' => 'required|min:6|confirmed',
			];

			//Posibles mensajes de error de validación
			$messages = [
				'name.required' => 'El campo es requerido',
				'name.min' => 'El mínimo de caracteres permitidos son 3',
				'name.max' => 'El máximo de caracteres permitidos son 16',
				'name.regex' => 'Sólo se aceptan letras',
				'email.required' => 'El campo es requerido',
				'email.email' => 'El formato de email es incorrecto',
				'email.max' => 'El máximo de caracteres permitidos son 255',
				'email.unique' => 'El email ya existe',
				'password.required' => 'El campo es requerido',
				'password.min' => 'El mínimo de caracteres permitidos son 6',
				'password.max' => 'El máximo de caracteres permitidos son 18',
				'password.confirmed' => 'Los passwords no coinciden',
			];

			$validator = Validator::make($request->all(), $rules, $messages);

			//Si la validación no es correcta redireccionar al formulario con los errores
			if ( $validator->fails() ){
				return redirect()->back()->withErrors($validator);
			}
			else { // De los contrario guardar al usuario
				$user = new User;
				$user->name = $request->name;
				$user->lastname = $request->lastname;
				$user->email = $request->email;
				$user->password = bcrypt($request->password);
                $user->remember_token = str_random(100);
                $user->role = $request->role;
                
                $user->dni = $request->dni;
                $user->calendario_nivel_id = $request->calendario_nivel_id;
                $user->calendario_subnivel_id = $request->calendario_subnivel_id;

				if ( $user->save() ) {
                    $empleado = Empleado::where('dni', $user->dni)->first();
                    if ( !empty($empleado) ) {
                        $empleado->user_id = $user->id;
                        $empleado->update();
                    }
                    return redirect()->route('admin.users.index')->with('message', __('User added successfully'));
				} else {
					return redirect()->back()->with('error', __('An error occurred while saving the data'));
				}
			}
		}
		return View('calendario-vacaciones.admin.administracion.users.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = User::find($id);
        $empleados = Empleado::whereNull('user_id')->get()->sortBy('nombre');
        $years = CalendarioUserYear::where('user_id', "=", $id)->orderBy('year', 'DESC')->paginate(20);
        $niveles = CalendarioNivel::get()->sortby('name');
        $subniveles = CalendarioSubnivel::get()->sortby('name');
        $level = Auth::user()->rol->level;
        $rols = Rol::where('level', '<=', $level)->get()->sortby('name');
        return View('calendario-vacaciones.admin.administracion.users.edit', compact(
            'item', 'empleados', 'years', 'niveles', 'subniveles', 'rols'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $user = User::find($id);

        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->role = $request->role;

        $user->dni = $request->dni;
        $user->calendario_nivel_id = $request->calendario_nivel_id;
        $user->calendario_subnivel_id = $request->calendario_subnivel_id;
        $user->city = $request->city;
        $user->consentiment_naixement = ($request->consentiment_naixement) ? 1 : 0;
        $user->consentiment_privacitat = ($request->consentiment_privacitat) ? 1 : 0;
        $user->lopd = ($request->lopd) ? 1 : 0;
        $user->address = $request->address;
        $user->fecha_nacimiento = $request->fecha_nacimiento;
        $user->codigo_postal = $request->codigo_postal;
        $user->mobile = $request->mobile;
        $user->phone = $request->phone;

        if ( !empty($request->password) ) $user->password = bcrypt($request->password);

        $user->update();

        return redirect()->route('admin.users.index')->with('message',  __('User updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $rows = User::destroy($id);
		if ( $rows > 0 ) {
            // Desasignar user_id a Empleado
            $empleado = Empleado::where('user_id', $id)->first();
            if ( !empty($empleado) ) {
                $empleado->user_id = null;
                $empleado->update();
            }
            return redirect()->back()->with('message', __('User deleted successfully'));
        }
        else return redirect()->back()->with('error', __('An error occurred while deleting the data'));
    }

    /* UserYear */
    public function years_create($locale, $id)
    {
        return view('calendario-vacaciones.admin.administracion.users.years.create', compact('id'));
    }

    public function years_store(Request $request)
    {
        $data = $request->all();
        CalendarioUserYear::create($data);
        return redirect()->route('admin.users.edit', $data['user_id'])->with(['message' => __('Year added successfully')]);
    }

    public function years_edit($locale, $id)
    {
        $item = CalendarioUserYear::findOrFail($id);
        return view('calendario-vacaciones.admin.administracion.users.years.edit', array('item' => $item));
    }

    public function years_update(Request $request, $locale, $id)
    {
        $item = CalendarioUserYear::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('admin.users.edit', $data['user_id'])->with(['message' => __('Year updated successfully')]);
    }

    public function years_destroy($locale, $id, $user_id)
    {
        $item = CalendarioUserYear::findOrFail($id);
        $item->delete();
        return redirect()->route('admin.users.edit', $user_id)->with(['message' => __('Year deleted successfully')]);
    }
    /* end UserYear */

    public function exportar()
    {
        \Excel::create(__('Users'), function($excel) {
            $users = User::where('email', 'NOT LIKE', '%@6tems.com%')->get();

            $excel->sheet('Users', function($sheet) use($users) {
                
                //$sheet->fromArray($users);
                $sheet->row(1, [
                    __('Name'), __('Lastname'), __('Email'), __('Birthdate'), __('ID card'), __('City'),
                    __('Address'), __('ZIP zone'), __('Mobile'), __('Phone'), __('Level'), __('Privacy policy'),
                    __('Consent of birthday')
                ]);
                foreach($users as $index => $user) {
                    $nivel_name = '';
                    if ( $user->nivel()->exists()  ) $nivel_name = $user->nivel->name;
                    else if ( $user->subnivel()->exists() ) $nivel_name = $user->subnivel->name;
                    $sheet->row($index+2, [
                        $user->name, $user->lastname, $user->email, 
                        \Carbon\Carbon::parse($user->fecha_nacimiento)->format('d/m/Y'), 
                        $user->dni, $user->city, $user->address, $user->codigo_postal, $user->mobile, $user->phone,
                        $nivel_name,
                        ($user->consentiment_privacitat ? __('Yes') : __('No')), 
                        ($user->consentiment_naixement ? __('Yes') : __('No')),
                    ]);	
                }
         
            });
         
        })->export('xlsx');
    }
}
