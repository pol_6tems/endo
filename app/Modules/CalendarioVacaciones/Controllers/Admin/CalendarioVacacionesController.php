<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use Auth;
use App\Modules\CalendarioVacaciones\Models\User;
use App\Modules\CalendarioVacaciones\Models\CalendarioVacacion;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivel;
use App\Modules\CalendarioVacaciones\Models\CalendarioSubnivel;
use App\Modules\CalendarioVacaciones\Notifications\PeticionVacacionNotification;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivelEncarregat;
use App\Modules\CalendarioVacaciones\Models\CalendarioVetado;
use App\Modules\CalendarioVacaciones\Models\CalendarioFestivo;
use App\Models\Opcion;

class CalendarioVacacionesController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* CalendarioNivelEncarregat */
        $nivells_encarregats = array();
        $r = CalendarioNivelEncarregat::where('user_id', '=', Auth::user()->id)->get();
        if ( !$r->isEmpty() ) foreach ( $r as $r1 ) $nivells_encarregats[] = $r1->calendario_nivel_id;
        else if ( Auth::user()->rol->level < 90 ) return abort(403);
        /* end CalendarioNivelEncarregat */
        
        $any = date('Y');
        $any_anterior = $any - 1;
        $any_seguent = $any + 2;

        // Pendientes
        $pendientes = CalendarioVacacion::where([['year', '>=', $any_anterior], ['year', '<=', $any_seguent],['status', '=', 'pending']])->get();
        foreach ($pendientes as $key => $item) {
            $user_aux = User::find($item->user_id);
            if ( empty($user_aux) ) continue;
            $item->user_name = $user_aux->name . " " . $user_aux->lastname;
            $item->user_level_id = 'nivel-1';
            if ( $user_aux->calendario_subnivel_id > 0 ) $item->user_level_id = 'subnivel-' . $user_aux->calendario_subnivel_id;
            else if ( $user_aux->calendario_nivel_id > 0 ) $item->user_level_id = 'nivel-' . $user_aux->calendario_nivel_id;
            $pendientes[$key] = $item;
        }

        // Aprovados
        $aprovados = CalendarioVacacion::where([['year', '>=', $any_anterior], ['year', '<=', $any_seguent],['status', '=', 'approved']])->get();
        foreach ($aprovados as $key => $item) {
            $user_aux = User::find($item->user_id);
            if ( empty($user_aux) ) continue;
            $item->user_name = $user_aux->name . " " . $user_aux->lastname;
            $item->user_level_id = 'nivel-1';
            if ( $user_aux->calendario_subnivel_id > 0 ) $item->user_level_id = 'subnivel-' . $user_aux->calendario_subnivel_id;
            else if ( $user_aux->calendario_nivel_id > 0 ) $item->user_level_id = 'nivel-' . $user_aux->calendario_nivel_id;
            $aprovados[$key] = $item;
        }

        // Denegados
        $denegados = CalendarioVacacion::where([['year', '>=', $any_anterior], ['year', '<=', $any_seguent],['status', '=', 'denied']])->get();
        foreach ($denegados as $key => $item) {
            $user_aux = User::find($item->user_id);
            if ( empty($user_aux) ) continue;
            $item->user_name = $user_aux->name . " " . $user_aux->lastname;
            $item->user_level_id = 'nivel-1';
            if ( $user_aux->calendario_subnivel_id > 0 ) $item->user_level_id = 'subnivel-' . $user_aux->calendario_subnivel_id;
            else if ( $user_aux->calendario_nivel_id > 0 ) $item->user_level_id = 'nivel-' . $user_aux->calendario_nivel_id;
            $denegados[$key] = $item;
        }

        // Niveles -> Subniveles -> Empleados
        $niveles_max_limit = array();
        $niveles = CalendarioNivel::where('status', '=', 'publish')->orderby('name')->get();
        $niveles_permitidos = array();
        foreach ($niveles as $key => $nivel) {
            // CalendarioNivelEncarregat
            if ( !in_array($nivel->id, $nivells_encarregats) && !Auth::user()->isAdmin() ) continue;

            $subniveles = CalendarioSubnivel::where('status', '=', 'publish')->where('calendario_nivel_id', '=', $nivel->id)->orderby('name')->get();
            foreach ($subniveles as $key2 => $subnivel) {
                $usuarios = User::where('calendario_subnivel_id', '=', $subnivel->id)->where('calendario_nivel_id', '=', 0)->get();
                $subnivel->usuarios = $usuarios;
                $subniveles[$key2] = $subnivel;
                $niveles_max_limit['subnivel-' . $subnivel->id] = $subnivel->get_persones_max();
            }
            $nivel->subniveles = $subniveles;

            $usuarios = User::where('calendario_nivel_id', '=', $nivel->id)->where('calendario_subnivel_id', '=', 0)->get();
            foreach ( $usuarios as &$usuario ) {
                $usuario->dies_pendents = $this->get_dies_pendents($usuario->id);
                $usuario->dies_vetats = $this->get_dies_vetats($usuario->id);
                $usuario->dies_festius = $this->get_dies_festius($usuario->id);
            }
            $nivel->usuarios = $usuarios;

            $niveles_max_limit['nivel-' . $nivel->id] = $nivel->get_persones_max();

            $niveles_permitidos[$key] = $nivel;
            //$niveles[$key] = $nivel;
        }
        
        return View('calendario-vacaciones.admin.index', array(
            'pendientes' => $pendientes, 
            'aprovados' => $aprovados, 
            'denegados' => $denegados,
            'niveles' => $niveles_permitidos,
            'niveles_max_limit' => $niveles_max_limit,
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $dies = json_decode($data['dies']);
        $usuarios_a_enviar = array();

        foreach ( $dies as $dia ) {
            if ( $peticio = CalendarioVacacion::find( $dia->id ) ) {
                $is_peticio_modificada = $peticio->status != $dia->estado || $peticio->start != $dia->start || $peticio->end != $dia->end;

                if ( $dia->estado == 'deleted' ) $peticio->delete();
                else {
                    $peticio->start = $dia->start;
                    $peticio->end = $dia->end;
                    $peticio->user_aproved_id = \Auth::user()->id;
                    $peticio->status = $dia->estado;
                    $peticio->update();
                }

                if ( $is_peticio_modificada ) $usuarios_a_enviar[$dia->usuario_id][] = $peticio;
            } else {
                if ( CalendarioVacacion::where([['user_id', (int)$dia->usuario_id], ['start', $dia->start]])->count() <= 0 ) {
                    $peticio = new CalendarioVacacion();
                    $peticio->start = $dia->start;
                    $peticio->end = $dia->end;
                    $peticio->user_aproved_id = \Auth::user()->id;
                    $peticio->status = $dia->estado;
                    $peticio->user_id = (int)$dia->usuario_id;
                    $peticio->year = (int)$dia->any;

                    if ( $peticio->save() ) $usuarios_a_enviar[$dia->usuario_id][] = $peticio;
                }
            }
        }

        foreach ( $usuarios_a_enviar as $usuari_id => $peticiones ) {
            $usuario = User::find($usuari_id);
            if ( !empty($usuario) ) {
                usort($peticiones, function($a, $b) {
                    return strtotime($a->start) - strtotime($b->start);
                });
                $notificacion = new PeticionVacacionNotification($usuario, $peticiones, false);
                try {
                    $usuario->notify( $notificacion );
                } catch(\Exception $e) {
                    return redirect()->route('admin.vacaciones.index')->with(['message' => __('Email Error: ') . $e->getMessage()]);
                }
            }
        }
        
        return redirect()->route('admin.vacaciones.index')->with(['message' => __('Vacations assigned successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exportar(Request $request) {
        $data = $request->all();

        $year = $data['year'];
        $usuari = $data['usuari'];
        $niveles = array();
        $subniveles = array();
        foreach ( explode(',', $data['nivells']) as $nivell ) {
            if ( like_match('nivel-%', $nivell) ) $niveles[] = str_replace('nivel-', '', $nivell);
            else if ( like_match('subnivel-%', $nivell) )  $subniveles[] = str_replace('subnivel-', '', $nivell);
        }

        $params = [ 'niveles' => $niveles, 'subniveles' => $subniveles ];

        // Filtre any i status
        $users_q = User::whereHas( 'vacaciones', function ($q) use($year) {
            $q->where([
                ['year', '=', $year],
                //['status', '=', 'approved'],
                ['status', '!=', 'denied'],
            ]);
        });
        
        // Filtre nivells i subnivells
        if ( !empty($niveles) || !empty($subniveles) ) {
            $users_q->where(function ($q) use($params) {
                if ( !empty($params['niveles']) ) {
                    $q->whereHas( 'nivel', function ($q2) use($params) {
                        $q2->whereIn('id', $params['niveles']);
                    });
                    if ( !empty($params['subniveles']) ) {
                        $q->orWhereHas( 'subnivel', function ($q2) use($params) {
                            $q2->whereIn('id', $params['subniveles']);
                        });
                    }
                } else if ( !empty($params['subniveles']) ) {
                    $q->whereHas( 'subnivel', function ($q2) use($params) {
                        $q2->whereIn('id', $params['subniveles']);
                    });
                }
            });

        }
        // Filtre usuari
        if ( !empty($usuari) ) $users_q->where('id', $usuari);

        // Evitar que surtin usuaris de 6tems
        $users_q->where('email', 'NOT LIKE', '%@6tems.com');

        $users = $users_q->orderby('lastname')->get();

        /* Loop per posar tots els user_ids en una array */
        $users_ids = array();
        $usuarios = array();
        foreach ( $users as $u ) {
            $users_ids[] = $u->id;
        }
        /* end Loop per posar tots els user_ids en una array */

        /* All days in year */
        $days = array();
        $data_inici = strtotime("01-01-$year");
        $data_fi = strtotime("31-12-$year");
        // Use for loop to store dates into array ( 86400 sec = 24 hrs = 60*60*24 = 1 day )
        for ( $currentDate = $data_inici; $currentDate <= $data_fi; $currentDate += (86400) ) {
            $data_format1 = date('d/m/Y', $currentDate);
            $data_format2 = date('Y-m-d', $currentDate);
            
            $vacaciones_dia = CalendarioVacacion::where([
                ['year', '=', $year],
                //['status', '=', 'approved']
                ['status', '!=', 'denied'],
            ])->whereIn('user_id', $users_ids)
            ->whereRaw("'$data_format2' BETWEEN start AND end")
            ->get();

            if ( $vacaciones_dia->count() > 0 ) {
                $days[ $data_format1 ] = [
                    'users' => array(),
                ];
                foreach ( $vacaciones_dia as $v ) $days[ $data_format1 ]['users'][$v->user_id] = $v->status;
            }

        } 
        /* end All days in year */

        $params = [
            'users' => $users,
            'days' => $days,
            'year' => $year,
        ];

        \Excel::create(__('Vacancy calendar'), function($excel) use($params) {

            $excel->sheet($params['year'], function($sheet) use($params) {

                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => '000000'
                            ]
                        ]
                    ]
                ]);

                $days = array();
                $days[] = '';
                foreach ( $params['days'] as $k => $v ) $days[] = $k;
                    
                $sheet->row(1, $days);
                $sheet->getStyle('A1:' . $sheet->getHighestColumn() . '1')->getAlignment()->setTextRotation(90);
                
                /* AMPLADA DATES */
                $letra = self::getNameFromNumber(1);
                $sheet->setSize($letra . '1', 30, 70);

                $i = 2;
                foreach ( $params['days'] as $k2 => $v ) {
                    $letra = self::getNameFromNumber($i);
                    $sheet->setSize($letra . '1', 3, 70);
                    $i++;
                }
                //$sheet->getRowDimension(1)->setRowHeight(80);
                /* /AMPLADA DATES */
                
                $sheet->cells('A1:' . $sheet->getHighestColumn() . '1', function ($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                });
                
                foreach ( $params['users'] as $k => $user ) {
                    $row_num = $k + 2;
                    $sheet->row( $row_num, [ $user->fullname(true) ] );

                    $letra = self::getNameFromNumber(1);
                    $sheet->cell($letra . $row_num, function($cell) {
                        $cell->setBorder('thin', 'thin', 'thin', 'thin');
                    });
                    
                    $i = 2;
                    foreach ( $params['days'] as $k2 => $v ) {
                        $letra = self::getNameFromNumber($i);
                        
                        if ( array_key_exists($user->id, $v['users']) ) {
                            $status = $v['users'][$user->id];
                            $sheet->cells($letra . $row_num, function($cells) use ($status) {
                                if ( $status == 'approved' ) $cells->setBackground('#00c63b');
                                else if ( $status == 'pending' ) $cells->setBackground('#2196f3');
                                else if ( $status == 'denied' ) $cells->setBackground('#ffeb3b');
                            });
                        }
                        
                        $sheet->cell($letra . $row_num, function($cell) {
                            $cell->setBorder('thin', 'thin', 'thin', 'thin');
                        });

                        $i++;
                    }
                }
            
            });

        })->export('xlsx');
    }

    // A partir d'un número, et retorna el index equivalent a la columna en Excel
    // A = 1
    public static function getNameFromNumber($num) {
        $numeric = ($num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($num - 1) / 26);
        if ($num2 > 0) {
            return self::getNameFromNumber($num2) . $letter;
        } else {
            return $letter;
        }
    }

    public function get_dies_pendents($user_id = null) {
        if ( empty($user_id) ) $user_id = Auth::user()->id;
        $user = User::findOrFail( $user_id );

        $any = date('Y') - 1;
        $dies_pendents[$any] = 0;
        $dies_pendents[$any + 1] = 22;
        $dies_pendents[$any + 2] = 27;
        $pinks = $blues = $greens_0 = $greens_1 = array();
        $oranges = $greens_2 = array();

        /**
         * Primera assignació
         * Aqui les variables per defecte agafen els valors determinats a la pàgina d'opcions
         * var $dies_pendents
         * var $sel_dia_o_semana
         */
        if ( !empty(Opcion::where('key', 'dies_pendents_any_' . $any)->first()) )
            $dies_pendents[$any] = Opcion::where('key', 'dies_pendents_any_' . $any)->first()->value;

        if ( !empty(Opcion::where('key', 'dies_pendents_any_' . ($any + 1))->first()) )
            $dies_pendents[$any + 1] = Opcion::where('key', 'dies_pendents_any_' . ($any + 1))->first()->value;

        if ( !empty(Opcion::where('key', 'dies_pendents_any_' . ($any + 2))->first()) )
            $dies_pendents[$any + 2] = Opcion::where('key', 'dies_pendents_any_' . ($any + 2))->first()->value;
        /** Fi Primera Assignació */

        /**
         * Segona assignació
         * Aqui les variables es sobreescriuen per els valors assignats als subnivells
         * o bé pel seu nivell
         */
        if ( $user->subnivel()->exists() ) {
            $subnivel = $user->subnivel()->first();
            $sel_dia_o_semana = $subnivel->sel_dia_o_semana;
            if ( $subnivel->years()->exists() ) {
                $aux = $subnivel->years()->where('year', $any)->first();
                if ( !empty($aux) && !empty($aux->days) ) $dies_pendents[$any] = $aux->days;
                $aux = $subnivel->years()->where('year', $any + 1)->first();
                if ( !empty($aux) && !empty($aux->days) ) $dies_pendents[$any + 1] = $aux->days;
                $aux = $subnivel->years()->where('year', $any + 2)->first();
                if ( !empty($aux) && !empty($aux->days) ) $dies_pendents[$any + 2] = $aux->days;
            }
        } else if ( $user->nivel()->exists() ) {
            $nivel = $user->nivel()->first();
            if ( $nivel->years()->exists() ) {
                $aux = $nivel->years()->where('year', $any)->first();
                if ( !empty($aux) && !empty($aux->days) ) $dies_pendents[$any] = $aux->days;
                $aux = $nivel->years()->where('year', $any + 1)->first();
                if ( !empty($aux) && !empty($aux->days) ) $dies_pendents[$any + 1] = $aux->days;
                $aux = $nivel->years()->where('year', $any + 2)->first();
                if ( !empty($aux) && !empty($aux->days) ) $dies_pendents[$any + 2] = $aux->days;
            }
        }

        /**
         * Tercera assignació
         * Aqui les variables agafen el valor seleccionat a la personalització anual del nivell
         */
        if ( $user->years()->exists() ) {
            $aux = $user->years()->where('year', $any)->first();
            if ( !empty($aux) && !empty($aux->days) && $aux->days > 0 ) $dies_pendents[$any] = $aux->days;
            $aux = $user->years()->where('year', $any + 1)->first();
            if ( !empty($aux) && !empty($aux->days) && $aux->days > 0 ) $dies_pendents[$any + 1] = $aux->days;
            $aux = $user->years()->where('year', $any + 2)->first();
            if ( !empty($aux) && !empty($aux->days) && $aux->days > 0 ) $dies_pendents[$any + 2] = $aux->days;
        }
        /* end Dies pendents */

        // Peticions pendents aquest any
        $dies_pink = CalendarioVacacion::where([['year', '=', $any], ['user_id', '=', $user_id], ['status', '=', 'pending']])->get();
        $dies_green_0 = CalendarioVacacion::where([['year', '=', $any], ['user_id', '=', $user_id], ['status', '=', 'approved']])->get();
        // Peticions pendents any següent
        $dies_blue = CalendarioVacacion::where([['year', '=', $any + 1], ['user_id', '=', $user_id], ['status', '=', 'pending']])->get();
        $dies_green_1 = CalendarioVacacion::where([['year', '=', $any + 1], ['user_id', '=', $user_id], ['status', '=', 'approved']])->get();
        // Peticions pendents 2 any següent
        $dies_orange = CalendarioVacacion::where([['year', '=', $any + 2], ['user_id', '=', $user_id], ['status', '=', 'pending']])->get();
        $dies_green_2 = CalendarioVacacion::where([['year', '=', $any + 2], ['user_id', '=', $user_id], ['status', '=', 'approved']])->get();

        /* Preparar arrays */
        foreach ( $dies_pink as $dia_pink ) $pinks[] = array('start' => $dia_pink->start, 'end' => $dia_pink->end);
        foreach ( $dies_blue as $dia_blue ) $blues[] = array('start' => $dia_blue->start, 'end' => $dia_blue->end);
        foreach ( $dies_orange as $dia_orange ) $blues[] = array('start' => $dia_orange->start, 'end' => $dia_orange->end);
        foreach ( $dies_green_0 as $dia_green ) $greens_0[] = array('start' => $dia_green->start, 'end' => $dia_green->end);
        foreach ( $dies_green_1 as $dia_green ) $greens_1[] = array('start' => $dia_green->start, 'end' => $dia_green->end);
        foreach ( $dies_green_2 as $dia_green ) $greens_1[] = array('start' => $dia_green->start, 'end' => $dia_green->end);

        $dies_pendents_restats = $dies_pendents;
        foreach ( $pinks as $pink ) {
            $dStart = new \DateTime($pink['start']);
            $dEnd  = new \DateTime($pink['end']);
            $dDiff = $dStart->diff($dEnd);
            $dies_pendents_restats[$any] -= ($dDiff->days > 0 ? $dDiff->days : 1);
        }
        foreach ( $greens_0 as $green ) {
            $dStart = new \DateTime($green['start']);
            $dEnd  = new \DateTime($green['end']);
            $dDiff = $dStart->diff($dEnd);
            $dies_pendents_restats[$any] -= ($dDiff->days > 0 ? $dDiff->days : 1);
        }
        foreach ( $blues as $blue ) {
            $dStart = new \DateTime($blue['start']);
            $dEnd  = new \DateTime($blue['end']);
            $dDiff = $dStart->diff($dEnd);
            $dies_pendents_restats[$any + 1] -= ($dDiff->days > 0 ? $dDiff->days : 1);
        }
        foreach ( $greens_1 as $green ) {
            $dStart = new \DateTime($green['start']);
            $dEnd  = new \DateTime($green['end']);
            $dDiff = $dStart->diff($dEnd);
            $dies_pendents_restats[$any + 1] -= ($dDiff->days > 0 ? $dDiff->days : 1);
        }
        foreach ( $oranges as $orange ) {
            $dStart = new \DateTime($orange['start']);
            $dEnd  = new \DateTime($orange['end']);
            $dDiff = $dStart->diff($dEnd);
            $dies_pendents_restats[$any + 2] -= ($dDiff->days > 0 ? $dDiff->days : 1);
        }
        foreach ( $greens_2 as $green ) {
            $dStart = new \DateTime($green['start']);
            $dEnd  = new \DateTime($green['end']);
            $dDiff = $dStart->diff($dEnd);
            $dies_pendents_restats[$any + 2] -= ($dDiff->days > 0 ? $dDiff->days : 1);
        }

        return $dies_pendents_restats;
    }

    public function get_dies_vetats($user_id = null) {
        if ( empty($user_id) ) $user_id = Auth::user()->id;
        $user = User::findOrFail( $user_id );

        $any = date('Y') - 1;
        $dies_vetats = array();
        
        $vetats_args = array(['year', '>=', $any], ['year', '<=', $any + 2]);
        if ( !empty($user->subnivel->id) ) {
            $vetats_args[] = ['calendario_subnivel_id', '=', $user->subnivel->id];
        } else if ( !empty($user->nivel->id) ) {
            $vetats_args[] = ['calendario_nivel_id', '=', $user->nivel->id];
        } else {
            $vetats_args[] = ['calendario_nivel_id', '=', 0];
        }
        $vetats = CalendarioVetado::where($vetats_args)->get()->sortByDesc("year");
        
        foreach ( $vetats as $vetat ) {
            foreach ( explode(',', $vetat->days) as $vetat_day ) $dies_vetats[] = $vetat_day;
        }

        return $dies_vetats;
    }

    public function get_dies_festius($user_id = null) {
        if ( empty($user_id) ) $user_id = Auth::user()->id;
        $user = User::findOrFail( $user_id );

        $any = date('Y') - 1;
        $dies_festius = array();

        // Festius
        $dies_red_where = array(['year', '>=', $any], ['year', '<=', $any + 2]);
        if ( !empty($user->subnivel) ) $dies_red_where[] = array('calendario_subnivel_id', '=', $user->subnivel->id);
        else if ( !empty($user->nivel) ) $dies_red_where[] = array('calendario_nivel_id', '=', $user->nivel->id);
        else $dies_red_where[] = array('calendario_nivel_id', '=', 0);
        $dies_red = CalendarioFestivo::where( $dies_red_where )->get();

        foreach ( $dies_red as $dia_red ) {
            foreach ( explode(',', $dia_red->days) as $dia_red_day ) {
                $dies_festius[] = $dia_red_day;
            }
        }

        return $dies_festius;
    }

}
