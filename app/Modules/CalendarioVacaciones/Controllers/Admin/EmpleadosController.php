<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use Illuminate\Support\Facades\Input;
use App\Modules\CalendarioVacaciones\Models\Empleado;
use App\Modules\CalendarioVacaciones\Models\EmpleadoCategoria;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivel;
use App\Modules\CalendarioVacaciones\Models\User;
use App\Models\Opcion;
use Validator;
use Auth;

class EmpleadosController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Employees');
        $this->section_icon = 'person_pin';
        $this->section_route = 'admin.calendario.empleados';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Empleado::get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'toolbar_header' => [
                '<a href="javascript:void(0);" onclick="$(\'#importar_empleados_file\').trigger(\'click\');" class="btn btn-default btn-sm">
                    ' . __('import') . '
                </a>
                <form id="importar_empleados_form" action="' . route('admin.calendario.empleados.import') . '" method="post" enctype="multipart/form-data">
                    ' . csrf_field() . '
                    <input type="file" id="importar_empleados_file" name="importar_empleados_file" style="display:none" accept=".csv"/>
                </form>
                <script>
                document.getElementById("importar_empleados_file").onchange = function() {
                    document.getElementById("importar_empleados_form").submit();
                };
                </script>',
            ],
            'headers' => [
                __('Code') => [ 'width' => '10%' ],
                __('Name') => [],
                __('ID card') => [],
                __('Category') => [],
                __('Level') => [],
                __('Created at') => [ 'width' => '15%' ],
            ],
            'rows' => [
                ['value' => 'codigo'],
                ['value' => 'nombre'],
                ['value' => 'dni'],
                ['value' => ['categoria', 'nombre']],
                ['value' => ['nivel', 'name']],
                ['value' => 'created_at', 'type' => 'date'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = EmpleadoCategoria::get()->sortby('nombre');
        $niveles = CalendarioNivel::get()->sortby('name');
        $users = User::get()->sortby('id');
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'codigo', 'title' => __('Code'), 'required' => true],
                ['value' => 'nombre', 'title' => __('Name'), 'required' => true],
                ['value' => 'dni', 'title' => __('ID card'), 'required' => true],
                ['value' => 'fecha_nacimiento', 'title' => __('Birthday'), 'required' => true, 'type' => 'date'],
                ['value' => 'fecha_alta', 'title' => __('Entry date'), 'required' => true, 'type' => 'date'],
                ['value' => 'fecha_baja', 'title' => __('Leave date'), 'required' => false, 'type' => 'date'],
                ['value' => 'categoria_id', 'title' => __('Category'), 'type' => 'select', 'required' => false,
                    'datasource' => $categorias, 'sel_value' => 'id', 'sel_title' => ['nombre'], 'sel_search' => true
                ],
                ['value' => 'calendario_nivel_id', 'title' => __('Level'), 'type' => 'select', 'required' => true,
                    'datasource' => $niveles, 'sel_value' => 'id', 'sel_title' => ['name'], 'sel_search' => true
                ],
                ['value' => 'user_id', 'title' => __('User'), 'type' => 'select', 'required' => false, 'class' => 'col-6',
                    'datasource' => $users, 'sel_value' => 'id', 'sel_title' => ['fullname()', 'email'], 'sel_search' => true
                ],
                ['value' => 'sexo', 'title' => __('Gender'), 'type' => 'select', 
                    'choices' => [
                        ['value' => 'Hombre', 'title' => __('Male')],
                        ['value' => 'Mujer', 'title' => __('Female')],
                    ]
                ],
            ],
            
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();
        if ( !empty($data['fecha_nacimiento']) ) {
            $data['fecha_nacimiento'] = \Carbon\Carbon::createFromFormat('d/m/Y', $data['fecha_nacimiento'] )->format('Y-m-d');
        }
        if ( !empty($data['fecha_alta']) ) {
            $data['fecha_alta'] = \Carbon\Carbon::createFromFormat('d/m/Y', $data['fecha_alta'] )->format('Y-m-d');
        }
        if ( !empty($data['fecha_baja']) ) {
            $data['fecha_baja'] = \Carbon\Carbon::createFromFormat('d/m/Y', $data['fecha_baja'] )->format('Y-m-d');
        }
        Empleado::create($data);
        return redirect()->route('admin.calendario.empleados.index')->with(['message' => __('Employee added successfully.')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        $item = Empleado::find($id);
        $categoria = EmpleadoCategoria::where('id', $item->categoria_id)->first();
        if ( empty($categoria) ) {
            $categoria = new EmpleadoCategoria();
            $categoria->nombre = 'Sin categoría';
        }

        $item->categoria = $categoria;
        if ( !empty($item->fecha_nacimiento) ) $item->fecha_nacimiento = new \Carbon\Carbon($item->fecha_nacimiento); 
        if ( !empty($item->fecha_alta) ) $item->fecha_alta = new \Carbon\Carbon($item->fecha_alta); 
        if ( !empty($item->fecha_baja) ) $item->fecha_baja = new \Carbon\Carbon($item->fecha_baja); 
        return View('calendario-vacaciones.admin.calendario.empleados.show', compact('item', $item));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = Empleado::find($id);
        $categorias = EmpleadoCategoria::get()->sortby('nombre');
        $niveles = CalendarioNivel::get()->sortby('name');
        $users = User::get()->sortby('id');
        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'codigo', 'title' => __('Code'), 'required' => true],
                ['value' => 'nombre', 'title' => __('Name'), 'required' => true],
                ['value' => 'dni', 'title' => __('ID card'), 'required' => true],
                ['value' => 'fecha_nacimiento', 'title' => __('Birthday'), 'required' => true, 'type' => 'date'],
                ['value' => 'fecha_alta', 'title' => __('Entry date'), 'required' => true, 'type' => 'date', 'date_format' => 'd/m/Y'],
                ['value' => 'fecha_baja', 'title' => __('Leave date'), 'required' => false, 'type' => 'date'],
                ['value' => 'categoria_id', 'title' => __('Category'), 'type' => 'select', 'required' => false,
                    'datasource' => $categorias, 'sel_value' => 'id', 'sel_title' => ['nombre'], 'sel_search' => true
                ],
                ['value' => 'calendario_nivel_id', 'title' => __('Level'), 'type' => 'select', 'required' => true,
                    'datasource' => $niveles, 'sel_value' => 'id', 'sel_title' => ['name'], 'sel_search' => true
                ],
                ['value' => 'user_id', 'title' => __('User'), 'type' => 'select', 'required' => false, 'class' => 'col-6',
                    'datasource' => $users, 'sel_value' => 'id', 'sel_title' => ['fullname()', 'email'], 'sel_search' => true
                ],
                ['value' => 'sexo', 'title' => __('Gender'), 'type' => 'select', 
                    'choices' => [
                        ['value' => 'Hombre', 'title' => __('Male')],
                        ['value' => 'Mujer', 'title' => __('Female')],
                    ]
                ],
            ],
            
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $item = Empleado::findOrFail($id);
        $data = $request->all();
        
        if ( !empty($data['fecha_nacimiento']) ) {
            $data['fecha_nacimiento'] = \Carbon\Carbon::createFromFormat('d/m/Y', $data['fecha_nacimiento'] )->format('Y-m-d');
        }
        if ( !empty($data['fecha_alta']) ) {
            $data['fecha_alta'] = \Carbon\Carbon::createFromFormat('d/m/Y', $data['fecha_alta'] )->format('Y-m-d');
        }
        if ( !empty($data['fecha_baja']) ) {
            $data['fecha_baja'] = \Carbon\Carbon::createFromFormat('d/m/Y', $data['fecha_baja'] )->format('Y-m-d');
        }
        $item->update($data);
        return redirect()->route('admin.calendario.empleados.index')->with(['message' => __('Employee updated successfully.')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $rows = Empleado::destroy($id);
		if ( $rows > 0 ) return redirect()->back()->with('message', 'Empleado eliminado correctamente.');
		else return redirect()->back()->with('error', 'Ha ocurrido un error al eliminar el empleado.');
    }

    public function massDestroy(Request $request)
    {
        $empleados = explode(',', $request->input('ids'));
        foreach ($empleados as $empleado_id) {
            $empleado = Empleado::findOrFail($empleado_id);
            $empleado->delete();
        }
        return redirect()->route('admin.calendario.empleados.index')->with(['message' => __('Employee deleted succesfully.')]);
    }

    public function import(Request $request) {

        if ( !empty(Opcion::where('key', 'calendario_sin_nivel_id')->first()) )
            $calendario_sin_nivel_id = Opcion::where('key', 'calendario_sin_nivel_id')->first()->value;
        else $calendario_sin_nivel_id = 0;

        $arxiu = request()->importar_empleados_file;
        if ( empty($arxiu) || (!empty($arxiu) && $arxiu->getClientOriginalExtension() != 'csv') ) {
            return back()->with('error', 'Empty or incorrect file.');
        }
        if ( empty($arxiu->path()) ) {
            return back()->with('error', 'Empty or incorrect file.');
        }

        $csv = $this->csv_to_array($arxiu->path(), ';', false);
        $csv = $this->utf8_converter($csv);
        
        $error = false;
        foreach ($csv as $key => $row) {
            $empleado_codigo = $row[0];
            $empleado = Empleado::where('codigo', $empleado_codigo)->first();
            if ( empty($empleado) ) $empleado = new Empleado();
            
            $empleado->codigo = $empleado_codigo;

            if ( !empty($row[1]) ) $empleado->nombre = $row[1];
            if ( !empty($row[2]) ) {
                $fecha_alta = \DateTime::createFromFormat('d-m-Y', $row[2]);
                $empleado->fecha_alta = $fecha_alta->format('Y-m-d H:i:s');
            }
            if ( !empty($row[3]) ) $empleado->dni = $row[3];
            if ( !empty($row[4]) ) $empleado->sexo = $row[4];
            if ( !empty($row[5]) ) {
                $fecha_nacimiento = \DateTime::createFromFormat('d-m-Y', $row[5]);
                $empleado->fecha_nacimiento = $fecha_nacimiento->format('Y-m-d H:i:s');
            }
            if ( !empty($row[6]) ) {
                $fecha_baja = \DateTime::createFromFormat('d-m-Y', $row[6]);
                $empleado->fecha_baja = $fecha_baja->format('Y-m-d H:i:s');
            }
            if ( !empty($row[7]) ) {
                $empleado->categoria_id = $row[7];

                $categoria = EmpleadoCategoria::where('nombre', $empleado->categoria_id)->first();
                if ( empty($categoria) ) {
                    $categoria = new EmpleadoCategoria();
                    $categoria->nombre = $empleado->categoria_id;
                    if ( $categoria->save() ) {
                        $empleado->categoria_id = $categoria->id;
                    }
                } else $empleado->categoria_id = $categoria->id;
            }
            if ( !empty($row[8]) ) {
                $empleado->calendario_nivel_id = $row[8];
                $nivel = CalendarioNivel::where('name', $empleado->calendario_nivel_id)->first();
                if ( empty($nivel) ) {
                    $nivel = new CalendarioNivel();
                    $nivel->name = $empleado->calendario_nivel_id;
                    if ( $nivel->save() ) {
                        $empleado->calendario_nivel_id = $nivel->id;
                    }
                } else $empleado->calendario_nivel_id = $nivel->id;
            }

            if ( empty($empleado->calendario_nivel_id) || $empleado->calendario_nivel_id == 0 ) {
                $empleado->calendario_nivel_id = 1;
            }

            if ( !$empleado->save() ) {
                if (!$error) $error = true;
            }
        }
        
        if ( !$error ) {
            return redirect()->back()->with('message', 'Enhorabuena importación realizada correctamente.');
        } else {
            return redirect()->back()->with('error', 'Ha ocurrido un error al guardar los datos.');
        }
    }

    public function utf8_converter($array) {
		array_walk_recursive($array, function(&$item, $key) {
			if ( !mb_detect_encoding($item, 'utf-8', true) ) {
				$item = utf8_encode($item);
			}
		});
		return $array;
    }
    
    public function csv_to_array($filename='', $delimiter=',', $with_header = true) {
        if ( !file_exists($filename) || !is_readable($filename) ) return false;
    
        $header = NULL;
        $data = array();
        if ( ($handle = fopen($filename, 'r')) !== false ) {
            while ( ($row = fgetcsv($handle, 1000, $delimiter)) !== false ) {
                if ( !$header )
                    $header = $row;
                else if ( $with_header )
                    $data[] = array_combine($header, $row);
                else
                    $data[] = $row;
            }
            fclose($handle);
        }
        return $data;
    }

}
