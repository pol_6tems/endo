<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\CalendarioVacaciones\Models\CalendarioMensaje;
use App\Modules\CalendarioVacaciones\Models\User;
use App\Modules\CalendarioVacaciones\Models\Empleado;
use App\Notifications\NuevoMensajeNotification;
use Auth;
use DB;

class CalendarioMensajesController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Calendar') . ' ' . __('Messages');
        $this->section_icon = 'message';
        $this->section_route = 'admin.calendario.mensajes';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereHas(
            'calendario_mensajes', function ($query) {
                $query->whereNotNull('mensaje');
            }
        )
        ->leftJoin('calendario_mensajes', 'users.id', '=', 'calendario_mensajes.user_id')
        ->groupBy(['users.id', 'users.name', 'users.email', 'users.avatar'])
        ->orderByRaw('max(calendario_mensajes.updated_at) desc')
        ->select(['users.id', 'users.name', 'users.email', 'users.avatar'])
        ->get();
        
        return view('calendario-vacaciones.admin.messaging', array(
            'users' => $users,
            //'items' => $items,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'user_class' => new User(),
            'user_messages' => 'calendario_mensajes',
        ));
    }

    public function contestar($user_id)
    {
        $user = User::find($user_id);
        $item = CalendarioMensaje::where('user_id', $user_id)->get()->sortByDesc('created_at');
        
        return View('calendario-vacaciones.admin.calendario.mensaje.contestar', compact('item', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        $mensaje = new CalendarioMensaje();
        $mensaje->user_id = $data['user_id'];
        $mensaje->from = Auth::user()->id;
        $mensaje->mensaje = $data['mensaje'];
        
        if ( $mensaje->save() ) {
            
            $usuario = User::find( $mensaje->user_id );
            $notificacion = new NuevoMensajeNotification($usuario,  $mensaje, Auth::user());
            $usuario->notify( $notificacion );

            echo 'OK';
        }
        else echo 'KO';

        die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function visto(Request $request)
    {
        $user_id = $request->user_id;
        DB::table('calendario_mensajes')->where('user_id', $user_id)->update(['visto' => true]);
        return response()->json(['success' => true]);
    }
}
