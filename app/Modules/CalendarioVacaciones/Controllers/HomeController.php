<?php

namespace App\Modules\CalendarioVacaciones\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController as HomeControllerBase;
use Illuminate\Support\Facades\Auth;
use Session;


class HomeController extends HomeControllerBase
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('Front::home');
    }

    public function politica_de_privacitat()
    {
        $user = Auth::user();
        $name = $dni = '';
        if ( !empty($_REQUEST['name']) && !empty($_REQUEST['dni']) ) {
            $name = $_REQUEST['name'];
            $dni = $_REQUEST['dni'];
        } else if ( !empty($user) ) {
            $name = $user->name . ' ' . $user->lastname;
            $dni = $user->dni;
        }
        return View('Front::acces.politica_de_privacitat_' . \App::getLocale(), compact('name', 'dni'));
    }
}
