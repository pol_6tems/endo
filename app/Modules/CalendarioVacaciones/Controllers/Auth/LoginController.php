<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Auth\LoginController as LoginControllerBase;
use App\Modules\CalendarioVacaciones\Models\User;

class LoginController extends LoginControllerBase
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Login username to be used by the controller.
     *
     * @var string
     */
    protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout');

        $this->dni = $this->findDni();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findDni()
    {
        $login = request()->input('dni');
        if ( empty($login) ) $login = request()->input('email');
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'dni';
 
        request()->merge([$fieldType => $login]);
 
        return $fieldType;
    }
 
    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->dni;
    }

    public function showLoginForm()
    {
        return parent::showLoginForm();
    }

    public function redirectTo()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);

        $no_te_access = true;
        if ( $user->role == 'user' ) {
            if ( !empty($user->empleado) && !empty($user->empleado->fecha_baja) && $user->empleado->fecha_baja != null ) {
                $fecha_baja = \Carbon\Carbon::parse($user->empleado->fecha_baja);
                $no_te_access = \Carbon\Carbon::now().lessThanOrEqualTo($fecha_baja);
            }
            else $no_te_access = false;
        }
        else $no_te_access = false;

        if ( $no_te_access ) \Auth::logout();
        
        return $this->redirectTo;

    }
}
