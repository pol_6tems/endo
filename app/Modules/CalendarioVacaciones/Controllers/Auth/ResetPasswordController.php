<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\ResetPasswordController as ResetPasswordControllerBase;

class ResetPasswordController extends ResetPasswordControllerBase
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('guest');
    }

    public function showResetForm(Request $request, $locale, $token = null)
    {
        return View('Front::auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
