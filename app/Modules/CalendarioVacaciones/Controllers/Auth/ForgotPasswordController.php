<?php

namespace App\Modules\CalendarioVacaciones\Controllers\Auth;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Controllers\Auth\ForgotPasswordController as ForgotPasswordControllerBase;
use View;

class ForgotPasswordController extends ForgotPasswordControllerBase
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('guest');
    }

    public function showLinkRequestForm() {
        if ( View::exists('Front::auth.passwords.email') ) return View('Front::auth.passwords.email');
        else return View('auth.passwords.email');
    }
}
