<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\CalendarioVacaciones\Models\User;

class CalendarioUserYear extends Model
{
    protected $table = 'calendario_users_years';
    protected $fillable = ['user_id', 'year', 'days'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
