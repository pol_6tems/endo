<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\CalendarioVacaciones\Models\User;

class CalendarioMensaje extends Model
{
    protected $table = 'calendario_mensajes';
    
    protected $fillable = ['user_id', 'from', 'mensaje', 'visto'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function from()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
