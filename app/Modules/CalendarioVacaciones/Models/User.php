<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivel;
use App\Modules\CalendarioVacaciones\Models\CalendarioSubnivel;
use App\Modules\CalendarioVacaciones\Models\CalendarioVacacion;
use App\Modules\CalendarioVacaciones\Models\Formacio;
use App\Models\Rol;
use App\Models\Mensaje;
use App\User as UserBase;

class User extends UserBase
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'calendario_nivel_id', 'calendario_subnivel_id',
        'lastname',
        'phone',
        'mobile',
        'address',
        'codigo_postal',
        'city',
        'idioma',
        'fecha_nacimiento',
        'lopd',
        'consentiment_naixement',
        'consentiment_privacitat',
        'role',
        'dni',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class, 'calendario_nivel_id');
    }
    
    public function subnivel()
    {
        return $this->belongsTo(CalendarioSubnivel::class, 'calendario_subnivel_id');
    }
    
    public function vacaciones()
    {
        return $this->hasMany(CalendarioVacacion::class, 'user_id');
    }
    
    public function empleado()
    {
        return $this->hasOne(Empleado::class, 'user_id');
    }
    
    public function years()
    {
        return $this->hasMany(CalendarioUserYear::class);
    }
    
    public function calendario_mensajes()
    {
        return $this->hasMany(CalendarioMensaje::class, 'user_id');
    }

    public function formacions()
    {
        return $this->belongsToMany(Formacio::class)->withTimestamps();
    }

    public function rol() {
        return $this->belongsTo(Rol::class, 'role', 'name');
    }

    public function contactar_mensajes() {
        return $this->hasMany(Mensaje::class, 'user_id');
        //return Mensaje::where('user_id', $this->id)->orWhere('from', $this->id);
    }
}
