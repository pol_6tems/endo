<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\CalendarioVacaciones\Models\CalendarioSubnivel;

class CalendarioSubnivelWeek extends Model
{
    protected $fillable = ['calendario_subnivel_id', 'year', 'week', 'max_limit'];

    public function nivel()
    {
        return $this->belongsTo(CalendarioSubnivel::class);
    }
}
