<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarioVetado extends Model
{
    protected $fillable = ['year', 'days', 'calendario_nivel_id', 'calendario_subnivel_id'];
}
