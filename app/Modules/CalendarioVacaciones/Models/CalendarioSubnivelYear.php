<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\CalendarioVacaciones\Models\CalendarioSubnivel;

class CalendarioSubnivelYear extends Model
{
    protected $fillable = ['calendario_subnivel_id', 'year', 'days'];

    public function nivel()
    {
        return $this->belongsTo(CalendarioSubnivel::class);
    }
}

