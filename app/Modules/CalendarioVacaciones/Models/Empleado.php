<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivel;
use App\Modules\CalendarioVacaciones\Models\EmpleadoCategoria;
use App\Modules\CalendarioVacaciones\Models\User;

class Empleado extends Model
{
    protected $table = 'empleados';
	
    protected $fillable = [
        'id', 'nombre', 'fecha_alta', 'dni', 'sexo', 'fecha_nacimiento', 'fecha_baja', 
        'categoria_id', 'user_id', 'codigo', 'calendario_nivel_id'
    ];

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class, 'calendario_nivel_id');
    }

    public function categoria()
    {
        return $this->belongsTo(EmpleadoCategoria::class, 'categoria_id');
    }
    
    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
}
