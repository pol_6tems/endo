<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\CalendarioVacaciones\Models\User;

class Suggeriment extends Model
{
    protected $table = 'suggeriments';
    
    protected $fillable = ['user_id', 'subject', 'message', 'visto'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
