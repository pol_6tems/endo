<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\CalendarioVacaciones\Models\User;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivel;

class CalendarioNivelEncarregat extends Model
{
    protected $fillable = ['calendario_nivel_id', 'user_id'];

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
