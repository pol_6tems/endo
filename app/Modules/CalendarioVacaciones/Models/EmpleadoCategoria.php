<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;

class EmpleadoCategoria extends Model
{
    protected $table = 'empleado_categorias';
	
    protected $fillable = [
        'id', 'nombre'
    ];
}