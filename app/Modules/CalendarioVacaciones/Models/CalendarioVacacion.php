<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\CalendarioVacaciones\Models\User;

class CalendarioVacacion extends Model
{
    use SoftDeletes;

    protected $table = 'calendario_vacaciones';
    
    protected $fillable = ['start', 'end', 'year', 'status', 'user_id', 'user_aproved_id'];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function user_aproved()
    {
        return $this->belongsTo(User::class, 'user_aproved_id');
    }
}
