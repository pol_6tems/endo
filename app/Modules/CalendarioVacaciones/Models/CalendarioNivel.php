<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivelYear;
use App\Modules\CalendarioVacaciones\Models\CalendarioSubnivel;
use App\Modules\CalendarioVacaciones\Models\User;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivelWeek;

class CalendarioNivel extends Model
{
    use SoftDeletes;

    protected $table = 'calendario_niveles';
    
    protected $fillable = ['name', 'status', 'sel_dia_o_semana', 'no_bloquejat', 'max_limit'];
    
    protected $dates = ['deleted_at'];

    public function years()
    {
        return $this->hasMany(CalendarioNivelYear::class);
    }

    public function subniveles()
    {
        return $this->hasMany(CalendarioSubnivel::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function weeks()
    {
        return $this->hasMany(CalendarioNivelWeek::class);
    }

    public function get_persones_max()
    {
        $persones_max = array('general' => $this->max_limit);
        if ( !empty($this->weeks) ) {
            foreach ($this->weeks as $item) {
                $persones_max[$item->year . '-' . $item->week] = $item->max_limit;
            }
        }
        return $persones_max;
    }
}
