<?php

namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivel;

class CalendarioNivelYear extends Model
{    
    protected $fillable = ['calendario_nivel_id', 'year', 'days'];

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class);
    }
}
