<?php namespace App\Modules\CalendarioVacaciones\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\CalendarioVacaciones\Models\CalendarioNivel;
use App\Modules\CalendarioVacaciones\Models\EmpleadoCategoria;
use App\Modules\CalendarioVacaciones\Models\User;

class Formacio extends Model
{
    protected $table = 'formacions';
	
    protected $fillable = ['id', 'image', 'title', 'content', 'url', 'visited'];

    function users()
	{
		return $this->belongsToMany(User::class)->withTimestamps();
	}
}
