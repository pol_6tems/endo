<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegistreToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('dni')->nullable()->unique();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('address')->nullable();
            $table->string('codigo_postal')->nullable();
            $table->string('city')->nullable();
            $table->string('idioma')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('lopd')->default('0');
            $table->string('consentiment_privacitat')->default('0');
            $table->string('consentiment_naixement')->default('0');
            //$table->string('avatar')->default('user.jpg');
            $table->integer('calendario_nivel_id')->unsigned()->default(0);
            $table->integer('calendario_subnivel_id')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('dni');
            $table->dropColumn('phone');
            $table->dropColumn('mobile');
            $table->dropColumn('address');
            $table->dropColumn('codigo_postal');
            $table->dropColumn('city');
            $table->dropColumn('idioma');
            $table->dropColumn('fecha_nacimiento');
            $table->dropColumn('lopd');
            $table->dropColumn('consentiment_privacitat');
            $table->dropColumn('consentiment_naixement');
            //$table->dropColumn('avatar');
            $table->dropColumn('calendario_nivel_id');
            $table->dropColumn('calendario_subnivel_id');
        });
    }
}
