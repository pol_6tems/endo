<?php

// Home
Route::get('/{index?}', 'HomeController@index')->name('index');

Route::get(__("/access"), "AccesController@index")->name("access");
Route::get(__("/acces"), "AccesController@check_dni")->name("acces.dni");
Route::get(__("/register"), "AccesController@registre")->name("register");
Route::get(__("/register"), "AccesController@registre")->name("acces.registre");
Route::get(__("/acces.benvingut"), "AccesController@benvingut")->name("acces.benvingut");
Route::post(__("/acces.crear_user"), "AccesController@crear_user")->name("acces.crear_user");

// Auth
Route::get(__("/login"), "Auth\LoginController@showLoginForm")->name("login");
Route::post(__("/login"), "Auth\LoginController@login")->name("login");

//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset', "Auth\ForgotPasswordController@showLinkRequestForm")->name('password.request');
Route::get('password/reset/{token}', "Auth\ResetPasswordController@showResetForm")->name('password.reset');
Route::post('password/reset', "Auth\ResetPasswordController@reset");