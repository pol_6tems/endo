<?php

Route::get("/", "HomeController@index")->name("index");
Route::get("/home", "HomeController@index")->name("home");

Route::get(__("/usuario.perfil"), "UsuariosController@perfil")->name("usuario.perfil");
Route::post(__("/usuario.perfil"), "UsuariosController@update")->name("usuario.update");
Route::post(__("/perfil_update_password"), "UsuariosController@update_password")->name("usuario.update_password");
Route::post(__("/perfil_update_avatar"), "UsuariosController@update_avatar")->name("usuario.update_avatar");

//Route::resource("calendari", "CalendariController");
Route::get(__("/calendari.index"), "CalendariController@index")->name("calendari.index");
Route::post(__("/calendari") . "/store", "CalendariController@store")->name("calendari.store");

Route::resource("calendario-mensajes", "CalendarioMensajesController");

//Route::resource("contactar", "MensajesController");
Route::get(__("/contactar.index"), "MensajesController@index")->name("contactar.index");
Route::post("/contact/store", "MensajesController@store")->name("contactar.store");

// Bústia de suggeriments - store
Route::post("/suggeriment/store", "SuggerimentsController@store")->name("suggeriment.store");
Route::post('/formacions/store', "FormacionsController@store")->name('formacions.store');

// UserMeta
//Route::resource('user_meta', "Admin\UserMetaController");