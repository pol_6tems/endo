<?php

// Vacaciones
Route::post('vacaciones/exportar', "Admin\CalendarioVacacionesController@exportar")->name('admin.vacaciones.exportar');
Route::resource('vacaciones', "Admin\CalendarioVacacionesController", ['as' => 'admin']);

// Niveles - Years
Route::get('niveles/years/create/{calendario_nivel_id}', "Admin\CalendarioNivelesController@years_create")->name('admin.calendario.niveles.years.create');
Route::post('niveles/years/store', "Admin\CalendarioNivelesController@years_store")->name('admin.calendario.niveles.years.store');
Route::get('niveles/years/edit/{id}', "Admin\CalendarioNivelesController@years_edit")->name('admin.calendario.niveles.years.edit');
Route::post('niveles/years/update/{id}', "Admin\CalendarioNivelesController@years_update")->name('admin.calendario.niveles.years.update');
Route::delete('niveles/years/destroy/{id}', "Admin\CalendarioNivelesController@years_destroy")->name('admin.calendario.niveles.years.destroy');

// Niveles - Weeks
Route::get('niveles/weeks/create/{calendario_nivel_id}', "Admin\CalendarioNivelesController@weeks_create")->name('admin.calendario.niveles.weeks.create');
Route::post('niveles/weeks/store', "Admin\CalendarioNivelesController@weeks_store")->name('admin.calendario.niveles.weeks.store');
Route::get('niveles/weeks/edit/{id}', "Admin\CalendarioNivelesController@weeks_edit")->name('admin.calendario.niveles.weeks.edit');
Route::post('niveles/weeks/update/{id}', "Admin\CalendarioNivelesController@weeks_update")->name('admin.calendario.niveles.weeks.update');
Route::delete('niveles/weeks/destroy/{id}', "Admin\CalendarioNivelesController@weeks_destroy")->name('admin.calendario.niveles.weeks.destroy');

// Niveles - Encarregat
Route::get('niveles/encarregat/create/{calendario_nivel_id}', "Admin\CalendarioNivelesController@encarregat_create")->name('admin.calendario.niveles.encarregat.create');
Route::post('niveles/encarregat/store', "Admin\CalendarioNivelesController@encarregat_store")->name('admin.calendario.niveles.encarregat.store');
Route::get('niveles/encarregat/edit/{id}', "Admin\CalendarioNivelesController@encarregat_edit")->name('admin.calendario.niveles.encarregat.edit');
Route::post('niveles/encarregat/update/{id}', "Admin\CalendarioNivelesController@encarregat_update")->name('admin.calendario.niveles.encarregat.update');
Route::delete('niveles/encarregat/destroy/{id}', "Admin\CalendarioNivelesController@encarregat_destroy")->name('admin.calendario.niveles.encarregat.destroy');

// Niveles
Route::delete('niveles/mass_destroy', "Admin\CalendarioNivelesController@massDestroy")->name('admin.calendario.niveles.mass_destroy');
Route::resource('niveles', "Admin\CalendarioNivelesController", ['as' => 'admin.calendario']);

// SubNiveles - Years
Route::get('subniveles/years/create/{calendario_nivel_id}', "Admin\CalendarioSubnivelesController@years_create")->name('admin.calendario.subniveles.years.create');
Route::post('subniveles/years/store', "Admin\CalendarioSubnivelesController@years_store")->name('admin.calendario.subniveles.years.store');
Route::get('subniveles/years/edit/{id}', "Admin\CalendarioSubnivelesController@years_edit")->name('admin.calendario.subniveles.years.edit');
Route::post('subniveles/years/update/{id}', "Admin\CalendarioSubnivelesController@years_update")->name('admin.calendario.subniveles.years.update');
Route::delete('subniveles/years/destroy/{id}', "Admin\CalendarioSubnivelesController@years_destroy")->name('admin.calendario.subniveles.years.destroy');

// SubNiveles - Weeks
Route::get('subniveles/weeks/create/{calendario_nivel_id}', "Admin\CalendarioSubnivelesController@weeks_create")->name('admin.calendario.subniveles.weeks.create');
Route::post('subniveles/weeks/store', "Admin\CalendarioSubnivelesController@weeks_store")->name('admin.calendario.subniveles.weeks.store');
Route::get('subniveles/weeks/edit/{id}', "Admin\CalendarioSubnivelesController@weeks_edit")->name('admin.calendario.subniveles.weeks.edit');
Route::post('subniveles/weeks/update/{id}', "Admin\CalendarioSubnivelesController@weeks_update")->name('admin.calendario.subniveles.weeks.update');
Route::delete('subniveles/weeks/destroy/{id}', "Admin\CalendarioSubnivelesController@weeks_destroy")->name('admin.calendario.subniveles.weeks.destroy');

// Subniveles
Route::delete('subniveles/mass_destroy', "Admin\CalendarioSubnivelesController@massDestroy")->name('admin.calendario.subniveles.mass_destroy');
Route::resource('subniveles', "Admin\CalendarioSubnivelesController", ['as' => 'admin.calendario']);

// Empleados
Route::delete('empleados/mass_destroy', "Admin\EmpleadosController@massDestroy")->name('admin.calendario.empleados.mass_destroy');
Route::post('empleados/import', "Admin\EmpleadosController@import")->name('admin.calendario.empleados.import');

//Route::resource('empleados_categorias', "Admin\CategoriasEmpleadoController", ['as' => 'admin.calendario']);
Route::resource('empleados', "Admin\EmpleadosController", ['as' => 'admin.calendario']);

// Festivos
Route::resource('festivos', "Admin\CalendarioFestivosController", ['as' => 'admin.calendario']);

//Vetados
Route::resource('vetados', "Admin\CalendarioVetadosController", ['as' => 'admin.calendario']);

// Mensajes
Route::post('mensajes/visto', "Admin\CalendarioMensajesController@visto")->name('admin.calendario.mensajes.visto');
Route::get('mensajes/contestar/{id}', "Admin\CalendarioMensajesController@contestar")->name('admin.calendario.mensajes.contestar');
Route::resource('mensajes', "Admin\CalendarioMensajesController", ['as' => 'admin.calendario']);