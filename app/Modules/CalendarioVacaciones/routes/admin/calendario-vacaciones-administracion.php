<?php

// UserYears
Route::post('users/years/update/{id}', "Admin\UsersController@years_update")->name('admin.users.years.update');
Route::post('users/years/store', "Admin\UsersController@years_store")->name('admin.users.years.store');
Route::get('users/years/edit/{id}', "Admin\UsersController@years_edit")->name('admin.users.years.edit');
Route::delete('users/years/destroy/{id}/{user_id}', "Admin\UsersController@years_destroy")->name('admin.users.years.destroy');
Route::get('users/years/create/{calendario_nivel_id}', "Admin\UsersController@years_create")->name('admin.users.years.create');
// Users
Route::resource('users', "Admin\UsersController", ['as' => 'admin']);