@extends('Admin::layouts.admin')

@section('section-title')
	@Lang('Vacancy calendar')
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">date_range</i>
                </div>
                <h4 class="card-title">
					@Lang('Vacancy calendar')
					<form class="col-6" id="form_enviar_dies" action="{{route('admin.vacaciones.store')}}" method="post" style="display: inline-block;">
						{{ csrf_field() }}
						<input id="dies" name="dies" type="hidden" />
						<input class="btn btn-primary btn-sm" type="submit" value="@Lang('Save')">
						<button type="button" onclick="window.location.href='{{route('admin.vacaciones.index')}}'" class="btn btn-default btn-sm">@Lang('Cancel')</button>
						<button style="display:none!important;" id="seleccionar-filter-dies-btn" type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal" data-action="add">@Lang('Filter by levels')</button>
						<button style="display:none!important;" id="seleccionar-filter-users-btn" type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#filtrarUsuari" data-action="add">@Lang('Filter by users')</button>
					</form>

					<button type="button" onclick="exportar();" class="btn btn-sm btn-default" style="float:right;">@Lang('Export')</button>
					<form id="exportar-vacaciones-form" action="{{ route('admin.vacaciones.exportar') }}" method="POST">
						{!! csrf_field() !!}
						<input type="hidden" name="usuari">
						<input type="hidden" name="nivells">
						<input type="hidden" name="year">
					</form>
                </h4>
            </div>
            <div class="card-body">
				<div id='calendar' style="max-width: none !important;"></div>
				<div id='store' style="max-width: none !important;"></div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myModalLabel">@Lang('Filter by levels')</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<select class="selectpicker" id="selecciona-nivells" name="nivells" data-live-search="true" data-style="select-with-transition" multiple title="@Lang('Select levels')" data-size="7">
						<option value="" selected>@Lang('All')</option>
						@foreach($niveles as $item)
							<option value="nivel-{{$item->id}}">{{$item->name}}</option>
							@foreach($item->subniveles as $subitem)
								<option value="subnivel-{{$subitem->id}}">{{$subitem->name}}</option>
							@endforeach
						@endforeach
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
				<button type="submit" class="btn btn-primary">@Lang('Filter')</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="canviarEstat" tabindex="-1" role="dialog" aria-labelledby="canviarEstatLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="canviarEstatLabel">@Lang('Sel·lecciona el nou estat')</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<input id="estat-vista" type="hidden" value="" />
		<input id="estat-id" type="hidden" value="" />
		<select class="selectpicker" id="selecciona-dia" data-style="select-with-transition" multiple title="@Lang('Dies')" data-size="7"></select>
		<select class="selectpicker" id="selecciona-estat" data-style="select-with-transition" title="@Lang('Estat')" data-size="7">
			<option value="pending" selected>@Lang('Pendent')</option>
			<option value="approved">@Lang('Aprovat')</option>
			<option value="denied">@Lang('Denegat')</option>
		</select>
      </div>
      <div class="modal-footer" style="justify-content: flex-start;">
			<button type="button" id="eliminarDies" class="btn btn-link btn-danger mr-auto" data-dismiss="modal">@Lang('Delete')</button>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
        	<button type="button" id="guardaEstats" class="btn btn-primary">@Lang('Save')</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="filtrarUsuari" tabindex="-1" role="dialog" aria-labelledby="filtrarUsuari" aria-hidden="true">
	<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">@Lang('Enter the name of the user')</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<input id="estat-vista" type="hidden" value="" />
		<input id="estat-id" type="hidden" value="" />
		<select class="selectpicker" id="selecciona-usuari" name="filtreUsuaris" data-live-search="true" data-style="select-with-transition" title="@Lang('Select a user')" data-size="7">
			<option value="" selected>@Lang('Cap')</option>
			@foreach($niveles as $item)
				@foreach($item->usuarios as $subitem)
						<option value="usuario-{{$subitem->id}}">{{$subitem->fullname(true)}}</option>
				@endforeach
			@endforeach
		</select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
        <button type="button" id="filtraUsuaris" class="btn btn-primary">@Lang('Filter')</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Afegir Event -->
<div class="modal fade" id="afegirEvent" tabindex="-1" role="dialog" aria-labelledby="afegirEventLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="afegirEventLabel">@Lang('Afegir dies')</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<input id="estat-vista" type="hidden" value="" />
		<input id="estat-id" type="hidden" value="" />
		<div class="dies" style="display: flex; flex-wrap: wrap;"></div>
		<select class="selectpicker" id="selecciona-estat" data-style="select-with-transition" title="@Lang('Estat')" data-size="7">
			<option value="pending" selected>@Lang('Pending')</option>
			<option value="approved">@Lang('Aprovat')</option>
			<option value="denied">@Lang('Denegat')</option>
		</select>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
		<button type="button" id="guardaEvent" class="btn btn-primary">@Lang('Save')</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('styles')
{{--<link rel='stylesheet' href="{{asset('Modules/CalendarioVacaciones/js/fullcalendar-3.9.0/fullcalendar.css')}}" />--}}
<link rel='stylesheet' href="{{asset('Modules/CalendarioVacaciones/js/fullcalendar-3.9.0/fullcalendar.print.css')}}" media='print' />
<link rel='stylesheet' href="{{asset('Modules/CalendarioVacaciones/js/fullcalendar-3.9.0/scheduler.min.css')}}" />
<link rel='stylesheet' href="{{asset('Modules/CalendarioVacaciones/css/override.css')}}" />
@endsection

@section('scripts')
<script src="{{asset('Modules/CalendarioVacaciones/js/fullcalendar-3.9.0/lib/moment.min.js')}}"></script>
<script src="{{asset('Modules/CalendarioVacaciones/js/fullcalendar-3.9.0/fullcalendar.min.js')}}"></script>
<script src="{{asset('Modules/CalendarioVacaciones/js/fullcalendar-3.9.0/scheduler.min.js')}}"></script>
<script src="{{asset('Modules/CalendarioVacaciones/js/fullcalendar-3.9.0/locale-all.js')}}"></script>
<!--<script src="{{asset('js/fullcalendar-3.9.0/fullcalendar.year.js')}}"></script>-->
<script>

var $current_user = {!! json_encode(Auth::user()) !!}; 
var $pendientes = {!! json_encode($pendientes) !!}; 
var $aprovados = {!! json_encode($aprovados) !!};
var $denegados = {!! json_encode($denegados) !!};

var $niveles = {!! json_encode($niveles) !!};
var $niveles_max_limit = {!! json_encode($niveles_max_limit) !!};
var $resources = [], $eventos = [], $recursos = []; $store = [];
var $events_set, $paginacio = 15;

var $posicio_calendari = null;

// Niveles -> Subniveles -> Usuarios
function construir_resources(filtre_per_resource_id = null) {
	if ( filtre_per_resource_id == '' ) filtre_per_resource_id = null;
	var resources = [];
	
	if ( filtre_per_resource_id != null && filtre_per_resource_id.includes("subnivel") ) {
		$.each($niveles, function($index, $nivel) {
			if ( typeof($nivel) !== 'undefined' ) {
				if ( $nivel.subniveles.length > 0 ) {
					$.each($nivel.subniveles, function($index2, $subnivel) {
						if ( $subnivel.usuarios.length > 0 ) {
							$.each($subnivel.usuarios, function($index3, $usuario) {
								if ( typeof($usuario) !== 'undefined' ) {
									if ( filtre_per_resource_id == 'subnivel-' + $subnivel.id ) {
										resources.push({
											'id': 'usuario-' + $usuario.id,
											'title': ($usuario.lastname != '' ? $usuario.lastname + ', ' : '') + $usuario.name,
											'nivel_id': 'subnivel-' + $subnivel.id,
											'dies_pendents': $usuario.dies_pendents,
											'dies_vetats': $usuario.dies_vetats,
											'dies_festius': $usuario.dies_festius,
										});
									}
								}
							});
						}
					});
				}
			}
		});
	} else {
		$.each($niveles, function($index, $nivel) {
			if ( typeof($nivel) !== 'undefined' ) {
				if ( filtre_per_resource_id == null || (filtre_per_resource_id != null && filtre_per_resource_id == 'nivel-' + $nivel.id) ) {
					if ( $nivel.usuarios.length > 0 ) {
						$.each($nivel.usuarios, function($index3, $usuario) {
							if ( typeof($usuario) !== 'undefined' ) {
								if ( filtre_per_resource_id == null || filtre_per_resource_id == 'nivel-' + $nivel.id ) {
									resources.push({
										'id': 'usuario-' + $usuario.id,
										'title': ($usuario.lastname != '' ? $usuario.lastname + ', ' : '') + $usuario.name,
										'nivel_id': 'nivel-' + $nivel.id,
										'dies_pendents': $usuario.dies_pendents,
										'dies_vetats': $usuario.dies_vetats,
										'dies_festius': $usuario.dies_festius,
									});
								}
							}
						});
					}
					
					if ( $nivel.subniveles.length > 0 ) {
						$.each($nivel.subniveles, function($index2, $subnivel) {
							if ( $subnivel.usuarios.length > 0 ) {
								$.each($subnivel.usuarios, function($index3, $usuario) {
									if ( typeof($usuario) !== 'undefined' ) {
										if ( filtre_per_resource_id == null || filtre_per_resource_id == 'subnivel-' + $subnivel.id ) {
											resources.push({
												'id': 'usuario-' + $usuario.id,
												'title': ($usuario.lastname != '' ? $usuario.lastname + ', ' : '') + $usuario.name,
												'nivel_id': 'subnivel-' + $subnivel.id,
											});
										}
									}
								});
							}
						});
					}
				}
			}
		});
	}
	return resources;
}


$('#filtraUsuaris').click(function(e) {
	var usuari = $('#selecciona-usuari').val();
	var recursos = construir_resources();
	if (usuari) {
		recursos = recursos.filter(function(e) {
			return e.id.indexOf(usuari) > -1
		});
	}
	reinicialitza_calendari($events, recursos, $('#calendar').fullCalendar('getView').name);
	$('#filtrarUsuari').modal('toggle');
});

/**
 * Tags Disponibles:
 * Pending, approved, denied
 * 
 * Tipus agrupacions
 * None: Si no s'ha d'agrupar, cada event s'obtindrà individualment a la llista
 * 
 * Group: Si la agrupacio es grup, s'agruparàn els events dins del primer que coincideixi
 * la mateixa setmana, es diferencia del "DATE" en que no actualitza la data d'inici i de final
 * 
 * Date: Fa el mateix que GROUP però actualitza la data d'inici i la de final per mostrar
 * un rang al calendari
 */
function getAndTagEvents($arrays, tag, agrupacio = "Date") {
	var events = [];
	$.each($arrays, function($index, $dia) {
		// Construim el nou event
		var nouevent = {
			id: $dia.id,
			title: $dia.user_name,
			start: $dia.start,
			end: $dia.end,
			className: tag + ' ' + 'event-id-' + $dia.id,
			estado: tag,
			weekgroup: moment($dia.start).week(),
			usuario_id: $dia.user_id,
			user_level_id: $dia.user_level_id,
			any: $dia.year,
			resourceId: 'usuario-' + $dia.user_id,
			constraint: {
				resourceIds: [ 'usuario-' + $dia.user_id ] // constrain dragging to these
			},
			allDay: false,
		}
		// Agrupació d'events
		if ( agrupacio != 'None' && typeof($dia) !== 'undefined' ) {
			// Clonem el nou event per poder-lo modificar
			var aux = Object.assign({}, nouevent);

			// Revisem si existeix ja un event a aquella setmana i que pertanyi al mateix usuari
			// I no hi ha cap event al mig
			var existeix = events.find(function(x) {
				return x.usuario_id == aux.usuario_id && aux.weekgroup == x.weekgroup && aux.estado == x.estado;
			});

			// Si no te cap event aquella setmana l'usuari actual l'afegim a la llista
			if (!existeix) {
				var etiqueta = '';
				if (tag == 'approved') etiqueta = 'Aprovat';
				else if (tag == 'pending') etiqueta = 'Pendent';
				else if (tag == 'denied') etiqueta = 'Denegat';

				aux.numero = 1; aux.dies = [];
				if (agrupacio == 'Date') {
					aux.end = moment(aux.end).add(1, 'd').format('Y-MM-DD');
				}
				aux.dies.push(nouevent);
				events.push(aux);
			} else {
				// Incrementem el numero d'events d'aquella setmana
				existeix.numero++;
				// Afegim els altres dies
				existeix.dies.push(aux);

				if (agrupacio == 'Date') {
					// Altrament, si ja existia corregim les dates
					var oldStart = moment(existeix.start);
					var oldEnd = moment(existeix.end);

					var newStart = moment(nouevent.start);
					var newEnd = moment(nouevent.end).add(1, 'd');
					
					if (oldEnd < newEnd) existeix.end = newEnd.format('Y-MM-DD');
					if (oldStart > newStart) existeix.start = newStart.format('Y-MM-DD');
				}
			}
		} else {
			events.push(nouevent);
		}
	});
	return events;
}
/* INIT EVENTOS */
/**
 * Mirar TIPUS AGRUPACIO a la funció getAndTagEvents(..)
 */
function construir_eventos($tipus_agrupacio) {
	/**
	 * Obtenim els events i els col·loquem la etiqueta corresponent
	 * pel fullCalendar.
	 */
	$events_set = $tipus_agrupacio;

	var pendents = getAndTagEvents($pendientes, 'pending', $tipus_agrupacio);
	var aprovats = getAndTagEvents($aprovados, 'approved', $tipus_agrupacio);
	var denegats = getAndTagEvents($denegados, 'denied', $tipus_agrupacio);

	return [].concat(pendents, aprovats, denegats);
}
/* end INIT EVENTOS */


function inicialitzarStore(events) {
	var result = [];
	events.forEach(function(evt) {
		result.push({
				'id': evt.id,
				'className': evt.className,
				'estado': evt.estado,
				'usuario_id': evt.usuario_id,
				'any': evt.any,
				'start': evt.start,
				'end': evt.end
		});
	});
	return result;
}


var scroll;

$(document).ready(function() {
	// Lloc on guardarem els canvis
	$store = inicialitzarStore(construir_eventos("None"));
	$store2 = inicialitzarStore(construir_eventos("None"));

	// Agafar primer nivell per no carregar tots els events
	$('#selecciona-nivells option:selected').prop('selected', false);
	var primer_nivell = $('#selecciona-nivells option[value!=""]').first();
	primer_nivell.prop('selected', true);

	$resources = construir_resources( primer_nivell.val() );
	$eventos = construir_eventos("Group");
	var today = new Date().toISOString().slice(0,10);

	// Init Calendar
	reinicialitza_calendari($eventos, $resources, 'timelineYearPerWeek');
	
	// Enviar Dies
	$('#form_enviar_dies').submit(function(e) {
		/*
		 Quan agafa de nou els events, caldrà canviar primer de conjunt de events. Si al 
		 inici tenim el grup d'events agrupats 
		*/
		$store_canviado = $store.filter(function(e) { return typeof(e.canviado) !== 'undefined' && e.canviado });
		$('#dies').val(JSON.stringify($store_canviado));
		//console.log($store_canviado);
		//e.preventDefault();
		return true;
		//return false;
	});
});
	
Array.prototype.remove = function() {
	var what, a = arguments, L = a.length, ax;
	while (L && this.length) {
		what = a[--L];
		while ((ax = this.indexOf(what)) !== -1) {
			this.splice(ax, 1);
		}
	}
	return this;
};

/**
 * Aquesta funció recorre tots els DOM elements que corresponen als events i
 * els afegeix una Tooltip https://mdbootstrap.com/docs/jquery/javascript/tooltips/
 * amb els dies que representen, en cas d'estar agrupats.
 */
function colocarTooltip() {
	eventids = [];
	// Per cada DOM element
	var event = $('.fc-timeline-event').each(function(idx, element) {
		// Obtenim l'id de l'event col·locat durant la construcció del event inclós en la classe
		var eventid = $(element).attr('class').split(' ').filter(function(clas) {
			var regexp = new RegExp("event-id-");
			return regexp.test(clas)
		})[0].replace('event-id-', '');

		if (eventid.length > 0) {
			// Obtenim l'event del calendari
			var $currEvent = $('#calendar').fullCalendar('clientEvents', eventid)[0];
			// Començem a generar el text a mostrar, però primer caldrà saber les dates per ordenar-les
			var tooltip = [];
			// Afegim tots es les dates en format moment js dins l'array de tooltip
			if ( typeof($currEvent) != 'undefined' ) {
				$currEvent.dies.forEach(function(element) {
					moment.locale('ca');
					tooltip.push(moment(element.start));
				});
			}
			// Començem a generar el contingut de la tooltip
			var title = '';
			tooltip.sort(function(a, b) {
				var dateA = moment(a || '10000-01-01');
				var dateB = moment(b || '10000-01-01');
				return dateA.diff(dateB);
			}).forEach(function(e) {
				// Per cada data, obtenim el nom del Dia de la setmana actual
				title += e.format('dddd') + '.';
			});

			// Finalment mostrem la tooltip
			$(element).tooltip({
				html: true,
				trigger: 'hover',
				title: title.split('.').join('<br>').toUpperCase()
			});
		}
	});
}

function generarTotal(eventos, tipus = 'approved') {
	var evts = [];
	for (var i = 1; i < 53; i++) {
		var total = 0;
		eventos.forEach(function(k) {
			total += k.dies.filter(function(e) {return e.estado == tipus && e.weekgroup == i}).length
		});
		evts.push({
			id: 'week-'+ tipus + '-' + i,
			title: "APROVATS ("+total+")",
			start: moment('2019-01-01').startOf('month').add(i, 'w').subtract(1, 'w'),
			end: moment('2019-01-01').startOf('month').add(i, 'w').subtract(1, 'w').add(1, 'd'),
			num: eventos.filter(function(e) {return e.estado == tipus && e.weekgroup == i}).length,
			className: 'setmana-'+tipus,
			resourceId: 'total'
		});
	}
	return evts;
}

var $general_any = parseInt(moment().format('Y'));
var $aquest_any = parseInt(moment().format('Y'));
var $any_final = typeof($current_user.rol) !== 'undefined' && $current_user.rol.level >= 2 ? $general_any + 2 : $general_any + 1;

function reinicialitza_calendari(eventos, recursos, view) {
	var today = $general_any + '-01-01';
	var weeks_any = moment( $general_any ).isoWeeksInYear();
	
	$events = eventos;
	$recursos = recursos;

	$('#calendar').fullCalendar('destroy');
	$('#calendar').fullCalendar({
		now: today,
		defaultDate: today,
		defaultView: view,
		validRange: {
			start: ($aquest_any - 1) + '-01-01',
			end: $any_final + '-12-31',
		},
		titleFormat: '[' + $general_any + ']',
		header: {
			left: 'prev,next filter_dies filter_user',
			center: 'title',
			//right: 'month,basicWeek,basicDay,timelineDay,timelineTenDay,timelineMonth,timelineYear'
			//right: 'timelineYearPerWeek,timelineYear,timelineMonth,timelineWeek,timelineDay',
			right: 'timelineYearPerWeek,timelineYear,timelineMonth',
		},
		buttonText: {
			timelineYear: '@Lang('Year')'
		},
		customButtons: {
			prev: {
				click: function() {
					$posicio_calendari = $('.fc-scroller').last().scrollLeft();
					$general_any -= 1;
					if ( $('#calendar').fullCalendar('getView').name == 'timelineYearPerWeek' ) {
						var events = construir_eventos("Group");
						reinicialitza_calendari(events, $recursos, 'timelineYearPerWeek');
					} else {
						reinicialitza_calendari(events, $recursos, $('#calendar').fullCalendar('getView').name);
					}
					//$('#calendar').fullCalendar('prev');
				}
			},
			next: {
				click: function() {
					$posicio_calendari = $('.fc-scroller').last().scrollLeft();
					$general_any += 1;
					if ( $('#calendar').fullCalendar('getView').name == 'timelineYearPerWeek' ) {
						var events = construir_eventos("Group");
						reinicialitza_calendari(events, $recursos, 'timelineYearPerWeek');
					} else {
						reinicialitza_calendari(events, $recursos, $('#calendar').fullCalendar('getView').name);
					}
					//$('#calendar').fullCalendar('next');
				}
			},
			filter_dies: {
				text: '@Lang('Filter by levels')',
				click: function() {
					//$('#seleccionar-filter-dies-btn').click();
					$('#myModal').modal('toggle');
				}
			},
			filter_user: {
				text: '@Lang('Filter by users')',
				click: function() {
					$('#filtrarUsuari').modal('toggle');
					//$('#seleccionar-filter-users-btn').click();
				}
			},
			timelineYearPerWeek: {
				text: 'Any (Setmanes)',
				click: function() {
					var events = construir_eventos("Group");
					reinicialitza_calendari(events, $recursos, 'timelineYearPerWeek');
				}
			},
			timelineYear: {
				text: '@Lang('Year')',
				click: function() {
					if ($events_set != 'None') var events = construir_eventos("None");
					else var events = $events;

					reinicialitza_calendari(events, $recursos, 'timelineYear');
				}
			},
			timelineMonth: {
				text: '@Lang('Month')',
				click: function() {
					if ($events_set != 'None') var events = construir_eventos("None");
					else var events = $events;
					reinicialitza_calendari(events, $recursos, 'timelineMonth');
				}
			},
			timelineWeek: {
				text: '@Lang('Week')',
				click: function() {
					if ($events_set != 'None') var events = construir_eventos("None");
					else var events = $events;

					reinicialitza_calendari(events, $recursos, 'timelineWeek');
				}
			},
			timelineDay: {
				text: '@Lang('dia')',
				click: function() {
					if ($events_set != 'None') var events = construir_eventos("None");
					else var events = $events;
					
					reinicialitza_calendari(events, $recursos, 'timelineDay');
				}
			}
		},
		views: {
			timelineWeek: {
				type: 'timelineWeek',
				//duration: { days: 7 },
				groupByResource: true,
				slotDuration: '24:00:00'
			},
			timelineDay: {
				type: 'timeline',
				duration: { days: 1 },
				groupByResource: true,
				slotDuration: '24:00:00'
			},
			timelineYearPerWeek: {
				slotWidth: 100,
				type: 'timeline',
				duration: { weeks: weeks_any },
				groupByResource: true,
				slotDuration: { weeks: 1 },
				slotLabelFormat: [
					'W', // top level of text
					//'ddd'        // lower level of text
				]
			},
			timelineYear: {
				type: 'timeline',
				groupByResource: true,
				slotDuration: { days: 1 }
			},
		},
		locale: '{{$language_code}}',
		firstDay: 1,
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		eventLimit: 1,
		navLinks: true, // can click day/week names to navigate views
		resourceAreaWidth: '15%',
		resourceLabelText: '@Lang('Users')',
		resources: $recursos,
		events: $events,
		eventOverlap: true,
		selectable: true,
		eventDrop: function(event, delta, revertFunc) {
			$('#calendar').fullCalendar('updateEvent', event);
			
			if ( $('#calendar').fullCalendar('getView').name == 'timelineYearPerWeek' ) {
				// Canviar els dies de l'agrupació
				var num_week_act = parseInt(event.start.format('w'));
				event.dies.forEach(function(e, key) {
					var num_week_old = parseInt(moment(e.start).format('w'));
					event.dies[key].start = moment(e.start).add(num_week_act - num_week_old, 'weeks').format('Y-MM-DD');
					event.dies[key].end = moment(e.end).add(num_week_act - num_week_old, 'weeks').format('Y-MM-DD');
					$store.find(o => o.id === e.id).start = event.dies[key].start;
					$store.find(o => o.id === e.id).end = event.dies[key].end;
					$store.find(o => o.id === e.id).canviado = true;
				});

				colorColumns();
			}

			
			return true;
		},
		eventClick: function(calEvent, jsEvent, view) {
			// Preparem el modal per ser mostrat
			if (view.name == 'timelineYearPerWeek') {
				$('#selecciona-dia').empty();
				$('#estat-id').val(calEvent.id);
				$('#estat-vista').val(view.name);
				calEvent.dies.forEach(function(e) {
					$('#selecciona-dia').append(`<option value="${e.id}-${e.start}">${e.start}</option>`);
				});
				$('#selecciona-dia').selectpicker("refresh");
				$('#canviarEstat').modal('toggle');
				colorColumns();
			} else {
				var event = $store.find(function(e) { return e.id == calEvent.id });
				if ( calEvent.className.includes('pending') ) {
					event.className = event.className.replace('pending', 'approved');
					event.estado = 'approved';
					
					calEvent.className.remove('pending');
					calEvent.className.push('approved');
					calEvent.estado = 'approved';
				} else if ( calEvent.className.includes('approved') ) {
					event.className = event.className.replace('approved', 'denied');
					event.estado = 'denied';

					calEvent.className.remove('approved');
					calEvent.className.push('denied');
					calEvent.estado = 'denied';
				} else if ( calEvent.className.includes('denied') ) {
					event.className = event.className.replace('denied', 'pending');
					event.estado = 'pending';

					calEvent.className.remove('denied');
					calEvent.className.push('pending');
					calEvent.estado = 'pending';
				}

				$('#calendar').fullCalendar('updateEvent', calEvent);
			}
		},
		viewRender: function (view) {

			if ( $posicio_calendari == null ) {
				var punt_data = today;
				if ( view.name == "timelineYearPerWeek" ) {
					punt_data = moment( today ).startOf('isoWeek').format('Y-MM-DD');
				}
				
				var elemento = $('.fc-widget-header[data-date=' + punt_data + ']').position();
				if ( typeof(elemento) !== 'undefined' ) {
					//$('.fc-scroller').animate({scrollLeft: elemento.left}, 500);
					$('.fc-scroller').scrollLeft(elemento.left);
				}
			} else {
				$('.fc-scroller').scrollLeft($posicio_calendari);
				$posicio_calendari = null;
			}
			if ( view.name == "timelineYearPerWeek" ) colorColumns();
		},
		dayClick: function(date, jsEvent, view, resourceObj) {
			$('#afegirEvent .modal-body .dies').html('');
			$('#afegirEvent .modal-body .dies').append(`<input id="resource_id" type="hidden" value="${resourceObj.id}">`);
			var avui = new Date().toISOString().slice(0,10);
			var es_dia_valid = date.isAfter(avui);
			var year = parseInt(date.format('Y'));
			var dies_pendents = resourceObj.dies_pendents[year];
			$('#afegirEvent').modal('toggle');
			var dia = date;
			for ( var i = 0; i < dies_pendents; i++ ) {
				$('#afegirEvent .modal-body .dies').append(`
				<div class="form-check" style="min-width: 150px;">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" value="${dia.format('DD-MM-Y')}"> ${dia.format('DD/MM/Y')}
						<span class="form-check-sign">
							<span class="check"></span>
						</span>
					</label>
				</div>
				`);
				
				var dia_valid = false;
				while ( !dia_valid ) {
					dia.add(1, 'days');
					var dia_js = date.toDate();
					es_weekend = dia_js.getDay() == 6 || dia_js.getDay() == 0;
					//es_weekend = false;
					es_vetat = resourceObj.dies_vetats.indexOf(dia.format('Y-MM-DD')) >= 0;
					es_festiu = resourceObj.dies_festius.indexOf(dia.format('Y-MM-DD')) >= 0;
					dia_valid = !es_weekend && !es_vetat && !es_festiu;
				}
			}
		}/*,
		select: function(start, end, jsEvent, view) {
			if (moment().diff(start, 'days') > 0) {
				$('#calendar').fullCalendar('unselect');
				return false;
			}
		}*/
	});

	if (view == 'timelineYearPerWeek') {
		colocarTooltip();
		colorColumns();
	}

	colorResources();
}
</script>
<style>
.circle {
	border-radius: 50%;
	width: 20px;
	height: 20px;
	background-color: #cecece;
	opacity: 0.3;
}

.circle::before {
	content: "+";
	height:20px;
	width:20px;
	font-size:20px;
	display:flex;
	flex-direction:row;
	align-items:center;
	justify-content:center;
	font-weight:bold;
	font-family:courier;
	color:white;
}
</style>
<script>
/* MODAL GUARDAR ESTATS */
$("#guardaEstats").click(function(e) {
	$('#canviarEstat').modal('toggle');

	// Canviem el valor a l'store
	var id = $('#estat-id').val(); // ID del pare
	var newState = $('#selecciona-estat').val(); // Nou Estat

	var event = $('#calendar').fullCalendar('clientEvents', id)[0];

	if (event) {
		var dies = $('#selecciona-dia option:selected').map(function(i, e) { return $(e).text() }).toArray();
		var oldState = event.estado;
		var element = event.dies.filter(function(e) { return dies.includes(e.start) })
			.map(function(e) {
				return $pendientes.find(function(el) { return e.id == el.id }) ||
					$aprovados.find(function(el) { return e.id == el.id }) ||
					$denegados.find(function(el) { return e.id == el.id });
			});
		
		if (element.length > 0) {
			element.forEach(function(el) {
				var upd = $store.find(function(e) { return e.id == el.id });
				el.estado = upd.estado = el.status = newState;
				upd.className = upd.className.replace(oldState, newState);
				upd.canviado = true;

				if (oldState == 'pending') $pendientes.remove(el);
				else if (oldState == 'approved') $aprovados.remove(el);
				else if (oldState == 'denied') $denegados.remove(el);

				if (newState == 'approved') $aprovados.push(el);
				else if (newState == 'pending') $pendientes.push(el);
				else if (newState == 'denied') $denegados.push(el);
			});
		}
	}

	if ($('#estat-vista').val() == 'timelineYearPerWeek') {
		$eventos = construir_eventos("Group");
	} else {
		$eventos = construir_eventos("None");
	}

	$posicio_calendari = $('.fc-scroller').last().scrollLeft();
	reinicialitza_calendari($eventos, $recursos, $('#estat-vista').val());
});

/* FI MODAL GUARDAR ESTATS */
/* MODAL */
var action = '';
$('#myModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	action = button.data('action');
});

$('#myModal').find('button[type=submit]').on('click', function() {
	var nivells_sel = $('#selecciona-nivells').val();
	if (nivells_sel.length < 1 || nivells_sel[0] != '') {
		var resources = [];
		$.each(nivells_sel, function(index, nivell_sel) {
			if ( nivell_sel != '' || (nivell_sel == '' && nivells_sel.length == 1 ) ) {
				resources = resources.concat(construir_resources(nivell_sel) );
			}
		});
	} else {
		$('.fc-day.colored').css('background', 'none');
		$('.fc-day.colored').remove('colored');
		var resources = construir_resources(null);
	}
	
	reinicialitza_calendari($eventos, resources, 'timelineYearPerWeek');
	$('#myModal').modal('hide');
	$('#myModal').removeClass('in');
});
/* end MODAL */

var groupBy = function(xs, key) {
  return xs.reduce(function(rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, []);
};

function colorColumnsClear() {
	$('.fc-day').removeClass('colored');
	$('.fc-day').css('background', 'rgba(0, 0, 0, 0)');

	var tots_events = $('#calendar').fullCalendar('clientEvents');
	var columnes = groupBy(tots_events, 'weekgroup');
	columnes.forEach(function(col) {
		var dilluns_date = moment(col[0].start).startOf('week');
		var dilluns = dilluns_date.format('Y-MM-DD');
		$('.fc-widget-header[data-date="'+dilluns+'"]').find('.fc-cell-text').html(dilluns_date.format('w'));
	});
}
function colorColumns() {
	colorColumnsClear();

	var aprovats = [], columnes = [];
	$('.fc-day').removeClass('colored');
	$('.fc-day').css('background', 'rgba(0, 0, 0, 0)');

	var nivells_sel = $('#selecciona-nivells').val();
	if ( nivells_sel.length == 1 && nivells_sel[0] != '' ) {
		var nivel_sel = nivells_sel[0];
		aprovats = $('#calendar').fullCalendar('clientEvents').filter(function(x) {return x.estado != 'denied' && x.user_level_id == nivel_sel});
		columnes = groupBy(aprovats, 'weekgroup');
		columnes.forEach(function(col) {
			var dilluns_date = moment(col[0].start).startOf('week');
			var dilluns = dilluns_date.format('Y-MM-DD');
			var max_limit = get_max_limit(col[0].user_level_id, dilluns_date);
			if (col.length > max_limit) {
				$('.fc-day[data-date="'+dilluns+'"]').addClass('colored');
				$('.fc-day[data-date="'+dilluns+'"]').css('background', 'rgba(255, 0, 0, 0.2)');
			}
			// Reescriure header num semana
			var week = dilluns_date.format('w');
			var titol_header = week + " (" + col.length + "/" + max_limit + ")";
			$('.fc-widget-header[data-date="'+dilluns+'"]').find('.fc-cell-text').html(titol_header);
		});
	}
	return columnes;
}

function get_max_limit(nivel_id, dia) {
	var year = dia.format('YYYY');
	var week = dia.format('w');
	if ( $niveles_max_limit[nivel_id] !== undefined ) {
		if ( $niveles_max_limit[nivel_id][year + '-' + week] !== undefined )
			return $niveles_max_limit[nivel_id][year + '-' + week];
		else 
			return $niveles_max_limit[nivel_id]['general'];
	}
	else return 0;
}

function colorResources() {
	$('#calendar').fullCalendar('getResources').forEach(function(r) {
		if ( $('#calendar').fullCalendar( 'getResourceEvents', r.id).length == 0 ) {
			$('.fc-resource-area tr[data-resource-id=' + r.id + '] td').css('background', 'rgba(255, 0, 0, 0.2)');
		}
	});
}
</script>

<!-- EXPORTAR -->
<script>
function exportar(e) {	
	var usuari = $('#selecciona-usuari').val();
	var nivells = $('#selecciona-nivells').val();
	var year = $('#calendar').fullCalendar('getDate').year();
	$('#exportar-vacaciones-form').find('input[type=hidden][name=usuari]').val( usuari );
	$('#exportar-vacaciones-form').find('input[type=hidden][name=nivells]').val( nivells );
	$('#exportar-vacaciones-form').find('input[type=hidden][name=year]').val( year );
	$('#exportar-vacaciones-form').submit();
}
</script>
<!-- end EXPORTAR -->
<script>
/* MODAL GUARDAR EVENT */
$("#guardaEvent").click(function(e) {
	$('#afegirEvent').modal('toggle');

	var dies = $('#afegirEvent .modal-body input[type=checkbox]:checked');
	var estat = $('#afegirEvent #selecciona-estat').val();
	var resource_id = $('#resource_id').val();
	var resource = $('#calendar').fullCalendar('getResources').filter(function(e) { return e.id == resource_id })[0];

	dies.each(function(){
		var item = $(this);
		var now = moment().format("YYYY-MM-DD HH:mm:ss");
		var dia = moment(item.val(), 'DD-MM-YYYY');
		var dia_end = moment(item.val(), 'DD-MM-YYYY').add(1, 'days');
		var start = dia.format('Y-MM-DD');
		var end = dia_end.format('Y-MM-DD');
		var el = {
			'id': resource_id + '_' + dia,
			'className': estat + ' ' + resource_id + '_' + dia,
			'created_at': now,
			'deleted_at': null,
			'end': end,
			'start': start,
			'status': estat,
			'updated_at': now,
			'user_aproved_id': $current_user.id,
			'user_id': resource_id.replace('usuario-', ''),
			'user_level_id': resource.nivel_id,
			'user_name': resource.title,
			'year': dia.format('Y'),
		};
		if (estat == 'approved') $aprovados.push(el);
		else if (estat == 'pending') $pendientes.push(el);
		else if (estat == 'denied') $denegados.push(el);

		// Afegir al store
		$store.push({
			'id': el.id,
			'className': el.className,
			'estado': el.status,
			'usuario_id': el.user_id,
			'any': el.year,
			'start': el.start,
			'end': el.end,
			'canviado': true,
		});
	});

	var vista = $('#calendar').fullCalendar('getView');
	if ( vista.name == 'timelineYearPerWeek') {
		$eventos = construir_eventos("Group");
	} else {
		$eventos = construir_eventos("None");
	}

	$posicio_calendari = $('.fc-scroller').last().scrollLeft();
	reinicialitza_calendari($eventos, $('#calendar').fullCalendar('getResources'), vista.name);
});
/* FI MODAL GUARDAR EVENT */

/* MODAL ELIMINAR DIES */
$("#eliminarDies").click(function(e) {
	$('#canviarEstat').modal('toggle');

	// Canviem el valor a l'store
	var id = $('#estat-id').val(); // ID del pare
	var newState = 'deleted';

	var event = $('#calendar').fullCalendar('clientEvents', id)[0];

	if (event) {
		var dies = $('#selecciona-dia option:selected').map(function(i, e) { return $(e).text() }).toArray();
		var oldState = event.estado;
		var element = event.dies.filter(function(e) { return dies.includes(e.start) })
			.map(function(e) {
				return $pendientes.find(function(el) { return e.id == el.id }) ||
					$aprovados.find(function(el) { return e.id == el.id }) ||
					$denegados.find(function(el) { return e.id == el.id });
			});
		
		if (element.length > 0) {
			element.forEach(function(el) {
				var upd = $store.find(function(e) { return e.id == el.id });
				el.estado = upd.estado = el.status = newState;
				upd.className = upd.className.replace(oldState, newState);
				upd.canviado = true;

				if (oldState == 'pending') $pendientes.remove(el);
				else if (oldState == 'approved') $aprovados.remove(el);
				else if (oldState == 'denied') $denegados.remove(el);
			});
		}
	}

	var vista = $('#calendar').fullCalendar('getView');
	if (vista.name == 'timelineYearPerWeek') {
		$eventos = construir_eventos("Group");
	} else {
		$eventos = construir_eventos("None");
	}

	$posicio_calendari = $('.fc-scroller').last().scrollLeft();
	reinicialitza_calendari($eventos, $('#calendar').fullCalendar('getResources'), vista.name);
});
/* FI MODAL ELIMINAR */
</script>
@endsection