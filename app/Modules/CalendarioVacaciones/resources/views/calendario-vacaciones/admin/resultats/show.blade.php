@extends('Admin::layouts.admin')

@section('section-title')
    {{ $section_title }}
@endsection

@php

$d_trans = $item->translate();
$preguntes = $item->get_field('form');
if ( !is_array($preguntes) ) $preguntes = (array)json_decode( $preguntes );
$custom_field_id = $item->get_field_id('form');
$user_results = \App\Models\UserMeta::get_meta($item->id, $custom_field_id);

@endphp

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card pb-5">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">{{ $section_icon }}</i>
                </div>
                <h4 class="card-title">
                    {{ $item->title }}
                    @if ( !empty($toolbar_header) )
                        @foreach ( $toolbar_header as $tool )
                            {!! $tool !!}
                        @endforeach
                    @endif
                </h4>
            </div>
            <div class="card-body">
                <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                    @if ( !empty($toolbar) )
                        @foreach ( $toolbar as $tool )
                            {!! $tool !!}
                        @endforeach
                    @endif
                </div>
            </div>
            @foreach ($preguntes as $k => $p)
            <div id="accordion" role="tablist" class="pl-5 pr-5 pt-2 pb-2">
                <div class="card-collapse">
                    <div class="card-header" role="tab" id="heading{{$k+1}}">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" href="#Question{{$k+1}}" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                {{$k+1}}. {{$p->question}}
                                <i class="material-icons">keyboard_arrow_down</i>
                            </a>
                        </h5>
                    </div>
                    <div id="Question{{$k+1}}" class="collapse" role="tabpanel" aria-labelledby="heading{{$k+1}}" data-parent="#accordion" style="">
                        <div class="card p-2">
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>@Lang('User')</th>
                                        <th>@Lang('Result')</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>@Lang('User')</th>
                                        <th>@Lang('Result')</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                @forelse( $user_results as $result )
                                    @php( $answer = json_decode($result->value) )
                                    @if ( empty($answer[$k]) ) @continue
                                    @else
                                    <tr>
                                        <td>{{ $result->user->email }}</td>
                                        <td>{{ $answer[$k] }}</td>
                                    </tr>
                                    @endif
                                @empty
                                    <tr><td colspan="2">@Lang('No entries found.')</td></tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>        
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
    var table = $('.table').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "@Lang('All')"]
        ],
        responsive: true,
        /*language: {
            search: "_INPUT_",
            searchPlaceholder: "@Lang('Search')",
        }*/
        language: { "url": "{{asset('js/datatables/Catalan.json')}}" }
    });
});
</script>
@endsection