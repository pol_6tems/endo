@extends('layouts.admin')

@section('section-title')
    @Lang('Levels') - @Lang('Years')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Add new')</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.calendario.niveles.years.store') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="calendario_nivel_id" value="{{$id}}" />

        <div class="form-group">
            <label for="year" class="bmd-label-floating">@Lang('Year')</label>
            <input type="number" class="form-control" name="year" name="year" value="0" tabindex="1" min="0" required>
        </div>
        <div class="form-group">
            <label for="days" class="bmd-label-floating">@Lang('Days')</label>
            <input type="number" class="form-control" name="days" name="days" value="0" tabindex="2" min="0" required>
        </div>
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.calendario.niveles.edit', $id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
        </div>
    </form>

</div>
@endsection