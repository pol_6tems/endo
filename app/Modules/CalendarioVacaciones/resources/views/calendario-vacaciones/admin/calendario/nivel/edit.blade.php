@extends('layouts.admin')

@section('section-title')
    @Lang('Levels')
@endsection

@section('content')
<div class="">
    <div class="card-noconflict p-3">
        <h4>
            @Lang('Edit'): {{ $item->name }}
        </h4>

        @if ($errors->count() > 0)
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <form action="{{ route('admin.calendario.niveles.update', $item->id) }}" method="post">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
            <input type="hidden" name="status" value="{{ $item->status }}" />
            
            <div class="form-group">
                <label for="name" class="bmd-label-floating">@Lang('Name')</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $item->name }}" required tabindex="1">
            </div>
            <div class="form-group">
                <label for="sel_dia_o_semana" class="bmd-label-floating" style="top: 0.4rem;">@Lang('Selection')</label>
                <select class="form-control custom-select" name="sel_dia_o_semana" data-live-search="true" title="@Lang('Selection')" required tabindex=2>
                    <option value="dia" {{ ($item->sel_dia_o_semana == 'dia' ? 'selected' : '') }}>@Lang('dia')</option>
                    <option value="semana" {{ ($item->sel_dia_o_semana == 'semana' ? 'selected' : '') }}>@Lang('semana')</option>
                </select>
            </div>
            <div class="form-group">
                <label for="no_bloquejat" class="bmd-label-floating" style="top: 0.4rem;">@Lang('Blocked per request')</label>
                <select class="form-control custom-select" name="no_bloquejat" data-live-search="true" title="@Lang('Blocked per request')" required tabindex=3>
                    <option value="0" {{ ($item->no_bloquejat == 0 ? 'selected' : '') }}>@Lang('Yes')</option>
                    <option value="1" {{ ($item->no_bloquejat == 1 ? 'selected' : '') }}>@Lang('No')</option>
                </select>
            </div>
            <div class="form-group">
                <label for="max_limit" class="bmd-label-floating">@Lang('Limit')</label>
                <input type="number" class="form-control" name="max_limit" name="max_limit" value="{{ $item->max_limit }}" tabindex="4" min="0">
            </div>        
            <div class="form-group mt-5">
                <input onclick="window.location.href='{{ route('admin.calendario.niveles.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
                <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
            </div>
                
            </div>
        </form>
    </div>

    <!-- CalendarioNivelYear -->
    <div class="col-sm-12 mt-5 mb-5">
        <h4>
            @Lang('Personalized holiday pending days')
            <a href="{{ route('admin.calendario.niveles.years.create', $item->id) }}" class="btn btn-default btn-raised">
                @Lang('Add year')
            </a>
        </h4>

        <div class="card">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>@Lang('Year')</th>
                    <th>@Lang('Days')</th>
                    <th width="5%">@Lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @forelse($years as $year)
                <tr>
                    <td>{{ $year->year }}</td>
                    <td>{{ $year->days }}</td>
                    <td class="cela-opcions">
                        <div class="btn-group">
                            <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $year->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </button>  
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $year->id }}">
                                <a class="dropdown-item" href="{{ route('admin.calendario.niveles.years.edit', $year->id) }}">@Lang('Edit')</a>
                                <!--<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('admin.calendario.niveles.destroy', $year->id) }}" data-nombre="@Lang('Year'): {{$year->year}} - @Lang('Days'): {{$year->days}}">@Lang('Delete')</button>-->
                            </div>
                        </div>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="3">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        </div>
        {{ $years->links() }}
    </div>
    <!-- end CalendarioNivelYear -->

    <!-- CalendarioNivelWeek -->
    <div class="col-sm-12 mt-5 mb-5">
        <h4>
            @Lang('Employee limit per week')
            <a href="{{ route('admin.calendario.niveles.weeks.create', $item->id) }}" class="btn btn-default btn-raised">
                @Lang('Add week')
            </a>
        </h4>

        <div class="card">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>@Lang('Year')</th>
                    <th>@Lang('Week')</th>
                    <th>@Lang('Limit')</th>
                    <th width="5%">@Lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @forelse($weeks as $week)
                <tr>
                    <td>{{ $week->year }}</td>
                    <td>{{ $week->week }}</td>
                    <td>{{ $week->max_limit }}</td>                    
                    <td class="cela-opcions">
                        <div class="btn-group">
                            <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $week->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </button>  
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $week->id }}">
                                <a class="dropdown-item" href="{{ route('admin.calendario.niveles.weeks.edit', $week->id) }}">@Lang('Edit')</a>
                                <!--<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('admin.calendario.niveles.destroy', $week->id) }}" data-nombre="@Lang('Year'): {{$week->year}} - @Lang('Week'): {{$week->week}}">@Lang('Delete')</button>-->
                            </div>
                        </div>
                    </td>
                    
                </tr>
                @empty
                    <tr>
                        <td colspan="4">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        </div>
        {{ $weeks->links() }}
    </div>
    <!-- end CalendarioNivelWeek -->
    
    <!-- CalendarioNivelEncarregat -->
    <div class="col-sm-12 mt-5 mb-5">
        <h4>
            @Lang('Managers')
            <a href="{{ route('admin.calendario.niveles.encarregat.create', $item->id) }}" class="btn btn-default btn-raised">
                @Lang('Add manager')
            </a>
        </h4>

        <div class="card">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th width="5%">@Lang('Id')</th>
                    <th>@Lang('Manager')</th>
                    <th>@Lang('Email')</th>
                    <th width="15%">@Lang('Role')</th>
                    <th width="5%">@Lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @forelse($encarregats as $encarregat)
                <tr>
                    <td>{{ $encarregat->user_id }}</td>
                    <td>{{ $encarregat->user->fullname() }}</td>
                    <td>{{ $encarregat->user->email }}</td>
                    <td>{{ __($encarregat->user->role) }}</td>
                    <td class="cela-opcions">
                        <div class="btn-group">
                            <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $encarregat->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </button>  
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $encarregat->id }}">
                                <a class="dropdown-item" href="{{ route('admin.calendario.niveles.encarregat.edit', $encarregat->id) }}">@Lang('Edit')</a>
                                <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('admin.calendario.niveles.encarregat.destroy', $encarregat->id) }}" data-nombre="@Lang('Manager'): {{ $encarregat->user->fullname() }} - @Lang('Email'): {{ $encarregat->user->email }}">@Lang('Delete')</button>
                            </div>
                        </div>
                    </td>
                    
                </tr>
                @empty
                    <tr>
                        <td colspan="5">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        </div>
        {{ $encarregats->links() }}
    </div>
    <!-- end CalendarioNivelEncarregat -->
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script>
   $('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var url = button.data('url');
		var nombre = button.data('nombre');
		var modal = $(this);
		modal.find('form').attr('action', url);
		modal.find('.modal-body').html("@Lang('Are you sure to delete it?')");
	});
</script>
@endsection