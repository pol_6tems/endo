@extends('layouts.admin')

@section('section-title')
    @Lang('Levels') - @Lang('Managers')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Add new')</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.calendario.niveles.encarregat.store') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="calendario_nivel_id" value="{{ $id }}" />

        <div class="form-group">
            <label for="user_id" class="bmd-label-floating" style="top: 0.4rem;">@Lang('User')</label>
            <select class="form-control custom-select" name="user_id" data-live-search="true" title="@Lang('User')" required tabindex=1>
                @foreach ($users as $user)
                    <option value="{{$user->id}}">{{$user->fullname()}} ({{$user->email}}) - @Lang('Role'): {{ __($user->role) }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.calendario.niveles.edit', $id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
        </div>
    </form>

</div>
@endsection