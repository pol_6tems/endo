@extends('layouts.admin')

@section('section-title')
    @Lang('Sublevels')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Add new')</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.calendario.subniveles.store') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="status" value="publish" />

        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" class="form-control" name="name" name="name" value="{{ old('name') }}" tabindex="1" required>
        </div>
        <div class="form-group">
            <label for="sel_dia_o_semana" class="bmd-label-floating" style="top: 0.4rem;">@Lang('Selection')</label>
            <select class="form-control custom-select" name="sel_dia_o_semana" data-live-search="true" title="@Lang('Selection')" required tabindex=2>
                <option value="dia" selected>@Lang('dia')</option>
                <option value="semana">@Lang('semana')</option>
            </select>
        </div>
        <div class="form-group">
            <label for="no_bloquejat" class="bmd-label-floating" style="top: 0.4rem;">@Lang('Blocked per request')</label>
            <select class="form-control custom-select" name="no_bloquejat" data-live-search="true" title="@Lang('Blocked per request')" required tabindex=3>
                <option value="0" selected>@Lang('Yes')</option>
                <option value="1">@Lang('No')</option>
            </select>
        </div>
        <div class="form-group">
            <label for="calendario_nivel_id" class="bmd-label-floating" style="top: 0.4rem;">@Lang('Level')</label>
            <select class="form-control custom-select" name="calendario_nivel_id" data-live-search="true" title="@Lang('Level')" required tabindex=4>
                @foreach ($niveles as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="max_limit" class="bmd-label-floating">@Lang('Limit')</label>
            <input type="number" class="form-control" name="max_limit" name="max_limit" value="0" tabindex=5 min="0">
        </div>
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.calendario.subniveles.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
        </div>
    </form>

</div>
@endsection