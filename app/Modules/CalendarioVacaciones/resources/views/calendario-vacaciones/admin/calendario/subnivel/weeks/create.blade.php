@extends('layouts.admin')

@section('section-title')
    @Lang('Sublevels') - @Lang('Weeks')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Add new')</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.calendario.subniveles.weeks.store') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="calendario_subnivel_id" value="{{$id}}" />

        <div class="form-group">
            <label for="year" class="bmd-label-floating">@Lang('Year')</label>
            <input type="number" class="form-control" name="year" name="year" value="0" tabindex="1" min="0" required>
        </div>
        <div class="form-group">
            <label for="week" class="bmd-label-floating">@Lang('Week')</label>
            <input type="number" class="form-control" name="week" name="week" value="0" tabindex="2" min="0" required>
        </div>
        <div class="form-group">
            <label for="max_limit" class="bmd-label-floating">@Lang('Limit')</label>
            <input type="number" class="form-control" name="max_limit" name="max_limit" value="0" tabindex="3" min="0" required>
        </div>
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.calendario.subniveles.edit', $id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
        </div>
    </form>

</div>
@endsection