@extends('layouts.admin')

@section('section-title')
    @Lang('Sublevels') - @Lang('Weeks')
@endsection

@section('content')
<div class="card-noconflict p-3">
    <h4>@Lang('Edit')</h4>

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('admin.calendario.subniveles.weeks.update', $item->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="calendario_subnivel_id" value="{{ $item->calendario_subnivel_id }}" />
        
        <div class="form-group">
            <label for="year" class="bmd-label-floating">@Lang('Year')</label>
            <input type="number" class="form-control" name="year" name="year" value="{{ $item->year }}" tabindex="1" min="0" required>
        </div>
        <div class="form-group">
            <label for="week" class="bmd-label-floating">@Lang('Week')</label>
            <input type="number" class="form-control" name="week" name="week" value="{{ $item->week }}" tabindex="2" min="0" required>
        </div>
        <div class="form-group">
            <label for="max_limit" class="bmd-label-floating">@Lang('Limit')</label>
            <input type="number" class="form-control" name="max_limit" name="max_limit" value="{{ $item->max_limit }}" tabindex="3" min="0" required>
        </div>
        <div class="form-group mt-5">
            <input onclick="window.location.href='{{ route('admin.calendario.subniveles.edit', $item->calendario_subnivel_id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
            <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
        </div>
    </form>

</div>
@endsection