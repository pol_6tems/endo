<?php

// Auth Routes
Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    
    // AdminController
    Route::get('/', "Admin\AdminController@index")->name('admin');
    
    Route::group(['prefix' => 'calendario-vacaciones'], function () {
        include('routes/admin/calendario-vacaciones.php');
    });
    
    Route::group(['prefix' => 'administracion'], function () {
        include('routes/admin/calendario-vacaciones-administracion.php');
    });
    
    Route::get('users/exportar', "Admin\UsersController@exportar")->name('admin.users.exportar');

    // Mensajes
    Route::resource('mensajes', "Admin\MensajesController", ['as' => 'admin']);

    // Bústia de suggeriments
    Route::resource('suggeriments', "Admin\SuggerimentsController", ['as' => 'admin']);

    // Resultats
    Route::resource('resultats', "Admin\ResultatsController", ['as' => 'admin']);

});

include('routes/calendario-vacaciones-no_auth.php');

Route::middleware('auth')->group(function () {
    include('routes/calendario-vacaciones-auth.php');
});