<?php

namespace App\Modules\Ratings\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Ratings\Models\Rating;
use App\Modules\Ratings\Models\RatingResult;
use App\Post;

class RatingsController extends Controller
{

    public static function show_ratings($post_id) {
        $ratings = self::get_ratings($post_id);
        $post = Post::find($post_id);
        $post_type = !empty($post) ? $post->type : 'post';
        
        return [
            'view' => 'ratings.single-grid',
            'params' => [
                'ratings' => $ratings,
                'post_id' => $post_id,
                'post_type' => $post_type,
            ]
        ];
    }

    public static function show_ratings_avg($post_id) {
        $ratings = self::get_ratings($post_id);
        
        return [
            'view' => 'ratings.avg',
            'params' => [
                'ratings' => $ratings,
                'post_id' => $post_id,
            ]
        ];
    }

    public static function get_ratings($post_id) {
        $post = Post::find($post_id);
        $ratings = Rating::where('post_type', $post->type)->withAll()->get();
        $num_ratings = RatingResult::where('post_id', $post_id)->select('user_id')->groupby('user_id')->pluck('user_id')->toArray();
        
        $max = !empty($ratings[0]) ? $ratings[0]->maximo : 0;
        $full = !empty($ratings[0]) ? $ratings[0]->icono_full->get_thumbnail_url('thumbnail') : '';
        $empty = !empty($ratings[0]) ? $ratings[0]->icono_empty->get_thumbnail_url('thumbnail') : '';
        
        return [
            'avg' => self::get_ratings_avg_value($ratings, $post_id),
            'num' => count($num_ratings),
            'max' => $max,
            'full' => $full,
            'empty' => $empty,
            'children' => $ratings,
            'post_id' => $post_id,
        ];
    }

    protected static function get_ratings_avg_value($ratings, $post_id) {
        $value = 0;
        $count = count($ratings);

        foreach ($ratings as $r) {
            $value += $r->get_avg_value($post_id);
        }
        if ( $value > 0 && $count > 0 ) $value = ceil($value / $count);

        return $value;
    }

    public function store(Request $request) {
        $data = request()->all();
        
        if ( !empty($data['user_id']) && !empty($data['rating_id']) && !empty($data['post_id']) ) {
            $ratingResult = RatingResult::firstOrNew([
                'user_id' => $data['user_id'],
                'rating_id' => $data['rating_id'],
                'post_id' => $data['post_id']
            ]);

            $ratingResult->value = $data['value'];

            $ratingResult->save();

            event('store_rating', ['user_id' => $data['user_id'], 'post_id' => $data['post_id']]);
        }
        return response()->json(['success' => true, 'rating' => $ratingResult]);
    }

    public static function get_num_ratings_by_user_and_post($user_id, $post_id) {
        $num = 0;

        if ( !empty($user_id) && !empty($post_id) ) {
            $num = RatingResult::where('user_id', $user_id)->where('post_id', $post_id)->count();
        }
        return $num;
    }

    public static function ratings_order_avg($posts, $order = 'ASC') {
        $posts = $posts->sort(function ($a, $b) use ($order) {
            $val_a = 0;
            $ratings = self::get_ratings($a->id);
            $val_a = !empty($ratings['avg']) ? $ratings['avg'] : 0;

            $val_b = 0;
            $ratings = self::get_ratings($b->id);
            $val_b = !empty($ratings['avg']) ? $ratings['avg'] : 0;

            if ($val_a == $val_b) return 0;

            if ( $order == 'ASC' ) return ($val_a < $val_b) ? -1 : 1;
            else return ($val_a > $val_b) ? -1 : 1;
        });
        return $posts;
    }

    public static function get_ratings_by_user_and_post($user_id, $post_id) {
        return RatingResult::where('user_id', $user_id)->where('post_id', $post_id)->get();
    }

}
