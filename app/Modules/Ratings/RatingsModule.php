<?php

namespace App\Modules\Ratings;

use App\Support\Module;

class RatingsModule extends Module {

    const MODULE_NAME = 'Ratings';
    const MODULE_ROUTE_ADMIN = 'admin.ratings';
    const MODULE_ROUTE = 'ratings';
    
}