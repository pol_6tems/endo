<?php

namespace App\Modules\Ratings\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Ratings\Models\Rating;
use App\User;

class RatingResult extends Model {

    protected $table = 'ratings_results';
    protected $fillable = ['rating_id', 'user_id', 'post_id', 'value'];

    public function rating() {
        return $this->belongsTo(Rating::class, 'rating_id');
    }
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
