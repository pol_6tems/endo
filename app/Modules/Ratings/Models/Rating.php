<?php

namespace App\Modules\Ratings\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Media;
use App\Modules\Ratings\Models\RatingResult;

class Rating extends Model {

    protected $fillable = ['name', 'post_type', 'params', 'maximo', 'icono_full_id', 'icono_empty_id'];

    public function scopeWithAll($query) {
        $query->with('ratings', 'icono_full', 'icono_empty');
    }

    public function icono_full() {
        return $this->belongsTo(Media::class, 'icono_full_id');
    }

    public function icono_empty() {
        return $this->belongsTo(Media::class, 'icono_empty_id');
    }

    public function ratings() {
        return $this->hasMany(RatingResult::class, 'rating_id', 'id')->orderBy('value');
    }

    public function get_ratings($post_id) {
        return RatingResult::where('rating_id', $this->id)->where('post_id', $post_id)->orderBy('value')->get();
    }

    public function get_params() {
        return !empty($this->params) ? json_decode($this->params, TRUE) : array();
    }

    public function get_param($key) {
        $params = $this->get_params();
        return !empty($params[$key]) ? $params[$key] : null;
    }

    public function get_avg_value($post_id = null) {
        $value = $count = 0;
        $ratings_results = $this->get_ratings($post_id);
        if ( count($ratings_results) > 0 ) {
            foreach ( $ratings_results as $r ) {
                $value += $r->value;
                $count++;
            }
        }
        
        if ( $value > 0 && $count > 0 ) $value = ceil($value / $count);

        return $value;
    }

}
