<?php

use App\Modules\Ratings\Controllers\RatingsController;

function show_ratings($post_id) {
    return RatingsController::show_ratings($post_id);
}

function get_ratings($post_id) {
    return RatingsController::get_ratings($post_id);
}

function show_ratings_avg($post_id) {
    return RatingsController::show_ratings_avg($post_id);
}

function get_num_ratings_by_user_and_post($user_id, $post_id) {
    return RatingsController::get_num_ratings_by_user_and_post($user_id, $post_id);
}

function get_ratings_by_user_and_post($user_id, $post_id) {
    return RatingsController::get_ratings_by_user_and_post($user_id, $post_id);
}

function ratings_order_avg($posts, $order = 'ASC') {
    return RatingsController::ratings_order_avg($posts, $order);
}
/*
use App\Modules\Ratings\Repositories\RatingsRepository;

define("RATINGS_REPOSITORY_COOLDOWN", 1);

function show_ratings($post_id) {
    return cacheable(RatingsRepository::class, RATINGS_REPOSITORY_COOLDOWN)->show_ratings($post_id);
}

function get_ratings($post_id) {
    return cacheable(RatingsRepository::class, RATINGS_REPOSITORY_COOLDOWN)->get_ratings($post_id);
}

function show_ratings_avg($post_id) {
    return cacheable(RatingsRepository::class, RATINGS_REPOSITORY_COOLDOWN)->show_ratings_avg($post_id);
}

function get_num_ratings_by_user_and_post($user_id, $post_id) {
    return app(RatingsRepository::class)->get_num_ratings_by_user_and_post($user_id, $post_id);
}

function get_ratings_by_user_and_post($user_id, $post_id) {
    return app(RatingsRepository::class)->get_ratings_by_user_and_post($user_id, $post_id);
}

function ratings_order_avg($posts, $order = 'ASC') {
    return cacheable(RatingsRepository::class, RATINGS_REPOSITORY_COOLDOWN)->ratings_order_avg($posts, $order);
}
*/