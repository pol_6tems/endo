<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('ratings') ) {
            Schema::create('ratings', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('post_type');
                $table->text('params')->nullable();
                $table->integer('icono_full_id')->unsigned()->default(0);
                $table->integer('icono_empty_id')->unsigned()->default(0);
                $table->integer('maximo')->unsigned()->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
