<?php

Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    Route::put("ratings/{id}/duplicate", "AdminRatingsController@duplicate")->name("admin.ratings.duplicate");
    Route::resource('ratings', "AdminRatingsController", ['as' => 'admin']);
});

Route::resource('ratings', "RatingsController");