<div id="cf{{ $custom_field->id }}" data-id="{{ $custom_field->id }}" class="col-lg-12 m-auto custom-field number-field">
    <div class="programa">
        <div class="toolbar">
            <ul class="programa-tabs nav nav-pills no-margin" role="tablist">
                @if ($value)
                    @php ($i=1)
                    @foreach($value->dies as $dia => $activitats)
                        @if ($activitats)
                        <li class="nav-item links">
                            <a href="#dies{{ $i }}" data-toggle="tab" class="nav-link {{ ($i == 1) ? 'active show' : '' }}" role="tablist">Dia {{ $i }}</a>
                        </li>
                        @php($i++)
                        @endif
                    @endforeach
                @else
                    <li class="nav-item links">
                        <a href="#dies1" data-toggle="tab" class="nav-link">Dia 1</a>
                    </li>
                @endif
            </ul>
        </div>
        
        <div class="tab-content">
            <div>
                <ul>
                    <li>Define cada día los horarios y actividades programadas para ayudar al participante a hacerse una idea de tu retiro.</li>
                    <li>Una vez creado un día tipo, puedes duplicarlo a los siguientes días</li>
                </ul>
            </div>
            @if ($value)
                @php ($i=1)
                @foreach($value->dies as $activitats)
                    @if ($activitats)
                    <div id="dies{{ $i }}" data-dia="{{ $i }}" class="pestanya tab-pane fade in {{ ($i == 1) ? 'active show' : '' }}" role="tabpanel">
                        <select class="selectpicker">
                            @php($k = 1)
                            @foreach( $value->dies as $dia )
                                <option value="dies{{ $k }}">Dia {{ $k }}</option>
                            @php($k++)
                            @endforeach
                        </select>
                        <button class="duplicate_field btn btn-md btn-info"><i class="material-icons">file_copy</i></button>
                        <br><br>
                        <div class="activitats">
                        @foreach($activitats as $nAct => $activitat)
                            <div class="added_{{ $i }} input-group mb-1">
                                <div class="input-group-prepend">
                                    <input 
                                            type="text"
                                            class="clockpicker hora"
                                            name="custom_fields[{{ $language_item->code }}][{{ $custom_field->id }}][dies][{{ $i }}][{{ $nAct }}][hora]"
                                            value="{{ $activitat->hora }}"
                                            required />
                                </div>

                                <input
                                        type="text"
                                        class="form-control act"
                                        id="basic-url"
                                        data-order="{{ $nAct }}"
                                        name="custom_fields[{{ $language_item->code }}][{{ $custom_field->id }}][dies][{{ $i }}][{{ $nAct }}][valor]"
                                        value="{{ $activitat->valor }}"
                                        required />
                    
                                <button class="remove_field btn btn-sm btn-danger" style="padding: 0 15px;">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </div>
                        @endforeach
                        </div>
                        <hr>
                        <div class="adding-input input-group mb-3" style="justify-content: flex-end;">
                            <button data-field="programa" data-dia="{{ $i }}" class="add_field btn btn-primary btn-sm">@Lang('Add')</button>
                        </div>
                    </div>
                    @php($i++)
                    @endif
                @endforeach
            @else
                <div id="dies1" class="pestanya tab-pane fade in">
                    <select class="selectpicker">
                        <option value="dies1">Dia 1</option>
                    </select>
                    <button class="duplicate_field btn btn-md btn-info"><i class="material-icons">file_copy</i></button>
                    <br><br>
                    <div class="activitats"></div>
                    <hr>
                    <div class="adding-input input-group mb-3" style="justify-content: flex-end;">
                        <button data-field="programa" data-dia="1" class="add_field btn btn-primary btn-sm">@Lang('Add')</button>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

@section("scripts-cf$custom_field->id")
    <script>
        var name = 'custom_fields[{{ $language_item->code }}][{{ $custom_field->id }}][dies]';
    </script>
    <link href="{{asset('Modules/EncuentraTuRetiro/css/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <script src="{{asset('Modules/EncuentraTuRetiro/js/disponibilitat.js') }}"></script>
    <script src="{{asset('Modules/EncuentraTuRetiro/js/jquery-clockpicker.min.js') }}"></script>
    <script>let workerFile = "{{asset('Modules/EncuentraTuRetiro/js/renderPrograma.js') }}";</script>
    <script>
        $(function () {
            $('.clockpicker').clockpicker({
                placement: 'top',
                align: 'left',
                autoclose: true
            });
        });

        $(document).on('click', '.remove_day', eliminarDia);
        $(document).on('click', '.remove_field', function(e) {
            e.preventDefault();
            var input = $(this).parent().find('input');
            var $pestanya = $(this).closest('.pestanya');
            var parent = $(this).closest('.bmd-form-group');

            var order = parent.find('input.act').data('order');
            parent.remove();
        });
        $(document).on('click', '.add_field', afegirActivitat);
        $(document).on('keypress', '.activitat', enterKey);
    </script>
@endsection
