@section('styles')
<link href="{{asset($_admin.'js/summernote/summernote.css')}}" rel="stylesheet">
<link href="{{asset($_admin.'js/summernote/plugin/image-list/summernote-image-list.min.css')}}" rel="stylesheet">
<link href="{{asset('Modules/EncuentraTuRetiro/css/bootstrap-year-calendar.min.css')}}" rel="stylesheet">
<link href="{{asset('Modules/EncuentraTuRetiro/css/calendari.css')}}" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{asset($_admin.'js/summernote/plugin/codemirror/codemirror.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset($_admin.'js/summernote/plugin/codemirror/monokai.css')}}">
@endsection

<div id="cf{{ $custom_field->id }}" data-id="{{ $custom_field->id }}" class="col-lg-12 m-auto custom-field number-field">
    <label class="col-md-12 mt-4">
        {{ __($title) }}
        @if (Auth::user()->role == 'admin')
            (<button class="copy">{{ $custom_field->name }}</button>)
        @endif
    </label>
    <div style="border: 2px solid var(--grn);padding: 20px;">
        <!-- Sel·leccio de Tipus de Reserva -->
        <div class="field-group calendari" style="margin-bottom">
            <div>
                <ul>
                    <li><strong>EVENTO ÚNICO:</strong> Retiro que sólo tiene una fecha programada</li>
                    <li><strong>EVENTO REPETITIVO:</strong> El mismo Retiro con varias fechas programadas que no se solapan</li>
                    <li><strong>SERIE DE DÍAS FIJO:</strong> El Retiro en que el participante decide la fecha de inicio</li>
                </ul>
            </div>
            <label class="control-label col-md-12">@lang('Tipo de Fechas')</label>
            <select class="selectpicker tipus" name="custom_fields[{{ $language_item->code }}][{{ $custom_field->id }}][tipus]" data-style="btn btn-primary">
                <optgroup label="Tipus">
                    <option value="1" {{ (isset($value) && $value != '' && $value->tipus == "1") ? 'selected' : '' }}>Evento Único</option>
                    <option value="2" {{ (isset($value) && $value != '' && $value->tipus == "2") ? 'selected' : '' }}>Evento Repetitivo</option>
                    <option value="3" {{ (isset($value) && $value != '' && $value->tipus == "3") ? 'selected' : '' }}>Série de dias fijo</option>
                    {{--<option value="4" {{ (isset($value) && $value != '' && $value->tipus == "4") ? 'selected' : '' }}>Série de dias variable</option>--}}
                </optgroup>
            </select>
        </div>
        <!-- Fi Sel·leccio de Tipus de Reserva -->

        <!-- Fi Sel·leccio de Tipus de Reserva -->
        <div class="field-group dias">
            <label class="control-label">Días del retiro (incluyendo el día de llegada y el día de salida)</label>
            <div class="input-group suffix">
                <!-- Duración del retiro -->
                <input
                    type="number"
                    class="form-control duracion"
                    value="{{ (isset($value->duracion) && $value != '') ? $value->duracion : '1' }}"
                    min="1"
                    max="30"
                    step="1"
                    name="custom_fields[{{ $language_item->code }}][{{ $custom_field->id }}][duracion]"
                    placeholder="Duración del retiro"
                    onhover="(function (e) {e.preventDefault();})(event);">
                <span class="input-group-addon" style="border: 1px solid #d8d8d8;background: #f6f6f6;">días</span>
                
                <!-- Selector del calendario -->
                <input
                    type="hidden"
                    class="calendar_selector"
                    value="{{ (isset($value->selector) && $value != '') ? $value->selector : '' }}"
                    name="custom_fields[{{ $language_item->code }}][{{ $custom_field->id }}][selector]">
            </div>
        </div>

        <!-- Calendari -->
        <div class="field-group availablilty">
            <div class="llegenda" style="padding: 30px 0;">
                <button class="btn btn-sm btn-warning" data-role="clear">Limpia el Calendario</button>
            </div>

            <div>
                <ul>
                    <li>Si es EVENTO ÚNICO: Selecciona las fechas exactas del Retiro</li>
                    <li>Si es EVENTO REPETITIVO: Selecciona todas la fechas en que se hará el Retiro</li>
                    <li>Si es SERIE DE DÍAS FIJO: Selecciona todas las Fechas de Inicio posibles para el Retiro</li>
                </ul>
            </div>
            @if (isset($value->calendari))
                @php( $calendari_var = str_replace("\\", "", trim(stripslashes(htmlspecialchars(json_encode($value->calendari), ENT_QUOTES, 'UTF-8')), "&quot;")))
            @endif
            <input id="calendar_input" type="hidden" name="custom_fields[{{ $language_item->code }}][{{ $custom_field->id }}][calendari]" value="@if (isset($calendari_var)){!! $calendari_var !!}@endif"/>
            <div class="calendar_{{ $language_item->code }}"></div>
        </div>
        <!-- Fi Calendari -->
    </div>
</div>

@section("scripts-cf$custom_field->id")
    <script src="{{asset('Modules/EncuentraTuRetiro/js/bootstrap-year-calendar.min.js') }}"></script>
    <script src="{{asset('Modules/EncuentraTuRetiro/js/bootstrap-year-calendar.es.js') }}"></script>
    <script src="{{asset('Modules/EncuentraTuRetiro/js/calendari.js') }}"></script>
    <script>
    var tipus = "{{ (isset($value) && $value != '') ? $value->tipus : '1'}}",
        field_type = 'custom_fields[{{ $language_item->code }}][{{ $custom_field->id }}]',
        cal_dies = @if (isset($calendari_var) && $value != '') {!! str_replace("&quot;", "\"", $calendari_var) !!} @else [] @endif,
        lastAdded = [],
        duracio = '{{ (isset($value->duracion) && $value != '') ? $value->duracion : '' }}';
    </script>
    <script>
        @foreach($_languages as $language_item)
            calendari('.calendar_{{ $language_item->code }}', cal_dies, tipus);
        @endforeach
    </script>
@endsection