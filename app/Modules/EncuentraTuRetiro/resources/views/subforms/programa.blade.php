<div class="programa">
    <div class="toolbar">
        <ul class="nav nav-pills no-margin">
            <li class="nav-item links">
                <a href="#dies1" data-toggle="tab" class="nav-link">Dia 1</a>
            </li>
        </ul>
    </div>
      
    <div class="tab-content">
        <div id="dies1" class="pestanya tab-pane fade in">
            <div class="adding-input input-group mb-3">
                <div class="input-group-prepend">
                    <input
                        type="text"
                        class="data form-control timepicker"
                        placeholder="08:00 AM"
                        data-locale="es"
                        data-provides="anomaly.field_type.datetime"
                        value="08:00 AM" />
                </div>
                
                <input
                    type="text"
                    class="activitat form-control"
                    placeholder="Introduce una actividad"
                    data-dia="1" />

                <button data-field="programa" class="add_field btn btn-sm btn-info">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    </div>
</div>
