<!-- Sel·leccio de Tipus de Reserva -->
<div class="field-group calendari">
    <label class="control-label">Tipus de Retiro</label>
    <select class="selectpicker tipus" name="disponibilitat" data-style="btn btn-primary btn-round">
        <optgroup label="Tipus">
            <option value="1">Evento Único</option>
            <option value="2">Evento Repetitivo</option>
            <option value="3">Série de dias fijo</option>
            <option value="4">Série de dias variable</option>
        </optgroup>
    </select>
</div>
<!-- Fi Sel·leccio de Tipus de Reserva -->

<!-- Fi Sel·leccio de Tipus de Reserva -->
<div class="field-group">
    <label class="control-label">Duración del retiro</label>
    <div class="input-group suffix">
        <!-- Duración del retiro -->
        @php($duracio = '' == '' ? '15' : '1')
        <input
            type="number"
            class="form-control duracion"
            value="{{ $duracio }}"
            min="1"
            step="1"
            name="disponibilitat_duracion"
            placeholder="Duración del retiro"
            onhover="(function (e) {e.preventDefault();})(event);">
        <span class="input-group-addon ">días</span>
        
        <!-- Selector del calendario -->
        <input
            type="hidden"
            class="calendar_selector"
            value="{{ $duracio }}"
            name="disponibilitat_selector">
    </div>
</div>

<!-- Calendari -->
<div class="field-group availablilty">
    <div class="llegenda">
        <button class="btn btn-sm btn-warning" data-role="clear">Clear Calendar</button>
    </div>
    <div class="calendar_{{ $language_item->code }}"></div>
</div>
<!-- Fi Calendari -->