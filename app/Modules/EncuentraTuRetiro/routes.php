<?php
/**
 * Aquestes rutes corresponen al Namespace \App\Modules\EncuentraTuRetiro\Controllers
 */

Route::pattern('filter', '[a-z0-9-]+');
Route::pattern('location', '[a-z0-9-]+');

Route::get('retiros/l/{filter}/{location?}', 'RetirosController@landings')->name('retiros.landing')->middleware('old-landings');
Route::get('retiros/l/{filters}', 'RetirosController@landings')->name('retiros.old-landing')->middleware('old-landings');
Route::get('retiro-espiritual', 'RetirosController@landings')->name('retiros.old-archive-url')->middleware('old-landings');
Route::get('retiro-espiritual/retiros-{filter}-en-{location}', 'RetirosController@landings')->name('retiros.old-landing2')->middleware('old-landings');
Route::get('retiro-espiritual/retiros-{filters}', 'RetirosController@landings')->name('retiros.old-landing3')->middleware('old-landings');

Route::get('retiros/{post_name}/detalles', 'RetirosController@detalles_get');
Route::post('retiros/{post_name}/detalles', 'RetirosController@detalles')->name('retiros.detalles');
Route::post('retiros/{post_name}/detalles-registro', 'RetirosController@detallesRegister')->name('retiros.detalles.register');
Route::post('retiros/{post_name}/detalles-ajax-registro', 'RetirosController@detallesAjaxRegister')->name('retiros.detalles.ajax-register');

/** Redsys notify **/
Route::any('retiros/{post_name}/noti', 'RetirosController@notification')->name('retiros.comprar.noti');
Route::any('regalo-retiro/comprar/{kit}/noti', 'RegalosController@notification')->name('regalos.comprar.noti');


Route::group(['middleware' => ['auth']], function () {
    /** Redsys redirects **/
    Route::any('retiros/{post_name}/ok', 'RetirosController@formOK')->name('retiros.comprar.ok');
    Route::get('retiros/{post_name}/ko', 'RetirosController@formKO')->name('retiros.comprar.ko');
    Route::get('retiros/{post_name}/pdf', 'RetirosController@pdf')->name('retiros.comprar.pdf');

    Route::post('retiros/{post_name}/compra_realizada', 'RetirosController@comprar')->name('retiros.comprar');
    Route::get('retiros/{post_name}/reservado', 'RetirosController@reservado')->name('retiros.reservado');
    Route::get('retiros/{post_name}/valora', 'RetirosController@valora')->name('retiros.valora');
    Route::post('retiros/{post_name}/valora', 'RetirosController@postValora');

    Route::get('regalo-retiro/comprar/{kit}', 'RegalosController@comprar')->name('regalos.comprar');
    Route::post('regalo-retiro/comprar/{kit}', 'RegalosController@postComprar');

    Route::get('regalo-retiro/comprado/{kit}', 'RegalosController@comprado')->name('regalos.comprado');

    /** Redsys redirects **/
    Route::get('regalo-retiro/comprar/{kit}/ok', 'RegalosController@formOK')->name('regalos.comprar.ok');
    Route::get('regalo-retiro/comprar/{kit}/ko', 'RegalosController@formKO')->name('regalos.comprar.ko');
});

Route::post('register', 'Auth\MyRegisterController@store');
Route::post('login', 'Auth\MyLoginController@login');

Route::post('buscador', 'BuscadorController@retiros')->name('buscador');
Route::get('filters', 'BuscadorController@get_filters')->name('filters');

Route::get('perfil/favoritos','UserController@favoritos')->name('perfil.favoritos');

// Newsletter
Route::post('newsletter', 'NewsletterController@subscribe')->name('newsletter.subscribe');

Route::group(['middleware' => ['locale', 'rols', 'auth']], function () {
    Route::get('perfil','UserController@index')->name('perfil.show');
    Route::get('perfil/mensajes','UserController@mensajes')->name('perfil.mensajes');
    Route::get('perfil/comunidad','UserController@comunidad')->name('perfil.comunidad');
    Route::get('perfil/dharmas','UserController@dharmas')->name('perfil.dharmas');
    Route::get('perfil/reservas','UserController@reservas')->name('perfil.reservas');
    Route::get('perfil/registra-organizador', 'UserController@registerOrganizer')->name('perfil.register-organizer');
    Route::post('perfil/registra-organizador', 'UserController@postRegisterOrganizer');

    Route::post('perfil','UserController@update')->name('perfil.update');
    Route::post('mensaje','UserController@mensaje_store')->name('perfil.mensaje.store');

    Route::post('reservation/change-status', 'RetirosController@changeReservationStatus')->name('retiros.change-status');
});

Route::group(['middleware' => ['rols', 'auth', 'locale'], 'prefix' => 'admin'], function () {
    
    // Configuración
    Route::get('configuracion', "Admin\MyConfiguracionController@index")->name('admin.configuracion.index');
    Route::post('configuracion', "Admin\MyConfiguracionController@store")->name('admin.configuracion.store');
    Route::get('configuracions', "Admin\MyConfiguracionController@index")->name('admin.configuracions.index');
    Route::post('configuracions', "Admin\MyConfiguracionController@store")->name('admin.configuracions.store');

    // Users
    Route::resource('users', 'Admin\MyUsersController', ['as' => 'admin']);
    Route::resource('retiros', 'RetirosController', ['as' => 'admin']);
    Route::resource("posts", "MyPostsController", ["as" => "admin"]);
    Route::put("posts/{id}/aprove_pending", "MyPostsController@aprove_pending")->name("admin.posts.aprove_pending");
    Route::resource("organizadores", "OrganizadoresController", ["as" => "admin"]);

    Route::post('media/get_all', "Admin\MyMediaController@get_all")->name('admin.media.get_all');

    Route::resource('reservas', 'Admin\ReservationsController', ["as" => "admin"]);
    Route::put("reservas/{id}/restore", "Admin\ReservationsController@restore")->name("admin.reservas.restore");

    Route::resource('compra-regalos', 'Admin\RegalosController', ["as" => "admin"]);
    Route::put("compra-regalos/{id}/restore", "Admin\ReservationsController@restore")->name("admin.compra-regalos.restore");

    Route::resource('reviews', 'Admin\ReviewsController', ["as" => "admin"]);

    Route::resource('comments', "Admin\MyCommentsController", ['as' => 'admin']);
});

// Route::fallback('MyPostsController@process_routes');