<?php

namespace App\Modules\EncuentraTuRetiro\Observers;


use App\Modules\EncuentraTuRetiro\Models\Purchase;
use App\Post;

class ReservationObserver
{

    public function creating(Purchase $reservation)
    {
        $this->setOrderId($reservation);
    }


    public function saving(Purchase $reservation)
    {
        $this->setOrderId($reservation);
    }


    public function updating(Purchase $reservation)
    {
        $this->setOrderId($reservation);
    }


    public function saved($model)
    {
        /*$retiro = Post::find($model->retiro_id);
        $organizador = $retiro->get_field('organizador');

        Mensaje::create([
            'user_id' => $model->user_id,
            'from' => $organizador->id,
            'mensaje' => 'Un Cliente Ha hecho una reserva en tu retiro',
            'fitxer' => '',
            'visto' => 0,
        ]);*/
    }


    private function setOrderId(Purchase $reservation)
    {
        if (!isset($reservation->order_id) || !$reservation->order_id) {
            $type = Post::where('id', $reservation->post_id)->first()->type;

            if ($type == 'retiro') {
                $type = 'reserva';
            }

            $letter = get_letra_from_type($type);

            do {
                $orderId = $letter . mt_rand(100000, 999999);
                $free = !Purchase::where('order_id', $orderId)->exists();
            } while (!$free);

            $reservation->order_id = $orderId;
        }
    }
}