<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reservations')) {
            Schema::rename('reservations', 'purchases');

            Schema::table('purchases', function (Blueprint $table) {
                $table->dropUnique('reservation_unique_order');

                $table->renameColumn('retiro_id', 'post_id');

                $table->unique(['external_order_id', 'user_id', 'post_id'], 'reservation_unique_order');

                $table->bigInteger('habitacio_id')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('purchases')) {
            Schema::table('purchases', function (Blueprint $table) {
                $table->dropUnique('reservation_unique_order');

                $table->renameColumn('post_id', 'retiro_id');

                $table->unique(['external_order_id', 'user_id', 'retiro_id'], 'reservation_unique_order');
            });

            Schema::rename('purchases', 'reservations');
        }
    }
}
