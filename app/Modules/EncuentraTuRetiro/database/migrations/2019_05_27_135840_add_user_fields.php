<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('users', 'dharmas') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('dharmas')->default(0);
            });
        }

        if ( !Schema::hasColumn('users', 'city') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('city')->nullable();
            });
        }

        if ( !Schema::hasColumn('users', 'country') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('country')->nullable();
            });
        }

        if ( !Schema::hasColumn('users', 'gender') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('gender')->nullable();
            });
        }

        if ( !Schema::hasColumn('users', 'birthdate') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('birthdate')->nullable();
            });
        }

        if ( !Schema::hasColumn('users', 'token') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->longText('token')->nullable();
            });
        }

        if ( !Schema::hasColumn('users', 'status') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('status')->default('pending');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('users', 'dharmas') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('dharmas');
            });
        }

        if ( Schema::hasColumn('users', 'city') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('city');
            });
        }
        
        if ( Schema::hasColumn('users', 'country') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('country');
            });
        }

        if ( Schema::hasColumn('users', 'gender') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('gender');
            });
        }

        if ( Schema::hasColumn('users', 'birthdate') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('birthdate');
            });
        }

        if ( Schema::hasColumn('users', 'token') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('token');
            });
        }

        if ( Schema::hasColumn('users', 'status') ) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('status');
            });
        }
    }
}
