<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderIdToReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('reservations', 'order_id')) {
            Schema::table('reservations', function (Blueprint $table) {
                $table->string('order_id', 16)->nullable()->unique()->after('id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('reservations', 'order_id')) {
            Schema::table('reservations', function (Blueprint $table) {
                $table->dropColumn('order_id');
            });
        }
    }
}
