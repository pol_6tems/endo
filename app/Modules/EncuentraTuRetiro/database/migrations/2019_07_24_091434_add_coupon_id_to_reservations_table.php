<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCouponIdToReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('reservations', 'coupon_id')) {
            Schema::table('reservations', function (Blueprint $table) {
                $table->integer('coupon_id')->unsigned()->nullable()->after('habitacio_id');
                $table->integer('coupon_discount_amount')->nullable()->after('coupon_id');

                $table->index('coupon_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('reservations', 'coupon_id')) {
            Schema::table('reservations', function (Blueprint $table) {
                $table->dropColumn('coupon_id');
            });
        }
    }
}
