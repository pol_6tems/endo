<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProvinceNameShortnameField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('locations', 'provincia') ) {
            Schema::table('locations', function (Blueprint $table) {
                $table->string('provincia')->nullable();
            });
        }

        if ( !Schema::hasColumn('locations', 'code_provincia') ) {
            Schema::table('locations', function (Blueprint $table) {
                $table->string('code_provincia')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('locations', 'provincia') ) {
            Schema::table('locations', function (Blueprint $table) {
                $table->dropColumn('provincia');
            });
        }

        if ( Schema::hasColumn('locations', 'code_provincia') ) {
            Schema::table('locations', function (Blueprint $table) {
                $table->dropColumn('code_provincia');
            });
        }
    }
}
