<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('reservations')) {
            Schema::create('reservations', function (Blueprint $table) {
                $table->increments('id');
                $table->string('status');
                $table->bigInteger('user_id');
                $table->bigInteger('retiro_id');
                $table->bigInteger('habitacio_id');
                $table->longText('mensaje');
                $table->longText('acompanantes');
                $table->longText('cancelacion');
                $table->double('total');
                $table->double('payed');
                $table->double('left');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
