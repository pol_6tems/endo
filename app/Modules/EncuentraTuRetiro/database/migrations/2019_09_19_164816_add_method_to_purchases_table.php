<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMethodToPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('purchases', 'method')) {
            Schema::table('purchases', function (Blueprint $table) {
                $table->string('method')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('purchases', 'method')) {
            Schema::table('purchases', function (Blueprint $table) {
                $table->dropColumn('method');
            });
        }
    }
}
