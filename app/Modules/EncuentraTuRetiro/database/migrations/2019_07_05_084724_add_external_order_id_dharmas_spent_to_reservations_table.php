<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExternalOrderIdDharmasSpentToReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('reservations', 'external_order_id')) {
            Schema::table('reservations', function (Blueprint $table) {
                $table->string('external_order_id')->unique()->after('status');
                $table->integer('dharmas_spent')->after('left');
                $table->longText('extra_data')->nullable()->after('dharmas_spent');

                $table->unique(['external_order_id', 'user_id', 'retiro_id'], 'reservation_unique_order');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('reservations', 'external_order_id')) {
            Schema::table('reservations', function (Blueprint $table) {
                $table->dropUnique('reservation_unique_order');

                $table->dropColumn('extra_data');
                $table->dropColumn('dharmas_spent');
                $table->dropColumn('external_order_id');
            });
        }
    }
}
