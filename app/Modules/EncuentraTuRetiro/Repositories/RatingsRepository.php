<?php


namespace App\Modules\EncuentraTuRetiro\Repositories;


use App\Modules\Ratings\Models\Rating;
use App\Modules\Ratings\Models\RatingResult;
use Illuminate\Support\Facades\DB;

class RatingsRepository
{

    public function getRatingsByType($postType)
    {
        return $ratings = Rating::where('post_type', $postType)->withAll()->get();
    }


    public function getRatingResultsByAuthor($authorId, $postType)
    {
        return RatingResult::select(DB::raw('distinct ratings_results.*'))
            ->join('posts', 'posts.id', '=', 'ratings_results.post_id')
            ->where('posts.author_id', $authorId)
            ->where('posts.type', $postType)
            ->get();
    }
}