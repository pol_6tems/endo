<?php


namespace App\Modules\EncuentraTuRetiro\Repositories;


use App\Modules\EncuentraTuRetiro\Models\PopularSearch;

class PopularSearchesRepository
{
    
    public function addPopularSearch($post_id)
    {
        $popularSearch = PopularSearch::firstOrNew([
            'post_id' => $post_id
        ]);

        if (isset($popularSearch->times)) {
            $popularSearch->times++;
        } else {
            $popularSearch->times = 1;
        }

        $popularSearch->save();
    }


    public function getPopularSearches($limit = 10)
    {
        return PopularSearch::with(['post.translations', 'post.customPostTranslations'])
            ->orderBy('times', 'desc')->limit($limit)->get();
    }
}