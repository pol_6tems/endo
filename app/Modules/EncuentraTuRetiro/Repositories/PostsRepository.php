<?php


namespace App\Modules\EncuentraTuRetiro\Repositories;


use App\Modules\EncuentraTuRetiro\Models\Favourite;
use App\Modules\EncuentraTuRetiro\Models\Location;
use App\Post;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

class PostsRepository
{

    public function getMonedas()
    {
        return Post::withTranslation()
            ->with('metas.customField')
            ->where('type', 'moneda')
            ->where('status', 'publish')
            ->orderBy('order', 'asc')
            ->get();
    }
    
    
    public function getAllActiveRetiros()
    {
        $retiros = Post::where('type', 'retiro')->where('status', 'publish')->get();

        return $this->filterRetirosByDates($retiros);
    }


    public function getSearchTypes()
    {
        return Post::withTranslation()
            ->with(['customPostTranslations'])
            ->whereIn('type', [
                'tipos-retiro',
                'retiro-categoria',
                'beneficio',
                'tipos-de-yoga',
                'tipo-de-meditacion',
            ])->where('status', 'publish')
            ->get();
    }


    public function userFavorites($userId)
    {
        return Favourite::where('user_id', $userId)->get();
    }


    public function getMaxPrice(Collection $retiros, $filtered = false)
    {
        $key = 'GetMaxPrice_' . md5($retiros->map(function ($retiro) {
            return $retiro->id;
        })->implode('_'));

        if (Cache::has($key)) {
            return unserialize(Cache::get($key));
        }

        $max = 0;

        if (!$filtered) {
            $retiros = filterRetirosByDates($retiros);
        }

        foreach($retiros as $item) {
            $oferta = obtenirOfertaBarata($item);
            $max = ($oferta && $oferta['precio']['value'] > $max) ? $oferta['precio']['value'] : $max;
        }

        Cache::put($key, serialize($max), 60);

        return $max;
    }


    public function filterRetirosByDates(Collection $retiros)
    {
        $key = 'FilterRetirosByDate_' . md5($retiros->map(function ($retiro) {
            return $retiro->id;
        })->implode('_'));

        if (Cache::has($key)) {
            $ids = unserialize(Cache::get($key));

            $retiros = $retiros->whereIn('id', $ids);
        } else {
            $retiros = filterRetirosByDates($retiros);

            $ids = $retiros->map(function ($retiro) {
                return $retiro->id;
            });

            Cache::put($key, serialize($ids), 60);
        }

        return $retiros;
    }


    public function ordenarPosts(Collection &$posts, $order, $rating = false)
    {
        $key = 'OrdenarPosts' . md5($posts->map(function ($retiro) {
                return $retiro->id;
            })->implode('_')) . '_' . $order . '_' . $rating;

        $user = auth()->user();

        if (Cache::has($key) && (!$user || !$user->isAdmin())) {
            $ids = unserialize(Cache::get($key));

            if (!is_array($ids)) {
                $ids = $ids->values()->all();
            }

            $posts = $posts->sortBy(function ($post) use ($ids) {
                return array_search($post->id, $ids);
            });
        } else {
            ordenar_posts($posts, $order, $rating);

            $ids = $posts->map(function ($post) {
                return $post->id;
            })->values()->all();

            if (!$user || !$user->isAdmin()) {
                Cache::put($key, serialize($ids), 60);
            }
        }
    }


    public function get_active_countries()
    {
        $countries = get_active_countries();

        $countries = $countries->filter(function ($element, $key) {
            $alojamientos = Location::where('code_pais', $key)->pluck('alojamiento_id');
            foreach ($alojamientos as $alojamiento) {
                $posts = get_posts([
                    'post_type' => 'retiro',
                    'metas' => [
                        ['alojamiento', $alojamiento],
                    ],
                ]);

                $posts = $this->filterRetirosByDates($posts);
                if (!$posts->isEmpty()) return true;
            }

            return false;
        });

        return $countries;
    }
}