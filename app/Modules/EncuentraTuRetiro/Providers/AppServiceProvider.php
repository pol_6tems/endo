<?php


namespace App\Modules\EncuentraTuRetiro\Providers;


use App\Modules\EncuentraTuRetiro\Middleware\OldLandings;
use App\Modules\EncuentraTuRetiro\Models\Purchase;
use App\Modules\EncuentraTuRetiro\Observers\ReservationObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        Purchase::observe(ReservationObserver::class);

        $router = $this->app['router'];
        $router->aliasMiddleware('old-landings', OldLandings::class);
    }


    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {

    }
}