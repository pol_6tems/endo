<?php


namespace App\Modules\EncuentraTuRetiro\Providers;


use App\Modules\EncuentraTuRetiro\Composers\ReviewsAvgRatingComposer;
use App\Modules\EncuentraTuRetiro\Composers\ReviewsComposer;
use Illuminate\Support\ServiceProvider;
use App\Modules\EncuentraTuRetiro\Composers\FooterComposer;
use App\Modules\EncuentraTuRetiro\Composers\HeaderComposer;
use App\Modules\EncuentraTuRetiro\Composers\BuscadorComposer;
use App\Modules\EncuentraTuRetiro\Composers\CarouselEquipoComposer;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('Front::partials.header', HeaderComposer::class);
        view()->composer('Front::partials.footer', FooterComposer::class);
        view()->composer('Front::partials.buscador', BuscadorComposer::class);
        view()->composer('Front::partials.carousel-equipo', CarouselEquipoComposer::class);
        view()->composer('Front::partials.reviews', ReviewsComposer::class);
        view()->composer('Front::partials.reviews', ReviewsComposer::class);
        view()->composer('Front::partials.reviews-avg-rating', ReviewsAvgRatingComposer::class);
    }


    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {

    }
}