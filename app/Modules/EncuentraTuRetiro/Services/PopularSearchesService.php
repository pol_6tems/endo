<?php


namespace App\Modules\EncuentraTuRetiro\Services;


use App\Modules\EncuentraTuRetiro\Repositories\PopularSearchesRepository;
use BotDetector;

class PopularSearchesService
{

    public function addPopularSearches($metas)
    {
        if (!BotDetector::isBot()) {
            foreach ($metas as $meta) {
                if (count($meta) == 3 && $meta[0] != 'duracion-retiros') {
                    $post_id = array_last($meta);

                    $post_id = filter_var($post_id, FILTER_SANITIZE_NUMBER_INT);

                    if ($post_id && is_numeric($post_id)) {
                        app(PopularSearchesRepository::class)->addPopularSearch($post_id);
                    }
                }
            }
        }
    }
    
    
    public function getPopularSearches($limit = 4)
    {
        $popularSearches = cacheable(PopularSearchesRepository::class, 10)->getPopularSearches($limit);

        return $popularSearches->pluck('post');
    }
}