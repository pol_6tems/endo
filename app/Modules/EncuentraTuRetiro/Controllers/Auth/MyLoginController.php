<?php

namespace App\Modules\EncuentraTuRetiro\Controllers\Auth;

use Illuminate\Support\Facades\App;
use App\Http\Controllers\Auth\LoginController;


class MyLoginController extends LoginController
{
    public function redirectTo()
    {
        if (request('after_login')) {
            return request('after_login');
        }

        return url()->previous();
    }
}
