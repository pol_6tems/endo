<?php

namespace App\Modules\EncuentraTuRetiro\Controllers\Auth;

use App\Modules\EncuentraTuRetiro\Models\Favourite;
use App\Modules\EncuentraTuRetiro\Requests\MyRegisterRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\RegisterController;

class MyRegisterController extends RegisterController
{
    /**
     * Create a new user instance after a valid registration.
     *
     * @param $locale
     * @param MyRegisterRequest $request
     * @return \App\User
     */
    public function store($locale, MyRegisterRequest $request)
    {
        try {
            $data = request()->all();

            $userData = [
                'name' => $data['nombre'],
                'lastname' => isset($data['apellido']) ? $data['apellido'] : '',
                'email' => $data['correo'],
                'password' => bcrypt($data['contrasena']),
                'role' => 'user',
                'dharmas' => 0,
                'status' => 'pending',
            ];
    
            $user = User::create($userData);

            event('add_dharmas', [$user, 300, 'Alta Usuari@']);
    
            if (Auth::loginUsingId($user->id)) {
                try {
                    $key = 'etr_favorites';

                    $postIds = collect(json_decode(request()->cookie($key, json_encode([])), true));

                    foreach ($postIds as $postId) {
                        Favourite::create([
                            'user_id' => $user->id,
                            'post_id' => $postId,
                            'type' => 'retiro',
                        ]);
                    }
                } catch (\Exception $e) {
                    // Do nothing, but don't break..
                }

                if (array_key_exists('organizer', $data) && $data['organizer']) {
                    return redirect(route('perfil.register-organizer'));
                }

                if (array_key_exists('after_register', $data)) {
                    return redirect($data['after_register']);
                }

                return redirect()->back()->with('success', __('Registered Completed'));
            }
        } catch (\Exception $e) {
            return redirect(route('index'))->with('error', __('Error on register'));
        }

        return redirect(route('index'))->with('error', __('Error on register'));
    }
}
