<?php

namespace App\Modules\EncuentraTuRetiro\Controllers;

use App\Models\PostMeta;
use App\Post;
use App\User;
use App\Models\CustomPost;
use Illuminate\Http\Request;
use App\Models\Configuracion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Admin\PostsController;

class MyPostsController extends PostsController
{
    const POST_TYPES_WITH_APPROVE = ['retiro', 'blog-post', 'espacio', 'voluntariado'];

    protected $post_type = 'post';
    protected $post_type_plural = 'posts';
    protected $section_route = 'admin.posts';
    protected $def_status = 'publish';
    protected $subforms = [];
    
    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home_page = Configuracion::get_config('home_page');
        $home_page_id = !empty($home_page) ? $home_page->id : 0;

        $status = $this->def_status;

        // PENDINGS
        $pendings = Post::where('type', $this->post_type)->where('status', 'pending')->withTranslation();
        if ( !Auth::user()->isAdmin() ) {
            $pendings = $pendings->where('author_id', Auth::id())->get();
        } else {
            $pendings = $pendings->get();
        }
        
        // DRAFT
        $drafts = Post::where('type', $this->post_type)->where('status', 'draft')->withTranslation();
        if ( !Auth::user()->isAdmin() ) {
            $drafts = $drafts->where('author_id', Auth::id())->get();
        } else {
            $drafts = $drafts->get();
        }

        if ( !empty(request()->status) && request()->status == 'trash' ) {
            $items = Post::onlyTrashed()->orderby('order')->where('type', $this->post_type);
            if ( !Auth::user()->isAdmin() ) {
                $items = $items->where('author_id', Auth::id())->get();
            } else {
                $items = $items->get();
            }
            $status = 'trash';
        } else if ( !empty(request()->status) && request()->status == 'pending' ) {
            $items = $pendings;
            $status = 'pending';
        } else if ( !empty(request()->status) && request()->status == 'draft' ) {
            $items = $drafts;
            $status = 'draft';
        } else {
            $builder = Post::where('type', $this->post_type)
                ->where('status', '<>', 'pending')
                ->where('status', '<>', 'draft')
                ->orderby('order');
            if ( !Auth::user()->isAdmin() ) $builder->where('author_id', Auth::id());

            if ( !empty(request()->parent)) {
                $parent_id = request()->parent;
                $builder->where('parent_id', $parent_id);
            }

            $items = $builder->withTranslation()->with(['parent', 'customPostTranslations.customPost.translations'])->get();
        }

        $num_trashed = Post::onlyTrashed()->where('type', $this->post_type)->where('author_id', Auth::id())->count();
        $num_pending = $pendings->count();
        $num_draft = $drafts->count();

        if ($this->post_type == 'alojamiento-habitacion') {
            view()->share('post_type_title', 'Habitaciones');
        }

        $order_col = 6;
        $order_dir = 'desc';

        if ($this->post_type == 'page') {
            $order_col = 0;
            $order_dir = 'asc';
        }

        if (!empty(request()->status) && request()->status == 'trash' && $this->post_type != 'page') {
            $order_col = $order_col - 1;
        }

        return view('Front::admin.index', compact('items', 'status', 'num_trashed', 'num_pending', 'num_draft', 'home_page_id', 'order_col', 'order_dir'));
    }

    public function create()
    {
        $view = parent::create();
        $view_name = "Front::admin.create";
        $authors = User::where('role', 'Organizador')->get();

        $can_aprove_pending = auth()->user()->rol->level >= Post::ROL_LEVEL_LIMIT_REVISIO;

        if (!$can_aprove_pending && in_array($this->post_type, self::POST_TYPES_WITH_APPROVE)) {
            $cf_groups = collect($view->getData()['cf_groups']);

            foreach ($cf_groups as $cf_group) {
                if ($cf_group->fields->where('name', 'destacado')->first()) {
                    $cf_group->setRelation('fields', $cf_group->fields->filter(function ($field) {
                        return $field->name != 'destacado';
                    }));
                }

                if ($this->post_type == 'retiro' && $cf_group->fields->where('name', 'video-youtube')->first()) {
                    $cf_group->setRelation('fields', $cf_group->fields->filter(function ($field) {
                        return $field->name != 'video-youtube';
                    }));
                }
            }

            return view($view_name, array_merge($view->getData(), ['save_disclaimer' => 'Al seleccionar ENVIAR PARA PUBLICAR el ' . $this->post_type_title . ' queda pendiente de revisión para su publicación lo antes posible', 'users' => $authors, 'cf_groups' => $cf_groups]));
        }

        if ($this->post_type == 'page') {
            view()->share('post_type_title', __('Page'));
        }

        return view($view_name, array_merge($view->getData(), ['users' => $authors]));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $custom_fields = array();
        if ( !empty($data['custom_fields']) ) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code]) ) {
                $sanitize_title = str_slug($data[$lang->code]['title'], '-');
                $aux = $sanitize_title;
                $i = 1;

                while (Post::whereTranslation('post_name', $aux, $lang->code)->exists()) {
                    $aux = $sanitize_title . '-' . $i;
                    $i++;
                }
                $sanitize_title = $aux;
                $data[$lang->code]['post_name'] = $sanitize_title;
                $data[$lang->code]['media_id'] = ( !empty($data['media_id']) ) ? $data['media_id'] : null;
            }

            if ($this->post_type == 'retiro') {
                foreach($custom_fields[$lang->code] as $key => $cf) {
                    if ($key == 296) {
                        $calendari = $cf['calendari'];

                        if (!is_array($calendari)) {
                            $calendari = json_decode($calendari);
                        }

                        $custom_fields[$lang->code][$key]['calendari'] = $calendari;
                    }
                }
            }
        }

        $data['author_id'] = Auth::id();
        $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : 0;

        // Check if is revision
        if ( $data['status'] != 'draft' 
            && Auth::user()->rol->level < Post::ROL_LEVEL_LIMIT_REVISIO 
            && in_array($this->post_type, self::POST_TYPES_WITH_APPROVE)
        ) {
            $data['status'] = 'pending';
        }

        $post = Post::create($data);

        /* Custom Fields */
        foreach ($custom_fields as $lang => $fields) {
            $this->save_custom_fields($post, $fields, $lang);
        }
        /* end Custom Fields */

        // Hook
        execute_actions('save_post', $post);

        return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $this->post_type)->with(['message' => __(ucfirst($this->post_type) . ' added successfully')]);
    }


    public function edit($locale, $id)
    {
        $view = parent::edit($locale, $id);
        $view_name = "Front::admin.edit";
        if (get_class($view) == 'Illuminate\View\View') {
            $post = $view->getData();
            $post = $post['post'];

            if ($this->post_type != $post->type) {
                return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $post->type);
            }

            if (!Auth::user()->isAdmin() && isset($post->author) && $post->author->id != Auth::id()) return view('Admin::errors.403');
        } else if (get_class($view) == 'Illuminate\Http\RedirectResponse') {
            return $view;
        }

        $can_aprove_pending = auth()->user()->rol->level >= Post::ROL_LEVEL_LIMIT_REVISIO;

        $authors = User::where('role', 'Organizador')->get();

        if (!$can_aprove_pending && in_array($this->post_type, self::POST_TYPES_WITH_APPROVE)) {
            $cf_groups = collect($view->getData()['cf_groups']);

            foreach ($cf_groups as $cf_group) {
                if ($cf_group->fields->where('name', 'destacado')->first()) {
                    $cf_group->setRelation('fields', $cf_group->fields->filter(function ($field) {
                        return $field->name != 'destacado';
                    }));
                }

                if ($this->post_type == 'retiro' && $cf_group->fields->where('name', 'video-youtube')->first()) {
                    $cf_group->setRelation('fields', $cf_group->fields->filter(function ($field) {
                        return $field->name != 'video-youtube';
                    }));
                }
            }

            return view($view_name, array_merge($view->getData(), ['save_disclaimer' => 'Al seleccionar ENVIAR PARA PUBLICAR el retiro queda pendiente de revisión para su publicación lo antes posible', 'users' => $authors, 'cf_groups' => $cf_groups]));
        }

        if ($this->post_type == 'page') {
            view()->share('post_type_title', __('Page'));
        }

        return view($view_name, array_merge($view->getData(), ['users' => $authors]));
    }

    public function show($lang, $id) {
		return redirect(route('admin.posts.edit', $id) . '?post_type=' . $this->post_type);
	}

    public function update(Request $request, $locale, $id)
    {
        $post = Post::findOrFail($id);
        $data = $request->all();

        $post = $this->updatePost($post, $data);

        return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $this->post_type)->with(['message' => __('Post updated successfully')]);
    }


    public function aprove_pending($locale, $id) {
        $post = Post::find($id);
        if ( empty($post) ) return redirect(route($this->section_route . '.index') . '?post_type=' . $this->post_type)->with([ 'message' => __(":name doesn't exists.", ['name' => __('Post')]) ]);
        $post = $this->updatePost($post, request()->all());

        $post_parent = Post::find($post->pending_id);
        if ( !empty($post_parent) ) {
            $post_parent_id = $post_parent->id;
            $post_parent->forceDelete();
            PostMeta::where('post_id', $post_parent_id)->delete();

            DB::statement("SET FOREIGN_KEY_CHECKS=0"); // to disable them
            // POSTS META
            DB::statement("
                UPDATE post_meta SET
                    post_id = " . $post_parent_id . "
                WHERE post_id = " . $post->id . "
            ");

            // POSTS TRANSLATIONS
            DB::statement("
                UPDATE post_translations SET
                    post_id = " . $post_parent_id . "
                WHERE post_id = " . $post->id . "
            ");

            // POSTS
            DB::statement("
                UPDATE posts SET
                    id = " . $post_parent_id . ",
                    pending_id = 0,
                    status = 'publish'
                WHERE id = " . $post->id . "
            ");
            DB::statement("SET FOREIGN_KEY_CHECKS=1"); // to enable them
        } else {
            if (isset($post->numero)) unset($post->numero);
            $post->status = 'publish';
            $post->pending_id = 0;
            $post->save();
        }

        return redirect(route($this->section_route . '.index') . '?post_type=' . $this->post_type)->with([ 'message' => __(":name aproved successfully.", ['name' => __('Post')]) ]);
    }

    


    public function process_routes($locale) {
        $params = collect(request()->segments());
        $params->forget(0); // Treure primer segment (lang)
        $data = [];
        
        return $this->renderView($params, $data);
    }


    protected function updatePost(Post $post, $data)
    {
        $custom_fields = array();
        if ( !empty($data['custom_fields']) ) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code]) ) {
                $sanitize_title = str_slug($data[$lang->code]['title'], '-');
                if ( !empty( $data[$lang->code]['post_name'] ) ) {
                    $sanitize_title = str_slug($data[$lang->code]['post_name'], '-');
                }
                $aux = $sanitize_title;
                $i = 1;
                while (Post::whereTranslation('post_name', $aux, app()->getLocale())->where('id', '<>', $post->id)->where('pending_id', '<>', $post->pending_id)->exists()) {
                    $aux = $sanitize_title . '-' . $i;
                    $i++;
                }
                $sanitize_title = $aux;
                $data[$lang->code]['post_name'] = $sanitize_title;
                $data[$lang->code]['post_name'] = str_replace('//', '/', $data[$lang->code]['post_name']);
                
                $data[$lang->code]['media_id'] = ( !empty($data['media_id']) ) ? $data['media_id'] : null;

                if ($this->post_type == 'retiro') {
                    foreach($custom_fields[$lang->code] as $key => $cf) {
                        if ($key == 296) {
                            $calendari = $cf['calendari'];

                            if (!is_array($calendari)) {
                                $calendari = json_decode($calendari);
                            }

                            $custom_fields[$lang->code][$key]['calendari'] = $calendari;
                        }
                    }
                }
            }
        }

        if ( empty($data['author_id']) ) $data['author_id'] = Auth::id();

        // Check if is revision
        if ( !in_array($post->status, array('pending', 'draft'))
            && Auth::user()->rol->level < Post::ROL_LEVEL_LIMIT_REVISIO
            && in_array($this->post_type, self::POST_TYPES_WITH_APPROVE)
        ) {
            $data['status'] = 'pending';
            $data['pending_id'] = $post->id;
            $post = Post::create($data);
        } else {
            $post->update($data);
        }

        /* Custom Fields */
        foreach ($custom_fields as $lang => $fields) {
            $this->save_custom_fields($post, $fields, $lang);
        }
        
        // Hook
        execute_actions('save_post', $post);

        return $post;
    }
    
    
    /**
     * Funció que retorna la vista corresponent segons els parametres d'entrada.
     * Només necessitem saber el primer element (Distingir si és un CP Plural (Archive) | Post Name (Single) | Page Name (Template))
     *
     * @param Array $params
     * @param array $data
     * @return View
     */
    private function renderView($params, $data = []) {
        $routes = ['Admin::errors.404'];
        $first = $params->shift();
        
        // Is Archive
        if ($cp = CustomPost::ofPlural($first)->first()) {
            $last = $params->last(); // Valid Post name o no
            
            $item = Post::with(['metas.customField', 'translations.media'])
                        ->ofType($cp->post_type)
                        ->publish();
            
            $post = clone $item;
            $post = $post->ofName($last)->first();

            if ( $last && !is_null($post) ) {
                // Single
                $item   = $post;
                $trans  = $post->getTranslation();
                $routes = [ "Front::posts.single-{$item->type}", "Front::posts.single" ];
                $data   = array_merge($data, [ 'item' => $item, 'trans' => $trans ]);
            } else {
                // Archive
                $routes = [ "Front::posts.archive-{$cp->post_type}", "Front::posts.archive"];
                $data   = array_merge($data, [ 'items' => $item->get() ]);
            }
        }

        if ( !isset($post) && ($item = Post::ofName($first)->ofType('page')->first())) {
            // Page
            $trans  = $item->getTranslation();
            $routes = [ "Front::posts.templates.{$item->template}", 'Front::posts.page' ];
            $data   = array_merge($data, [ 'item' => $item, 'page' => $item, 'trans' => $trans ]);
        }

        return view()->first($routes, $data);
    }

    private function processURLString($urlString) {
        return trim($this->unparse_url(parse_url($urlString)), '/');
    }

    private function unparse_url($parsed_url) { 
        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : ''; 
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : ''; 
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : ''; 
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : ''; 
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : ''; 
        $pass     = ($user || $pass) ? "$pass@" : ''; 
        $path     = isset($parsed_url['path']) ? str_replace("//", "/", $parsed_url['path']) : ''; 
        $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : ''; 
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : ''; 
        return "$scheme$user$pass$host$port$path$query$fragment"; 
    }
}
