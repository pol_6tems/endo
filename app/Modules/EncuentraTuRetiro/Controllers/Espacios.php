<?php

namespace App\Modules\EncuentraTuRetiro\Controllers;

use App\Http\Controllers\Controller;
use App\Post;

class Espacios extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {
        $post_type = 'espacio';
        $posts = Post::where(['type' => $post_type])->get();
        
        return View('Front::espacios.espacios', [
            'espacios' => $posts
        ]);
    }

    public function show($locale, $id)
    {
        $post = Post::where(['id' => $id])->first();
        
        if (!$post) return redirect(route('espacios'))->with(['message' => __('Post doesn\'t exists')]);

        return View('Front::espacios.fitxa', [
            'post' => $post
        ]);
    }

    public function list($locale) {
        $post_type = 'espacio';
        $posts = Post::where(['type' => $post_type])->paginate(10);

        return View('Front::espacios.llista', [
            'espacios' => $posts,
        ]);
    }
}
