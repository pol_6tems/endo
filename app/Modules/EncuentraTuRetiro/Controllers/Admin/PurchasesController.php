<?php


namespace App\Modules\EncuentraTuRetiro\Controllers\Admin;


use App\Post;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\AdminController;
use App\Modules\EncuentraTuRetiro\Models\Purchase;

class PurchasesController extends AdminController
{
    
    protected $post_type;
    protected $post_type_plural;
    protected $section_title;
    protected $section_route;
    protected $purchase_item;
    protected $admin_view_path;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $query = Purchase::select(DB::raw('purchases.*'))
            ->join('posts', 'posts.id', '=', 'purchases.post_id')
            ->where('posts.type', $this->purchase_item)
            ->orderBy('updated_at', 'DESC')
            ->with('post.author', 'user');


        $trashedQuery = clone $query;

        $trashed = $trashedQuery->onlyTrashed()->get();

        if (request('status') == 'trash') {
            $purchases = $trashed;
        } else {
            $purchases = $query->get();
        }

        $user = auth()->user();

        if ($user->rol->level < Post::ROL_LEVEL_LIMIT_REVISIO) {
            $purchases = $purchases->filter(function ($purchase) use ($user) {
                return $purchase->post->author_id == $user->id;
            });

            $trashed = $trashed->filter(function ($purchase) use ($user) {
                return $purchase->post->author_id == $user->id;
            });
        }

        return view($this->admin_view_path . 'index', [
            'items' => $purchases,
            'num_trashed' => $trashed->count()
        ]);
    }


    public function create()
    {
        $purchase = new Purchase();

        return view($this->admin_view_path . 'edit', [
            'statuses' => $purchase->present()->statuses
        ]);
    }


    public function edit($locale, $id)
    {
        $purchase = Purchase::withTrashed()->with('post.author', 'user')->findOrFail($id);

        return view($this->admin_view_path . 'edit', [
            'item' => $purchase,
            'statuses' => $purchase->present()->statuses
        ]);
    }


    public function update($locale, $id)
    {
        $reservation = Purchase::withTrashed()->with('post.author', 'user')->findOrFail($id);

        if ($reservation->status == Purchase::RESERVATION_STATUS_CONFIRMED) {
            abort(400);
        }

        $reservation->update([
            'status' => request('status', Purchase::RESERVATION_STATUS_PENDING)
        ]);

        return redirect(route($this->section_route  . '.edit', ['id' => $reservation->id]))->with(['message' => __(':post_type updated successfully', ['post_type' => $this->post_type])]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $reservation = Purchase::findOrFail($id);
        $reservation->delete();

        return redirect(route($this->section_route  . '.index'))->with(['message' => __(':post_type sent to trash', ['post_type' => $this->post_type])]);
    }


    public function restore($locale, $id)
    {
        $reservation = Purchase::withTrashed()->find($id);
        $reservation->restore();

        return redirect(route($this->section_route  . '.index'))->with(['message' => __(':post_type restored successfully.', ['post_type' => $this->post_type])]);
    }


    public function callAction($method, $parameters)
    {
        view()->share([
            'section_title' => $this->section_title,
            'post_type_plural' => $this->post_type_plural,
            'section_route' => $this->section_route
        ]);

        return parent::callAction($method, $parameters);
    }
}