<?php


namespace App\Modules\EncuentraTuRetiro\Controllers\Admin;


use App\Modules\Comments\Controllers\AdminCommentsController;
use App\Modules\Comments\Models\Comment;
use App\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MyCommentsController extends AdminCommentsController
{

    protected function get_form_fields() {
        $users = User::get();
        $posts = Post::where('status', 'publish')->whereIn('type', ['retiro', 'voluntariado', 'espacio'])->withTranslation()->get();

        return array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'post_id', 'title' => __('Post'), 'type' => 'select', 'required' => true,
                    'datasource' => $posts,
                    'sel_value' => 'id',
                    'sel_title' => ['title'],
                    'sel_search' => true,
                    'with_post_type' => true,
                ],
                ['value' => 'user_id', 'title' => __('User'), 'type' => 'select', 'required' => true,
                    'datasource' => $users,
                    'sel_value' => 'id',
                    'sel_title' => ['fullname()'],
                    'sel_search' => true,
                    'with_post_type' => false,
                ],
                ['value' => 'created_at', 'title' => __('Date'), 'type' => 'date'],
                ['value' => 'comment', 'title' => __('Comment'), 'type' => 'textarea', 'rows' => 10],
            ],
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();

        $data['created_at'] = isset($data['created_at']) ? Carbon::createFromFormat('d/m/Y', $data['created_at']) : Carbon::now();
        Comment::create($data);
        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name added successfully', ['name' => __('Comment')]) ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id) {
        $item = Comment::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');

        $data = $request->all();
        $data['created_at'] = Carbon::createFromFormat('d/m/Y', $data['created_at']);
        $item->update($data);

        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name updated successfully', ['name' => __('Comment')]) ]);
    }
}