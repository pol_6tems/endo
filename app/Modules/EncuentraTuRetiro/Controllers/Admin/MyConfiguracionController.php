<?php

namespace App\Modules\EncuentraTuRetiro\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\ConfiguracionController;

class MyConfiguracionController extends ConfiguracionController
{
    protected $new_configs =  [
        'whatsapp_num' => [
            "title" => "Número Whatsapp",
            "name" => "whatsapp_num",
            "type" => "text",
        ],
        'whatsapp_text' => [
            "title" => "Text Whatsapp",
            "name" => "whatsapp_text",
            "type" => "text",
        ],
        'mail_reservas' => [
            "title" => "Email Reservas",
            "name" => "mail_reservas",
            "type" => "text",
        ]
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->configs['general'] = array_merge($this->configs['general'],  $this->new_configs);
        return parent::index();
    }

    public function store(Request $request) {
        $this->configs['general'] = array_merge($this->configs['general'],  $this->new_configs);
        return parent::store($request);
    }
}
