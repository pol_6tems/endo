<?php


namespace App\Modules\EncuentraTuRetiro\Controllers\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Models\Traits\Presentable;
use App\Modules\EncuentraTuRetiro\Models\Review;
use App\Post;
use App\User;
use Illuminate\Http\Request;

class ReviewsController extends AdminController
{
    use Presentable;

    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Reviews');
        $this->section_icon = 'chat_bubble';
        $this->section_route = 'admin.reviews';
    }


    public function index()
    {
        $items = Review::all();

        return view('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'has_duplicate' => false,
            'headers' => [
                __('#') => [ 'width' => '5%' ],
                __('User') => [ 'width' => '15%' ],
                __('Retiro') => [ 'width' => '20%' ],
                __('Comment') => [],
                __('Status') => ['width' => '10%'],
                __('Updated at') => [ 'width' => '15%' ],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => ['purchase', 'user', 'fullname()']],
                ['value' => ['purchase', 'post', 'title'] ],
                ['value' => 'comment', 'class' => 'textarea'],
                ['value' => 'present()->reviewStatus'],
                ['value' => 'updated_at', 'type' => 'date'],
            ],
            'order_col' => 5,
            'order_dir' => 'desc'
        ));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return redirect()->back()->with('error', 'Error');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name added successfully', ['name' => __('Comment')]) ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id) {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id) {
        $item = Review::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');

        $retiro = $item->purchase->post;
        $ratings = get_ratings($retiro->id);

        return view('Front::admin.reviews.edit', [
            'item' => $item,
            'retiro' => $retiro,
            'ratings' => $ratings,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'section_route' => $this->section_route,
            'statuses' => $item->present()->statuses
        ]);
    }


    public function update($locale, $id)
    {
        $review = Review::findOrFail($id);

        $review->update([
            'status' => request('status', Review::STATUS_PENDING)
        ]);

        return redirect(route($this->section_route  . '.edit', ['id' => $review->id]))->with(['message' => __(':post_type updated successfully', ['post_type' => __('Review')])]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $review = Review::findOrFail($id);
        $review->delete();

        return redirect(route($this->section_route  . '.index'))->with(['message' => __(':post_type deleted', ['post_type' => __('Review')])]);
    }
}