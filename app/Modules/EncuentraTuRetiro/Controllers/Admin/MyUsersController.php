<?php

namespace App\Modules\EncuentraTuRetiro\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\UsersController;
use App\Modules\EncuentraTuRetiro\Models\Transaction;

class MyUsersController extends UsersController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = parent::index();

        return View('Front::admin.users.index', array_merge($view->getData(), [
            'order_col' => 4,
            'order_dir' => 'desc'
        ]));
    }

    public function edit($locale, User $user)
    {
        $view = parent::edit($locale, $user);
        $transactions = Transaction::where('user_id', $user->id)->ordered();

        $data = $view->getData();
        $data['transactions'] = $transactions;

        $data['rows'][] = ['value' => 'gender', 'title' => __('Género'), 'type' => 'select',
            'choices' => [
                ['title' => 'Hombre', 'value' => 'hombre'],
                ['title' => 'Mujer', 'value' => 'mujer'],
                ['title' => 'Otro', 'value' => 'otro'],
            ],
        ];

        $data['rows'][] = ['value' => 'country', 'title' => __('País')];
        $data['rows'][] = ['value' => 'city', 'title' => __('Localidad')];
        $data['rows'][] = ['value' => 'birthdate', 'title' => __('Fecha Nacimiento'), 'type' => 'date'];

        return View('Front::admin.users.edit', $data);
    }

    public function update(Request $request, $locale, User $user)
    {
        if (is_numeric($user)) {
            $user = User::find($user);
        }

        if (is_array($request->dharmas)) {
            $amount = $request->dharmas['value'];
            $text = $request->dharmas['text'];
            event('add_dharmas', [$user, (int)round($amount), $text]);

            return redirect()->route('admin.users.edit', ['id' => $user->id])->with('message',  __('User updated successfully'));
        }

        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->role = (isset($request->role)) ? $request->role : $user->rol->name;
        $user->gender = $request->gender;

        if ( !empty($request->password)) {
            if ($request->password != $request->password_confirmation) {
                return redirect()->back()->with('error', trans('validation.confirmed', ['attribute' => 'password']));
            }
            
            $user->password = bcrypt($request->password);
        }

        $user->update(); 

        if (isset($request->custom_fields)) {
            $this->save_custom_fields($user, $request->custom_fields);
        }

        // Hook
        execute_actions('save_user', $user);

        $permisos = $user->rol->get_permisos();

        if ($user->isAdmin() || (!empty($permisos['general']['adminuserscontroller']['index']) && $permisos['general']['adminuserscontroller']['index'])) {
            return redirect()->route('admin.users.index')->with('message',  __('User updated successfully'));
        } else {
            return redirect()->route('admin.users.edit', ['id' => $user->id])->with('message',  __('User updated successfully'));
        }
    }
}
