<?php


namespace App\Modules\EncuentraTuRetiro\Controllers\Admin;


class ReservationsController extends PurchasesController
{
    const POST_TYPE = 'reserva';
    const POST_TYPE_PLURAL = 'reservas';
    const SECTION_ROUTE = 'admin.reservas';
    const SECTION_TITLE = 'Reservas';

    
    public function callAction($method, $parameters)
    {
        $this->post_type = self::POST_TYPE;
        $this->post_type_plural = self::POST_TYPE_PLURAL;
        $this->section_route = self::SECTION_ROUTE;
        $this->section_title = self::SECTION_TITLE;

        $this->purchase_item = 'retiro';
        $this->admin_view_path = 'Front::admin.reservations-';

        return parent::callAction($method, $parameters);
    }
}