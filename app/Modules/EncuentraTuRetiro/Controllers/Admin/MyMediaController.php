<?php


namespace App\Modules\EncuentraTuRetiro\Controllers\Admin;


use App\Http\Controllers\Admin\MediaController;
use App\Models\Media;
use Illuminate\Http\Request;

class MyMediaController extends MediaController
{

    public function get_all(Request $request)
    {
        $user = auth()->user();

        $level = $user->rol->level ?: 0;

        $search = $request->input('search', '');
        $sortby = $request->input('sortby', 'updated_at');
        $order = $request->input('order', 'DESC');

        if (empty($sortby)) {
            $sortby = 'updated_at';
        }

        if (empty($order)) {
            $order = 'DESC';
        }

        $filter = $request->input('filter');

        $query = Media::where('level', '<=', $level)->orderby($sortby, $order);

        if (isset($filter['crop_height']) && isset($filter['crop_width'])) {
            $query = $query->where('height', $filter['crop_height'])->where('width', $filter['crop_width']);
        }

        if (!$user->isAdmin()) {
            $query = $query->where('user_id', $user->id);
        }

        if (empty($search)) {
            $items = $query->paginate(150);
        } else {
            $items = $query->where(function ($q) use ($search) {
                $q->orWhere('title', 'LIKE', '%' . strtoupper($search) . '%')
                    ->orWhere('legend', 'LIKE', '%' . strtoupper($search) . '%')
                    ->orWhere('description', 'LIKE', '%' . strtoupper($search) . '%')
                    ->orWhere('file_name', 'LIKE', '%' . strtoupper($search) . '%')
                    ->orWhere('id', 'LIKE', '%' . strtoupper($search) . '%');
            })->paginate(50);
        }

        $links_appends = array('sortby' => $sortby, 'order' => $order);

        if (!empty($search)) {
            $links_appends['search'] = $search;
        }

        $links = $items->appends($links_appends)->links();

        foreach ($items as $key => $item) {
            $items[$key]['thumbnails'] = $item->get_all_thumbnails();
            $items[$key]['isCroppable'] = $item->isCroppable() ? 1 : 0;
        }

        return response()->json([
            'items' => $items,
            'links' => $links,
            'search' => $search,
            'sortby' => $sortby,
            'order' => $order,
        ]);
    }
}
