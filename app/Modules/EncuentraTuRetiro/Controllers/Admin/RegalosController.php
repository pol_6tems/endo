<?php


namespace App\Modules\EncuentraTuRetiro\Controllers\Admin;


class RegalosController extends PurchasesController
{

    const POST_TYPE = 'regalo';
    const POST_TYPE_PLURAL = 'regalos';
    const SECTION_ROUTE = 'admin.compra-regalos';
    const SECTION_TITLE = 'Regalos';


    public function callAction($method, $parameters)
    {
        $this->post_type = self::POST_TYPE;
        $this->post_type_plural = self::POST_TYPE_PLURAL;
        $this->section_route = self::SECTION_ROUTE;
        $this->section_title = self::SECTION_TITLE;

        $this->purchase_item = 'retiro-regalo';
        $this->admin_view_path = 'Front::admin.regalos-';

        return parent::callAction($method, $parameters);
    }
}