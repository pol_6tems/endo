<?php

namespace App\Modules\EncuentraTuRetiro\Controllers;

use App\Modules\EncuentraTuRetiro\Services\PopularSearchesService;
use App\Post;
use stdClass;
use Carbon\Carbon;
use App\Models\CustomPost;
use App\Models\CustomFieldGroup;
use App\Modules\EncuentraTuRetiro\Models\Location;

class BuscadorController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function retiros()
    {
        $data = request()->all();

        $duracion = isset($data['duracion-y-llegada']) ? $data['duracion-y-llegada'] : null;
        $buscador = isset($data['buscador']) ? $data['buscador'] : null;
        $range = isset($data['range']) ? $data['range'] : null;
        $localization = isset($data['localization']) ? $data['localization'] : null;
        $capacidad = isset($data['capacidad']) ? $data['capacidad'] : null;

        $tipus = $data['post_type'] ;

        $cp = CustomPost::whereTranslation('post_type', $tipus)->first();
        
        $url = Post::get_archive_link($cp->post_type_plural);
        $customPosts = [];
        $params = [];
        
        if ($capacidad) {
            $post = Post::whereTranslation('post_name', $capacidad)->first();
            if ($post) $params[] = "filter[]=" . $post->post_name;
        }
        
        if ($duracion) {
            $durationParams = explode(' | ', $duracion);

            if (count($durationParams) > 1) {
                $date = $durationParams[1];
                $duracion = $durationParams[0];
            } else {
                $date = $duracion;
            }

            $processed = array_reverse(explode("/", $date));

            if (count($processed) == 3) {
                $processed[1] = sprintf('%02d', $processed[1]);
                $processed[2] = sprintf('%02d', $processed[2]);
                $processed = join('-', $processed);

                if ($date = Carbon::parse($processed)) {
                    $params[] = "date=$processed&range=$range";
                }
            }

            $durations = explode(', ', $duracion);

            foreach ($durations as $duration) {
                if ($post = Post::whereTranslation('title', $duration)->first()) {
                    $params[] = "filter[]=".$post->post_name;
                }
            }
        }

        if ($buscador) {
            $posts = Post::whereIn('id', $buscador)->get();
            if ($posts->isEmpty()) {
                foreach($buscador as $location) {
                    $aux = explode('-', $location);
                    if (count($aux) > 0) {
                        $place = $aux[0];
                        $id = $aux[1];

                        if ($place == 'pais') {
                            $location = Location::where('code_pais', $id)->first();
                            $customPosts[] = 'loc[]=' . $location->code_pais;
                        } else if ($place == 'region') {
                            $location = Location::where('code_region', $id)->first();
                            $customPosts[] = 'loc[]=' . $location->code_region;
                        } else if ($place == 'provincia') {
                            $location = Location::where('code_provincia', $id)->first();
                            $customPosts[] = 'loc[]=' . $location->code_provincia;
                        }
                    }
                }
            } else {
                foreach($posts as $post) {
                    $customPosts[] = 'filter[]=' . $post->post_name;
                }
            }
        }

        if ($localization) {
            $params[] = "coords=" . $localization['coordenadas'];
            
            if (is_json($localization['localidad'])) {
                $params[] = 'city=' . $localization['localidad'] . '&province=' . $localization['provincia'] . '&region=' . $localization['region'] . '&country=' . $localization['pais'];
                $params[] = "locInput=" . json_decode($localization['localidad'])->long_name . ', ' . json_decode($localization['pais'])->long_name;
            }
        }

        $params = join("&", $params);

        if (count($customPosts) > 0 && !empty($params)) {
            $params = '&' . $params;
        }

        if (count($customPosts) || !empty($params)) {
            $url .= '?' . join("&", $customPosts) . $params;
        }

        return redirect($url);
    }

    public function get_filters($parameters) {
        $query = request()->q;

        $countries = [];

        if ( empty($query) ) {
            foreach(get_active_countries() as $locations) {
                $countries = array_merge($countries, $this->process_locations($locations));
            }
        } else {
            $locations = Location::where('pais', 'like', "%$query%")
                                ->orwhere('region', 'like', "%$query%")
                                ->orwhere('provincia', 'like', "%$query%")
                                ->get();
            
            $countries = $this->process_locations($locations, $query);
        }

        $posts = collect([
            "Regiones" => array_values($countries),
            "Tipo Retiro" => Post::whereTranslationLike('title', "%$query%")->where('type', 'tipos-retiro')->get()->map(function($e) {
                $e->label = "Tipo";
                return $e;
            }),
            "Categoría Retiro" => Post::whereTranslationLike('title', "%$query%")->where('type', 'retiro-categoria')->get()->map(function($e) {
                $e->label = "Categoría";
                return $e;
            }),
            "Beneficio" => Post::whereTranslationLike('title', "%$query%")->where('type', 'beneficio')->get()->map(function($e) {
                $e->label = "Beneficio";
                return $e;
            }),
            "Tipo de Yoga" => Post::whereTranslationLike('title', "%$query%")->where('type', 'tipos-de-yoga')->get()->map(function($e) {
                $e->label = "Tipo de Yoga";
                return $e;
            }),
            "Tipo de Meditación" => Post::whereTranslationLike('title', "%$query%")->where('type', 'tipo-de-meditacion')->get()->map(function($e) {
                $e->label = "Tipo de Meditación";
                return $e;
            }),
        ]);
        
        if ( !empty($query) ) {
            $posts->merge(["Retiros" => Post::whereTranslationLike('title', "%$query%")->where('type', 'retiro')->get()->map(function($e) {
                $e->label = "Retiro";
                return $e;
            })]);
            $posts->merge(["Duración" => Post::whereTranslationLike('title', "%$query%")->where('type', 'retiro-duracion')->get()->map(function($e) {
                $e->label = "Duración";
                return $e;
            })]);

            $posts->merge(["Nivel" => Post::whereTranslationLike('title', "%$query%")->where('type', 'retiro-nivel')->get()->map(function($e) {
                $e->label = "Nivel";
                return $e;
            })]);
            $posts->merge(["Formato" => Post::whereTranslationLike('title', "%$query%")->where('type', 'retiro-formato')->get()->map(function($e) {
                $e->label = "Formato";
                return $e;
            })]);
            $posts->merge(["Público" => Post::whereTranslationLike('title', "%$query%")->where('type', 'retiro-publico')->get()->map(function($e) {
                $e->label = "Público";
                return $e;
            })]);
            $posts->merge(["Alimentación" => Post::whereTranslationLike('title', "%$query%")->where('type', 'retiro-alimentacion')->get()->map(function($e) {
                $e->label = "Alimentación";
                return $e;
            })]);
            $posts->merge(["Incluye" => Post::whereTranslationLike('title', "%$query%")->where('type', 'retiro-incluye')->get()->map(function($e) {
                $e->label = "Prestación";
                return $e;
            })]);
            $posts->merge(["Tipos de Habitación" => Post::whereTranslationLike('title', "%$query%")->where('type', 'retiro-tipo-habitaciones')->get()->map(function($e) {
                $e->label = "Tipo de Habitación";
                return $e;
            })]);
        }

        $popularSearches = app(PopularSearchesService::class)->getPopularSearches();

        return json_encode(['query' => $query, 'items' => $posts, 'popular' => $popularSearches]);
    }

    private function getFilteredPosts($parameters) {

        $data = request()->all();

        $buscador = $data['buscador']; 
        $cfsRetiro = CustomFieldGroup::get_cfs_by_post_type('retiro');

        $customPostsMetas = [];
        foreach($buscador as $key => $filtre) {
            $post = Post::find($filtre);

            $t_cp = $post->customPostTranslations;

            $t_cp = $t_cp->filter(function($value, $key) {
                return $value != app()->getLocale();
            })->first();

            $customPostsMetas[$filtre] = $t_cp->customPost;
        }

        $metas = [];
        // Per cada custom field dels retiros agafem els de tipus seleccio i 
        foreach($cfsRetiro as $cf) {
            if ( $cf->type != 'selection') continue;
            
            $params = json_decode($cf->params);
            $post_object = $params->post_object;

            foreach($customPostsMetas as $valor => $filtre) {
                if ($filtre->id == $post_object) {
                    $metas[] = [$cf->name, 'like', "%$valor%"];
                }
            }
        }

        return get_posts([
            'post_type' => 'retiro',
            'metas' => $metas
        ]);
    }


    function process_locations($locations, $query = null) {
        $countries = [];
        foreach($locations as $location) {
            $location = (is_array($location)) ? current($location) : $location;
            if (is_null($query) || like_match("%$query%", $location->pais)) {
                $pais = new stdClass();
                $pais->id = $location->id + "-1";
                $pais->label = 'Pais';
                $pais->type = "pais";
                $pais->title = $location->pais;
                $pais->post_name = $pais->code = $location->code_pais;
                if ($pais->title) $countries[$pais->code] = $pais;
            }

            if (is_null($query) || like_match("%$query%", $location->region)) {
                $regio = new stdClass();
                $regio->id = $location->id + "-2";
                $regio->label = 'Región';
                $regio->type = "region";
                $regio->title = $location->region;
                $regio->post_name = $regio->code = $location->code_region;
                if ($regio->title) $countries[$regio->code] = $regio;
            }

            if (is_null($query) || like_match("%$query%", $location->provincia)) {
                $provincia = new stdClass();
                $provincia->id = $location->id + "-3";
                $provincia->label = 'Provincia';
                $provincia->type = "provincia";
                $provincia->title = $location->provincia;
                $provincia->post_name = $provincia->code = $location->code_provincia;
                if ($provincia->title) $countries[$provincia->code] = $provincia;
            }
        }
        return $countries;
    }
}
