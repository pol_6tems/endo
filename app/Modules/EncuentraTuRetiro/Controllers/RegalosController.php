<?php


namespace App\Modules\EncuentraTuRetiro\Controllers;


use App\Http\Controllers\PostsController;
use App\Models\Mensaje;
use App\Modules\EncuentraTuRetiro\Models\Purchase;
use App\Notifications\EmailNotification;
use App\Post;
use App\User;
use Sermepa\Tpv\Tpv;
use stdClass;

class RegalosController extends PostsController
{


    public function comprar($locale, $kit)
    {
        $kit = $this->is_single($kit);
        
        if (!$kit) {
            abort(404);
        }

        $portes = $kit->get_field('portes');

        if (!$portes) {
            $portes = 0;
        }

        return view('Front::regalos.comprar', compact('kit', 'portes'));
    }
    
    
    public function postComprar($locale, $kit)
    {
        $kit = $this->is_single($kit);

        if (!$kit) {
            abort(404);
        }

        $portes = $kit->get_field('portes');

        if (!$portes) {
            $portes = 0;
        }
        
        $data = request()->all();

        $amount = $kit->get_field('precio') + $portes;

        $pan = null;
        $expiracion = null;
        $cvv = null;

        if (array_key_exists('tarjeta_credito', $data)) {
            $pan = str_replace(' ', '', $data['tarjeta_credito']);
        }

        if (array_key_exists('expiracion', $data) && $data['expiracion']) {
            $expiracion = str_replace(' ', '', $data['expiracion']);
            $expiracion = explode('/', $expiracion);
            $expiracion = implode('', [$expiracion[1], $expiracion[0]]);
        }

        if (array_key_exists('cvv', $data)) {
            $cvv = str_replace(' ', '', $data['cvv']);
        }

        //$key = 'HjXY/fdWsQWUFzKwU1OoXxD+N09lSBN2';
        $key = env('REDSYS_KEY', 't2o5TT3lOJfjvJeIFREaVRoTefc+AqCd');
        $env = env('REDSYS_ENV', 'test');

        try {
            $orderId = array_key_exists('external_order_id', $data) ? $data['external_order_id'] : time();
    
            $redsys = new Tpv();
            $redsys->setAmount($amount);
            $redsys->setOrder($orderId);
            $redsys->setMerchantcode('185006673');
    
            // No funciona
            $redsys->setCurrency(978);
    
            $redsys->setTransactiontype('0');
            $redsys->setTerminal('2');
            $redsys->setMethod('C'); // Solo pago con tarjeta, no mostramos iupay
    
            unset($data['tarjeta_credito']);
            unset($data['expiracion']);
            unset($data['cvv']);
            unset($data['_token']);
    
            $data['user_currency'] = userCurrency(true);
    
            $data['order_id'] = $orderId;
            $data['user_id'] = auth()->user()->id;
            $data['kit_id'] = $kit->id;
    
            $orderToken = endoEncrypt($data);
    
            $redsys->setNotification(route('regalos.comprar.noti', ['locale' => app()->getLocale(), 'kit' => $kit->post_name, 'orderToken' => $orderToken])); //Url de notificacion // Comprovació del Resultat
            $redsys->setUrlOk(route('regalos.comprar.ok', ['locale' => app()->getLocale(), 'kit' => $kit->post_name, 'orderToken' => $orderToken])); //Url OK
            $redsys->setUrlKo(route('regalos.comprar.ko', $kit->post_name)); //Url KO
    
    
            $redsys->setVersion('HMAC_SHA256_V1');
            $redsys->setTradeName('ENCUENTRA TU RETIRO');
            $redsys->setTitular('SERGI ARRIBAS TORRAS');
    
            // Redsys pago por web service
            if ($pan && $expiracion && $cvv) {
                $redsys->setPan($pan); //Número de la tarjeta
                $redsys->setExpiryDate($expiracion); //AAMM (año y mes)
                $redsys->setCVV2($cvv); //CVV2 de la tarjeta
            } // ELSE: Redsys pago por redirección
    
    
            $redsys->setEnvironment($env); //Entorno test
    
            $signature = $redsys->generateMerchantSignature($key); //
    
            $redsys->setMerchantSignature($signature); // 

            $redsys->executeRedirection();
        } catch (\Sermepa\Tpv\TpvException $e) {
           echo $e->getMessage();
        }
    }


    public function formOK($locale, $kit)
    {
        $data = $this->getOrderData();

        $kit = $this->is_single($kit);

        if (!$kit) {
            abort(404);
        }

        $user = auth()->user();

        if (!$data || $data['user_id'] != $user->id) {
            abort(403);
        }

        $purchase = $this->confirmRegalo($data, $user, $kit);

        return redirect()->route('regalos.comprado', [
            'locale' => $locale,
            'kit' => $kit->post_name,
            'order_id' => $purchase->order_id
        ]);
    }

    /**
     * TODO?
     *
     * @param $locale
     * @param $kit
     * @return \Illuminate\Http\RedirectResponse
     */
    public function formKO($locale, $kit)
    {
        $kit = $this->is_single($kit);
        return redirect($kit->get_url())->with('compra', false);
    }


    public function notification()
    {
        $data = $this->getOrderData();

        if ($data) {
            $this->confirmRegalo($data);
        }

        $response = response(json_encode(['success' => true, 'error' => false, 'message' => 'success']), 200, [
            'Content-Type' => 'application/json'
        ]);

        return $response;
    }


    public function comprado($locale, $kit)
    {
        $orderId = request('order_id');
        $externalOrderId = request('external_order_id');
        $kit = $this->is_single($kit);

        if ((!$orderId && !$externalOrderId) || !$kit) {
            abort(400);
        }

        $query = Purchase::where('user_id', auth()->user()->id)
            ->where('post_id', $kit->id);

        if ($orderId) {
            $query = $query->where('order_id', $orderId);
        } else {
            $query = $query->where('external_order_id', $externalOrderId);
        }

        $purchase = $query->first();

        if (!$purchase) {
            abort(400);
        }

        $portes = $kit->get_field('portes');

        if (!$portes) {
            $portes = 0;
        }

        return view('Front::regalos.comprado', compact('kit', 'portes', 'purchase'));
    }


    /**
     * @return mixed
     */
    private function getOrderData() {
        try {
            $redsys = new Tpv();
            //$key = 't2o5TT3lOJfjvJeIFREaVRoTefc+AqCd';
            $key = env('REDSYS_KEY');

            $parameters = $redsys->getMerchantParameters(request('Ds_MerchantParameters'));
            $DsResponse = $parameters["Ds_Response"];
            $DsResponse += 0;
            $orderToken = request('orderToken');

            if ($redsys->check($key, request()->all()) && $DsResponse <= 99 && $orderToken) {
                //acciones a realizar si es correcto, por ejemplo validar una reserva, mandar un mail de OK, guardar en bbdd o contactar con mensajería para preparar un pedido

                $data = endoDecrypt($orderToken);
                return $data;
            }
        } catch (\Sermepa\Tpv\TpvException $e) {
            abort(400, $e->getMessage());
        }
    }


    private function confirmRegalo($data, $user = null, $kit = null)
    {
        if (!$user) {
            $user = User::find($data['user_id']);
        }

        $purchase = Purchase::firstOrNew([
            'external_order_id' => $data['order_id'],
            'user_id' => $user->id,
            'post_id' => $data['kit_id']
        ]);

        if ($purchase->status == Purchase::RESERVATION_STATUS_CONFIRMED) {
            return $purchase;
        }

        if (!$kit) {
            $kit = Post::find($data['kit_id']);
        }

        $portes = $kit->get_field('portes');

        $total = $kit->get_field('precio') + ($portes ?: 0);

        $updateParams = [
            'status' => Purchase::RESERVATION_STATUS_CONFIRMED,
            'mensaje' => '',
            'acompanantes' => '',
            'cancelacion' => '',
            'total' => $total,
            'payed' => $total,
            'left' => 0,
            'dharmas_spent' => 0,
            'method' => 'redsys'
        ];

        unset($data['user_id']);
        unset($data['order_id']);
        unset($data['kit_id']);

        $updateParams['extra_data'] = json_encode($data);

        if ($purchase->exists) {
            $purchase->update($updateParams);
        } else {
            $purchase = Purchase::create(array_merge([
                'external_order_id' => $purchase->external_order_id,
                'user_id' => $user->id,
                'post_id' => $purchase->post_id
            ], $updateParams));
        }


        if (isProEnv()) {
            try {
                $this->purchaseEmails($purchase);
            } catch (\Exception $e) {
                // Do nothing
            }
        }

        return $purchase;
    }


    private function purchaseEmails(Purchase $purchase)
    {
        $kit = $purchase->post;
        $extraData = json_decode($purchase->extra_data, true);

        // Client email
        $data = [
            'Regalo de' => $purchase->user->fullname(),
            'Con email' => $purchase->user->email,
            'Kit' => $kit->title,
            'Para' => $extraData['name'] . ' ' . $extraData['lastname'],
            'Teléfono' => $extraData['phone_number'],
            'Dirección' => $extraData['address'] . ', ' . $extraData['cp'] . ', ' . $extraData['city'],
        ];

        $from = User::where('email', 'mensajes@encuentraturetiro.com')->first();

        $infoUser = User::firstOrNew(['email' => 'reservas@encuentraturetiro.com']);

        if (!$infoUser->name) {
            $infoUser->name = 'Retiro';
            $infoUser->lastname = 'Regalo';
        }
        /*

        $sergiETRUser = User::firstOrNew(['email' => 'sergi@encuentraturetiro.com']);

        if (!$sergiETRUser->name) {
            $sergiETRUser->name = 'Sergi';
            $sergiETRUser->lastname = 'Arribas';
        }

        $anaETRUser = User::firstOrNew(['email' => 'ana@encuentraturetiro.com']);

        if (!$anaETRUser->name) {
            $anaETRUser->name = 'Ana';
            $anaETRUser->lastname = 'Mancebo';
        }
        */

        $user = $purchase->user;

        $mensaje = new Mensaje();
        $mensaje->user_id = 0;
        $mensaje->mensaje = '';
        $mensaje->fitxer = '';
        $mensaje->email = '';
        $mensaje->name = '';
        $mensaje->type = 'email';
        $mensaje->params = json_encode($data);

        $params = new stdClass();
        $params->usuario = $user;
        $params->mensaje = $mensaje;
        $params->from = $from;
        $params->email_id = 8;

        $notificacion = new EmailNotification($params);
        if ($notificacion->check_email()) {
            $user->notify($notificacion);
        }

        // Organizer email
        $params = new stdClass();
        $params->usuario = $infoUser;
        $params->mensaje = $mensaje;
        $params->from = $from;
        $params->email_id = 9;

        $notificacion = new EmailNotification($params);
        if ($notificacion->check_email()) {
            $infoUser->notify($notificacion);
        }

        /*
        if (isset($sergiETRUser) && $sergiETRUser) {
            $params->usuario = $sergiETRUser;
            $notificacion = new EmailNotification($params);

            $sergiETRUser->notify($notificacion);
        }

        if (isset($anaETRUser) && $anaETRUser) {
            $params->usuario = $anaETRUser;
            $notificacion = new EmailNotification($params);

            $anaETRUser->notify($notificacion);
        } */
    }
}