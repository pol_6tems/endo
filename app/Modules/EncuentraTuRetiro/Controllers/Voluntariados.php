<?php

namespace App\Modules\EncuentraTuRetiro\Controllers;

use App\Http\Controllers\Controller;
use App\Post;

class Voluntariados extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {
        $post_type = 'voluntariado';
        $posts = Post::where(['type' => $post_type])->get();

        return View('Front::voluntariado.voluntariados', [
            'voluntariados' => $posts
        ]);
    }

    public function show($locale, $id)
    {
        $post = Post::where(['id' => $id])->first();

        if (!$post) return redirect(route('voluntariados'))->with(['message' => __('Post doesn\'t exists')]);

        return View('Front::voluntariado.fitxa', [
            'post' => $post
        ]);
    }

    public function list($locale) {
        return View('Front::voluntariado.llista');
    }
}
