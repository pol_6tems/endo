<?php

namespace App\Modules\EncuentraTuRetiro\Controllers;

use App\Modules\EncuentraTuRetiro\Models\Location;
use Date;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PDF;
use App\Post;
use App\User;
use stdClass;
use Carbon\Carbon;
use Sermepa\Tpv\Tpv;
use GuzzleHttp\Client;
use App\Models\Mensaje;
use Illuminate\Support\Facades\Auth;
use App\Notifications\EmailNotification;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\MensajesController;
use App\Modules\EncuentraTuRetiro\Models\Coupon;
use App\Modules\EncuentraTuRetiro\Models\Review;
use App\Modules\EncuentraTuRetiro\Models\Purchase;
use App\Modules\EncuentraTuRetiro\Redsys\RedsysAPIWs;
use App\Modules\EncuentraTuRetiro\Requests\MyRegisterRequest;
use App\Modules\EncuentraTuRetiro\Repositories\PostsRepository;


class RetirosController extends PostsController
{
    protected $post_type = 'retiro';
    protected $post_type_plural = 'retiros';
    protected $section_route = 'admin.retiros';

    private $secret;

    public function __construct()
    {
        parent::__construct();

        $this->secret = env('ETR_SECRET', '9UpeiZvaHk7O41oTHyN5cUByBPbLHsjCWEkE7c96');
    }


    public function landings($locale, $filter, $locationFilter = null)
    {
        $parameters = [];

        $tags = [
            'tipos-retiro' => 'tipos-de-retiro',
            'retiro-categoria' => 'categoria',
            'retiros-formato' => 'formato',
            'price' => 'retiro-duracion',
            'retiro-tipo-habitaciones' => 'tipo-habitaciones',
            'retiro-duracion' => 'duracion',
            'retiros-publico' => 'publico',
            'retiros-nivel' => 'nivel',
            'retiros-alimentacion' => 'alimentacion',
            'retiro-incluye' => 'incluye2',
            'retiro-especial' => 'especiales'
        ];

        $postTypes = Post::with('translations')->whereIn('type', array_keys($tags))->where('status', 'publish')->get();

        $postType = $postTypes->filter(function ($postType) use ($filter) {
            return $postType->post_name == $filter;
        })->first();

        if (!$postType) {
            $location = Location::where('pais', $filter)
                ->orWhere('region', $filter)
                ->orWhere('provincia', $filter)
                ->first();

            if (!$location) {
                return redirect(Post::get_archive_link('retiros'));
            }
        }

        if ($postType && isset($tags[$postType->type])) {
            $parameters['metas'][0] = [
                $tags[$postType->type],
                'like',
                '%"' . $postType->id . '"%'
            ];

            $parameters['metas_or'][] = $tags[$postType->type];
        }

        if (!isset($location) && $locationFilter) {
            $locationFilter = str_replace('-', ' ', $locationFilter);

            $location = Location::where('pais', $locationFilter)
                ->orWhere('region', $locationFilter)
                ->orWhere('provincia', $locationFilter)
                ->first();
        }

        if (isset($location) && $location) {
            if (str_replace('-', ' ', str_slug(strtolower($location->provincia))) == strtolower($locationFilter)) {
                $code = $location->code_provincia;
                $locName = $location->provincia;
            } else if (str_replace('-', ' ', str_slug(strtolower($location->region))) == strtolower($locationFilter)) {
                $code = $location->code_region;
                $locName = $location->region;
            } else if (str_replace('-', ' ', str_slug(strtolower($location->pais))) == strtolower($locationFilter)) {
                $code = $location->code_pais;
                $locName = $location->pais;
            }

            if (isset($code)) {
                $parameters['location'][] = $code;
            }
        }

        $parameters['order'] = 'rating_desc';

        $retiros = loadRetiros($parameters, false);

        if (!$retiros->total() && !$retiros->count()) {
            return redirect(Post::get_archive_link('retiros'));
        }

        $items = app(PostsRepository::class)->getAllActiveRetiros();

        $routes = [ "Front::posts.archive-retiro", "Front::posts.archive-retiros", "Front::posts.archive"];

        $totalRetiros = max($retiros->total(), $retiros->count());

        $metaTitle = __(':num retiros de :type :location - Encuentra Tu Retiro', [
            'num' => $totalRetiros,
            'type' => $postType ? $postType->title : '',
            'location' => isset($locName) ? __('en ') . $locName : ''
        ]);

        $metaDesc = __('Reserva tus vacaciones de :type :location. :num retiros de :type :location ideales para encontrar el equilibrio de cuerpo, mente y espíritu. ¡Descúbrelos!', [
            'num' => $totalRetiros,
            'type' => $postType ? $postType->title : '',
            'location' => isset($locName) ? __('en ') . $locName : ''
        ]);

        $h1Title = __('<span>Retiros de :type</span> :location', [
            'type' => $postType ? $postType->title : '',
            'location' => isset($locName) ? __('en ') . $locName : ''
        ]);

        $listTitle = __('<span><span class="num">:num</span> ' . __('Experiencias de Retiro') . '</span> ' . __('alrededor del mundo'), ['num' => $totalRetiros]);

        $landingRobots = 'index,follow';

        if (strpos($filter, '-1') !== false) {
            $landingRobots = 'noindex,follow';
        }

        return view()->first($routes, [
            'items' => $items,
            'meta_title' => $metaTitle,
            'meta_desc' => $metaDesc,
            'type_filter' => $postType,
            'location' => isset($location) ? $location : null,
            'locationParam' => $locationFilter,
            'locationTitle' => isset($locName) ? $locName : null,
            'totalRetiros' => $totalRetiros,
            'h1Title' => $h1Title,
            'listTitle' => $listTitle,
            'landingRobots' => $landingRobots,
            'retiros' => $retiros
        ]);
    }

    public function detalles_get($locale, $postName)
    {
        $p = request()->all();

        if (!$p && count(old()) && count(session()->get('errors'))) {
            $errors = [];

            foreach (session()->get('errors')->getBags() as $error) {
                $errors = array_merge($errors, $error->getMessages());
            }

            $retiro = $this->is_single($postName);
            return redirect($retiro->get_url())->withInput(old())->withErrors($errors);
        }

        if (empty($p['token'])) return abort('403');

        $token = str_replace(' ', '+', $p['token']);

        $data = decrypt_token($token);
        if (empty($data)) return abort('403');

        $data = json_decode($data, TRUE);
        if (empty($data)) return abort('403');

        $data['token_retiro'] = $token;

        if ($data['user_id'] != auth()->user()->id) {
            abort(400);
        }

        $retiro = Post::findOrFail($data['retiro']);

        $offer = isset($data['offer']) ? $data['offer'] : '';
        $habitacion_id = !empty($data["habitacion"][$offer]) ? $data["habitacion"][$offer] : 0;
        $habitacion = Post::find($habitacion_id);
        $preu = !empty($data['precio'][$offer]) ? $data['precio'][$offer] : 0;
        $personas = !empty($data['personas'][$offer]) ? $data['personas'][$offer] : 0;

        $author = $retiro->author;

        $dharmas = !empty($data['dharmas']) ? $data['dharmas'] : 0;
        $deposito = !empty($data['deposito']) ? $data['deposito'] : 0;

        if (isset($data['external_order_id'])) {
            $reservation = Purchase::firstOrNew([
                'external_order_id' => $data['external_order_id'],
                'user_id' => $data['user_id'],
                'post_id' => $data['retiro']
            ]);

            $updateParams = [
                'status' => Purchase::RESERVATION_STATUS_ACCEPTED,
                'habitacio_id' => $habitacion->id,
                'mensaje' => isset($data['mensaje']) ? $data['mensaje'] : '',
                'acompanantes' => $personas,
                'cancelacion' => $author->get_field('politica'),
                'total' => $preu,
                'payed' => floatval($deposito - $dharmas),
                'left' => (floatval($preu) - floatval($deposito - $dharmas)),
                'dharmas_spent' => isset($dharmas) ? $dharmas * 100 : 0,
                'extra_data' => json_encode(['dia' => $data['dia']])
            ];

            if ($reservation->exists) {
                $reservation->update($updateParams);
            } else {
                if (Purchase::where('external_order_id', $reservation->external_order_id)->first()) {
                    abort(400);
                }

                Purchase::create(array_merge([
                    'external_order_id' => $reservation->external_order_id,
                    'user_id' => $reservation->user_id,
                    'post_id' => $reservation->post_id
                ], $updateParams));
            }
        }

        return $this->detalles_show($data, true);
    }

    public function detalles()
    {
        $data = request()->all();

        if (!isFavorite($data['retiro'])) {
            storeFavourite(['post' => $data['retiro'], 'type' => 'retiro']);
        }

        return $this->detalles_show($data, !auth()->check());
    }


    public function detallesRegister()
    {
        $data = request()->all();

        $user = auth()->user();

        if ($user) {
            $data['send_request'] = 1;
        }

        if (!$user) {
            // Request validator
            app(MyRegisterRequest::class);

            try {
                if ($data['nombre'] && $data['apellido'] && $data['correo'] && $data['contrasena'] && $data['genero'] && count($data['fecha_nacimiento'])) {
                    $birthday = Carbon::parse($data['fecha_nacimiento']['ano'] . '-' . $data['fecha_nacimiento']['mes'] . '-' . $data['fecha_nacimiento']['dia']);

                    $userData = [
                        'name' => $data['nombre'],
                        'lastname' => $data['apellido'],
                        'email' => $data['correo'],
                        'password' => bcrypt($data['contrasena']),
                        'role' => 'user',
                        'dharmas' => 0,
                        'birthdate' => $birthday,
                        'gender' => $data['genero'],
                        'status' => 'pending',
                        'city' => isset($data['city']) ? $data['city'] : null,
                        'country' => isset($data['country']) ? $data['country'] : null
                    ];

                    $user = User::create($userData);

                    event('add_dharmas', [$user, 300, 'Alta Usuari@']);

                    if (auth()->loginUsingId($user->id)) {
                        $data['send_request'] = 1;
                    }
                }
            } catch (\Exception $e) {
                return redirect()->back()->with('error', __('Error on register'));
            }
        }

        if ($user && isset($data['phone-number']) && !$user->get_field('phone-number')) {
            $user->set_field('phone-number', $data['phone-number']);
        }

        return $this->detalles_show($data, !auth()->check(), true);
    }


    public function detallesAjaxRegister()
    {
        $data = request()->all();

        $user = auth()->user();

        if (!$user && $data['nombre'] && $data['apellido'] && $data['correo'] && $data['contrasena'] && $data['genero'] && count($data['fecha_nacimiento'])) {
            $tmpU = User::where('email', $data['correo'])->first();

            if ($tmpU && Hash::check($data['contrasena'], $tmpU->password)) {
                $user = $tmpU;
            } else {
                if ($tmpU) {
                    return response()->json(['status' => 'error', 'msg' => __('Este email ya está en uso')]);
                }

                try {
                    $birthday = Carbon::parse($data['fecha_nacimiento']['ano'] . '-' . $data['fecha_nacimiento']['mes'] . '-' . $data['fecha_nacimiento']['dia']);

                    $userData = [
                        'name' => $data['nombre'],
                        'lastname' => $data['apellido'],
                        'email' => $data['correo'],
                        'password' => bcrypt($data['contrasena']),
                        'role' => 'user',
                        'dharmas' => 0,
                        'birthdate' => $birthday,
                        'gender' => $data['genero'],
                        'status' => 'pending',
                        'city' => isset($data['city']) ? $data['city'] : null,
                        'country' => isset($data['country']) ? $data['country'] : null
                    ];

                    $user = User::create($userData);

                    event('add_dharmas', [$user, 300, 'Alta Usuari@']);
                } catch (\Exception $e) {
                    return response()->json(['status' => 'error', 'msg' => __('Error on register')]);
                }
            }

            if ($user) {
                auth()->loginUsingId($user->id);
            }
        }

        if ($user && isset($data['phone-number']) && !$user->get_field('phone-number')) {
            $user->set_field('phone-number', $data['phone-number']);
        }

        return response()->json(['status' => 'ok']);
    }


    /**
     * @param $data
     * @param bool $no_mail
     * @param bool $from_login
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detalles_show($data, $no_mail = false, $from_login = false)
    {
        $offer = isset($data['offer']) ? $data['offer'] : '';
        $habitacion_id = !empty($data["habitacion"][$offer]) ? $data["habitacion"][$offer] : 0;
        $habitacion = Post::find($habitacion_id);
        $preu = !empty($data['precio'][$offer]) ? $data['precio'][$offer] : 0;
        $personas = !empty($data['personas'][$offer]) ? $data['personas'][$offer] : 0;

        $retiro = Post::findOrFail($data['retiro']);

        $entrada = $sortida = null;
        if (!empty($data['dia'])) $entrada = explode("@", $data['dia'])[0];
        if (!empty($data['dia'])) $sortida = explode("@", $data['dia'])[1];

        $dharmas = !empty($data['dharmas']) ? $data['dharmas'] : 0;
        $deposito = !empty($data['deposito']) ? $data['deposito'] : 0;

        $payDaysBefore = isset($data['pay_days_before']) ? $data['pay_days_before'] : null;
        $cancelPolicy = isset($data['cancel_policy']) ? $data['cancel_policy'] : null;
        $cancelDaysBefore = isset($data['cancel_days_before']) ? $data['cancel_days_before'] : null;

        $author = $retiro->author;

        if ($author) {
            $moneda = ($author->get_field('moneda')) ? $author->get_field('moneda') : null;
            $dharmasQuantity = round(100 *  toEur($moneda->get_field('iso-code'), $dharmas));
        }

        if ($author && (!$payDaysBefore || !$cancelPolicy || !$cancelDaysBefore)) {
            $pago = $author->get_field('pago');

            if (is_null($cancelPolicy)) {
                $cancelPolicy = $author->get_field('politica');
            }

            if (is_null($payDaysBefore)) {
                $payDaysBefore = 0;

                if ($pago == '10_antes') {
                    $payDaysBefore = 10;
                }

                if ($pago == '30_antes') {
                    $payDaysBefore = 30;
                }
            }

            if (is_null($cancelDaysBefore)) {
                $cancelDaysBefore = 10;

                if ($cancelPolicy == 'moderada') {
                    $cancelDaysBefore = 30;
                }
            }
        }

        $isDirectReservation = false;
        $reserveTypes = $retiro->get_field('tipo-reserva');

        if ($reserveTypes) {
            $reserveTypes = json_decode($reserveTypes, true);

            if (isset($reserveTypes['immediata']) && $reserveTypes['immediata']) {
                $isDirectReservation = true;
            }
        }

        $offerOriginal = collect($retiro->get_field('ofertas-retiro'))->filter(function ($offer) use ($habitacion_id) {
            return isset($offer['habitacion']['value']) && isset($offer['habitacion']['value']->id) && $offer['habitacion']['value']->id == $habitacion_id;
        })->get($data['offer']);        
        
        $depositoOriginal = $author->get_field('deposito');

        $descuento = 0;
        if (isset($offerOriginal['descuento']['value']) && $offerOriginal['descuento']['value'] && $offerOriginal['fecha-limite']['value']) {
            $fecha = Carbon::createFromFormat('d/m/Y', $offerOriginal['fecha-limite']['value']);
			if ($fecha > Carbon::now()) $descuento = $offerOriginal['descuento']['value'];
        }

        $depositoQuantity = round(($offerOriginal['precio']['value'] - $descuento) * ($depositoOriginal / 100), 0, PHP_ROUND_HALF_DOWN);

        $amount = $data['deposito'];
        $amount = fromUserCurrency('EUR', $amount, false);

        
        // Fix if currency change
        if (round($amount, 2) != round($depositoQuantity, 2) || $offerOriginal['precio']['value'] - $descuento != $preu) {
            $deposito = toUserCurrency($moneda->get_field('iso-code'), $depositoQuantity, false);

            if ($dharmas) {
                $dharmasQuantity = auth()->user()->dharmas;
                $dharmas = toUserCurrency($moneda->get_field('iso-code'), $dharmasQuantity / 100, false);
            }
        }


        $viewParams = [
            'offer' => $offer,
            'habitacion' => $habitacion,
            'precio' => $preu,
            'personas' => $personas,
            'entrada' => $entrada,
            'sortida' => $sortida,
            'retiro' => $retiro,
            'dharmas' => $dharmas,
            'dharmasQuantity' => isset($dharmasQuantity) ? $dharmasQuantity : round($dharmas * 100),
            'deposito' => $deposito,
            'retiro_aceptado' => !empty($data['token_retiro']),
            'pay_days_before' => $payDaysBefore,
            'cancel_policy' => $cancelPolicy,
            'cancel_days_before' => $cancelDaysBefore,
            'author' => $author,
            'galeria' => $retiro->get_field('galeria'),
            'es_reserva_inmediata' => $isDirectReservation,
            'plazas' => !empty($habitacion) ? $habitacion->get_field('plazas') : null,
            'currencySymbol' => userCurrency(),
            'moneda' => isset($moneda) && $moneda ? $moneda : null,
            'moreDharmas' => isset($moneda) && $moneda ? toEur($moneda->get_field('iso-code'), $preu - fromUserCurrency('EUR', $dharmas, false)) : $preu,
            'external_order_id' => isset($data['external_order_id']) ? $data['external_order_id'] : null,
            'from_login' => $from_login
        ];

        /* Mensaje data */
        if (!$no_mail && !$isDirectReservation && isset($data['send_request']) && $data['send_request']) {

            $reservation = Purchase::create([
                'external_order_id' => time(),
                'user_id' => auth()->user()->id,
                'post_id' => $retiro->id,
                'status' => Purchase::RESERVATION_STATUS_PENDING,
                'habitacio_id' => $habitacion->id,
                'mensaje' => isset($data['mensaje']) ? $data['mensaje'] : '',
                'acompanantes' => $personas,
                'cancelacion' => $author->get_field('politica'),
                'total' => $preu,
                'payed' => floatval($deposito - $dharmas),
                'left' => (floatval($preu) - floatval($deposito - fromUserCurrency('EUR', $dharmas, false))),
                'dharmas_spent' => isset($dharmas) ? $dharmas * 100 : 0,
                'extra_data' => json_encode(['dia' => $data['dia']])
            ]);

            $data['external_order_id'] = $reservation->external_order_id;
            $data['user_id'] = auth()->user()->id;

            $url_token = encrypt_token($data);
            $mensaje_data = array();
            $mensaje_data['no_mensaje_allowed'] = true;
            $mensaje_data['user_id'] = Auth::user()->id;
            $mensaje_data['type'] = 'retiro';
            $mensaje_data['chat'] = Auth::user()->id . '_';
            $mensaje_data['params']['Retiro'] = '"<a href="' . $retiro->get_url() . '" target="_blank">' . $retiro->translate(true)->title . '</a>"';
            $mensaje_data['params']['Organizador'] = $retiro->author->fullname();
            $mensaje_data['params']['retiro_name'] = $retiro->translate(true)->title;
            $mensaje_data['params']['Fechas'] = 'Del ' . $entrada . ' al ' . $sortida;
            $mensaje_data['params']['Habitación'] = !empty($habitacion) ? $habitacion->translate(true)->title : '';
            $mensaje_data['params']['Plazas'] = $personas;
            $mensaje_data['params']['Precio'] = $preu . ' ' . $moneda->get_field('short');
            $mensaje_data['params']['Mensaje'] = isset($data['mensaje']) ? $data['mensaje'] : '';
            $mensaje_data['to'] = !empty($data['user_id_filter']) ? $data['user_id_filter'] : 0;
            $mensaje_data['params']['URL'] = '<strong>' . __('PETICIÓN DE RESERVA APROBADA') .'</strong><br>"<a href="' . route('retiros.detalles', $retiro->translate(true)->post_name) . '?token=' . $url_token . '" target="_blank">' . __('Retiro y fechas: ":post_name"', ['post_name' => $retiro->title]) . '</a>"<br>' . __('CONFIRMA Y PAGA LA RESERVA') . '&nbsp;<a href="' . route('retiros.detalles', $retiro->translate(true)->post_name) . '?token=' . $url_token . '" target="_blank">' . __('AQUÍ') . '</a><br><br>' . __('Recuerda que sólo se guarda la plaza hasta 24h después de la aprobación.');
            $mensaje_data['params']['ACCEPT_ACTION'] = '$.ajax({
                    url: \''. route('retiros.change-status') . '\',
                    method: \'POST\',
                    data: {\'_token\': $(\'meta[name=csrf-token]\').attr("content"), \'status\': \'' . Purchase::RESERVATION_STATUS_ACCEPTED . '\', \'external_order_id\': ' . $reservation->external_order_id . ', \'user_id\': ' . auth()->user()->id . ', \'retiro_id\':' . $retiro->id . '},
                })';
            $mensaje_data['params']['DENY_URL'] = '<strong>' . __('PETICIÓN RECHAZADA') .'</strong><br>"<a href="' . $retiro->get_url() . '" target="_blank">' . $retiro->title . '</a>"<br>' . __('No tenemos disponibilidad para la petición de reserva que nos has formulado.');
            $mensaje_data['params']['DENY_ACTION'] = '$.ajax({
                    url: \''. route('retiros.change-status') . '\',
                    method: \'POST\',
                    data: {\'_token\': $(\'meta[name=csrf-token]\').attr("content"), \'status\': \'' . Purchase::RESERVATION_STATUS_DENIED . '\', \'external_order_id\': ' . $reservation->external_order_id . ', \'user_id\': ' . auth()->user()->id . ', \'retiro_id\':' . $retiro->id . '},
                })';
            MensajesController::enviar_mensaje($mensaje_data);


            // Email client
            if (isProEnv()) {
                $this->emailClient($author, $retiro->translate(true)->title);
            }

            $viewParams['send_request'] = 0;

            return view('Front::retiros.reserva', $viewParams);
        }
        /* end Mensaje data */
        $viewParams['send_request'] = 1;

        if ($isDirectReservation && !auth()->check()) {
            return View('Front::retiros.comanda-immediata', $viewParams);
        } else {
            return View('Front::retiros.comanda', $viewParams);
        }
    }


    /**
     * Funció per realitzar el pagament a través del formulari de REDSYS
     *
     *  Test:
     *  Número de comercio (Ds_Merchant_MerchantCode): 999008881
     *  Terminal (Ds_Merchant_Terminal): 001
     *  Clave secreta: sq7HjrUOBfKmC576ILgskD5srU870gJ7
     *  Tarjeta aceptada:
     * o Numeración: 4548812049400004
     * o Caducidad: 12/20
     * o Código CVV2: 123
     * En modo de compra segura (CES), en la que se requiera autenticación
     * del comprador, el código de identificación personal (CIP) es: 123456
     */
    public function comprar()
    {
        $data = request()->all();
        $retiro = Post::find($data['retiro']);
        $amount = $this->getAmount($data, $retiro);

        $pan = null;
        $expiracion = null;
        $cvv = null;

        if (array_key_exists('tarjeta_credito', $data)) {
            $pan = str_replace(' ', '', $data['tarjeta_credito']);
        }

        if (array_key_exists('expiracion', $data) && $data['expiracion']) {
            $expiracion = str_replace(' ', '', $data['expiracion']);
            $expiracion = explode('/', $expiracion);
            $expiracion = implode('', [$expiracion[1], $expiracion[0]]);
        }

        if (array_key_exists('cvv', $data)) {
            $cvv = str_replace(' ', '', $data['cvv']);
        }

        //$key = 'HjXY/fdWsQWUFzKwU1OoXxD+N09lSBN2';
        $key = env('REDSYS_KEY');
        $env = env('REDSYS_ENV');
        $term = env('REDSYS_TERMINAL');

        try {
            $orderId = array_key_exists('external_order_id', $data) ? $data['external_order_id'] : time();

            $redsys = new Tpv();
            $redsys->setAmount($amount);
            $redsys->setOrder($orderId);
            $redsys->setMerchantcode('185006673');

            // No funciona
            // $redsys->setCurrency($currency); // 978 para Euros, 840 para Dólares, 826 para libras esterlinas y 392 para Yenes
            $redsys->setCurrency(978);

            $redsys->setTransactiontype('0');
            $redsys->setTerminal($term);
            $redsys->setMethod('C'); // Solo pago con tarjeta, no mostramos iupay

            unset($data['tarjeta_credito']);
            unset($data['expiracion']);
            unset($data['cvv']);
            unset($data['_token']);
            
            $data['user_currency'] = userCurrency(true);

            $data['order_id'] = $orderId;
            $data['user_id'] = auth()->user()->id;

            $orderToken = endoEncrypt($data);

            $redsys->setNotification(route('retiros.comprar.noti', ['locale' => app()->getLocale(), 'postName' => $retiro->post_name, 'orderToken' => $orderToken])); //Url de notificacion // Comprovació del Resultat
            $redsys->setUrlOk(route('retiros.comprar.ok', ['locale' => app()->getLocale(), 'postName' => $retiro->post_name, 'orderToken' => $orderToken])); //Url OK
            $redsys->setUrlKo(route('retiros.comprar.ko', $retiro->post_name)); //Url KO


            $redsys->setVersion('HMAC_SHA256_V1');
            $redsys->setTradeName('ENCUENTRA TU RETIRO');
            $redsys->setTitular('SERGI ARRIBAS TORRAS');

            // Redsys pago por web service
            if ($pan && $expiracion && $cvv) {
                $redsys->setPan($pan); //Número de la tarjeta
                $redsys->setExpiryDate($expiracion); //AAMM (año y mes)
                $redsys->setCVV2($cvv); //CVV2 de la tarjeta
            } // ELSE: Redsys pago por redirección


            $redsys->setEnvironment($env); //Entorno test

            $signature = $redsys->generateMerchantSignature($key); //

            if ($pan && $expiracion && $cvv) {
                $redsysWs = new RedsysAPIWs();

                $amount = $data['deposito'];
                $id = time();

                $datosEnt="<DATOSENTRADA><DS_MERCHANT_AMOUNT>$amount</DS_MERCHANT_AMOUNT><DS_MERCHANT_ORDER>$id</DS_MERCHANT_ORDER><DS_MERCHANT_MERCHANTCODE>185006673</DS_MERCHANT_MERCHANTCODE><DS_MERCHANT_CURRENCY>978</DS_MERCHANT_CURRENCY><DS_MERCHANT_PAN>$pan</DS_MERCHANT_PAN><DS_MERCHANT_CVV2>$cvv</DS_MERCHANT_CVV2><DS_MERCHANT_TRANSACTIONTYPE>0</DS_MERCHANT_TRANSACTIONTYPE><DS_MERCHANT_TERMINAL>2</DS_MERCHANT_TERMINAL><DS_MERCHANT_EXPIRYDATE>$expiracion</DS_MERCHANT_EXPIRYDATE><DS_MERCHANT_MERCHANTNAME>ENCUENTRA TU RETIRO</DS_MERCHANT_MERCHANTNAME></DATOSENTRADA>";

                $nuevaFirma = $redsysWs->createMerchantSignatureHostToHost($key, $datosEnt);

                $nuevaEntrada = "<REQUEST>".$datosEnt."<DS_SIGNATUREVERSION>HMAC_SHA256_V1</DS_SIGNATUREVERSION><DS_SIGNATURE>".$nuevaFirma."</DS_SIGNATURE></REQUEST>";

                $soap_request  = "<?xml version=\"1.0\"?>\n";
                $soap_request .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.sis.sermepa.es">';
                $soap_request .= '<soapenv:Header/>';
                $soap_request .= '<soapenv:Body>';
                $soap_request .= '<web:trataPeticion>';
                $soap_request .= '<web:datoEntrada><![CDATA['.$nuevaEntrada.']]></web:datoEntrada>';
                $soap_request .= '</web:trataPeticion>';
                $soap_request .= '</soapenv:Body>';
                $soap_request .= '</soapenv:Envelope>';

                $client = new Client(['defaults' => [
                    'verify' => false,
                    'timeout'  => 10,
                ]]);

                $response = $client->request('POST', env('REDSYS_WS_URL'), [
                    'body' => $soap_request,
                    'headers' => [
                        'Content-Type' => 'text/xml;charset="UTF-8"',
                        'Accept' => 'text/xml',
                        'Cache-Control' => 'no-cache',
                        'Pragma' => 'no-cache',
                        'SOAPAction' => '"run"',
                        'Content-length' => strlen($soap_request)
                    ]
                ]);

                $data = (string)$response->getBody();
                preg_match ( "/<p[0-9]+:trataPeticionReturn>/", $data, $tag );
                $result = htmlspecialchars_decode ( $redsysWs->getTagContent ($data, str_replace ( "<", "", str_replace ( ">", "", $tag [0]))));
            }
            $redsys->setMerchantSignature($signature); // 

            // dump($redsys->createForm());
            $redsys->executeRedirection();
        } catch (\Sermepa\Tpv\TpvException $e) {
            echo $e->getMessage();
        }
    }

    public function formOK($locale, $postName)
    {
        $method = request('method');
        $user = auth()->user();

        if (!$method) {
            $data = $this->getOrderData();
        } else {
            $data = request()->all();
            $retiro = Post::find($data['retiro']);

            $this->getAmount($data, $retiro);

            $data['order_id'] = $this->paypalPay($data);
            $data['user_id'] = $user->id;
            $data['user_currency'] = userCurrency(true);

            $this->confirmReservation($data, $user, $method);
        }

        if (!$data || $data['user_id'] != $user->id) {
            return redirect(join("/", $postName))->with('compra', false);
        }

        $reservation = $this->confirmReservation($data, $user);

        if (!$method) {
            return redirect()->route('retiros.reservado', [
                'locale' => $locale,
                'postName' => $postName,
                'order_id' => $reservation->order_id
            ]);
        }

        return response()->json(['url' =>route('retiros.reservado', [
            'locale' => $locale,
            'postName' => $postName,
            'order_id' => $reservation->order_id
        ])]);
    }

    /**
     * TODO?
     *
     * @param $locale
     * @param $postName
     * @return \Illuminate\Http\RedirectResponse
     */
    public function formKO($locale, $postName)
    {
        $retiro = $this->is_single($postName);
        return redirect($retiro->get_url())->with('compra', false);
    }


    public function notification()
    {
        $data = $this->getOrderData();

        if ($data) {
            $this->confirmReservation($data);
        }

        $response = response(json_encode(['success' => true, 'error' => false, 'message' => 'success']), 200, [
            'Content-Type' => 'application/json'
        ]);

        return $response;
    }


    public function reservado($locale, $postName)
    {
        $orderId = request('order_id');
        $externalOrderId = request('external_order_id');
        $retiro = $this->is_single($postName);

        if ((!$orderId && !$externalOrderId) || !$retiro) {
            abort(400);
        }

        $query = Purchase::where('user_id', auth()->user()->id)
            ->where('post_id', $retiro->id);

        if ($orderId) {
            $query = $query->where('order_id', $orderId);
        } else {
            $query = $query->where('external_order_id', $externalOrderId);
        }

        $reservation = $query->first();

        if (!$reservation) {
            abort(400);
        }

        $data = json_decode($reservation->extra_data, true);
        $countAcompanantes = $reservation->acompanantes;

        if (!is_numeric($countAcompanantes)) {
            $countAcompanantes = count(json_decode($countAcompanantes, true));
        }

        if (!empty($data['dia'])) $entrada = explode("@", $data['dia'])[0];
        if (!empty($data['dia'])) $sortida = explode("@", $data['dia'])[1];

        $author = $retiro->author;

        $moneda = ($author->get_field('moneda')) ? $author->get_field('moneda') : null;

        $cancelPolicy = $reservation->cancelacion;

        $cancelDaysBefore = 10;

        if ($cancelPolicy == 'moderada') {
            $cancelDaysBefore = 30;
        }

        $dharmasQuantity = round(toEur($moneda->get_field('iso-code'), $reservation->dharmas_spent));

        $isDirectReservation = false;
        $reserveTypes = $retiro->get_field('tipo-reserva');

        if ($reserveTypes) {
            $reserveTypes = json_decode($reserveTypes, true);

            if (isset($reserveTypes['immediata']) && $reserveTypes['immediata']) {
                $isDirectReservation = true;
            }
        }

        $pago = $author->get_field('pago');

        $payDaysBefore = 0;

        if ($pago == '10_antes') {
            $payDaysBefore = 10;
        }

        if ($pago == '30_antes') {
            $payDaysBefore = 30;
        }

        return view('Front::retiros.reservado', [
            'retiro' => $retiro,
            'galeria' => $retiro->get_field('galeria'),
            'author' => $author,
            'entrada' => $entrada,
            'sortida' => $sortida,
            'personas' => $countAcompanantes,
            'pay_days_before' => $payDaysBefore,
            'precio' => $reservation->total,
            'deposito' => toUserCurrency('EUR', $reservation->payed, false),
            'dharmas' => toUserCurrency('EUR', $reservation->coupon ? $reservation->coupon_discount_amount : $reservation->dharmas_spent / 100, false),
            'es_reserva_inmediata' => $isDirectReservation,
            'habitacion' => $reservation->habitacio,
            'cancel_policy' => $cancelPolicy,
            'cancel_days_before' => $cancelDaysBefore,
            'currencySymbol' => userCurrency(),
            'dharmasQuantity' => isset($dharmasQuantity) ? $dharmasQuantity : $reservation->dharmas_spent,
            'moneda' => isset($moneda) && $moneda ? $moneda : null,
            'coupon' => $reservation->coupon,
            'coupon_amount' => $reservation->coupon_discount_amount,
            'reservation' => $reservation
        ]);
    }


    public function changeReservationStatus()
    {
        $externalOrderid = request('external_order_id');
        $userId = request('user_id');
        $retiroId = request('retiro_id');
        $status = request('status');

        if ($externalOrderid && $userId && $retiroId && $status) {
            Purchase::where([
                'external_order_id' => $externalOrderid,
                'user_id' => $userId,
                'post_id' => $retiroId,
            ])->where('status', '<>', 'confirmed')->where('status', '<>', 'denied')->update([
                'status' => $status
            ]);
        }
    }


    public function valora($locale, $postName)
    {

        $token = request()->token;

        if ( !isset($token) ) {
            abort(403);
        }

        $data = json_decode(endoDecrypt($token));

        $purchase = Purchase::find($data->id);
        $review = Review::where(['hash' => $token, 'purchase_id' => $purchase->id])->first();
        $retiro = $this->is_single($postName);

        if ($review->status == 0) {

            if (!$retiro) {
                abort(400);
            }

            $ubicacion = null;
            $ofertes = $retiro->get_field('ofertas-retiro');

            if (count($ofertes) && isset($ofertes[0]['habitacion']['value'])) {
                $habitacion = $ofertes[0]['habitacion']['value'];
                $alojamiento = $habitacion->get_field('alojamiento');

                $ubicacion = ($alojamiento && is_object($alojamiento)) ? $alojamiento->get_field('localizacion') : null;
            }

            return view('Front::retiros.valora', [
                'retiro' => $retiro,
                'author' => $retiro->author,
                'trans'  => $retiro->translate(),
                'ubicacion' => $ubicacion,
                'ratings' => get_ratings($retiro->id)
            ]);
        }

        // TODO: Abort? Redirect + Error?
        return redirect($retiro->get_url())->with('rating', '2'); // 2 -> Ja ha estat contestada
    }


    public function postValora($locale, $postName)
    {
        $retiro = $this->is_single($postName);
        $token = request()->token;

        if ( !isset($token) ) {
            abort(404);
        }
        
        if (!$retiro) {
            abort(400);
        }
        
        $user = User::find(json_decode(endoDecrypt($token))->user_id);
        
        $review = Review::where('hash', urlencode($token))->first();
        if ( $review ) {
            $review->like = request()->like;
            $review->improve = request()->improve;
            $review->comment = request()->comment;
            $rating = request()->rating;
            $review->etr = $rating['encuentra'];

            unset($rating['encuentra']);
            
            $ratings = [];
            foreach($rating as $key => $rate) {
                $ratings[] = [
                    'rating_id' => $key,
                    'post_id' => $retiro->id,
                    'user_id' => $user->id,
                    'value' => $rate
                ];
            }

            event('add_dharmas', [$user, 300, 'Valoración Retiro']);

            $review->ratings = json_encode($ratings);
            $review->status = 1;
            $review->save();
        }
        
        return redirect($retiro->get_url())->with('rating', '1');
    }


    private function emailClient(User $author, $retiroName)
    {
        $from = User::where('email', 'mensajes@encuentraturetiro.com')->first();

        if (!$from) {
            $from = $author;
        }

        $mensaje = new Mensaje();
        $mensaje->user_id = 0;
        $mensaje->mensaje = '';
        $mensaje->fitxer = '';
        $mensaje->email = '';
        $mensaje->name = '';
        $mensaje->type = 'email';
        $mensaje->params = json_encode([
            'retiro_name' => $retiroName
        ]);

        $user = auth()->user();

        $params = new stdClass();
        $params->usuario = $user;
        $params->mensaje = $mensaje;
        $params->from = $from;
        $params->author = $author;
        $params->email_id = 5;

        $notificacion = new EmailNotification($params);
        if ($notificacion->check_email()) {
            $user->notify($notificacion);
        }
    }

    /**
     * @param $tokenArray
     * @return string
     */
    private function encrypt($tokenArray)
    {
        $encrypted = openssl_encrypt(json_encode($tokenArray), "AES-256-CBC", hash('sha256', $this->secret), 0, substr(hash('sha256', $this->secret), 0, 16));
        return urlencode(base64_encode($encrypted));
    }


    /**
     * @param $token
     * @return mixed
     */
    private function decrypt($token)
    {
        $data = openssl_decrypt(base64_decode(urldecode($token)), "AES-256-CBC", hash('sha256', $this->secret), 0, substr(hash('sha256', $this->secret), 0, 16));
        return json_decode($data, true);
    }


    /**
     * @return mixed
     */
    private function getOrderData() {
        try {
            $redsys = new Tpv();
            $key = env('REDSYS_KEY');

            $parameters = $redsys->getMerchantParameters(request('Ds_MerchantParameters'));
            $DsResponse = $parameters["Ds_Response"];
            $DsResponse += 0;
            $orderToken = request('orderToken');

            if ($redsys->check($key, request()->all()) && $DsResponse <= 99 && $orderToken) {
                //acciones a realizar si es correcto, por ejemplo validar una reserva, mandar un mail de OK, guardar en bbdd o contactar con mensajería para preparar un pedido

                $data = endoDecrypt($orderToken);
                return $data;
            }
        } catch (\Sermepa\Tpv\TpvException $e) {
            abort(400, $e->getMessage());
        }
    }


    /**
     * @param array $data
     * @param User $user
     * @param string $method
     * @return Purchase
     */
    private function confirmReservation(array $data, User $user = null, $method = 'redsys')
    {
        if (!$user) {
            $user = User::find($data['user_id']);
        }

        $reservation = Purchase::firstOrNew([
            'external_order_id' => $data['order_id'],
            'user_id' => $user->id,
            'post_id' => $data['retiro']
        ]);

        if ($reservation->status == Purchase::RESERVATION_STATUS_CONFIRMED) {
            return $reservation;
        }

        $userId = $data['user_id'];

        $author = Post::find($data['retiro'])->author;

        $depositoEur = floatval(toEur($data['user_currency'], $data['deposito']));

        if (isset($data['coupon_code']) && $data['coupon_code']) {
            $coupon = Coupon::where('code', $data['coupon_code'])->first();

            if ($coupon) {
                $data['dharmas'] = 0;
            }
        }

        $total = $data['preu'] - (toEur($data['user_currency'], $data['dharmas'])) - (isset($coupon) && $coupon ? $coupon->value : 0);

        $updateParams = [
            'method' => $method,
            'status' => Purchase::RESERVATION_STATUS_CONFIRMED,
            'habitacio_id' => $data['habitacion'],
            'mensaje' => !empty( $data['mensaje'] ) ? $data['mensaje'] : '',
            'acompanantes' => json_encode($data['acompanantes']),
            'cancelacion' => $author->get_field('politica'),
            'total' => $data['preu'],
            'payed' => $depositoEur,
            'left' => (floatval($total) - $depositoEur),
            'dharmas_spent' => isset($data['dharmas']) ? toEur($data['user_currency'], $data['dharmas']) * 100 : 0,
            'coupon_id' => isset($coupon) && $coupon ? $coupon->id : null,
            'coupon_discount_amount' => isset($coupon) && $coupon ? $coupon->value : null
        ];

        unset($data['user_id']);
        unset($data['order_id']);
        unset($data['retiro']);
        unset($data['habitacion']);
        unset($data['mensaje']);
        unset($data['acompanantes']);
        unset($data['preu']);
        unset($data['deposito']);
        unset($data['dharmas']);
        unset($data['coupon_code']);

        $updateParams['extra_data'] = json_encode($data);

        if ($reservation->exists) {
            $reservation->update($updateParams);
        } else {
            $reservation = Purchase::create(array_merge([
                'external_order_id' => $reservation->external_order_id,
                'user_id' => $user->id,
                'post_id' => $reservation->post_id
            ], $updateParams));
        }

        if ($reservation->dharmas_spent) {
            if (!$user) {
                $user = User::find($userId);
            }

            if ($user && $reservation->dharmas_spent) {
                event('add_dharmas', [$user, (-$reservation->dharmas_spent), 'Gasto dharmas compra retiro']);
            }
        }

        $amountDharmas = $reservation->total - ($reservation->dharmas_spent / 100) - $reservation->coupon_discount_amount;

        event('add_dharmas', [$user, (int)round($amountDharmas), 'Compra de retiro']);

        if (isProEnv()) {
            try {
                $this->reserveEmails($reservation);
            } catch (\Exception $e) {
                Log::channel('encuentra_email')->error('Reserve email crash: ' . $e->getMessage());
            }
        }

        return $reservation;
    }


    private function reserveEmails(Purchase $reservation)
    {
        $from = User::where('email', 'mensajes@encuentraturetiro.com')->first();
        $usrReserva = User::where('email', 'reservas@encuentraturetiro.com')->first();
        if ($email = setting('mail_reservas')) {
            $usrReserva = User::where('email', $email)->first();
        }

        $retiro = $reservation->post;
        $extraData = json_decode($reservation->extra_data, true);
        $acompanantes = json_decode($reservation->acompanantes, true);
        $author = $reservation->post->author;
        $aux = array_values($acompanantes)[0];
        
        if (!$from) {
            $from = $author;
        }
        
        $monedas = cacheable(PostsRepository::class)->getMonedas();
        $userMonedaISO = isset($extraData['user_currency']) ? $extraData['user_currency'] : 'EUR';

        $userMoneda = $monedas->filter(function ($moneda) use ($userMonedaISO) {
            return $moneda->get_field('iso-code') == $userMonedaISO;
        })->first();
        
        $userMonedaSymbol = $userMoneda->get_field('short');
        $people = count($acompanantes);
        $cancelPolicy = $author->get_field('politica');
        $cancelDaysBefore = 10;
        
        if ($cancelPolicy == 'moderada') {
            $cancelDaysBefore = 30;
        }
        
        $cancel_l1 = __('Más de :days días antes que empiece el retiro: Se devuelve el depósito', ['days' => $cancelDaysBefore]);
        $cancel_l2 = __('Menos de :days días antes que empiece el retiro: No se devuelve el depósito', ['days' => $cancelDaysBefore]);
        
        if ($cancelPolicy == 'estricta') {
            $cancel_l1 = __('Se canjea por otras fechas si se cancela 10 días antes del inicio');
            $cancel_l2 = __('No se devuelve el depósito');
        }
        
        $discount = 0;
        
        if ($reservation->coupon_discount_amount) {
            $discount += $reservation->coupon_discount_amount;
        }
        
        if ($reservation->dharmas_spent) {
            $discount += $reservation->dharmas_spent / 100;
        }
        

        $birth = Date::parse($aux['fecha_nacimiento']['ano'] . '-' . $aux['fecha_nacimiento']['mes'] . '-' . $aux['fecha_nacimiento']['dia']);
        $years = $birth->diffInYears(Date::now());
        
        // Client email
        $data = [
            'target' => 'client',
            'retiro' => $retiro->title,
            'retiro_link' => '"<a href="' . $retiro->get_url() . '" target="_blank">' . $retiro->title . '</a>"',
            'order_id' => $reservation->order_id,
            'order_date' => Date::now()->format('d/m/Y'),
            'habitacion' => $reservation->habitacio->title,
            'deposito' => $userMonedaSymbol . ' ' . fromEur($userMonedaISO, $reservation->payed),
            'deposito_value' => fromEur($userMonedaISO, $reservation->payed),
            'discount_value' => fromEur($userMonedaISO, $discount),
            'al_organizador' => $userMonedaSymbol . ' ' . fromEur($userMonedaISO, $reservation->left),

            'fecha_nacimiento' => $years,
            'author_city' => $aux['city'],
            'author_country' => $aux['country'],

            'total' => $userMonedaSymbol . ' ' . fromEur($userMonedaISO, $reservation->total),
            'fechas' => str_replace('@', ' - ', $extraData['dia']),
            'personas' => $people,
            'author_name' => $author->fullname(),
            'author_phone' => $author->get_field('telefono-persona-de-contacto'),
            'author_email' => $author->email,
            'total_per_person' => $userMonedaSymbol . ' ' . fromEur($userMonedaISO, $reservation->total / $people),
            'deposito_per_person' => $userMonedaSymbol . ' ' . fromEur($userMonedaISO, $reservation->payed / $people),
            'al_organizador_per_person' => $userMonedaSymbol . ' ' . fromEur($userMonedaISO, $reservation->left / $people),
            'cancel_l1' => $cancel_l1,
            'cancel_l2' => $cancel_l2,
            'message_to_org' => $reservation->mensaje,
            'is_org' => false
        ];
        
        $data['discount_value'] = $discount;
        
        if ($discount) {
            $data['discount'] = $userMonedaSymbol . ' ' . fromEur($userMonedaISO, $discount);
        }
        
        $mensaje = new Mensaje();
        $mensaje->user_id = 0;
        $mensaje->mensaje = '';
        $mensaje->fitxer = '';
        $mensaje->email = '';
        $mensaje->name = '';
        $mensaje->type = 'email';
        $mensaje->params = json_encode($data);

        $user = $reservation->user;
        
        $params = new stdClass();
        $params->usuario = $user;
        $params->mensaje = $mensaje;
        $params->from = $from;
        $params->author = $author;
        $params->email_id = 6;
        $params->attach_data = [
            'output' => PDF::loadView('Front::pdfs.reserva-cliente', $data)->output(),
            'filename' => __('Reserva') . '-ETR-' . $reservation->order_id . '.pdf'
        ];
        
        
        $notificacion = new EmailNotification($params);
        if ($notificacion->check_email()) {
            $user->notify($notificacion);
            Log::channel('encuentra_email')->info('Email sent to client');
        } else {
            Log::channel('encuentra_email')->warning('Email not sent to client');
        }

        $moneda = $retiro->author->get_field('moneda');

        $monedaSymbol = $moneda->get_field('short');
        $monedaISO = $moneda->get_field('iso-code');

        $totalWithoutDiscount = $reservation->total;

        $realDeposit = isset($extraData['real_deposit']) ? $extraData['real_deposit'] : $reservation->payed;

        $pago = $author->get_field('pago');

        $payDaysBefore = 0;

        if ($pago == '10_antes') {
            $payDaysBefore = 10;
        }

        if ($pago == '30_antes') {
            $payDaysBefore = 30;
        }

        $day = explode('@', $extraData['dia'])[0];
        $payDay = Carbon::createFromFormat('d/m/Y', $day)->subDays($payDaysBefore)->format('d/m/Y');

        $cancel_l1 = __('Más de :days días antes que empiece el retiro: Se devuelve el depósito al usuario', ['days' => $cancelDaysBefore]);
        $cancel_l2 = __('Menos de :days días antes que empiece el retiro: No se devuelve el depósito al usuario y se compensa al Organizador con el depósito pagado por el usuario', ['days' => $cancelDaysBefore]);

        if ($cancelPolicy == 'estricta') {
            $cancel_l1 = __('Más de 10 días antes que empiece el retiro: El Organizador canjea por otras fechas disponibles');
            $cancel_l2 = __('Menos de 10 días antes que empiece el retiro: No se canjea ni devuelve depósito');
        }

        $porcentajeComision = $author->get_field('porcentaje-comision');

        $etrCommission = round($totalWithoutDiscount * ($porcentajeComision / 100));
        $etrPending = ($reservation->payed + $discount) - $etrCommission;

        $participants = json_decode($reservation->acompanantes, true);
        $participantsText = '';

        foreach ($participants as $key => $participant) {
            if (isset($participant['nombre'])) {

                $participantsText .= '<br><strong>' . __('Nombre Participante :key', ['key' => $key > 1 ? $key : '']) . '</strong>';
                $participantsText .= '<br>' . __('Nombre: ') . $participant['nombre'] . ($participant['apellidos'] ? ' ' . $participant['apellidos'] : '');

                if (isset($participant['email'])) {
                    $participantsText .= '<br>' . __('Email: ') . $participant['email'];
                }

                if (isset($participant['genero'])) {
                    $participantsText .= '<br>' . __('Género: ') . $participant['genero'];
                }

                if (isset($participant['telefono'])) {
                    $participantsText .= '<br>' . __('Teléfono: ') . $participant['telefono'];
                }

                if (isset($participant['fecha_nacimiento'])) {
                    $birth = Carbon::parse($participant['fecha_nacimiento']['ano'] . '-' . $participant['fecha_nacimiento']['mes'] . '-' . $participant['fecha_nacimiento']['dia']);
                    $years = $birth->diffInYears(Carbon::now());
                    $participantsText .= '<br>' . __('Edad: ') . $years;
                }
            }
        }
        
        
        $author_data = [
            'target' => 'organizador',
            'retiro' => $retiro->title,
            'retiro_link' => '"<a href="' . $retiro->get_url() . '" target="_blank">' . $retiro->title . '</a>"',
            'order_id' => $reservation->order_id,
            'discount' => isset($data['discount']) ? $data['discount']: null,
            'order_date' => Carbon::now()->format('d/m/Y'),
            'habitacion' => $reservation->habitacio->title,
            'deposito_value' => fromEur($monedaISO, $reservation->payed + $discount),
            'discount_value' => 0,
            'deposito' => $monedaSymbol. ' '  . fromEur($monedaISO, $realDeposit),
            'al_organizador' => $monedaSymbol. ' '  . fromEur($monedaISO, $reservation->left),
            'al_organizador_per_person' => $monedaSymbol. ' '  . fromEur($monedaISO, $reservation->left / count($acompanantes)),
            'total' => $monedaSymbol. ' '  . fromEur($monedaISO, $totalWithoutDiscount),
            'total_per_person' => $monedaSymbol. ' '  . fromEur($monedaISO, $totalWithoutDiscount / count($acompanantes)),
            'deposito_per_person' => $monedaSymbol . ' ' . fromEur($monedaISO, ($reservation->payed + $discount) / $people),
            'fechas' => str_replace('@', ' - ', $extraData['dia']),
            'personas' => count($acompanantes),
            'fecha_nacimiento' => $years,
            'author_city' => $aux['city'],
            'author_country' => $aux['country'],
            'participants_arr' => $participants,
            'author_name' => $user->fullname(),
            'author_phone' => $aux['telefono'],
            'author_email' => $user->email,
            'left_per_person' => $monedaSymbol. ' '  . fromEur($monedaISO, $reservation->left / $people),
            'pay_date' => $payDay,
            'cancel_l1' => $cancel_l1,
            'cancel_l2' => $cancel_l2,
            'message_to_org' => $reservation->mensaje,
            'org_terms' => get_page_url('terminos-y-condiciones-del-servicio-a-organizadores'),
            'commission' => $monedaSymbol. ' '  . fromEur($monedaISO, $etrCommission),
            'etr_pending' => $etrPending != 0 ? $monedaSymbol. ' '  . fromEur($monedaISO, $etrPending) : 0,
            'is_org' => true
        ];
        
        $author_data['participants'] = $participantsText;
        $mensaje->params = json_encode($author_data);

        // Organizer email
        $params = new stdClass();
        $params->usuario = $user;
        $params->mensaje = $mensaje;
        $params->from = $from;
        $params->author = $author;
        $params->email_id = 7;
        $params->attach_data = [
            'output' => PDF::loadView('Front::pdfs.reserva-cliente', array_merge($author_data, ['participants' => $participants]))->output(),
            'filename' => __('Reserva') . '-ETR-' . $reservation->order_id . '.pdf'
        ];

        $notificacion = new EmailNotification($params);
        if ($notificacion->check_email()) {
            $author->notify($notificacion);
            Log::channel('encuentra_email')->info('Email sent to author (organizador)');
        } else {
            Log::channel('encuentra_email')->warning('Email not sent to author (organizador)');
        }

        $params = new stdClass();
        $params->usuario = $usrReserva;
        $params->user = $user;
        $params->user->edad = Date::parse($user->birthdate)->diff(Carbon::now())->format('%y años');
        $params->user->genero = isset($aux['genero']) ? $aux['genero'] : '';
        $params->user->telefono = isset($aux['telefono']) ? $aux['telefono'] : '';
        $params->mensaje = $mensaje;
        $params->from = $from;
        $params->author = $author;
        $params->email_id = 13;

        $pdfOrg = PDF::loadView('Front::pdfs.reserva-cliente', $author_data);
        $pdfClient = PDF::loadView('Front::pdfs.reserva-cliente', $data);
        $params->attach_data_multiple = [
            [
                'output' => $pdfOrg->output(),
                'filename' => __('Reserva-Organizador') . '-ETR-' . $reservation->order_id . '.pdf',
            ],
            [
                'output' => $pdfClient->output(),
                'filename' => __('Reserva-Usuario') . '-ETR-' . $reservation->order_id . '.pdf'
            ]
        ];

        $notificacion = new EmailNotification($params);

        if ($notificacion->check_email()) {
            $usrReserva->notify($notificacion);
            Log::channel('encuentra_email')->info('Email sent to reservas@');
        } else {
            Log::channel('encuentra_email')->warning('Email not sent to reservas@');
        }
    }


    /**
     * For testing
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pdf()
    {
        $porcentajeComision = 18.15;

        $etrCommission = round(550 * ($porcentajeComision / 100));
        $etrPending = 100 - $etrCommission;
        /*$etrPending = 0;*/

        $data = [
            'retiro' => '6TEMS PROVA 2',
            'retiro_link' => '"<a href="http://encuentraturetiro2.6tems.es/es/retiros/6tems-prova-2" target="_blank">6TEMS PROVA 2</a>"',
            'order_id' => 'R12345',
            'order_date' => Carbon::now()->format('d/m/Y'),
            'habitacion' => 'Habitació 6TEMS',
            'deposito' => '€ 170.16',
            'al_organizador' => '€ 810',
            'total' => '€ 990',
            'fechas' => '29/08/2019 - 04/09/2019',
            'personas' => 3,
            'author_name' => 'Sergi Solsona',
            'author_phone' => '675',
            'author_email' => 'sergi.solsona@6tems.com',
            'total_per_person' => '€ 330',
            'deposito_per_person' => '€ 56.72',
            'al_organizador_per_person' => '€ 270',
            'cancel_l1' => 'Más de 10 días antes que empiece el retiro: Se devuelve el depósito',
            'cancel_l2' => 'Menos de 10 días antes que empiece el retiro: No se devuelve el depósito',
            'message_to_org' => 'dsa da das d ad as',
            'participants' => [[
                'nombre' => 'Sergi',
                'apellidos' => 'Solsona',
                'email' => 'sergi.solsona@6tems.com',
                'genero' => 'Hombre',
                'years' => 36
            ]],
            'commission' => '€' . ' '  . fromEur('EUR', $etrCommission),
            'etr_pending' => $etrPending != 0 ? '€' . ' '  . fromEur('EUR', $etrPending) : 0
        ];

        /*return PDF::loadView('Front::pdfs.reserva-organizador', $data)->download();
        return view('Front::pdfs.reserva-organizador', $data);*/
        return PDF::loadView('Front::pdfs.reserva-cliente', $data)->download();
        return view('Front::pdfs.reserva-cliente', $data);
    }

    public function reviews() {
        $token = request()->token;
        $data = json_decode(endoDecrypt($token));

        $purchase = Purchase::find($data->id);
        $user = User::find($data->user_id);
        $post = Post::find($data->post);
        
        return view('Front::reviews.review', [
            'retiro' => $post,
            'user' => $user,
            'purchase' => $purchase,
        ]);
    }


    private function getAmount(&$data, $retiro)
    {
        // Comprovem que no hagin manipulat l'html
        $habId = $data['habitacion'];
        $offer = collect($retiro->get_field('ofertas-retiro'))->filter(function ($offer) use ($habId) {
            return $offer['habitacion']['value'] && $offer['habitacion']['value']->id == $habId;
        })->get($data['offer']);

        if (!$offer) {
            abort(400);
        }

        $author = $retiro->author;
        $deposito = $author->get_field('deposito');

        $descuento = 0;
        if (isset($offer['descuento']['value']) && $offer['descuento']['value'] && $offer['fecha-limite']['value']) {
            $fecha = Carbon::createFromFormat('d/m/Y', $offer['fecha-limite']['value']);
            if ($fecha > Carbon::now()) $descuento = $offer['descuento']['value'];
        }

        $current = toEur($author->get_field('moneda')->get_field('iso-code'), $offer['precio']['value'], false);

        $depositoQuantity = round(($current - $descuento) * ($deposito / 100), 0, PHP_ROUND_HALF_DOWN);
        $amount = $data['deposito'];
        $amount = fromUserCurrency('EUR', $amount, false);

        $dharmas = $data['dharmas'];
        $dharmas = fromUserCurrency('EUR', $dharmas, false);

        $couponAmount = 0;

        if (isset($data['coupon_code']) && $data['coupon_code']) {
            $coupon = Coupon::where('code', $data['coupon_code'])->first();

            if ($coupon) {
                $data['dharmas'] = 0;
                $couponAmount = $coupon->value;
            }
        }

        if (round($amount + $dharmas + $couponAmount, 2) != round($depositoQuantity, 2) || $offer['precio']['value'] - $descuento != $data['preu']) {
            abort(400);
        } // Else: No s'ha manipulat l'html

        $data['real_deposit'] = round($depositoQuantity, 2);

        return $amount;
    }


    private function paypalPay($data)
    {
        $clientId = env('PAYPAL_CLIENT_ID');
        $secret = env('PAYPAL_SECRET');
        $mode = env('PAYPAL_MODE', 'sandbox');

        if ($mode == 'production') {
            $mode = 'live';
        }

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $clientId,     // ClientID
                $secret      // ClientSecret
            )
        );
        $apiContext->setConfig([
            'mode'=>$mode
        ]);

        $paymentId = $data['paymentID'];
        $payerId = $data['payerID'];
        if((!$paymentId) || (!$payerId)) {
            abort(400, 'UNEXPECTED VARIABLES');
        }

        $payment = Payment::get($paymentId, $apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        $result = $payment->execute($execution, $apiContext);

        return $result->id;
    }
}
