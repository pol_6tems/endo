<?php

namespace App\Modules\EncuentraTuRetiro\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Desconecta y recárgate en retiros espirituales en todo el mundo.";
        return View('Front::home', compact('title'));
    }
}
