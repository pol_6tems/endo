<?php

namespace App\Modules\EncuentraTuRetiro\Controllers;

use App\Modules\EncuentraTuRetiro\Services\AcumbamailService;
use App\Models\Configuracion;
use function GuzzleHttp\json_encode;

class NewsletterController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subscribe()
    {
        $validatedData = request()->validate([
            'email' => 'required|email',
            'policy' =>'accepted',
        ]);

        if ( !is_array($validatedData) && $validatedData->fails() ) {
            return json_encode(["success" => false, "errors" => $validatedData->errors()]);
        }
        
        $email = request()->email;
        $api = Configuracion::where('key', 'api_acumbamail')->first();
        $api = new AcumbamailService($api->value);
        $res = $api->getSubscriberDetails( request()->acumba_list , $email);
        if ( !$res ) {
            $res = $api->addSubscriber( request()->acumba_list , array('email'  => $email), 0, 0, 1);
        } else {
            $res = current($res);
        }

        if ($res)  return json_encode(["success" => true, "result" => $res]);
        return json_encode(["success" => false]);
    }
}
