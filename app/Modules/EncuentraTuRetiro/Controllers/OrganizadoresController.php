<?php

namespace App\Modules\EncuentraTuRetiro\Controllers;

use App\Http\Controllers\Admin\UsersController;
use App\User;

class OrganizadoresController extends UsersController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;
    protected $section_type;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Users');
        $this->section_icon = 'person_pin';
        $this->section_route = 'admin.users';
        $this->section_type = 'user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = User::where('role', 'Organizador')->get();

        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'email',
            'toolbar_header' => [
                //'<a target="_blank" href="' . route('admin.users.exportar') . '" class="btn btn-default btn-sm">' . __('Export') . '</a>',
            ],
            'headers' => [
                __('#') => [ 'width' => '5%' ],
                __('Name') => [],
                __('Email') => [],
                __('ID card') => [],
                __('Created at') => [ 'width' => '15%' ],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => ['fullname()']],
                ['value' => 'email'],
                ['value' => 'dni'],
                ['value' => 'created_at', 'type' => 'date'],
            ],
            'order_col' => 4,
            'order_dir' => 'desc'
        ));
    }
}
