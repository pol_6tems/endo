<?php

namespace App\Modules\EncuentraTuRetiro\Controllers;

use App\Http\Controllers\Admin\MediaController;
use App\Http\Controllers\Controller;
use App\Models\CustomFieldGroup;
use App\Models\UserMeta;
use App\Modules\EncuentraTuRetiro\Models\Purchase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Mensaje;
use App\User;
use App\Notifications\EmailNotification;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('Front::users.perfil');
    }

    public function update() {
        $data = request()->all();
        $request = request();

        /**
         *  return [
         *      'name' => 'required|string|max:255',
         *      'password' => 'nullable|required_with:password_confirmation|string|confirmed',
         *      'current_password' => 'required',
         *  ];
         */
        
        $rules = [
            'name' => 'required|min:3|max:25',
            'email' => 'required|email',
            'city' => 'required',
            'country' => 'required',
            'gender' => 'required',
            'password' => 'required|confirmed',
        ];

        $messages = [
            'required' => 'El campo :attribute es obligatorio.',
            'email' => ':attribute no es un email.',
            'min' => ':attribute debe tener como mínimo 3 caracteres.',
            'max' => ':attribute puede tener como maximo 25 caracteres.',
            'confirmed' => 'las dos contraseñas no coinciden',
        ];

        $validator = Validator::make(request()->all(), $rules, $messages);
        $cumpleanos = \Carbon\Carbon::parse(sprintf('%02d', $data['dia']) . '-' . sprintf('%02d', $data['mes']) . '-' . $data['ano']);
        $cumpleanos = $cumpleanos->toDateTimeString();

        $data['birthdate'] = $cumpleanos;
        unset($data['_token']);
        unset($data['avatar']);
        unset($data['password']);
        unset($data['password_confirmation']);
        unset($data['dia']);
        unset($data['mes']);
        unset($data['ano']);
        $phoneNumber = isset($data['phone-number']) ? $data['phone-number'] : null;
        unset($data['phone-number']);

        $user = Auth::user();

        if ( $request->hasFile('avatar') ) {
            $avatar = $request->avatar;
            Storage::put('public/avatars', $avatar);
            $data['avatar'] = $avatar->hashName();
        }

        if ( !Hash::check(request()->password, Auth::user()->password) ) {
            $validator->add('password', 'Contraseña incorrecte');
        }

        if ( !$validator->fails() ) {
            $user->update($data);

            if ($phoneNumber) {
                $user->set_field('phone-number', $phoneNumber);
            }

            return Redirect::back()->with(['success' => true]);
        }

        // Hook
        execute_actions('save_user', $user);

        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function mensajes() {
        $user_id = Auth::user()->id;
        $chat_id = $user_id . '_';
        $mensajes = null;

        $user_avatar_default = asset('storage/avatars/user-endo.jpg');
        $my_user_avatar = $user_avatar_default;
        $avatar_pub = public_path('storage/avatars/' . Auth::user()->avatar);
        if ( file_exists($avatar_pub) ) $my_user_avatar = asset('storage/avatars/' . Auth::user()->avatar);
        
        if ( Auth::user()->rol->level >= Mensaje::ROL_LEVEL_LIMIT ) {
            $chats_aux = Mensaje::selectRaw('mensajes.chat, MAX(mensajes.created_at) AS last_date, mensajes.type, MAX(mensajes.id) AS last_message_id')
            ->groupBy(['mensajes.chat', 'mensajes.type'])
            ->orderByRaw('MAX(mensajes.created_at) desc')
            ->get();
        } else {
            $chats_aux = Mensaje::where(function ($query) {
                $query->where('user_id', Auth::user()->id)->orWhere('to', Auth::user()->id);
            })
            ->selectRaw('mensajes.chat, MAX(mensajes.created_at) AS last_date, mensajes.type, MAX(mensajes.id) AS last_message_id')
            ->groupBy(['mensajes.chat', 'mensajes.type'])
            ->orderByRaw('MAX(mensajes.created_at) desc')
            ->get();
        }

        $items = array();
        foreach ( $chats_aux as $k => $chat ) {

            if ( Auth::user()->rol->level >= Mensaje::ROL_LEVEL_LIMIT ) {
                $mensajes = Mensaje::where('chat', $chat->chat)->where('type', $chat->type)->orderBy('created_at')->withAll()->get();
            } else {
                $mensajes = Mensaje::where('chat', $chat->chat)->where('type', $chat->type)
                ->where(function ($query) {
                    $query->where('user_id', Auth::user()->id)->orWhere('to', Auth::user()->id);
                })
                ->orderBy('created_at')->withAll()->get();
            }

            $last_mensaje = $mensajes->last();

            /* USUARIOS */
            $chat_users = $last_mensaje->get_users();
            $user_id = 0;
            $user_name = [];
            $user_email = [];
            $user_avatar = $user_avatar_default;
            
            foreach ( $chat_users as $chat_user ) {
                if ( is_int($chat_user) ) {
                    $user = User::find($chat_user);

                    if ( empty($user) ) continue;

                    $user_email[] = $user->email;
                    $user_name[] = $user->name;
                    if ( count($chat_users) == 1 )  {
                        $user_name = [$user->fullname()];
                        if ( !empty($user->avatar) ) {
                            $avatar = asset('storage/avatars/' . $user->avatar);
                            $avatar_pub = public_path('storage/avatars/' . $user->avatar);
                            if ( file_exists($avatar_pub) ) $user_avatar = $avatar;
                        }
                    }
                } else {
                    $parts = explode('##', $chat_user);
                    $user_id = $parts[1];
                    $user_name[] = $parts[0];
                    $user_email[] = $parts[1];
                    $user_avatar = $user_avatar_default;
                }
            }
            /* end USUARIOS */

            $aux = new \stdClass();
            $aux->chat = str_replace('.', '-', str_replace('@', '-', $chat->chat));
            $aux->chat_noencode = $chat->chat;
            $aux->type = $chat->type;
            $aux->mensajes = $mensajes;
            $aux->last_mensaje = $last_mensaje;
            $aux->user_name = !empty($user_name) ? implode(', ', $user_name) : Auth::user()->fullname();
            $aux->user_email = count($chat_users) == 1 ? reset($user_email) : '';
            $aux->user_id = $user_id;
            $aux->user_avatar = !empty($user_name) ? $user_avatar : $my_user_avatar;
            $items[] = $aux;
        }
        return View('Front::users.mensajes', compact('items', 'mensajes', 'user_avatar_default'));
    }


    public function favoritos() {
        return View('Front::users.favoritos');
    }

    public function comunidad() {
        return View('Front::users.comunidad');
    }

    public function dharmas() {
        return View('Front::users.dharmas');
    }


    public function reservas()
    {
        $reservations = Purchase::select(DB::raw('purchases.*'))
            ->join('posts', 'posts.id', '=', 'purchases.post_id')
            ->with(['post', 'habitacio'])
            ->where('purchases.user_id', auth()->user()->id)
            ->where('posts.type', 'retiro')
            ->orderBy('created_at', 'desc')
            ->get();
        
        return view('Front::users.reservas', compact('reservations'));
    }


    public function mensaje_store(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();

        $mensaje = new Mensaje();
        $mensaje->user_id = !empty($data['user_id']) ? $data['user_id'] : 0;
        $mensaje->mensaje = !empty($data['mensaje']) ? $data['mensaje'] : '';
        $mensaje->email = !empty($data['email']) ? $data['email'] : '';
        $mensaje->name = !empty($data['name']) ? $data['name'] : '';
        $mensaje->type = !empty($data['type']) ? $data['type'] : 'chat';
        $mensaje->params = !empty($data['params']) ? json_encode($data['params']) : NULL;

        /* TO */
        $last_mensaje = Mensaje::where('chat', $mensaje->chat)->where('type', $mensaje->type)
        ->orderBy('created_at', 'ASC')->get()->last();
        $mensaje->to = 0;
        if ( !empty($last_mensaje->user_id) && $last_mensaje->user_id != Auth::user()->id )
            $mensaje->to = $last_mensaje->user_id;
        else if ( !empty($last_mensaje->to) && $last_mensaje->to != Auth::user()->id )
            $mensaje->to = $last_mensaje->to;
        /* end TO */

        $mensaje->chat = !empty($data['chat_id']) ? $data['chat_id'] : $mensaje->to . '_' . $mensaje->user_id . '_' . $mensaje->email;

        if (isset($request->fitxer) && !empty($request->fitxer) && gettype($request->fitxer) == 'object' ) {
            $filename = $user->id.'_f_'.time().'.'.$request->fitxer->getClientOriginalExtension();
            $path = $request->fitxer->storeAs('public/fitxers', $filename);
            $mensaje->fitxer = str_replace('public/fitxers', 'fitxers', $path);
        } else $mensaje->fitxer = '';

        if ( $id = $mensaje->save() ) {
            
            if ( isProEnv() ) {
                $chat_users = $mensaje->get_users();

                foreach ( $chat_users as $chat_user ) {
                    if ( is_int($chat_user) ) $usuario = User::find( $chat_user );
                    else {
                        $parts = explode('##', $chat_user);
                        $usuario = new User();
                        $usuario->id = 0;
                        $usuario->name = $parts[0];
                        $usuario->lastname = '';
                        $usuario->email = $parts[1];
                    }

                    $params = new \stdClass();
                    $params->usuario = $usuario;
                    $params->mensaje = $mensaje;
                    $params->from = Auth::user();
                    
                    $chat = \App\Models\Chat::where('type', $mensaje->type)->first();
                    $params->email_id = !empty($chat) ? $chat->email_id : 0;

                    $notificacion = new EmailNotification($params);
                    if ( $notificacion !== false ) $usuario->notify( $notificacion );
                }
            }

            echo 'OK';
        }
        else echo 'KO';

        die();
    }


    public function registerOrganizer()
    {
        $currencies = get_posts(['post_type' => 'moneda']);

        $cf_group_politica = CustomFieldGroup::where('post_type', 'user')->with(['fields'])->get()->filter(function ($cf_group) {
            return $cf_group->fields->where('name', 'politica')->first();
        })->first();

        $fields = $cf_group_politica->fields;

        return View('Front::users.organizer', compact('currencies', 'fields'));
    }


    public function postRegisterOrganizer(Request $request)
    {
        $data = request()->all();
        $user = auth()->user();

        $cf_groups = CustomFieldGroup::where('post_type', 'user')->orderBy('order')->get();

        foreach ($cf_groups as $cf_group) {
            foreach ($cf_group->fields as $field) {
                if (array_key_exists($field->name, $data)) {
                    if ($field->name != 'foto-organizador') {
                        $userMeta = UserMeta::firstOrNew([
                            'user_id' => $user->id,
                            'custom_field_id' => $field->id,
                            'post_id' => 'user'
                        ]);

                        if (is_array($data[$field->name])) {
                            $value = [];

                            foreach ($data[$field->name] as $datum) {
                                $value[$datum] = $datum;
                            }

                            $userMeta->value = json_encode($value);
                        } else {
                            $userMeta->value = $data[$field->name];
                        }

                        $userMeta->save();
                    } else {
                        $file = $request->file('foto-organizador');

                        $return = app(MediaController::class)->upload_file([], $file);
                        $userMeta = UserMeta::firstOrNew([
                            'user_id' => $user->id,
                            'custom_field_id' => $field->id,
                            'post_id' => 'user'
                        ]);

                        $userMeta->value = $return['new_item']->id;
                        $userMeta->save();
                    }
                }
            }
        }

        $user->update(['role' => 'Organizador']);

        return redirect()->route('admin', ['locale' => app()->getLocale()]);
    }
}
