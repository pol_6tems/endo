<?php


namespace App\Modules\EncuentraTuRetiro\Middleware;


use App\Modules\EncuentraTuRetiro\Models\Location;
use Closure;

class OldLandings
{
    public function handle($request, Closure $next)
    {
        $filters = $request->route('filters');
        if (strpos($filters, '-en-') === false) {
            $filters = explode('-', $filters);
            $typeFilter = str_slug($filters[0]);
            $locationFilter = null;

            if (count($filters) > 1) {
                $locationFilter = str_slug($filters[1]);
            }

            if ($typeFilter && $locationFilter) {
                return redirect(route('retiros.landing', ['filter' => $typeFilter, 'location' => $locationFilter]), 301);
            }
        }

        if (route_name('retiros.old-landing2')) {
            $filter = $request->route('filter');
            $location = $request->route('location');

            if ($filter && $location) {
                return redirect(route('retiros.landing', ['filter' => $filter, 'location' => $location]), 301);
            }
        }

        if (route_name('retiros.old-archive-url')) {
            return  redirect(get_archive_link('retiro') . ($request->getQueryString() ? '?' . preg_replace("/\[(.*)\]/", '[]', urldecode($request->getQueryString())) : ''), 301);
        }

        $locationFilter = $request->route('location');

        if ($locationFilter) {
            $regionConversions = Location::REGION_CONVERSIONS;

            foreach ($regionConversions as $key => $regionConversion) {
                if (str_slug($key) == $locationFilter) {
                    return redirect(route('retiros.landing', ['filter' => $request->route('filter'), 'location' => str_slug($regionConversion)]), 301);
                }
            }
        }


        return $next($request);
    }
}