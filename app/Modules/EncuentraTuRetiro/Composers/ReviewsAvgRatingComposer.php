<?php


namespace App\Modules\EncuentraTuRetiro\Composers;


use App\Modules\EncuentraTuRetiro\Repositories\RatingsRepository;
use App\Modules\Ratings\Models\Rating;
use App\Modules\Ratings\Models\RatingResult;
use App\Post;
use Illuminate\View\View;

class ReviewsAvgRatingComposer extends ReviewsBaseComposer
{

    public function compose(View $view)
    {
        $viewData = $view->getData();

        $reviews = null;
        $ratings = null;
        $reviewsAvg = null;

        $retiro = array_key_exists('retObj', $viewData) ? $viewData['retObj'] : null;

        if ($retiro) {
            $reviews = $this->getReviews($retiro);

            $ratings = $this->get_ratings($retiro);

            $view->with(['one_rating' => $ratings['children'][0]]);

            $reviewsAvg = $this->reviewsAvg($reviews, $ratings)['total_avg'];

            if (is_null($reviewsAvg)) {
                if ($ratings && array_key_exists('avg', $ratings) && $ratings['num'] > 0) {
                    $reviewsAvg = $ratings['avg'];
                }
            }
        }

        $view->with([
            'reviews_avgs' => $reviewsAvg
        ]);
    }


    private function get_ratings(Post $retiro)
    {
        $ratingsRepo = cacheable(RatingsRepository::class, 20);
        $ratings = $ratingsRepo->getRatingsByType($retiro->type);

        $ratingResults = $ratingsRepo->getRatingResultsByAuthor($retiro->author_id, $retiro->type);
        $ratingResultsGrouped = $ratingResults->groupBy(function($item) {
            return $item->post_id . '_' . $item->user_id;
        });

        $realRatingResults = collect();

        foreach ($ratingResultsGrouped as $item) {
            $firstRatingResult = $item->first();

            foreach ($ratings as $rating) {
                $ratingResult = $item->where('rating_id', $rating->id)->first();

                if ($ratingResult) {
                    $realRatingResults->push($ratingResult);
                } else {
                    $fakeRatingResult = clone $firstRatingResult;
                    $fakeRatingResult->rating_id = $rating->id;
                    $fakeRatingResult->value = 0;

                    $realRatingResults->push($fakeRatingResult);
                }
            }
        }

        $max = $ratings->first() ? $ratings->first()->maximo : 0;
        $full = $ratings->first() ? $ratings->first()->icono_full->get_thumbnail_url('thumbnail') : '';
        $empty = $ratings->first() ? $ratings->first()->icono_empty->get_thumbnail_url('thumbnail') : '';

        return [
            'avg' => ceil($realRatingResults->avg('value')),
            'num' => $ratingResults->unique(function ($ratingResult) {
                return $ratingResult->user_id . $ratingResult->post_id;
            })->count(),
            'max' => $max,
            'full' => $full,
            'empty' => $empty,
            'children' => $ratings
        ];
    }
}