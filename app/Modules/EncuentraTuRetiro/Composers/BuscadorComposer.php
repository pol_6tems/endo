<?php


namespace App\Modules\EncuentraTuRetiro\Composers;


use App\Modules\EncuentraTuRetiro\Services\PopularSearchesService;
use Carbon\Carbon;
use Illuminate\View\View;
use App\Modules\EncuentraTuRetiro\Repositories\PostsRepository;
use Jenssegers\Date\Date;

class BuscadorComposer
{

    public function compose(View $view)
    {
        $posts = cacheable(PostsRepository::class, 10)->getSearchTypes()->groupBy('type');

        $popularSearches = app(PopularSearchesService::class)->getPopularSearches();

        $firstArrivalDate = Carbon::today()->addMonth();
        $firstArrivalDate->day = 15;
        $lastArrivalDate = clone $firstArrivalDate;
        $lastArrivalDate->addYear();

        $arrivalDates = collect();

        while ($firstArrivalDate < $lastArrivalDate) {
            $arrivalDates->push(Date::parse($firstArrivalDate));
            $firstArrivalDate->addMonth();
        }

        $filter = request()->query('filter', []);

        $duraciones = get_posts('retiro-duracion');
        $duracionYLlegadaString = '';
        $llegadaString = '';

        $currentDuraciones = $duraciones->filter(function($value) use ($filter) {
            return in_array($value->post_name, $filter);
        });
        
        if ($currentDuraciones->count()) {
            foreach ($currentDuraciones as $currentDuracion) {
                if ($currentDuracion != $currentDuraciones->first()) {
                    $duracionYLlegadaString .= ', ';
                }

                $duracionYLlegadaString .= $currentDuracion->title;
            }
        }

        if (request('date')) {
            $date = Date::parse(request('date'));
            $range = request('range');

            if ($currentDuraciones->count()) {
                $duracionYLlegadaString .= ' | ';
            }

            if ($date->day == 15 && $range == 15) {
                $llegadaString = $date->format('F Y');
                $duracionYLlegadaString .= $llegadaString;
            } else {
                $duracionYLlegadaString .= $date->format('d/m/Y');
            }
        }

        $view->with([
            'types' => $posts,
            'popularSearches' => $popularSearches,
            'arrivalDates' => $arrivalDates,
            'filter' => $filter,
            'duraciones' => $duraciones,
            'duracionYLlegadaString' => $duracionYLlegadaString,
            'llegadaString' => $llegadaString
        ]);
    }
}