<?php


namespace App\Modules\EncuentraTuRetiro\Composers;


use App\Modules\EncuentraTuRetiro\Models\Review;
use App\Post;
use Illuminate\Support\Facades\DB;

abstract class ReviewsBaseComposer
{

    protected function getReviews($retiro)
    {
        return Review::select(DB::raw('reviews.*'))
            ->with(['purchase.user'])
            ->join('purchases', 'purchases.id', '=', 'reviews.purchase_id')
            ->join('posts', 'posts.id', '=', 'purchases.post_id')
            ->where('posts.author_id', $retiro->author_id)
            ->where('reviews.status', Review::STATUS_ACCEPTED)
            ->orderBy('id', 'desc')
            ->get();
    }


    protected function reviewsAvg($reviews, $ratings)
    {
        if ($reviews && $reviews->count()) {
            $ratingsAux = [];
            $totalAvg = 0;
            $count = 0;

            foreach ($reviews as $review) {
                $revRatings = collect(json_decode($review->ratings));
                foreach ($ratings['children'] as $rating) {
                    $ratingsAux[$rating->id] = [
                        'id' => $rating->id,
                        'name' => $rating->name,
                    ];

                    $reviewRating = $revRatings->where('rating_id', $rating->id)->first()->value;
                    $ratingsAux[$rating->id]['ratings'][] = $reviewRating;
                    $ratingsAux[$rating->id]['avg'] = array_sum($ratingsAux[$rating->id]['ratings']) / count($ratingsAux[$rating->id]['ratings']);

                    $totalAvg += $reviewRating;
                    $count++;
                }
            }

            return [
                'ratings_avgs' => $ratingsAux,
                'total_avg' => round($totalAvg / $count, 1)
            ];
        }
    }
}