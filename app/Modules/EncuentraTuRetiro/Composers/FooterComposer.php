<?php


namespace App\Modules\EncuentraTuRetiro\Composers;


use App\Modules\EncuentraTuRetiro\Repositories\PostsRepository;
use Illuminate\View\View;

class FooterComposer
{
    const EUR_CURRENCY_ID = 995;

    public function compose(View $view)
    {
        $activeRetiros = app(PostsRepository::class)->getAllActiveRetiros()->count();

        $monedas = cacheable(PostsRepository::class)->getMonedas();

        $currentMonedaId = request()->cookie('etr_currency', self::EUR_CURRENCY_ID);

        $currentMoneda = $monedas->where('id', $currentMonedaId)->first();

        if (!$currentMoneda) {
            $currentMoneda = $monedas->where('id', self::EUR_CURRENCY_ID)->first();
        }

        $view->with([
            'activeRetiros' => $activeRetiros,
            'monedas' => $monedas,
            'currentMoneda' => $currentMoneda
        ]);
    }
}