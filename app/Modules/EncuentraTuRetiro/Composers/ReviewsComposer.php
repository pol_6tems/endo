<?php


namespace App\Modules\EncuentraTuRetiro\Composers;


use App\Modules\Comments\Models\Comment;
use App\Modules\EncuentraTuRetiro\Models\Review;
use App\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ReviewsComposer extends ReviewsBaseComposer
{

    public function compose(View $view)
    {

        $reviews = null;
        $retiro = null;
        $ratings = null;

        $viewData = $view->getData();

        if (array_key_exists('item', $viewData) && $viewData['item'] instanceof Post && $viewData['item']->type == 'retiro') {
            $retiro = $viewData['item'];
        }

        if (!$retiro && array_key_exists('post', $viewData) && $viewData['post'] instanceof Post && $viewData['post']->type == 'retiro') {
            $retiro = $viewData['post'];
        }

        if (!$retiro && array_key_exists('retiro', $viewData) && $viewData['retiro'] instanceof Post && $viewData['retiro']->type == 'retiro') {
            $retiro = $viewData['retiro'];
        }

        if ($retiro) {
            $reviews = $this->getReviews($retiro);

            $reviews->each(function ($review) {
                $revRatings = collect(json_decode($review->ratings));
                $review->avg = round($revRatings->avg('value'), 1);
                $review->decodedRatings = $revRatings;
            });

            $ratings = get_ratings($retiro->id);

            $view->with(['one_rating' => $ratings['children'][0]]);

            if (!$reviews || !$reviews->count() || $reviews->count() < 4) {
                $comments = $this->get_comments($retiro);

                $view->with(['comments' => $comments]);
            }
        }

        $view->with([
            'reviews' => $reviews,
            'reviews_avgs' => $this->reviewsAvg($reviews, $ratings),
            'ratings' => $ratings['children']
        ]);
    }



    private function get_comments(Post $retiro)
    {
        return Comment::select(DB::raw('distinct comments.*'))
            ->join('posts', 'posts.id', '=', 'comments.post_id')
            ->where('posts.author_id', $retiro->author_id)
            ->where('posts.type', $retiro->type)
            ->withAll()->orderby('created_at', 'DESC')->get();
    }
}