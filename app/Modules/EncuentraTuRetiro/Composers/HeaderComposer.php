<?php


namespace App\Modules\EncuentraTuRetiro\Composers;


use App\Modules\EncuentraTuRetiro\Repositories\PostsRepository;
use Illuminate\View\View;

class HeaderComposer
{

    const EUR_CURRENCY_ID = 995;

    public function compose(View $view)
    {
        $view->with(['user_favorites' => get_user_favourites()]);
        // Moved to footer
        /*$monedas = cacheable(PostsRepository::class)->getMonedas();

        $currentMonedaId = request()->cookie('etr_currency', self::EUR_CURRENCY_ID);

        $currentMoneda = $monedas->where('id', $currentMonedaId)->first();

        if (!$currentMoneda) {
            $currentMoneda = $monedas->where('id', self::EUR_CURRENCY_ID)->first();
        }

        $view->with([
            'monedas' => $monedas,
            'currentMoneda' => $currentMoneda
        ]);*/
    }
}