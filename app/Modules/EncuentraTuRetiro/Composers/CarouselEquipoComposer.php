<?php


namespace App\Modules\EncuentraTuRetiro\Composers;


use Illuminate\View\View;

class CarouselEquipoComposer
{    
    public function compose(View $view)
    {
        $equipo = get_posts('equipo');
        $equipo = $equipo->filter(function($value, $key) {
            return $value->get_field('perfil');
        })->shuffle();
        
        $view->with(['equipo' => $equipo]);
    }
}