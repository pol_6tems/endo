<?php


namespace App\Modules\EncuentraTuRetiro\Console;


use Illuminate\Console\Scheduling\Schedule;
use App\Modules\EncuentraTuRetiro\Console\Commands\SendReviewForm;
use App\Modules\EncuentraTuRetiro\Console\Commands\SendEspaciosMessage;

class Kernel
{

    /**
     * The Artisan commands provided by your module.
     *
     * @var array
     */
    protected $commands = [
        SendReviewForm::class,
        SendEspaciosMessage::class,
    ];


    public function getCommands()
    {
        return $this->commands;
    }

    /**
     * Define the module's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule)
    {

        $schedule->command('update:exchange-rate')->everyThirtyMinutes();
        $schedule->command('send:review-form')->dailyAt('18:00');
    }
}