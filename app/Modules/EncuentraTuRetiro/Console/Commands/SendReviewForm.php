<?php

namespace App\Modules\EncuentraTuRetiro\Console\Commands;

use App\Post;
use App\User;
use Carbon\Carbon;
use App\Models\Mensaje;
use Illuminate\Console\Command;
use App\Notifications\EmailNotification;
use App\Modules\EncuentraTuRetiro\Models\Review;
use App\Modules\EncuentraTuRetiro\Models\Purchase;

class SendReviewForm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:review-form {--rollback : Envia tots els emails no enviats fins al moment}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia todos los emails de las reviews pendientes de contestar';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {        
        $allpurchases = Purchase::with('post')->where('status', 'confirmed')->get();
        $allpurchases = $allpurchases->filter(function($value) {
            if ($value->post && $value->post->type == 'retiro') {
                $extra_data = json_decode($value->extra_data);
                $dies = explode("@", $extra_data->dia);
                $endDay = Carbon::createFromFormat("d/m/Y", $dies[1]);

                $review = Review::where('purchase_id', $value->id)->first();
                return is_null($review) || (!is_null($review) && $review->status == 0);
            }
        });

        $count = 0;

        $allpurchases->map(function($element) use(&$count) {
            if ($element->post && $element->post->type == 'retiro') {
                $notify = false;
                $extra_data = json_decode($element->extra_data);
                $dies = explode("@", $extra_data->dia);
                $endDay = Carbon::createFromFormat("d/m/Y", $dies[1]);
                
                $mensaje = new Mensaje();
                $mensaje->user_id = 0;
                $mensaje->mensaje = '';
                $mensaje->fitxer = '';
                $mensaje->email = '';
                $mensaje->name = '';
                $mensaje->type = 'email';

                $params = new \stdClass();
                $params->usuario = $element->user;
                $params->mensaje = $mensaje;

                if ($this->option('rollback')) {
                    if (Carbon::now() > $endDay) {
                        $notify = true;
                        $params->email_id = 11;
                    }
                } else {
                    if (Carbon::now()->format('d/m/Y') == $endDay->format('d/m/Y')) {
                        $notify = true;
                        $params->email_id = 11;
                    }

                    if (Carbon::now()->format('d/m/Y') == $endDay->addDays(7)->format('d/m/Y')) {
                        $notify = true;
                        $params->email_id = 12;
                    }
                }

                if ($notify) {
                    $retiro = Post::find($element->post_id);
                    $organizador = $retiro->author;

                    $params->retiro = $retiro->title;
                    $params->post_name = $retiro->post_name;
                    $params->organizador = $organizador->fullname();
                    $params->from = User::where('email', 'mensajes@encuentraturetiro.com')->first();

                    // Generar un registre a una taula amb un hash
                    $token = endoEncrypt(json_encode([
                        'id' => $element->id,
                        'order_id' => $element->order_id,
                        'user_id' => $element->user_id,
                        'post' => $element->post_id,
                    ]));

                    $params->token = $token;
                    
                    if ( !Review::where('hash', $token)->exists() ) {
                        Review::create([
                            'purchase_id' => $element->id,
                            'hash' =>  $token,
                            'status' => 0,
                        ]);
                    }

                    $notificacion = new EmailNotification($params);
                    if ($notificacion->check_email()) {
                        $element->user->notify($notificacion);
                        $count++;
                    }
                }
            }
        });

        $this->info("S'han enviat $count correus");
    }
}