<?php

namespace App\Modules\EncuentraTuRetiro\Console\Commands;

use App\User;
use DateTime;
use stdClass;
use App\Models\Mensaje;
use Jenssegers\Date\Date;
use Illuminate\Console\Command;
use App\Notifications\EmailNotification;
use Illuminate\Support\Facades\Validator;

class SendEspaciosMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:espacios-mensajes {data_inici}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia todos los mensajes de la lista de espacios';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app()->setLocale("es");
        url()->defaults(["locale" => "es"]);
        $data_inici = $this->argument("data_inici");
        $validator = !Validator::make([ 'data_inici' => $data_inici ], [ 'data_inici' => 'date_format:d/m/Y' ])->fails();
        if ($validator) {
            $data = Date::createFromFormat("d/m/Y", $data_inici);
            $mensajes = Mensaje::where("created_at", ">=", $data)->get();
            foreach($mensajes as $mensaje) {
                if ($mensaje->type == "espacio" || $mensaje->type == "voluntariado") {
                    if ($reciver = User::find($mensaje->to)) {
                        if (!$sender = User::find($mensaje->user_id)) {
                            $sender = new User();
                            $sender->id = 0;
                            $sender->name = $mensaje->name;
                            $sender->lastname = '';
                            $sender->email = $mensaje->email;
                        }

                        $params = new stdClass();
                        $params->usuario = $reciver;
                        $params->mensaje = $mensaje;
                        $params->from = User::where('email', 'mensajes@encuentraturetiro.com')->first();
                        $params->reciver = $reciver;
                        $params->sender = $sender;
                        $params->email_id = 2;

                        $notificacion = new EmailNotification($params);
                        if ($notificacion->check_email()) {
                            $send = true;

                            if ($params->mensaje->get_param('Fecha Inicio 1', false) == "") $send = false;
                            if ($params->mensaje->get_param('Teléfono', false) == "") $send = false;
                            if ($send) $reciver->notify($notificacion);
                        } else {
                            $this->error("No check email");
                        }
                    }
                }
            }
        } else {
            $this->error("Format wrong, expected format: d/m/Y");
        }
    }
}
