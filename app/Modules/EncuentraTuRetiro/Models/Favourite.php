<?php

namespace App\Modules\EncuentraTuRetiro\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $fillable = [ 'user_id', 'post_id', 'type' ];
}
