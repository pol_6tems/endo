<?php

namespace App\Modules\EncuentraTuRetiro\Models;

use App\Models\Traits\Presentable;
use App\Modules\EncuentraTuRetiro\Models\Presenters\ReservationPresenter;
use App\Modules\Shop\Models\Subscription;
use App\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;

class Purchase extends Model
{
    use SoftDeletes;
    use Presentable;

    const RESERVATION_STATUS_PENDING = 'pending';
    const RESERVATION_STATUS_DENIED = 'denied';
    const RESERVATION_STATUS_ACCEPTED = 'accepted';
    const RESERVATION_STATUS_CONFIRMED = 'confirmed';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dates = ['deleted_at'];
    
    protected $presenter = ReservationPresenter::class;


    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function subscription() {
        return $this->hasOne(Subscription::class);
    }

    public function habitacio()
    {
        return $this->belongsTo(Post::class);
    }
    
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }
    
    
    public function getCreatedAtAttribute($value)
    {
        return Date::parse($value);
    }

    public function isConfirmed() {
        return $this->status == self::RESERVATION_STATUS_CONFIRMED;
    }
    
}
