<?php


namespace App\Modules\EncuentraTuRetiro\Models;


use App\Post;
use Illuminate\Database\Eloquent\Model;

class PopularSearch extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}