<?php

namespace App\Modules\EncuentraTuRetiro\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyResetPassword;

use App\Models\Admin\Rol;
use App\Models\Mensaje;
use App\Models\Post;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'lastname', 'dharmas', 'city', 'country', 'gender', 'birthdate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts() {
        return $this->hasMany(Post::class, 'author_id');
    }


    public function fullname($first_lastname = false) {
        if ( $first_lastname )
            return ( !empty($this->lastname) ? $this->lastname . ', ' : '' ) . $this->name;
        else
            return $this->name . ( !empty($this->lastname) ? ' ' . $this->lastname : '' );
    }

    public function rol() {
        return $this->belongsTo(Rol::class, 'role', 'name');
    }
    
    public function sendPasswordResetNotification($token) {
        $notificacio = new MyResetPassword($token);
        $notificacio->user_fullname = $this->fullname();
        $this->notify($notificacio);
    }

    public function contactar_mensajes()
    {
        return $this->hasMany(Mensaje::class, 'user_id');
    }
    
    public function isAdmin()
    {
        return $this->rol->name === 'admin';
    }
}
