<?php


namespace App\Modules\EncuentraTuRetiro\Models;


use App\Models\Traits\CouponTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes, CouponTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    
    
    protected $purchaseClass = Purchase::class;
    
    protected $userClass = User::class;

    protected $dates = ['deleted_at'];

    protected $softDelete = true;
}