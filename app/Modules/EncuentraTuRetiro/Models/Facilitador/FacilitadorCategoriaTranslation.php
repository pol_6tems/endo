<?php

namespace App\Modules\EncuentraTuRetiro\Models\Facilitador;

use Illuminate\Database\Eloquent\Model;
use App\Models\Media;

class FacilitadorCategoriaTranslation extends Model
{
    public $timestamps = false;
    public $fillable = ['name'];
}
