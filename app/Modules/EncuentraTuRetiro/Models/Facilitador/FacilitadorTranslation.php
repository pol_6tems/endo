<?php

namespace App\Modules\EncuentraTuRetiro\Models\Facilitador;

use Illuminate\Database\Eloquent\Model;
use App\Models\Media;
use App\Modules\EncuentraTuRetiro\Models\Facilitador\FacilitadorCategoria;
use App\Modules\EncuentraTuRetiro\Models\Facilitador\FacilitadorIdioma;

class FacilitadorTranslation extends Model
{
    public $timestamps = false;
    public $fillable = ['name', 'localizacion', 'frase', 'descripcion', 'categoria_id', 'idioma_id', 'media_id'];

    public function media() {
        return $this->belongsTo(Media::class, 'media_id', 'id');
    }

    public function has_thumbnail() {
        if ( empty($this->media) ) return false;
        else return $this->media->has_thumbnail();
    }

    public function get_thumbnail_url($size = '') {
        if ( empty($this->media) ) return null;
        else return $this->media->get_thumbnail_url($size);
    }

    public function categoria() {
        return $this->belongsTo(FacilitadorCategoria::class, 'categoria_id', 'id');
    }

    public function idioma() {
        return $this->belongsTo(FacilitadorIdioma::class, 'idioma_id', 'id');
    }
}
