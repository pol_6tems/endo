<?php

namespace App\Modules\EncuentraTuRetiro\Models\Facilitador;

use Illuminate\Database\Eloquent\Model;
use App\Modules\EncuentraTuRetiro\Models\Facilitador\Facilitador;

class FacilitadorCategoria extends Model
{
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['name'];
    protected $fillable = [];
    protected $guarded = array();

    public function facilitadores() {
        return $this->hasMany(Facilitador::class, 'categoria_id');
    }
}
