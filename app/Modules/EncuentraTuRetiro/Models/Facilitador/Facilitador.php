<?php

namespace App\Modules\EncuentraTuRetiro\Models\Facilitador;

use Illuminate\Database\Eloquent\Model;

class Facilitador extends Model
{
    use \Dimsav\Translatable\Translatable;
    
    protected $table = 'facilitadores';
    public $translatedAttributes = ['name', 'localizacion', 'frase', 'descripcion', 'categoria_id', 'idioma_id', 'media_id'];
    protected $fillable = [];
    protected $guarded = array();
}
