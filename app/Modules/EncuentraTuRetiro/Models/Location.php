<?php

namespace App\Modules\EncuentraTuRetiro\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [ 'alojamiento_id', 'pais', 'code_pais', 'region', 'code_region' ];


    const REGION_CONVERSIONS = [
        'Comunidad de Madrid' => 'Madrid',
        'Región de Murcia' => 'Murcia',
        'Lérida' => 'Lleida',
        'Comunidad Valenciana' => 'Valencia',
        'Valencian Community' => 'Valencia',
        'Balearic Islands' => 'Islas Baleares',
        'País Vasco' => 'Euskadi'
    ];
}
