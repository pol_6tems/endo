<?php

namespace App\Modules\EncuentraTuRetiro\Models;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $fillable = [ 'object_id', 'counter' ];
}
