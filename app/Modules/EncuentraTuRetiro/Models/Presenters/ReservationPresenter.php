<?php


namespace App\Modules\EncuentraTuRetiro\Models\Presenters;


use App\Modules\EncuentraTuRetiro\Models\Purchase;
use App\Support\Presenter;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class ReservationPresenter extends Presenter
{

    public function statusColored()
    {
        $statusText = $this->getStatusText($this->status);
        $statusColor = $this->getStatusColor();

        return "<span class=\"pr-itm-status-$statusColor\">$statusText</span>";
    }


    public function firstImg()
    {
        $galeria = $this->post->get_field('galeria');

        if ($galeria && count($galeria)) {
            $imgUrl = $galeria[0]->get_thumbnail_url();

            return "<img src=\"$imgUrl\" alt=\"\">";
        }

        return '';
    }
    
    
    public function dates()
    {
        $dates = '';

        $data = json_decode($this->extra_data, true);

        if (!empty($data['dia'])) {
            $entrada = Date::createFromFormat('d/m/Y', explode("@", $data['dia'])[0]);
            $sortida = Date::createFromFormat('d/m/Y', explode("@", $data['dia'])[1]);

            $dates = $entrada->format('j M, Y') . ' - ' . $sortida->format('j M, Y');
        }

        return $dates;
    }


    public function people()
    {
        if (is_numeric($this->acompanantes)) {
            $count = $this->acompanantes;
        } else {
            $acompanantes = json_decode($this->acompanantes, true);

            $count = 0;

            if (is_array($acompanantes)) {
                $count = count($acompanantes);
            }
        }

        return $count == 1 ? __(':people persona', ['people' => $count]) : __(':people personas', ['people' => $count]);
    }


    public function statuses()
    {
        return [
            Purchase::RESERVATION_STATUS_PENDING => $this->getStatusText(Purchase::RESERVATION_STATUS_PENDING),
            Purchase::RESERVATION_STATUS_DENIED => $this->getStatusText(Purchase::RESERVATION_STATUS_DENIED),
            Purchase::RESERVATION_STATUS_ACCEPTED => $this->getStatusText(Purchase::RESERVATION_STATUS_ACCEPTED),
            Purchase::RESERVATION_STATUS_CONFIRMED => $this->getStatusText(Purchase::RESERVATION_STATUS_CONFIRMED)
        ];
    }


    public function currencySymbol()
    {
        if (!$this->post || $this->post->type != 'retiro') {
            return '€';
        }

        $author = $this->post->author;

        $currency = $author->get_field('moneda');

        return $currency->get_field('short');
    }

    public function currencyIso()
    {
        if (!$this->post || $this->post->type != 'retiro') {
            return 'EUR';
        }

        $author = $this->post->author;

        $currency = $author->get_field('moneda');

        return $currency->get_field('iso-code');
    }


    public function dharmasCurrency()
    {
        return $this->currencySymbol() . ' -' . round(fromEur($this->currencyIso(), $this->dharmas_spent / 100, $this->created_at), 2);
    }

    public function couponCurrency()
    {
        return $this->currencySymbol() . ' -' . round(fromEur($this->currencyIso(), $this->coupon_discount_amount, $this->created_at), 2);
    }


    public function payedCurrency()
    {
        return $this->currencySymbol() . ' ' . round(fromEur($this->currencyIso(), $this->payed, $this->created_at), 2);
    }


    public function leftCurrency()
    {
        return $this->currencySymbol() . ' ' . round(fromEur($this->currencyIso(), $this->left, $this->created_at), 2);
    }


    public function totalCurrency()
    {
        return $this->currencySymbol() . ' ' . round(fromEur($this->currencyIso(), $this->total - ($this->dharmas_spent / 100) - ($this->coupon_discount_amount ?: 0), $this->created_at), 2);
    }


    public function totalPrice($withSymbol = true)
    {
        $total = $this->total - ($this->dharmas_spent / 100) - ($this->coupon_discount_amount ?: 0);

        return toUserCurrency($this->currencyIso(), $total, $withSymbol, $this->created_at);
    }


    public function payedPrice($withSymbol = true)
    {
        return toUserCurrency($this->currencyIso(), $this->payed, $withSymbol, $this->created_at);
    }

    public function leftPrice($withSymbol = true)
    {
        return toUserCurrency($this->currencyIso(), $this->left, $withSymbol, $this->created_at);
    }


    public function extraFullname()
    {
        $extraData = json_decode($this->extra_data, true);

        return $extraData['name'] . ' ' . $extraData['lastname'];
    }


    public function extraPhone()
    {
        $extraData = json_decode($this->extra_data, true);

        $phoneNumber = '&nbsp;';

        if (isset($extraData['phone_number'])) {
            $phoneNumber = $extraData['phone_number'];
        } else {
            $firstAcompanante = $this->getFirstAcompanante();

            if ($firstAcompanante && isset($firstAcompanante['telefono'])) {
                $phoneNumber = $firstAcompanante['telefono'];
            }
        }

        return $phoneNumber;
    }


    public function extraAddress()
    {
        $extraData = json_decode($this->extra_data, true);

        if (isset($extraData['address']) && $extraData['cp'] && $extraData['city']) {
            return $extraData['address'] . ', ' . $extraData['cp'] . ', ' . $extraData['city'];
        }
    }


    public function organizer()
    {
        $author = $this->post->author;

        if ($author) {
            return $author->fullname();
        }
    }


    public function extraAge()
    {
        $firstAcompanante = $this->getFirstAcompanante();

        if ($firstAcompanante && isset($firstAcompanante['fecha_nacimiento'])) {
            $birth = Carbon::parse($firstAcompanante['fecha_nacimiento']['ano'] . '-' . $firstAcompanante['fecha_nacimiento']['mes'] . '-' . $firstAcompanante['fecha_nacimiento']['dia']);

            return $birth->diffInYears(Carbon::now());
        }

        return '&nbsp;';
    }


    public function extraCity()
    {
        $extraData = json_decode($this->extra_data, true);

        $city = '&nbsp;';

        if (isset($extraData['city'])) {
            $city = $extraData;
        } else {
            $firstAcompanante = $this->getFirstAcompanante();

            if ($firstAcompanante && isset($firstAcompanante['city'])) {
                $city = $firstAcompanante['city'];
            }
        }

        return $city;
    }


    public function extraCountry()
    {
        $extraData = json_decode($this->extra_data, true);

        $country = '&nbsp;';

        if (isset($extraData['country'])) {
            $country = $extraData;
        } else {
            $firstAcompanante = $this->getFirstAcompanante();

            if ($firstAcompanante && isset($firstAcompanante['country'])) {
                $country = $firstAcompanante['country'];
            }
        }

        return $country;
    }


    private function getStatusText($status)
    {
        $string = 'Pendiente';

        if ($status == Purchase::RESERVATION_STATUS_DENIED) {
            $string = 'Denegada';
        }

        if ($status == Purchase::RESERVATION_STATUS_ACCEPTED) {
            $string = 'Aceptada';
        }

        if ($status == Purchase::RESERVATION_STATUS_CONFIRMED) {
            $string = 'Confirmada';
        }

        return __($string);
    }


    private function getStatusColor()
    {
        $string = 'orange';

        if ($this->status == Purchase::RESERVATION_STATUS_ACCEPTED) {
            $string = 'yellow';
        }

        if ($this->status == Purchase::RESERVATION_STATUS_DENIED) {
            $string = 'red';
        }

        if ($this->status == Purchase::RESERVATION_STATUS_CONFIRMED) {
            $string = 'green';
        }

        return __($string);
    }


    private function getFirstAcompanante()
    {
        $extraData = json_decode($this->acompanantes, true);

        if (is_array($extraData) && count($extraData)) {
            return array_first($extraData);
        }
    }
}