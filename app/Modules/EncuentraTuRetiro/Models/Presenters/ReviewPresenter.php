<?php


namespace App\Modules\EncuentraTuRetiro\Models\Presenters;


use App\Modules\EncuentraTuRetiro\Models\Review;
use App\Support\Presenter;

class ReviewPresenter extends Presenter
{

    public function reviewStatus($status = null)
    {
        if (is_null($status)) {
            $status = $this->status;
        }

        $str = 'Not reviewed';

        if ($status == 1) {
            $str = 'Pending';
        }

        if ($status == 2) {
            $str = 'Approved';
        }

        if ($status == 3) {
            $str = 'Denied';
        }

        return __($str);
    }


    public function rating($id)
    {
        $ratings = collect(json_decode($this->ratings));

        return $ratings->where('rating_id', $id)->first()->value;
    }


    public function statuses()
    {
        return [
            Review::STATUS_NOT_REVIEWED => $this->reviewStatus(Review::STATUS_NOT_REVIEWED),
            Review::STATUS_PENDING => $this->reviewStatus(Review::STATUS_PENDING),
            Review::STATUS_ACCEPTED => $this->reviewStatus(Review::STATUS_ACCEPTED),
            Review::STATUS_DENIED => $this->reviewStatus(Review::STATUS_DENIED)
        ];
    }
}