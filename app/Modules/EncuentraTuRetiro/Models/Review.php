<?php

namespace App\Modules\EncuentraTuRetiro\Models;

use App\Models\Traits\Presentable;
use App\Modules\EncuentraTuRetiro\Models\Presenters\ReviewPresenter;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use Presentable;

    const STATUS_NOT_REVIEWED = 0;
    const STATUS_PENDING = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_DENIED = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dates = ['created_at', 'deleted_at'];

    protected $presenter = ReviewPresenter::class;


    public function purchase()
    {
        return $this->belongsTo(Purchase::class);
    }
}
