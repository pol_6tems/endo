<?php


namespace App\Modules\EncuentraTuRetiro\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;

class MyRegisterRequest extends FormRequest
{


    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);

        if (route_name('retiros.detalles.register')) {
            $this->redirect = get_after_login_url();
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:255',
            'correo' => 'required|string|email|max:255|' . Rule::unique('users', 'email'),
            'contrasena' => 'required',
        ];
    }
}