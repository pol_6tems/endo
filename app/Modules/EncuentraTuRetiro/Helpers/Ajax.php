<?php

use App\Post;
use Jenssegers\Date\Date;
use App\Models\CustomPost;
use App\Facades\BotDetector;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Modules\EncuentraTuRetiro\Models\Location;
use App\Modules\EncuentraTuRetiro\Models\Tracking;
use App\Modules\EncuentraTuRetiro\Models\Favourite;
use App\Modules\EncuentraTuRetiro\Repositories\PostsRepository;
use App\Modules\EncuentraTuRetiro\Services\PopularSearchesService;

function ajax_no_priv_storeFavourite() {}
function storeFavourite($parameters) {

    $post = $parameters['post'];

    if (auth()->check()) {
        $type = $parameters['type'];

        if ($actual = Favourite::where('user_id', Auth::id())->where('post_id', $post)->first()) {
            $actual->delete();

            cacheable(PostsRepository::class, 60, true)->userFavorites(auth()->user()->id);

            return response()->json(['success' => true, 'exists' => true]);
        } else {
            $favorito = Favourite::create([
                'user_id' => Auth::id(),
                'post_id' => $post,
                'type' => $type,
            ]);

            cacheable(PostsRepository::class, 60, true)->userFavorites(auth()->user()->id);

            return response()->json(['success' => $favorito ? true : false, 'exists' => false]);
        }
    }

    $key = 'etr_favorites';

    $postIds = collect(json_decode(request()->cookie($key, json_encode([])), true));

    $contains = $postIds->contains(function ($value, $key) use ($post) {
        return $value == $post;
    });

    if ($contains) {
        $postIds = $postIds->filter(function ($value) use ($post) {
            return $value != $post;
        });
    } else {
        $postIds->push($post);
    }

    $postIds = $postIds->unique();

    \Cookie::queue(cookie($key, json_encode($postIds), 525600));

    return response()->json(['success' => true, 'exists' => $contains])->withCookie(cookie($key, json_encode($postIds), 525600));
}

function ajax_no_priv_loadFavourites() {}
function loadFavourites($parameters) {
    $data = get_user_favourites();

    return view('Front::ajax.retiro', $data);
}

function ajax_no_priv_loadRetiros() {}
function loadRetiros($parameters, $renderView = true) {
    pagination();
    $perPage = 16;
    $currentPage = LengthAwarePaginator::resolveCurrentPage();

    $query = [
        'post_type' => 'retiro',
        'metas_or' => ['calendario'],
    ];

    if ( !empty($parameters['metas']) ) {
        app(PopularSearchesService::class)->addPopularSearches($parameters['metas']);
        
        // Corregim preus
        foreach($parameters['metas'] as $key => $meta) {
            if ($meta[0] == 'precio') {
                if (isset($meta[2]) && is_numeric($meta[2])) {
                    $meta[2] = fromUserCurrency("EUR", floatval($meta[2]), false);
                } else if (isset($meta[1]) && is_numeric($meta[1])) {
                    $meta[1] = fromUserCurrency("EUR", floatval($meta[1]), false);
                }
            }
            $parameters['metas'][$key] = $meta;
        }

        $query['metas'] = $parameters['metas'];
    }

    if ( !empty($parameters['metas_or']) ) {
        $query['metas_or'] = $parameters['metas_or'];
    }

    if ( !empty($parameters['metas_and']) ) {
        $andParams =  $parameters['metas_and'];
        $andMetas = array_filter($query['metas'], function($a) use($andParams) {
            return in_array($a[0], $andParams);
        });
        $query['metas'] = array_filter($query['metas'], function($a) use($andParams) {
            return !in_array($a[0], $andParams);
        });
    }

    $query['with'] = ['author.metas.customField', 'metas.customField'];
    $retiros = get_posts($query);

    // There are future valid dates
    $postsRepository = app(PostsRepository::class);
    $retiros = $postsRepository->filterRetirosByDates($retiros);


    if ( !empty($parameters['metas_and']) ) {
        $retiros = $retiros->filter(function($retiro) use ($andMetas, $andParams) {
            $trobat = false;
            foreach($andParams as $param) {
                if ($value = $retiro->get_field($param)) {
                    if (isset($value->calendari)) {
                        $calendari = collect($value->calendari);
                        foreach($andMetas as $meta) {
                            if ($param == 'calendario' && $meta[0] == 'calendario') {
                                $metas = str_replace(["\"%", "%\""], ["", ""], $meta[2]);
                                if (!$trobat && $calendari->contains($metas)) $trobat = true;
                            }
                        }
                    }
                }
            }

            return $trobat;
        });
    }

    if ( !empty($parameters['location']) ) {
        $location = $parameters['location'];

        $locations = Location::whereIn('code_pais', $location)
                                ->orWhereIn('code_region', $location)
                                ->orWhereIn('code_provincia', $location)
                                ->pluck('alojamiento_id');

        $retiros = $retiros->filter(function ($value, $key) use ($locations) {
            $currentAloj = $value->get_field('alojamiento');

            if ($currentAloj && $currentAloj instanceof Post) {
                return $locations->contains($currentAloj->id);
            }
			else return false;
        });
    }

    if ( !empty($parameters['order']) ) {
        $postsRepository->ordenarPosts($retiros, $parameters['order'], true);
        /*ordenar_posts($retiros, $parameters['order'], true);*/
    }

    // Select Destacado
    $destacado = null;
    $retiros = $retiros->values();
    $feturedRetiros = collect();
    $positions = collect([2, 12]);

    $totalRetiros = $retiros->count();
    /*$retiros->load(['metas.customField']);*/

    foreach($retiros as $key => $retiro) {
        if ( $retiro->get_field('destacado') == 1) {
            $feturedRetiros->push(['key' => $key, 'retiro' => $retiro, 'position' => $positions->shift()]);
            
            if (!$positions->count()) {
                break;
            }
        }
    }
    
    if ($feturedRetiros->count()) {
        foreach ($feturedRetiros->sortByDesc('key') as $item) {
            $retiros->forget($item['key']);
        }
    }

    $sliced = $retiros->slice($perPage * ($currentPage - 1), $perPage);
    $retiros->load(['translations']);
    if ($feturedRetiros->count()) {
        foreach ($feturedRetiros as $feturedRetiro) {
            $pos = $feturedRetiro['position'];
            
            $sliced->splice($pos, 0, [$feturedRetiro['retiro']]);
        }
    }


    execute_actions('after_destacados', $sliced);
    afegir_banners('retiros', $sliced, 0, ($perPage/2)+1);

    if (!BotDetector::isBot()) {
        /**
         * Store new tracking data
         */
        $sliced->each(function ($retiro) {
            if (Tracking::where('object_id', $retiro->id)->exists()) {
                Tracking::where('object_id', $retiro->id)->increment('counter');
            } else {
                Tracking::create(['object_id' => $retiro->id]);
            }
        });
    }

    $retiros = new LengthAwarePaginator($sliced, $retiros->count(), $perPage, $currentPage);

    $retiros->withPath(!route_name('retiros.landing') ? Post::get_archive_link('retiros') : '');

    if (!empty($parameters['order']) && !route_name('retiros.landing')) {
        $retiros->appends(['order' => $parameters['order']]);
    }

    if ($renderView) {
        return view('Front::ajax.retiro', compact('retiros', 'query', 'totalRetiros'));
    }

    return $retiros;
}


function afegir_banners($tipo, &$llista, $start = 0, $step = 10) {
    // Show banner every 
    $banners = get_posts([
        "post_type" => "banner-publicidad",
        "metas" => [
            ["tipo", "like", "%\"$tipo\":\"$tipo\"%"],
        ],
    ]);

    if ($banners->count() > 0) {
        $chunks = $llista->splice($start)->chunk($step);

        $full = collect();
        $banners = $banners->shuffle();
        foreach($chunks as $chunk) {
            $randomBanner = $banners->shift();
            // If chunk is full
            
            if ($chunk->count() == $step) $chunk->push($randomBanner);
            $full = $full->merge($chunk);
        }
        
        $llista = $full;
    }
}

function ordenar_posts(&$posts, $order, $rating = false) {
    // PRECIO
    if ( $order == 'price_asc' || $order == 'price_desc' ) {
        $posts = $posts->sort(function ($a, $b) use ($order) {
            $val_a = 0;
            $barata = obtenirOfertaBarata($a);
            if ( !empty($barata) ) $val_a = obtenirPreuAmbDescompteOferta($barata);
            
            $val_b = 0;
            $barata = obtenirOfertaBarata($b);
            if ( !empty($barata) ) $val_b = obtenirPreuAmbDescompteOferta($barata);

            if ($val_a == $val_b) return 0;

            if ( $order == 'price_asc' ) return ($val_a < $val_b) ? -1 : 1;
            else return ($val_a > $val_b) ? -1 : 1;
        });
    // RATING / RECOMENDADO
    } else if ( $order == 'rating_asc' || $order == 'rating_desc' ) {
        // Algoritme Rating Recomendado
        if ($rating) {
            $custom_coef = 1;
            $rating_coef = 1;
            $coef_calidad = 1;
            $loc_coef = 10;
            $politica = [
                "flexible" => 5,
                "moderada" => 2,
                "estricta" => 1,
            ];
            $rand_min = 0;
            $rand_max = 2;
            
            $location = collect(json_decode(request()->cookie('location', json_encode([]))));
            if ($location->count() > 0) {
                if ($location['status'] == 'success') {
                    // Current Location
                    $coords = $location['lat'] . ',' .$location['lon'];
                }
            }
            
            foreach($posts as $post) {
                $punctuation = new stdClass();
                $punctuation->total = 0;
                
                $punctuation->calidad = 0;
                if ($calidad = $post->get_field('calidad')) {
                    $punctuation->calidad = $calidad * $coef_calidad;
                    $punctuation->total += $calidad * $coef_calidad;
                }

                $punctuation->location = 0;
                $alojamiento = $post->get_field('alojamiento');

                if ($alojamiento && is_object($alojamiento)) {
                    $localizacion = $alojamiento->get_field('localizacion');
                    if ($localizacion && isset($coords) && $coords && $localizacion->coordenades) {
                        $newCoords = $localizacion->coordenades;
                        // Diff escalada en 75Km
                        $diff = round(75000 / differenceBetweenTwoCoordinates($newCoords, $coords) * $loc_coef, 0);
                        if ($diff > 10) $diff = 10;
                        
                        $punctuation->location = intval($diff);
                        $punctuation->total += $diff;
                    }
                }

                $punctuation->custom_rating = 0;
                if ($organizador = $post->author) {
                    // Custom Rating ETR
                    if ($custom = $organizador->get_field('rating')) {
                        $punctuation->custom_rating = $custom * $custom_coef;
                        $punctuation->total += $custom * $custom_coef;
                    }

                    // Politica Organizador
                    $punctuation->politica = 0;
                    if ($current_politica = $organizador->get_field('politica')) {
                        $punctuation->politica = $politica[$current_politica];
                        $punctuation->total += $politica[$current_politica];
                    }

                    // Ratings Organizador
                    $punctuation->rating = 0;
                    if ($rating = do_action('get_ratings', $post->id)) {
                        $punctuation->rating = $rating['avg'] * $rating_coef;
                        $punctuation->total += $rating['avg'] * $rating_coef;
                    }
                }
                
                // Random
                $random = rand($rand_min, $rand_max);
                $punctuation->random = $random;
                $punctuation->total += $random;

                $post->punctuation = $punctuation;
            }

            $posts = $posts->sort(function ($a, $b) {
                if ($a->punctuation->total == $b->punctuation->total) return 0;
                else return ($a->punctuation->total > $b->punctuation->total) ? -1 : 1;
            });

            // Fi Algoritme de Promoció
        } else {
            $sort = $order == 'rating_desc' ? 'DESC' : 'ASC';
            $aux = do_action('ratings_order_avg', [$posts, $sort]);
            if ( !empty($aux) ) $posts = $aux;
        }
    // PRECIO POR NOCHE
    } else if ( $order == 'price_night_asc' || $order == 'price_night_desc' ) {
        $posts = $posts->sort(function ($a, $b) use ($order) {
            $val_a = 0;
            $barata = obtenirOfertaBarata($a);
            if ( !empty($barata) ) $val_a = obtenirPreuAmbDescompteOferta($barata);

            $duracion_a = $a->get_field('calendario');
            if ( !empty($duracion_a->duracion) && $val_a > 0 ) {
                $val_a = $val_a / ( $duracion_a->duracion - 1 );
            }
            
            $val_b = 0;
            $barata = obtenirOfertaBarata($b);
            if ( !empty($barata) ) $val_b = obtenirPreuAmbDescompteOferta($barata);

            $duracion_b = $b->get_field('calendario');
            if ( !empty($duracion_b->duracion) && $val_b > 0 ) {
                $val_b = $val_b / ( $duracion_b->duracion - 1 );
            }

            if ($val_a == $val_b) return 0;

            if ( $order == 'price_night_asc' ) return ($val_a < $val_b) ? -1 : 1;
            else return ($val_a > $val_b) ? -1 : 1;
        });
    // DURACIÓN
    } else if ( $order == 'duration_asc' || $order == 'duration_desc' ) {
        $posts = $posts->sort(function ($a, $b) use ($order) {
            $val_a = 0;
            $duracion_a = $a->get_field('calendario');
            if ( !empty($duracion_a->duracion) ) $val_a = $duracion_a->duracion;
            
            $val_b = 0;
            $duracion_b = $b->get_field('calendario');
            if ( !empty($duracion_b->duracion) ) $val_b = $duracion_b->duracion;

            if ($val_a == $val_b) return 0;

            if ( $order == 'duration_asc' ) return ($val_a < $val_b) ? -1 : 1;
            else return ($val_a > $val_b) ? -1 : 1;
        });
    } else if ($order == 'fecha_inicio') {
        $posts = $posts->sort(function ($a, $b) use ($order) {
            $datesA = obtenirProximesDates($a);
            $datesB = obtenirProximesDates($b);

            return $datesA->flatten()->first() < $datesB->flatten()->first() ? -1 : 1;
        });
    }
}

function ajax_no_priv_loadVoluntariadosEspacios() {}
function loadVoluntariadosEspacios($parameters) {
    pagination();
    $perPage = 16;
    $currentPage = LengthAwarePaginator::resolveCurrentPage();

    $query = [
        'post_type' => $parameters['post_type'],
    ];

    $cp = CustomPost::whereTranslation('post_type', $parameters['post_type'])->first();

    if ($parameters && isset($parameters['metas'])) {
        $query['metas'] = $parameters['metas'];
    }
    
    if ( !empty($parameters['metas_or']) ) {
        $query['metas_or'] = $parameters['metas_or'];
    }

    $llista = get_posts($query);

    $cfName = 'ubicacion';

    if ($parameters['post_type'] == 'voluntariado') {
        $cfName = 'ubicacion-y-localizacion';
    }

    if ($parameters && isset($parameters['coords'])) {
        $coordenades = $parameters['coords'];

        $llista = $llista->sortBy(function ($item) use ($coordenades, $cfName) {
            $locItem = $item->get_field($cfName);

            if ($locItem && isset($locItem->coordenades) && $locItem->coordenades) {
                $diff = differenceBetweenTwoCoordinates($coordenades, $locItem->coordenades);

                return $diff;
            }

            return 999999999;
        });
    } else {
        if ( !empty($parameters['order']) ) {
            ordenar_posts($llista, $parameters['order']);
        }
    }


    $total = $llista->count();

    afegir_banners($parameters['post_type'], $llista);

    $sliced = $llista->slice($perPage * ($currentPage - 1), $perPage);

    $llista = new LengthAwarePaginator($sliced, $llista->count(), $perPage, $currentPage);
    $llista->withPath(Post::get_archive_link($cp->post_type_plural));

    if (array_key_exists('coords', $parameters)) {
        $llista->appends('coords', $parameters['coords']);
    }

    if (array_key_exists('filter', $parameters) && count($parameters['filter'])) {
        $llista->appends('filter', $parameters['filter']);
    }

    return view('Front::ajax.espacio', compact('llista', 'total'));
}


function ajax_no_priv_addRetiroVisitCookie() {}
function addRetiroVisitCookie($params) {
    $key = 'visited_retiros';
    $cookieVal = $params['value'];

    $retiros = collect(json_decode(request()->cookie($key, json_encode([])), true));

    $retiros->prepend($cookieVal);

    $retiros = $retiros->unique()->slice(0,9);

    \Cookie::queue(cookie($key, json_encode($retiros), 525600));

    return response()->json(['previousCookieValue' => request()->cookie($key, json_encode([])), 'newCookieValue' => json_encode($retiros)])->withCookie(cookie($key, json_encode($retiros), 525600));
}

function ajax_no_priv_getRetiroVisitCookie() {}
function getRetiroVisitCookie() {
    return response()->json(['cookieValue' => request()->cookie('visited_retiros', json_encode([]))]);
}

function ajax_no_priv_setCurrencyCookie() {}
function setCurrencyCookie($params) {
    $key = 'etr_currency';

    $cookieVal = $params['value'];

    \Cookie::queue(cookie($key, $cookieVal, 525600));

    return response()->json(['status' => 'ok'])->withCookie(cookie($key, $cookieVal, 525600));
}

function ajax_no_priv_checkLocation() {}
function checkLocation() {
    $key = 'location';
    $client = new \GuzzleHttp\Client();

    $location = collect(json_decode(request()->cookie($key, json_encode([]))));

    if ($location->count() < 1) {
        $response = $client->request('GET', 'http://ip-api.com/json/'.request()->ip().'?lang=es');
        $body = $response->getBody();
        $body = json_decode($body);
        $body =  json_encode($body);

        \Cookie::queue(cookie($key, $body, 525600));
        return response()->json(['location' => $body])->withCookie(cookie($key, $body, 525600));
    }
    return response()->json(['location' => json_encode($location)]);
}