<?php
use GuzzleHttp\Client;
use App\Models\PostMeta;
use App\Models\UserMeta;
use App\Models\CustomField;
use function GuzzleHttp\json_decode;
use Illuminate\Support\Facades\Cookie;
use App\Modules\EncuentraTuRetiro\Models\Location;

add_action('save_post', 'save_post_etr_numero');
add_action('save_user', 'save_user_etr_numero');

function save_post_etr_numero($post) {
    $numero = $post->get_field('numero');
    $letra = get_letra_from_type($post->type);
    if ( empty($numero) && !empty($letra) ) {
        $cf_id = create_numero_custom_field();
        $libre = false;
        while(!$libre) {
            $six_digit_random_number = mt_rand(100000, 999999);
            $numero = $letra . $six_digit_random_number;
            $libre = !PostMeta::where('custom_field_id', $cf_id)->where('value', $numero)->exists();
        }
        if ( $libre ) {
            PostMeta::create(['post_id' => $post->id, 'custom_field_id' => $cf_id, 'value' => $numero, 'locale' => app()->getLocale()]);
        }
    }
}

function save_user_etr_numero($user) {
    $numero = $user->get_field('numero');
    $letra = get_letra_from_type('user');
    if ( empty($numero) && !empty($letra) ) {
        $cf_id = create_numero_custom_field();
        $libre = false;
        while(!$libre) {
            $six_digit_random_number = mt_rand(100000, 999999);
            $numero = $letra . $six_digit_random_number;
            $libre = !UserMeta::where('custom_field_id', $cf_id)->where('value', $numero)->exists();
        }
        if ( $libre ) {
            UserMeta::create(['user_id' => $user->id, 'post_id' => 0, 'custom_field_id' => $cf_id, 'value' => $numero]);
        }
    }
}

function create_numero_custom_field() {
    $cf = CustomField::where('name', '=', 'numero')->first();
    if ( $cf == null ) {
        $cf = CustomField::create([
            'order' => 0, 'cfg_id' => 0, 'title' => 'Numero', 'name' => 'numero', 'type' => 'hidden'
        ]);
    }
    return $cf->id;
}

function get_letra_from_type($type) {
    switch($type) {
        case ('espacio'):
            return 'E';
            break;
        case ('retiro'):
            return 'RT';
            break;
        case ('organizador'):
            return 'O';
            break;
        case ('reserva'):
            return 'R';
            break;
        case ('user'):
            return 'U';
            break;
        case ('retiro-regalo'):
            return 'RR';
        case ('subscription'):
            return 'S';
        case ('buyable'):
            return 'B';
    }
    return null;
}



add_action('save_post', 'guardar_paisos');
function guardar_paisos($post) {
    $regionConversion = Location::REGION_CONVERSIONS;

    if ($post->type == 'alojamientos') {
        $location = Location::where([
            'alojamiento_id' => $post->id,
        ])->first();
            
        $localization = $post->get_field('localizacion');
        if ($localization->object) {
            $object = json_decode($localization->object);
            $data = ['alojamiento_id' => $post->id];
            foreach($object->address_components as $component) {
                if ($component->types[0] == 'country') {
                    $data['pais'] = $component->long_name;
                    $data['code_pais'] = $component->short_name;
                }

                if ($component->types[0] == 'administrative_area_level_1') {
                    $data['region'] = $component->long_name;
                    $data['code_region'] = $component->short_name;
                }

                if ($component->types[0] == 'administrative_area_level_2') {
                    $data['provincia'] = $component->long_name;
                    $data['code_provincia'] = $component->short_name;
                }
            }

            if ( !$location ) {
                $location = Location::create($data);
            }
        }
        
        // Fix Countries
        $fields = array("pais", "code_pais", "region", "code_region", "provincia", "code_provincia");
        foreach($fields as $field) {
            if (array_key_exists($location->{$field}, $regionConversion)) {
                $location->{$field} = $regionConversion[$location->{$field}];
            }
        }
        $location->save();
    }
}