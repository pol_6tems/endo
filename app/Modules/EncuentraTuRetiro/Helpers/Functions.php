<?php

use App\Post;
use App\Modules\EncuentraTuRetiro\Composers\HeaderComposer;
use App\Modules\EncuentraTuRetiro\Models\Favourite;
use App\Modules\EncuentraTuRetiro\Models\Transaction;
use App\Modules\EncuentraTuRetiro\Models\Location;
use App\Modules\EncuentraTuRetiro\Repositories\PostsRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

function filterAddURL($post_name) {
    $filters = request()->query();
    
    if ( empty($filters['filter']) || !in_array($post_name, $filters['filter']) ) {
        $filters['filter'][] = $post_name;
    }

    $url = '';
    if ( !empty($filters['filter']) ) {
        foreach($filters['filter'] as $key => $i) {
            $url .= 'filter[]='.$i;
            if ( $key < (count($filters['filter']) - 1) ) $url .= '&';
        }
    }

    return $url;
}


function filterRemovedURL($post_name) {
    $filters = request()->query();

    if( !empty($filters['filter']) && in_array($post_name, $filters['filter']) ) {
        foreach($filters['filter'] as $key => $item) {
            if ($item == $post_name) unset($filters['filter'][$key]);
        }
    }

    $url = '';
    if ( !empty($filters['filter']) ) {
        foreach($filters['filter'] as $key => $i) {
            $url .= 'filter[]='.$i;
            if ( $key < (count($filters['filter']) - 1) ) $url .= '&';
        }
    }

    return $url;
}

function get_active_countries($locations = []) {
    /*if (count($locations) > 0) {
        $result = Location::whereIn('code_pais', $locations)
                        ->orWhereIn('code_region', $locations)
                        ->orWhereIn('code_provincia', $locations)
                        ->get()
                        ->groupBy(['code_pais', 'code_region', 'code_provincia']);
    }*/
        
    $result = Location::all()->groupBy(['code_pais', 'code_region', 'code_provincia']);

    return $result;
}

/**
 * $objects ha de correspondre a una llista dels noms del objectes pels quals s'ha de filtrar en la llista actual
 * Ha de ser del tipus:
 *
 * [
 *      'ENTORNO' => 'espacios-entorno'
 * ]
 * @param $post_object
 * @param $objects
 * @param bool $mobile
 * @return string
 * @throws Throwable
 */
function getFilters($post_object, $objects, $mobile = false) {
    $types = collect($objects)->filter(function ($object) {
        return isset($object['post_type']) && (!isset($object['type']) || !in_array($object['type'], ['price', 'location']));
    })->map(function ($object) {
        return $object['post_type'];
    });

    $typoObjs = Post::with(['metas.customField', 'translations.media'])->publish()->whereIn('type', $types)->orderby('order')->get();

    $newObjects = [];
    foreach ($objects as $key => $object) {
        if (isset($object['post_type'])) {
            $object['objs'] = $typoObjs->where('type', $object['post_type']);
        }

        $newObjects[$key] = $object;
    }

    $objects = $newObjects;

    return view('Front::partials.filters.filters', [
        'post_object' => $post_object,
        'objects' => $objects,
        'mobile' => $mobile,
    ])->render();
}


function ordenarXdata($a, $b) {
	if (count(explode(":", $a->hora)) > 1) {
		$hA = intval(explode(":", $a->hora)[0]);
		$mA = intval(explode(":", $a->hora)[1]);
	} else return 0;
	
	if (count(explode(":", $b->hora)) > 1) {
		$hB = intval(explode(":", $b->hora)[0]);
		$mB = intval(explode(":", $b->hora)[1]);
	} else return 0;

    if ($hA < $hB || ($hA == $hB && $mA < $mB)) return -1;
    else if ($hA == $hB && $mA == $mB) return 0;
    else return 1;
}

function groupCustomList($post, $fieldStatic, $fieldCustom) {
    $list1 = $post->get_field($fieldStatic);
    $list2 = $post->get_field($fieldCustom);

    if ($list1 && count($list1) && !is_array($list1)) {
        $list1->load(['translations']);
    }

    if ($list2 && count($list2) && !is_array($list2)) {
        $list2->load(['translations']);
    }

    $result = [];

    // Convertim la Llista que no es un repeater en un Repeater Like
    if ( is_array($list2) || is_object($list2) ) {
        foreach($list2 as $l2item) {
            $result[] = ["item" => ["value" => $l2item->title]];
        }
    }

    if ( is_array($list1) || is_object($list1) ) {
        $list1 = array_map('convertToRepeater', $list1);
        $result = array_merge($result, $list1);
    }

    return $result;
}

function convertToRepeater($item) {
    return [ 'item' => reset($item) ];
}

function obtenirOfertaBarata($retiro) {
    $ofertes = $retiro->get_field('ofertas-retiro');
    if ($ofertes) {
        $ofertes = collect($ofertes)->filter(function($element) {
            return is_object($element['habitacion']['value']);
        });
        
        $barata = $ofertes->first();
        foreach($ofertes as $oferta) {
            if (obtenirPreuAmbDescompteOferta($oferta) < obtenirPreuAmbDescompteOferta($barata)) {
                $barata = $oferta;
            }
        }

        return $barata;
    }
    return null;
}

function obtenirPreuAmbDescompteOferta($oferta) {
    if (!is_numeric($oferta['precio']['value'])) {
        return null;
    }

    if ($oferta['descuento']['value'] != '' && $oferta['fecha-limite']['value'] != '') {
        $data = Carbon::createFromFormat('d/m/Y', "31/12/2099");
        if ($oferta['fecha-limite']['value'] != "") {
            $data = convertDate($oferta['fecha-limite']['value']);
        }
        if ($data && $data->gte(new Carbon()) && is_numeric($oferta['descuento']['value'])) {
            return $oferta['precio']['value'] - $oferta['descuento']['value'];
        }
    }

    return $oferta['precio']['value'];
}

function obtenirProximesDates($retiro) {
    $calendari = $retiro->get_field('calendario');
    if ($calendari && isset($calendari->calendari)) {
        $calendari = collect($calendari->calendari)->chunk($calendari->duracion);
        $dies = $calendari->map(function($e) {
            return $e->map(function($e1) {
                try {
					return Date::parse($e1);
				} catch(\Exception $ex) {}
            });
        });

        return $dies->filter(function($e) {
            if ($e->first()) {
                return $e->first() > Date::now() || $e->first()->format('d/m/Y') == Date::now()->format('d/m/Y');
            }
        });
    }

    return null;
}


function get_transactions() {
    return Transaction::where('user_id', Auth::id())->ordered();
}

function renderRetiroHome($retiro, $mobile = false, $owl = false) {
    $moneda = ($retiro->author && $retiro->author->get_field('moneda')) ? $retiro->author->get_field('moneda') : null;

    return view("Front::partials.retiro", compact('retiro', 'mobile', 'moneda', 'owl'));
}


function differenceBetweenTwoCoordinates($coordsA, $coordsB) {
    $aux = explode(',', $coordsA);
    $latA = $aux[0];
    $lngA = $aux[1];

    $aux = explode(',', $coordsB);
    $latB = $aux[0];
    $lngB = $aux[1];
    return vincentyGreatCircleDistance($latA, $lngA, $latB, $lngB);
}

/**
 * OBTENIR LA DISTÀNCIA EN METRES DONADES DOS COORDENADES
 * 
 * 
 * Calculates the great-circle distance between two points, with
 * the Vincenty formula.
 * @param float $latitudeFrom Latitude of start point in [deg decimal]
 * @param float $longitudeFrom Longitude of start point in [deg decimal]
 * @param float $latitudeTo Latitude of target point in [deg decimal]
 * @param float $longitudeTo Longitude of target point in [deg decimal]
 * @param float $earthRadius Mean earth radius in [m]
 * @return float Distance between points in [m] (same as earthRadius)
 */
function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);
  
    $lonDelta = $lonTo - $lonFrom;
    $a = pow(cos($latTo) * sin($lonDelta), 2) +
      pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
    $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
  
    $angle = atan2(sqrt($a), $b);
    return $angle * $earthRadius;
  }


if (! function_exists('getAfterLoginUrl')) {

    /**
     * Exchange currency from Euros (€)
     *
     * @return Application|mixed
     */
    function get_after_login_url() {
        $fallbackPlaceholder = request()->route()->parameter('fallbackPlaceholder');

        if ($fallbackPlaceholder) {
            return url('/') . request()->route()->parameter('locale') . '/' . $fallbackPlaceholder;
        }
        
        $post_name = request()->route()->parameter('post_name');
        
        if ($post_name) {
            $segments = request()->segments();
            
            if (count($segments) > 1 && $segments[0] == app()->getLocale()) {
                $post = get_post($post_name, $segments[1]);
                
                if ($post) {
                    return $post->get_url();
                }
            }
        }

        if (request()->isMethod('POST')) {
            return route('index', ['locale' => app()->getLocale()]);
        }
    }
}


if (! function_exists('userCurrency')) {
    function userCurrency($iso = false) {
        $monedas = cacheable(PostsRepository::class)->getMonedas();

        $currentMonedaId = request()->cookie('etr_currency', HeaderComposer::EUR_CURRENCY_ID);

        if (!$iso) {
            return $monedas->where('id', $currentMonedaId)->first() ? $monedas->where('id', $currentMonedaId)->first()->get_field('short') : '€';
        }

        return $monedas->where('id', $currentMonedaId)->first() ? $monedas->where('id', $currentMonedaId)->first()->get_field('iso-code') : 'EUR';
    }
}


if (! function_exists('toUserCurrency')) {

    /**
     * Exchange currency to User's currency
     *
     * @param $isoCodeFrom
     * @param $value
     * @param bool $withSymbol
     * @param null $datetime
     * @return Application|mixed
     */
    function toUserCurrency($isoCodeFrom, $value, $withSymbol = true, $datetime = null) {
        $value = toEur($isoCodeFrom, $value, $datetime);

        $monedas = cacheable(PostsRepository::class)->getMonedas();

        $currentMonedaId = request()->cookie('etr_currency', HeaderComposer::EUR_CURRENCY_ID);

        $currentMoneda = $monedas->where('id', $currentMonedaId)->first();

        $return = fromEur($currentMoneda->get_field('iso-code'), $value, $datetime);

        if ($withSymbol) {
            $return = $currentMoneda->get_field('short') . ' ' . format_money($return);
        }

        return $return;
    }
}


if (! function_exists('fromUserCurrency')) {

    /**
     * Exchange currency from User's currency
     *
     * @param $isoCodeTo
     * @param $value
     * @param bool $withSymbol
     * @param null $datetime
     * @return Application|mixed|string
     */
    function fromUserCurrency($isoCodeTo, $value, $withSymbol = true, $datetime = null) {
        $monedas = cacheable(PostsRepository::class)->getMonedas();

        $currentMonedaId = request()->cookie('etr_currency', HeaderComposer::EUR_CURRENCY_ID);

        $currentMoneda = $monedas->where('id', $currentMonedaId)->first();

        $value = toEur($currentMoneda->get_field('iso-code'), $value, $datetime);

        $return = fromEur($isoCodeTo, $value, $datetime);

        if ($withSymbol) {
            $return = $currentMoneda->get_field('short') . ' ' . format_money($return);
        }

        return $return;
    }
}


if (! function_exists('filterRetirosByDates')) {

    /**
     * Filter retiros by calendar date
     *
     * @param Collection $retiros
     * @return Collection
     */
    function filterRetirosByDates(Collection $retiros) {
        return $retiros->filter(function ($retiro) {
            // Obtenim les proximes dates valides
            $days = null;
            if ( !is_string($retiro) ) {
                $days = obtenirProximesDates($retiro);
            }

            return $days && $days->isNotEmpty();
        });
    }
}


if (! function_exists('isFavorite')) {

    /**
     * Checks if post is favourite
     *
     * @param $post_id
     * @return bool
     */
    function isFavorite($post_id) {
        if (auth()->check()) {
            $userFavorites = cacheable(PostsRepository::class)->userFavorites(auth()->user()->id);
            return $userFavorites->count() && $userFavorites->where('post_id', $post_id)->first() != null;
        }

        $key = 'etr_favorites';

        $postIds = collect(json_decode(request()->cookie($key, json_encode([])), true));

        return $postIds->contains(function ($value, $key) use ($post_id) {
            return $value == $post_id;
        });
    }
}


if (! function_exists('isUsersNewsletter')) {

    /**
     * Si la url coincideix amb alguna de les URLs especificades pel client 
     * com a Newsletter de Usuari retorna cert
     *
     * @param String $key
     * @return boolean
     */
    function isUsersNewsletter($key) {
        return in_array($key, [
                route('index'), // Home
                get_page_url('home'), // Home x2
                Post::get_archive_link('retiros'), // Retiros
                get_page_url('dharmas-1') // dharmas-1
            ]);
    }
}

if (! function_exists('isOrganizadoresNewsletter')) {
    /**
     * Retorna cert si la url donada coincideix amb alguna URL de les
     * especificades pel Client
     *
     * @param String $key
     * @return boolean
     */
    function isOrganizadoresNewsletter($key) {
        return in_array($key, [
            get_page_url('ayuda-organizadores'), // Soporte Ayuda Organizadores
            get_page_url('espacios-y-lugares-de-retiro'), // Espacios Home
            get_page_url('promociona-tu-espacio'), // Promociona tu Espacio
            get_page_url('promociona-tu-retiro') // Promociona tu Retiro
        ]) || like_match('%lista-espacios%', $key);
    }
}

if (! function_exists('print_location')) {

    /**
     * Returns the string in the following format: Administravite 2 / Administravite 1 / Country
     *
     * @param Gmaps_Location $location
     * @return String
     */
    function print_location($location, $levels = []) {
        if ($location) {
            if (isset($location->object) && $location->object) {
                $object = json_decode($location->object);
                $components = collect($object->address_components);
                $components = $components->map(function($e) use ($levels) {
                    if (count($levels) > 0) {
                        $tipus = collect($e->types);
                        $tipus = $tipus->intersect($levels);
                        if ($tipus->count()) return $e->long_name;
                    }
                });
                return trim($components->implode(', '), ', ');
            } else if (isset($location->adreca)) {
                return $location->adreca;
            }
        }
        return "";
    }
}


if (! function_exists('get_user_favourites')) {
    function get_user_favourites() {
        if (auth()->check()) {
            $favoritos = Favourite::where('user_id', Auth::id())->get()->map(function ($favorito) {
                return $favorito->post_id;
            });
        } else {
            $key = 'etr_favorites';
            $favoritos = collect(json_decode(request()->cookie($key, json_encode([])), true));
        }
    
        $retiros = Post::withTranslation()->whereIn('id', $favoritos)->where('status', 'publish')->paginate($favoritos->count());
        
        return [
            'retiros' => $retiros,
            'totalRetiros' => $retiros->total()
        ];
    }
}