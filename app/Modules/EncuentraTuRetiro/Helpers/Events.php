<?php

use App\User;
use Illuminate\Support\Facades\Event;
use App\Modules\EncuentraTuRetiro\Models\Transaction;

Event::listen('add_dharmas', function(User $user, $amount, $comments = '') {
    $user->dharmas += $amount;
    $user->save();

    event('add_transaction', [$user, $amount, 'dharmas', $comments]);
});

Event::listen('add_transaction', function(User $user, $amount, $type, $comments = '') {
    Transaction::create([
        'user_id' => $user->id,
        'amount' => $amount,
        'type' => $type,
        'comments' => $comments,
    ]);
});

Event::listen('store_rating', function($user_id, $post_id) {
    $user = User::find($user_id);

    $ratings = do_action('get_ratings_by_user_and_post', [$user_id, $post_id]);

    $lastRating = $ratings->sortByDesc('updated_at')->first();

    if ($ratings->count() == 6 && $lastRating->created_at == $lastRating->updated_at) {
        event('add_dharmas', [$user, 300, 'Al hacer una valoración de tu experiencia de retiro']);
    }
});