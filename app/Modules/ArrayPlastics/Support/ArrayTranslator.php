<?php
/**
 * Created by PhpStorm.
 * User: sergisolsona
 * Date: 29/08/2019
 * Time: 18:07
 */

namespace App\Modules\ArrayPlastics\Support;

use App\Support\Translator;

class ArrayTranslator extends Translator
{

    /**
     * Get the translation for a given key from the JSON translation files.
     *
     * @param string $key
     * @param array $replace
     * @param null $locale
     * @return string|array|null
     */
    public function getFromJson($key, array $replace = [], $locale = null)
    {
        if (!$locale) {
            $locale = request('user_lang');
        }

        if (!$locale) {
            $user = auth()->user();

            if ($user) {
                $locale = $user->get_field('idioma');
            }
        }

        if (!$locale) {
            $locale = request()->cookie('user_lang');
        }

        if (!$locale) {
            $lang = substr(request()->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
            $acceptLang = ['ca', 'es', 'en'];
            $locale = in_array($lang, $acceptLang) ? $lang : 'en';
        }

        return parent::getFromJson($key, $replace, $locale);
    }
}