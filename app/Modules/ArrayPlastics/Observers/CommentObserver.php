<?php


namespace App\Modules\ArrayPlastics\Observers;


use App\Models\Mensaje;
use App\Modules\ArrayPlastics\Models\UserDocumentComment;
use App\Notifications\EmailNotification;
use App\User;
use stdClass;

class CommentObserver
{

    public function creating(UserDocumentComment $comment)
    {
        if (isProEnv()) {
            $currentLang = app()->getLocale();

            $user = $comment->user;
            $document = $comment->post;
            $category = $document->get_field('categoria');

            $mensaje = new Mensaje();
            $mensaje->user_id = 0;
            $mensaje->mensaje = '';
            $mensaje->fitxer = '';
            $mensaje->email = '';
            $mensaje->name = '';
            $mensaje->type = 'email';
            $mensaje->params = json_encode([
                'username' => $user->fullname(),
                'filename' => $document->title,
                'client_id' => $user->id,
                'category' => $category ? $category->title : 'N/A'
            ]);

            $emailInfo = env('EMAIL_INFO');

            if (!$emailInfo) {
                dd('Error: falta variable EMAIL_INFO');
            }

            $from = User::where('email', $emailInfo)->first();
            $author = $document->author;

            if ($category && $responsible = $category->get_field('responsable')) {
                $author = $responsible;
            }

            $userToNotify = $comment->userDocument->user;

            if ($user->id == $comment->userDocument->user->id) {
                $userToNotify = $author;
            }

            $params = new stdClass();
            $params->gestor = $author;
            $params->usuario = $author;
            $params->mensaje = $mensaje;
            $params->from = $from;
            $params->email_id = 3;

            $lang = $userToNotify->get_field('idioma');

            if ($lang) {
                app()->setLocale($lang);
            }

            $notificacion = new EmailNotification($params);
            if ($notificacion->check_email()) {
                $author->notify($notificacion);
            }

            if (app()->getLocale() != $currentLang) {
                app()->setLocale($currentLang);
            }
        }
    }

    public function created(UserDocumentComment $comment)
    {

    }

    public function saving(UserDocumentComment $comment)
    {
    }


    public function updating(UserDocumentComment $comment)
    {
    }


    public function saved(UserDocumentComment $comment)
    {
    }
}