<?php

namespace App\Modules\ArrayPlastics\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Media;

class PostTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'description', 'post_name', 'media_id'];

    public function media() {
        return $this->belongsTo(Media::class, 'media_id', 'id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function get_thumbnail_url($size = '') {
        if ( empty($this->media) ) return null;
        else return $this->media->get_thumbnail_url($size);
    }
}
