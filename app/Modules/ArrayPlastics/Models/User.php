<?php

namespace App\Modules\ArrayPlastics\Models;

use App\Modules\ArrayPlastics\Notifications\MyResetPassword;
use App\User as AuxUser;

class User extends AuxUser
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'first_login'
        ];

    public function groups() {
        return $this->belongsToMany(Group::class)
                        ->withPivot('gestor')
                        ->withTimestamps();
    }

    public function groupsGestionats() {
        return $this->groups()->wherePivot("gestor", 1);
    }

    public function categories() {
        return $this->posts()->ofType('categoria');
    }


    public function sendPasswordResetNotification($token)
    {
        $notificacio = new MyResetPassword($token);
        $notificacio->user_fullname = $this->fullname();
        $this->notify($notificacio);
    }
}