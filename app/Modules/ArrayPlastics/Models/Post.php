<?php


namespace App\Modules\ArrayPlastics\Models;


use App\Models\CustomPost;
use App\Models\CustomPostTranslation;
use App\Models\Traits\Categorizable;
use App\Models\Traits\HasCustomFields;
use App\User;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Date\Date;
use App\Post as PostCore;

class Post extends PostCore
{
    use SoftDeletes, HasCustomFields, Translatable, Categorizable;

    const ROL_LEVEL_LIMIT_REVISIO = 50;

    public $translatedAttributes = ['title', 'description', 'post_name'];

    protected $useTranslationFallback = true;

    protected $fillable = ['type', 'status', 'template', 'parent_id', 'author_id', 'pending_id', 'order', 'creator_id'];

    protected $dates = ['deleted_at'];

    /*protected $with = ['translations'];*/

    protected $attributes = [
        'parent_id' => 0,
    ];

    private $urls_cached;

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->defaultLocale = \App::getLocale();
    }

    // Revisar on s'utilitza el metode Fullname al Select CF
    public function __call($name, $arguments) {
        if ($name != 'fullname') return parent::__call($name, $arguments);
        else {
            // dd($this);
        }
    }

    public function author() {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    public function parent() {
        return $this->belongsTo(\App\Post::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Post::class, 'parent_id');
    }

    public function customPostTranslations()
    {
        return $this->hasMany(CustomPostTranslation::class, 'post_type', 'type');
    }


    public function get_url($lang = null)
    {
        if ( $lang == null ) $lang = \App::getLocale();

        if (isset($this->urls_cached) && array_key_exists($lang, $this->urls_cached)) {
            return $this->urls_cached[$lang];
        }

        if (!isset($this->urls_cached)) {
            $this->urls_cached = [];
        }

        $custom_post = $this->customPostTranslations->where('locale', $lang)->first();
        if ($custom_post) {
            if (! $trans = $this->getTranslation($lang)) return 'javascript:void(0)';
            $postName = $this->getTranslation($lang)->post_name;

            if (!$postName) {
                $postName = $this->getTranslation()->post_name;
            }

            $this->urls_cached[$lang] = url('/') . '/' . $lang . '/' . $custom_post->customPost->translate($lang)->post_type_plural . '/' . $postName;
        } else {
            $this->urls_cached[$lang] =  url('/') . '/' . $lang . '/' . $this->getTranslation( \App::getLocale(), true )->post_name;
        }

        return $this->urls_cached[$lang];
    }

    public static function get_url_by_post_id($post_id, $lang = null)
    {
        if ( $lang == null ) $lang = \App::getLocale();

        if ($post_id instanceof Post) {
            $post = $post_id;
        } else {
            $post = Post::whereTranslation('post_id', $post_id, $lang)->first();
        }

        if ($post) {
            $custom_post_original = CustomPost::whereTranslation('post_type', $post->type)->first();

            if ($custom_post_original) {
                $custom_post = CustomPost::whereTranslation('custom_post_id', $custom_post_original->id, $lang)->first();
                if ( $custom_post && !empty($post->translate($lang)->post_name) ) {
                    return url('/') . str_replace(
                            '//', '/', '/' . $lang . '/' . $custom_post->translate($lang)->post_type_plural . '/' . $post->translate($lang)->post_name
                        );
                }
            } else if ( !empty($post->translate($lang)->post_name) ) {
                return url('/') . str_replace(
                        '//', '/', '/' . $lang . '/' . $post->translate($lang)->post_name
                    );
            }
        }
        else return '';
    }

    public static function get_url_by_post_name($post_name, $lang = null)
    {
        if ($lang == null) {
            $lang = \App::getLocale();
        }

        $post = Post::whereTranslation('post_name', $post_name, $lang)->first();

        if ($post) {
            return self::get_url_by_post_id($post, $lang);
        } else {
            return '';
        }
    }

    public static function get_archive_link($post_type_plural, $lang = null) {
        if ( $lang == null ) $lang = \App::getLocale();

        $custom_post_original = CustomPost::whereTranslation('post_type_plural', $post_type_plural)->first();

        if ( $custom_post_original ) {
            $custom_post = CustomPost::whereTranslation('custom_post_id', $custom_post_original->id, $lang)->first();

            if ( $custom_post ) {
                return url('/') . str_replace(
                        '//', '/', '/' . $lang . '/' . $custom_post->translate($lang)->post_type_plural
                    );
            }
        }
    }

    public function get_parent() {
        if ($this->parent_id) {
            return Post::where('id', $this->parent_id)->first();
        } else {
            return null;
        }
    }

    /* POST Formacio */
    public static function formacio_vista($id_form) {
        $user_id = Auth::id();

        $vista = DB::table('formacions_usuaris')->where([
            ['user_id', $user_id],
            ['formacio_id', $id_form]
        ])->get();

        $res = (count($vista) > 0) ? 'checked' : '';

        return $res;
    }
    /* FI FORMACIO*/

    public static function get_posts($params) {
        return self::where($params)->get();
    }

    public function media() {
        return $this->translate(true)->media;
    }

    /**
     * Scopes
     */
    public function scopeOfName($query, $post_name) {
        return $query->whereTranslation('post_name', $post_name);
    }

    public function scopePublish($query) {
        return $query->where('posts.status', 'publish');
    }

    public function scopePending($query) {
        return $query->where('posts.status', 'pending');
    }

    public function scopeDraft($query) {
        return $query->where('posts.status', 'draft');
    }

    public function scopeOwner($query, $user) {
        if ($user->isAdmin()) return $query;

        return $query->where('posts.author_id', $user->id);
    }

    public function scopeOfType($query, $type)
    {
        return $query->where('posts.type', $type);
    }

    public function scopeOfParent($query, $parent_id)
    {
        return $query->where('posts.parent_id', $parent_id);
    }

    public function scopeOfStatus($query, $status)
    {
        if ($status == 'trash') return $query->onlyTrashed();
        return $query->where('posts.status', $status);
    }

    public function getCreatedAtAttribute($value)
    {
        return new Date($value);
    }
}