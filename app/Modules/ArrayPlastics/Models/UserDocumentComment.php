<?php


namespace App\Modules\ArrayPlastics\Models;


use App\Post;
use Illuminate\Database\Eloquent\Model;

class UserDocumentComment extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }


    public function userDocument()
    {
        return $this->belongsTo(UserDocument::class);
    }


    public function post()
    {
        return $this->userDocument->post();
    }
}