<?php

namespace App\Modules\ArrayPlastics\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['title'];

    function users() {
        return $this->belongsToMany(User::class)
                ->withPivot('gestor')
                ->withTimestamps();
    }

    function gestors() {
        return $this->users()->wherePivot("gestor", 1);
    }

    function clients() {
        return $this->users()->wherePivot("gestor", 0);
    }

    function documents() {
        return $this->belongsToMany(Post::class, 'group_documents');
    }

    function categories() {
        return $this->belongsToMany(Post::class);
    }

    function esGestor(User $user) {
        return $this->gestors()->where("user_id", $user->id)->exists();
    }

    function noEsGestor(User $user) {
        return !$this->esGestor($user);
    }

    function has(User $user) {
        return $this->users->where('id', $user->id)->isNotEmpty();
    }
}
