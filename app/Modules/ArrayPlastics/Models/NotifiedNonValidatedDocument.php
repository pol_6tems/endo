<?php


namespace App\Modules\ArrayPlastics\Models;


use App\Post;
use Illuminate\Database\Eloquent\Model;

class NotifiedNonValidatedDocument extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];


    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}