<?php


namespace App\Modules\ArrayPlastics\Models;


use App\Models\Media;
use App\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDocument extends Model
{
    use SoftDeletes;
    
    const DOC_TYPE_WEB_VALIDABLE = 'validable';
    const DOC_TYPE_SIGNATURE = 'signature';

    private $statuses = [
        'pendent' => ['key' => 'pendent', 'name' => 'Pendent'],
        'revisio' => ['key' => 'revisio', 'name' => 'Revisio'],
        'validat' => ['key' => 'validat', 'name' => 'Validat'],
        'no-acceptat' => ['key' => 'no-acceptat', 'name' => 'No acceptat']
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dates = ['validation_date', 'max_validation_date', 'expiry_date', 'deleted_at'];

    public function isSignature()
    {
        return $this->type == self::DOC_TYPE_SIGNATURE;
    }

    public function isValidable()
    {
        return $this->type == self::DOC_TYPE_WEB_VALIDABLE;
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }


    public function validator()
    {
        return $this->belongsTo(\App\User::class, 'validator_id');
    }


    public function comments()
    {
        return $this->hasMany(UserDocumentComment::class);
    }

    public function author() {
        return $this->post()->author();
    }


    public function post()
    {
        return $this->belongsTo(Post::class)->withTrashed();
    }


    public function getStatuses()
    {
        return $this->statuses;
    }


    public function get_field($field_name, $lang = null, $args = null)
    {
        return $this->post ? $this->post->get_field($field_name, $lang, $args) : null;
    }


    public function areAllCommentsViewed()
    {
        $user = auth()->user();

        if ($user->id == $this->user_id) {
            $pendingComments = $this->comments->filter(function ($comment) {
                return $comment->user_id != $this->user_id && !$comment->viewed;
            });
        } else {
            $pendingComments = $this->comments->filter(function ($comment) {
                return $comment->user_id == $this->user_id && !$comment->viewed;
            });
        }

        return $pendingComments->count() == 0;
    }


    public function getValidationFileMedias()
    {
        if ($this->validation_files) {
            if ($imatges = (array)json_decode($this->validation_files)) {
                return Media::whereIn('id', $imatges)->get()->sortBy(function ($media) use ($imatges) {
                    return array_search($media->id, $imatges);
                })->all();
            }
        }

        return [];
    }


    public function isValidationFile($id)
    {
        if ($this->validation_files) {
            if ($imatges = (array)json_decode($this->validation_files)) {
                return in_array($id, $imatges);
            }
        }

        return false;
    }


    public function getAllFiles()
    {
        $files = $this->post->get_field('fitxers');

        if ($this->validation_files) {
            $files = array_merge($files, $this->getValidationFileMedias());
        }
        /*dd($files);*/
        return $files;
    }
}