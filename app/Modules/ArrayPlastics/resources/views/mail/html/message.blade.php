@php($localize = isset($localize) ? $localize : 'es')
@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer', ['localize' => $localize])
            @isset($gestor)
                {!! __("Enviat per <strong>:gestor_name</strong> - <a style=\"font-style: italic;\" href=\"mailto::gestor_email\">:gestor_email</a>", ["gestor_name" => $gestor->fullname(), "gestor_email" => $gestor->email], $localize) !!}
                <br>
            @endif
            © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
