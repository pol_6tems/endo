<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="{{ asset(config('front_theme')."images/logo-array-plastics-mini.png") }}" />
                </td>
                <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: #AEAEAE; font-size: 12px; text-align: left;">
                        @Lang("Pots veure la política de privacitat a <a href=\":url_rgpd\">RGPD</a>, pots veure les <a href=\":url_instruccions\">instruccions</a>.", ["url_rgpd" => get_page_url("rgpd"), "url_instruccions" => get_page_url("instruccions")], $localize)
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="100" class="content-cell" align="center">
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                </td>
            </tr>
        </table>
    </td>
</tr>
