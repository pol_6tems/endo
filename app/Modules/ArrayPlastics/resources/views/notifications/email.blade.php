@php($localize = isset($localize) ? $localize : 'es')
@component('mail::message',  isset($gestor) ? ["gestor" => $gestor, 'localize' => $localize] : [])
{{-- Greeting --}}
@if (! empty($greeting))
# {!! $greeting !!}
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{!! nl2br($line) !!}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color, 'localize' => $localize])
{!! $actionText !!}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{!! nl2br($line) !!}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{!! $salutation !!}
@else
Regards,<br>{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy', ['localize' => $localize])
@Lang('mail_subcopy_text', [], $localize) [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent