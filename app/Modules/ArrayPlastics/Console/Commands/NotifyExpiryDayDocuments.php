<?php

namespace App\Modules\ArrayPlastics\Console\Commands;

use App\Post;
use App\User;
use Illuminate\Support\Facades\App;
use stdClass;
use Carbon\Carbon;
use App\Models\Mensaje;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Notifications\EmailNotification;
use App\Modules\ArrayPlastics\Models\UserDocument;
use App\Modules\ArrayPlastics\Notifications\ArrayNotification;

class NotifyExpiryDayDocuments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:expiry-day-documents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify users it\'s the expiry date for documents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        url()->defaults(['locale' => app()->getLocale()]);

        $today = Carbon::today();

        $documents = UserDocument::with(['post.metas.customField'])
            ->where(function ($query) {
                $query->whereNotNull('max_validation_date')
                    ->orWhereNotNull('expiry_date');
            })
            ->get();

        $emailInfo = env('EMAIL_INFO');

        if (!$emailInfo) {
            dd('Error: falta variable EMAIL_INFO');
        }

        $from = User::where('email', $emailInfo)->first();
        // $from = User::where('email', 'pol@6tems.com')->first();

        foreach ($documents as $document) {
            if (is_null($document->post)) continue;
            $this->info('Doc Id: ' . $document->post->id);

            if ($document->validation_status && $document->validation_status == 'validat') {
                $this->info('Validated document, continue..');
                continue;
            }

            if ($document->post->status != 'publish') {
                $this->info('Trashed document, continue..');
                continue;
            }

            $docExpiryDate = $document->max_validation_date;

            if (!$docExpiryDate) {
                $docExpiryDate = $document->expiry_date;
            }

            if (!$docExpiryDate) {
                $this->warn('No expiry date');
                continue;
            }

            $docExpiryDate = $docExpiryDate->startOfDay();

            if ($docExpiryDate != $today) {
                $this->info('No expires today, continue..');
                continue;
            }

            $validators = [$document->user];

            $emailId = null;

            if ($document->max_validation_date) {
                $emailId = 10;
                /*$emailParams = json_encode([
                    'document' => $document->post->title,
                    'validacio' => $document->max_validation_date->format('d/m/Y'),
                    'caducitat' => ($document->expiry_date) ? $document->expiry_date->format('d/m/Y') : __('No Caduca'),
                    'estat' => __(ucwords($document->validation_status)),
                ]);*/
            } elseif ($document->expiry_date) {
                continue;
                /*
                $emailId = 11;
                $emailParams = json_encode([
                    'document' => $document->post->title
                ]);*/
            }

            if (isset($emailId) && $emailId) {
                foreach ($validators as $validator) {
                    $mensaje = new Mensaje();
                    $mensaje->user_id = 0;
                    $mensaje->mensaje = '';
                    $mensaje->fitxer = '';
                    $mensaje->email = '';
                    $mensaje->name = '';
                    $mensaje->type = 'email';

                    $lang = $validator->get_field('idioma');
                    if ($lang) {
                        app()->setLocale($lang);
                    }

                    if ($document->max_validation_date) {
                        $emailParams = json_encode([
                            'document' => $document->post->title,
                            'validacio' => $document->max_validation_date->format('d/m/Y'),
                            'caducitat' => ($document->expiry_date) ? $document->expiry_date->format('d/m/Y') : __('No Caduca', [], $lang),
                            'estat' => __(ucwords($document->validation_status), [], $lang),
                        ]);
                    }

                    $mensaje->params = $emailParams;

                    $params = new stdClass();
                    $params->gestor = $document->post->author;
                    $params->usuario = $validator;
                    $params->mensaje = $mensaje;
                    $params->from = $from;
                    $params->email_id = $emailId;

                    $notificacion = new ArrayNotification($params);
                    if ($notificacion->check_email()) {
                        $validator->notify($notificacion);
                        sleep(1);
                    }
                }
            }
        }
    }
}
