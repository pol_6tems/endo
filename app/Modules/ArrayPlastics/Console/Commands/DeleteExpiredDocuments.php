<?php

namespace App\Modules\ArrayPlastics\Console\Commands;

use App\Post;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Modules\ArrayPlastics\Models\UserDocument;

class DeleteExpiredDocuments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:expired-documents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes expired documents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();

        $documents = UserDocument::with(['post.metas.customField'])
            ->whereNotNull('expiry_date')
            ->where('expiry_date', '<', $now)
            ->get();

        foreach ($documents as $document) {
            if (!$document->expiry_date) {
                $this->warn('No expiry date');
                continue;
            }

            $docExpiryDate = $document->expiry_date->startOfDay();

            if ($docExpiryDate > $now) {
                $this->info('Not expired date, continue..');
                continue;
            }

            if ($fitxers = $document->get_field('fitxers')) {
                foreach($fitxers as $fitxer) {
                    $this->info('Deleting file');

                    $fitxer->remove_thumbnails();
                    $fitxer->delete();
                }
            } else {
                $this->warn('No file to delete');
            }

            $document->post->update(['status' => 'trash']);
            $document->post->delete();

            $document->update(['validation_status' => 'trash']);
            $document->delete();

            $this->info($document->title . ' document deleted');
        }

        $this->info('Done!');
    }
}
