<?php

namespace App\Modules\ArrayPlastics\Console\Commands;

use App\Post;
use App\User;
use stdClass;
use Carbon\Carbon;
use App\Models\Mensaje;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Notifications\EmailNotification;
use App\Modules\ArrayPlastics\Models\UserDocument;
use App\Modules\ArrayPlastics\Notifications\ArrayNotification;
use App\Modules\ArrayPlastics\Models\NotifiedNonValidatedDocument;

class NotifyExpiredDocuments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:expired-documents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify users expired documents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        url()->defaults(['locale' => app()->getLocale()]);

        $now = Carbon::now();

        $documents = UserDocument::with(['post.metas.customField'])
            ->whereNotNull('max_validation_date')
            ->where('max_validation_date', '<', $now)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('notified_non_validated_documents')
                    ->whereRaw('notified_non_validated_documents.post_id = user_documents.post_id');
            })
            ->get();

        $emailInfo = env('EMAIL_INFO');

        if (!$emailInfo) {
            dd('Error: falta variable EMAIL_INFO');
        }

        $from = User::where('email', $emailInfo)->first();
        // $from = User::where('email', 'pol@6tems.com')->first();

        foreach ($documents as $document) {
            $this->info('Doc Id: ' . $document->post->id);

            if ($document->validation_status && $document->validation_status == 'validat') {
                $this->info('Validated document, continue..');
                continue;
            }

            if (!$document->max_validation_date) {
                $this->warn('No expiry date');
                continue;
            }

            if ($document->post->status != 'publish') {
                $this->info('Trashed document, continue..');
                continue;
            }

            $docExpiryDate = $document->max_validation_date->startOfDay();

            if ($docExpiryDate > $now) {
                $this->info('Not expired date, continue..');
                continue;
            }

            $validators = [$document->user];

            foreach ($validators as $validator) {
                $mensaje = new Mensaje();
                $mensaje->user_id = 0;
                $mensaje->mensaje = '';
                $mensaje->fitxer = '';
                $mensaje->email = '';
                $mensaje->name = '';
                $mensaje->type = 'email';

                $lang = $validator->get_field('idioma');
                if ($lang) {
                    app()->setLocale($lang);
                }

                $mensaje->params = json_encode([
                    'date' => $document->max_validation_date->format('d/m/Y'),
                    'document' => $document->post->title,
                    'caducitat' => ($document->expiry_date) ? $document->expiry_date->format('d/m/Y') : __('No Caduca', [], $lang),
                    'estat' => __(ucwords($document->validation_status), [], $lang),
                ]);

                $params = new stdClass();
                $params->gestor = $document->post->author;
                $params->usuario = $validator;
                $params->mensaje = $mensaje;
                $params->from = $from;
                $params->email_id = 12;

                $notificacion = new ArrayNotification($params);
                if ($notificacion->check_email()) {
                    $validator->notify($notificacion);
                    sleep(1);
                }
            }

            NotifiedNonValidatedDocument::firstOrCreate([
                'post_id' => $document->id
            ]);
        }
    }
}
