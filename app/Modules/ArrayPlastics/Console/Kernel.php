<?php
/**
 * Created by PhpStorm.
 * User: sergisolsona
 * Date: 12/06/2019
 * Time: 11:58
 */

namespace App\Modules\ArrayPlastics\Console;

use Illuminate\Console\Scheduling\Schedule;
use App\Modules\ArrayPlastics\Console\Commands\NotifyNewDocuments;
use App\Modules\ArrayPlastics\Console\Commands\DeleteExpiredDocuments;
use App\Modules\ArrayPlastics\Console\Commands\NotifyExpiredDocuments;
use App\Modules\ArrayPlastics\Console\Commands\NotifyLastDayDocuments;
use App\Modules\ArrayPlastics\Console\Commands\NotifyExpiryDayDocuments;

class Kernel
{

    /**
     * The Artisan commands provided by your module.
     *
     * @var array
     */
    protected $commands = [
        DeleteExpiredDocuments::class,
        NotifyNewDocuments::class,
        NotifyLastDayDocuments::class,
        NotifyExpiryDayDocuments::class,
        NotifyExpiredDocuments::class,
    ];


    public function getCommands()
    {
        return $this->commands;
    }


    /**
     * Define the module's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule)
    {
        $schedule->command('delete:expired-documents')->dailyAt('00:30');

        $schedule->command('notify:new-documents')->weeklyOn(1, '09:00');

        $schedule->command('notify:expiry-day-documents')->dailyAt('09:15');

        $schedule->command('notify:last-day-documents')->dailyAt('09:30');

        // $schedule->command('notify:expired-documents')->dailyAt('09:45');
    }
}