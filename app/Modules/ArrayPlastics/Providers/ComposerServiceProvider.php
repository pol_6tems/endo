<?php


namespace App\Modules\ArrayPlastics\Providers;



use App\Modules\ArrayPlastics\Composers\HeaderComposer;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('Front::partials.header', HeaderComposer::class);
    }


    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {

    }
}