<?php


namespace App\Modules\ArrayPlastics\Providers;


use App\Modules\ArrayPlastics\Models\UserDocumentComment;
use Illuminate\Support\ServiceProvider;
use App\Modules\ArrayPlastics\Middleware\AcceptPolicy;
use App\Modules\ArrayPlastics\Observers\CommentObserver;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        config()->set('auth.providers.users.model', \App\Modules\ArrayPlastics\Models\User::class);
        config()->set('auth.passwords.users.expire', 10800);
        config()->set("mail.markdown.paths", [ app_path('Modules/ArrayPlastics/resources/views/mail') ]);

        UserDocumentComment::observe(CommentObserver::class);

        $router = $this->app['router'];
        $router->aliasMiddleware('accept-policy', AcceptPolicy::class);

        $this->app->register(ArrayTranslationServiceProvider::class);
    }


    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {

    }
}