<?php


namespace App\Modules\ArrayPlastics\Composers;


use App\User;
use Illuminate\View\View;
use App\Models\CustomFieldGroup;
use App\Modules\ArrayPlastics\Repositories\GroupsRepository;

class HeaderComposer
{

    public function compose(View $view)
    {
        $user = auth()->user();

        if ($user->rol->level >= 50) {
            $author = $user;

            if ($author->rol->level < 51 && $author->groups->count()) {
                $groups = $author->groups->filter(function ($group) use ($author) {
                    return $group->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->count();
                })->first();

                if ($groups) {
                    $author = $groups->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->first();
                } else {
                    $author = $author->groups->first()->gestors->first();
                }

                if (!$author) {
                    $author = $user;
                }
            }
            
            /*$clients = cacheable(GroupsRepository::class, 10)->getManagerClients($author);*/
            $clients = app(GroupsRepository::class)->getManagerClients($author);
            
            $view->with(['clients' => $clients]);
        }

        $cf_group = CustomFieldGroup::where('post_type', 'user')->first();
        $fields = $cf_group->fields;

        $params = json_decode($fields->where('name', 'idioma')->first()->params);

        $languagesArray = explode("\r\n", $params->choices);
        $languages = [];

        foreach ($languagesArray as $value) {
            $choice = explode(':', $value);
            $languages[] = [
                'code' => $choice[0],
                'name' => $choice[1]
            ];
        }

        $view->with(['languages' => $languages]);
        $view->with(['locale' => $this->getLocale($user)]);
    }


    private function getLocale(User $user)
    {
        $locale = $user->get_field('idioma');

        if (!$locale) {
            $locale = request()->cookie('user_lang');
        }

        if (!$locale) {
            $lang = substr(request()->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
            $acceptLang = ['ca', 'es', 'en'];
            $locale = in_array($lang, $acceptLang) ? $lang : 'en';
        }

        return $locale;
    }
}