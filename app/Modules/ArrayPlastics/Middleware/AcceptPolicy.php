<?php


namespace App\Modules\ArrayPlastics\Middleware;


use Closure;

class AcceptPolicy
{

    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if (!$user) {
            return redirect()->route('login');
        }

        $acceptPolicy = $user->get_field('accepta-politica');

        if (!$acceptPolicy) {
            return redirect()->route('accept-policy');
        }

        return $next($request);
    }
}