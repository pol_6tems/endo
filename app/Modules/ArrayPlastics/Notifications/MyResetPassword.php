<?php

namespace App\Modules\ArrayPlastics\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword;

class MyResetPassword extends ResetPassword
{
    public $user_fullname;

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting( '')
            ->subject(__('Set the password', [], 'es'))
            ->line(__('Dear :name', ['name' => $this->user_fullname], 'es') . ',')
            ->line(__('reset_password_mail_text1', [], 'es'))
            ->line(__('reset_password_mail_text1b', ["url_instruccions" => get_page_url("instruccions")], 'es'))
            ->line(__('reset_password_mail_text2', [], 'es'))
            ->line('<a href="' . route('password.reset', ['locale' => 'es', $this->token, 'email' => $notifiable->email]) . '">' . __('Set the password', [], 'es') . '</a>')
            ->line(__('reset_password_mail_text3a', [], 'es'))
            ->line(__('reset_password_mail_text3', ['url' => route('index', ['locale' => 'es'])], 'es'))
            ->line(__('reset_password_mail_text4', [], 'es'))
            ->line(__('Kind regards', [], 'es') . ',<br>Array Plàstics, S.L.<br><hr><br>')
            ->line(__('Dear :name', ['name' => $this->user_fullname],'en') . ',')
            ->line(__('reset_password_mail_text1', [], 'en'))
            ->line(__('reset_password_mail_text1b', ["url_instruccions" => get_page_url("instruccions")], 'en'))
            ->line(__('reset_password_mail_text2', [], 'en'))
            ->line('<a href="' . route('password.reset', ['locale' => 'en', $this->token, 'email' => $notifiable->email]) . '">' . __('Set the password', [], 'en') . '</a>')
            ->line(__('reset_password_mail_text3a', [], 'en'))
            ->line(__('reset_password_mail_text3', ['url' => route('index', ['locale' => 'en'])], 'en'))
            ->line(__('reset_password_mail_text4', [], 'en'))
            ->line(__('Kind regards', [], 'en') . ',<br>Array Plàstics, S.L.')
            ->salutation( '')->markdown("ArrayPlastics::notifications.email", [
                'gestor' => auth()->user(),
            ]);

    }
}