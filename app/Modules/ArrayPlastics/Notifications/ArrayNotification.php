<?php

namespace App\Modules\ArrayPlastics\Notifications;

use App\Modules\ArrayPlastics\Models\User;
use App\Notifications\EmailNotification;
use Illuminate\Support\Facades\App;

class ArrayNotification extends EmailNotification
{
    public function toMail($notifiable)
    {
        $mailable = parent::toMail($notifiable)->markdown("ArrayPlastics::notifications.email", [
            'gestor' => $this->params->gestor,
            'localize' => App::getLocale()
        ]);

        $emailInfo = env('EMAIL_INFO');

        if (!$emailInfo) {
            dd('Error: falta variable EMAIL_INFO');
        }

        $from = User::where('email', $emailInfo)->first();

        $mailable->from($from->email, $from->fullname());
        $mailable->replyTo($this->params->gestor->email);
        return $mailable;
    }
}
