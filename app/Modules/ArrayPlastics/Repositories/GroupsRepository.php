<?php


namespace App\Modules\ArrayPlastics\Repositories;


use App\Post;
use Illuminate\Support\Facades\DB;
use App\Modules\ArrayPlastics\Models\User;
use App\Modules\ArrayPlastics\Models\Group;

class GroupsRepository
{

    public function getManagerClients(User $user)
    {
        $groups = $this->getManagerGroups($user);

        $users = $groups->map(function ($item) {
            return $item->users;
        });

        return $users->flatten()->unique("id")->filter(function ($client) {
            return $client->rol->level < 50;
        })->sortBy(function ($user) {
            return $user->fullname();
        }, SORT_NATURAL|SORT_FLAG_CASE);
    }


    public function getManagerClientsAndManagers(User $user)
    {
        $groups = $this->getManagerGroups($user);

        $users = $groups->map(function ($item) {
            return $item->users;
        });

        return $users->flatten()->unique("id")->filter(function ($client) {
            return $client->rol->level <= 51;
        })->sortBy(function ($user) {
            return $user->fullname();
        }, SORT_NATURAL|SORT_FLAG_CASE);
    }


    public function getManagerGroups(User $user)
    {
        return $user->groupsGestionats;
    }


    public function getGroupUsers(Group $group)
    {
        return $group->users;
    }


    public function getGroupDocs(Post $group)
    {
        // TODO: Revisar

        return Post::select(DB::raw('distinct posts.*'))
            ->join('post_meta', 'post_meta.post_id', '=', 'posts.id')
            ->join('custom_fields', 'custom_fields.id', '=', 'post_meta.custom_field_id')
            ->where('posts.type', 'document')
            ->where('custom_fields.name', 'grup-validadors')
            ->where('post_meta.value', $group->id)
            ->get();
    }
}