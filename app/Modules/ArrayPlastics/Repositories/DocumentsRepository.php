<?php


namespace App\Modules\ArrayPlastics\Repositories;


use App\Modules\ArrayPlastics\Models\UserDocument;
use App\Post;

class DocumentsRepository
{

    public function getDocUsers(Post $document)
    {
        return UserDocument::with(['user'])
            ->where('post_id', $document->id)
            ->get()->map(function ($userDocument) {
                return $userDocument->user;
            });
    }
}