<?php

Route::group(['middleware' => ['auth']], function () {
    Route::get('/accept-policy', 'AcceptPolicyController@index')->name('accept-policy');
    Route::post('/accept-policy', 'AcceptPolicyController@accept');
});

Route::group(['middleware' => ['auth', 'accept-policy']], function () {
    Route::get('/{category}/create', 'DocumentsController@create')->name('document.create');
    Route::post('/{category}/create', 'DocumentsController@store')->name('document.store');
    Route::post('/{category}/upload-file', 'DocumentsController@uploadFile')->name('document.upload-file');
    Route::get('/documents/{doc}/comments', 'DocumentsController@comments')->name('document.comments');
    Route::post('/documents/{doc}/comments/store', 'DocumentsController@commentStore')->name('doc.comments.store');

    Route::get('/validate/{doc}', 'DocumentsController@validateDoc')->name('document.validate');
    Route::post('/validate/{doc}', 'DocumentsController@postValidateDoc');

    Route::post('/documents/{doc}', '\App\Http\Controllers\PostsController@process_routes')->name('document.comment');
    Route::get('/{index?}', 'HomeController@index')->name('index');
    Route::get('/all', 'HomeController@indexAll')->name('index-all');

    Route::get('/user/edit', 'UsersController@edit')->name('user.edit');
    Route::put('/user/update', 'UsersController@update')->name('user.update');
    Route::put('/user/update-pwd', 'UsersController@updatePwd')->name('user.update-pwd');
    Route::post('/user/update-language', 'UsersController@updateLanguage')->name('user.update-language');

    Route::get('/download-files', 'HomeController@downloadMultipleFiles')->name('home.download-files');
    Route::post('/document-download', 'HomeController@documentDownload')->name('document.download');
    Route::post('/document-view-comments', 'HomeController@documentViewComments')->name('document.view-comments');

    Route::post('/delete-validation-document', 'HomeController@deleteValidationDocument')->name('document.delete-validation-document');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function () {
    Route::get('/',  function () {
        return redirect()->route('admin.validation-status');
    })->name('admin');
});


Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
    Route::resource('users', "Admin\UsersController", ["as" => "admin"])->except([
        'store', 'show'
    ]);

    Route::resource('groups', "Admin\GroupsController", ["as" => "admin"])->except(['show']);
    Route::delete('groups/{group}/user/{user}/remove', 'Admin\GroupsController@remove_user')->name('admin.groups.user.remove');
    Route::put('groups/{group}/user/{user}/authorize', 'Admin\GroupsController@authorize_user')->name('admin.groups.user.authorize');


    Route::resource('posts', "Admin\MyPostsController", ["as" => "admin"]);
    Route::resource('doc-comments', "Admin\CommentsController", ["as" => "admin"]);

    Route::post('/users', 'Admin\UsersController@customStore')->name('admin.users.store');

    Route::get('/validation-status', 'Admin\ValidationStatusController@index')->name('admin.validation-status');
});