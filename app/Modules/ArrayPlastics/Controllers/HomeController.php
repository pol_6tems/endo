<?php


namespace App\Modules\ArrayPlastics\Controllers;

use App\Modules\ArrayPlastics\Models\Post;
use ZipArchive;
use App\Models\Media;
use App\Models\PostMeta;
use App\Models\CustomFieldGroup;
use App\Http\Controllers\Controller;
use App\Modules\ArrayPlastics\Models\User;
use App\Modules\ArrayPlastics\Models\UserDocument;
use App\Modules\ArrayPlastics\Repositories\GroupsRepository;
use App\Modules\ArrayPlastics\Controllers\Traits\hasMultilanguage;

class HomeController extends Controller
{
    use hasMultilanguage;

    public function index()
    {
        $user = auth()->user();
        $clientId = request('client');
        $categories = collect();
        $grupGestor = $user->groups;
        // Si no és Gestor de Departament i Està en algun grup?
        
        if ($user->rol->level < 50) {
            if ($grupGestor->isNotEmpty()) {
                $gestors = $user->groups->map(function($group) {
                    return $group->gestors;
                })->flatten()->unique("id");
                
                $categories = $gestors->map(function($gestor) {
                    return $gestor->categories()->get();
                })->flatten()->unique("id");
            }
        } else {
            $author = $user;

            if ($user->rol->level < 51 && $author->groups->count()) {
                $groups = $author->groups->filter(function ($group) use ($author) {
                    return $group->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->count();
                })->first();

                if ($groups) {
                    $author = $groups->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->first();
                } else {
                    $author = $author->groups->first()->gestors->first();
                }

                if (!$author) {
                    return redirect()->route('admin')->with('error', 'No assignat a cap grup');
                }

                /*$author = $author->groups->first()->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->first();*/
            }

            $categories = get_posts(['post_type' => 'categoria', 'where' => [['author_id', $author->id]]])->sortBy('order');
        }


        $userValidator = $user;

        // Gestor Departament / Usuari Departament
        if ($user->rol->level >= 50) {            
            // Get parameter defined
            if ($clientId) {
                // Show user
                $client = User::find($clientId);
            } else {
                // Home
                // Get Parameter not defined
                $clients = app(GroupsRepository::class)->getManagerClients($author);
                $client = $clients->first();
            }

            if ($client) {
                $userValidator = $client;
            }
        }
        // No Gestor

        $categories = $categories->filter(function($category) use ($userValidator) {
            $usuaris = $category->get_field('usuaris');

            $groups_usuari = $userValidator->groups;
            $groups_categoria = \DB::table("group_post")->where("post_id", $category->id)->get()->map(function($item) use($category) {
                return \App\Modules\ArrayPlastics\Models\Group::find($item->group_id);
            });

            return ($usuaris && $usuaris->where("id", $userValidator->id)->isNotEmpty()) || ($groups_usuari->intersect($groups_categoria)->isNotEmpty());
        });

        $userDocuments = UserDocument::with(['post.metas.customField'])
            ->withTrashed()
            ->where('user_id', $userValidator->id)
            ->get();

        foreach ($categories as $category) {
            $documents = $userDocuments->filter(function ($userDocument) use ($category) {
                return $userDocument->post && $userDocument->post->status == 'publish' && is_object($userDocument->get_field('categoria')) && $userDocument->get_field('categoria')->id == $category->id;
            })->sortBy('created_at');

            $category->documents = $documents->filter(function($doc) {
                return !$doc->trashed();
            });

            $category->documents_trashed = $documents->filter(function($doc) {
                return $doc->trashed();
            });
        }

        $locale = $this->getLocale($user);

        return View('Front::home', compact('categories', 'locale'));
    }


    public function indexAll()
    {
        $user = auth()->user();
        $categories = collect();
        $grupGestor = $user->groups;
        // Si no és Gestor de Departament i Està en algun grup?

        $author = $user;

        if ($user->rol->level < 51 && $author->groups->count()) {
            $groups = $author->groups->filter(function ($group) use ($author) {
                return $group->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->count();
            })->first();

            if ($groups) {
                $author = $groups->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->first();
            } else {
                $author = $author->groups->first()->gestors->first();
            }

            if (!$author) {
                return redirect()->route('admin')->with('error', 'No assignat a cap grup');
            }

            /*$author = $author->groups->first()->gestors->filter(function ($gestor) use ($author) {
                return $gestor->rol->level >= 51 && $gestor->id != $author->id;
            })->first();*/
        }

        $categories = get_posts(['post_type' => 'categoria', 'where' => [['author_id', $author->id]]])->sortBy('order');

        $authorDocuments = Post::where('type', 'document')
            ->where(function ($q) use ($author) {
                $q->where('author_id', $author->id)
                    ->orWhere('creator_id');
            })
            ->orderBy('created_at', 'asc')
            ->get();

        foreach ($categories as $category) {
            $category->author_documents = $authorDocuments->filter(function ($authorDoc) use ($category) {
                $docCat = $authorDoc->get_field('categoria');

                return $docCat && is_object($docCat) && $docCat->id == $category->id;
            });
        }
        
        foreach ($categories as $category) {
            $category->documents = collect();

            $category->documents_trashed = collect();
        }

        $locale = $this->getLocale($user);

        return View('Front::home', compact('categories', 'locale'));
    }


    private function getCustomFieldParentId($post_type, $fieldName)
    {
        $cf_group = CustomFieldGroup::with(['fields'])->where('post_type', $post_type)->first();

        foreach ($cf_group->fields as $field) {
            if ($field->name == $fieldName) {
                return $field->id;
            }
        }
    }


    public function downloadMultipleFiles()
    {
        $ids = request('ids');
        $filename = request('filename', 'files.zip');

        if (!$ids) {
            return redirect()->back()->with('error', __('S\'ha produit un error'));
        }

        $ids = explode(',', $ids);

        $files = Media::whereIn('id', $ids)->get();

        if (!$files->count()) {
            return redirect()->back()->with('error', __('S\'ha produit un error'));
        }
        $firstFile = $files->first();

        $filenameWithPath = str_replace($firstFile->filename . '.' . $firstFile->file_type, $filename, $firstFile->getFilePath());

        $zip = new ZipArchive();
        $zip->open($filenameWithPath, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        foreach ($files as $file) {
            $new_filename = str_replace('.zip', '/', $filename) . substr($file->getFilePath(),strrpos($file->getFilePath(),'/') + 1);
            $zip->addFile($file->getFilePath(), $new_filename);
        }

        $zip->close();

        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename='.$filename);
        header('Content-Length: ' . filesize($filenameWithPath));

        readfile($filenameWithPath);
    }


    public function documentDownload()
    {
        $userDocumentId = request('document');

        $userDocument = UserDocument::findOrFail($userDocumentId);

        $return = ['refresh' => 0];

        $user = auth()->user();

        $validador = $userDocument->post->get_field('validador');

        if (
                ($user->id == $userDocument->user_id && ($userDocument->user_id == $userDocument->post->creator_id || (!$validador || ($userDocument->user_id == $validador->id)))) ||
                ($user->rol->level >= 50 && $userDocument->user_id == $userDocument->post->creator_id)
            ) {


            if (!$userDocument->downloaded) {
                $userDocument->downloaded = true;

                $return['refresh'] = 1;
            }

            if ($userDocument->validation_status == 'pendent') {
                $userDocument->validation_status = 'revisio';
            }

            $userDocument->save();
        }

        return response()->json($return);
    }


    public function documentViewComments()
    {
        $userDocumentId = request('document');

        $userDocument = UserDocument::findOrFail($userDocumentId);

        $user = auth()->user();

        if ($user->id == $userDocument->user_id) {
            $userDocument->comments()->where('user_id', '<>', $user->id)
                ->where('viewed', 0)
                ->update(['viewed' => 1]);
        } elseif ($user->rol->level >= 50) {
            $userDocument->comments()->where('user_id', $userDocument->user_id)
                ->where('viewed', 0)
                ->update(['viewed' => 1]);
        }

        return response()->json([]);
    }


    public function deleteValidationDocument()
    {
        $userDocumentId = request('user_document');

        $userDocument = UserDocument::findOrFail($userDocumentId);

        $mediaId = request('media');

        if ($mediaId && $userDocument->validation_files) {
            if ($imatges = (array)json_decode($userDocument->validation_files)) {
                $cleanImatges = [];
                foreach ($imatges as $imatge) {
                    if ($mediaId != $imatge) {
                        $cleanImatges[] = $imatge;
                    }
                }

                $userDocument->update([
                    'validation_files' => json_encode($cleanImatges, JSON_FORCE_OBJECT),
                    'validation_status' => count($cleanImatges) ? $userDocument->validation_status : 'revisio'
                ]);
            }
        }

        return redirect()->route('index')->with('success', __('Documento eliminado correctamente'));
    }
}