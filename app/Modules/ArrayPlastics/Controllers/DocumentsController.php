<?php


namespace App\Modules\ArrayPlastics\Controllers;

use Illuminate\Support\Facades\Log;
use stdClass;
use Carbon\Carbon;
use App\Models\Media;
use App\Models\Mensaje;
use App\Models\PostMeta;
use App\Models\CustomFieldGroup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Notifications\EmailNotification;
use App\Modules\ArrayPlastics\Models\Post;
use App\Modules\ArrayPlastics\Models\User;
use App\Modules\ArrayPlastics\Models\Group;
use App\Http\Controllers\Admin\MediaController;
use App\Modules\ArrayPlastics\Models\UserDocument;
use App\Modules\ArrayPlastics\Models\UserDocumentComment;
use App\Modules\ArrayPlastics\Repositories\GroupsRepository;
use App\Modules\ArrayPlastics\Notifications\ArrayNotification;

class DocumentsController extends Controller
{

    public function create($locale, $category)
    {
        $category = Post::publish()
            ->ofType('categoria')
            ->whereTranslation('post_name', $category)
            ->first();

        $user = auth()->user();
        $clients = null;
        $groups = null;

        if ($user->rol->level >= 50) {
            $author = $user;

            if ($author->rol->level < 51 && $author->groups->count()) {
                $groups = $author->groups->filter(function ($group) use ($author) {
                    return $group->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->count();
                })->first();

                if ($groups) {
                    $author = $groups->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->first();
                } else {
                    $author = $author->groups->first()->gestors->first();
                }

                if (!$author) {
                    return redirect()->route('admin')->with('error', 'No assignat a cap grup');
                }
            }

            $clients = cacheable(GroupsRepository::class, 10)->getManagerClients($author);
            $groups = $author->groupsGestionats;

            $groups = $groups->filter(function ($group) use ($category) {
                return $group->categories()->where('id', $category->id)->first();
            });
        }

        $maxSize = $this->file_upload_max_size();
        $maxSizeMsg = __('La suma de la mida dels fitxers és massa gran. Màxim :max', ['max' => $this->file_upload_max_size(false)]);

        $cf_groups = CustomFieldGroup::where('post_type', 'document')->first();
        $fields = $cf_groups->fields;

        $validationTypesParams = json_decode($fields->where('name', 'tipus-validacio')->first()->params);
        $validationTypes = explode("\r\n", $validationTypesParams->choices);

        $maxPeriodParams = json_decode($fields->where('name', 'periode-maxim-validacio')->first()->params);
        $maxPeriods = explode("\r\n", $maxPeriodParams->choices);

        return view('Front::documents.create', compact('category', 'clients', 'groups', 'maxSize', 'maxSizeMsg', 'validationTypes', 'maxPeriods'));
    }


    public function store($locale, $category)
    {
        $category = Post::publish()
            ->ofType('categoria')
            ->whereTranslation('post_name', $category)
            ->first();

        if (!$category) {
            abort(404);
        }

        $author = auth()->user();
        $data = request()->all();

        if ($author->rol->level >= 50 && !((isset($data['validador']) && $data['validador']) || (isset($data['grup-validadors']) && $data['grup-validadors']))) {

            if (request('file_id')) {
                $mediaFile = Media::find(request('file_id'));

                if ($mediaFile) {
                    $mediaFile->remove_thumbnails();
                    $mediaFile->delete();
                }
            }

            return redirect()->back()->with('error', __('Falta validador o grup de validadors'))->withInput(Input::all());
        } else if ($author->rol->level < 50) {
            $data['validador'] = request()->input('validador', $author->id);
        } else {
            $validatorType = request()->input('tipus-validador');

            if ($validatorType == 'validador') {
                unset($data['grup-validadors']);
            }

            if ($validatorType == 'grup-validadors') {
                unset($data['validador']);
            }
        }

        if (!request('file_ids')) {
            return redirect()->back()->with('error', __('Falta fitxer'))->withInput(Input::all());
        }

        $data['categoria'] = $category->id;
        $data['estat-validacio'] = 'pendent';

        $data['fitxers'] = json_encode((is_array($data['file_ids']) ? $data['file_ids'] : explode(',', $data['file_ids'])), JSON_FORCE_OBJECT);

        if ($author->rol->level < 51) {
            $managerGroups = $author->groups;

            if ($managerGroups->isNotEmpty()) {
                $author = $managerGroups->first()->gestors()->first();
            }
        }

        $postData = [
            'type' => 'document',
            'status' => 'publish',
            'author_id' => $author->id,
        ];

        foreach ($this->_languages as $lang) {
            $postName = str_slug($data['title'], '-');
            $aux = $postName;
            $i = 1;
            while (Post::whereTranslation('post_name', $aux, $lang->code)->exists()) {
                $aux = $postName . '-' . $i;
                $i++;
            }

            $postName = $aux;

            $postData[$lang->code] = [
                'title' => $data['title'],
                'description' => isset($data['description']) ? $data['description'] : '',
                'post_name' => $postName
            ];
        }

        $postData['creator_id'] = auth()->id();

        $oldPost = Post::where('type', 'document')
            ->where('status', 'publish')
            ->where('author_id', $author->id)
            ->where('creator_id', auth()->id())
            ->where('created_at', '>', Carbon::now()->subSeconds(30))
            ->first();

        // Fix duplicate documents
        if ($oldPost) {
            $file = $oldPost->get_field('fitxer');

            if ($file && $file->id == $data['file_id']) {
                // Already created
                $params = [];

                if (auth()->user()->rol->level >= 50 && isset($data['validador']) && $data['validador']) {
                    $params = ['index' => '', 'client' => $data['validador']];
                }

                $route = 'index';

                if (auth()->user()->rol->level >= 50) {
                    $route = 'index-all';
                }

                return redirect()->route($route, array_merge($params, ['#category' . $category->id]))->with('success', __('Document creat correctament'));
            }
        }

        $post = Post::create($postData);

        $cf_groups = CustomFieldGroup::where('post_type', 'document')->orderBy('order')->get();

        foreach ($this->_languages as $lang) {
            foreach ($cf_groups as $cf_group) {
                foreach ($cf_group->fields as $field) {
                    if (array_key_exists($field->name, $data)) {
                        PostMeta::create([
                            'post_id' => $post->id,
                            'custom_field_id' => $field->id,
                            'value' => $data[$field->name],
                            'locale' => $lang->code
                        ]);
                    }
                }
            }
        }

        // TODO
        $maxValidationDate = (isset($data['data-maxima-validacio']) && $data['data-maxima-validacio']) ? Carbon::createFromFormat('d/m/Y', $data['data-maxima-validacio']) : null;

        if (!$maxValidationDate && isset($data['periode-maxim-validacio']) && $data['periode-maxim-validacio']) {
            $maxValidationDate = Carbon::parse($data['periode-maxim-validacio'])->startOfDay();
        }

        $expiryDate = (isset($data['data-caducitat']) && $data['data-caducitat']) ? Carbon::createFromFormat('d/m/Y', $data['data-caducitat']) : null;
        $docUsers = [];

        if (isset($data['validador']) && $data['validador']) {
            $docUsers[] = User::find($data['validador']);
        }

        if (isset($data['grup-validadors']) && $data['grup-validadors']) {
            $group = Group::find($data['grup-validadors']);
            $docUsers = $group->clients;

            $docUsers = $docUsers->filter(function ($docUser) {
                return $docUser->rol->level < 50;
            });

            $group->documents()->syncWithoutDetaching($post->id);
        }

        foreach ($docUsers as $docUser) {
            UserDocument::create([
                'post_id' => $post->id,
                'user_id' => $docUser->id,
                'validation_status' => $data['estat-validacio'],
                'type' => isset($data['tipus-validacio']) && $data['tipus-validacio'] ? $data['tipus-validacio'] : UserDocument::DOC_TYPE_WEB_VALIDABLE,
                'validation_date' => null,
                'max_validation_date' => $maxValidationDate,
                'expiry_date' => $expiryDate,
                'validation_files' => ''
            ]);
        }

        $currentLang = app()->getLocale();

        if (isProEnv()) {
            $user = auth()->user();
            $emailInfo = env('EMAIL_INFO');

            if (!$emailInfo) {
                dd('Error: falta variable EMAIL_INFO');
            }

            $from = User::where('email', $emailInfo)->first();
            //$from = User::where('email', 'pol@6tems.com')->first();

            $mensaje = new Mensaje();
            $mensaje->user_id = 0;
            $mensaje->mensaje = '';
            $mensaje->fitxer = '';
            $mensaje->email = '';
            $mensaje->name = '';
            $mensaje->type = 'email';

            if ($category->get_field('responsable') instanceof \App\User) {
                $author = $category->get_field('responsable');
            }


            $caducitat = '';
            $validacio = '';
            $caducitat_validacio = "";
            if ( $maxValidationDate ) {
                $caducitat_validacio .= "<strong>Data de Validació</strong>:" . $maxValidationDate->format('d/m/Y');
                $validacio = $maxValidationDate->format('d/m/Y');
            } else if ( $expiryDate ) {
                $caducitat_validacio .= "<strong>Data de Caducitat</strong>:" . $expiryDate->format('d/m/Y');
                $caducitat = $expiryDate->format('d/m/Y');
            }
            
            $params = [
                'caducitat_validacio' => $caducitat_validacio,
                'estat' => ucwords($data['estat-validacio']),
                'caducitat' => $caducitat,
                'validacio' => $validacio
            ];

            if (auth()->user()->rol->level >= 50) {
                $mensaje->params = json_encode(array_merge($params, [
                    'document' => $post->title,
                ]));

                foreach ($docUsers as $validator) {
                    $params = new stdClass();
                    $params->gestor = $author;
                    $params->usuario = $validator;
                    $params->mensaje = $mensaje;
                    $params->from = $from;
                    $params->email_id = 5;

                    try {
                        $notificacion = new ArrayNotification($params);
                        if ($notificacion->check_email()) {
                            $lang = $validator->get_field('idioma');
                            if ($lang) {
                                app()->setLocale($lang);
                            }

                            $validator->notify($notificacion);
                        }
                    } catch (\Exception $e) {
                        // Do nothing
                        Log::error($e->getMessage());
                    }
                }
            } else {
                $mensaje->params = json_encode(array_merge($params, [
                    'username' => $user->fullname(),
                    'client_id' => $user->id,
                    'document' => $post->title,
                    'category' => $category->title,
                ]));


                if ($category && $responsible = $category->get_field('responsable')) {
                    $author = $responsible;
                }

                $params = new stdClass();
                $params->gestor = auth()->user();
                $params->usuario = $author;
                $params->mensaje = $mensaje;
                $params->from = $from;
                $params->email_id = 4;

                try {
                    $notificacion = new ArrayNotification($params);
                    if ($notificacion->check_email()) {
                        $lang = $author->get_field('idioma');
                        if ($lang) {
                            app()->setLocale($lang);
                        }

                        $author->notify($notificacion);
                    }
                } catch (\Exception $e) {
                    // Do nothing
                    Log::error($e->getMessage());
                }
            }
        }

        $params = [];

        $route = 'index';

        if (auth()->user()->rol->level >= 50) {
            $route = 'index-all';
        }

        if (auth()->user()->rol->level >= 50 && isset($data['validador']) && $data['validador']) {
            $params = ['index' => '', 'client' => $data['validador']];
        }

        if (app()->getLocale() != $currentLang) {
            app()->setLocale($currentLang);
        }

        return redirect()->route($route, array_merge($params, ['#category' . $category->id]))->with('success', __('Document creat correctament'));
    }


    public function uploadFile()
    {
        $files = request()->file('files');
        $response = ['files' => []];

        foreach ($files as $file) {
            $return = app(MediaController::class)->upload_file([], $file);

            $response['files'][] = [
                'id' => $return['new_item']->id
            ];
        }


        return response()->json($response);
    }


    public function validateDoc($locale, $doc)
    {

        $user = auth()->user();
        $clientId = request('client');

        $userValidator = $user;

        if ($user->rol->level >= 50) {
            if ($clientId) {
                $client = User::find($clientId);
            } else {
                $author = $user;
                if ($author->rol->level < 51 && $author->groups->count() /*&& $grupGestor = $author->get_field('grup-gestor')*/) {
                    /*$author = $grupGestor->first()->get_field('gestor');*/
                    $groups = $author->groups->filter(function ($group) use ($author) {
                        return $group->gestors->filter(function ($gestor) use ($author) {
                            return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                        })->count();
                    })->first();

                    if ($groups) {
                        $author = $groups->gestors->filter(function ($gestor) use ($author) {
                            return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                        })->first();
                    } else {
                        $author = $author->groups->first()->gestors->first();
                    }

                    if (!$author) {
                        return redirect()->route('admin')->with('error', 'No assignat a cap grup');
                    }
                }

                $client = app(GroupsRepository::class)->getManagerClients($author)->first();
            }

            if ($client) {
                $userValidator = $client;
            }
        }

        $document = Post::publish()
            ->ofType('document')
            ->whereTranslation('post_name', $doc)
            ->first();

        if ($document) {
            $userDoc = UserDocument::where('user_id', $userValidator->id)
                ->where('post_id', $document->id)
                ->first();

            if ($user->rol->level < 50 && $userDoc->user_id == $document->creator_id) {
                abort(403);
            }

            $validationStatus = $userDoc->validation_status;
            $category = 'documents';

            $maxSize = $this->file_upload_max_size();
            $maxSizeMsg = __('La suma de la mida dels fitxers és massa gran. Màxim :max', ['max' => $this->file_upload_max_size(false)]);

            return view('Front::documents.validate', compact('document', 'validationStatus', 'category', 'maxSize', 'maxSizeMsg'));
        }

        abort(404);
    }


    public function postValidateDoc($locale, $doc)
    {
        $user = auth()->user();
        $clientId = request('client');

        $userValidator = $user;

        if ($user->rol->level >= 50) {
            if ($clientId) {
                $client = User::find($clientId);
            } else {
                $author = $user;

                if ($author->rol->level < 51 && $author->groups->count() /*&& $grupGestor = $author->get_field('grup-gestor')*/) {
                    /*$author = $grupGestor->first()->get_field('gestor');*/
                    $groups = $author->groups->filter(function ($group) use ($author) {
                        return $group->gestors->filter(function ($gestor) use ($author) {
                            return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                        })->count();
                    })->first();

                    if ($groups) {
                        $author = $groups->gestors->filter(function ($gestor) use ($author) {
                            return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                        })->first();
                    } else {
                        $author = $author->groups->first()->gestors->first();
                    }

                    if (!$author) {
                        return redirect()->route('admin')->with('error', 'No assignat a cap grup');
                    }
                }

                $client = app(GroupsRepository::class)->getManagerClients($author)->first();
            }

            if ($client) {
                $userValidator = $client;
            }
        }

        $document = Post::publish()
            ->ofType('document')
            ->whereTranslation('post_name', $doc)
            ->first();

        $userDoc = null;

        if ($document) {
            $userDoc = UserDocument::where('user_id', $userValidator->id)
                ->where('post_id', $document->id)
                ->first();
        }

        if (!$document || !$userDoc) {
            abort(404);
        }

        if ($user->rol->level < 50 && $userDoc->user_id == $document->creator_id) {
            abort(403);
        }

        $newStatus = request()->input('estat-validacio');
        $comment = request()->input('comment');

        if ($document->get_field('tipus-validacio') == 'signature' && !request('file_ids') && $newStatus == 'validat' && $user->rol->level < 50) {
            return redirect()->back()->with('error', __('Falta fitxer'))->withInput(Input::all());
        }

        if ($newStatus == 'no-acceptat' && !$comment) {
            return redirect()->back()->with('error', __('Falta comentari'))->withInput(Input::all());
        }

        if ($newStatus) {
            $userDoc->validation_status = $newStatus;

            if ($newStatus == 'validat') {
                $userDoc->validation_date =  Carbon::now();

                if ($fitxers = request('file_ids')) {
                    $fitxers = is_array($fitxers) ? $fitxers : explode(',', $fitxers);

                    $newFiles = [];
                    if ($currFiles = $userDoc->validation_files) {
                        $newFiles = json_decode($currFiles, true);
                    }

                    $newFiles = array_merge($newFiles, $fitxers);

                    /*$userDoc->validation_files = json_encode($newFiles, JSON_FORCE_OBJECT);*/

                    $data = [
                        'categoria' => $document->get_field('categoria') ? $document->get_field('categoria')->id : '',
                        'estat-validacio' => 'pendent',
                        'fitxers' => json_encode($newFiles, JSON_FORCE_OBJECT),
                        'title' => $document->title
                    ];

                    if ($userValidator->rol->level < 51) {
                        $managerGroups = $userValidator->groups;

                        if ($managerGroups->isNotEmpty()) {
                            $author = $managerGroups->first()->gestors()->first();
                        }
                    } else {
                        $author = $userValidator;
                    }


                    $postData = [
                        'type' => 'document',
                        'status' => 'publish',
                        'author_id' => $author->id,
                        'tipus-validador' => 'validador',
                        'validador' => $userValidator->id,
                        'tipus-validacio' => 'signature',
                        'periode-maxim-validacio' => 0,
                        'data-caducitat' => null,
                    ];

                    foreach ($this->_languages as $lang) {
                        $data['title'] = __('Validación ', [], $lang->code) . $data['title'];
                        $postName = str_slug($data['title'], '-');
                        $aux = $postName;
                        $i = 1;
                        while (Post::whereTranslation('post_name', $aux, $lang->code)->exists()) {
                            $aux = $postName . '-' . $i;
                            $i++;
                        }

                        $postName = $aux;

                        $postData[$lang->code] = [
                            'title' => $data['title'],
                            'description' => isset($data['description']) ? $data['description'] : '',
                            'post_name' => $postName
                        ];
                    }

                    $postData['creator_id'] = auth()->id();

                    $post = Post::create($postData);

                    $cf_groups = CustomFieldGroup::where('post_type', 'document')->orderBy('order')->get();

                    foreach ($this->_languages as $lang) {
                        foreach ($cf_groups as $cf_group) {
                            foreach ($cf_group->fields as $field) {
                                if (array_key_exists($field->name, $data)) {
                                    PostMeta::create([
                                        'post_id' => $post->id,
                                        'custom_field_id' => $field->id,
                                        'value' => $data[$field->name],
                                        'locale' => $lang->code
                                    ]);
                                }
                            }
                        }
                    }

                    UserDocument::create([
                        'post_id' => $post->id,
                        'user_id' => $userValidator->id,
                        'validation_status' => $data['estat-validacio'],
                        'type' => isset($data['tipus-validacio']) && $data['tipus-validacio'] ? $data['tipus-validacio'] : UserDocument::DOC_TYPE_WEB_VALIDABLE,
                        'validation_date' => null,
                        'max_validation_date' => null,
                        'expiry_date' => null,
                        'validation_files' => ''
                    ]);
                }
            }

            $userDoc->validator_id = auth()->user()->id;

            $userDoc->save();
        }

        if ($newStatus == 'no-acceptat' && $comment) {
            $commentData = [
                'user_id' => auth()->user()->id,
                'user_document_id' => $userDoc->id,
                'comment' => $comment
            ];

            UserDocumentComment::create($commentData);
        }

        $routeParams = [];

        $user = auth()->user();

        if ($user->rol->level >= 50) {
            $routeParams = ['index' => '', 'client' => $userValidator->id];
        }

        if (isProEnv() && ($newStatus == 'validat' || $newStatus == 'no-acceptat')) {
            $category = $document->get_field('categoria');

            $mensaje = new Mensaje();
            $mensaje->user_id = 0;
            $mensaje->mensaje = '';
            $mensaje->fitxer = '';
            $mensaje->email = '';
            $mensaje->name = '';
            $mensaje->type = 'email';
            $mensaje->params = json_encode([
                'username' => $user->fullname(),
                'filename' => $document->title,
                'client_id' => $user->id,
                'category' => $category ? $category->title : 'N/A'
            ]);

            $emailInfo = env('EMAIL_INFO');

            if (!$emailInfo) {
                dd('Error: falta variable EMAIL_INFO');
            }

            $from = User::where('email', $emailInfo)->first();
            // $from = User::where('email', 'pol@6tems.com')->first();
            $author = $document->author;

            if ($category && $responsible = $category->get_field('responsable')) {
                $author = $responsible;
            }

            $params = new stdClass();
            $params->usuario = $author;
            $params->mensaje = $mensaje;
            $params->from = $from;
            $params->email_id = 1;

            if ($newStatus == 'no-acceptat') {
                $params->email_id = 2;
            }

            try {
                $notificacion = new EmailNotification($params);
                if ($notificacion->check_email()) {
                    $author->notify($notificacion);
                }
            } catch (\Exception $e) {
                // Do nothing
                Log::error($e->getMessage());
            }
        }

        return redirect()->route('index', $routeParams)->with('success', __('Actualitzat correctament'));
    }


    public function comments($locale, $doc)
    {
        $user = auth()->user();
        $clientId = request('client');

        $userValidator = $user;

        if ($user->rol->level >= 50) {
            if ($clientId) {
                $client = User::find($clientId);
            } else {
                $author = $user;

                if ($author->rol->level < 51 && $author->groups->count() /*&& $grupGestor = $author->get_field('grup-gestor')*/) {
                    /*$author = $grupGestor->first()->get_field('gestor');*/
                    $groups = $author->groups->filter(function ($group) use ($author) {
                        return $group->gestors->filter(function ($gestor) use ($author) {
                            return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                        })->count();
                    })->first();

                    if ($groups) {
                        $author = $groups->gestors->filter(function ($gestor) use ($author) {
                            return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                        })->first();
                    } else {
                        $author = $author->groups->first()->gestors->first();
                    }

                    if (!$author) {
                        return redirect()->route('admin')->with('error', 'No assignat a cap grup');
                    }
                }

                $client = app(GroupsRepository::class)->getManagerClients($author)->first();
            }

            if ($client) {
                $userValidator = $client;
            }
        }

        $document = Post::publish()
            ->ofType('document')
            ->whereTranslation('post_name', $doc)
            ->first();

        $view = '';

        if ($document) {
            $userDoc = UserDocument::where('user_id', $userValidator->id)
                ->where('post_id', $document->id)
                ->first();

            if ($userDoc) {
                $comments = UserDocumentComment::where('user_document_id', $userDoc->id)->orderby('updated_at', 'asc')->get();

                try {
                    $view = view('Front::partials.comments', compact('document', 'comments'))->render();
                } catch (\Throwable $e) {
                    // Do nothing
                }
            }
        }

        return response()->json(['widget' => $view]);
    }


    public function commentStore($locale, $doc)
    {
        $data = request()->all();

        $user = auth()->user();
        $clientId = request('client');

        $userValidator = $user;

        if ($user->rol->level >= 50) {
            if ($clientId) {
                $client = User::find($clientId);
            } else {
                $author = $user;

                if ($author->rol->level < 51 && $author->groups->count() /*&& $grupGestor = $author->get_field('grup-gestor')*/) {
                    /*$author = $grupGestor->first()->get_field('gestor');*/
                    $groups = $author->groups->filter(function ($group) use ($author) {
                        return $group->gestors->filter(function ($gestor) use ($author) {
                            return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                        })->count();
                    })->first();

                    if ($groups) {
                        $author = $groups->gestors->filter(function ($gestor) use ($author) {
                            return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                        })->first();
                    } else {
                        $author = $author->groups->first()->gestors->first();
                    }

                    if (!$author) {
                        return redirect()->route('admin')->with('error', 'No assignat a cap grup');
                    }
                }

                $client = app(GroupsRepository::class)->getManagerClients($author)->first();
            }

            if ($client) {
                $userValidator = $client;
            }
        }

        $document = Post::publish()
            ->ofType('document')
            ->whereTranslation('post_name', $doc)
            ->first();

        if ($document) {
            $userDoc = UserDocument::where('user_id', $userValidator->id)
                ->where('post_id', $document->id)
                ->first();

            if ($userDoc && !empty($data['comment']) && !empty($data['post_id']) ) {
                UserDocumentComment::create([
                    'user_document_id' => $userDoc->id,
                    'user_id' => $user->id,
                    'comment' => $data['comment'],
                ]);
            }
        }

        return !empty($data['redirect_to']) ? redirect($data['redirect_to']) : abort(404);
    }


    /**
     * Returns a file size limit in bytes based on the PHP upload_max_filesize and post_max_size
     *
     * @param bool $parsed
     * @return int
     */
    private function file_upload_max_size($parsed = true) {
        static $max_size = -1;
        static $non_parse_max_size = -1;

        if ($max_size < 0) {
            // Start with post_max_size.
            $non_parsed_post_max_size = ini_get('post_max_size');
            $post_max_size = $this->parse_size($non_parsed_post_max_size);
            if ($post_max_size > 0) {
                $max_size = $post_max_size;
                $non_parse_max_size = $non_parsed_post_max_size;
            }

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $non_parsed_upload_max = ini_get('upload_max_filesize');
            $upload_max = $this->parse_size($non_parsed_upload_max);
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
                $non_parse_max_size = $non_parsed_upload_max;
            }
        }

        return $parsed ? $max_size : $non_parse_max_size;
    }

    private function parse_size($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
            return round($size);
        }
    }
}