<?php


namespace App\Modules\ArrayPlastics\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\ArrayPlastics\Requests\UserUpdateRequest;
use App\Modules\ArrayPlastics\Requests\UserPasswordRequest;

class UsersController extends Controller
{

    public function edit($locale)
    {
        return view('Front::users.edit');
    }


    public function update(UserUpdateRequest $request, $locale)
    {
        $user = auth()->user();

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return redirect()->route('user.edit')->with('success', __('Dades actualitzades correctament'));
    }


    /*
    public function updatePwd(UserPasswordRequest $request, $locale)
    {
        $user = auth()->user();

        $user->update([
            'password' => bcrypt($request->password)
        ]);

        return redirect()->route('user.edit')->with('success', __('Dades actualitzades correctament'));
    }*/


    public function updateLanguage($locale)
    {
        $user = auth()->user();

        $lang = request('lang');

        if ($lang) {
            $user->set_field('idioma', $lang);
        }

        $route = request('after_route', 'index');

        $params = [];

        if ($user->rol->level >= 50 && request('client')) {
            if (request('is_index')) {
                $params['index'] = '';
            }

            $params['client'] = request('client');
        }

        return redirect()->route($route, $params);
    }
}