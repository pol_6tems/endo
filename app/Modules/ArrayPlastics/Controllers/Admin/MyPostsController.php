<?php


namespace App\Modules\ArrayPlastics\Controllers\Admin;


use App\User;
use App\Models\Admin\Rol;
use App\Models\CustomPost;
use Illuminate\Http\Request;
use App\Models\Configuracion;
use App\Models\CustomFieldGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Modules\ArrayPlastics\Models\Post;
use App\Modules\ArrayPlastics\Models\Group;
use App\Repositories\CustomFieldGroupRepository;
use App\Modules\ArrayPlastics\Repositories\GroupsRepository;
use App\Modules\ArrayPlastics\Repositories\DocumentsRepository;

class MyPostsController extends \App\Http\Controllers\Admin\PostsController
{

    public function index()
    {
        if ($this->post_type == 'page') {
            return parent::index();
        }

        $author = auth()->user();

        if ($author->rol->level < 51 && $author->groups->count()) {
            $groups = $author->groups->filter(function ($group) use ($author) {
                return $group->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->count();
            })->first();

            if ($groups) {
                $author = $groups->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->first();
            } else {
                $author = $author->groups->first()->gestors->first();
            }

            if (!$author) {
                return redirect()->route('admin')->with('error', 'No assignat a cap grup');
            }
        }

        $parent = (request()->parent) ? request()->parent : null;
        $status = (request()->status) ? request()->status : $this->def_status;

        $home_page = Configuracion::get_config('home_page');
        $home_page_id = !empty($home_page) ? $home_page->id : 0;

        $builder = Post::ofType($this->post_type)
            ->ofStatus($status)
            ->withTranslation();

        if ( !$status ) {
            // Revisar
            // Todo: Revisar-ne l'us
            if ( $parent ) {
                $parent_id = $parent;
                $parent = Post::ofType($this->post_type)->where('id', $parent_id)->get();
                if ($parent) {
                    $builder = $parent->merge(Post::ofType($this->post_type)->ofType($this->post_type)->OfParent($parent_id)->get());
                } else {
                    $builder = $builder->OfParent($parent_id);
                }
            } else {
                $builder = $builder->with(['parent', 'customPostTranslations.customPost.translations']);
            }
        }

        // View::share ( 'post_types', $post_types );

        $trashedQuery = Post::onlyTrashed()->ofType($this->post_type);
        $pendingQuery = Post::ofType($this->post_type)->pending();
        $draftQuery = Post::ofType($this->post_type)->draft();

        if (!$author->isAdmin()) {
            $builder = $builder->where('author_id', $author->id);
            $trashedQuery = $trashedQuery->where('author_id', $author->id);
            $pendingQuery = $pendingQuery->where('author_id', $author->id);
            $draftQuery = $draftQuery->where('author_id', $author->id);
        }

        $items = $builder->orderByDesc('created_at')->get();

        $num_trashed = $trashedQuery->count();
        $num_pending = $pendingQuery->count();
        $num_draft = $draftQuery->count();

        return view('Front::admin.posts.index', compact('items', 'status', 'num_trashed', 'num_pending', 'num_draft', 'home_page_id'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $author = auth()->user();

        if ($author->rol->level < 51 && $author->groups->count()) {
            $groups = $author->groups->filter(function ($group) use ($author) {
                return $group->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->count();
            })->first();

            if ($groups) {
                $author = $groups->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->first();
            } else {
                $author = $author->groups->first()->gestors->first();
            }

            if (!$author) {
                return redirect()->route('admin')->with('error', 'No assignat a cap grup');
            }
        }

        $templates = $this->get_templates();

        $cf_groups = CustomFieldGroup::where(['post_type' => $this->post_type, 'template' => null])->orderBy('order')->get();

        // Filter Custom Fields by Role
        $cf_groups->map(function($group) {
            return $group->fields = $group->fields->filter(function ($field) {
                $params = json_decode($field->params);
                if (isset($params->role)) {
                    $cf_level = Rol::find($params->role)->level;
                    return auth()->user()->rol->level >= $cf_level;
                } else {
                    return true;
                }
            });
        });

        $valor = '';

        $params = null;
        if ( !in_array($this->post_type, ['post', 'page']) ) {
            $cp = CustomPost::whereTranslation('post_type', $this->post_type)->first();
            if ( !empty($cp) ) $params = $cp->params;
        }

        $users = collect([$author]);

        if (auth()->user()->isAdmin()) {
            $users = User::select(DB::raw('users.*'))->join('rols', 'rols.name', '=', 'users.role')->where('rols.level', 51)->get();
        }

        $show_authors_select = true;

        return view('Admin::posts.create', compact('valor', 'templates', 'cf_groups', 'params', 'users', 'show_authors_select'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $custom_fields = array();
        if ( !empty($data['custom_fields']) ) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code]) ) {
                $sanitize_title = str_slug($data[$lang->code]['title'], '-');
                $aux = $sanitize_title;
                $i = 1;
                while (Post::whereTranslation('post_name', $aux, $lang->code)->exists()) {
                    $aux = $sanitize_title . '-' . $i;
                    $i++;
                }

                $sanitize_title = $aux;
                $data[$lang->code]['post_name'] = $sanitize_title;
                $data[$lang->code]['media_id'] = ( !empty($data['media_id']) ) ? $data['media_id'] : null;
            }
        }

        if (!isset($data['author_id'])) {
            $data['author_id'] = auth()->id();
        }
        $data['creator_id'] = auth()->id();
        $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : 0;

        // Check if is revision
        if ( $data['status'] != 'draft' && auth()->user()->rol->level < Post::ROL_LEVEL_LIMIT_REVISIO ) {
            $data['status'] = 'pending';
        }

        $lastPost = Post::ofType($data['type'])
            ->orderBy('id', 'desc')
            ->first();

        if ($lastPost && !is_null($lastPost->order)) {
            $data['order'] = $lastPost->order + 1;
        }

        $post = Post::create($data);

        /* Custom Fields */
        foreach ($custom_fields as $lang => $fields) {
            $this->save_custom_fields($post, $fields, $lang);
        }
        /* end Custom Fields */

        // Hook
        execute_actions('save_post', $post);

        return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $this->post_type)->with(['message' => __(ucfirst($this->post_type) . ' added successfully')]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $author = auth()->user();

        if ($author->rol->level < 51 && $author->groups->count()) {
            $groups = $author->groups->filter(function ($group) use ($author) {
                return $group->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->count();
            })->first();

            if ($groups) {
                $author = $groups->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->first();
            } else {
                $author = $author->groups->first()->gestors->first();
            }

            if (!$author) {
                return redirect()->route('admin')->with('error', 'No assignat a cap grup');
            }
        }

        $post = Post::withTrashed()->findOrFail($id);
        $templates = $this->get_templates();
        $cf_groups = cacheable(CustomFieldGroupRepository::class, 60, true)->getPostCustomFields($post);

        if ($this->post_type != $post->type) return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $post->type);

        $params = null;
        if ( !in_array($this->post_type, ['post', 'page']) ) {
            $cp = CustomPost::whereTranslation('post_type', $this->post_type)->first();
            if ( !empty($cp) ) $params = $cp->params;
        }

        // Check if User can aprove pending post
        $can_aprove_pending = $post->status == 'pending' && auth()->user()->rol->level >= Post::ROL_LEVEL_LIMIT_REVISIO;

        $users = collect([$author]);

        if (auth()->user()->isAdmin()) {
            $users = User::select(DB::raw('users.*'))->join('rols', 'rols.name', '=', 'users.role')->where('rols.level', 51)->get();
        }

        $show_authors_select = true;

        $custom_user = auth()->user();

        if ($custom_user->rol->level < 51 && $grupGestor = $custom_user->get_field('grup-gestor')) {
            $aux_custom_user = $grupGestor->first()->get_field('gestor');

            if ($aux_custom_user) {
                $custom_user = $aux_custom_user;
            }
        }

        $custom_user_id = $custom_user->id;


        if ($this->post_type != 'grup-usuari') {
            if ($this->post_type == 'document') {
                $clients = app(DocumentsRepository::class)->getDocUsers($post);
                $clients = $clients->filter(function ($user) {
                    return $user;
                });

                return view('Front::admin.documents.edit', compact('post', 'templates', 'users', 'cf_groups', 'params', 'can_aprove_pending', 'show_authors_select', 'custom_user_id', 'clients'));
            } else if ($this->post_type == 'categoria') {
                if ($custom_user->isAdmin()) {
                    $groups = Group::all();
                } else {
                    $groups = $author->groupsGestionats;
                }

                $query = Post::ofType('document')
                    ->ofStatus('publish')
                    ->withTranslation();

                if (!auth()->user()->isAdmin()) {
                    $query = $query->where('author_id', $author->id);
                }

                $documents = $query
                    ->orderByDesc('created_at')->get()->filter(function ($document) use ($id) {
                        $docCat = $document->get_field('categoria');
                        $docCatId = is_object($docCat) ? $docCat->id : $docCat;
                        return $id == $docCatId;
                    });

                return view('Front::admin.categoria.edit', compact('post', 'templates', 'groups', 'users', 'cf_groups', 'params', 'can_aprove_pending', 'show_authors_select', 'custom_user_id', 'documents'));
            } else {
                return view('Admin::posts.edit', compact('post', 'templates', 'users', 'cf_groups', 'params', 'can_aprove_pending', 'show_authors_select', 'custom_user_id'));
            }
        } else {
            $clients = app(GroupsRepository::class)->getGroupUsers($post);

            $clients = $clients->filter(function ($user) {
                return $user;
            });

            return view('Front::admin.group.edit', compact(
                'post',
                'templates',
                'users',
                'cf_groups',
                'params',
                'can_aprove_pending',
                'show_authors_select',
                'clients'
            ));
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $data = $request->all();
        
        if ($this->post_type == 'categoria') {
            $groups = collect($request->groups);
            $idGroups = $groups->filter(function($item) { return $item == "1"; })->keys();
            
            $new_groups = $groups->each(function($item, $key) use ($id) {
                if ($item == "1") {
                    Group::find($key)->categories()->syncWithoutDetaching($id);
                } else {
                    Group::find($key)->categories()->detach($id);
                }
            });
            unset($data["groups"]);
        }


        $post = Post::findOrFail($id);

        if (!$post->creator_id) {
            $data['creator_id'] = auth()->id();
        }

        $post = $this->updatePost($post, $data);

        return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $this->post_type)->with(['message' => __('Post updated successfully')]);
    }
}