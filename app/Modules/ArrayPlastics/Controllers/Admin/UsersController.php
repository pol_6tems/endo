<?php


namespace App\Modules\ArrayPlastics\Controllers\Admin;


use App\Post;
use App\User;
use Carbon\Carbon;
use App\Models\Admin\Rol;
use Illuminate\Http\Request;
use App\Models\CustomFieldGroup;
use Illuminate\Support\Facades\Artisan;
use App\Modules\ArrayPlastics\Models\UserDocument;
use App\Modules\ArrayPlastics\Repositories\GroupsRepository;
use App\Modules\ArrayPlastics\Requests\AdminCreateUserRequest;
use App\Http\Controllers\Admin\UsersController as AdminUsersController;

class UsersController extends AdminUsersController
{

    use \Illuminate\Foundation\Auth\ResetsPasswords;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = \App\Modules\ArrayPlastics\Models\User::get();

        if (!auth()->user()->isAdmin()) {
            $items->load(['metas']);
            $user = auth()->user();

            $author = auth()->user();

            if ($author->rol->level <= 51 && $grupGestor = $author->get_field('grup-gestor')) {
                $author = $grupGestor->first()->get_field('gestor');
            }

            $items = $items->filter(function ($item) use ($author, $user) {
                return $item->rol->level <= 51 || $item->id == $user->id;
            });

            /*
            $items = $items->filter(function ($item) use ($author, $user) {
                $grupGestors = $item->get_field('grup-gestor');

                if ($grupGestors && $grupGestors->count()) {
                    $grupGestor = $grupGestors->first();
                    $grupAuthor = $grupGestor->get_field('gestor');
                }
                return (isset($grupAuthor) && $grupAuthor && $grupAuthor->id == $author->id) || $user->id == $item->id;
            }); */
        }

        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'email',
            'toolbar_header' => [
                //'<a target="_blank" href="' . route('admin.users.exportar') . '" class="btn btn-default btn-sm">' . __('Export') . '</a>',
            ],
            'headers' => [
                __('#') => [ 'width' => '5%' ],
                __('Name') => [],
                __('Email') => [],
                __('Empresa') => [],
                __('Departament') => [],
                __('Created at') => [],
                __('Alta') => [],
                __('Role') => [],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => ['fullname()']],
                ['value' => 'email'],
                ['value' => 'get_field("empresa")'],
                ['value' => 'get_field("departament")'],
                ['value' => 'created_at', 'type' => 'date'],
                ['value' => 'first_login', 'type' => 'date'],
                ['value' => 'role', 'translate' => true, 'auth' => true],
            ],
        ));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $level = auth()->user()->rol->level;
        $rols = Rol::where('level', '<=', $level)->get()->sortby('name');

        $cf_groups = CustomFieldGroup::with(['fields'])->where('post_type', $this->section_type)->orderBy('order')->get();

        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'type' => $this->section_type,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'lastname', 'title' => __('Lastname'), 'required' => true],
                ['value' => 'email', 'title' => __('Email'), 'required' => true, 'type' => 'email'],
                ['value' => 'password', 'title' => __('Password'), 'required' => false, 'type' => 'password'],
                ['value' => 'password_confirmation', 'title' => __('Confirm password'), 'required' => false, 'type' => 'password'],
                ['value' => 'role', 'title' => __('Role'), 'type' => 'select', 'required' => true,
                    'datasource' => $rols, 'sel_value' => 'name', 'sel_title' => ['name'], 'sel_search' => true
                ],
            ],
            'cf_groups' => $cf_groups,
        ));
    }


    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function customStore(AdminCreateUserRequest $request) {
        //Si la validación no es correcta redireccionar al formulario con los errores
        $createParams = [
            "name" => $request->name,
            "lastname" => $request->lastname,
            "email" => $request->email,
            "remember_token" => str_random(100),
            "role" => $request->role
        ];

        $hasPwd = false;

        if (isset($request->password) && $request->password && isset($request->password_confirmation)
            && $request->password_confirmation && $request->password == $request->password_confirmation) {
            $hasPwd = true;
            $createParams['password'] = bcrypt($request->password);
        } else {
            $createParams['password'] = bcrypt(str_random(10));
        }

        /*$author = auth()->user();
        
        if (!auth()->user()->isAdmin()) {
            if ($author->rol->level < 51 && $grupGestor = $author->get_field('grup-gestor')) {
                $author = $grupGestor->first()->get_field('gestor');
            }

            $oneGroup = Post::where('type', 'grup-usuari')->where('author_id', $author->id)->first();

            if (!$oneGroup) {
                return redirect()->back()->with('error', __('Es necessari un grup gestor abans de crear usuaris'));
            }
        }*/

        $user = \App\Modules\ArrayPlastics\Models\User::create($createParams);

        if (!$hasPwd) {
            $this->broker()->sendResetLink(['email' => $user->email]);
        }

        if (isset($request->custom_fields)) {
            $this->save_custom_fields(User::find($user->id), $request->custom_fields);
        }

        if ($user) {
            
            Artisan::call('cache:clear');
            
            execute_actions('save_user', $user); // Hook
            return redirect()->route('admin.users.edit', ['id' => $user->id])->with('message', __('User added successfully'));
        } else {
            return redirect()->back()->with('error', __('An error occurred while saving the data'));
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, User $user)
    {
        $custom_user = auth()->user();
        $level = $custom_user->rol->level;

        if ($user->rol->level > $level && !$custom_user->isAdmin()) {
            return back()->with([ "success" => true, "message" => __('You don\'t have permissions to edit this user') ]);
        }

        $rols = Rol::where('level', '<=', $level)->get()->sortby('name');
        
        $cf_groups = CustomFieldGroup::with(['fields'])->where('post_type', $this->section_type)->orderBy('order')->get();

        $permisos = auth()->user()->rol->get_permisos();
        $canCreate = true;

        if (!auth()->user()->isAdmin() && !(!empty($permisos['general']['adminuserscontroller']['create']) && $permisos['general']['adminuserscontroller']['create'])) {
            $canCreate = false;
        }

        if ($custom_user->rol->level < 51 && $grupGestor = $custom_user->get_field('grup-gestor')) {
            $aux_custom_user = $grupGestor->first()->get_field('gestor');

            if ($aux_custom_user) {
                $custom_user = $aux_custom_user;
            }
        }

        $custom_user_id = $custom_user->id;


        return View('Admin::default.edit', array(
            'item' => $user,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'item_type' => $this->section_type,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'lastname', 'title' => __('Lastname'), 'required' => true],
                ['value' => 'email', 'title' => __('Email'), 'required' => true, 'type' => 'email'],
                ['value' => 'password', 'title' => __('Password'), 'required' => false, 'type' => 'password'],
                ['value' => 'password_confirmation', 'title' => __('Confirm password'), 'required' => false, 'type' => 'password'],
                ['value' => 'role', 'title' => __('Role'), 'type' => 'select', 'required' => true,
                    'datasource' => $rols, 'sel_value' => 'name', 'sel_title' => ['name'], 'sel_search' => true
                ],
            ],
            'cf_groups' => $cf_groups,
            'can_create' => $canCreate,
            'custom_user_id' => $custom_user_id
        ));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, User $user)
    {
        $user = $this->fillUser($user, $request);

        // Hook
        execute_actions('save_user', $user);

        if ($user->rol) {
            $permisos = $user->rol->get_permisos();
        } else {
            $permisos = auth()->user()->rol->get_permisos();
        }

        if ($user->isAdmin() || (!empty($permisos['general']['adminuserscontroller']['index']) && $permisos['general']['adminuserscontroller']['index'])) {
            return redirect()->route('admin.users.index')->with([ "success" => true, "message" => __('User updated successfully') ]);
        } else {
            return redirect()->route('admin.users.edit', ['id' => $user->id])->with([ "success" => true, "message" => __('User updated successfully') ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $locale
     * @param User $user
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($locale, User $user)
    {
        $custom_user = auth()->user();
        $level = $custom_user->rol->level;

        if ($user->rol->level > $level && !$custom_user->isAdmin()) {
            return back()->with([ "success" => true, "message" => __('You don\'t have permissions to delete this user') ]);
        }

        $user->delete();
        if ( $user ) return redirect()->back()->with('message', __('User deleted successfully'));
        else return redirect()->back()->with('error', __('An error occurred while deleting the data'));
    }
}