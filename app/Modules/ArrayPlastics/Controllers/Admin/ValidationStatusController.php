<?php


namespace App\Modules\ArrayPlastics\Controllers\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Modules\ArrayPlastics\Models\UserDocument;
use App\Modules\ArrayPlastics\Repositories\GroupsRepository;
use App\User;

class ValidationStatusController extends AdminController
{

    public function index()
    {

        $users = User::get();

        if (!auth()->user()->isAdmin()) {
            $users->load(['metas']);

            $author = auth()->user();

            if ($author->rol->level < 51 && $author->groups->count()) {
                $groups = $author->groups->filter(function ($group) use ($author) {
                    return $group->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->count();
                })->first();

                if ($groups) {
                    $author = $groups->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->first();
                } else {
                    $author = $author->groups->first()->gestors->first();
                }

                if (!$author) {
                    $author = auth()->user();
                }
            }

            $users = app(GroupsRepository::class)->getManagerClients($author);
        }

        $userIds = $users->map(function ($item) {
            return $item->id;
        });

        $docs = UserDocument::with(['post.metas.customField'])
            ->whereIn('user_id', $userIds)
            ->where('validation_status', '<>', 'validat')
            ->get();

        $docs = $docs->filter(function ($doc) {
            return $doc->post && $doc->post->status == 'publish';
        });

        $groupedDocs = $docs->groupBy('user_id')->sortBy(function ($doc) {
            return $doc->first()->user->fullname();
        }, SORT_NATURAL|SORT_FLAG_CASE);

        $groupedByCategoryDocs = $docs->filter(function ($doc) {
            $categoria = $doc->post->get_field('categoria');
            return $categoria && is_object($categoria);
        })->groupBy(function ($doc) {
            $categoria = $doc->post->get_field('categoria');
            return $categoria && is_object($categoria) ? $categoria->id : $categoria;
        })->sortBy(function ($doc) {
            return $doc->first()->post->title;
        }, SORT_NATURAL|SORT_FLAG_CASE);

        return view('Front::admin.status', compact('groupedDocs', 'groupedByCategoryDocs'));
    }
}