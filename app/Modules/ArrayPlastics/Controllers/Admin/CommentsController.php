<?php


namespace App\Modules\ArrayPlastics\Controllers\Admin;


use App\Http\Controllers\Admin\AdminController;
use App\Modules\ArrayPlastics\Models\UserDocumentComment;
use App\Modules\ArrayPlastics\Repositories\GroupsRepository;
use App\Modules\Comments\Models\Comment;
use App\Post;
use App\User;
use Illuminate\Http\Request;

class CommentsController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Comments', [], app()->getLocale());
        $this->section_icon = 'chat_bubble';
        $this->section_route = 'admin.doc-comments';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = UserDocumentComment::get();

        $user = auth()->user();

        if ($user->rol->level >= 50 && !$user->isAdmin()) {
            $author = $user;

            if ($author->rol->level < 51 && $author->groups->count()) {
                $groups = $author->groups->filter(function ($group) use ($author) {
                    return $group->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->count();
                })->first();

                if ($groups) {
                    $author = $groups->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->first();
                } else {
                    $author = $author->groups->first()->gestors->first();
                }

                if (!$author) {
                    return redirect()->route('admin')->with('error', 'No assignat a cap grup');
                }
            }

            $clients = app(GroupsRepository::class)->getManagerClientsAndManagers($author);

            $items = $items->filter(function ($item) use ($clients) {
                return $clients->where('id', $item->user_id)->first();
            });
        }

        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'has_duplicate' => false,
            'headers' => [
                __('#') => [ 'width' => '5%' ],
                __('User') => [ 'width' => '15%' ],
                __('Document') => [ 'width' => '20%' ],
                __('Comment') => [],
                __('Updated at') => [ 'width' => '15%' ],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => ['user', 'fullname()']],
                ['value' => ['post', 'title'] ],
                ['value' => 'comment', 'class' => 'textarea'],
                ['value' => 'updated_at', 'type' => 'date'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return View('Admin::default.create', $this->get_form_fields());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();
        UserDocumentComment::create($data);
        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name added successfully', ['name' => __('Comment')]) ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id) {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id) {
        $item = UserDocumentComment::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');

        return View('Admin::default.edit', array_merge(
            ['item' => $item],
            $this->get_form_fields()
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id) {
        $item = UserDocumentComment::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');

        $data = $request->all();
        $item->update($data);
        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name updated successfully', ['name' => __('Comment')]) ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id) {
        $item = UserDocumentComment::find($id);
        if ( empty($item) ) return redirect()->route($this->section_route . '.index');

        $item->delete();

        return redirect()
            ->route($this->section_route . '.index')
            ->with(['message' => __(':name deleted successfully', ['name' => __('Comment')]) ]);
    }


    protected function get_form_fields() {

        $users = User::get();
        $posts = Post::where('status', 'publish')->withTranslation()->get();

        return array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'userDocument->post_id', 'title' => __('Post'), 'type' => 'select', 'required' => true,
                    'datasource' => $posts,
                    'sel_value' => 'id',
                    'sel_title' => ['title'],
                    'sel_search' => true,
                    'with_post_type' => true,
                ],
                ['value' => 'user_id', 'title' => __('User'), 'type' => 'select', 'required' => true,
                    'datasource' => $users,
                    'sel_value' => 'id',
                    'sel_title' => ['fullname()'],
                    'sel_search' => true,
                    'with_post_type' => false,
                ],
                ['value' => 'comment', 'title' => __('Comment'), 'type' => 'textarea', 'rows' => 10],
            ],
        );
    }
}