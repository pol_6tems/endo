<?php


namespace App\Modules\ArrayPlastics\Controllers\Admin;


use Carbon\Carbon;
use App\Models\Admin\Rol;
use App\Models\CustomPost;
use Illuminate\Http\Request;
use App\Models\Configuracion;
use App\Models\CustomFieldGroup;
use Illuminate\Support\Facades\DB;
use App\Modules\ArrayPlastics\Models\Post;
use App\Modules\ArrayPlastics\Models\User;
use App\Modules\ArrayPlastics\Models\Group;
use App\Repositories\CustomFieldGroupRepository;
use App\Modules\ArrayPlastics\Models\UserDocument;
use App\Modules\ArrayPlastics\Repositories\GroupsRepository;
use App\Modules\ArrayPlastics\Repositories\DocumentsRepository;
use App\Modules\ArrayPlastics\Controllers\Admin\MyPostsController;

class GroupsController extends MyPostsController
{
    protected $post_type = 'group';
    protected $post_type_plural = 'groups';
    protected $section_route = 'admin.groups';
    protected $def_status = 'publish';
    protected $subforms = [];
    protected $post_type_title = 'Grup';

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Grups');
        $this->section_icon = 'supervisor_account';
        $this->section_route = 'admin.groups';
    }

    public function index()
    {
        if (auth()->user()->isAdmin()) {
            $groups = Group::all();
        } else {
            $author = auth()->user();

            if ($author->rol->level < 51 && $author->groups->count()) {
                $groups = $author->groups->filter(function ($group) use ($author) {
                    return $group->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->count();
                })->first();

                if ($groups) {
                    $author = $groups->gestors->filter(function ($gestor) use ($author) {
                        return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                    })->first();
                } else {
                    $author = $author->groups->first()->gestors->first();
                }

                if (!$author) {
                    return redirect()->route('admin')->with('error', 'No assignat a cap grup');
                }
            }

            $groups = $author->groupsGestionats;
        }

        return view("Front::admin.group.index", [
            "groups" => $groups
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        return View('Front::admin.group.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'title', 'title' => __('Titulo'), 'required' => true],
            ],
        ));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = Group::create(['title' => $request->title]);

        $author = auth()->user();

        if ($author->rol->level < 51 && $author->groups->count()) {
            $groups = $author->groups->filter(function ($group) use ($author) {
                return $group->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->count();
            })->first();

            if ($groups) {
                $author = $groups->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->first();
            } else {
                $author = $author->groups->first()->gestors->first();
            }

            if (!$author) {
                return redirect()->route('admin')->with('error', 'No assignat a cap grup');
            }
        }

        $group->users()->attach($author, ['gestor' => 1]);

        if ($author->id != auth()->id()) {
            $group->users()->attach(auth()->user(), ['gestor' => 1]);
        }

        return redirect(route('admin.groups.edit', $group->id))->with(['message' => __(ucfirst($this->post_type) . ' added successfully')]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $group = Group::find($id);

        $author = auth()->user();

        if ($author->rol->level < 51 && $author->groups->count()) {
            $groups = $author->groups->filter(function ($group) use ($author) {
                return $group->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->count();
            })->first();

            if ($groups) {
                $author = $groups->gestors->filter(function ($gestor) use ($author) {
                    return $gestor->rol->level >= 51 && $gestor->id != $author->id;
                })->first();
            } else {
                $author = $author->groups->first()->gestors->first();
            }

            if (!$author) {
                return redirect()->route('admin')->with('error', 'No assignat a cap grup');
            }
        }
        
        if (!is_null($group) && (auth()->user()->isAdmin() || $group->has($author))) {
            $idUsers = $group->users->pluck("id");
            
            $users = User::whereNotIn('id', $idUsers)->whereHas('rol', function($query) {
                $query->where("level", "<", auth()->user()->rol->level);
            })->get();
            
            return View('Front::admin.group.edit-custom', array(
                'group' => $group,
                'users' => $users,
                'section_title' => $this->section_title,
                'section_icon' => $this->section_icon,
                'route' => $this->section_route,
                'rows' => [
                    ['value' => 'title', 'title' => __('Titulo'), 'required' => true],
                ],
            ));
        }

        if ($group) return redirect(route('admin.groups.index'))->with(['error' => __('You don\'t have permissions to edit this :object', ["object" => __("Group")])]);
        else  return redirect(route('admin.groups.index'))->with(['error' => __('This :object doesn\'t exist', ["object" => __("Group")])]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $group = Group::find($id);
        $user = User::find($request->user);

        if ($user && $user->rol->level < 50) {
            foreach ($group->documents()->get() as $document) {
                if (UserDocument::where('post_id', $document->id)->where('user_id', $user->id)->first()) {
                    continue;
                }

                $maxValidationDate = null;

                if ($document->get_field('periode-maxim-validacio')) {
                    $maxValidationDate = Carbon::parse($document->get_field('periode-maxim-validacio'))->startOfDay();
                }

                UserDocument::create([
                    'post_id' => $document->id,
                    'user_id' => $user->id,
                    'validation_status' => 'pendent',
                    'type' => $document->get_field('tipus-validacio') ? $document->get_field('tipus-validacio') : UserDocument::DOC_TYPE_WEB_VALIDABLE,
                    'validation_date' => null,
                    'max_validation_date' => $maxValidationDate ?: null,
                    'expiry_date' => $document->get_field('data-caducitat') ? Carbon::createFromFormat('d/m/Y', $document->get_field('data-caducitat')) : null,
                    'validation_files' => ''
                ]);
            }
        }

        if ($user) {
            $group->users()->attach($user->id);
        }
        
        $group->update(['title' => $request->title]);
        
        return redirect(route('admin.groups.edit', $group->id))->with(['message' => __(ucfirst($this->post_type) . ' updated successfully')]);
    }

    public function remove_user(Request $request, $locale, Group $group, User $user)
    {
        $group->users()->detach($user);

        return json_encode([ 'success' => true ]);
    }

    public function authorize_user(Request $request, $locale, Group $group, User $user)
    {
        $toggle = ($group->users()->wherePivot('user_id', $user->id)->first()->pivot->gestor) ? 0 : 1;
        
        $group->users()->updateExistingPivot($user, ['gestor' => $toggle]);

        return json_encode([ 'success' => true ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $group = Group::find($id);
        $group->delete();
        
        return redirect(route('admin.groups.index'))->with(['message' => __('Post sent to trash')]);
    }
}