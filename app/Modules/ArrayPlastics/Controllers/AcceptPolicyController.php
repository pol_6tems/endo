<?php


namespace App\Modules\ArrayPlastics\Controllers;


use App\Post;
use App\User;
use App\Http\Controllers\Controller;
use App\Modules\ArrayPlastics\Controllers\Traits\hasMultilanguage;

class AcceptPolicyController extends Controller
{
    use hasMultilanguage;

    public function index()
    {
        $user = auth()->user();

        if (!$user) {
            return redirect()->route('login');
        }

        $acceptPolicy = $user->get_field('accepta-politica');

        if ($acceptPolicy) {
            return redirect()->route('index');
        }

        $content = Post::publish()
            ->ofType('page')
            ->whereTranslation('post_name', 'accept-policy')
            ->first();

        $locale = $this->getLocale($user);

        if ($locale == 'ca') {
            $description = $content->description;
        } else {
            $description = $content->get_field('descripcio-' . $locale);
        }

        return view('Front::posts.templates.accept-policy', compact('content', 'locale', 'description'));
    }


    public function accept()
    {
        $user = auth()->user();

        $acceptPolicy = request()->input('accepta-politica');

        if (!$acceptPolicy) {
            return redirect()->back()->with('error', __('Per poder continuar s\'ha d\'acceptar la política'));
        }

        $user->set_field('accepta-politica', 1);

        return redirect()->route('index');
    }
}