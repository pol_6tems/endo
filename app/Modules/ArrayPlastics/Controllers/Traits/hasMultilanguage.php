<?php

namespace App\Modules\ArrayPlastics\Controllers\Traits;

use App\Modules\ArrayPlastics\Models\User;

trait hasMultilanguage {
    private function getLocale(User $user)
    {
        $locale = $user->get_field('idioma');

        if (!$locale) {
            $locale = request()->cookie('user_lang');
        }

        if (!$locale) {
            $lang = substr(request()->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
            $acceptLang = ['ca', 'es', 'en'];
            $locale = in_array($lang, $acceptLang) ? $lang : 'en';
        }

        return $locale;
    }
}