<?php

namespace App;


use App\Models\CustomFieldGroup;
use App\Models\Mensaje;
use App\Models\UserMeta;
use App\Models\Admin\Rol;
use App\Notifications\MyResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ["password_confirmation"];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class, 'author_id');
    }


    public function metas()
    {
        return $this->hasMany(UserMeta::class, 'user_id', 'id');
    }


    public function fullname($first_lastname = false)
    {
        if ($first_lastname)
            return (!empty($this->lastname) ? $this->lastname . ', ' : '') . $this->name;
        else
            return $this->name . (!empty($this->lastname) ? ' ' . $this->lastname : '');
    }

    public function rol()
    {
        return $this->belongsTo(Rol::class, 'role', 'name');
    }

    public function sendPasswordResetNotification($token)
    {
        $notificacio = new MyResetPassword($token);
        $notificacio->user_fullname = $this->fullname();
        $this->notify($notificacio);
    }

    public function contactar_mensajes()
    {
        //return $this->hasMany(Mensaje::class, 'user_id');
        return Mensaje::where('user_id', $this->id)->orWhere('from', $this->id);
    }


    public function isAdmin()
    {
        return $this->rol->name === 'admin';
    }

    public function isUser()
    {
        return $this->rol->name === 'user';
    }


    public function get_field_object($field_name)
    {
        $meta = $this->metas->filter(function ($meta) use ($field_name) {
            $return = $meta->customField && $meta->customField->name == $field_name;

            if (!$return) {
                $return = $meta->field_name == $field_name;
            }

            if (!empty($args) && $return) {
                if (!empty($args['parent_id'])) {
                    $return = $meta->parent_id == $args['parent_id'];
                }

                if (isset($args['order']) && $return) {
                    $return = $meta->order == $args['order'];
                }
            }

            return $return;
        })->first();

        if ($meta && $meta->customField) {
            $meta->name = $meta->customField->name;


            $meta->type = $meta->customField->type;
            $meta->params = $meta->customField->params;
        }

        return $meta;
    }


    public function get_field($field_name)
    {
        $obj = $this->get_field_object($field_name);

        return $obj ? $obj->value : null;
    }

    public function get_avatar_url()
    {
        $user_avatar_default = 'storage/avatars/user-endo.jpg';
        $user_avatar = 'storage/avatars/' . $this->avatar;

        if (file_exists(public_path($user_avatar))) $my_user_avatar = asset($user_avatar);
        else if (file_exists(public_path($user_avatar_default))) $my_user_avatar = asset($user_avatar_default);
        else $my_user_avatar = asset('user-endo.jpg');

        return $my_user_avatar;
    }


    public function cfGroups()
    {
        return CustomFieldGroup::where('post_type', 'user')->with(['customFields'])->get();
    }


    public function set_field($field_name, $value)
    {
        $saved = false;
        // Searching in custom field groups the field_name
        if (!is_array($field_name)) $field_name = [$field_name => $value];

        $this->cfGroups()->each(function ($el) use ($field_name, &$saved) {
            foreach ($field_name as $key => $field) {
                if ($customField = $el->get_field($key)) {
                    $saved = UserMeta::updateOrCreate([
                        'user_id' => $this->id,
                        'post_id' => 'user',
                        'custom_field_id' => $customField->id,
                    ], ['value' => $field,]);
                }
            }
        });

        if ($saved) {
            return $this;
        }

        return $saved;
    }
}
