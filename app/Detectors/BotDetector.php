<?php namespace App\Detectors;


use Browser;
use Illuminate\Http\Request;

/**
 * Custom Bot Detector
 * 
 * makes use of Third Party BrowserDetect And custom User Agents marked as Bots 
 */
class BotDetector
{
    /**
     * Defines the Google Bot used for their Crawler.
     */
    const GOOGLE_BOT_USER_AGENT = 'Googlebot';

	/**
	 * App Request
	 * @var Illuminate\Http\Request;
	 */
	protected $request;

	/**
	 * List of user agent strings marked as Bot
	 * @var array
	 */
	protected $botList = [];


	/**
	 * List of Bot IP's
	 * @var array
	 */
	protected $botIPs = [];

	/**
	 * Constructor
	 * @param array $config 
	 */
	public function __construct($config, Request $request)
	{		
		$this->botlist = $config['botList'];
		$this->botIPs = $config['ips_madafaca'];
		$this->request = $request;
	}

	/**
	 * Tests if current user agent is a bot againts two sources: a third party lib first || our custom bot lib
	 * @return boolean 
	 */
	public function isBot()
	{
		return ( Browser::isBot() || $this->testUserAgent() || $this->checkIfIPExists($this->request->getClientIP()) );
	}

	/**
	 * Matches a string against the BotList
	 * @param  string $stringToMatch the potential string to match
	 * @return boolean
	 */
	protected function matchAgainstBotList($stringToMatch)
	{
		foreach ($this->botlist as $bot) {

			if ( stripos($stringToMatch, $bot) !== false )
			{
				return true;
			}
		}

		return false;
	}

    /**
     * Identifies the Google Bot only.
     *
     * @return bool
     */
    public function isGooglebot()
    {
        return ( stripos( $this->request->header('user-agent'), self::GOOGLE_BOT_USER_AGENT ) !== false );
    }

	/**
	 * Tests Request's user agent
	 * @return boolean 
	 */
	protected function testUserAgent()
	{
		return $this->matchAgainstBotList( $this->request->header('user-agent') );		
	}

	/**
     * Checks whether an IP address exists in a given array of IP addresses
     *
     * @return boolean
     */
    function checkIfIPExists($ip)
    {
        foreach ($this->botIPs as $blacklistedIP) 
        {
            if (strpos($blacklistedIP, '*')) 
            {
                $range = [ 
                    str_replace('*', '0', $blacklistedIP),
                    str_replace('*', '255', $blacklistedIP)
                ];

                if($this->ip_exists_in_range($range, $ip)) return true;
            } 
            else if(strpos($blacklistedIP, '-'))
            {
                $range = explode('-', str_replace(' ', '', $blacklistedIP));

                if($this->ip_exists_in_range($range, $ip)) return true;
            }
            else 
            {
                if (ip2long($blacklistedIP) === ip2long($ip)) return true;
            }
        }
        return false;
    }

	/**
     * Checks whether an IP address exists within a range of IP addresses
     *
     * @return boolean
     */
    protected function ip_exists_in_range(array $range, $ip)
    {
        if (ip2long($ip) >= ip2long($range[0]) && ip2long($ip) <= ip2long($range[1])) 
        {
            return true;
        }
        return false;
    }
}
