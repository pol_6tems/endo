<?php


namespace App\Repositories;


use App\Models\PostMeta;
use DB;

class MetasRepository
{

    public function getPostMeta($postId, $lang, $field_name, $args = null)
    {
        $where = [
            ['post_meta.post_id', $postId],
            ['post_meta.locale', $lang],
            ['custom_fields.name', $field_name],
        ];

        if ( !empty($args) ) {
            if ( !empty($args['parent_id']) ) $where[] = ['post_meta.parent_id', $args['parent_id']];
            if ( isset($args['order']) ) $where[] = ['post_meta.order', $args['order']];
        }

        return PostMeta::where($where)
            ->leftJoin('custom_fields', 'post_meta.custom_field_id', '=', 'custom_fields.id')
            ->where( $where )
            ->select(DB::raw('post_meta.*, custom_fields.name, custom_fields.type, custom_fields.params'))
            ->first();
    }
}