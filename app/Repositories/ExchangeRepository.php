<?php


namespace App\Repositories;


use App\Models\Exchange\CurrencyExchangeRate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;

class ExchangeRepository
{

    public function getExchangeRates($datetime = null)
    {
        $lastDate = $this->getLastDate($datetime);

        return CurrencyExchangeRate::with('currency')->where('date', $lastDate)->get();
    }


    private function getLastDate($datetime = null)
    {
        if (!Schema::hasTable('currency_exchange_rates')) {
            dd('Falten les migracions de les currencies.');
        }

        $query = CurrencyExchangeRate::orderBy('date', 'desc');

        if ($datetime) {
            $query = $query->where('created_at', '<=', $datetime);
        }

        $exchangeRate = $query->first();

        if (!$exchangeRate && !$datetime) {
            dd('No exchange rates!!' . PHP_EOL . 'Configura un laravel schedule al Console/Kernel.php del mòdul: $schedule->command(\'update:exchange-rate\')->everyThirtyMinutes();' . PHP_EOL . 'O executa manualment: php artisan update:exchange-rate');
        }

         return $exchangeRate->date;
    }
}