<?php
namespace App\Repositories;

use App\Post;
use App\Models\Admin\Rol;
use App\Models\CustomFieldGroup;
use Illuminate\Support\Facades\Auth;


class CustomFieldGroupRepository
{

    public function getPostCustomFields(Post $post)
    {
        $cf_groups = CustomFieldGroup::where('post_type', $post->type);

        // Si el paràmetre de tipus es diferent al del post, redirigim al post amb el tipus correcte (EVITAR RENDERITZAR CUSTOM FIELDS QUE NO CORRESPONEN)
        if ($post->type == 'page') {
            if ( !empty($post->template) ) return $cf_groups->where('template', $post->template)->get();
            else return collect();
        } else {
            return $cf_groups->orderBy('order')->get()->filter(function ($value, $key) {
                $fields = $value->fields;
                $value->fields = $fields->filter(function($field, $key) {
                    $param = json_decode($field->params);
                    if (isset($param->role)) {
                        $role = ($param->role != '') ? Rol::where('id', $param->role)->first() : '';
                        return $role->level <= Auth::user()->rol->level;
                    }
                    return true;
                });
                return $value->fields->count() > 0;
            });
        }
    }

}