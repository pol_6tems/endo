<?php


namespace App\Repositories;


use App\Language;
use App\Models\CustomPost;
use App\Models\Media;
use DB;
use App\Post;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;

class PostsRepository
{
    protected $reqCache = [];
    protected $postTransCols = [];
    protected $hasOrder;

    protected $arguments;
    protected $with;
    protected $post_type;
    protected $orderBy;
    protected $pagination;
    protected $status;
    protected $numberposts;
    protected $where;
    protected $orwhere;
    protected $metas_or;
    protected $metas = [];

    private const OPERATORS = ['=', '!=', '>', '<', '<>', '<=', '>=', 'LIKE', 'like'];

    public function find($id, $with = []) {
        $key = 'Find' . md5(serialize($id));

        if (isset($this->reqCache[$key])) {
            return $this->reqCache[$key];
        }

        $this->reqCache[$key] = Post::where('id', $id)->with($with)->first();

        return $this->reqCache[$key];
    }


    public function whereIn($ids, $with = [])
    {
        $key = 'WhereIn' . md5(serialize($ids));

        if (isset($this->reqCache[$key])) {
            return $this->reqCache[$key];
        }

        $this->reqCache[$key] = Post::whereIn('id', $ids)->with($with)->get();

        return $this->reqCache[$key];

    }


    public function getCPWithTranslation($col, $value, $lang = null) {
        $key = 'CPWithTrans' . md5(serialize([$col, $value, $lang]));

        if (isset($this->reqCache[$key])) {
            return $this->reqCache[$key];
        }

        $this->reqCache[$key] = CustomPost::whereTranslation($col, $value, $lang)->first();

        return $this->reqCache[$key];
    }


    public function getDefaultLang()
    {
        $key = 'DefLang';

        if (isset($this->reqCache[$key])) {
            return $this->reqCache[$key];
        }

        $this->reqCache[$key] = Language::where("default", 1)->pluck("code")->first();

        return $this->reqCache[$key];

    }


    public function findMedia($id)
    {
        return Media::find($id);
    }


    public function initializeVariables($args) {
        unset($this->with);
        unset($this->post_type);
        unset($this->pagination);
        unset($this->status);
        unset($this->numberposts);
        unset($this->where);
        unset($this->orwhere);
        unset($this->metas_or);
        $this->metas = [];

        $this->arguments = $args;

        if (!isset($this->hasOrder)) {
            $this->hasOrder = Schema::hasColumn('posts', 'order');
        }
        
        if (isset($args['orderBy'])) $this->orderBy = $args['orderBy'];
        else if ($this->hasOrder) $this->orderBy = [ "col" => "posts.order", "dir" => "asc" ];
        else $this->orderBy = [ "col" => "created_at", "dir" => "asc" ];

        if (is_array($args)) {
            $this->with = $args['with'] ?? [];
            $this->post_type = $args['post_type'] ?? 'post';
            $this->pagination = $args['pagination'] ?? null;
            $this->status = $args['status'] ?? 'publish';
            $this->numberposts = $args['numberposts'] ?? -1;
            $this->where = $args['where'] ?? [ ['locale', app()->getLocale()] ];
            $this->orwhere = $args['orwhere'] ?? [];
            $this->metas_or = $args['metas_or'] ?? [];

            // Initialize Locale
            foreach($this->where as $where) {
                if (!in_array('locale', $where)) {
                    $this->where[] = ['locale', app()->getLocale()];
                    break;
                }
            }

            $meta = $args['metas'] ?? [];
            
            foreach($meta as $w) {
                $this->metas[] = $this->getOperands($w);
            }
        }
    }
    
    public function getPosts($args, $reqCache = true) {
        if ($reqCache) {
            $key = 'GetPosts' . md5(serialize($args));

            if (isset($this->reqCache[$key])) {
                return $this->reqCache[$key];
            }
        }

        $this->initializeVariables($args);

        /*
        if (Cache::has($key) && !$this->pagination) {
            $ids = unserialize(Cache::get($key));

            if (!is_array($ids)) {
                $ids = $ids->values()->all();
            }

            $query = Post::whereIn('id', $ids);

            if ($this->with) {
                $query = $query->with($this->with);
            }
            
            return $query->get();
        }*/

        if (is_array($this->arguments)) {
            $query = Post::withTranslation()
                ->with($this->with)
                ->ofType($this->post_type)
                ->ofStatus($this->status)
                ->leftJoin('users', 'users.id', '=', 'posts.author_id');

            if (count($this->metas) > 0) {
                $query->leftJoin('custom_field_groups', 'custom_field_groups.post_type', '=', 'posts.type')
                    ->leftJoin('custom_fields', 'custom_fields.cfg_id', '=', 'custom_field_groups.id')
                    ->leftJoin('post_meta', 'post_meta.post_id', '=', 'posts.id')
                    ->whereRaw('custom_fields.id = post_meta.custom_field_id');
            }

            execute_actions('before_query_posts', $query);

            $query->where(function($query) {
                $this->setWhereQueries($query);
                $this->setCFQueries($query);
            });

            $query->select(DB::raw("posts.*"));
            $query->distinct();

            if ($this->orderBy) {
                $query->orderBy($this->orderBy['col'], $this->orderBy['dir']);
            } else {
                if (Schema::hasColumn('posts', 'order')) $query->orderBy('order');
            }

            if ($this->pagination && $this->pagination > 0) {
                return $query->paginate($this->pagination);
            } else {
                $posts = $query->limit($this->numberposts)->get();
            }
        } else {
            $post = Post::withTranslation()->ofType($this->arguments)->publish();
            if ($this->orderBy) $post->orderBy($this->orderBy['col'], $this->orderBy['dir']);
            $posts = $post->get();
        }

        $ids = $posts->pluck('id')->all();

        if ($reqCache) {
            $this->reqCache[$key] = $posts;
        }
        // Cache::put($key, serialize($ids), 10);

        return $posts;
    }

    private function getOperands($row) {
        $operand1 = $row[0];
        $operator = (in_array($row[1], self::OPERATORS)) ? $row[1] : '=';
        $operand2 = (in_array($row[1], self::OPERATORS)) ? $row[2] : $row[1];
    
        return [$operand1, $operator, $operand2];
    }

    private function setCFQueries(&$query) {
        if (count($this->metas) > 0) {
            /**
             * Agrupem les querys per meta
             */
            $groups = array();
            foreach($this->metas as $meta) {
                $groups[$meta[0]][] = [$meta[1], $meta[2]];
            }
    
            $metas_or = $this->metas_or;
            /**
             * Si es troben dins un mateix custom field, caldrà buscar-ho amb un where (AND), degut a que buscarà dins la mateixa fila
             * del resultat el valor.
             * 
             * En canvi, si es un altre custom field, caldrà buscar-ho amb un orwhere (OR) i posteriorment agrupar per ID degut a que 
             * retornarà més de una fila per els escollits
             */
            $query->where(function($query) use ($groups, $metas_or) {
                foreach($groups as $key => $t) {
                    $query->orwhere(function($query) use($key, $t, $metas_or) {
                        foreach($t as $l) {
                            $operator = $l[0];
                            $value = (is_numeric($l[1])) ?  DB::raw(floatval($l[1])) : $l[1];
                            $q = [ 'custom_fields.name' => $key, ['post_meta.value', $operator, $value] ];
                            
                            if (in_array($key, $metas_or)) {
                                $query->orwhere($q);
                            } else {
                                $query->where($q);
                            }
                        }
                    });
                }
            });
            $query->distinct();
        }
    }

    private function setWhereQueries(&$query) {
        foreach($this->where as $w) {
            [$operand1, $operator, $operand2] = $this->getOperands($w);

            if (!isset($this->postTransCols[$operand1])) {
                $this->postTransCols[$operand1] = Schema::hasColumn('post_translations', $operand1);
            }

            // Si la columna existeix dins la traducció buscar-ho allà, altrament busca-ho a taula de posts
            if($this->postTransCols[$operand1] && $operand1 != 'id') {
                if ($operator == "LIKE") $query->whereTranslation($operand1, $operand2, app()->getLocale());
                else $query->whereTranslation($operand1, $operand2, app()->getLocale());
            } else {
                $query->where($operand1, $operator, $operand2);
            }
        }
    
        foreach($this->orwhere as $key => $w) {
            [$operand1, $operator, $operand2] = $this->getOperands($w);

            if (!isset($this->postTransCols[$operand1])) {
                $this->postTransCols[$operand1] = Schema::hasColumn('post_translations', $operand1);
            }

            if($this->postTransCols[$operand1]) {
                if ($operator == "LIKE") {
                    if ($key == 0) {
                        $query->whereTranslationLike($operand1, $operand2, app()->getLocale());
                    } else {
                        $query->orWhereTranslationLike($operand1, $operand2, app()->getLocale());
                    }
                } else {
                    if ($key == 0) {
                        $query->whereTranslation($operand1, $operand2, app()->getLocale());
                    } else {
                        $query->orWhereTranslation($operand1, $operand2, app()->getLocale());
                    }
                }
            } else {
                if ($key == 0) {
                    $query->where($operand1, $operator, $operand2);
                } else {
                    $query->orwhere($operand1, $operator, $operand2);
                }
            }
        }
    }
}