<?php


namespace App\Repositories;


class YoutubeRepository
{

    public function getVideoInfo($id)
    {
        $content = file_get_contents("http://youtube.com/get_video_info?video_id=".$id);
        parse_str($content, $ytarr);

        return $ytarr;
    }
}