<?php
/**
 * Created by PhpStorm.
 * User: sergisolsona
 * Date: 08/11/2018
 * Time: 17:53
 */

namespace App\Services;


use App\Models\Traits\CouponTrait;
use App\Theme;
use Carbon\Carbon;
use Exception;

class CouponsService
{
    /**
     * Checks coupon validity
     * 
     * @param $coupon
     * @param $purchaseAmount
     * @param $userId
     * @return bool
     * @throws Exception
     */
    public function validateCoupon($coupon, $purchaseAmount, $userId = null)
    {
        if (!is_object($coupon)) {
            $couponClassName = $this->getCouponModel();
            $coupon = call_user_func([$couponClassName, 'where'], ['code' => $coupon])->first();
        }

        if (!$userId) {
            $userId = auth()->user()->id;
        }

        if (!$userId || !$purchaseAmount) {
            abort(400);
        }

        // Exists
        if (!$coupon) {
            abort(403, __('Lo sentimos este cupón no es válido'));
        }

        // Is active
        if ($coupon->status != 1) {
            abort(403, __('Lo sentimos este cupón no es válido'));
        }

        // Total uses exceeded
        if(!is_null($coupon->total_uses) && $coupon->total_uses <= $coupon->purchases()->count()) {
            abort(403, __('Lo sentimos este cupón no es válido'));
        }

        // Is for platform owner
        if (!is_null($coupon->owner_id) && $coupon->owner_id != $userId) {
            abort(403, __('Lo sentimos este cupón no es válido'));
        }

        // User uses exceeded
        if(!is_null($coupon->uses_per_user)) {
            $userPurchases = $coupon->purchases()->where($coupon->getPurchaseUserColumn(), $userId)->count();

            if ($coupon->uses_per_user <= $userPurchases) {
                abort(403, __('Lo sentimos has llegado al máximo número de usos de este cupón'));
            }
        }

        // Min amount check
        if(!is_null($coupon->min_purchase_amount) && $coupon->min_purchase_amount > $purchaseAmount) {
            abort(403, __('Lo sentimos, el importe de la compra es insuficiente'));
        }

        $today = Carbon::today();

        // Min date check
        if(!is_null($coupon->min_date) && $coupon->min_date > $today) {
            abort(403, __('Lo sentimos, este código aún no es válido'));
        }

        // Max date check
        if(!is_null($coupon->max_date) && $coupon->max_date < $today) {
            abort(403, __('Lo sentimos, este código ya no es válido'));
        }

        return $coupon;
    }


    public function getCouponModel()
    {
        if (\Schema::hasTable('modules')) {
            $theme = Theme::where([['active', true], ['admin', false]])->first();

            $front_theme_path = $theme->name;

            $path = app_path() . "/Modules/$front_theme_path/Models";
            $models = getModels($path);

            foreach ($models as $model) {
                $className = str_replace('/', '\\', ucfirst(substr($model, strpos($model, "app/Modules/$front_theme_path"))));

                $class = new $className;

                $traits = class_uses_deep($class, false);

                if (in_array(CouponTrait::class, $traits)) {
                    return $className;
                    break;
                }
            }
        }

        abort(500, 'No coupon Model found');
    }
}