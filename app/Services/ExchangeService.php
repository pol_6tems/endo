<?php


namespace App\Services;


use App\Repositories\ExchangeRepository;
use Carbon\Carbon;

class ExchangeService
{

    const EUR_ISO_CODE = 'EUR';

    /**
     * Exchange currency to Euros (€)
     *
     * @param $isoCode
     * @param $value
     * @param Carbon|null $datetime
     * @return float|int
     */
    public function toEurValue($isoCode, $value, $datetime = null)
    {
        if (is_string($value) && !is_numeric($value)) {
            $value = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
        }

        if ($isoCode == self::EUR_ISO_CODE) {
            return round($value, 2);
        }

        $exchangeRate = $this->getExchangeRate($isoCode, $datetime);

        return round($value * (1 / $exchangeRate->rate), 2);
    }


    /**
     * Exchange currency from Euros (€)
     *
     * @param $isoCode
     * @param $value
     * @param Carbon|null $datetime
     * @return float|int
     */
    public function fromEurValue($isoCode, $value, $datetime = null)
    {
        if (is_string($value) && !is_numeric($value)) {
            $value = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
        }

        if ($isoCode == self::EUR_ISO_CODE) {
            return round($value, 2);
        }
        
        $exchangeRate = $this->getExchangeRate($isoCode, $datetime);

        return round($value * $exchangeRate->rate, 2);
    }


    /**
     * Clear exchange cache
     */
    public function updateCache()
    {
        cacheable(ExchangeRepository::class, 60, true)->getLastDate();
    }


    private function getExchangeRates($datetime = null)
    {
        $exchangeRate = cacheable(ExchangeRepository::class)->getExchangeRates($datetime);

        if (!$exchangeRate) {
            dd('No exchange rates!!' . PHP_EOL . 'Configura un laravel schedule al Console/Kernel.php del mòdul: $schedule->command(\'update:exchange-rate\')->everyThirtyMinutes();' . PHP_EOL . 'O executa manualment: php artisan update:exchange-rate');
        }

        return $exchangeRate;
    }


    private function getExchangeRate($isoCode, $datetime = null)
    {
        $exchangeRates = $this->getExchangeRates($datetime);

        $exchangeRate = $exchangeRates->filter(function ($exchangeRate) use ($isoCode) {
            return $exchangeRate->currency->iso_code == $isoCode;
        })->first();

        if (!$exchangeRate) {
            dd("No exchange rate for '$isoCode'");
        }

        return $exchangeRate;
    }
}