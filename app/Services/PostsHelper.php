<?php


namespace App\Services;


use App\Facades\PostsRepo;
use App\Models\CustomPost;
use App\Post;
use App\PostTranslation;

class PostsHelper
{


    /**
     * @var array
     */
    private $urls;


    public function __construct()
    {
        $this->urls = [];
    }


    public function getPageUrl($post_name)
    {
        $key = 'page_' . $post_name;

        if (array_key_exists($key, $this->urls)) {
            return $this->urls[$key];
        }

        $post = Post::ofName($post_name)->ofType('page')->first();

        if ( !$post ) {
            $postTranslate = PostTranslation::with('post')->where('post_name', $post_name)->first();
        }


        if ( is_null($post) && !isset($postTranslate) ) {
            $this->urls[$key] = 'javascript:void(0);';

            return 'javascript:void(0);';
        }

        if ( isset($postTranslate)  ) {
            $post = $postTranslate->post;
        }

        $url = $post ? $post->get_url() : 'javascript:void(0);';

        $this->urls[$key] = $url;

        return $url;
    }


    public function getArchiveLink($post_name, $lang = null)
    {
        if ( $lang == null ) $lang = app()->getLocale();

        $key = 'archive_' . $lang . '_' . $post_name;

        if (array_key_exists($key, $this->urls)) {
            return $this->urls[$key];
        }

        $custom_post_original = PostsRepo::getCPWithTranslation('post_type', $post_name);

        if ( $custom_post_original ) {
            $custom_post = PostsRepo::getCPWithTranslation('custom_post_id', $custom_post_original->id, $lang);

            if ( $custom_post ) {
                $url = url('/') . str_replace(
                        '//', '/', '/' . $lang . '/' . $custom_post->translate($lang)->post_type_plural
                    );
            }
        }

        $url = isset($url) ? $url : 'javascript:void(0);';

        $this->urls[$key] = $url;

        return $url;
    }


    public function getPostUrl($post_name)
    {
        $key = 'post_' . $post_name;

        if (array_key_exists($key, $this->urls)) {
            return $this->urls[$key];
        }

        if ( $post = Post::ofName($post_name)->first() ) {
            $url = $post->get_url();
        } else {
            $url = 'javascript:void(0);';
        }

        $this->urls[$key] = $url;

        return $url;
    }
}