<?php

namespace App\Console;

use App\Console\Commands\CreateProject;
use App\Console\Commands\ExchangeUpdater;
use App\Console\Commands\ScheduleList;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ScheduleList::class,
        ExchangeUpdater::class,
        CreateProject::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        if ( \Schema::hasTable('modules') ) {
            foreach (glob(app_path('Modules/*'), GLOB_ONLYDIR) as $dirname) {
                $mod = \DB::table('modules')->select('*')->where('name', basename($dirname))->where('active', 1)->exists();
                if ( $mod && file_exists("$dirname/Console/Kernel.php") ) {
                    app('App\Modules\\' . basename($dirname) . '\Console\Kernel')->schedule($schedule);
                }
            }
        }
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
