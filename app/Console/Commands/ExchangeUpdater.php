<?php

namespace App\Console\Commands;

use App\Models\Exchange\Currency;
use App\Models\Exchange\CurrencyExchangeRate;
use App\Services\ExchangeService;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use SimpleXMLElement;

class ExchangeUpdater extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:exchange-rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update exchange rates table';

    /**
     * @var Client
     */
    private $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    private function init()
    {
        $this->client = new Client();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();

        try {
            $response = $this->client->request('GET', 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml');

            $body = (string)$response->getBody();

            $xml = new SimpleXMLElement($body);
            
            $time = Carbon::parse($xml->Cube->Cube->attributes()->time);

            foreach($xml->Cube->Cube->Cube as $rate) {
                $currency = Currency::where('iso_code', $rate['currency'])->first();

                if (!$currency) {
                    $currency = Currency::create([
                        'iso_code' => $rate['currency']
                    ]);
                }

                $currencyExchangeRate = CurrencyExchangeRate::where('currency_id', $currency->id)
                    ->where('date', $time)
                    ->first();

                if (!$currencyExchangeRate) {
                    CurrencyExchangeRate::create([
                        'currency_id' => $currency->id,
                        'date' => $time,
                        'rate' => $rate['rate']
                    ]);
                }
            }

            // Clear Exchange list cache
            app(ExchangeService::class)->updateCache();
        } catch (GuzzleException $e) {
            $this->error($e->getMessage());
        }
    }
}
