<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Console\GeneratorCommand;

class CreateProject extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:project {name}';


    protected $type = 'Project';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Project directory structure, folder on Modules and on Resources';

    
    protected function getStub()
    {
        return __DIR__.'/stubs/newTheme.stub';
    }

    private $module_structure = [
        'Controllers',
        'database' => [
            'migrations'
        ],
        'Helpers' => [
            'functions.php'
        ],
        'resources' => [
            'lang' => [
                'en.json',
                'es.json',
                'ca.json'
            ],
            'views'
        ],
        'routes.php',
    ];

    private $resources_structure = [
        'assets' => [
            'js' => [
                'app.js',
            ],
            'sass' => [
                'app.scss',
            ],
        ],
        'layouts' => [
            'base.blade.php',
        ],
        'posts' => [
            'templates',
            'archive.blade.php',
            'page.blade.php',
            'single.blade.php',
        ],
        'home.blade.php',
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        
        if ( !File::exists(resource_path('themes/'.$name))) {
            File::makeDirectory(resource_path('themes/'.$name));
            $this->generateDirectories(resource_path('themes/'.$name), $this->resources_structure);
        } else {
        }
        
        if ( !File::exists(app_path('Modules/'.$name))) {
            File::makeDirectory(app_path('Modules/'.$name));
            $this->generateDirectories(app_path('Modules/'.$name), $this->module_structure);
        } else {
        }
        parent::handle();
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Modules\\'.$this->argument('name');
    }

    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);
        $name .= 'Module';

        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.php';
    }

    protected function replaceClass($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name.'Module');
        $stub = str_replace('dummyName', $this->argument('name'), $stub);
        $stub = str_replace('dummyRoute', strtolower($this->argument('name')), $stub);

        return str_replace('DummyClass', $class, $stub);
    }

    /**
     * Generate directory structure
     * @param $basePath, path where all directories will be created in
     * @param $relativePath, recursive array in structure of directories
     */
    function generateDirectories($basePath, $structure)
    {
        //If array, unfold it
        if(is_array($structure)) {
            foreach($structure as $key => $path) {
                //If key is not numeric, add it to foldername, else empty
                $folderName = is_numeric($key) ? '' : '\\' . $key;
                $folderName = str_replace('${name}', $this->argument('name'), $folderName);
                $namespace = $basePath . $folderName;

                if ( !File::exists($namespace) ) {
                    File::makeDirectory($namespace, true);
                }
                $this->generateDirectories($namespace, $path);
            }
        } else {
            try {
                $structure = str_replace('${name}', $this->argument('name'), $structure);
                $path = $basePath . '\\' . $structure;
                $basename = basename($path);
                $content = Str::contains($basename, ".json") ? "{\n}" : "";

                if ( !File::exists($path) ) {
                    // Is a file name
                    if (Str::contains($basename, ".")) {
                        $this->files->put($path, $content);
                    } else {
                        File::makeDirectory($path, true);
                    }
                }
            } catch(\Exception $ex) {
                $this->error("error");
            }
        }
    }
}
