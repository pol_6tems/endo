<?php
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;


function obtenirInicials($text) {
    $words = explode(' ', strtoupper($text));
    $w_txt = '';
    foreach ($words as $word) {
        $w_txt .= substr($word, 0, 1);
    }
    return $w_txt;
}

function mostrarIdiomes($idiomes, $currentLang, $prepend = '') {
    if ( count($idiomes) <= 1 ) return;
?>
    <div class="toolbar">
        <ul class="nav nav-pills nav-pills-primary">
        <?php foreach($idiomes as $language_item) { ?>
                <li class="nav-item <?= ($language_item->code == $currentLang) ? 'active' : '' ?>">
                    <a class="nav-link <?= ($language_item->code == $currentLang) ? 'active' : '' ?> nav-lang-<?= $language_item->code ?>" data-toggle="tab" href="#<?= $prepend . $language_item->code ?>"><?= $language_item->code ?></a>
                </li>
        <?php } ?>
        </ul>
    </div>
<?php
}

function mostrarErrors($errors) {
    if ($errors->count() > 0) { ?>
    <ul>
    <?php foreach($errors->all() as $error) { ?>
        <li><?= $error ?></li>
    <?php } ?>
    </ul>
<?php }
}

// És com el LIKE de MYSQL o si is_contains mirarà si el pattern està dintre del subject
function like_match($pattern, $subject, $is_contains = false) {

    if ( !$is_contains ) {
        $pattern = str_replace('%', '.*', preg_quote($pattern, '/'));
        return (bool) preg_match("/^{$pattern}$/ui", $subject);
    }
    else {
        if ($pattern && $subject) return mb_strpos($subject, $pattern) !== false;
        else return false;
    }
}

/**
 * Agafa una llista de posts i la retorna com una cadena amb un separador Ex: "Cotxe, Moto, Camió"
 */
function separatedList($items, $field = 'title', $separator = ', ', $nItems = -1, $end = "") {
    $aux = '';
    
    if ($items instanceof Illuminate\Database\Eloquent\Collection) {
        $key = 0;
        foreach($items as $key => $item) {
            if ($key == $nItems) break;
            $aux .= $item->translate()->$field.$separator;
        }
        
        $aux = rtrim($aux, $separator);
        if ($items->count() > ($key+1)) {
            $aux .= ' ' . $end;
        }
    }
    return $aux;
}

function is_decimal( $val ) {
    return is_numeric( $val ) && floor( $val ) != $val;
}

function getDayName($number) {
    $names['es'] = [
        0 => 'domingo',
        1 => 'lunes',
        2 => 'martes',
        3 => 'miercoles',
        4 => 'jueves',
        5 => 'viernes',
        6 => 'sábado'
    ][$number];

    return $names[ App::getLocale() ];
}

function getMonthName($number) {
    $names['es'] = [
        1 => 'enero',
        2 => 'febrero',
        3 => 'marzo',
        4 => 'abril',
        5 => 'mayo',
        6 => 'junio',
        7 => 'julio',
        8 => 'agosto',
        9 => 'setiembre',
        10 => 'octubre',
        11 => 'noviembre',
        12 => 'diciembre',
    ][$number];

    return $names[ App::getLocale() ];
}

/**
 * Converteix dates en format dd/mm/YYYY en YYYY-mm-dd per Carbon
 */
function convertDate($date) {
	if ($date) {
		$aux = explode("/", $date);
		if (count($aux) > 2) {
			$aux[0] = sprintf('%02d', $aux[0]);
			$aux[1] = sprintf('%02d', $aux[1]);
		} else return null;

		return Carbon::parse(join("-", $aux));
    }
	return null;
}


if (! function_exists('toEur')) {

    /**
     * Exchange currency to Euros (€)
     *
     * @param $isoCode
     * @param $value
     * @param Carbon|null $datetime
     * @return Application|mixed
     */
    function toEur($isoCode, $value, $datetime = null) {
        return app(\App\Services\ExchangeService::class)->toEurValue($isoCode, $value, $datetime);
    }
}


if (! function_exists('fromEur')) {

    /**
     * Exchange currency from Euros (€)
     *
     * @param $isoCode
     * @param $value
     * @param Carbon|null $datetime
     * @return Application|mixed
     */
    function fromEur($isoCode, $value, $datetime = null) {
        return app(\App\Services\ExchangeService::class)->fromEurValue($isoCode, $value, $datetime);
    }
}


if (! function_exists('format_money')) {

    /**
     * Formats a number as a currency string with 2 decimals
     *
     * @param $value
     * @param int $divide Default 1. Set 100 if value saved as integer at DB
     * @return int|string
     */
    function format_money($value, $divide = 1) {
        // Sanitize strings...
        if (is_string($value) && !is_numeric($value)) {
            $value = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
        }

        $value = $value / $divide;

        if (is_numeric($value) && floor($value) == $value) {
            return (int)$value;
        }

        return money_format('%.2n', $value);
    }
}


if (! function_exists('validateCoupon')) {

    /**
     * Checks coupon validity
     *
     * @param $coupon
     * @param $purchaseAmount
     * @param $userId
     * @return bool
     * @throws Exception
     */
    function validateCoupon($coupon, $purchaseAmount, $userId = null) {
        return app(\App\Services\CouponsService::class)->validateCoupon($coupon, $purchaseAmount, $userId);
    }
}


if (! function_exists('getModels')) {

    /**
     * Get all models in a path
     *
     * @param $path
     * @return array
     */
    function getModels($path)
    {
        $out = [];
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.' or $result === '..') continue;
            $filename = $path . '/' . $result;
            if (is_dir($filename)) {
                $out = array_merge($out, getModels($filename));
            } else {
                $out[] = substr($filename, 0, -4);
            }
        }
        return $out;
    }
}


if (! function_exists('class_uses_deep')) {

    /**
     * Get all object traits
     *
     * @param $class
     * @param bool $autoload
     * @return array
     */
    function class_uses_deep($class, $autoload = true)
    {
        $traits = [];
        do {
            $traits = array_merge(class_uses($class, $autoload), $traits);
        } while ($class = get_parent_class($class));
        foreach ($traits as $trait => $same) {
            $traits = array_merge(class_uses($trait, $autoload), $traits);
        }
        return array_unique($traits);
    }
}

if (! function_exists('remove_style_tags')) {
    /**
     * Remove all style attributes on html string
     *
     * @param String $text
     * @return String
     */
    function remove_style_tags($text) {
        return preg_replace('/(<[^>]+) style=".*?"/i', '$1', $text);
    }
}


if (! function_exists('getQueries')) {

    /**
     * Shows a complete query with the bindings to stick directly on MSql
     *
     * @param Builder $builder
     * @return void
     */
    function getQueries(Builder $builder)
    {
        $addSlashes = str_replace('?', "'?'", $builder->toSql());
        return vsprintf(str_replace('?', '%s', $addSlashes), $builder->getBindings());
    }
}



if (! function_exists('validateValue')) {
    /**
     * Use the Laravel validation to validate a value.
     *
     * @param $value the value to test
     * @param $rules Laravel standard rules: 'required|numeric'
     * @return bool
     */
    function validateValue($value, $rules)
    {
        //needs to be an assoc
        $assoc = ['value' => $value];

        $validator = Validator::make($assoc, [
            'value' => $rules,
        ]);

        return !$validator->fails();
    }
}

function isVowel ($char) {
    return in_array($char, ['a', 'e', 'i', 'o', 'u', 'à', 'á', 'è', 'é', 'ì', 'í', 'ó', 'ò', 'ú', 'ù']);
}

function getJSON($url, $store = false) {
    $client = new \GuzzleHttp\Client(['verify' => false]);
    $response = $client->request('GET', $url);
	$etag = $response->getHeaderLine("ETag");
    $result = utf8_encode($response->getBody()->getContents());
	
	if ($store) {
		$filename = env("PROJECT_NAME");
        if ($etag) $filename .= '/' . str_replace('"', "",  $etag);
        $filename .=  '-' . basename($url);
		
		if (! Storage::exists($filename) ) {
			Storage::put($filename, $result);
		}
		$result = Storage::get($filename);
	}
		
    return json_decode( $result, true );
}

function getURLparam($url, $param) {
	$val = null;
	$url = parse_url($url, PHP_URL_QUERY);
	$parameters = explode("&", $url);
	foreach( $parameters as $parameter ) {
		$aux = explode("=", $parameter);
		if ($aux[0] == $param) {
			$val = $aux[1];
		}
	}
	return $val;
}

function pagination() {
    $parts = parse_url(url()->previous());
    
    if (isset($parts['query'])) {
        parse_str($parts['query'], $query);

        if (isset($query) && isset($query['page'])) {
            $currentPage = $query['page'];
            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
        }
    }
}

function filterAndSort($collection, $field) {
    $filtered = $collection->filter(function($item) use($field) {
        $data = $item->get_field($field);
        if ($data) return Date::now()->timestamp <= Date::createFromFormat("d/m/Y", $data)->timestamp;
    });

    return sortByCF($filtered, $field, "desc");
}

function sortByCF($collection, $field, $order = 'asc') {
    if ($order == 'asc') {
        return $collection->sortBy(function($value, $key) use($field) {
            $data = $value->get_field($field);
            if ($data) return Date::createFromFormat("d/m/Y", $data)->timestamp;
        });
    } else {
        return $collection->sortByDesc(function($value, $key) use($field) {
            $data = $value->get_field($field);
            if ($data) return Date::createFromFormat("d/m/Y", $data)->timestamp;
        });
    }
}

function mayusWords($input, $exceptions = array()) {
    $exceptions = array_map(function($item) { return strtolower($item); }, $exceptions);
    $result = "";
    $words = explode(" ", ucwords(strtolower($input)));
    foreach ($words as $word) {
        if (in_array(strtolower($word), $exceptions)) {
            $result .= " ".strtolower($word);
        } else {
            $startException = false;
            foreach ($exceptions as $exception) {
                if (strpos(strtolower($word), $exception) === 0) {
                    $startException = true;

                    $result .= " " . strtolower($word);
                    break;
                }
            }

            if (!$startException) {
                $result .= " " . $word;
            }
        }
    }
    return trim($result);
}
