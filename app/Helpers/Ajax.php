<?php
use App\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Funció per registrar-se i loginar-se a través de la API de Google
 *
 * @param array $parameters
 * @return void
 */
function ajax_no_priv_signInGoogle() {}
function signInGoogle($parameters) {
    try {
		if (!User::where('token', $parameters['token'])->exists()) {
			$user = [
				'name' => $parameters['name'],
				'email' => $parameters['email'],
				'password' => bcrypt($parameters['token']),
				'role' => 'user',
				'lastname' => isset($parameters['lastname']) ? $parameters['lastname'] : '',
				'token' => $parameters['token'],
            ];
            
			User::create($user);
		} else {
			return json_encode([ 'success' => true ]);
		}
		
		if (Auth::attempt(['email' => $parameters['email'], 'password' => $parameters['token']], true)) {
			return json_encode([ 'success' => true ]);
		}
		
		return json_encode([ 'success' => false ]);
    } catch (Exception $e) {
        return json_encode([
            'success' => false,
            'error' => $e->getMessage()
        ]);
    }
}

function ajax_no_priv_loginGoogle() {}
function loginGoogle($parameters) {
    try {
        if ($user = Auth::attempt(['email' => $parameters['email'], 'password' => $parameters['password']], true)) {
            return json_encode([
                'success' => true,
                'user' => Auth::user()
            ]);
        } else {
            return json_encode(['success' => false]);
        }
    } catch (Exception $e) {
        return json_encode([
            'success' => false,
            'error' => $e->getMessage()
        ]);
    }
}

function ajax_no_priv_loginUser() {}
function loginUser($parameters) {
    try {
        if ($user = Auth::attempt(['email' => $parameters['email'], 'password' => $parameters['password']], true)) {
            return json_encode([
                'success' => true,
                'user' => Auth::user()
            ]);
        } else {
            return json_encode(['success' => false]);
        }
    } catch (Exception $e) {
        return json_encode([
            'success' => false,
            'error' => $e->getMessage()
        ]);
    }
}

function ajax_no_priv_registerUser() {}
function registerUser($parameters) {
    try {
        $datetime = DateTime::createFromFormat('d/m/Y', sprintf("%02d", $parameters['dia_nacimiento']) . '/' . sprintf("%02d", $parameters['mes_nacimiento']) . '/' . $parameters['ano_nacimiento']);
        $birthday = Carbon::parse($datetime->format('d-m-Y'));
        
        $user = [
            'name' => $parameters['nombre'],
            'email' => $parameters['email'],
            'password' => bcrypt($parameters['password']),
            'role' => 'user',
            'lastname' => $parameters['apellidos'],
            'city' => $parameters['poblacion'],
            'country' => $parameters['pais'],
            'gender' => $parameters['genero'],
            'birthdate' => $birthday
        ];

        $user = User::create($user);

        if ( !Auth::user() ) {
            $login = Auth::loginUsingId($user->id);
        }

        return json_encode($user);
    } catch (Exception $e) {
        return json_encode($e->getMessage());
    }
}

function ajax_no_priv_enviar_mensaje() {}
function ajax_enviar_mensaje() {}
function enviar_mensaje($params) {
    return app('App\Http\Controllers\MensajesController')->enviar_mensaje($params);
}

function ajax_no_priv_enviar_email() {}
function ajax_enviar_email() {}
function enviar_email($params) {
    return app('App\Http\Controllers\MensajesController')->enviar_email($params);
}


function ajax_reorderRow() {}
function reorderRow($params) {
    $posts = json_decode($params);
    foreach($posts as $post) {
        $p = Post::find($post->post_id);
        $new_pos = $post->newPosition;
        $p->order = $new_pos;
        $p->save();
    }
    return true;
}




function ajax_no_priv_getParentChildrenData() {}
function ajax_getParentChildrenData() {}
function getParentChildrenData($params) {
    $parent = isset($params['parent']) ? $params['parent'] : null;
    $post_type = $params['post_type'];
    $custom_field = $params['customField'];

    $posts = get_posts(['post_type' => $post_type, 'metas' => [ [$custom_field, '=', $parent] ]]);

    return json_encode($posts);
}


function ajax_setCoupon() {}
function setCoupon($params) {
    $code = $params['code'];
    $amount = $params['amount'];

    $coupon = validateCoupon($code, $amount);

    if ($coupon) {
        $valueUserCurrency = toUserCurrency('EUR', $coupon->value, false);
        $currencySymbol = userCurrency();

        return response()->json(['code' => $code, 'value' => $coupon->value, 'value_user_currency' => $valueUserCurrency, 'currency_symbol' => $currencySymbol, 'message' => __('Cupón aplicado correctamente')]);
    }

    abort(400);
}
