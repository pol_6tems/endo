<?php
/**
 * Created by PhpStorm.
 * User: sergisolsona
 * Date: 22/05/2019
 * Time: 17:59
 */

use App\Repositories\YoutubeRepository;
use App\Support\Cacheable;
use App\Theme;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

if (! function_exists('route_name')) {
    /**
     * @param null $as
     *
     * @return bool|string|null
     */
    function route_name($as = null)
    {
        if (!request()->route()) {
            return null;
        }

        $route = request()->route()->getAction();
        if (array_key_exists('as', $route)) {
            return $as ? $as == $route['as'] : $route['as'];
        }
    }
}

if (! function_exists('endo_view')) {
    /**
     * @param string $view
     * @param array $data
     * @param array $mergeData
     * @return bool|string|null
     */
    function endo_view($view = null, $data = [], $mergeData = [])
    {
        $actionName = request()->route()->getActionName();
        $actionExploded = explode('@', $actionName);

        $controllerPathExploded = explode('\\', $actionExploded[0]);

        $controllerName = last($controllerPathExploded);
        $method = $actionExploded[1];

        $front_theme_path = Theme::where([['active', true], ['admin', false]])->pluck('path')->first();

        $className = "\App\Modules\\$front_theme_path\Controllers\\$controllerName";
        
        if (!class_exists($className)) {
            return view($view, $data, $mergeData);
        }

        $newView =  app($className)->{$method}();

        if ($newView instanceof RedirectResponse) {
            return $newView;
        }

        if ($newView instanceof View) {
            return view($newView->getName(), array_merge($data, $newView->getData()), $mergeData);
        }

        return view($view, $data, $mergeData);
    }
}


if (! function_exists('cut_text')) {
    
    function cut_text($string, $length = 80, $etc = '...', $break_words = false, $middle = false, $p = false)
    {
        $string = strip_tags($string);

        if ($length == 0) {
            $ret_string = '';
        } else if (mb_strlen($string) > $length) {
            $length -= min($length, strlen($etc));

            if (!$break_words && !$middle) {
                $string = wordwrap($string, $length);
                $string = explode("\n", $string)[0];
                $string = mb_substr($string, 0, $length, "utf-8");
            }
            if (!$middle) {
                $ret_string = trim(mb_substr($string, 0, $length, "utf-8")) . $etc;
            } else {
                $ret_string = mb_substr($string, 0, $length / 2, "utf-8") . $etc . mb_substr($string, -$length / 2, "utf-8");
            }
        } else {
            $ret_string = $string;
        }

        if ($p) {
            $ret_string = '<p>' . $ret_string . '</p>';
        }

        return $ret_string;
    }
}


if (! function_exists('cacheable')) {

    /**
     * Caches object calls.
     *
     * @param $object
     * @param int $cacheTime
     * @param bool $freshCache
     * @return Application|mixed
     */
    function cacheable($object, $cacheTime = 60, $freshCache = false) {
        if (!$cacheTime) {
            $cacheTime = 60;
        }

        return new Cacheable($object, $cacheTime, $freshCache);
    }
}


if (!function_exists('get_youtube_id')) {

    function get_youtube_id($url)
    {
        $id = null;

        preg_match('/v=([^&#]{5,})/', $url, $match);

        if (count($match)) {
            $id = $match[1];
        }

        if (!$id) {
            preg_match('/youtu\.be\/([^&#]{5,})/', $url, $match);

            if (count($match)) {
                return $match[1];
            }
        }

        return $id;
    }
}

if ( !function_exists('get_header') ) {
    function get_header() {
        execute_actions('get_header');
    }
}

if ( !function_exists('get_home_page') ) {
    function get_home_page() {
        $url = null;
        
        // Configuracions
        $configurations = array();
        $config_items = \App\Models\Configuracion::orderby('order')->get();
        foreach ( $config_items as $c ) {
            $configurations[$c->key] = [
                'obj' => $c,
                'title' => $c->title,
                'name' => $c->key,
                'type' => $c->type,
                'value' => $c->value,
                'file' => $c->file,
            ];
        }

        if ( !empty($configurations['home_page']['value']) ) {
            $url = \App\Post::get_url_by_post_id( $configurations['home_page']['value'] );
        }

        if ( empty($url) ) $url = route('index');

        return $url;
    }
}


if ( !function_exists('is_json') ) {
    function is_json($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}


if ( !function_exists('endoEncrypt') ) {
    function endoEncrypt($tokenArray) {
        return app(\App\Support\Encrypter::class)->encrypt($tokenArray);
    }
}

if ( !function_exists('endoDecrypt') ) {
    function endoDecrypt($token) {
        return app(\App\Support\Encrypter::class)->decrypt($token);
    }
}

if ( !function_exists('getYoutubePreviewUrl') ) {

    function getYoutubePreviewUrl($url)
    {
        $id = get_youtube_id($url);

        if ($id) {
            return "https://img.youtube.com/vi/$id/mqdefault.jpg";
        }
    }
}


if (!function_exists('getYoutubePreviewData')) {
    function getYoutubePreviewData($url)
    {
        $id = get_youtube_id($url);

        if ($id) {
            $video = new stdClass();
            $video->embedLink = "https://www.youtube.com/embed/$id";
            $video->previewImg = "https://img.youtube.com/vi/$id/mqdefault.jpg";

            $ytarr = cacheable(YoutubeRepository::class)->getVideoInfo($id);

            if (array_key_exists('title', $ytarr)) {
                $video->previewTitle = $ytarr['title'];
            }

            $details = json_decode($ytarr['player_response'], true)['videoDetails'];

            if (!isset($video->previewTitle) && array_key_exists('title', $details)) {
                $video->previewTitle = $details['title'];
            }

            if (array_key_exists('shortDescription', $details)) {
                $video->previewShortDescription = $details['shortDescription'];
            }

            return $video;
        }
    }
}

if (!function_exists('formatSizeUnits')) {
    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' B';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' B';
        } else {
            $bytes = '0 B';
        }

        return $bytes;
    }
}