<?php

$ACTIONS = array();

function add_action($action, $function) {
    global $ACTIONS;
    $ACTIONS[$action][] = $function;
}

function execute_actions($action, $params = null) {
    global $ACTIONS;
    if ( !empty($ACTIONS[$action]) ) {
        foreach ( $ACTIONS[$action] as $function ) {
            do_action($function, $params);
        }
    }
}

function do_action($action, $params = null) {
    if ( !is_array($params) ) $params = [$params];
    if ( function_exists($action) ) {
        if ( like_match('show_%', $action) ) {
            $view_params = call_user_func_array($action, $params);
            echo show_view($view_params);
        }
        else return call_user_func_array($action, $params);
    }
}

function show_view($params) {
    if ($params) {
        $view_name = $params['view'];
        if ( view()->exists('Front::' . $view_name) ) $view_name = 'Front::' . $view_name;
        return view($view_name, array(
            'params' => $params['params'],
        ))->render();
    }
}

