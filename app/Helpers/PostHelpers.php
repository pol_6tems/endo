<?php

use \App\Post;
use App\PostTranslation;
use App\Models\CustomPost;
use App\Facades\PostsRepo;
use App\Repositories\PostsRepository;

function get_excerpt( $content, $length = 40, $more = '...' ) {
	$excerpt = strip_tags( trim( $content ) );
	$words = str_word_count( $excerpt, 2 );
	if ( count( $words ) > $length ) {
		$words = array_slice( $words, 0, $length, true );
		end( $words );
		$position = key( $words ) + strlen( current( $words ) );
		$excerpt = substr( $excerpt, 0, $position ) . $more;
	}
    return $excerpt;
}

/**
 * @param $id
 * @param bool $plural
 * @return Post|null
 */
function get_post($id, $plural = false) {
    if (is_numeric($id)) {
        return Post::find($id);
    }

    if (is_string($id)) {
        $posts = Post::publish()->whereTranslation('post_name', $id, app()->getLocale())->get();

        if ($plural) {
            foreach ($posts as $post) {
                $cp = $post->customPostTranslations->first();
                if ($plural == $cp->post_type_plural) {
                    return $post;
                }
            }
        }

        return $posts->first();
    }
}

/**
 * Accepta els seguents paràmetres:
 * 
 * @post_type
 * 
 * o
 * 
 * @args
 *      @post_type => string
 *      @numberposts => integer
 *      @orwhere => array
 *      @where => array
 *      @whereTrans => array
 *      @orwhereTrans => array
 *      @pagination => integer
 *      @metas => array
 *      @with => array
 * 
 * Exemple:
 * 
 *  get_posts([
 *      'post_type' => 'voluntariado',
 *      'numberposts' => 6,
 *      'orwhere' => [
 *          ['field-trans', '=', 'value']
 *       ],
 *      'where' => [
 *          ['field-trans', '=', 'value']
 *       ],
 *      'whereTrans' => [
 *          ['field-trans', '=', 'value']
 *       ],
 *      'orwhereTrans' => [
 *          ['field-trans', '=', 'value']
 *       ],
 *      'pagination' => 6,
 *      'metas' => [
 *          ['custom-field-name', '=', 'value']
 *      ],
 *      'with' => ['author']
 *  ])
 * 
 */

function get_posts($args, $reqCache = true) {
    return PostsRepo::getPosts($args, $reqCache);
}

if (!function_exists('get_page_url')) {
    function get_page_url($post_name)
    {
        return PostsHelper::getPageUrl($post_name);
    }
}

if (!function_exists('get_archive_link')) {
    function get_archive_link($post_name, $lang = null) {
        return PostsHelper::getArchiveLink($post_name, $lang);
        /*if ( $lang == null ) $lang = \App::getLocale();
        
        $custom_post_original = CustomPost::whereTranslation('post_type', $post_name)->first();

        if ( $custom_post_original ) {
            $custom_post = CustomPost::whereTranslation('custom_post_id', $custom_post_original->id, $lang)->first();

            if ( $custom_post ) {
                return url('/') . str_replace(
                    '//', '/', '/' . $lang . '/' . $custom_post->translate($lang)->post_type_plural
                );
            }
        }*/
    }
}

if (!function_exists('get_post_url')) {
    function get_post_url($post_name)
    {
        return PostsHelper::getPostUrl($post_name);
        /*if ( $post = Post::ofName($post_name)->first() ) {
            return $post->get_url();
        } else {
            return 'javascript:void(0);';
        }*/
    }
}
