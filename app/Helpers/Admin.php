<?php

use App\Models\Opcion;
use App\Models\Admin\Rol;
use App\Models\Configuracion;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\PostsController;

// Does not support flag GLOB_BRACE
function rglob($pattern, $flags = 0) {
    $files = glob($pattern, $flags); 
    foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
        $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
    }
    return $files;
}

function isDevIp() {
    $dev_ips = array('91.126.218.219', '80.64.37.127');
    return in_array($_SERVER['REMOTE_ADDR'], $dev_ips);
}

function getDeclaredMethods($className) {
    $reflector = new ReflectionClass($className);
    $methodNames = array();
    $lowerClassName = strtolower($className);
    foreach ($reflector->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
        if (strtolower($method->class) == $lowerClassName) {
            $methodNames[] = $method->name;
        }
    }
    return $methodNames;
}

function getPostsControllerMethods() {
    return getDeclaredMethods(PostsController::class);
}

/**
 * Mètode per evitar col·locar a la vista \App\Models\Admin\Rol...
 *
 * @return void
 */
function getRoles() {
    return Rol::orderby('level', 'ASC')->get();
}

function getMenu() {
    return AdminController::getMenu();
}

function isProEnv() {
    return config('app.env') == 'production';
}

$_item;
function global_item($v) {
    global $_item;
    $_item = $v;
}

function get_configurations() {
    /* CONFIGURATIONS */
    $configurations = array();
    
    $config_items = Configuracion::orderby('order')->get();
    foreach ( $config_items as $c ) {
        $configurations[$c->key] = [
            'title' => $c->title,
            'name' => $c->key,
            'type' => $c->type,
            'value' => $c->value,
            'file' => $c->file,
        ];
    }
    /* end CONFIGURATIONS */
    return $configurations;
}

/**
 * Retrive Configuration/options/config/env with the format: 
 * apis.google_maps => By Group
 * option.google_maps => By Option
 *
 * @param [type] $key
 * @return void
 */
function setting($key) {
    $groups = explode(".", $key);
    $group = 'general';

    if (count($groups) > 1) {
        $group = array_shift($groups);
    }
    $key = array_shift($groups);

    return  Opcion::where('key', $key)->first()->value ??
            Configuracion::where(['key' => $key, 'group' => $group])->pluck('value')->first() ??
            config("$group.$key") ??
            env("$key");
}


function get_option($key) {
    if ($option = Opcion::where('key', $key)->first()) {
        return $option->value;
    }
    
    return $option;
}

function admin_bar() {

    global $_item;

    if ( !Auth::check() ) return;
    
    $permisos = Auth::user()->rol->get_permisos();

    if ( empty($permisos) && !Auth::user()->isAdmin() ) return;

    $configurations = get_configurations();

    if ( empty($configurations['show_admin_bar']['value']) || ( !empty($configurations['show_admin_bar']['value']) && $configurations['show_admin_bar']['value'] != true ) ) {
        return;
    }

    /* TE PERMIS ADMIN */
    $obj = array(
        'grup' => 'general',
        'controller' => 'adminadmincontroller',
        'method' => 'index',
    );
    $parameters = '';
    $te_permis = AdminController::menu_te_permisos( $permisos, $obj, $parameters );

    if (!$te_permis) {
        $te_permis = AdminController::menu_te_permisos( $permisos,
            [
                'grup' => 'general',
                'controller' => 'adminadmincontroller',
                'method' => 'clearCache',
            ]
            , $parameters );
    }

    if ( !$te_permis && !Auth::user()->isAdmin() ) return;
    /* end TE PERMIS ADMIN */

    /* TE PERMIS POST */
    $_te_post = false;
    if ( !empty($_item) ) {
        $obj = array(
            'grup' => 'General',
            'controller' => 'adminpostscontroller',
            'method' => 'index',
        );
        $parameters = 'post_type=' . $_item->type;
        $te_permis = AdminController::menu_te_permisos( $permisos, $obj, $parameters );
        $_te_post = $te_permis || Auth::user()->isAdmin();
    }
    /* end TE PERMIS POST */

    echo view('Admin::front_admin_bar', [
        '_config' => $configurations,
        '_te_post' => $_te_post,
        '_item' => $_item,
        '_current_user' => Auth::user(),
    ])->render();
}

function get_app_name() {
    $configurations = get_configurations();
    
    if ( empty($configurations['app_name']['value']) )
        $app_name = config('app.name', 'Endo');
    else
        $app_name = $configurations['app_name']['value'];
    
    return $app_name;
}

function encrypt_token($data) {
    // FIRST CONVERT THE $userDataArray INTO JSON STRING
    $userDataJSON   = json_encode($data);

    // NEXT CREATE A PASSWORD AND METHOD TO USE FOR ENCRYPTION...
    $pass           = 'suDoLetMeIn';
    $method         = 'aes-128-cbc';
    $initVector     = "0123456789012345";    // MUST BE 16 BYTES LONG...

    // NOW ENCRYPT THE DATA...
    $encrypted      = openssl_encrypt($userDataJSON, $method, $pass, false, $initVector);

    return $encrypted;
}

function decrypt_token($encrypted) {
    // NEXT CREATE A PASSWORD AND METHOD TO USE FOR ENCRYPTION...
    $pass           = 'suDoLetMeIn';
    $method         = 'aes-128-cbc';
    $initVector     = "0123456789012345";    // MUST BE 16 BYTES LONG...

    // ATTEMPT TO DECRYPT THE DATA
    $decrypted      = openssl_decrypt($encrypted, $method, $pass, false, $initVector);

    return $decrypted;
}

/**
 * Return the classname of a given path
 * 
 * converts
 * 
 * /home/vagrant/code/endo/app/Modules/.../.../... .php
 * 
 * into
 * 
 * App\Modules\...\...\...
 *
 * @param String $path
 * @return String
 */
function get_className($path) {
    $className = 'App'. str_replace(app_path(), "", $path);
    $className = str_replace("/", "\\", $className);
    $className = str_replace(".php", "", $className);
    
    return $className;
}