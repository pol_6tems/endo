<?php

use App\Models\Notification;

function get_notifications() {
    $notifications = array();
    $items = Notification::where('active', true)->orderby('order')->get();
    foreach ($items as $item) {
        if (strpos($item->controller,'App\\') !== 0) {
            $controller = "App\\Http\\Controllers\\" . $item->controller;
        } else {
            $controller = $item->controller;
        }

        $method = $item->method;
        $parameters = $item->parameters;
        try {
            $notification = $controller::$method($parameters, $item->name);
            if ( !empty($notification) ) $notifications[] = $notification;
        } catch(Exception $e) { }
    }
    return $notifications;
}