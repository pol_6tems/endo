<?php


namespace App\Support;


abstract class Presenter
{
    /**
     * @var mixed
     */
    protected $entity;


    /**
     * @param $entity
     */
    function __construct($entity)
    {
        $this->entity = $entity;
    }


    /**
     * Allow for property-style retrieval
     *
     * @param $property
     * @return mixed
     */
    public function __get($property)
    {
        if (isset($this->entity->{$property})) {
            return $this->entity->{$property};
        }

        if (method_exists($this, $property)) {
            return $this->{$property}();
        }
    }


    public function __call($method, $parameters)
    {
        if (method_exists($this->entity, $method)) {
            return $this->entity->{$method}($parameters);
        }

        throw new \Exception();
    }
}