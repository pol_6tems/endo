<?php
/**
 * Created by PhpStorm.
 * User: sergisolsona
 * Date: 10/11/16
 * Time: 8:42
 */

namespace App\Support;

use Illuminate\Support\Facades\Cache;

class Cacheable
{

    private $object;

    private $caheTime;

    /**
     * @var bool
     */
    private $freshCache;


    public function __construct(string $object, $cacheTime = 60, $freshCache = false)
    {
        $this->object   = app($object);
        $this->caheTime = $cacheTime;
        $this->freshCache = $freshCache;
    }


    /**
     * Override any call to an object to cache it's result
     *
     * @param $method
     * @param $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        $redisOnline = true;
        $key = get_class($this->object) . '-' . $method . '-' . md5(serialize($parameters)) . '-serialized';

        if (!$this->freshCache && Cache::has($key) && strpos(route_name(), 'admin') === false) {
            return unserialize(Cache::get($key));
        }

        $results = call_user_func_array([$this->object, $method], $parameters);

        if($redisOnline) {
            Cache::put($key, serialize($results), $this->caheTime);
        }

        return $results;
    }
}