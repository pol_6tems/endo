<?php


namespace App\Support;


class Encrypter
{

    private $secret;

    public function __construct()
    {
        $this->secret = env('ETR_SECRET', '9UpeiZvaHk7O41oTHyN5cUByBPbLHsjCWEkE7c96');
    }


    /**
     * @param $tokenArray
     * @return string
     */
    public function encrypt($tokenArray)
    {
        $encrypted = openssl_encrypt(json_encode($tokenArray), "AES-256-CBC", hash('sha256', $this->secret), 0, substr(hash('sha256', $this->secret), 0, 16));
        return urlencode(base64_encode($encrypted));
    }


    /**
     * @param $token
     * @return mixed
     */
    public function decrypt($token)
    {
        $data = openssl_decrypt(base64_decode(urldecode($token)), "AES-256-CBC", hash('sha256', $this->secret), 0, substr(hash('sha256', $this->secret), 0, 16));
        return json_decode($data, true);
    }
}