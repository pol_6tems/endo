<?php
/**
 * Created by PhpStorm.
 * User: sergisolsona
 * Date: 29/08/2019
 * Time: 18:07
 */

namespace App\Support;


use App\Theme;

class Translator extends \Illuminate\Translation\Translator
{

    private $theme;


    /**
     * Get the translation for a given key from the JSON translation files.
     *
     * @param string $key
     * @param array $replace
     * @param null $locale
     * @return string|array|null
     */
    public function getFromJson($key, array $replace = [], $locale = null)
    {
        $mLocale = $locale ?: $this->locale;

        if (!isProEnv() && !$this->customHas($key, $locale, false)/* && !parent::get($key, $replace, $locale, false)*/) {
            if (isset($this->theme) || \Schema::hasTable('themes') ) {
                // Themes - Front
                if (isset($this->theme) || $this->theme = Theme::where([['active', true], ['admin', false]])->first()) {
                    $frontPath = $this->theme->path;
                    $moduleDir = __DIR__ . "/../Modules/$frontPath";
                    $moduleDirExists = file_exists($moduleDir);

                    if (!$moduleDirExists) {
                        $frontName = $this->theme->name;
                        $moduleDir = __DIR__ . "/../Modules/$frontName";
                        $moduleDirExists = file_exists($moduleDir);
                    }

                    if ($moduleDirExists) {
                        $filename = $moduleDir. "/resources/lang/$mLocale.json";

                        if (file_exists($filename)) {
                            $trans = json_decode(file_get_contents($filename), true);
                        } else {
                            $trans = [];

                            if (!file_exists($moduleDir. "/resources/lang")) {
                                if (!file_exists($moduleDir. "/resources")) {
                                    mkdir($moduleDir . "/resources");
                                }

                                mkdir($moduleDir. "/resources/lang");
                            }
                        }

                        $trans[$key] = $key;
                        ksort($trans);

                        file_put_contents($filename, json_encode($trans, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                    }
                }
            }
        }

        return parent::getFromJson($key, $replace, $locale);
    }


    private function customHas($key, $locale = null, $fallback = true)
    {
        $line = $this->customGet($key, [], $locale, $fallback);

        return !is_null($line);
    }


    private function customGet($key, array $replace = [], $locale = null, $fallback = true)
    {
        list($namespace, $group, $item) = $this->parseKey($key);

        // Here we will get the locale that should be used for the language line. If one
        // was not passed, we will use the default locales which was given to us when
        // the translator was instantiated. Then, we can load the lines and return.
        $locales = $fallback ? $this->localeArray($locale)
            : [$locale ?: $this->locale];

        foreach ($locales as $locale) {
            if (! is_null($line = $this->getLine(
                $namespace, $group, $locale, $item, $replace
            ))) {
                break;
            }
        }

        if (!isset($line) || is_null($line)) {
            $this->load('*', '*', $locale);
            $locales = $this->localeArray($locale);

            foreach ($locales as $locale) {
                $line = $this->loaded['*']['*'][$locale][$key] ?? null;
                if (! is_null($line)) {
                    break;
                }
            }
        }

        return $line;
    }
}