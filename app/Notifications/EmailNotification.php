<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\App;

class EmailNotification extends Notification
{
    use Queueable;

    protected $params;

    /**
     * Create a new notification instance.
     *
     * @param  stdClass  $params
     * @return void
     */
    public function __construct($params) {
        $this->params = $params;
    }

    public function check_email() {
        $params = $this->params;
        $from = isset($params->from) ? $params->from : null;

        if ( $from ) {
            $from_email = $from->email;
        } else if ( !empty($params->mensaje) && !empty( $params->mensaje->user ) ) {
            $from_email = $params->mensaje->email;
        } else {
            $from_email = config("mail.from.address");
        }

        return $from_email != $params->usuario->email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // TODO: Revisar Refactor!
        $params = $this->params;

        $localize = App::getLocale();

        $from_name = '';
        $from_email = '';
        $from = isset($params->from) ? $params->from : null;

        if ( $from ) {
            $from_name = $from->fullname();
            $from_email = $from->email;
        } else if ( !empty($params->mensaje) && !empty( $params->mensaje->user ) ) {
            return false;
        }

        if ( !$from_name && !$from_email && !empty($params->mensaje) && !empty( $params->mensaje->name ) ) {
            $from_name = $params->mensaje->name;
            $from_email = $params->mensaje->email;
        }

        if ( !empty($params->email_id) ) {
            $email = \App\Models\Email::find($params->email_id);
            if ( empty($email) ) return false;

            $mail_message = new MailMessage();
            
            // Subject
            ob_start();
            eval("echo " . $email->subject . ';');
            $subject_value = ob_get_clean();
            $mail_message->subject( $subject_value );

            // Greeting
            if ( !empty($email->greeting) ) {
                ob_start();
                eval("echo " . $email->greeting . ';');
                $greeting_value = ob_get_clean();
                $mail_message->greeting( $greeting_value );
            }
            
            // Lines
            foreach ( $email->get_lines() as $line ) {
                if ( empty($line['line']) ) continue;
                
                
                if ( empty($line['is_action']) ) {
                    // Line
                    ob_start();
                    eval("echo " . $line['line'] . ';');
                    $line_value = ob_get_clean();

                    $mail_message->line( $line_value );
                } else {
                    // Action
                    $line_parts = explode('##', $line['line']);

                    ob_start();
                    eval("echo " . $line_parts[0] . ';');
                    $line_value = ob_get_clean();
                    
                    $line_url = '';
                    if ( !empty($line_parts[1]) ) {
                        ob_start();
                        eval("echo " . $line_parts[1] . ';');
                        $line_url = ob_get_clean();
                    }

                    $mail_message->action( $line_value, $line_url );
                }
            }
            // end Lines
            
            // Salutation
            if ( !empty($email->salutation) ) {
                ob_start();
                eval("echo " . $email->salutation . ';');
                $salutation_value = ob_get_clean();
                $mail_message->salutation( $salutation_value );
            }

            if (isset($params->attach_data['output']) && $params->attach_data['filename']) {
                $mail_message->attachData($params->attach_data['output'], $params->attach_data['filename']);
            }

            if (isset($params->attach_data_multiple) && is_array($params->attach_data_multiple) && count($params->attach_data_multiple) > 0) {
                foreach($params->attach_data_multiple as $pdf) {
                    $mail_message->attachData($pdf['output'], $pdf['filename']);
                }
            }

            return $mail_message;
        }
        else {
            return false;
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

}
