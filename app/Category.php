<?php

namespace App;

use App\Post;
use App\User;
use App\Models\CustomPost;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function posts() {
        return $this->morphedByMany(Post::class, 'categorizable');
    }

    public function customPosts() {
        return $this->morphedByMany(CustomPost::class, 'categorizable');
    }

    public function users() {
        return $this->morphedByMany(User::class, 'categorizable');
    }
}
