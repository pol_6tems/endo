<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use URL;

class LanguageController extends Controller
{
    public function setLocale($locale = 'en') {
        
        $locales = ['en', 'es', 'ca'];

        if ( !in_array($locale, $locales) ) {
            $locale = 'ca';
        }
        Session::put('locale', $locale);
        \App::setLocale($locale);
        if ( !empty( Session::get('current_url') ) ) {
            $translations = json_decode(file_get_contents(base_path() . "/resources/lang/$locale.json"), true);
            if ( !empty($translations) ) {
                $key = array_search(Session::get('current_url'), $translations);
                if ( !empty($key) ) {
                    Session::put('current_url', $key);
                }
            }
            
            return redirect(url( __(Session::get('current_url')) ));
        } else {
            return redirect(url(URL::previous()));
        }
    }
}
