<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use \Response;
use Auth;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {
        $action = request()->action;
        $parameters = request()->parameters;        
        $method = (request()->method) ? request()->method : 'json';
        
        $access_code = $this->get_access_code($action);
        if ( $access_code == 200 ) {
            if ($method == 'json') {
                return response()->json([
                    'action' => $action,
                    'method' => !empty( request()->method ) ? request()->method : 'json',
                    'result' => $action($parameters),
                    'success'=> true
                ]);
            } else {
                return $action($parameters);
            }
        } else {
            return response()->json([
                'action' => $action,
                'success'=> false
            ], $access_code);
        }
    }

    protected function get_access_code($action) {
        $code = 404;
        if ( function_exists($action) ) {
            if ( function_exists('ajax_no_priv_' . $action) || ( function_exists('ajax_' . $action) && Auth::check() ) )
                $code = 200;
            else
                $code = 403;
        }
        return $code;
    }
}