<?php

namespace App\Http\Controllers;

use App\Facades\PostsRepo;
use View;
use App\Post;
use App\Models\CustomPost;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    protected $api;

    protected function is_single($post_name) {
        // Busquem primer si hi ha una pàgina i a continuació si hi ha un Post
        $page = Post::publish()
                        ->ofType('page')
                        ->whereTranslation('post_name', $post_name)
                        ->first();

        if ( !empty($page) ) return $page;
        else {
            $posts = Post::publish()
                            ->whereTranslation('post_name', $post_name, app()->getLocale())
                            ->get();

			$segments = request()->segments();
			
			$trobat = false;
			foreach($posts as $post) {
				// Mirem si dins de la url hi ha el plural del post_type que ha de tenir el post name
				foreach(array_reverse($segments) as $segment) {
					$cp = $post->customPostTranslations->first();
					if ($segment == $cp->post_type_plural) {
						$trobat = true;
						break;
					}
				}
				if ($trobat) return $post;
			}
			
			return $posts->first();
        }
    }

    protected function is_archive($post_name) {
        return PostsRepo::getCPWithTranslation('post_type_plural', $post_name, \App::getLocale());
    }

    /**
     * Coincideix amb totes les rutes.
     * Itera per totes les rutes començant pel final i va buscant si coincideix amb algun template.
     * 
     * Finalitza quan es troba una pàgina, que en mostra o bé el template o bé la pàgina de defecte
     */
    public function process_routes($locale) {
        $params = request()->segments();
        $lang = app()->getLocale();
        
        $is_archive = false;
        $is_single = false;
        $is_taxonomy = false;

        $item = null;
        $trans = null;
        $error = false;

        // si la URL te un /d/ vol dir que són parametres de cerca
        $uri_is_parameters = in_array('d', $params);

        $firstParam = array_shift($params); // Treure primer segment (lang)
        
        if (Auth::check() && Auth::user()->isAdmin()) {
            $this->api = $firstParam == 'api'; // API
        }

        $firstParamsIsNothing = false;

        foreach ( array_reverse($params) as $key => $param ) {
            $single = $this->is_single($param);
            $archive = $this->is_archive($param);

            if ( !empty($single) && !$uri_is_parameters ) {
                if ( $key == 0 ) {
                    $item = $single;
                    $trans = $single->getTranslation($lang);
                    $is_single = true;

                    $currentUrl = request()->url();
                    $singleUrl = $item->get_url();

                    if ($currentUrl != $singleUrl) {
                        $queryString = request()->getQueryString();
                        return redirect($singleUrl . ($queryString ? preg_replace("/\[(.*)\]/", '[]', urldecode($queryString)) : ''), 301);
                    }
                }
            } else if ( !empty($archive) && !$is_single ) {
                if ($key == 0) {
                    ini_set('memory_limit', '256M');

                    $trans = $archive->getTranslation($lang);
                    $item = Post::with(['metas.customField', 'translations.media'])
                                    ->publish()
                                    ->ofType($trans->post_type)
                                    ->get();

                    $is_archive = true;
                } else {
                    return redirect(Post::get_archive_link($param));
                }
            } else if ( !$error ) {
                $error = true;

                if ($param == array_reverse($params)[0]) {
                    $firstParamsIsNothing = true;
                }
            }
        }

        $couldBeLanding = false;
        if (count($params) == 2 && $firstParamsIsNothing) {
            $couldBeLanding = true;
        }

        $error = !$is_single && !$is_archive;

        if ( $error ) return response()->view('Admin::errors.404', [], 404);

        if ( $is_single ) {
            return $this->show($item);
        }

        if ( $is_archive ) {
            $routes = [ "Front::posts.archive-{$trans->post_type}", "Front::posts.archive-{$trans->post_type_plural}", "Front::posts.archive"];
            $data   = [ 'items' => $item ];
            

            if ($this->api) return $item;
            
            return view()->first($routes, $data);
        }

        return response()->view('Admin::errors.404', [], 404);
        
    }

    public function show($item, $trans = null) {
        $routes = ['Admin::errors.404'];
        $data = [];
        $lang = app()->getLocale();

        if ( empty($trans) ) $trans = $item->translate($lang);

        global_item($item);

        if ( $item->type == 'page' ) {
            $trans  = $item->getTranslation();
            $routes = [ "Front::posts.templates.{$item->template}", 'Front::posts.page' ];
            $data   = [ "item" => $item, "page" => $item, "trans" => $trans ];
        } else {
            $trans  = $item->getTranslation();
            $routes = [ "Front::posts.single-{$item->type}", "Front::posts.single" ];
            $data   = [ "item" => $item, "trans" => $trans ];
        }
        
        if ($this->api) return $item;

        return view()->first($routes, $data);
    }
}