<?php

namespace App\Http\Controllers;

use App\Facades\PostsRepo;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Session;
use App;
use View;
use Request;
use Route;
use App\Post;
use App\Models\CustomPost;
use App\Models\Configuracion;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /*
    public function __construct()
    {
        // Do nothing
    }
    */
    

    /**
     * S'ha substituit el __constructor per callAction que s'executa abans de
     * cada funció degut a que els constructors es criden abans dels middlwares,
     * de manera que el middleware de Locale no era cridat i no havia col·locat
     * bé la variable d'idioma. Llavors, al fer el getTranslation() ho feia sobre
     * el primer idioma, no sobre l'actual de la URL.
     *
     * @param [type] $method
     * @param [type] $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __construct()
    {
        /* CONFIGURATIONS */
        $configurations = self::get_configurations();
        /* end CONFIGURATIONS */

        /* LANGUAGE */
        $locale = app()->getLocale();
        $language_name = 'Español';
        $this->_languages = DB::table('languages')->where('active', true)->get();
        foreach ($this->_languages as $key => $language) {
            if ($language->code == $locale) $language_name = $language->name;
            $language->url = $this->get_url($language->code);
        }
        /* end LANGUAGE */

        view()->share('language_code', $locale);
        view()->share('_languages', $this->_languages);
        view()->share('language_name', $language_name);
        view()->share('_admin', config('admin_theme'));
        view()->share('_front', config('front_theme'));
        view()->share('_config', $configurations);

    }


    public function get_url($lang) {
        $route_parameters = Route::current() != null ? Route::current()->parameters : [];
        $route_parameters['locale'] = $lang;
        if ( Route::currentRouteName() == 'posts' ) {
            // ROUTES POSTS
            $request_uri = request()->getRequestUri();
            $path_info = request()->getPathInfo();
            $web_parameters = str_replace($path_info, '', $request_uri);
            $post_name = str_replace('//', '',
                str_replace('/' . App::getLocale(), '',
                    $path_info
                )
            );
            if ( strpos($post_name, '/') == 0 ) $post_name = substr($post_name, 1);

            $post = \DB::table('post_translations')->where([
                ['post_name', $post_name],
                ['locale', App::getLocale()]
            ]);
            if ( $post->exists() ) {
                $post_id = $post->first()->post_id;
                return Post::get_url_by_post_id($post_id, $lang);
            }

            $q = \DB::table('custom_post_translations')->where([
                ['post_type_plural', $post_name],
                ['locale', App::getLocale()]
            ]);
            if ( $q->exists() ) {
                $cp = PostsRepo::getCPWithTranslation('custom_post_id', $q->first()->custom_post_id, $lang);
                return Post::get_archive_link( $cp->post_type_plural, $lang ) . $web_parameters;
            }
            if (strpos($post_name, '/') !== false) {
                $parts = explode('/', $post_name);
                $post_name = $parts[1];

                $post = \DB::table('post_translations')->where([
                    ['post_name', $post_name],
                    ['locale', App::getLocale()]
                ]);

                if ( $post->exists() ) {
                    $post_id = $post->first()->post_id;
                    return Post::get_url_by_post_id($post_id, $lang);
                }
            }
        } else if ( Route::has(Route::currentRouteName()) ) {
            if ( self::like_match('{locale}/admin%', Route::current()->getAction()['prefix']) ) {
                // ROUTES ADMIN
                return route(Route::currentRouteName(), $route_parameters);
            } else {
                // ROUTES FRONT
                $request_uri = request()->getRequestUri();
                $path_info = request()->getPathInfo();
                $web_parameters = str_replace($path_info, '', $request_uri);

                $savedLocale = App::getLocale();
                App::setLocale($lang);
                //$no_lang = str_replace('/' . $savedLocale . '/', '', $path_info);
                $ruta = __('/' . Route::currentRouteName() );
                App::setLocale($savedLocale);

                return url('/') . "/$lang" . $ruta . $web_parameters;
            }
        } else {
            // NO ROUTES
            return null;
        }
    }

    public static function like_match($pattern, $subject)
	{
		$pattern = str_replace('%', '.*', preg_quote($pattern, '/'));
		return (bool) preg_match("/^{$pattern}$/i", $subject);
    }
    
    public static function get_configurations()
	{
		$configurations = array();
        $config_items = Configuracion::orderby('order')->get();
        foreach ( $config_items as $c ) {
            $configurations[$c->key] = [
                'obj' => $c,
                'title' => $c->title,
                'name' => $c->key,
                'type' => $c->type,
                'value' => $c->value,
                'file' => $c->file,
            ];
        }
        return $configurations;
	}
    
}
