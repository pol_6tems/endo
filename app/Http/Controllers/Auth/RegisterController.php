<?php

namespace App\Http\Controllers\Auth;

use App\Models\CustomFieldGroup;
use App\Models\UserMeta;
use App\Theme;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    protected $section_type;

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');

        $this->section_type = 'user';
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $userData = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ];

        if (array_key_exists('lastname', $data)) {
            $userData['lastname'] = $data['lastname'];
        }

        $cf_groups = CustomFieldGroup::where('post_type', $this->section_type)->orderBy('order')->get();

        $user = User::create($userData);

        foreach ($cf_groups as $cf_group) {
            foreach ($cf_group->fields as $field) {
                if (array_key_exists($field->name, $data)) {
                    UserMeta::create([
                        'user_id' => $user->id,
                        'custom_field_id' => $field->id,
                        'value' => $data[$field->name],
                        'post_id' => $this->section_type
                    ]);
                }
            }
        }

        return $user;
    }


    public function redirectTo()
    {
        $front_theme_path = Theme::where([['active', true], ['admin', false]])->pluck('path')->first();
        if ($front_theme_path == 'Stocktextil') {
            $locale = request()->get('locale', app()->getLocale());
            return route('index', ['locale' => $locale]);
        }

        return $this->redirectTo;
    }
}
