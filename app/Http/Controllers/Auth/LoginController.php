<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Theme;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use View;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        if ( View::exists('Front::auth.login') ) return View('Front::auth.login');
        else if ( View::exists('Admin::auth.login') ) return View('Admin::auth.login');
        else return View('auth.login');
    }


    public function redirectTo()
    {
        $front_theme_path = Theme::where([['active', true], ['admin', false]])->pluck('path')->first();

        if ($front_theme_path == 'Stocktextil') {
            $user = auth()->user();
            $locale = $user->get_field('locale');
            $locale = $locale ?: request()->get('locale', app()->getLocale());

            return route('index', ['locale' => $locale]);
        }

        if ($front_theme_path == 'Marimon') {
            return route('admin', ['locale' => app()->getLocale()]);
        }

        return $this->redirectTo;
    }
}
