<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\UserMeta;
use App\Post;
use Auth;

class UserMetaController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        if ( empty($data['respostes']) ) {
            return response()->json(['error'=>'Done!'], 401);
        }
        $resultats = array();
        foreach ($data['respostes'] as $resposta) {
            if (strpos($resposta['name'], 'resultats') === false) continue;
            $idx = str_replace(']', '',
                str_replace('resultats[', '', $resposta['name'])
            );
            $resultats[$idx] = $resposta['value'];
        }
        unset($data['respostes']);
        $data['value'] = json_encode($resultats);

        // Elimina UserMeta existent
        UserMeta::where([
            ['user_id', $data['user_id']],
            ['post_id', $data['post_id']],
            ['custom_field_id', $data['custom_field_id']],
        ])->delete();

        unset($data['_token']);
        if ( UserMeta::create($data) ) {
            return response()->json(['success'=>'Done!']);            
        } else return response()->json(['error'=>'Done!'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
