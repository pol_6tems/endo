<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Email;

class EmailsController extends AdminController
{

    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Emails');
        $this->section_icon = 'email';
        $this->section_route = 'admin.emails';
    }

    public function index() {
        $items = Email::orderby('name')->get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'toolbar_header' => [],
            'has_duplicate' => true,
            'headers' => [
                __('Name') => [],
                __('Subject') => [],
                __('Updated at') => [],
            ],
            'rows' => [
                ['value' => 'name'],
                ['value' => 'subject'],
                ['value' => 'updated_at', 'type' => 'date'],
            ],
        ));
    }

    public function create() {
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'subject', 'title' => __('Subject'), 'required' => true, 'class' => 'text-console'],
                ['value' => 'greeting', 'title' => __('Greeting'), 'class' => 'text-console'],
                ['type' => 'subform_inline', 'value' => 'Admin::default.subforms.email_lines', 'help' => '$params->mensaje = Mensaje<br>$params->usuario = User<br>$params->from = Auth::user()<br>$from_name = From Name'],
                ['value' => 'salutation', 'title' => __('Salutation'), 'class' => 'text-console'],
            ],
        ));
    }

    public function store(Request $request) {
        $data = $request->all();
        $data['lines'] = json_encode($data['lines']);
        Email::create($data);
        return redirect()->route($this->section_route . '.index')->with([ 'message' => __(':name added successfully', ['name' => __('Email')]) ]);
    }

    public function show($locale, $id) {}

    public function edit($locale, $id) {
        $item = Email::find($id);
        
        if ( empty($item) ) return redirect()->route($this->section_route . '.index')->with([ 'message' => __(":name not found", ['name' => __('Email')]) ]);

        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'subject', 'title' => __('Subject'), 'required' => true, 'class' => 'text-console'],
                ['value' => 'greeting', 'title' => __('Greeting'), 'class' => 'text-console'],
                ['type' => 'subform_inline', 'value' => 'Admin::default.subforms.email_lines', 'help' => '$params->mensaje = Mensaje<br>$params->usuario = User<br>$params->from = Auth::user()<br>$from_name = From Name'],
                ['value' => 'salutation', 'title' => __('Salutation'), 'class' => 'text-console'],
            ],
        ));
    }

    public function update(Request $request, $locale, $id) {
        $item = Email::find($id);
        
        if ( empty($item) ) return redirect()->route($this->section_route . '.index')->with([ 'message' => __(":name not found", ['name' => __('Email')]) ]);

        $data = $request->all();
        $data['lines'] = json_encode($data['lines']);

        if ( $item->update($data) ) {
            $msg = __(':name updated successfully', ['name' => __('Email')]);
        } else {
            $msg = __('Error saving data');
        }
        return redirect()->route('admin.emails.edit', $id)->with([ 'message' => $msg ]);
    }

    public function destroy($locale, $id) {
        $item = Email::find($id);
        if ( !empty($item) && $item->delete() ) {
            return response()->json(['success'=>'Done!']);
        }
        return response()->json(['error'=>'Done!'], 401);
    }

    public function duplicate($locale, $id) {
        $item = Email::find($id);
        
        $data = [
            'name' => $item->name . ' (copy)',
            'subject' => $item->subject,
            'greeting' => $item->greeting,
            'lines' => $item->lines,
            'salutation' => $item->salutation,
        ];

        $new = Email::create($data);

        if ( !empty($new) ) {
            $msg = __(':name duplicated successfully', ['name' => __('Email')]);
        } else {
            $msg = __('Error saving data');
        }
        return redirect()->route('admin.emails.index')->with([ 'message' => $msg ]);
    }
}
