<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\Post;
use App\User;
use App\Theme;
use App\Models\PostMeta;
use App\Models\Admin\Rol;
use App\Models\CustomPost;
use App\Models\CustomField;
use Illuminate\Http\Request;
use App\Models\Configuracion;
use App\Models\CustomFieldGroup;
use App\Repositories\CustomFieldGroupRepository;

class PostsController extends AdminController
{
    protected $post_type = 'post';
    protected $post_type_plural = 'posts';
    protected $section_route = 'admin.posts';
    protected $def_status = 'publish';
    protected $subforms = [];
    protected $post_type_title = 'Post';

    /**
     * S'ha substituit el __constructor per callAction que s'executa abans de
     * cada funció degut a que els constructors es criden abans dels middlwares,
     * de manera que el middleware de Locale no era cridat i no havia col·locat
     * bé la variable d'idioma. Llavors, al fer el getTranslation() ho feia sobre
     * el primer idioma, no sobre l'actual de la URL.
     *
     * @param [type] $method
     * @param [type] $parameters
     * @return void
     */
    public function callAction($method, $parameters)
    {
        $post_type = request()->post_type;
        if ( !empty($post_type) ) {
            if ( !in_array($post_type, ['post', 'page']) ) {
                $custom_post = CustomPost::whereTranslation('post_type', $post_type)->first();
                if (is_null($custom_post)) return abort(404);
                else {
                    $this->post_type = $custom_post->getTranslation()->post_type;
                    $this->post_type_plural = $custom_post->getTranslation()->post_type_plural;
                    $this->post_type_title = $custom_post->getTranslation()->title;
                }
            } else if ( $post_type == 'page' ) {
                $this->post_type = 'page';
                $this->post_type_plural = 'pages';
                $this->post_type_title = __('Page');
            }
        }
        
        $plural = $this->post_type_plural;

        if ( !in_array($this->post_type, ['post', 'page']) ) {
            $plural = !empty($custom_post) ? $custom_post->title_plural : $plural;
        }
        
        view()->share('section_route', $this->section_route);
        view()->share('post_type', $this->post_type);
        view()->share('post_type_plural', $plural);
        view()->share('subforms', $this->subforms);
        view()->share('post_type_title', $this->post_type_title);

        return parent::callAction($method, $parameters);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parent = (request()->parent) ? request()->parent : null;
        $status = (request()->status) ? request()->status : $this->def_status;

        $home_page = Configuracion::get_config('home_page');
        $home_page_id = !empty($home_page) ? $home_page->id : 0;

        $builder = Post::ofType($this->post_type)
                        ->ofStatus($status)
                        ->withTranslation();
        
        if ( !$status ) {
            // Revisar
            // Todo: Revisar-ne l'us
            if ( $parent ) {
                $parent_id = $parent;
                $parent = Post::ofType($this->post_type)->where('id', $parent_id)->get();
                if ($parent) {
                    $items = $parent->merge(Post::ofType($this->post_type)->ofType($this->post_type)->OfParent($parent_id)->get());
                } else {
                    $items = $builder->OfParent($parent_id);
                }
            } else {
                $items = $builder->with(['parent', 'customPostTranslations.customPost.translations']);
            }
        }

        // View::share ( 'post_types', $post_types );

        $items = $builder->orderByDesc('created_at')->get();
        $num_trashed = Post::onlyTrashed()->ofType($this->post_type)->count();
        $num_pending = Post::ofType($this->post_type)->pending()->count();
        $num_draft = Post::ofType($this->post_type)->draft()->count();

        return view('Admin::posts.index', compact('items', 'status', 'num_trashed', 'num_pending', 'num_draft', 'home_page_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $templates = $this->get_templates();

        $cf_groups = CustomFieldGroup::where(['post_type' => $this->post_type, 'template' => null])->orderBy('order')->get();
        
        // Filter Custom Fields by Role
        $cf_groups->map(function($group) {
            return $group->fields = $group->fields->filter(function ($field) {
                $params = json_decode($field->params);
                if (isset($params->role)) {
                    $cf_level = Rol::find($params->role)->level;
                    return Auth::user()->rol->level >= $cf_level;
                } else {
                    return true;
                }
            });
        });

        $valor = '';

        $params = null;
        if ( !in_array($this->post_type, ['post', 'page']) ) {
            $cp = CustomPost::whereTranslation('post_type', $this->post_type)->first();
            if ( !empty($cp) ) $params = $cp->params;
        }

        $users = User::all();

        return view('Admin::posts.create', compact('valor', 'templates', 'cf_groups', 'params', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $custom_fields = array();
        if ( !empty($data['custom_fields']) ) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code]) ) {
                $sanitize_title = str_slug($data[$lang->code]['title'], '-');
                $aux = $sanitize_title;
                $i = 1;
                while (Post::whereTranslation('post_name', $aux, $lang->code)->exists()) {
                    $aux = $sanitize_title . '-' . $i;
                    $i++;
                }

                $sanitize_title = $aux;
                $data[$lang->code]['post_name'] = $sanitize_title;
                $data[$lang->code]['media_id'] = ( !empty($data['media_id']) ) ? $data['media_id'] : null;

                if (!isset($data['template']) || isset($data[$lang->code]['template'])) $data['template'] = isset($data[$lang->code]['template']) ? $data[$lang->code]['template'] : null;
                else $data[$lang->code]['template'] = $data['template'] ?? $data[$lang->code]['template'];
            }
        }
        
        $data['author_id'] = Auth::id();
        $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : 0;

        // Check if is revision
        if ( $data['status'] != 'draft' && Auth::user()->rol->level < Post::ROL_LEVEL_LIMIT_REVISIO ) {
            $data['status'] = 'pending';
        }

        $lastPost = Post::ofType($data['type'])
                            ->orderBy('id', 'desc')
                            ->first();

        if ($lastPost && !is_null($lastPost->order)) {
            $data['order'] = $lastPost->order + 1;
        }

        $post = Post::create($data);


        
        /* Custom Fields */
        foreach ($custom_fields as $lang => $fields) {
            $this->save_custom_fields($post, $fields, $lang);
        }
        /* end Custom Fields */
        
        // Hook
        execute_actions('save_post', $post);
        
        return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $this->post_type)->with(['message' => __(ucfirst($this->post_type) . ' added successfully')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $post = Post::withTrashed()->findOrFail($id);
        $templates = $this->get_templates();
        $cf_groups = cacheable(CustomFieldGroupRepository::class, 60, true)->getPostCustomFields($post);
        
        if ($this->post_type != $post->type) return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $post->type);
        
        $params = null;
        if ( !in_array($this->post_type, ['post', 'page']) ) {
            $cp = CustomPost::whereTranslation('post_type', $this->post_type)->first();
            if ( !empty($cp) ) $params = $cp->params;
        }

        $users = \App\User::all();

        // Check if User can aprove pending post
        $can_aprove_pending = $post->status == 'pending' && Auth::user()->rol->level >= Post::ROL_LEVEL_LIMIT_REVISIO;

        return view('Admin::posts.edit', compact('post', 'templates', 'users', 'cf_groups', 'params', 'can_aprove_pending'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $post = Post::findOrFail($id);
        $data = $request->all();

        $post = $this->updatePost($post, $data);

        return redirect(route('admin.posts.edit', $post->id) . '?post_type=' . $this->post_type)->with(['message' => __('Post updated successfully')]);
    }

    /**
     * Potser podriem posar aquesta funció dins del model de Post per guardar massivament
     * custom fields
     */
    public function save_custom_fields($post, $fields, $lang, $is_child = false) {
        if ( $is_child ) {
            foreach ($fields as $idx => $subfields) {
                $this->save_custom_fields($post, $subfields, $lang);
            }
        } else if ($lang == 'deleted') {
            foreach($fields as $lang => $field) {
                foreach($field as $parent => $f) {

                    // Obtenim els Posts Meta que tinguin el parent_id i el post_id del eliminat
                    $postMetaBuilder = PostMeta::where('parent_id', $parent)
                                            ->where('post_id', $post->id)
                                            ->where('locale', $lang);

                    // Obtenim els posts que comparteixin els ordres que hem d'eliminar
                    $postMetaBuilder->where(function($query) use ($f) {
                        foreach(array_keys($f) as $order) {
                            $query->orwhere( 'order', $order);
                        }
                    });

                    // Eliminem els posts
                    $postMetaBuilder->delete();
                }
            }
        } else {
            $order = -1;
            foreach ($fields as $field_id => $field) {
                
                if ( $field_id == 'order' ) {
                    $order = $field;
                    continue;
                }
                
                $field_value = empty($field['fields']) ? $field : null;

                if ( is_array($field_value) ) {
                    $field_value = json_encode($field_value);
                }

                $meta = [
                    'post_id' => $post->id,
                    'custom_field_id' => 0,
                    'value' => $field_value,
                    'locale' => $lang,
                ];

                if ( is_int($field_id) ) {
                    $meta['custom_field_id'] = $field_id;
                    $where = [['post_id', $post->id], ['custom_field_id', $field_id], ['locale', $lang]];
                } else {
                    $meta['field_name'] = $field_id;
                    $where = [['post_id', $post->id], ['field_name', $field_id], ['locale', $lang]];
                }

                // Parent_id
                $f_aux = CustomField::find($field_id);
                
                if ( !empty($f_aux->parent_id) ) {
                    $meta['parent_id'] = $f_aux->parent_id;
                    $where[] = ['parent_id', $f_aux->parent_id];
                }
                // Order
                if ( $order >= 0 ) {
                    $meta['order'] = $order;
                    $where[] = ['order', $order];
                }
                
                // Update/Create
                if ( PostMeta::where( $where )->exists() ) {
                    $post_meta = PostMeta::where( $where );
                    $post_meta->update($meta);
                } else {
                    PostMeta::create($meta);
                }

                // Save SubFields
                if ( !empty($field['fields']) ) {
                    $this->save_custom_fields($post, $field['fields'], $lang, true);
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $post = Post::findOrFail($id);
        $post->status = 'trash';
        $post->update();
        $post->delete();
        
        return redirect(route('admin.posts.index') . '?post_type=' . $this->post_type)->with(['message' => __('Post sent to trash')]);
    }
    
    public function massDestroy(Request $request)
    {
        $posts = explode(',', $request->input('ids'));
        foreach ($posts as $post_id) {
            $post = Post::findOrFail($post_id);
            $post->status = 'trash';
            $post->update();
            $post->delete();
        }
        return redirect(route('admin.posts.index') . '?post_type=' . $this->post_type)->with(['message' => __('Post sent to trash')]);
    }

    public function restore($locale, $id)
    {
        $post = Post::withTrashed()->findOrFail($id);
        $post->status = 'publish';
        $post->update();
        $post->restore();
        
        return redirect(route('admin.posts.index') . '?post_type=' . $this->post_type)->with(['message' => __('Post restored successfully.')]);
    }

    public function destroy_permanent($locale, $id)
    {
        $post = Post::withTrashed()->findOrFail($id);
        $post->forceDelete();
        PostMeta::where('post_id', $id)->delete();
        
        return redirect(route('admin.posts.index') . '?post_type=' . $this->post_type)->with(['message' => __('Post deleted successfully.')]);
    }

    public function get_templates($post_type = null) {
        if ( $post_type == null ) $post_type = $this->post_type;
        $templates = array();
        $theme = Theme::where([['active', true], ['admin', false]]);
        if ( $theme->count() > 0 ) {
            $tema_path = base_path() . '/resources/themes/' . $theme->first()->path . "/posts/templates";
            if ( is_dir( $tema_path ) ) {
                foreach ( scandir($tema_path) as $dir ) {
                    if ( is_dir( $dir ) ) continue;
                    $templates[] = str_replace('.blade.php', '', $dir);
                }
            }
        }
        return $templates;
    }

    public function duplicate($locale, $id) {
        $post = Post::find($id);
        $data = [
            'type' => $post->type,
            'status' => $post->status,
            'template' => $post->template,
            'author_id' => Auth::id(),
        ];
        foreach ($this->_languages as $lang) {
            $trans = $post->getTranslation($lang->code);
            $exists = true;
            $i = 1;
            $name = $trans->post_name . "-$i";
            while ( $exists ) {
                if ( !Post::whereTranslation('post_name', $name, $lang->code)->exists() ) {
                    $exists = false;
                } else {
                    $i++;
                    $name = $trans->post_name . "-$i";
                }
            }
            $data[$lang->code] = [
                'title' => $trans->title . ' (copy)',
                'description' => $trans->description,
                'post_name' => $name,
                'media_id' => $trans->media_id,
            ];
        }

        $new = Post::create($data);

        // Duplicate post_meta
        $custom_fields = PostMeta::where('post_id', $id)->get();
        foreach ( $custom_fields as $field ) {
            $new_field = [
                'post_id' => $new->id,
                'custom_field_id' => $field->custom_field_id,
                'value' => $field->getOriginal('value'),
                'file' => $field->file,
                'locale' => $field->locale,
                'parent_id' => $field->parent_id
            ];

            if (isset($field->order)) {
                $new_field['order'] = $field->order;
            }
            
            PostMeta::create($new_field);
        }

        return redirect(route('admin.posts.index') . '?post_type=' . $this->post_type)->with(['message' => __(':name duplicated successfully', ['name' => __(ucfirst($this->post_type))]) ]);
    }

    public function aprove_pending($locale, $id) {
        $post = Post::find($id);
        if ( empty($post) ) return redirect(route($this->section_route . '.index') . '?post_type=' . $this->post_type)->with([ 'message' => __(":name doesn't exists.", ['name' => __('Post')]) ]);

        $post = $this->updatePost($post, request()->all());

        $post_parent = Post::find($post->pending_id);
        if ( !empty($post_parent) ) {
            $post_parent_id = $post_parent->id;
            $post_parent->forceDelete();
            PostMeta::where('post_id', $post_parent_id)->delete();
    
            DB::statement("SET FOREIGN_KEY_CHECKS=0"); // to disable them
            // POSTS META
            DB::statement("
                UPDATE post_meta SET
                    post_id = " . $post_parent_id . "
                WHERE post_id = " . $post->id . "
            ");
    
            // POSTS TRANSLATIONS
            DB::statement("
                UPDATE post_translations SET
                    post_id = " . $post_parent_id . "
                WHERE post_id = " . $post->id . "
            ");

            // POSTS
            DB::statement("
                UPDATE posts SET
                    id = " . $post_parent_id . ",
                    pending_id = 0,
                    status = 'publish'
                WHERE id = " . $post->id . "
            ");
            DB::statement("SET FOREIGN_KEY_CHECKS=1"); // to enable them
        } else {
            $post->status = 'publish';
            $post->pending_id = 0;
            $post->save();
        }        

        return redirect(route($this->section_route . '.index') . '?post_type=' . $this->post_type)->with([ 'message' => __(":name aproved successfully.", ['name' => __('Post')]) ]);
    }

    public function copy_content($locale, Post $post) {
        $to = request()->to;
        $from = request()->from;

        $to_metas = $post->metas->where('locale', $to);
        $from_metas = $post->metas->where('locale', $from);

        $from_post = $post->translate($from);
        $to_post = $post->translate($to);

        $aux = $from_post->updateOrCreate(
            ['post_id' => $post->id, 'locale' => $from],
            [
                'title' => $to_post->title,
                'description' => $to_post->description,
                'post_name' => $to_post->post_name,
                'media_id' => $to_post->media_id
            ]
        );

        foreach($to_metas as $to_meta) {
            $custom_field = $to_meta->custom_field;

            $post = $aux->post->metas()->updateOrCreate(
                ['locale' => $from, 'custom_field_id' => $custom_field->id],
                [
                    'file' => $to_meta->file,
                    'value' => $to_meta->getOriginal('value'),
                    'order' => $to_meta->order,
                    'parent_id' => $to_meta->parent_id,
                    'field_name' => $to_meta->field_name
                ]
            );
        }
        
        return json_encode([
            'url' => route('admin.posts.edit', ['post' =>  $post, 'post_type' => $aux->type]),
            'success' =>  true
        ]);
    }


    protected function updatePost(Post $post, $data)
    {
        $custom_fields = array();

        if ( !empty($data['custom_fields']) ) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code] && (isset($data[$lang->code]['post_name']) || $data[$lang->code]['title'])) ) {
                $sanitize_title = str_slug($data[$lang->code]['title'], '-');
                if ( !empty( $data[$lang->code]['post_name'] ) ) {
                    $sanitize_title = str_slug($data[$lang->code]['post_name'], '-');
                }
                $aux = $sanitize_title;
                $i = 1;
                while (Post::whereTranslation('post_name', $aux, $lang->code)->where('id', '<>', $post->id)->where('pending_id', '<>', $post->pending_id)->exists()) {
                    $aux = $sanitize_title . '-' . $i;
                    $i++;
                }
                $sanitize_title = $aux;
                $data[$lang->code]['post_name'] = str_replace('//', '/', $sanitize_title);
                
                if (!isset($data['media_id'])) $data['media_id'] = isset($data[$lang->code]['media_id']) ? $data[$lang->code]['media_id'] : null;
                else $data[$lang->code]['media_id'] = $data['media_id'] ?? $data[$lang->code]['media_id'];
                
                if (!isset($data['template']) || isset($data[$lang->code]['template'])) $data['template'] = isset($data[$lang->code]['template']) ? $data[$lang->code]['template'] : null;
                else $data[$lang->code]['template'] = $data['template'] ?? $data[$lang->code]['template'];
            }
        }
        
        if (empty($data['author_id']) && !$post->author_id) {
            $data['author_id'] = Auth::id();
        }

        // Check if is revision
        if ( !in_array($post->status, array('pending', 'draft')) && Auth::user()->rol->level < Post::ROL_LEVEL_LIMIT_REVISIO ) {
            $data['status'] = 'pending';
            $data['pending_id'] = $post->id;
            $post = Post::create($data);
        } else {
            $post->update($data);
        }

        /* Custom Fields */
        foreach ($custom_fields as $lang => $fields) {
            // $post->save_custom_fields($fields, $lang);
            $this->save_custom_fields($post, $fields, $lang);
        }

        // Hook
        execute_actions('save_post', $post);

        return $post;
    }
}
