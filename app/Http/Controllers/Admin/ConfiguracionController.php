<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Configuracion;
use App\Post;
use Storage;

class ConfiguracionController extends AdminController
{
    protected $configs = [
        'general' => [
            'app_name' => [
                "title" => "Application name",
                "name" => "app_name",
                "type" => "text",
                "required" => true
            ],
            'show_admin_bar' => [
                "title" => "Show Admin Bar",
                "name" => "show_admin_bar",
                "type" => "true_false",
            ],
            'home_page' => [
                "title" => "Home page",
                "name" => "home_page",
                "type" => "page",
            ],
            'color' => [
                "title" => "Color",
                "name" => "color",
                "type" => "color",
            ],
            'app_icon' => [
                "title" => "Application icon",
                "name" => "app_icon",
                "type" => "img",
            ],
            'img_sidebar' => [
                "title" => "Sidebar image",
                "name" => "img_sidebar",
                "type" => "img",
            ],
            'img_email' => [
                "title" => "Email image",
                "name" => "img_email",
                "type" => "img",
            ],
        ],
        'apis' => [
            'google_maps' => [
                "title" => "API Google Maps",
                "name" => "google_maps",
                "type" => "text",
            ],
            'api_acumbamail' => [
                "title" => "API Acumbamail",
                "name" => "api_acumbamail",
                "type" => "text",
            ],
        ],
    ];

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Post::ofType('page')->get();
        $items = $this->configs;
        $configuraciones = Configuracion::orderby('order')->get();

        foreach ( $configuraciones as $c ) {
            if (!isset($items[$c->group])) $items[$c->group] = array();
            $items[$c->group][$c->key] = [
                'obj' => $c,
                'title' => $c->title,
                'name' => $c->key,
                'type' => $c->type,
                'value' => $c->value,
                'file' => $c->file,
            ];
        }
        return View('Admin::configuracion', compact('items', 'pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        foreach ($this->configs as $group_name => $configs) {
            foreach ( $configs as $k => $c ) {
                $item = Configuracion::where('key', $k)->first();
                if ( empty($item) ) $item = new Configuracion();

                $item->title = $c['title'];
                $item->key = $c['name'];
                $item->type = $c['type'];
                $item->group = $group_name;
                
                if ( $item->type == 'img' ) {
                    if ( $file = $request->file($k) ) {
                        //$item->file = file_get_contents($file->getRealPath());
                        $item->file = $this->upload_file($file, $item->key);
                    }
                } else {
                    $item->value = !empty($data[$group_name][$k]) ? $data[$group_name][$k] : null;
                }

                $item->save();
            }
        }

        /*
        $configuraciones = Configuracion::orderby('order')->get();
        foreach ( $configuraciones as $c ) {
            if ( array_key_exists($c->key, $data) ) {
                if ( $c->type == 'img' ) {
                    if ( $file = $request->file($c->key) ) {
                        $c->file = file_get_contents($file->getRealPath());
                    }
                } else {
                    $c->value = $data[$c->key];
                }
                $c->update();
            }
        }*/
        return redirect()->route('admin.configuracion.index')->with(['message' => __('Configuration saved successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function upload_file($file, $config_name) {
        $filename = $config_name . '.' . $file->getClientOriginalExtension();

        Storage::delete(Configuracion::get_config_file_dir() . '/' . $filename);
        $file->storeAs(Configuracion::get_config_file_dir(), $filename);

        return $filename;

    }
}
