<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Language;
use DB;

class AdminIdiomasController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Languages');
        $this->section_icon = 'language';
        $this->section_route = 'idiomas';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Language::orderby('name')->get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'headers' => [
                __('Name') => [],
                __('Code') => [ 'width' => '20%' ],
                __('Active') => [ 'width' => '10%' ],
                __('Default') => [ 'width' => '10%' ],
            ],
            'rows' => [
                ['value' => 'name'],
                ['value' => 'code'],
                ['value' => 'active', 'type' => 'switch_active', 'route' => $this->section_route.'.switch_active', 'onlyone' => false],
                ['value' => 'default', 'type' => 'switch_active', 'route' => $this->section_route.'.switch_default', 'onlyone' => true],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'width' => 6,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'code', 'title' => __('Code'), 'required' => true],
            ],
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['code'] = strtolower($data['code']);
        Language::create($data);
        return redirect()->route('idiomas.index')->with(['message' => __('Language added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = Language::findOrFail($id);
        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'width' => 6,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'code', 'title' => __('Code'), 'required' => true],
            ],
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $data = $request->all();
        $data['code'] = strtolower($data['code']);
        $item = Language::findOrFail($id);
        $item->update($data);
        return redirect()->route('idiomas.edit', $id)->with(['message' => __('Language updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $language = Language::findOrFail($id);
        if ( $language->delete() ) {
            return response()->json(['success'=>'Done!']);
        }
        return response()->json(['error'=>'Done!'], 401);
    }

    public function switch_active(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $language = Language::findOrFail($id);
        $language->active = !$language->active;
        $language->update();
        return response()->json(['success'=>'Done!', 'active' => $language->active]);
    }

    public function switch_default(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $language = Language::findOrFail($id);
        DB::table('languages')->where('default', true)->where('id', '!=', $id)->update(['default' => false]);
        $language->default = !$language->default;
        $language->update();
        return response()->json(['success'=>'Done!', 'active' => $language->default]);
    }
}
