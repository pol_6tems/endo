<?php

namespace App\Http\Controllers\Admin;

use DB;
use App;
use Auth;
use View;
use Route;
use App\Models\Opcion;
use App\Models\CustomPost;
use App\Models\Configuracion;
use App\Models\Admin\AdminMenu;
use App\Models\Admin\AdminMenuSub;
use App\Http\Controllers\Controller;
use App\Models\CustomPostTranslation;
use Illuminate\Support\Facades\Artisan;

class AdminController extends Controller
{
    public $notifications;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $locale = app()->getLocale();
        $datatableLangFile = 'js/datatables/Spanish.json';

        if ($locale == 'ca') {
            $datatableLangFile = 'js/datatables/Catalan.json';
        }

        view()->share('datatableLangFile', $datatableLangFile);
    }

    protected function add_notification($msg) {
        $this->notifications[] = $msg;
        View::share ( 'notifications', $this->notifications );
    }

    protected static function get_controllers()
    {
        $controllers = [];
        foreach (\Route::getRoutes()->getRoutes() as $route)
        {
            $action = $route->getAction();

            if (array_key_exists('controller', $action))
            {
                // You can also use explode('@', $action['controller']); here
                // to separate the class name from the method
                $parts = explode('@', $action['controller']);
                $controller_name = str_replace('App\Http\Controllers\\', '', 
                    str_replace('App\Modules\Http\Controllers\\', '', 
                $parts[0]));

                if (isset($parts[1])) {
                    $controllers[$controller_name][] = $parts[1];
                }
            }
        }
        return $controllers;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin::index');
    }

    /**
     * @return mixed
     *
     * @deprecated Moving to SidebarComposer
     * @see \App\Http\Composers\Admin\SidebarComposer.php 
     */
    public static function getMenu() {
		$permisos = Auth::user()->rol->get_permisos();
        $menus = AdminMenu::orderby('order')->get();
        $custom_posts = CustomPostTranslation::where('locale', \App::getLocale() )->orderby('title')->get();
        
        // Menus Loop
        foreach ($menus as $key => $menu) {
            $is_active = false;

            /* Menu automàtic Contingut */
            if ( $menu->name == 'Content' ) {
                $put_custom_posts_in_content_menu = Opcion::where('key', 'put_custom_posts_in_content_menu');
                if ( $put_custom_posts_in_content_menu->exists() && $put_custom_posts_in_content_menu->first()->value == '1' ) {
                    $menu = self::generar_content_automatic_submenus( $menu );
                }
            }
            /* end Menu automàtic Contingut */

            // Submenus Loop
            foreach ( $menu->submenus as $key2 => $sub ) {
                $obj = self::get_controller_by_route_name($sub->url);
                if ( !empty($obj) ) {
                    // Revisem si té permisos per veure el controlador i el mètode sel·leccionat
                    if ( !self::menu_te_permisos( $permisos, $obj, $sub->parameters ) ) {
                        unset($menus[$key]->submenus[$key2]);
                    } else {
                        $sub->is_active = self::is_menu_active( $sub );
                        if ( $sub->is_active ) $is_active = true;
                        $sub->href = self::get_menu_url($sub, true);
                        $menus[$key]->submenus[$key2] = $sub;
                    }
                } else unset($menus[$key]->submenus[$key2]);
            }


            if ( empty($menu->controller) || $menu->controller == 'Admin\PostsController' ) {
                $parametres = [];
                if ($menu->parameters) {
                    list($k, $v) = explode('=', $menu->parameters);
                    $parametres[ $k ] = $v;
                    
                    if ( !self::checkCustomPostPermission($parametres['post_type']) ) { unset($menus[$key]); }
                } else {
                    if ($menu->submenus->isEmpty()) {
                        unset($menus[$key]);
                    }
                }
            } else {
                $obj = self::get_controller_by_route_name($menu->url);
                if (!self::menu_te_permisos($permisos, $obj, $menu->parameters)) {
                    unset($menus[$key]);
                }
            }
            
            if ( !empty($menus[$key]) ) {
                if ( !$is_active ) $is_active = self::is_menu_active( $menu );
                $menus[$key]->is_active = $is_active;
                $menus[$key]->href = self::get_menu_url( $menu );
            }
        }
		return $menus;
    }

    public static function get_menu_url($menu, $is_submenu = false) {
        $href = 'javascript:void(0)';
        if ( $menu->has_url == 1 || $is_submenu ) {
            if ( $menu->url_type == 'route' && \Route::has($menu->url)) {
                $href = route($menu->url) . ($menu->parameters ? '?' . $menu->parameters : '');
            } else if ($menu->url_type == 'url') {
                $href = $menu->url . ($menu->parameters ?: '');
            }
        }
        return $href;
    }

    public static function is_menu_active( $menu ) {
        $is_active = like_match($menu->controller, \Route::current()->getActionName(), true);
        if ( !empty(request()->post_type) ) {
            $post_type = request()->post_type;
            
            // Convertir paràmetres (string) en una array associativa
            if ( !empty($menu->parameters) ) {
                list($k, $v) = explode('=', $menu->parameters);
                $parametres[ $k ] = $v;
                
                $is_active = $is_active && $parametres['post_type'] == $post_type;
            }
            else $is_active = false;
            
        } else if ( !empty(request()->type) ) {
            $type = request()->type;
            
            // Convertir paràmetres (string) en una array associativa
            if ( !empty($menu->parameters) ) {
                list($k, $v) = explode('=', $menu->parameters);
                $parametres[ $k ] = $v;
                
                $is_active = $is_active && $parametres['type'] == $type;
            }
            else $is_active = false;
            
        } else if ( !empty($menu->parameters) ) $is_active = false;

        return $is_active;
    }

    public static function menu_te_permisos( $permisos, $obj, $parameters ) {
        $grup = $obj['grup'];
        $controller = $obj['controller'];
        $method = $obj['method'];

        $permisos = (isset($permisos[$grup][$controller][$method])) ? $permisos[$grup][$controller][$method] : false;

        if (($controller == 'adminpostscontroller' && $parameters) || $parameters) {
            $params = [];
            // Convertir paràmetres (string) en una array associativa
            list($k, $v) = explode('=', $parameters);
            $params[ $k ] = $v;

            if ( isset($params['post_type']) ) {
                $permisos = self::checkCustomPostPermission($params['post_type']);
			}
        }

        return Auth::user()->isAdmin() || ( !empty($permisos) && $permisos );
    }
    
    public static function get_controller_by_route_name($name)
    {
        $modules = scandir(app_path('Modules'));
        $routes = \Route::getRoutes();
        $result = ['grup' => null, 'controller' => null, 'method' => null];
        foreach ($routes as $route) {
            $action = $route->getAction();
            if (!empty($action['as']) && $name == $action['as']) {
                $controller = ltrim(str_replace('Controllers\\', '',
                    str_replace('App\Http\\', '', 
                        str_replace('App\Modules\\', '', $action['controller'])
                    )
                ), '\\');
                $parts = explode('\\', $controller);
                $module_name = $parts[0];
                if ( in_array($module_name, $modules) ) {
                    $controller_name = str_replace("$module_name\\", '', $controller);
                    $c_parts = explode('@', $controller_name);
                    $result['grup'] = str_slug($module_name, '-');
                    $result['controller'] = str_slug($c_parts[0], '-');
                    $result['method'] = $c_parts[1];
                } else {
                    $controller_name = $controller;
                    $c_parts = explode('@', $controller_name);
                    $result['grup'] = str_slug('General', '-');
                    $result['controller'] = str_slug($c_parts[0], '-');
                    $result['method'] = $c_parts[1];
                }
            }
        }

        return $result;
    }

    public function get_controllers_grouped() {
        $grups = array('General' => []);
        $controllers = $this->get_controllers();
        $modules = scandir(app_path('Modules'));
        foreach ( $controllers as $controller => $methods ) {
            $controller = str_replace('Controllers\\', '',
                str_replace('App\Http\\', '', 
                    str_replace('App\Modules\\', '', $controller)
                )
            );
            $parts = explode('\\', $controller);
            $module_name = $parts[0];
            if ( in_array($module_name, $modules) ) {
                $controller_name = str_replace("$module_name\\", '', $controller);
                $grups[$module_name][$controller_name] = $methods;
            } else {
                $grups['General'][$controller] = $methods;
            }
        }

        foreach ( $grups as $key => $item ) ksort($grups[$key]);

        return $grups;
    }

    public function get_url($lang) {
        $route_parameters = Route::current() != null ? Route::current()->parameters : [];
        $route_parameters['locale'] = $lang;
        if ( Route::currentRouteName() == 'posts' ) {
            // ROUTES POSTS
            $request_uri = request()->getRequestUri();
            $path_info = request()->getPathInfo();
            $web_parameters = str_replace($path_info, '', $request_uri);
            $post_name = str_replace('//', '',
                str_replace('/' . App::getLocale(), '',
                    $path_info
                )
            );
            if ( strpos($post_name, '/') == 0 ) $post_name = substr($post_name, 1);

            $q = \DB::table('custom_post_translations')->where([
                ['post_type_plural', $post_name],
                ['locale', App::getLocale()]
            ]);
            if ( $q->exists() ) {
                $cp = CustomPost::whereTranslation('custom_post_id', $q->first()->custom_post_id, $lang)->first();
                return Post::get_archive_link( $cp->post_type_plural, $lang ) . $web_parameters;
            }
            if (strpos($post_name, '/') !== false) {
                $parts = explode('/', $post_name);
                $post_name = $parts[1];
            }
            $post = \DB::table('post_translations')->where([
                ['post_name', $post_name],
                ['locale', App::getLocale()]
            ]);
            if ( $post->exists() ) {
                $post_id = $post->first()->post_id;
                return Post::get_url_by_post_id($post_id, $lang);
            }
        } else if ( Route::has(Route::currentRouteName()) ) {
            if ( self::like_match('{locale}/admin%', Route::current()->getAction()['prefix']) ) {
                // ROUTES ADMIN
                return route(Route::currentRouteName(), $route_parameters);
            } else {
                // ROUTES FRONT
                $request_uri = request()->getRequestUri();
                $path_info = request()->getPathInfo();
                $web_parameters = str_replace($path_info, '', $request_uri);

                $savedLocale = App::getLocale();
                App::setLocale($lang);
                $ruta = __('/' . Route::currentRouteName());
                App::setLocale($savedLocale);

                return url('/') . "/$lang" . $ruta . $web_parameters;
            }
        } else {
            // NO ROUTES
            return null;
        }
    }
    
    public function getRoute() {
        return (isset($this->section_route)) ? $this->section_route : 'admin.posts';
    }

    protected static function generar_content_automatic_submenus($menu) {
        $cps = CustomPost::get();
        foreach ( $cps as $k => $cp ) {
            $cp_t = $cp->translate();
            $submenu = new AdminMenuSub();
            $submenu->name = !empty($cp_t->title_plural) ? $cp_t->title_plural : $cp_t->title;
            $submenu->url_type = 'route';
            $submenu->url = 'admin.posts.index';
            $submenu->controller = 'Admin\PostsController';
            $submenu->order = (count($menu->submenus) + $k) * 10;
            $submenu->parameters = 'post_type=' . $cp_t->post_type;
            $menu->submenus[] = $submenu;
        }
        return $menu;
    }

    private static function checkCustomPostPermission($post_type) {
        $permisos = true;
        if ($post_type) {
            if ( $cp = CustomPost::whereTranslation('post_type', $post_type)->first()) {
                $permissions = (array) $cp->permissions;
                if (isset($permissions) && isset($permissions[Auth::user()->rol->name])) {
                    return $permissions[Auth::user()->rol->name]->index == "1" ? true : false;
                }
            } else {
                $permissions = Auth::user()->rol->get_permisos();
                if ( isset($permissions['roles']) ) {
                    if ( isset($permissions['roles'][$post_type][Auth::user()->rol->name]) ) {
                        return $permissions['roles'][$post_type][Auth::user()->rol->name];
                    } else return false;
                }
            }
        }

        return $permisos;
    }

    public function clearCache() {
        Artisan::call('cache:clear');
        return back();
    }
}
