<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Module;
use DB;
use File;

class ModulesController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Modules');
        $this->section_icon = 'extension';
        $this->section_route = 'admin.modules';
    }

    public function index()
    {
        $items = Module::orderby('name')->get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            //'has_reset' => true,
            'field_name' => 'name',
            'toolbar_header' => [
                '<a href="' . route('admin.modules.refresh') . '" class="btn btn-default btn-sm">' . __('Refresh') . '</a>',
            ],
            'headers' => [
                __('Name') => [],
                __('Active') => [ 'width' => '10%' ],
            ],
            'rows' => [
                ['value' => 'name'],
                ['value' => 'active', 'type' => 'switch_active', 'route' => 'admin.modules.active', 'onlyone' => false]
            ],
        ));
    }

    public function create()
    {
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'width' => 6,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
            ],
        ));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        Module::create($data);
        return redirect()->route('admin.modules.index')->with([ 'message' => __(':name added successfully', ['name' => __('Module')]) ]);
    }

    public function show($locale, $id)
    {
        //
    }

    public function edit($locale, $id)
    {
        $item = Module::findOrFail($id);
        $languages = DB::table('languages')->where('active', true)->get();
        if (count($languages) == 0) $languages = DB::table('languages')->where('code', 'en')->get();
        $columns = [];
        $columnsCount = $languages->count();
        if ( $languages->count() > 0 ) {
	        foreach ($languages as $key => $language) {
	            if ( $key == 0 ) $columns[$key] = $this->openJSONFile($language->code, $item->name);
	            $columns[++$key] = [
                    'data'=> $this->openJSONFile($language->code, $item->name), 
                    'lang' => $language->code
                ];
	        }
        }

        return View('Admin::modules.edit', compact('item', 'languages','columns','columnsCount'));
    }

    public function update(Request $request, $locale, $id)
    {
        $data = $request->all();
        $item = Module::findOrFail($id);
        $item->update($data);
        return redirect()->route('admin.modules.edit', $id)->with([ 'message' => __(':name updated successfully', ['name' => __('Module')]) ]);
    }

    public function destroy($locale, $id)
    {
        $item = Module::findOrFail($id);
        
        $module_name = $item->name;
        $file_path = app_path("Modules/$module_name/module.php");
        if ( file_exists( $file_path ) ) {
            include( $file_path );
            $module_class_name = 'App\\Modules\\' . $module_name . '\\' . $module_name . 'Module';
            $response = call_user_func("$module_class_name::reset");
            
            if ( $response == TRUE ) {
                if ( $item->delete() ) {
                    return response()->json(['success'=>'Done!']);
                }
            } else return response()->json(['error'=> $response ], 401);
        }
        else if ( $item->delete() ) {
            return response()->json(['success'=>'Done!']);
        }
        return response()->json(['error'=>'Done!'], 401);
    }

    public function active(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $item = Module::findOrFail($id);
        
        $module_name = $item->name;
        $file_path = app_path("Modules/$module_name/module.php");
        if ( file_exists( $file_path ) && !$item->active ) {
            include( $file_path );
            $module_class_name = 'App\\Modules\\' . $module_name . '\\' . $module_name . 'Module';
            $response = call_user_func("$module_class_name::install");

            if ( $response != TRUE ) return response()->json(['error'=> $response ], 401);
        }
        
        $item->active = !$item->active;

        $item->update();
        return response()->json(['success'=>'Done!', 'active' => $item->active]);
    }

    public function reset(Request $request) {
        $data = $request->all();
        $id = $data['id'];
        $item = Module::findOrFail($id);
        
        $module_name = $item->name;
        $file_path = app_path("Modules/$module_name/module.php");
        if ( file_exists( $file_path ) ) {
            include( $file_path );
            $module_class_name = 'App\\Modules\\' . $module_name . '\\' . $module_name . 'Module';
            $response = call_user_func("$module_class_name::refresh");

            if ( $response == TRUE ) return response()->json(['success'=>'Done!', 'active' => $response]);
            else return response()->json(['error'=> $response ], 401);
        }
        else return response()->json(['error'=>'Error!'], 401);
    }

    /* Translations */
    public function transStore (Request $request, $locale, $module_id) {
        $module = Module::findOrFail($module_id);
        $this->validate($request, [
		    'key' => 'required',
		    'value' => 'required',
        ]);

		$data = $this->openJSONFile('en', $module->name);
        $data[$request->key] = $request->value;
        //$data[$request->key] = addcslashes($request->value, '"\\/');
        
        array_walk_recursive($data, function(&$item, $key) {
            $item = addcslashes($item, '"\\/');
		});

        $this->saveJSONFile('en', $data, $module->name);

        return redirect()->route('admin.modules.edit', $module_id);
    }

    public function transUpdate(Request $request, $locale, $module_id) {
        $module = Module::findOrFail($module_id);
        $data = $this->openJSONFile($request->code, $module->name);
        $data[$request->pk] = $request->value;

        array_walk_recursive($data, function(&$item, $key) {
            $item = addcslashes($item, '"\\/');
		});

        $this->saveJSONFile($request->code, $data, $module->name);
        return response()->json(['success'=>'Done!']);
    }

    public function transUpdateKey(Request $request, $locale, $module_id){
        $module = Module::findOrFail($module_id);
        $languages = DB::table('languages')->get();
        if ( $languages->count() > 0 ) {
            foreach ( $languages as $language ) {
                $data = $this->openJSONFile($language->code, $module->name);
                if ( isset($data[$request->pk]) ) {
                    $data[$request->value] = $data[$request->pk];
                    unset($data[$request->pk]);
                    $this->saveJSONFile($language->code, $data, $module->name);
                }
            }
        }
        return response()->json(['success'=>'Done!']);
    }

    public function transDestroy($locale, $module_id, $key) {
        $module = Module::findOrFail($module_id);
        $key = base64_decode($key);
        $languages = DB::table('languages')->get();
        if ( $languages->count() > 0 ) {
            foreach ($languages as $language){
                $data = $this->openJSONFile($language->code, $module->name);
                unset($data[$key]);
                $this->saveJSONFile($language->code, $data, $module->name);
            }
        }
        return response()->json(['success' => $key]);
    }

    private function openJSONFile($code, $module_name) {
        $jsonString = [];
        $file_path = app_path("Modules/$module_name/resources/lang/$code.json");
        if ( File::exists($file_path) ) {
            $jsonString = file_get_contents( $file_path );
            $jsonString = json_decode($jsonString, true);
        }
        return $jsonString;
    }

    private function saveJSONFile($code, $data, $module_name) {
        $file_path = app_path("Modules/$module_name/resources/lang/$code.json");
        ksort($data);
        $jsonData = json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
        file_put_contents($file_path, stripslashes($jsonData));
    }
    /* end Translations */

    public function refresh() {
        foreach (glob(app_path('Modules/*'), GLOB_ONLYDIR) as $dirname) {
            if ( !Module::where('name', basename($dirname))->exists() ) {
                DB::table('modules')->insert(['name' => basename($dirname)]);
            }
        }
        return redirect()->route('admin.modules.index')->with([ 'message' => __(':name updated successfully', ['name' => __('Modules')]) ]);
    }

}
