<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Notification;

class NotificationsController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Notifications');
        $this->section_icon = 'notification_important';
        $this->section_route = 'admin.notifications';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Notification::orderby('order')->get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'has_duplicate' => true,
            'headers' => [
                __('Order') => ['width' => '15%'],
                __('Name') => [],
                __('Controller') => [],
                __('Method') => [],
                __('Active') => ['width' => '15%'],
            ],
            'rows' => [
                ['value' => 'order'],
                ['value' => 'name'],
                ['value' => 'controller'],
                ['value' => 'method'],
                ['value' => 'active', 'type' => 'switch_active', 'route' => $this->section_route . '.active', 'onlyone' => false],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'order', 'title' => __('Order'), 'type' => 'number'],
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'controller', 'title' => __('Controller'), 'required' => true],
                ['value' => 'method', 'title' => __('Method'), 'required' => true],
                ['value' => 'parameters', 'title' => __('Parameters')],
            ],
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Notification::create($data);
        return redirect()->route($this->section_route.'.index')->with(['message' => __('Notification added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = Notification::findOrFail($id);
        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'order', 'title' => __('Order'), 'type' => 'number'],
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'controller', 'title' => __('Controller'), 'required' => true],
                ['value' => 'method', 'title' => __('Method'), 'required' => true],
                ['value' => 'parameters', 'title' => __('Parameters')],
            ],
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $data = $request->all();
        $item = Notification::findOrFail($id);
        $item->update($data);
        return redirect()->route($this->section_route.'.edit', $id)->with(['message' => __('Notification updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $item = Notification::findOrFail($id);
        if ( $item->delete() ) {
            return response()->json(['success'=>'Done!']);
        }
        return response()->json(['error'=>'Done!'], 401);
    }

    public function active(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $item = Notification::findOrFail($id);
        $item->active = !$item->active;
        $item->update();
        return response()->json(['success'=>'Done!', 'active' => $item->active]);
    }

    public function duplicate($locale, $id) {
        $item = Notification::find($id);
        
        $data = [
            'order' => $item->order,
            'name' => $item->name . ' (copy)',
            'controller' => $item->controller,
            'method' => $item->method,
            'parameters' => $item->parameters,
            'active' => false,
        ];

        $new = Notification::create($data);

        if ( !empty($new) ) {
            $msg = __(':name duplicated successfully', ['name' => __('Notification')]);
        } else {
            $msg = __('Error saving data');
        }
        return redirect()->route($this->section_route.'.index')->with([ 'message' => $msg ]);
    }

}
