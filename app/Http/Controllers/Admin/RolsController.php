<?php

namespace App\Http\Controllers\Admin;

use App\Models\Chat;
use Illuminate\Http\Request;
use App\Models\Admin\Rol;
use App\Models\CustomPost;

class RolsController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Roles');
        $this->section_icon = 'supervisor_account';
        $this->section_route = 'admin.rols';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Rol::orderby('level', 'DESC')->get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'headers' => [
                __('Name') => [],
                __('Title') => [],
                __('Level') => [ 'width' => '15%' ],
                __('Users') => [ 'width' => '15%' ],
            ],
            'rows' => [
                ['value' => 'name'],
                ['value' => 'name', 'translate' => true],
                ['value' => 'level'],
                ['value' => 'users', 'type' => 'count' ],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'width' => 6,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'level', 'title' => __('Level')],
            ],
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if ( empty($data['level']) ) $data['level'] = 0;
        Rol::create($data);
        return redirect()->route('admin.rols.index')->with(['message' => __('Role added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        $item = Rol::findOrFail($id);
        return view('admin.administracion.rols.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = Rol::findOrFail($id);
        $controllers = $this->get_controllers_grouped();
        $customPosts = CustomPost::all();
        $chats = Chat::all();

        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'level', 'title' => __('Level')],
                ['type' => 'subform', 'value' => 'Admin::default.subforms.rols_permisos', 'controllers' => $controllers],
                ['type' => 'subform', 'value' => 'Admin::default.subforms.custom_posts_permisos', 'custom_posts' => $customPosts],
                ['type' => 'subform', 'value' => 'Admin::default.subforms.dynamic_chats_permisos', 'chats' => $chats],
            ],
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $item = Rol::findOrFail($id);
        $data = $request->all();

        $roles = [];
        foreach($data['permissions'] as $cpid => $permission) {
            if (is_numeric($cpid)) {
                $cp = CustomPost::find($cpid);

                // Current
                $current = (array) $cp->permissions;

                if ( !isset($current[$item->name]) ) {
                    $current = array_merge($current, $permission);
                } else {
                    $current[$item->name] = $permission[$item->name];
                }

                $cp->permissions = json_encode($current);
                $cp->save();
            } else {
                $roles[$cpid] = $permission;
            }
        }

        // Add to Current Role Permissions
        $permission = (array) json_decode($item->permisos);        

        if ( isset($permission['roles']) ) {
            $current = (array) $permission['roles'];
            $new = (array) $roles;

            $data['permisos']['roles'] = array_merge($current, $new);
        } else {
            $data['permisos']['roles'] = $roles;
        }

        unset($data['permissions']);

        $data['permisos'] = !empty($data['permisos']) ? json_encode($data['permisos']) : json_encode([]);

        $item->update($data);
        return redirect()->route('admin.rols.edit', $id)->with(['message' => __('Role updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $item = Rol::findOrFail($id);
        $item->delete();
        
        return redirect()->route('admin.rols.index')->with(['message' => __('Role deleted successfully')]);
    }
}
