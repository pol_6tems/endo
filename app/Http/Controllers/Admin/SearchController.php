<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

class SearchController extends AdminController
{

    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Search');
        $this->section_icon = 'search';
        $this->section_route = 'search';
    }

    public function index() {
        $search_key = Input::get('s');

        $tables = $this->search( $search_key );

        $widgets = $this->parse_table_results($tables);

        return View('Admin::search', compact('widgets', 'search_key'));
    }

    protected function search($s) {
        $database = DB::getDatabaseName();
        if ( !$database ) return;

        // TABLES LOOP
        $sql = "SELECT table_name FROM information_schema.tables where table_schema = '" . $database . "';";
        $table_rows = DB::select( DB::raw( $sql ) );
        
        if ( count($table_rows) <= 0 ) return;

        foreach ($table_rows as $row) {
            $table = new \stdClass();
			$table->name = $row->table_name;
			$table->count = 0;
			$table->fields = array();
			
			$sql2 = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" . $database . "' AND TABLE_NAME = '" . $table->name . "';";
            $column_rows = DB::select( DB::raw( $sql2 ) );
			if ( count($column_rows) > 0 ) {
				foreach ( $column_rows as $row2 ) {
					$table->fields[] = $row2->COLUMN_NAME;
				}
			}
			
			$tables[] = $table;
        }

        foreach ( $tables as $key => $table ) {
            $sql_found = "SELECT * FROM `" . $table->name . "` WHERE ";
            foreach ( $table->fields as $key2 => $field ) {
                if ( $key2 > 0 ) $sql_found .= " OR ";
                $sql_found .= "`" . $field . "` LIKE '%$s%'";
            }
            
            $found_rows = DB::select( DB::raw( $sql_found ) );
            $table->count += count($found_rows);
            $table->found_rows = $found_rows;
            if ( $table->count <= 0) unset($tables[$key]);
        }
        
        // end TABLES LOOP

        return $tables;

    }

    protected function parse_table_results($tables) {
        $results = [];
        
        if ( !empty($tables) ) foreach ( $tables as $table ) {
            $model = $this->get_model_by_table_name( $table->name );
            if ( empty($model) ) continue;

            $is_subtable = $model['is_subtable'];
            $class_name = $model['class_name'];
            $class_name_alone = $model['class_name_alone'];
            $class_table = $model['class_table'];
            foreach ( $table->found_rows as $key_row => $row ) {

                $locale = !empty($row->locale) ? $row->locale : \App::getLocale();

                // Busquem el id original
                $id = $row->id;
                if ( $is_subtable ) {
                    $trobat = false;
                    foreach ( $table->fields as $field ) {
                        if ( !$trobat && $field != 'id' && like_match( '%_id', $field ) ) {
                            $id = $row->{$field};
                            $trobat = true;
                        }
                    }
                }

                // Mirem si l'id ja s'ha fet servir
                if ( !empty($results[ $class_name_alone]) && !empty($results[ $class_name_alone]['rows'][$id]) ) {
                    continue;
                }

                // Agafem l'item a partir del id
                $title = $post_type = null;
                if ( $class_name_alone == 'Post' ) {
                    $item = $class_name::withTrashed()->find( $id );

                    if ($item) {
                        // Títol i post_type
                        if (!in_array($item->type, ['post', 'page'])) {
                            $cp = \App\Models\CustomPost::whereTranslation('post_type', $item->type, $locale)->first();
                            if (!empty($cp)) $title = $cp->translate($locale)->title_plural;
                            else {
                                if ($item->type == 'page') $title = 'Pages';
                                else $title = 'Posts';
                            }
                        } else {
                            if ($item->type == 'page') $title = 'Pages';
                            else $title = 'Posts';
                        }
                        $post_type = $item->type;
                    }
                } else $item = $class_name::find( $id );

                if ( !empty($item) ) {

                    // Agafar URL del post abans de traduïr
                    if ( !empty($post_type) ) $post_url = $item->get_url();
                    
                    // Si és subtaula (_meta o _translations), traduïr item.
                    if ( $is_subtable ) $item = $item->translate( $locale );


                    // Màxim columnes/camps a ensenyar
                    $max_fields = 4;
                    $fillables = $item->getFillable();
                    if ( count($fillables) < $max_fields ) $max_fields = count($fillables);

                    // Prepara widget si no existeix
                    $index_results = $post_type ?: $class_name_alone;
                    if ( !array_key_exists($class_name_alone, $results) ) {
                        $results[ $index_results ] = [
                            'name' => $post_type ?: $table->name,
                            'title' => $title ? __($title) : __($index_results),
                            'icon' => 'table_chart',
                            'icon_class' => 'primary',
                            'list_url' => !empty($post_type) ? route('admin.posts.index') . "?post_type=$post_type" : "",
                            'headers' => [],
                            'rows' => [],
                        ];
                        for ( $i = 0; $i < $max_fields; $i++ ) {
                            $field_name = ucfirst(str_replace('_', ' ', $fillables[$i]));
                            $results[ $index_results ]['headers'][] = __( $field_name );
                        }
                    }

                    //if ( $index_results == 'Post' ) dd($table->found_rows);

                    // Emplenar rows
                    for ( $i = 0; $i < $max_fields; $i++ ) {
                        $field_name = $fillables[$i];
                        $value = $item->{$field_name};
                        if ( like_match('media%', $field_name) ) {
                            $thumbnail = '';
                            $thumbnail_url = $item->get_thumbnail_url('thumbnail');
                            if ( !empty($thumbnail_url) ) {
                                $thumbnail = "<img src='$thumbnail_url'>";
                            }
                            $results[ $index_results]['rows'][$id][] = $thumbnail;
                        } else {
                            $results[ $index_results]['rows'][$id][] = get_excerpt($value, 20);
                        }
                    }

                    // Edit
                    if ( \Route::has("admin.$class_table.edit") ) {
                        $results[ $index_results ]['rows'][$id]['edit'] = route("admin.$class_table.edit", $id) . ( !empty($post_type) ? "?post_type=$post_type" : "" );
                    } else if ( \Route::has("admin.$class_table.index") )
                        $results[ $index_results ]['rows'][$id]['edit'] = route("admin.$class_table.index");
                    else if ( \Route::has("admin.$class_table") )
                        $results[ $index_results ]['rows'][$id]['edit'] = route("admin.$class_table");
                    else 
                        $results[ $index_results ]['rows'][$id]['edit'] = '';
                    
                    // Show
                    if ( !empty($post_type) ) {
                        //$results[ $index_results ]['rows'][$id]['show'] = route("admin.posts.index") . "?post_type=$post_type";
                        $results[ $index_results ]['rows'][$id]['show'] = $post_url;
                    }
                    
                }
            }
        }
        return $results;
    }

    protected function get_model_by_table_name( $table_name ) {

        $is_subtable = false;
        if ( like_match('%_translations', $table_name) ) {
            $table_name = str_replace('_translations', '', $table_name) . 's';
            $is_subtable = true;
        } else if ( like_match('%_meta', $table_name) ) {
            $table_name = str_replace('_meta', '', $table_name) . 's';
            $is_subtable = true;
        }

        $p = glob(app_path('*.php'));
        $p = array_merge( rglob(app_path('Models/*.php')), $p );

        foreach ( $p as $file ) {
            if ( like_match( 'extends Model', file_get_contents( $file , FILE_USE_INCLUDE_PATH), true ) ) {
                $class_name = $this->get_class_name_by_file($file);
                $parts = array_reverse(explode('\\', $class_name));
                $class_name_alone = reset($parts);
                if ( !empty($class_name) && !empty(trim($class_name_alone)) ) {
                    if ( !class_exists($class_name) ) include($file);
                    $class = new $class_name;
                    if ( empty($class->table) ) {
                        $class->table = strtolower(preg_replace('/([A-Z])/', '_$1', $class_name_alone));
                        $class->table = substr($class->table, 1);
                        $class->table = $class->table . 's';
                    }
                    if ( $class->table == $table_name ) {
                        $class->is_subtable = $is_subtable;
                        return [
                            'class_name' => $class_name,
                            'class_name_alone' => $class_name_alone,
                            'class_table' => $class->table,
                            'is_subtable' => $is_subtable
                        ];
                    }
                }
                
            }
        }

        return;
    }

    protected function get_class_name_by_file($file) {
        $fp = fopen($file, 'r');
        $class = $namespace = $buffer = '';
        $i = 0;
        while (!$class) {
            if (feof($fp)) break;

            $buffer .= fread($fp, 512);
            $tokens = token_get_all($buffer);

            if (strpos($buffer, '{') === false) continue;

            for (;$i<count($tokens);$i++) {
                if ($tokens[$i][0] === T_NAMESPACE) {
                    for ($j=$i+1;$j<count($tokens); $j++) {
                        if ($tokens[$j][0] === T_STRING) {
                            $namespace .= '\\'.$tokens[$j][1];
                        } else if ($tokens[$j] === '{' || $tokens[$j] === ';') {
                            break;
                        }
                    }
                }

                if ($tokens[$i][0] === T_CLASS) {
                    for ($j=$i+1;$j<count($tokens);$j++) {
                        if ($tokens[$j] === '{' && !empty($tokens[$i+2][1]) ) {
                            $class = $tokens[$i+2][1];
                        }
                    }
                }
            }
        }
        return (!empty($namespace) ? $namespace . '\\' : '') . $class;
    }

    protected function get_widget($params) {
        
        $widget = new \stdClass();
        $widget->status = !empty($params['status']) ? $params['status'] : '';
        $widget->name = !empty($params['name']) ? $params['name'] : '';
        $widget->title = !empty($params['title']) ? $params['title'] : '';
        $widget->icon = !empty($params['icon']) ? $params['icon'] : '';
        $widget->icon_class = !empty($params['icon_class']) ? $params['icon_class'] : '';
        $widget->headers = !empty($params['headers']) ? $params['headers'] : [];
        $widget->rows = [];
        /*$items = \DB::table("calendario_vacaciones")
        ->select( \DB::raw("user_id, date_format(start, '%Y-%m') AS mes, MIN(start) AS min_start, MAX(end) AS max_end, MAX(updated_at) as last_updated") )
        ->where( 'status', $widget->status )
        ->groupBy(['user_id', 'mes'])
        ->orderByRaw("last_updated DESC, min_start ASC, user_id ASC")
        ->limit( ( isset($params['limit']) ? $params['limit'] : 10 ) )
        ->get();

        foreach ( $items as $item ) {
            $es_encarregat = false;
            $user = User::find($item->user_id);
            if ( $user == null || !$user->exists() ) continue;
            if ( $user->subnivel != null && $user->subnivel->exists() ) {
                $es_encarregat = in_array($user->subnivel->id, $nivells_encarregats) || $es_admin;
                $nivel = $user->subnivel->name;
            } else if ( $user->nivel != null && $user->nivel->exists() ) {
                $es_encarregat = in_array($user->nivel->id, $nivells_encarregats) || $es_admin;
                $nivel = $user->nivel->name;
            } else $nivel = '-';
            
            if ( !$es_encarregat ) continue;

            $start = \Carbon\Carbon::parse($item->min_start)->format('d/m/Y');
            $end = \Carbon\Carbon::parse($item->max_end)->format('d/m/Y');
            $dates = $start != $end ? $start.' - '.$end : $start;
            $widget->rows[] = [
                $user->fullname(true),
                $user->email,
                $nivel,
                $dates,
            ];
        }*/
        return $widget;
    }

}
