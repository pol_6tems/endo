<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Rol;
use Illuminate\Http\Request;
use App\Models\Chat;
use App\Models\Email;

class ChatsController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Chats');
        $this->section_icon = 'chat_bubble';
        $this->section_route = 'admin.chats';
    }

    public function index() {
        $items = Chat::orderby('name')->get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'toolbar_header' => [],
            'headers' => [
                __('Name') => [],
                __('Type') => [],
                __('Email') => [],
                __('Updated at') => [],
            ],
            'rows' => [
                ['value' => 'name'],
                ['value' => 'type'],
                ['value' => ['email', 'name']],
                ['value' => 'updated_at', 'type' => 'date'],
            ],
        ));
    }

    public function create() {
        $emails = Email::orderby('name')->get();
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name')],
                ['value' => 'type', 'title' => __('Type')],
                ['value' => 'email_id', 'title' => __('Email'), 'type' => 'select', 'required' => true,
                    'datasource' => $emails, 'sel_value' => 'id', 'sel_title' => ['name'], 'sel_search' => true
                ],
                ['type' => 'subform_inline', 'value' => 'Admin::default.subforms.email_lines', 'help' => '$this = Mensaje'],
                ['type' => 'subform_inline', 'value' => 'Admin::default.subforms.chat_buttons', 'help' => '$this = Mensaje'],
            ],
        ));
    }

    public function store(Request $request) {
        $data = $request->all();
        $data['lines'] = json_encode($data['lines']);
        $data['buttons'] = json_encode($data['buttons']);
        Chat::create($data);
        return redirect()->route($this->section_route . '.index')->with([ 'message' => __(':name added successfully', ['name' => __('Chat')]) ]);
    }

    public function show($locale, $id) { }

    public function edit($locale, $id) {
        $item = Chat::find($id);
        
        if ( empty($item) ) return redirect()->route($this->section_route . '.index')->with([ 'message' => __(":name not found", ['name' => __('Chat')]) ]);

        $emails = Email::orderby('name')->get();
        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name')],
                ['value' => 'type', 'title' => __('Type')],
                ['value' => 'email_id', 'title' => __('Email'), 'type' => 'select', 'required' => true,
                    'datasource' => $emails, 'sel_value' => 'id', 'sel_title' => ['name'], 'sel_search' => true
                ],
                ['type' => 'subform_inline', 'value' => 'Admin::default.subforms.email_lines', 'help' => '$this = Mensaje'],
                ['type' => 'subform_inline', 'value' => 'Admin::default.subforms.chat_buttons', 'help' => '$this = Mensaje'],
            ],
        ));
    }

    public function update(Request $request, $locale, $id) {
        $item = Chat::find($id);
        
        if ( empty($item) ) return redirect()->route($this->section_route . '.index')->with([ 'message' => __(":name not found", ['name' => __('Chat')]) ]);

        $data = $request->all();
        $data['lines'] = json_encode($data['lines']);
        $data['buttons'] = json_encode($data['buttons']);

        if ( $item->update($data) ) {
            $msg = __(':name updated successfully', ['name' => __('Chat')]);
        } else {
            $msg = __('Error saving data');
        }
        return redirect()->route('admin.chats.edit', $id)->with([ 'message' => $msg ]);
    }

    public function destroy($locale, $id) {
        $item = Chat::find($id);
        if ( !empty($item) && $item->delete() ) {
            return response()->json(['success'=>'Done!']);
        }
        return response()->json(['error'=>'Done!'], 401);
    }


    public function changePermission($locale, $id)
    {
        $status = request('status', 0);
        $roleId = request('role_id');

        if (!$roleId) {
            abort(404);
        }

        $chat = Chat::findOrFail($id);
        $role = Rol::findOrFail($roleId);

        $chat->permissions = array_merge($chat->permissions, [$role->name => $status]);
        $chat->save();
    }
}
