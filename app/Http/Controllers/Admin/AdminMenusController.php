<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Admin\AdminMenu;
use App\Models\Admin\AdminMenuSub;

class AdminMenusController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Menus');
        $this->section_icon = 'menu';
        $this->section_route = 'admin.menus';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = AdminMenu::get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'headers' => [
                __('Order') => [ 'width' => '10%' ],
                __('Title') => [],
                __('Name') => [],
                __('Icon') => [ 'width' => '10%' ],
            ],
            'rows' => [
                ['value' => 'order'],
                ['value' => 'name', 'translate' => true],
                ['value' => 'name'],
                ['value' => 'icon', 'type' => 'icon'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'order', 'title' => __('Order')],
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'icon', 'title' => __('Icon'), 'help' => '<a href="https://materializecss.com/icons.html">https://materializecss.com/icons.html</a>'],
                ['type' => 'subform', 'value' => 'Admin::default.subforms.menu'],
            ],
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if ( empty($data['order']) ) $data['order'] = 0;
        if ( !empty($data['has_url']) ) $data['has_url'] = $data['has_url'] == 'on';
        else $data['has_url'] = false;
        $new = AdminMenu::create($data);
        //return redirect()->route('admin.menus.index')->with(['message' => __('Menu added successfully')]);
        return redirect()->route('admin.menus.edit', $new->id)->with(['message' => __('Menu added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = AdminMenu::findOrFail($id);
        $subs = AdminMenuSub::where('menu_id', "=", $id)->orderby('order')->get();
        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'order', 'title' => __('Order')],
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'icon', 'title' => __('Icon'), 'help' => '<a href="https://materializecss.com/icons.html">https://materializecss.com/icons.html</a>'],
                ['type' => 'subform', 'value' => 'Admin::default.subforms.menu'],
            ],
            'subs' => [
                [
                    'section_title' => __('Submenus'),
                    'section_icon' => $this->section_icon,
                    'items' => $subs,
                    'route' => $this->section_route . '.subs',
                    'field_name' => 'name',
                    'headers' => [
                        __('Order') => [ 'width' => '10%' ],
                        __('Title') => [],
                        __('Url') => [],
                        __('Icon') => [ 'width' => '10%' ],
                    ],
                    'rows' => [
                        ['value' => 'order'],
                        ['value' => 'name', 'translate' => true],
                        ['value' => 'url'],
                        ['value' => 'icon', 'type' => 'icon'],
                    ]
                ]
            ]
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $item = AdminMenu::findOrFail($id);
        $data = $request->all();
        if ( empty($data['order']) ) $data['order'] = 0;
        if ( !empty($data['has_url']) ) $data['has_url'] = $data['has_url'] == 'on';
        else $data['has_url'] = false;
        $item->update($data);
        //return redirect()->route('admin.menus.index')->with(['message' => __('Menu updated successfully')]);
        return redirect()->route('admin.menus.edit', $id)->with(['message' => __('Menu updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $item = AdminMenu::findOrFail($id);
        $item->delete();
        return redirect()->route('admin.menus.index')->with(['message' => __('Menu deleted successfully')]);
    }

    /* MenuSub */
    public function submenu_create($locale, $id)
    {
        return View('Admin::default.subcreate', array(
            'section_title' => __('Submenus'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.subs',
            'route_parent' => $this->section_route,
            'rows' => [
                ['value' => 'menu_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'order', 'title' => __('Order')],
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'icon', 'title' => __('Icon'), 'help' => '<a href="https://materializecss.com/icons.html">https://materializecss.com/icons.html</a>'],
                ['value' => 'url_type', 'title' => __('Type'), 'type' => 'select', 'choices' => [
                    ['value' => 'route', 'title' => __('ROUTE')],
                    ['value' => 'url', 'title' => __('URL')]
                ], 'required' => true],
                ['value' => 'url', 'title' => __('URL')],
                ['value' => 'controller', 'title' => __('Controller')],
                ['value' => 'parameters', 'title' => __('Parameters')],
            ],
        ));
    }

    public function submenu_store(Request $request)
    {
        $data = $request->all();
        if (empty($data['order'])) $data['order'] = 0;
        AdminMenuSub::create($data);
        return redirect()->route('admin.menus.edit', $data['menu_id'])->with(['message' => __('Menu added successfully')]);
    }

    public function submenu_edit($locale, $id)
    {
        $item = AdminMenuSub::findOrFail($id);
        return View('Admin::default.subedit', array(
            'item' => $item,
            'section_title' => __('Submenus'),
            'section_icon' => $this->section_icon,
            'route' => $this->section_route . '.subs',
            'route_parent' => $this->section_route,
            'id_parent' => $item->menu_id,
            'rows' => [
                ['value' => 'menu_id', 'type' => 'hidden', 'content' => $id],
                ['value' => 'order', 'title' => __('Order')],
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'icon', 'title' => __('Icon'), 'help' => '<a href="https://materializecss.com/icons.html">https://materializecss.com/icons.html</a>'],
                ['value' => 'url_type', 'title' => __('Type'), 'type' => 'select', 'choices' => [
                    ['value' => 'route', 'title' => __('ROUTE')],
                    ['value' => 'url', 'title' => __('URL')]
                ], 'required' => true],
                ['value' => 'url', 'title' => __('URL')],
                ['value' => 'controller', 'title' => __('Controller')],
                ['value' => 'parameters', 'title' => __('Parameters')],
            ],
        ));
    }

    public function submenu_update(Request $request, $locale, $id)
    {
        $item = AdminMenuSub::findOrFail($id);
        $data = $request->all();
        if (empty($data['order'])) $data['order'] = 0;
        $item->update($data);
        return redirect()->route('admin.menus.edit', $item->menu_id)->with(['message' => __('Menu updated successfully')]);
    }
    
    public function submenu_destroy($locale, $id)
    {
        $item = AdminMenuSub::findOrFail($id);
        $menu_id = $item->menu_id;
        $item->delete();
        return redirect()->route('admin.menus.edit', $menu_id)->with(['message' => __('Menu deleted successfully')]);
    }
    /* end MenuSub */
}
