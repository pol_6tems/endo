<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Models\CustomPost;
use App\Models\CustomField;
use Illuminate\Http\Request;
use App\Models\CustomFieldGroup;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use App\Models\Admin\Rol;

class CustomFieldsController extends AdminController
{

    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Custom Fields');
        $this->section_icon = 'library_add';
        $this->section_route = 'admin.custom_fields';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = CustomFieldGroup::get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'title',
            'has_duplicate' => true,
            'headers' => [
                __('Order') => ['width' => '10%'],
                __('Type') => [],
                __('Title') => [],
                __('Template') => [ 'width' => '20%' ],
                __('Updated') => [ 'width' => '15%' ],
            ],
            'rows' => [
                ['value' => 'order'],
                ['value' => 'post_type'],
                ['value' => 'title'],
                ['value' => 'template'],
                ['value' => 'updated_at', 'type' => 'date'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $custom_posts = $this->get_custom_posts();

        $templates = (new PostsController)->get_templates();
        foreach ($templates as $k => $t) $templates[$k] = ['title' => $t, 'value' => $t];

        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'order', 'title' => __('Order'), 'type' => 'number'],
                ['value' => 'post_type', 'title' => __('Type'), 'type' => 'select', 'required' => true,
                    'datasource' => $custom_posts, 'sel_value' => 'post_type', 'sel_title' => ['title_plural'], 'sel_search' => true
                ],
                ['value' => 'title', 'title' => __('Title'), 'required' => true],
                ['value' => 'template', 'title' => __('Template'), 'type' => 'select', 'sel_search' => true,
                    'choices' => $templates
                ],
                ['value' => 'position', 'title' => __('Position'), 'type' => 'select',
                    'choices' => [
                        ['title' => 'Sidebar', 'value' => 'r-sidebar'],
                        ['title' => 'Bottom', 'value' => 'bottom'],
                        ['title' => 'Main', 'value' => 'main'],
                    ],
                ],
            ],
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        if ( empty($data['order']) ) $data['order'] = 0;
        if ( empty($data['position']) ) $data['position'] = 'bottom';
        
        $new = CustomFieldGroup::create($data);
        return redirect()->route('admin.custom_fields.edit', $new->id)->with(['message' => __('Group added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $custom_posts = $this->get_custom_posts();

        $item = CustomFieldGroup::findOrFail($id);
        $item->load(['fields.fields']);
        $templates = (new PostsController)->get_templates();
        foreach ($templates as $k => $t) $templates[$k] = ['title' => $t, 'value' => $t];

        $pages = array();
        foreach(Post::where('type', 'page')->get() as $page) {
            $pages[] = [
                'title' => $page->translate()->title . ' (' . $page->translate()->post_name . ')',
                'value' => $page->translate()->id
            ];
        }

        $post_types = array("any" => __('Any') . ' ('.__('All').')');
        foreach($custom_posts as $cp) {
            $key = !empty($cp->id) ? $cp->id : $cp->post_type;
            $post_types[$key] = $cp->title . ' ('. $cp->post_type .')';
        }

        $customFieldsGroups = CustomFieldGroup::where('post_type', $item->post_type)->where('template', $item->template)->get();

        $roles = Rol::get()->pluck('name', 'id')->toArray();

        View::share ( 'post_types', $post_types );
        View::share ( 'cfg', $customFieldsGroups );
        View::share ( 'roles', $roles );        

        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'order', 'title' => __('Order'), 'type' => 'number'],
                ['value' => 'post_type', 'title' => __('Type'), 'type' => 'select', 'required' => true,
                    'datasource' => $custom_posts, 'sel_value' => 'post_type', 'sel_title' => ['title_plural'], 'sel_search' => true
                ],
                ['value' => 'title', 'title' => __('Title'), 'required' => true],
                ['value' => 'template', 'title' => __('Template'), 'type' => 'select', 'sel_search' => true,
                    'choices' => $templates
                ],/*
                ['value' => 'template', 'title' => __('Reglas'), 'type' => 'select',
                    'choices' => $pages,
                ],*/
                ['value' => 'position', 'title' => __('Position'), 'type' => 'select',
                    'choices' => [
                        ['title' => 'Sidebar', 'value' => 'r-sidebar'],
                        ['title' => 'Bottom', 'value' => 'bottom'],
                        ['title' => 'Main', 'value' => 'main'],
                    ],
                ],
                ['type' => 'subform', 'value' => 'Admin::default.subforms.custom_fields'],
            ],
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $item = CustomFieldGroup::findOrFail($id);
        $data = $request->all();

        if ( empty($data['order']) ) $data['order'] = 0;
        if ( empty($data['position']) ) $data['position'] = 'bottom';

        $fields = array();
        if ( !empty($data['fields']) ) {
            $fields = $data['fields'];
            unset($data['fields']);
        }


        $data['post_type'] = str_slug($data['post_type'], '-');

        $item->update($data);
        
        /* FIELDS */
        $ids = array();

        
        // Ara els paràmetres no es salten simplement sinó que s'afegeixen al field que no en té i el qual es pare
        foreach ( $fields as $k => $aux ) {
            foreach ( $fields as $aux2 ) {
                // Si anem a saltar-lo mirem si té parent_id i li assignem al pare els parametres del fill
                if ( empty($aux2['field_title']) || empty($aux2['field_name']) || empty($aux2['field_type']) ) {
                    if ( !empty($aux['field_id']) && $aux['field_id'] == $aux2['parent_id'] && isset($aux2['params'])) {
                        $fields[$k]['params'] = $aux2['params'];
                    }
                }

            }
        }

        foreach ( $fields as $field ) {
            if ( empty($field['field_title']) || empty($field['field_name']) || empty($field['field_type']) ) {
                continue;
            }
            
            $f_data = [
                'order' => $field['field_order'],
                'cfg_id' => $id,
                'title' => $field['field_title'],
                'name' => $field['field_name'],
                'type' => $field['field_type'],
                'params' => (isset($field['params'])) ? json_encode($field['params']) : '{}',
            ];
            
            if ( !empty($field['parent_id']) ) $f_data['parent_id'] = $field['parent_id'];

            if ( !empty($field['field_id']) ) {
                $f = CustomField::find($field['field_id']);
                $f->update($f_data);
                $ids[] = $f->id;
            } else {
                $new = CustomField::create($f_data);
                $ids[] = $new->id;
            }
        }

        CustomField::where('cfg_id', $id)->whereNotIn('id', $ids)->delete();
        /* end FIELDS */

        return redirect()->route('admin.custom_fields.edit', $item->id)->with(['message' => __('Group updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $item = CustomFieldGroup::findOrFail($id);
        $item->delete();
        return redirect()->route('admin.custom_fields.index')->with(['message' => __('Group deleted successfully')]);
    }

    public function massDestroy(Request $request)
    {
        $ids = explode(',', $request->input('ids'));
        foreach ($ids as $id) {
            $item = CustomFieldGroup::findOrFail($id);
            //$item->status = 'trash';
            //$item->update();
            $item->delete();
        }
        return redirect()->route('admin.custom_fields.index')->with(['message' => __('Group deleted successfully')]);
    }

    public function get_field_types() {
        return [
            'text',
            'textarea',
            'number',
            'date',
            'date',
        ];
    }

    public function get_field_params($locale) {
        $fieldtype = request()->query()['field_type'];
        
        /*
        return View('Admin::default.custom_field_params.'.$fieldtype, [
            'is_create' => false,
            'params' => $params,
            'key' => $key,
            'item' => $item
        ]); */

        $params = '';
        foreach((new CustomField)->getParams()[$fieldtype] as $param) {
            $FieldType = new $param();
            $params .= $FieldType->getField()->render();
        }
        return $params;
    }

    public function duplicate($locale, $id) {
        $item = CustomFieldGroup::find($id);
        
        $data = [
            'title' => $item->title . ' (copy)',
            'post_type' => $item->post_type,
            'template' => $item->template,
            'order' => $item->order,
            'position' => $item->position,
        ];

        $new = CustomFieldGroup::create($data);

        // Duplicate custom_fields
        $custom_fields = CustomField::where('cfg_id', $id)->get();
        foreach ( $custom_fields as $field ) {
            $new_field = [
                'cfg_id' => $new->id,
                'order' => $field->order,
                'title' => $field->title,
                'name' => $field->name,
                'type' => $field->type,
                'instructions' => $field->instructions,
                'post_type' => $field->post_type,
                'post_field' => $field->post_field,
                'params' => $field->params,
                'parent_id' => $field->parent_id,
            ];
            CustomField::create($new_field);
        }

        return redirect(route('admin.custom_fields.index'))->with(['message' => __(':name duplicated successfully', ['name' => __('Group')]) ]);
    }


    private function get_custom_posts() {
        $custom_posts = collect();
        // Page
        $cp = new CustomPost();
        $cp->post_type = 'page';
        $cp->title_plural = __('Pages');
        $cp->title = __('Page');
        $custom_posts[] = $cp;
        // Post
        $cp = new CustomPost();
        $cp->post_type = 'post';
        $cp->title_plural = __('Posts');
        $cp->title = __('Post');
        $custom_posts[] = $cp;
        // User
        $cp = new CustomPost();
        $cp->post_type = 'user';
        $cp->title_plural = __('Users');
        $cp->title = __('User');
        $custom_posts[] = $cp;
        $cps = CustomPost::withTranslation()->get();
        
        return $custom_posts->merge($cps);
    }
}
