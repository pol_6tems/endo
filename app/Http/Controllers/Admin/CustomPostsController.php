<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\CustomPost;

class CustomPostsController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Custom Posts');
        $this->section_icon = 'library_books';
        $this->section_route = 'admin.custom_posts';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = CustomPost::get();
        return View('Admin::default.index', array(
            'translable' => true,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'title',
            'headers' => [
                __('Type') => [],
                __('Title') => [],
                __('Updated') => [ 'width' => '15%' ],
            ],
            'rows' => [ // Columnes a mostrar
                ['value' => 'post_type'],
                ['value' => 'title'],
                ['value' => 'updated_at', 'type' => 'date'],
            ],
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('Admin::default.lang_create', array(
            'translable' => true,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'post_type', 'title' => __('Type'), 'required' => true],
                ['value' => 'title', 'title' => __('Title'), 'required' => true],
                ['value' => 'post_type_plural', 'title' => __('Type plural'), 'required' => true],
                ['value' => 'title_plural', 'title' => __('Title plural'), 'required' => true],
            ],
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code]) ) {
                $data[$lang->code]['post_type'] = str_slug($data[$lang->code]['post_type'], '-');
                $data[$lang->code]['post_type_plural'] = str_slug($data[$lang->code]['post_type_plural'], '-');
            }
        }
        $data['params'] = json_encode($data['params']);
        // Default value for Admin:
        $data['permissions'] = '{"admin":{"index":"1","create":"1","store":"1","edit":"1","update":"1","destroy":"1","massDestroy":"1","restore":"1","destroy_permanent":"1","get_templates":"1","duplicate":"1"}}';

        unset($data['_token']);
        unset($data['next']);

        $new = CustomPost::create($data);
        return redirect()->route('admin.custom_posts.edit', $new->id)->with(['message' => __('Custom Post added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = CustomPost::findOrFail($id);
        return View('Admin::default.lang_edit', array(
            'translable' => true,
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'post_type', 'title' => __('Type'), 'required' => true],
                ['value' => 'title', 'title' => __('Title'), 'required' => true],
                ['value' => 'post_type_plural', 'title' => __('Type plural'), 'required' => true],
                ['value' => 'title_plural', 'title' => __('Title plural'), 'required' => true],
            ],
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $item = CustomPost::findOrFail($id);
        $data = $request->all();
        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code]) ) {
                $data[$lang->code]['post_type'] = str_slug($data[$lang->code]['post_type'], '-');
                $data[$lang->code]['post_type_plural'] = str_slug($data[$lang->code]['post_type_plural'], '-');
            }
        }

        $data['params'] = json_encode($data['params']);

        if ( isset($data['permissions']) ) {
            $data['permissions'] = json_encode($data['permissions']);
        }

        unset($data['_token']);
        unset($data['_method']);
        unset($data['next']);

        $item->update($data);
        return redirect()->route('admin.custom_posts.edit', $id)->with(['message' => __('Custom Post updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $item = CustomPost::findOrFail($id);
        $item->delete();
        return redirect()->route('admin.custom_posts.index')->with(['message' => __('Custom Post deleted successfully')]);
    }

    public function massDestroy(Request $request)
    {
        $ids = explode(',', $request->input('ids'));
        foreach ($ids as $id) {
            $item = CustomPost::findOrFail($id);
            //$item->status = 'trash';
            //$item->update();
            $item->delete();
        }
        return redirect()->route('admin.custom_posts.index')->with(['message' => __('Custom Post deleted successfully')]);
    }
}
