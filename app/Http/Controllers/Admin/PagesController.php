<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\StorePostRequest;
use App\Post;
use App\Theme;
use View;

class PagesController extends PostsController
{
    protected $post_type;
    protected $post_type_plural;

    public function __construct()
    {
        parent::__construct();
        $this->post_type = 'page';
        $this->post_type_plural = 'pages';
        
        View::share ( 'post_type', $this->post_type );
        View::share ( 'post_type_plural', $this->post_type_plural );
    }
    public function index()
    {
        $items = Post::where('type', $this->post_type)->paginate(20);
        $items_trash = Post::onlyTrashed()->where('type', $this->post_type)->paginate(20);
        return view('Admin::'.$this->post_type_plural.'.index', compact('items', 'items_trash'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Post::class);
        $templates = $this->get_templates();
        return view('Admin::'.$this->post_type_plural.'.create', compact('templates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Post::class);
        $data = $request->all();
        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code]) ) {
                $sanitize_title = str_slug($data[$lang->code]['title'], '-');
                $data[$lang->code]['post_name'] = $sanitize_title;
            }
        }
        Post::create($data);
        return redirect()->route('admin.'.$this->post_type_plural.'.index')->with(['message' => __(ucfirst($this->post_type) . ' added successfully')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $this->authorize('update', Post::class);
        $post = Post::withTrashed()->findOrFail($id);
        $templates = $this->get_templates();
        return view('Admin::'.$this->post_type_plural.'.edit', compact('post', 'templates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $this->authorize('update', Post::class);
        $post = Post::findOrFail($id);
        $data = $request->all();
        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code]) && empty( $data[$lang->code]['post_name'] ) ) {
                $sanitize_title = str_slug($data[$lang->code]['title'], '-');
                $data[$lang->code]['post_name'] = $sanitize_title;
                $data[$lang->code]['post_name'] = str_replace('//', '/', $data[$lang->code]['post_name']);
            } else if ( !empty($data[$lang->code]) && !empty( $data[$lang->code]['post_name'] ) ) {
                $sanitize_title = str_slug($data[$lang->code]['post_name'], '-');
                $data[$lang->code]['post_name'] = $sanitize_title;
                $data[$lang->code]['post_name'] = str_replace('//', '/', $data[$lang->code]['post_name']);
            }
        }
        $post->update($data);
        return redirect()->route('admin.'.$this->post_type_plural.'.edit', $post->id)->with(['message' => __(ucfirst($this->post_type) . ' updated successfully')]);
        //return redirect()->route('admin.'.$this->post_type_plural.'.index')->with(['message' => __(ucfirst($this->post_type) . ' updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $this->authorize('delete', Post::class);
        $post = Post::findOrFail($id);
        $post->status = 'trash';
        $post->update();
        $post->delete();
        
        return redirect()->route('admin.'.$this->post_type_plural.'.index')->with(['message' => __(ucfirst($this->post_type) . ' sent to trash')]);
    }

    public function massDestroy(Request $request)
    {
        $this->authorize('delete', Post::class);
        $posts = explode(',', $request->input('ids'));
        foreach ($posts as $post_id) {
            $post = Post::findOrFail($post_id);
            $post->status = 'trash';
            $post->update();
            $post->delete();
        }
        return redirect()->route('admin.'.$this->post_type_plural.'.index')->with(['message' => __(ucfirst($this->post_type) . ' sent to trash')]);
    }

    public function restore($locale, $id)
    {
        $this->authorize('delete', Post::class);
        $post = Post::withTrashed()->find($id);
        $post->status = 'publish';
        $post->update();
        $post->restore();
        
        return redirect()->route('admin.'.$this->post_type_plural.'.index')->with(['message' => __(ucfirst($this->post_type) . ' restored successfully.')]);
    }

    public function destroy_permanent($locale, $id)
    {
        $this->authorize('delete', Post::class);
        $post = Post::withTrashed()->find($id);
        $post->forceDelete();
        
        return redirect()->route('admin.'.$this->post_type_plural.'.index')->with(['message' => __(ucfirst($this->post_type) . ' deleted successfully.')]);
    }

    public function get_templates($post_type = null) {
        if ( $post_type == null ) $post_type = $this->post_type;
        $templates = array();
        $theme = Theme::where('active', true);
        if ( $theme->count() > 0 ) {
            $tema_path = base_path() . $theme->first()->path . "/posts/" . $post_type . "/templates";
            if ( is_dir( $tema_path ) ) {
                foreach ( scandir($tema_path) as $dir ) {
                    if ( is_dir( $dir ) ) continue;
                    $templates[] = str_replace('.blade.php', '', $dir);
                }
            }
        }
        return $templates;
    }
}
