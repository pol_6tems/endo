<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use Validator;
use App\Models\UserMeta;
use App\Models\Admin\Rol;
use App\Models\CustomField;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\CustomFieldGroup;
use App\Http\Requests\StoreEndoUser;
use Illuminate\Support\Facades\Input;

class UsersController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;
    protected $section_type;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Users');
        $this->section_icon = 'person_pin';
        $this->section_route = 'admin.users';
        $this->section_type = 'user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = User::get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'email',
            'toolbar_header' => [
                //'<a target="_blank" href="' . route('admin.users.exportar') . '" class="btn btn-default btn-sm">' . __('Export') . '</a>',
            ],
            'headers' => [
                __('#') => [ 'width' => '5%' ],
                __('Name') => [],
                __('Email') => [],
                __('ID card') => [],
                __('Created at') => [ 'width' => '15%' ],
                __('Role') => [],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => ['fullname()']],
                ['value' => 'email'],
                ['value' => 'dni'],
                ['value' => 'created_at', 'type' => 'date'],
                ['value' => 'role', 'translate' => true, 'auth' => true],
            ],
        ));
    }
    /*
    // Pol: Versió vella però ho deixo pel tema dels filtres i cerca per futures implementacions.
    public function index()
    {
        $sortby = Input::get('sortby', 'id');
        $order = Input::get('order', 'ASC');
        $search = Input::get('search', '');

        if ( empty($search) ) {
            $items = User::orderby($sortby, $order)->paginate(20);
        } else {
            $items = User::where(function($q) use ($search) {
                $q->orWhere('id', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('name', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('lastname', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('email', 'LIKE', '%'.strtoupper($search).'%');
            })->orderby($sortby, $order)->paginate(20);
        }

        return View('admin.administracion.users.index', compact('items', 'sortby', 'order', 'search'));
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $level = Auth::user()->rol->level;
        $cf_groups = CustomFieldGroup::where('post_type', $this->section_type)->orderBy('order')->get();
        $rols = Rol::where('level', '<=', $level)->get()->sortby('name');
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'type' => $this->section_type,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'lastname', 'title' => __('Lastname'), 'required' => true],
                ['value' => 'email', 'title' => __('Email'), 'required' => true, 'type' => 'email'],
                ['value' => 'password', 'title' => __('Password'), 'required' => true, 'type' => 'password'],
                ['value' => 'password_confirmation', 'title' => __('Confirm password'), 'required' => true, 'type' => 'password'],
                ['value' => 'role', 'title' => __('Role'), 'type' => 'select', 'required' => true,
                    'datasource' => $rols, 'sel_value' => 'name', 'sel_title' => ['name'], 'sel_search' => true
                ],
            ],
            'cf_groups' => $cf_groups,
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEndoUser $request) {
        //Si la validación no es correcta redireccionar al formulario con los errores
        if ( $request->validated() ) {
            $token = Str::random(60);
            
            $user = User::create([
                "name" => $request->name,
                "lastname" => $request->lastname,
                "email" => $request->email,
                "password" => bcrypt($request->password),
                "remember_token" => str_random(100),
                "role" => $request->role
            ]);
            
            if ( $user ) {
                if (isset($request->custom_fields)) {
                    $this->save_custom_fields($user, $request->custom_fields);
                }
                
                execute_actions('save_user', $user); // Hook
                return redirect()->route('admin.users.index')->with([ "success" => true, "message" => __("User added successfully") ]);
            } else {
                return redirect()->back()->with([ "success" => false, "error" => __("An error occurred while saving the data") ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        return redirect()->route('admin.users.edit', ['id' => $id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, User $user)
    {
        $level = Auth::user()->rol->level;
        $rols = Rol::where('level', '<=', $level)->get()->sortby('name');

        $cf_groups = CustomFieldGroup::where('post_type', $this->section_type)->orderBy('order')->get();

        $permisos = auth()->user()->rol->get_permisos();
        $canCreate = true;

        if (!auth()->user()->isAdmin() && !(!empty($permisos['general']['adminuserscontroller']['create']) && $permisos['general']['adminuserscontroller']['create'])) {
            $canCreate = false;
        }

        return View('Admin::default.edit', array(
            'item' => $user,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'item_type' => $this->section_type,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'lastname', 'title' => __('Lastname'), 'required' => true],
                ['value' => 'email', 'title' => __('Email'), 'required' => true, 'type' => 'email'],
                ['value' => 'password', 'title' => __('Password'), 'required' => false, 'type' => 'password'],
                ['value' => 'password_confirmation', 'title' => __('Confirm password'), 'required' => false, 'type' => 'password'],
                ['value' => 'role', 'title' => __('Role'), 'type' => 'select', 'required' => true,
                    'datasource' => $rols, 'sel_value' => 'name', 'sel_title' => ['name'], 'sel_search' => true, 'auth' => true
                ],
            ],
            'cf_groups' => $cf_groups,
            'can_create' => $canCreate
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, User $user)
    {
        $user = $this->fillUser($user, $request);
        // Hook
        execute_actions('save_user', $user);

        $permisos = $user->rol->get_permisos();

        if ($user->isAdmin() || (!empty($permisos['general']['adminuserscontroller']['index']) && $permisos['general']['adminuserscontroller']['index'])) {
            return redirect()->route('admin.users.index')->with([ "success" => true, "message" => __('User updated successfully') ]);
        } else {
            return redirect()->route('admin.users.edit', ['id' => $user->id])->with([ "success" => true, "message" => __('User updated successfully') ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, User $user)
    {
        $user->delete();
		if ( $user ) return redirect()->back()->with('message', __('User deleted successfully'));
        else return redirect()->back()->with('error', __('An error occurred while deleting the data'));
    }


    protected function save_custom_fields(User $user, $fields = [], $is_child = false)
    {
        if ($is_child) {
            foreach ($fields as $idx => $subfields) {
                $this->save_custom_fields($user, $subfields);
            }
        } else {
            $order = -1;

            foreach ($fields as $field_id => $field) {
                if ($field_id == 'order') {
                    $order = $field;
                    continue;
                }

                $field_value = empty($field['fields']) ? $field : null;

                if (is_array($field_value)) {
                    $field_value = json_encode($field_value);
                }

                $meta = [
                    'user_id' => $user->id,
                    'custom_field_id' => $field_id,
                    'value' => $field_value,
                    'post_id' => $this->section_type
                ];

                $where = [['user_id', $user->id], ['custom_field_id', $field_id]];

                // Parent_id
                $f_aux = CustomField::find($field_id);
                if (!empty($f_aux->parent_id)) {
                    $meta['parent_id'] = $f_aux->parent_id;
                    $where[] = ['parent_id', $f_aux->parent_id];
                }
                // Order
                if ($order >= 0) {
                    $meta['order'] = $order;
                    $where[] = ['order', $order];
                }


                // Update/Create
                if (UserMeta::where($where)->exists()) {
                    $user_meta = UserMeta::where($where);
                    $user_meta->update($meta);
                } else {
                    UserMeta::create($meta);
                }

                // Save SubFields
                if (!empty($field['fields'])) {
                    $this->save_custom_fields($user, $field['fields'], true);
                }
            }
        }
    }

    protected function fillUser(User $user, Request $request) {
        // Prevent losing session for different reference on current user
        if ($user->id == auth()->user()->id) {
            $user = auth()->user();
        }

        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->role = (isset($request->role)) ? $request->role : $user->rol->name;

        if ( !empty($request->password)) {
            if ($request->password != $request->password_confirmation) {
                return redirect()->back()->with('error', trans('validation.confirmed', ['attribute' => 'password']));
            }

            $user->password = bcrypt($request->password);
        }
        
        $user->update();

        if (isset($request->custom_fields)) {
            $this->save_custom_fields($user, $request->custom_fields);
        }

        return $user;
    }
}
