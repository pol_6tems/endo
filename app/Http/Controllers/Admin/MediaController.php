<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Media;
use Auth;
use Storage;
use Image;

class MediaController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Media');
        $this->section_icon = 'perm_media';
        $this->section_route = 'admin.media';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $level = Auth::user()->rol->level?:0;
        $items = Media::where('level', '<=', $level)->get();
        $view_params = array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'title',
            'headers' => [
                __('Id') => [ 'width' => '15%' ],
                __('Title') => [],
                __('File') => [],
                __('Thumbnail') => ['width' => '15%'],
                __('Updated') => [ 'width' => '15%' ],
            ],
            'rows' => [
                ['value' => 'id'],
                ['value' => 'title'],
                ['value' => 'file_name'],
                ['value' => 'thumbnail', 'type' => 'thumbnail' ],
                ['value' => 'updated_at', 'type' => 'date', 'format' => 'd/m/Y h:i' ],
            ],
        );
        if ( Auth::user()->isAdmin() ) {
            $view_params['headers'][ __('Level') ] = [];
            $view_params['rows'][] = ['value' => 'level', 'type' => 'number' ];
        }
        return View('Admin::default.index', $view_params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view_params = array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'title', 'title' => __('Title'), 'required' => true],
                ['value' => 'legend', 'title' => __('Legend')],
                ['value' => 'alt', 'title' => __('Alternative text')],
                ['value' => 'description', 'title' => __('Description'), 'type' => 'textarea'],
                ['value' => 'file', 'title' => __('Thumbnail'), 'type' => 'thumbnail'],
            ],
        );
        if ( Auth::user()->isAdmin() ) {
            $view_params['rows'][] = ['value' => 'level', 'type' => 'number', 'title' => __('Level') ];
        }
        return View('Admin::default.create', $view_params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();

        $file = $request->file('file');
        $return = $this->upload_file($data, $file);

        $error = $return['error'];
        $new_item = $return['new_item'];

        if ( empty($error) ) {
            return redirect()->route('admin.media.edit', $new_item->id)->with(['message' => __('Media created succesfully.')]);
        } else {
            return redirect()->back()->with( $error );
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $level = Auth::user()->rol->level?:0;
        $item = Media::findOrFail($id);
        
        if ( $item->level > $level ) return abort(403);

        $view_params = array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'title', 'title' => __('Title'), 'required' => true],
                ['value' => 'legend', 'title' => __('Legend')],
                ['value' => 'alt', 'title' => __('Alternative text')],
                ['value' => 'description', 'title' => __('Description'), 'type' => 'textarea'],
                ['value' => 'file', 'title' => __('Thumbnail'), 'type' => 'thumbnail'],
            ],
        );
        
        if ( Auth::user()->isAdmin() ) {
            $view_params['rows'][] = ['value' => 'level', 'type' => 'number', 'title' => __('Level') ];
        }
        
        return View('Admin::default.edit', $view_params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $locale, $id)
    {
        $item = Media::findOrFail($id);
        $data = $request->all();
        $file = $request->file('file');
        $now = new \Carbon\Carbon();

        if ( !empty($file) ) {

            if ( !$file->getSize() ) {
                return redirect()->back()->with(['error' => __('Image too big.')]);
            }

            $item->remove_thumbnails();

            $data['file_path'] = $now->format('Y/m');
            $data['file_size'] = $file->getSize();
            $data['user_id'] = Auth::user()->id;
            $sizes = getImageSize($file);
            if ($sizes) {
                $data['width'] = $sizes[0];
                $data['height'] = $sizes[1];
            }
            $file_name = str_replace(
                '.' . $file->getClientOriginalExtension(), '',
                $file->getClientOriginalName()
            );
            $file_type = $file->getClientOriginalExtension();
            $file_path = 'public/media/' . $data['file_path'] . '/';
            $file_no_public_path = 'app/public/media/' . $data['file_path'] . '/';
            
            $unic = false;
            $i = 0;
            $file_path_aux = $file_path . $file_name . '.' . $file_type;
            while( !$unic ) {
                if ( Storage::exists($file_path_aux) ) {
                    $i++;
                    $file_path_aux = $file_path . $file_name . '-' . $i . '.' . $file_type;
                } else {
                    $unic = true;
                }
            }

            $file_path_store = $file_path_aux;
            $file_name = $file_name . ($i > 0 ? '-' . $i : '');
            if ( $stored_file = $file->storeAs($file_path, $file_name . '.' . $file_type) ) {
                $data['file_name'] = $file_name;
                $data['file_type'] = $file_type;

                $thumbnail_sizes = Media::get_thumbnail_sizes();
                if ( isset($data['height']) && isset($data['width']) ) {
                    foreach ( $thumbnail_sizes as $size_name => $size_width ) {
                        if ( $size_name == 'full' ) continue;
                        $new_path = storage_path($file_no_public_path . $file_name . '-' . $size_name . '.' . $file_type);
                        $size_height = ($data['height'] * $size_width) / $data['width'];
                        Image::make($file->getRealPath())->resize($size_width, $size_height)->save($new_path);
                    }
                }
                $item->update($data);
                return redirect()->route('admin.media.edit', $id)->with(['message' => __('Media updated successfully.')]);
            } else {
                return redirect()->back()->with(['error' => __('Error saving data')]);
            }
        } else {
            $item->title = $data['title'];
            $item->alt = $data['alt'];
            $item->legend = $data['legend'];
            $item->description = $data['description'];
            if ( Auth::user()->isAdmin() ) $item->level = $data['level'];
            $item->update();
            return redirect()->route('admin.media.edit', $id)->with(['message' => __('Media updated successfully.')]);
        }

        return redirect()->back()->with(['error' => __('Error saving data')]);
    }


    public function crop(Request $request) {
        $cropData = $request->get('cropData');
        $mediaId = $request->get('id');
        $error = [];

        if ($mediaId) {
            $media = Media::find($mediaId);

            if ($media->is_image()) {
                $img = Image::make($media->getFilePath());

                $img->crop($cropData['width'], $cropData['height'], $cropData['x'], $cropData['y']);
                $result = $this->upload_file([], $img);
            }
        }
        
        if ($error) {
            return json_encode($error);
        } else {
            return json_encode(["success" => 'Correcte', 'image' => $result]);
        }
    }


    public function upload(Request $request) {
        ini_set('memory_limit', '256M');
        
        $data = $request->all();

        if ( !empty($data['id']) ) {
            $media = Media::find($data['id']);
            $thumbnail_sizes = Media::get_thumbnail_sizes();
            
            foreach($thumbnail_sizes as $size => $width) {
                // Storage::delete('public/media/' . $media->file_path . '/' . $media->file_name . '-'. $size . '.' . $media->file_type);
            }
            
            // $media->delete();
        }

        $files = $request->file('file');

        foreach($files as $file) {
            $result = $this->upload_file($data, $file);
            $error = $result['error'];
        }
        
        if ($error) return json_encode($error);
        else return json_encode(["success" => 'Correcte', 'image' => $result]);
    }
    
    public function upload_file($data, $file) {
        $now = new \Carbon\Carbon();
        $error = null;
        $new_item = null;

        if ( !isset($data['level']) || (isset($data['level']) && empty($data['level'])) ) {
            $data['level'] = Auth::user()->rol->level;
        }
        
        if ( !$file->getSize() ) {
            $error = ['error' => __('Image too big.')];
        }

        $data['file_path'] = $now->format('Y/m');

        $data['file_size'] = $file->getSize();
        $data['user_id'] = Auth::user()->id;

        if ($file instanceof \Intervention\Image\Image) {
            $file_type = $file->extension;
            $originalFileName = $file->filename;
        } else {
            $file_type = $file->getClientOriginalExtension();
            $originalFileName = $file->getClientOriginalName();
        }
        $data['width'] = 0;
        $data['height'] = 0;

        if ((new Media)->is_image($file_type)) {
            if ($file instanceof \Intervention\Image\Image) {
                $data['file_size'] = $file->filesize();
                $data['width'] = $file->width();
                $data['height'] = $file->height();
            } else {
                $sizes = getImageSize($file);
                if ($sizes) {
                    $data['width'] = $sizes[0];
                    $data['height'] = $sizes[1];
                }
            }
        }

        // Generar Fitxer
        $file_name = str_replace('.' . $file_type, '', $originalFileName);
        $file_path = 'public/media/' . $data['file_path'] . '/';
        $file_no_public_path = 'app/public/media/' . $data['file_path'] . '/';
        
        $i = 0;
        $file_path_aux = $file_path . $file_name . '.' . $file_type;
        while( Storage::exists($file_path_aux) ) {
            $i++;
            $file_path_aux = $file_path . $file_name . '-' . $i . '.' . $file_type;
        }

        $file_name = $file_name . ($i > 0 ? '-' . $i : '');

        if ($file instanceof \Intervention\Image\Image) {
            $filePathImg = str_replace( 'public/', public_path() .'/storage/', $file_path);
            $stored_file = $file->save($filePathImg . $file_name . '.' . $file_type);
        } else {
            $stored_file = $file->storeAs($file_path, $file_name . '.' . $file_type);
        }

        if ($stored_file) {
            if ( empty($data['title']) ) $data['title'] = $file_name;
            $data['file_name'] = $file_name;
            $data['file_type'] = $file_type;

            if ( (new Media)->is_image($file_type) ) {
                $thumbnail_sizes = Media::get_thumbnail_sizes();
                if ( (isset($data['height']) && $data['height'] != 0) && (isset($data['width']) && $data['width'] != 0) ) {
                    foreach ( $thumbnail_sizes as $size_name => $size_width ) {
                        if ( $size_name == 'full' ) continue;
                        $new_path = storage_path($file_no_public_path . $file_name . '-' . $size_name . '.' . $file_type);
                        $size_height = ($data['height'] * $size_width) / $data['width'];
                        if ($file instanceof \Intervention\Image\Image) {
                            $realPath = $file->basePath();
                        } else {
                            $realPath = $file->getRealPath();
                        }

                        Image::make($realPath)->resize($size_width, $size_height)->save($new_path);
                    }
                }
            }

            $new_item = Media::create($data);
            if ( empty($new_item) || !$new_item ) $error = ['error' => __('Error while uploading new Media')];
        }


        return [ 'error' => $error, 'new_item' => $new_item ];
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $level = Auth::user()->rol->level?:0;
        $item = Media::findOrFail($id);
        
        if ( $item->level > $level ) return response()->json(['error'=>'Done!'], 401);

        $item->remove_thumbnails();
        if ( $item->delete() ) {
            return response()->json(['success'=>'Done!']);
        }
        return response()->json(['error'=>'Done!'], 401);
    }

    public function massDestroy(Request $request)
    {
        $level = Auth::user()->rol->level?:0;
        $ids = explode(',', $request->input('ids'));
        foreach ($ids as $id) {
            $item = Media::findOrFail($id);
            if ( $item->level <= $level ) $item->delete();
        }
        return redirect()->route('admin.media.index')->with(['message' => __('Media deleted succesfully.')]);
    }

    public static function formatBytes($size, $precision = 2)
    {
        if ($size > 0) {
            $size = (int) $size;
            $base = log($size) / log(1024);
            $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');

            return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
        } else {
            return $size;
        }
    }

    public function get_all(Request $request) {
        $level = Auth::user()->rol->level?:0;

        $search = Input::get('search', '');
        $sortby = Input::get('sortby', 'updated_at');
        $order = Input::get('order', 'DESC');

        if ( empty($sortby) ) $sortby = 'updated_at';
        if ( empty($order) ) $order = 'DESC';

        $filter = $request->input('filter');

        $query = Media::where('level', '<=', $level)->orderby($sortby, $order);

        if (isset($filter['crop_height']) && isset($filter['crop_width'])) {
            $query = $query->where('height', $filter['crop_height'])->where('width', $filter['crop_width']);
        }

        if ( empty($search) ) {
            $items = $query->paginate(150);
        } else {
            $items = $query->where(function($q) use ($search) {
                $q->orWhere('title', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('legend', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('description', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('file_name', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('id', 'LIKE', '%'.strtoupper($search).'%');
            })->paginate(50);
        }
        
        $links_appends = array('sortby' => $sortby, 'order' => $order);
        if ( !empty($search) ) $links_appends['search'] = $search;
        
        $links = $items->appends($links_appends)->links();

        foreach ( $items as $key => $item ) {
            $items[$key]['thumbnails'] = $item->get_all_thumbnails();
        }

        return response()->json([
            'items' => $items,
            'links' => $links,
            'search' => $search,
            'sortby' => $sortby,
            'order' => $order,
        ]);
    }

    function updateImage($locale, $id) {
        $data = request()->all();
        $image = Media::where('id', $id)->first();
        $image->title = $data['title'];
        $image->legend = $data['legend'];
        $image->alt = $data['alt'];
        if ($image->save()) {
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }

    function getImages() {
        $files = array_filter(rglob(storage_path('app/public/*-medium.jpg')), 'is_file');

        $response = [];


        foreach ($files as $file) {
            $response[] = str_replace(storage_path('app/public/'), '/storage/',  asset($file));
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
}