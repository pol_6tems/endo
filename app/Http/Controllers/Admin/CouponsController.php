<?php


namespace App\Http\Controllers\Admin;


use App\Http\Requests\CouponRequest;
use App\Modules\EncuentraTuRetiro\Models\Coupon;
use App\Post;
use App\Services\CouponsService;
use Carbon\Carbon;

class CouponsController extends AdminController
{

    const POST_TYPE = 'coupon';
    const POST_TYPE_PLURAL = 'coupons';
    const SECTION_ROUTE = 'admin.coupons';
    const SECTION_TITLE = 'Cupones';

    private $couponClassName;


    public function index()
    {
        $coupons = call_user_func([$this->couponClassName, 'all']);

        return view('Admin::coupons.index', [
            'coupons' => $coupons
        ]);
    }


    public function create()
    {
        $statuses = call_user_func($this->couponClassName . '::getStatuses');
        return view('Admin::coupons.edit', compact('statuses'));
    }


    public function store(CouponRequest $request)
    {
        $coupon = call_user_func([$this->couponClassName, 'firstOrNew'], ['code' => $request->input('code')]);

        if ($coupon->exists) {
            abort(400, 'Item exists');
        }

        $coupon->fill([
            'code' => $request->input('code'),
            'status' => $request->input('status'),
            'value' => $request->input('value'),
            'total_uses' => $request->input('total_uses', null),
            'uses_per_user' => $request->input('uses_per_user', null),
            'min_purchase_amount' => $request->input('min_purchase_amount', null),
            'min_date' => $request->input('min_date') ? Carbon::parse($request->input('min_date')) : null,
            'max_date' => $request->input('max_date') ? Carbon::parse($request->input('max_date')) : null
        ]);

        $coupon->save();

        return redirect()->route(self::SECTION_ROUTE . '.index')->with(['message' => __(':name added successfully', ['name' => 'Cupón'])]);
    }


    public function edit($locale, $id)
    {
        $coupon = call_user_func($this->couponClassName . '::find', $id);

        if (!$coupon) {
            abort(400, 'Item not found');
        }

        $statuses = call_user_func([$this->couponClassName, 'getStatuses']);
        return view('Admin::coupons.edit', compact('statuses', 'coupon'));
    }


    public function update($locale, $id, CouponRequest $request)
    {
        $coupon = call_user_func($this->couponClassName . '::find', $id);

        if (!$coupon) {
            abort(400, 'Item not found');
        }

        $coupon->update([
            'code' => $request->input('code'),
            'status' => $request->input('status'),
            'value' => $request->input('value'),
            'total_uses' => $request->input('total_uses', null),
            'uses_per_user' => $request->input('uses_per_user', null),
            'min_purchase_amount' => $request->input('min_purchase_amount', null),
            'min_date' => $request->input('min_date') ? Carbon::parse($request->input('min_date')) : null,
            'max_date' => $request->input('max_date') ? Carbon::parse($request->input('max_date')) : null
        ]);

        return redirect()->route(self::SECTION_ROUTE . '.index')->with(['message' => __(':post_type updated successfully', ['post_type' => 'Cupón'])]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        $coupon = call_user_func($this->couponClassName . '::find', $id);
        $coupon->delete();

        return redirect()->route(self::SECTION_ROUTE . '.index')->with(['message' => __(':post_type deleted successfully', ['post_type' => 'Cupón'])]);
    }


    public function callAction($method, $parameters)
    {
        $this->couponClassName = app(CouponsService::class)->getCouponModel();

        view()->share([
            'section_title' => self::SECTION_TITLE,
            'post_type_plural' => self::POST_TYPE_PLURAL,
            'section_route' => self::SECTION_ROUTE
        ]);

        return parent::callAction($method, $parameters);
    }
}