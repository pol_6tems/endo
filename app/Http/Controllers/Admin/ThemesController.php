<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Theme;
use DB;
use File;

class ThemesController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Themes');
        $this->section_icon = 'color_lens';
        $this->section_route = 'admin.themes';
    }

    public function index()
    {
        //$items = Theme::where('admin', false)->orderby('name')->get();
        $items = Theme::orderby('name')->get();
        return View('Admin::default.index', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'items' => $items,
            'route' => $this->section_route,
            'field_name' => 'name',
            'toolbar_header' => [
                '<a href="' . route('admin.themes.refresh') . '" class="btn btn-default btn-sm">' . __('Refresh') . '</a>',
            ],
            'headers' => [
                __('Name') => [],
                __('Path') => [ 'width' => '20%' ],
                __('Thumbnail') => [ 'width' => '15%' ],
                __('Active') => [ 'width' => '10%' ],
                __('Admin') => [ 'width' => '10%' ],
            ],
            'rows' => [
                ['value' => 'name'],
                ['value' => 'path'],
                ['value' => 'thumbnail', 'type' => 'img'],
                ['value' => 'active', 'type' => 'switch_active', 'route' => 'admin.themes.active', 'onlyone' => false],
                ['value' => 'admin', 'type' => 'switch_active', 'route' => 'admin.themes.active_admin', 'onlyone' => false],
            ],
        ));
    }

    public function create()
    {
        return View('Admin::default.create', array(
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'path', 'title' => __('Path'), 'required' => true],
                ['value' => 'file', 'title' => __('Thumbnail'), 'type' => 'thumbnail'],
            ],
        ));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if ( $file = $request->file('thumbnail') ) {
            $data['thumbnail'] = file_get_contents($file->getRealPath());
        }
        Theme::create($data);
        return redirect()->route('admin.themes.index')->with([ 'message' => __(':name added successfully', ['name' => __('Theme')]) ]);
    }

    public function show($locale, $id)
    {
        //
    }

    public function edit($locale, $id)
    {
        $item = Theme::findOrFail($id);

        $languages = DB::table('languages')->where('active', true)->get();
        $columns = [];
        $columnsCount = $languages->count();
        if ( $languages->count() > 0 ) {
	        foreach ($languages as $key => $language) {
	            if ( $key == 0 ) $columns[$key] = $this->openJSONFile($language->code, $item->name);
	            $columns[++$key] = [
                    'data'=>$this->openJSONFile($language->code, $item->name), 
                    'lang' => $language->code
                ];
	        }
        }

        return View('Admin::default.edit', array(
            'item' => $item,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'rows' => [
                ['value' => 'name', 'title' => __('Name'), 'required' => true],
                ['value' => 'path', 'title' => __('Path'), 'required' => true],
                ['value' => 'thumbnail', 'title' => __('Thumbnail'), 'type' => 'thumbnail'],
                ['type' => 'subform_card', 'value' => 'Admin::default.subforms.themes_translations', 
                    'languages' => $languages, 'columns' => $columns, 'columnsCount' => $columnsCount, 
                ],
            ],
        ));
    }

    public function update(Request $request, $locale, $id)
    {
        $item = Theme::findOrFail($id);
        $data = $request->all();
        if ( $file = $request->file('thumbnail') ) {
            $data['thumbnail'] = file_get_contents($file->getRealPath());
        }
        $item->update($data);
        return redirect()->route('admin.themes.edit', $id)->with([ 'message' => __(':name updated successfully', ['name' => __('Theme')]) ]);
    }

    public function destroy($locale, $id)
    {
        $item = Theme::findOrFail($id);
        if ( $item->delete() ) {
            return response()->json(['success'=>'Done!']);
        }
        return response()->json(['error'=>'Done!'], 401);
    }

    public function active(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $item = Theme::findOrFail($id);
        DB::table('themes')->where('admin', false)->where('id', '!=', $id)->update(['active' => false]);
        $item->active = !$item->active;
        $item->update();
        return response()->json(['success'=>'Done!', 'active' => $item->active]);
    }

    public function active_admin(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $item = Theme::findOrFail($id);
        DB::table('themes')->where('admin', true)->where('id', '!=', $id)->update(['admin' => false]);
        $item->admin = !$item->admin;
        $item->update();
        return response()->json(['success'=>'Done!', 'active' => $item->active]);
    }

    public function refresh() {
        foreach (glob(base_path('resources/themes/*'), GLOB_ONLYDIR) as $dirname) {
            if ( !Theme::where('name', basename($dirname))->exists() ) {
                $thumbnail = null;
                if ( file_exists( $dirname . '/screenshot.png' ) ) $thumbnail = file_get_contents( $dirname . '/screenshot.png' );
                if ( file_exists( $dirname . '/screenshot.jpg' ) ) $thumbnail = file_get_contents( $dirname . '/screenshot.jpg' );
                DB::table('themes')->insert([
                    'name' => basename($dirname),
                    'path' => basename($dirname),
                    'thumbnail' => $thumbnail,
                ]);
            } else {
                $theme = Theme::where('name', basename($dirname))->first();
                if ( file_exists( $dirname . '/screenshot.png' ) ) {
                    $theme->thumbnail = file_get_contents( $dirname . '/screenshot.png' );
                    $theme->update();
                }
                if ( file_exists( $dirname . '/screenshot.jpg' ) ) {
                    $theme->thumbnail = file_get_contents( $dirname . '/screenshot.jpg' );
                    $theme->update();
                }
            }
        }
        return redirect()->route('admin.themes.index')->with([ 'message' => __(':name updated successfully', ['name' => __('Themes')]) ]);
    }

    /* Translations */
    public function transStore (Request $request, $locale, $theme_id) {
        $theme = Theme::findOrFail($theme_id);
        $this->validate($request, [
		    'key' => 'required',
		    'value' => 'required',
        ]);

		$data = $this->openJSONFile('en', $theme->name);
        $data[$request->key] = $request->value;
        //$data[$request->key] = addcslashes($request->value, '"\\/');
        
        array_walk_recursive($data, function(&$item, $key) {
            $item = addcslashes($item, '"\\/');
		});

        $this->saveJSONFile('en', $data, $theme->name);

        return redirect()->route('admin.modules.edit', $theme_id);
    }

    public function transUpdate(Request $request, $locale, $theme_id) {
        $theme = Theme::findOrFail($theme_id);
        $data = $this->openJSONFile($request->code, $theme->name);
        $data[$request->pk] = $request->value;

        array_walk_recursive($data, function(&$item, $key) {
            $item = addcslashes($item, '"\\/');
		});

        $this->saveJSONFile($request->code, $data, $theme->name);
        return response()->json(['success'=>'Done!']);
    }

    public function transUpdateKey(Request $request, $locale, $theme_id){
        $theme = Theme::findOrFail($theme_id);
        $languages = DB::table('languages')->get();
        if ( $languages->count() > 0 ) {
            foreach ( $languages as $language ) {
                $data = $this->openJSONFile($language->code, $theme->name);
                if ( isset($data[$request->pk]) ) {
                    $data[$request->value] = $data[$request->pk];
                    unset($data[$request->pk]);
                    $this->saveJSONFile($language->code, $data, $theme->name);
                }
            }
        }
        return response()->json(['success'=>'Done!']);
    }

    public function transDestroy($locale, $theme_id, $key) {
        $theme = Theme::findOrFail($theme_id);
        $key = base64_decode($key);
        $languages = DB::table('languages')->get();
        if ( $languages->count() > 0 ) {
            foreach ($languages as $language){
                $data = $this->openJSONFile($language->code, $theme->name);
                unset($data[$key]);
                $this->saveJSONFile($language->code, $data, $theme->name);
            }
        }
        return response()->json(['success' => $key]);
    }

    private function openJSONFile($code, $theme_name) {
        $jsonString = [];
        $file_path = app_path("Themes/$theme_name/lang/$code.json");
        if ( File::exists($file_path) ) {
            $jsonString = file_get_contents( $file_path );
            $jsonString = json_decode($jsonString, true);
        }
        return $jsonString;
    }

    private function saveJSONFile($code, $data, $theme_name) {
        $file_path = app_path("Modules/$theme_name/lang/$code.json");
        ksort($data);
        $jsonData = json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
        file_put_contents($file_path, stripslashes($jsonData));
    }
    /* end Translations */

}
