<?php

namespace App\Http\Controllers\Admin;

use App\Models\Chat;
use Illuminate\Http\Request;
use App\Notifications\EmailNotification;
use App\User;
use App\Models\Mensaje;
use Auth;
use DB;

class MensajesController extends AdminController
{
    protected $section_title;
    protected $section_icon;
    protected $section_route;

    public function __construct() {
        parent::__construct();
        $this->section_title = __('Messages');
        $this->section_icon = 'message';
        $this->section_route = 'admin.mensajes';
    }

    public function index() {

        $type = !empty(request()->type) ? request()->type : 'chat';

        $user_avatar_default = asset('storage/avatars/user-endo.jpg');
        $my_user_avatar = $user_avatar_default;
        $avatar_pub = public_path('storage/avatars/' . Auth::user()->avatar);
        if ( file_exists($avatar_pub) ) $my_user_avatar = asset('storage/avatars/' . Auth::user()->avatar);

        if ( Auth::user()->rol->level >= Mensaje::ROL_LEVEL_LIMIT ) {
            $chats = Mensaje::where('type', $type)
            ->selectRaw('mensajes.chat, MAX(mensajes.created_at)')
            ->groupBy(['mensajes.chat'])
            ->orderByRaw('MAX(mensajes.created_at) desc')
            ->pluck('mensajes.chat');
        } else {
            $chats = Mensaje::where('type', $type)->where(function ($query) {
                $query->where('user_id', Auth::user()->id)->orWhere('to', Auth::user()->id);
            })
            ->selectRaw('mensajes.chat, MAX(mensajes.created_at)')
            ->groupBy(['mensajes.chat'])
            ->orderByRaw('MAX(mensajes.created_at) desc')
            ->pluck('mensajes.chat');
        }
        
        $items = array();
        foreach ( $chats as $chat ) {
            if ( Auth::user()->rol->level >= Mensaje::ROL_LEVEL_LIMIT ) {
                $mensajes = Mensaje::where('chat', $chat)->where('type', $type)->orderBy('created_at')->withAll()->get();
            } else {
                $mensajes = Mensaje::where('chat', $chat)->where('type', $type)
                ->where(function ($query) {
                    $query->where('user_id', Auth::user()->id)->orWhere('to', Auth::user()->id);
                })
                ->orderBy('created_at')->withAll()->get();
            }
            $last_mensaje = $mensajes->last();
            $last_mensaje_chat = !empty($last_mensaje) ? $last_mensaje->get_chat : null;

            // All Params
            $all_params = array();
            if ( Auth::user()->rol->level >= Mensaje::ROL_LEVEL_LIMIT ) {
                $params = Mensaje::where('chat', $chat)->where('type', $type)
                ->whereNotNull('params')->orderBy('created_at', 'DESC')->pluck('params');
            } else {
                $params = Mensaje::where('chat', $chat)->where('type', $type)
                ->where(function ($query) {
                    $query->where('user_id', Auth::user()->id)->orWhere('to', Auth::user()->id);
                })
                ->whereNotNull('params')->orderBy('created_at', 'DESC')->pluck('params');
            }
            foreach ( $params as $param ) $all_params = array_merge(json_decode($param, TRUE), $all_params);

            $chat_users = $last_mensaje->get_users();
            $user_id = 0;
            $user_name = [];
            $user_email = [];
            $user_avatar = $user_avatar_default;
            
            foreach ( $chat_users as $chat_user ) {
                if ( is_int($chat_user) ) {
                    $user = User::find($chat_user);

                    if ( empty($user) ) continue;

                    $user_email[] = $user->email;
                    $user_name[] = $user->name;
                    if ( count($chat_users) == 1 )  {
                        $user_name = [$user->fullname()];
                        if ( !empty($user->avatar) ) {
                            $avatar = asset('storage/avatars/' . $user->avatar);
                            $avatar_pub = public_path('storage/avatars/' . $user->avatar);
                            if ( file_exists($avatar_pub) ) $user_avatar = $avatar;
                        }
                    }
                } else {
                    $parts = explode('##', $chat_user);
                    $user_id = $parts[1];
                    $user_name[] = $parts[0];
                    $user_email[] = $parts[1];
                    $user_avatar = $user_avatar_default;
                }
            }

            /* Botones chats */
            $buttons = array();
            if ( 
                !empty($last_mensaje) && !empty($last_mensaje_chat) && 
                !empty($last_mensaje_chat->get_buttons()) 
            ) {
                
                $chat_btns = $last_mensaje_chat->get_buttons();
                foreach ( $chat_btns as $k_btn => $chat_btn ) {
                    if ( empty($chat_btn) ) continue;

                    $chat_btn_parts = explode('##', $chat_btn);

                    if (empty($all_params[ $chat_btn_parts[0] ])) {
                        continue;
                    }
                    
                    $btn_action_url = $all_params[ $chat_btn_parts[0] ];
                    
                    $btn_action_txt = '';
                    if ( !empty($chat_btn_parts[1]) ) {
                        $btn_action_txt = $chat_btn_parts[1];
                    }

                    if ( empty($btn_action_txt) ) $btn_action_txt = _('Button') . " $k_btn";

                    $btn_action_txt_btn = '';
                    if ( !empty($chat_btn_parts[2]) ) {
                        $btn_action_txt_btn = $chat_btn_parts[2];
                    }

                    if ( empty($btn_action_txt_btn) ) $btn_action_txt_btn = $btn_action_txt;

                    $btn_action_js_action = '';
                    if ( !empty($chat_btn_parts[3]) && isset($all_params[$chat_btn_parts[3]]) ) {
                        $btn_action_js_action = $all_params[$chat_btn_parts[3]];
                    }

                    if ( !empty($btn_action_url) ) {
                        $buttons[] = [
                            'text' => $btn_action_txt,
                            'url' => $btn_action_url,
                            'text-btn' => $btn_action_txt_btn,
                            'action' => $btn_action_js_action
                        ];
                    }
                    
                }
            }
            /* end Botones chats */

            $item = new \stdClass();
            $item->chat = str_replace('.', '-', str_replace('@', '-', $chat));
            $item->chat_noencode = $chat;
            $item->mensajes = $mensajes;
            $item->user_name = !empty($user_name) ? implode(', ', $user_name) : Auth::user()->fullname();
            $item->user_email = count($chat_users) == 1 ? reset($user_email) : '';
            $item->user_id = $user_id;
            $item->user_avatar = !empty($user_name) ? $user_avatar : $my_user_avatar;
            $item->last_mensaje = $last_mensaje;
            $item->buttons = $buttons;
            $items[] = $item;
        }
        
        return view('Admin::default.messaging', array(
            'items' => $items,
            'section_title' => $this->section_title,
            'section_icon' => $this->section_icon,
            'route' => $this->section_route,
            'user_avatar_default' => $user_avatar_default,
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $mensaje = new Mensaje();
        $mensaje->chat = $data['chat_id'];
        $mensaje->user_id = Auth::user()->id;
        $mensaje->mensaje = $data['mensaje'];
        $mensaje->type = $data['type'];
        $mensaje->fitxer = '';
        $mensaje->name = '';
        $mensaje->email = '';

        /* TO */
        if ( !empty($data['to']) ) $mensaje->to = $data['to'];
        else {
            $last_mensaje = Mensaje::where('chat', $mensaje->chat)->where('type', $mensaje->type)
            ->orderBy('created_at', 'ASC')->get()->last();
            $mensaje->to = 0;
            if ( !empty($last_mensaje->user_id) && $last_mensaje->user_id != Auth::user()->id )
                $mensaje->to = $last_mensaje->user_id;
            else if ( !empty($last_mensaje->to) && $last_mensaje->to != Auth::user()->id )
                $mensaje->to = $last_mensaje->to;
        }
        /* end TO */

        /* Guardar ultims params */
        $all_params = array();
        if ( Auth::user()->rol->level >= Mensaje::ROL_LEVEL_LIMIT ) {
            $params = Mensaje::where('chat', $mensaje->chat)->where('type', $mensaje->type)
            ->whereNotNull('params')->orderBy('created_at', 'DESC')->pluck('params');
        } else {
            $params = Mensaje::where('chat', $mensaje->chat)->where('type', $mensaje->type)
            ->where(function ($query) {
                $query->where('user_id', Auth::user()->id)->orWhere('to', Auth::user()->id);
            })
            ->whereNotNull('params')->orderBy('created_at', 'DESC')->pluck('params');
        }

        foreach ( $params as $param ) $all_params = array_merge(json_decode($param, TRUE), $all_params);
        if ( !empty($all_params) ) {
            $mensaje->params = json_encode($all_params);
        }
        /* end Guardar ultims params */

        
        if ( $mensaje->save() ) {

            if ( isProEnv() ) {
                $chat_users = $mensaje->get_users();

                foreach ( $chat_users as $chat_user ) {
                    if ( is_int($chat_user) ) $usuario = User::find( $chat_user );
                    else {
                        $parts = explode('##', $chat_user);
                        $usuario = new User();
                        $usuario->id = 0;
                        $usuario->name = $parts[0];
                        $usuario->lastname = '';
                        $usuario->email = $parts[1];
                    }

                    $params = new \stdClass();
                    $params->usuario = $usuario;
                    $params->mensaje = $mensaje;
                    $params->from = Auth::user();
                    
                    $chat = \App\Models\Chat::where('type', $mensaje->type)->first();
                    $params->email_id = !empty($chat) ? $chat->email_id : 0;

                    $notificacion = new EmailNotification($params);
                    if ( $usuario && $notificacion !== false ) $usuario->notify( $notificacion );
                }
            }
            
            echo 'OK';
        }
        else echo 'KO';

        die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function visto(Request $request)
    {
        $chat_id = $request->chat_id;
        DB::table('mensajes')->where('chat', $chat_id)->update(['visto' => true]);
        return response()->json(['success' => true]);
    }

    public static function notification($parameters, $name = null) {
        $userRol = auth()->user()->role;

        list($k, $v) = explode('=', $parameters);
        $params[ $k ] = $v;

        $chat = Chat::where('type', $params['type'])->first();

        $permissions = $chat->permissions;
        if (empty($permissions) || !isset($permissions[$userRol]) || !$permissions[$userRol]) {
            return;
        }

        $mensajes = Mensaje::where('type', $params['type'])
        ->where('visto', false)->count();
        
        if ( $mensajes > 0 ) {
            $msg = __('Tienes :num mensajes nuevos', ['num' => $mensajes]);

            if ($name) {
                $msg = __('Tienes :num mensajes nuevos de tipo :type', ['num' => $mensajes, 'type' => $name]);
            }

            return [
                'href' => route('admin.mensajes.index', ['locale' => \App::getLocale()]) . '?type=' . $params['type'],
                'msg' => $msg,
            ];
        }
        else return;
    }


    public function callAction($method, $parameters)
    {
        $type = !empty(request()->type) ? request()->type : 'chat';
        $userRol = auth()->user()->role;

        if ($type != 'chat' && $userRol != 'admin') {
            $chat = Chat::where('type', $type)->first();

            if (!$chat) {
                abort(404);
            }

            $permissions = $chat->permissions;
            if (empty($permissions) || !isset($permissions[$userRol]) || !$permissions[$userRol]) {
                abort(403);
            }
        }

        return parent::callAction($method, $parameters);
    }
}
