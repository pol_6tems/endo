<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Models\Mensaje;
use App\Opcion;
use App\Notifications\NuevoMensajeNotification;
use App\Notifications\EmailNotification;
use App\User;

class MensajesController extends Controller {

    public function index() {
        $user = Auth::user();
        $mensajes = Mensaje::where('user_id', $user->id)->get()->sortby('created_at');
        return view('contactar.index', compact('mensajes'));
    }

    public function create() { }

    public function store(Request $request) {
        $user = Auth::user();
        $data = $request->all();

        $mensaje = new Mensaje();

        $mensaje->user_id = $data['user_id'];
        $mensaje->mensaje = $data['mensaje'];

        $moderadors_calendari_contacte = null;
        if ( !empty(Opcion::where('key', 'moderadors_calendari_contacte')->first()) ) {
            $moderadors_calendari_contacte = Opcion::where('key', 'moderadors_calendari_contacte')->first()->value;
            if ( !empty($moderadors_calendari_contacte) ) {
                $moderadors = explode('#', $moderadors_calendari_contacte);
                foreach ($moderadors as $mod_email) {
                    $mod_user = User::where('email', $mod_email)->first();
                    if ( !empty($mod_user) ) {
                        $notificacion = new NuevoMensajeNotification($mod_user, $mensaje);
                        $mod_user->notify( $notificacion );
                    }
                }
            }
        }

        if (isset($request->fitxer) && !empty($request->fitxer) && gettype($request->fitxer) == 'object' ) {
            $filename = $user->id.'_f_'.time().'.'.$request->fitxer->getClientOriginalExtension();
            $path = $request->fitxer->storeAs('public/fitxers', $filename);
            $mensaje->fitxer = str_replace('public/fitxers', 'fitxers', $path);
        } else $mensaje->fitxer = '';

        if ( $id = $mensaje->save() ) {
            echo 'OK';
        } else echo 'KO';

        die();
    }

    public function show($id) { }

    public function edit($id) {}

    public function update(Request $request, $id) {}

    public function destroy($id) {}

    public static function enviar_mensaje($params) {

        $data = array();
        /*if ( is_array($params) ) {
            foreach ( $params as $p ) $data[ $p['name'] ] = $p['value'];
        } else {
            parse_str($params, $data);
        }*/
        if ( !is_array($params) ) parse_str($params, $data);
        else $data = $params;

        $mensaje = new Mensaje();
        $mensaje->user_id = !empty($data['user_id']) ? $data['user_id'] : 0;
        $mensaje->mensaje = !empty($data['mensaje']) ? $data['mensaje'] : '';
        $mensaje->fitxer = '';
        $mensaje->email = !empty($data['email']) ? $data['email'] : '';
        $mensaje->name = !empty($data['name']) ? $data['name'] : '';
        $mensaje->type = !empty($data['type']) ? $data['type'] : 'chat';
        $mensaje->params = !empty($data['params']) ? json_encode($data['params']) : NULL;
        
        /* TO */
        if ( !empty($data['to']) ) $mensaje->to = $data['to'];
        else {
            $last_mensaje = Mensaje::where('chat', $mensaje->chat)->where('type', $mensaje->type)
            ->orderBy('created_at', 'ASC')->get()->last();
            $mensaje->to = 0;
            if ( !empty($last_mensaje->user_id) && $last_mensaje->user_id != Auth::user()->id )
                $mensaje->to = $last_mensaje->user_id;
            else if ( !empty($last_mensaje->to) && $last_mensaje->to != Auth::user()->id )
                $mensaje->to = $last_mensaje->to;
        }
        /* end TO */

        $mensaje->chat = $mensaje->to . '_' . $mensaje->user_id . '_' . $mensaje->email;

        if ( empty($mensaje->mensaje) && empty($data['no_mensaje_allowed']) ) {
            return response()->json([
                'msg' => __(':name is empty', ['name' => __('Message')]),
                'success'=> false
            ]);
        } else if ( $mensaje->save() ) {
            if ( isProEnv() ) {
                $to = User::find($mensaje->to);
                $chat_users = $mensaje->get_users();
                
                foreach ( $chat_users as $chat_user ) {
                    if ( is_int($chat_user) && $chat_user > 0 ) $usuario = User::find( $chat_user );
                    else {
                        $parts = explode('##', $chat_user);
                        $usuario = new User();
                        $usuario->id = 0;
                        $usuario->name = $parts[0];
                        $usuario->lastname = '';
                        $usuario->email = $parts[1];
                    }

                    $params = new \stdClass();
                    $params->usuario = $usuario;
                    $params->mensaje = $mensaje;
                    $params->from = Auth::user();
                    
                    $chat = \App\Models\Chat::where('type', $mensaje->type)->first();
                    $params->email_id = !empty($chat) ? $chat->email_id : 0;

                    $notificacion = new EmailNotification($params);
                    if ( $notificacion->check_email() && !empty($usuario) ) $to->notify( $notificacion );
                }
            }

            return response()->json([
                'msg' => __(':name sent successfully', ['name' => __('Message')]),
                'success'=> true
            ]);
        } else {
            return response()->json([
                'msg' => __('Error sending the :name', ['name' => __('Message')]),
                'success'=> false
            ]);
        }
    }


    public static function enviar_email($params)
    {
        if (!isProEnv()) {
            return;
        }

        if (!is_array($params)) {
            parse_str($params, $data);
        } else {
            $data = $params;
        }

        if (!isset($data['email_id']) || !$data['email_id'] || !isset($data['to']) || !$data['to'] ) {
            return response()->json([
                'msg' => __('Error sending the :name', ['name' => __('Email')]),
                'success'=> false
            ]);
        }

        $mensaje = new Mensaje();
        $mensaje->user_id = !empty($data['user_id']) ? $data['user_id'] : 0;
        $mensaje->mensaje = !empty($data['mensaje']) ? $data['mensaje'] : '';
        $mensaje->fitxer = '';
        $mensaje->email = !empty($data['email']) ? $data['email'] : '';
        $mensaje->name = !empty($data['name']) ? $data['name'] : '';
        $mensaje->type = 'email';
        $mensaje->params = json_encode($data);

        $from = auth()->user();

        if (!$from) {
            $from = new User([
                'name' => $data['name'],
                'lastname' => $data['lastname'],
                'email' => $data['email']
            ]);
        }

        $to = User::where('email', $data['to'])->first();

        if ($to) {
            $params = new \stdClass();
            $params->usuario = $to;
            $params->mensaje = $mensaje;
            $params->from = $from;
            $params->email_id = $data['email_id'];

            $notificacion = new EmailNotification($params);
            if ($notificacion->check_email() && !empty($to)) {
                $to->notify($notificacion);

                return;
            }
        }

        return response()->json([
            'msg' => __('Error sending the :name', ['name' => __('Email')]),
            'success'=> false
        ]);
    }
}
