<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Opcion;

class AvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $upload_max_filesize = 10000;
        if ( !empty(Opcion::where('key', 'upload_max_filesize')->first()) )
            $upload_max_filesize = Opcion::where('key', 'upload_max_filesize')->first()->value;

        return [
            'avatar' => "required|image|mimes:jpeg,bmp,jpg,svg,png|max:$upload_max_filesize",
        ];
    }
}
