<?php

namespace App\Http\Middleware;

use App\Models\CustomPost;
use Closure;
use Auth;
use App\Models\Admin\Rol;

class Rols
{
    public function handle($request, Closure $next)
    {
        $modules = scandir(app_path('Modules'));
        $controller = str_replace('Controllers\\', '',
            str_replace('App\Http\\', '', 
                str_replace('\App\Modules\\', '', 
                    request()->route()->getAction()['controller']
                )
            )
        );
        $parts = explode('\\', $controller);
        $module_name = $parts[0];

        foreach ($parts as $part) {
            if (in_array($part, $modules)) {
                $module_name = $part;
                break;
            }
        }

        if ( in_array($module_name, $modules) ) {
            $controller_name = str_replace("$module_name\\", '', $controller);
            $controller_name = str_replace("App\\Modules\\", '', $controller_name);
            $c_parts = explode('@', $controller_name);
            $grup = str_slug($module_name, '-');
            $controller = str_slug($c_parts[0], '-');
            $method = $c_parts[1];
        } else {
            $controller_name = $controller;
            $c_parts = explode('@', $controller_name);
            $grup = str_slug('General', '-');
            $controller = str_slug($c_parts[0], '-');
            $method = $c_parts[1];
        }

        if ( Auth::check() ) $permisos = Auth::user()->rol->get_permisos();
        else $permisos = Rol::where('name', 'guest')->first()->get_permisos();

        $can_access = !empty($permisos[$grup][$controller][$method]) && $permisos[$grup][$controller][$method];

        // Check custom posts permissions
        $postType = $request->get('post_type');

        if ($controller == 'adminpostscontroller' && $postType && !$can_access) {
            $customPosts = CustomPost::withTranslation()->get();

            $customPost = $customPosts->filter(function ($cp) use ($postType) {
                return $cp->post_type == $postType;
            })->first();

            if ($customPost) {
                $cpPermisos = (array)$customPost->permissions;
                $can_access = !empty($cpPermisos[auth()->user()->rol->name]->{$method}) && $cpPermisos[auth()->user()->rol->name]->{$method};
            }
        }

        if ( $can_access || ( Auth::check() && Auth::user()->isAdmin() ) ) return $next($request);
        else {
            return abort(403);
        }
    }
}
