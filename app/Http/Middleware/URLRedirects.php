<?php

namespace App\Http\Middleware;

use Closure;
use App\Module;

class URLRedirects
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( \Schema::hasTable('modules') ) {
            foreach (glob(app_path('Modules/*'), GLOB_ONLYDIR) as $dirname) {
                if ( Module::where([['name', basename($dirname)], ['active', true]])->exists() ) {
                    if (file_exists("$dirname/redirects.json")) {
                        $jsonString  = file_get_contents("$dirname/redirects.json");
                        $data = json_decode($jsonString, true);
                        $url = str_replace($request->root(), "", url()->current()) . "/";

                        if ($data && array_key_exists($url, $data)) {
                            return redirect($data[$url], 301);
                        }
                    }
                }
            }
        }

        return $next($request);
    }
}
