<?php

namespace App\Http\Middleware;

use URL;
use Closure;
use App\Language;
use Illuminate\Support\Facades\View;

class Locale
{
    private $excludedSegments = [
        '_debugbar',
        'robots.txt',
        'sitemap.xml'
    ];

    public function handle($request, Closure $next)
    {
        $segment = $request->segment(1);

        // Debugbar work arround
        if (in_array($segment, $this->excludedSegments)) {
            return $next($request);
        }

        $languages = Language::get();
        
        // Current is active
        $actual = $languages->firstWhere('code', $segment);
        $actual = ($actual && $actual->active) ? $actual->code : null;
        $default = $languages->firstWhere('default', 1)->code;

        // Fallbacks
        $current =  $actual ??
                    $languages->firstWhere('active', 1)->code ??
                    $default ??
                    config('app.locale');

        url()->defaults(['locale' => $current]);
        config()->set('translatable.fallback_locale', $default);
        view()->share ( 'language_code', $current );
        view()->share ( 'lang_default', $default );

        session(['locale' => $current]);
        app()->setLocale($current);

        // First segment error, fallback to active/default/env
        if (!in_array($segment, ['api']) && is_null($actual)) { // Api exception
            if ($segment == 'admin') return redirect(route('admin'));

            $segments = $request->segments();
            $fallback = $current;
            array_shift($segments);
            array_unshift($segments, $fallback);

            return redirect(implode('/', $segments) . '?' . (count($request->all()) ? http_build_query($request->all()) : ''));
        }

        execute_actions('after_locale');
        
        return $next($request);
    }
}
