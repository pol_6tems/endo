<?php


namespace App\Http\Composers\Admin;

use App\Models\Admin\AdminMenu;
use App\Models\Admin\AdminMenuSub;
use App\Models\Chat;
use App\Models\CustomPost;
use App\Models\Opcion;
use function GuzzleHttp\Promise\all;
use Illuminate\View\View;

class SidebarComposer
{
    protected $current_user_permisos;
    protected $current_user_role;

    protected $first_active = false;

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $permisos = $this->get_all_permisos_current_user();
        $this->current_user_permisos = $permisos;
        $this->current_user_role = auth()->user()->rol->name;
        
        $menus = AdminMenu::with(['submenus'])->orderby('order')->get();

        // Menus Loop
        foreach ($menus as $key => $menu) {
            $is_active = false;

            /* Menu automàtic Contingut */
            if ($menu->name == 'Content') {
                $put_custom_posts_in_content_menu = Opcion::where('key', 'put_custom_posts_in_content_menu')->first();
                if ($put_custom_posts_in_content_menu && $put_custom_posts_in_content_menu->value == '1') {
                    $menu = $this->generar_content_automatic_submenus($menu);

                    if (strpos(route_name(), 'admin.posts.') !== false) {
                        $is_active = true;
                    }
                }
            }
            /* end Menu automàtic Contingut */

            // Submenus Loop
            foreach ($menu->submenus as $key2 => $sub) {
                $obj = $this->get_controller_by_route_name($sub->url);

                if (!empty($obj)) {
                    // Revisem si té permisos per veure el controlador i el mètode sel·leccionat
                    if (!$this->menu_te_permisos($permisos, $obj, $sub->parameters)) {
                        unset($menus[$key]->submenus[$key2]);
                    } else {
                        $sub->is_active = $this->is_menu_active($sub);
                        if ($sub->is_active) $is_active = true;
                        $sub->href = $this->get_menu_url($sub, true);
                        $menus[$key]->submenus[$key2] = $sub;
                    }
                } else unset($menus[$key]->submenus[$key2]);
            }


            if (empty($menu->controller) || $menu->controller == 'Admin\PostsController') {
                $parametres = [];
                if ($menu->parameters) {
                    list($k, $v) = explode('=', $menu->parameters);
                    $parametres[$k] = $v;

                    if (!$this->checkCustomPostPermission($parametres['post_type'])) {
                        unset($menus[$key]);
                    }
                } else {
                    if ($menu->submenus->isEmpty()) {
                        unset($menus[$key]);
                    }
                }
            } else if ($menu->controller == 'Admin\MensajesController' && $menu->parameters) {
                    list($k, $v) = explode('=', $menu->parameters);
                    $params[$k] = $v;

                    if (isset($params['type'])) {
                        $allow = $this->checkChatPermission($params['type']);

                        if (!auth()->user()->isAdmin() && !$allow) {
                            unset($menu[$key]);
                        }
                    }

            } else {
                $obj = $this->get_controller_by_route_name($menu->url);
                if (!$this->menu_te_permisos($permisos, $obj, $menu->parameters)) {
                    unset($menus[$key]);
                }
            }

            if (!empty($menus[$key])) {
                if (!$is_active) $is_active = $this->is_menu_active($menu);

                if (!$this->first_active && $is_active) {
                    $this->first_active = true;
                } elseif ($this->first_active && $is_active) {
                    $is_active = false;
                }

                $menus[$key]->is_active = $is_active;
                $menus[$key]->href = $this->get_menu_url($menu);
            }
        }

        $view->with([
            'menu_items' => $menus
        ]);
    }


    private function get_controller_by_route_name($name)
    {
        $modules = scandir(app_path('Modules'));
        $routes = \Route::getRoutes();
        $result = ['grup' => null, 'controller' => null, 'method' => null];
        foreach ($routes as $route) {
            $action = $route->getAction();
            if (!empty($action['as']) && $name == $action['as']) {
                $controller = ltrim(str_replace('Controllers\\', '',
                    str_replace('App\Http\\', '',
                        str_replace('App\Modules\\', '', $action['controller'])
                    )
                ), '\\');
                $parts = explode('\\', $controller);
                $module_name = $parts[0];
                if (in_array($module_name, $modules)) {
                    $controller_name = str_replace("$module_name\\", '', $controller);
                    $c_parts = explode('@', $controller_name);
                    $result['grup'] = str_slug($module_name, '-');
                    $result['controller'] = str_slug($c_parts[0], '-');
                    $result['method'] = $c_parts[1];
                } else {
                    $controller_name = $controller;
                    $c_parts = explode('@', $controller_name);
                    $result['grup'] = str_slug('General', '-');
                    $result['controller'] = str_slug($c_parts[0], '-');
                    $result['method'] = $c_parts[1];
                }
            }
        }

        return $result;
    }


    private function menu_te_permisos($permisos, $obj, $parameters)
    {
        $grup = $obj['grup'];
        $controller = $obj['controller'];
        $method = $obj['method'];

        $permisos = (isset($permisos[$grup][$controller][$method])) ? $permisos[$grup][$controller][$method] : false;

        if (($controller == 'adminpostscontroller' && $parameters) || $parameters) {
            $params = [];
            // Convertir paràmetres (string) en una array associativa
            list($k, $v) = explode('=', $parameters);
            $params[$k] = $v;

            if (isset($params['post_type'])) {
                $permisos = $this->checkCustomPostPermission($params['post_type']);
            }
        }

        if ($controller == 'adminmensajescontroller' && $parameters) {
            list($k, $v) = explode('=', $parameters);
            $params[$k] = $v;

            if (isset($params['type'])) {
                $allow = $this->checkChatPermission($params['type']);

                return auth()->user()->isAdmin() || $allow;
            }
        }

        return auth()->user()->isAdmin() || (!empty($permisos) && $permisos);
    }


    private function checkCustomPostPermission($post_type)
    {
        if ($this->current_user_role == 'admin') {
            return true;
        }

        $te_permis = false;

        if ( $post_type ) {
            if ( !empty($this->current_user_permisos['custom_posts'][$post_type][$this->current_user_role]) )
                $te_permis = $this->current_user_permisos['custom_posts'][$post_type][$this->current_user_role]->index == "1";
            else if ( !empty($this->current_user_permisos['roles'][$post_type][$this->current_user_role]) )
                $te_permis = $this->current_user_permisos['roles'][$post_type][$this->current_user_role];
        }
        return $te_permis;
    }


    public function is_menu_active($menu)
    {
        if (isset($menu->is_active)) {
            return $menu->is_active;
        }

        $is_active = like_match($menu->controller, \Route::current()->getActionName(), true);
        if (!empty(request()->post_type)) {
            $post_type = request()->post_type;

            // Convertir paràmetres (string) en una array associativa
            if (!empty($menu->parameters)) {
                list($k, $v) = explode('=', $menu->parameters);
                $parametres[$k] = $v;

                $is_active = $is_active && $parametres['post_type'] == $post_type;
            } else $is_active = false;

        } else if (!empty(request()->type)) {
            $type = request()->type;

            // Convertir paràmetres (string) en una array associativa
            if (!empty($menu->parameters)) {
                list($k, $v) = explode('=', $menu->parameters);
                $parametres[$k] = $v;

                $is_active = $is_active && $parametres['type'] == $type;
            } else $is_active = false;

        } else if (!empty($menu->parameters)) $is_active = false;

        if ($this->first_active && $is_active) {
            $is_active = false;
        }

        return $is_active;
    }


    public function get_menu_url($menu, $is_submenu = false)
    {
        $href = 'javascript:void(0)';
        if ($menu->has_url == 1 || $is_submenu) {
            if ($menu->url_type == 'route' && \Route::has($menu->url)) {
                $href = route($menu->url) . ($menu->parameters ? '?' . $menu->parameters : '');
            } else if ($menu->url_type == 'url') {
                $href = $menu->url . ($menu->parameters ?: '');
            }
        }
        return $href;
    }


    protected function generar_content_automatic_submenus($menu)
    {
        $cps = CustomPost::withTranslation()->get();
        foreach ($cps as $k => $cp) {
            $cp_t = $cp->translate();
            if ( isset($cp_t->post_type) && $cp_t->title ) {
                $submenu = new AdminMenuSub();
                $submenu->name = !empty($cp_t->title_plural) ? $cp_t->title_plural : $cp_t->title;
                $submenu->url_type = 'route';
                $submenu->url = 'admin.posts.index';
                $submenu->controller = 'Admin\PostsController';
                $submenu->order = (count($menu->submenus) + $k) * 10;
                $submenu->parameters = 'post_type=' . $cp_t->post_type;

                if (strpos(route_name(), 'admin.posts.') !== false && request('post_type') == $cp_t->post_type && !$this->first_active) {
                    $submenu->is_active = true;
                }
                $menu->submenus[] = $submenu;
            }
        }
        return $menu;
    }

    protected function get_all_permisos_current_user() {
        $permisos = auth()->user()->rol->get_permisos();
        $cp_permissions = CustomPost::withTranslation()->whereNotNull('permissions')->get();
        foreach ( $cp_permissions as $cp_permission ) {
            $post_type = $cp_permission->post_type;
            $permisos['custom_posts'][$post_type] = (array)$cp_permission->permissions;
        }
        return $permisos;
    }


    private function checkChatPermission($type)
    {
        $chat = Chat::where('type', $type)->first();
        $userRol = auth()->user()->role;
        $allow = false;

        if ($chat) {
            $permissions = $chat->permissions;
            if (!empty($permissions) && isset($permissions[$userRol]) && $permissions[$userRol]) {
                $allow = true;
            }
        }

        return $allow;
    }

}