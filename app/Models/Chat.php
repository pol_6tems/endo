<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Email;
use App\Models\Mensaje;

class Chat extends Model
{
    protected $fillable = ['name', 'type', 'email_id', 'params', 'lines', 'buttons', 'permissions'];
    
    public function email() {
        return $this->belongsTo(Email::class, 'email_id');
    }

    public function get_params() {
        return !empty($this->params) ? json_decode($this->params, TRUE) : array();
    }

    public function get_lines() {
        return !empty($this->lines) ? json_decode($this->lines, TRUE) : array();
    }

    public function get_buttons() {
        return !empty($this->buttons) ? json_decode($this->buttons, TRUE) : array();
    }


    public function getPermissionsAttribute($value)
    {
        return !empty($value) ? json_decode($value, true) : [];
    }


    public function setPermissionsAttribute($value)
    {
        $this->attributes['permissions'] = $value ? json_encode($value) : '';
    }
}
