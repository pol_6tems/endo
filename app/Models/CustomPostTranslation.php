<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomPostTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'title_plural', 'post_type', 'post_type_plural'];


    public function customPost() {
        return $this->belongsTo(CustomPost::class);
    }
}
