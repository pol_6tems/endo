<?php

namespace App\Models\Traits;

use App\Facades\PostsRepo;
use App\Post;
use App\Language;
use App\Models\PostMeta;
use App\Models\CustomField;
use App\Models\CustomFieldGroup;

trait HasCustomFields
{
    public function metas() {
        return $this->hasMany(PostMeta::class, 'post_id', 'id');
    }

    public function cfGroups() {
        return $this->hasMany(CustomFieldGroup::class, 'post_type', 'type');
    }

    public function set_field($field_name, $value, $lang = null) {
        if (is_null($lang)) $lang = app()->getLocale();

        $saved = false;
        // Searching in custom field groups the field_name
        if (!is_array($field_name)) $field_name = [ $field_name => $value ];

        $this->cfGroups->each(function($el) use ($field_name, &$saved, $lang) {
            foreach($field_name as $key => $field) {
                if ($customField = $el->get_field($key)) {
                    $saved = PostMeta::updateOrCreate([
                        'post_id' => $this->id,
                        'custom_field_id' => $customField->id,
                        'locale' => $lang,
                    ],  ['value' => $field,]);
                }
            }
        });

        if ($saved) return $this;
        return $saved;
    }

    public function get_field($field_name, $lang = null, $args = null) {
        /*$fieldVar = str_slug($field_name . ($lang ? '_' . $lang : ''), '_');

        if (isset($this->$fieldVar) && $this->$fieldVar) {
            return $this->$fieldVar;
        }*/

        $obj = $this->get_field_object($field_name, $lang, $args);

        $val = $obj ? $obj->value : null;


        /*$this->$fieldVar = $val;*/


        return $val;
    }


    public function get_raw_field($field_name, $lang = null, $args = null) {
        /*$fieldVar = str_slug($field_name . ($lang ? '_' . $lang : ''), '_');

        if (isset($this->$fieldVar) && $this->$fieldVar) {
            return $this->$fieldVar;
        }*/

        $obj = $this->get_field_object($field_name, $lang, $args);

        $val = $obj ? $obj->getAttributes()['value'] : null;


        /*$this->$fieldVar = $val;*/


        return $val;
    }


    public function hasField($field_name, $lang = null) {
        return $this->get_field($field_name, $lang) != null;
    }

    public function get_field_id($field_name) {
        $obj = $this->get_field_object($field_name);

        if ( !empty($obj->custom_field_id) ) {
            return $obj->custom_field_id;
        } else {
            return null;
        }
    }

    public function get_field_object($field_name, $lang = null, $args = null) {
        if ( empty($lang) ) $lang = app()->getLocale();

        if (!isset($this->metas->first()->relations['customField'])) {
            $this->metas->load(['customField']);
        }

        $meta = $this->metas->filter(function ($meta) use ($field_name, $lang, $args) {
            $return = $meta->locale == $lang && $meta->customField && $meta->customField->name == $field_name;
            
            if ( !$return ) {
                $return = $meta->locale == $lang && $meta->field_name == $field_name;
            }

            if ( !empty($args) && $return ) {
                if ( !empty($args['parent_id']) ) {
                    $return = $meta->parent_id == $args['parent_id'];
                }

                if (isset($args['order']) && $return ) {
                    $return = $meta->order == $args['order'];
                }
            }

            return $return;
        })->first();

        if (is_null($meta)) {
            $lang = PostsRepo::getDefaultLang();
            $meta = $this->metas->filter(function ($meta) use ($field_name, $lang, $args) {
                $return = $meta->locale == $lang && $meta->customField && $meta->customField->name == $field_name;
                
                if ( !$return ) {
                    $return = $meta->locale == $lang && $meta->field_name == $field_name;
                }
    
                if ( !empty($args) && $return ) {
                    if ( !empty($args['parent_id']) ) {
                        $return = $meta->parent_id == $args['parent_id'];
                    }
    
                    if (isset($args['order']) && $return ) {
                        $return = $meta->order == $args['order'];
                    }
                }
    
                return $return;
            })->first();
        }

        if ($meta && $meta->customField) {
            $meta->name = $meta->customField->name;
            $meta->type = $meta->customField->type;
            $meta->params = $meta->customField->params;
        }

        return $meta;
    }

    /**
     * Retorna una `Collection` amb tots els Custom Field Groups
     * segons el Post Type especificat
     *
     * @param [String] $post_type
     * @return void
     */
    public function getCustomFields() {
        return $this->cfGroups->pluck('customFields')->flatten();
    }

    public function getSelectionsCfFromPost(Post $post) {
        $cpt = $post->customPostTranslations->first();
        $cf = $this->getCustomFields($this->type);
        $result = $cf->filter(function ($value, $key) use ($cpt) {
            if ($value->type == 'selection') {
                $params = json_decode($value->params);
                return $cpt->custom_post_id == $params->post_object;
            }
        });
        return $result->first();
    }

    public function save_custom_fields($fields, $lang, $is_child = false) {
        if ( $is_child ) {
            foreach ($fields as $idx => $subfields) {
                $this->save_custom_fields($subfields, $lang);
            }
        } else if ($lang == 'deleted') {
            foreach($fields as $lang => $field) {
                foreach($field as $parent => $f) {

                    // Obtenim els Posts Meta que tinguin el parent_id i el post_id del eliminat
                    $postMetaBuilder = PostMeta::where( 'parent_id', $parent)
                                            ->where( 'post_id', $this->id)
                                            ->where( 'locale', $lang);

                    // Obtenim els posts que comparteixin els ordres que hem d'eliminar
                    $postMetaBuilder->where(function($query) use ($f) {
                        foreach(array_keys($f) as $order) {
                            $query->orwhere( 'order', $order);
                        }
                    });

                    // Eliminem els posts
                    $postMetaBuilder->delete();
                }
            }
        } else {
            $order = -1;
            foreach ($fields as $field_id => $field) {


                if ( $field_id == 'order' ) {
                    $order = $field;
                    continue;
                }
                
                $field_value = empty($field['fields']) ? $field : null;

                if ( is_array($field_value) ) {
                    $field_value = json_encode($field_value);
                }

                $meta = [
                    'post_id' => $this->id,
                    'custom_field_id' => 0,
                    'value' => $field_value,
                    'locale' => $lang,
                ];

                if ( is_int($field_id) ) {
                    $meta['custom_field_id'] = $field_id;
                    $where = [['post_id', $this->id], ['custom_field_id', $field_id], ['locale', $lang]];
                } else {
                    $meta['field_name'] = $field_id;
                    $where = [['post_id', $this->id], ['field_name', $field_id], ['locale', $lang]];
                }

                // Parent_id
                $f_aux = CustomField::find($field_id);
                
                if ( !empty($f_aux->parent_id) ) {
                    $meta['parent_id'] = $f_aux->parent_id;
                    $where[] = ['parent_id', $f_aux->parent_id];
                }
                // Order
                if ( $order >= 0 ) {
                    $meta['order'] = $order;
                    $where[] = ['order', $order];
                }

                
                
                $post = PostMeta::updateOrCreate($where, $meta);
                
                // Save SubFields
                if ( !empty($field['fields']) ) {
                    $this->save_custom_fields($field['fields'], $lang, true);
                }
            }
        }
    }

    /**
     * Magic method for getting custom fields
     * by directly calling the name on the model
     *
     * @param [type] $key
     * @return void
     */
    /*
    public function __get($key)
    {
        if ($res = parent::__get($key)) {
            return $res;
        }
        return $this->get_field($key);
    }
    */

    
    /**
     * Magic method for setting custom fields
     * by dierctly calling the name on the model
     *
     * @param [type] $key
     * @param [type] $value
     */
    /*
    public function __set($key, $value)
    {
        if ($res = parent::__get($key)) {
            return $res;
        }
        return $this->set_field($key, $value);
    }
    */
}