<?php
/**
 * Created by PhpStorm.
 * User: sergisolsona
 * Date: 24/07/2019
 * Time: 08:29
 */

namespace App\Models\Traits;


use App\Models\Presenters\CouponPresenter;

trait CouponTrait
{
    use Presentable;

    public $customDates = ['min_date', 'max_date'];

    protected $purchaseColumnName = 'coupon_id';

    protected $purchaseUserColumnName = 'user_id';

    protected $presenter = CouponPresenter::class;


    public function __construct()
    {
        $this->dates = array_merge($this->dates, $this->customDates);

        parent::__construct();
    }


    public function purchases()
    {
        return $this->hasMany($this->purchaseClass, $this->purchaseColumnName);
    }


    public function user()
    {
        return $this->belongsTo($this->userClass, 'id', 'owner_id');
    }


    public function getValueAttribute($value)
    {
        return $value / 100;
    }


    public function setValueAttribute($value)
    {
        $this->attributes['value'] = $value * 100;
    }


    public function getMinPurchaseAmountAttribute($value)
    {
        if (!is_null($value)) {
            return $value / 100;
        }
    }


    public function setMinPurchaseAmountAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['min_purchase_amount'] = $value * 100;
        } else {
            $this->attributes['min_purchase_amount'] = null;
        }
    }


    public function getPurchaseUserColumn()
    {
        return $this->purchaseUserColumnName;
    }


    public static function getStatuses()
    {
        return [
            0 => 'Inactive',
            1 => 'Active'
        ];
    }
}