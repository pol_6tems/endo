<?php
/**
 * Created by PhpStorm.
 * User: sergisolsona
 * Date: 24/07/2019
 * Time: 08:29
 */

namespace App\Models\Traits;

use App\Category;

trait Categorizable
{
    public function categories() {
        return $this->morphToMany(Category::class, 'categorizable');
    }
}