<?php


namespace App\Models\Traits;


use App\Facades\PostsRepo;
use DB;
use App\Post;
use App\User;
use App\Models\Media;
use Jenssegers\Date\Date;
use App\Models\CustomPost;
use App\Models\CustomField;

trait ModelMetas
{
    public function customField()
    {
        return $this->belongsTo(CustomField::class, 'custom_field_id');
    }


    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'custom_field_id');
    }
    
    
    public function childrenCustomFields()
    {
        return $this->hasMany(CustomField::class, 'parent_id', 'custom_field_id');
    }


    public function repeaterMetas()
    {
        return $this->hasMany(self::class, 'post_id', 'post_id');
    }


    // TODO: Repeater

    public function getValueAttribute($value)
    {
        if($this->customField) {
            switch($this->customField->type) {
                case 'number':
                    $value = floatval(str_replace(",", ".", $value));
					if (!is_decimal($value)) return intval($value);
                    break;
                case 'selection':
                    $params = json_decode($this->customField->params);

                    $isChoices = false;

                    if (isset($params->choices) && $params->choices && $params->choices != '') {
                        $isChoices = true;
                    }

                    if (isset($params->multiple) && $params->multiple && !$isChoices) {
                        $valors = json_decode($value, TRUE);
                        if ($valors && is_array($valors)) {
                            $valors = array_filter($valors, function($k) { return $k != '0'; });
                            if (!empty($valors)) {
                                $valors = array_values($valors);

                                if ($params->post_object != 'user') {
                                    return PostsRepo::whereIn($valors);
                                } else {
                                    return User::whereIn('id', $valors)->get();
                                }
                            }
                        } else {
                            if ($params->post_object != 'user') {
                                return PostsRepo::whereIn([$value]);
                            } else {
                                return User::find($value);
                            }
                        }
                    } else {
                        $post = null;

                        if (is_numeric($value) && $value > 0 && !$isChoices) {
                            if ($params->post_object == 'any') {
                                $post = CustomPost::where('id', $value)->first();
                            } else if ($params->post_object != 'user') {
                                $post = PostsRepo::find($value);/*Post::where('id', $value)->first();*/
                            } else {
                                $post = User::find($value);
                            }
                        }

                        if ($post) {
                            return $post;
                        }

                        return $value;
                    }

                    return null;
                    break;
                case 'gallery':
                    if ( !empty($value) ) {
                        if ($imatges = (array) json_decode($value)) {
                            return Media::whereIn('id', $imatges)->get()->sortBy(function ($media) use ($imatges) {
                                return array_search($media->id, $imatges);
                            })->all();
                        }
                    }
                    break;
                case 'media':
                    if ( !empty($value) ) {
                        return PostsRepo::findMedia($value);
                    }
                    break;
                case 'repeater':
                    //$order_group = $this->children->where('post_id', $this->post_id)->sortBy('order')->unique('order');
                    $order_group = self::where('parent_id', $this->custom_field_id)->where('locale', $this->locale)->where('post_id', $this->post_id)->select('order')->orderby('order')->distinct()->get();
                    $temp = [];
                    
                    //$fields = $this->childrenCustomFields;
                    $fields = CustomField::where('parent_id', $this->custom_field_id)->get();
                
                    foreach ( $order_group as $order_obj ) {
                        $order = $order_obj->order;

                        foreach ($fields as $field) {
                            if ($field->name) {
                                $temp[$order][$field->name] = [
                                    "id" => $field->id,
                                    "cfg_id" => $field->cfg_id,
                                    "parent_id" => $field->parent_id,
                                    'value' => $this->get_field($field->name, $this->locale, ['parent_id' => $this->custom_field_id, 'order' => $order]),
                                ];
                            }
                        }
                    }

                    return $temp;
                    break;
                default:
                    if ( !empty($value) ) {
                        $json = json_decode($value);
                        if ($json) {
                            return $json;
                        } else {
                            return $value;
                        }
                    } else if ( !empty($this->file) ) {
                        return $this->file;
                    } else {
                        if (is_null($value)) {
                            $value = '';
                        }

                        return $value;
                    }
                    break;
            }
        }

        return $value;
    }
    


    private function get_field($field_name, $lang = null, $args = null)
    {
        if (empty($lang)) $lang = app()->getLocale();

        if (!isset($this->relations['repeaterMetas'])) {
            $this->load(['repeaterMetas' => function ($query) use ($args) {
                $query->where('parent_id', $args['parent_id']);
            }]);
        }

        if (!isset($this->repeaterMetas->first()->relations['customField'])) {
            $this->repeaterMetas->load(['customField']);

            $repeaters = $this->repeaterMetas->filter(function ($meta) {
                return $meta->customField && $meta->customField->type == 'repeater';
            });

            if ($repeaters->count()) {
                $repeaterParents = $repeaters->map(function ($repeater) {
                    return $repeater->custom_field_id;
                });

                $this->repeaterMetas->load(['children' => function ($query) use ($repeaterParents) {
                    $query->where('post_id', $this->post_id)->whereIn('parent_id', $repeaterParents);
                }, 'childrenCustomFields']);
            }
        }

        $meta = $this->repeaterMetas->filter(function ($meta) use ($field_name, $lang, $args) {
            $return = $meta->locale == $lang && $meta->customField && $meta->customField->name == $field_name;

            if (!empty($args) && $return) {
                if (!empty($args['parent_id'])) {
                    $return = $meta->parent_id == $args['parent_id'];
                }

                if (isset($args['order']) && $return) {
                    $return = $meta->order == $args['order'];
                }
            }

            return $return;
        })->first();

        if ($meta && $meta->customField) {
            $meta->name = $meta->customField->name;


            $meta->type = $meta->customField->type;
            $meta->params = $meta->customField->params;
        }

        if ($meta) return $meta->value;
        else return null;

        // TODO: remove
        $where = [
            ['post_meta.post_id', $this->post_id],
            ['post_meta.locale', $lang],
            ['custom_fields.name', $field_name],
        ];

        if (!empty($args)) {

            if (!empty($args['parent_id'])) $where[] = ['post_meta.parent_id', $args['parent_id']];
            if (isset($args['order'])) $where[] = ['post_meta.order', $args['order']];
        }

        $meta = self::with('customField')
            ->where($where)
            ->join('custom_fields', 'post_meta.custom_field_id', '=', 'custom_fields.id')
            ->where($where)
            ->select(DB::raw('post_meta.*, custom_fields.name, custom_fields.type, custom_fields.params'))
            ->first();

        if ($meta) return $meta->value;
        return true;
    }
}