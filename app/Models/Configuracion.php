<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Configuracion extends Model
{
    protected static $config_file_dir = 'public/configuracion';
    protected static $config_file_dir_public = 'storage/configuracion/';
    protected $fillable = ['key', 'value', 'type', 'title'];

    public static function get_config($key) {
        $config = self::where('key', $key)->get();
        if ( $config->count() > 0 ) {
            $c = $config->first();
            if ( $c->type == 'img' ) {
                return $c->get_file_url();
            } else if ( $c->type == 'page' && !empty($c->value) ) {
                return Post::find($c->value);
            } else {
                return $c->value;
            }
        }
        return null;
    }

    public function get_file_url() {
        return !empty($this->file) ? asset(self::$config_file_dir_public . $this->file) : '';
    }

    public static function get_config_file_dir() {
        return self::$config_file_dir;
    } 

    public static function get_config_file_dir_public() {
        return self::$config_file_dir_public;
    } 
}
