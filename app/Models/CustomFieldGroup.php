<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomFieldGroup extends Model
{
    protected $guarded = array();
    
    protected $fillable = ['title', 'post_type', 'template', 'order', 'position'];

    public function fields()
    {
        return $this->customFields()->where('parent_id', null)->orderBy('order');
    }

    public function get_field($field_name) {
        return $this->customFields->filter(function($value, $key) use ($field_name) {
            return $field_name == $value->name;
        })->first();
    }

    public function customFields()
    {
        return $this->hasMany(CustomField::class, 'cfg_id', 'id');
    }

    public static function get_cfs_by_post_type($post_type) {
        $custom_fields = array();
        $groups = CustomFieldGroup::where('post_type', $post_type)->orderBy('order')->get();
        foreach ( $groups as $group ) {
            $fields = $group->fields;
            foreach ( $fields as $field ) $custom_fields[] = $field;
        }
        return $custom_fields;
    }
}
