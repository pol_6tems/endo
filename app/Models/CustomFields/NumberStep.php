<?php


namespace App\Models\CustomFields;


class NumberStep extends FieldParam
{

    protected $id = 'input_step';
    protected $title = 'Step';
    protected $instructions = '.1, .01';
    protected $field = 'text';
}