<?php

namespace App\Models\CustomFields;

class PlaceholderParam extends FieldParam
{
    protected $id = 'placeholder';
    protected $title = 'Placeholder';
    protected $instructions = '';
    protected $field = 'textarea';
}