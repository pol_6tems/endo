<?php

namespace App\Models\CustomFields;

class MaxLengthParam extends FieldParam
{
    protected $id = 'max_length';
    protected $title = 'Max Length';
    protected $instructions = '';
    protected $field = 'number';
}