<?php

namespace App\Models\CustomFields;

class PostObjectParam extends FieldParam
{
    protected $id = 'post_object';
    protected $title = 'Post object';
    protected $instructions = '';
    protected $field = 'text';
}