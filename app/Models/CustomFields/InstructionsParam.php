<?php

namespace App\Models\CustomFields;

class InstructionsParam extends FieldParam
{
    protected $id = 'instructions_param';
    protected $title = 'Instructions';
    protected $instructions = '';
    protected $field = 'text';
}