<?php
namespace App\Models\CustomFields;

class ChoicesParam extends FieldParam
{
    protected $id = 'choices';
    protected $title = 'Choices';
    protected $instructions = '';
    protected $field = 'textarea';
}