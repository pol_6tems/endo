<?php

namespace App\Models\CustomFields;

class HeightParam extends FieldParam
{
    protected $id = 'height_param';
    protected $title = 'Height';
    protected $instructions = '';
    protected $field = 'text';
}