<?php

namespace App\Models\CustomFields;

class RequiredParam extends FieldParam
{
    protected $id = 'required';
    protected $title = 'Required';
    protected $instructions = '';
    protected $field = 'boolean';
}