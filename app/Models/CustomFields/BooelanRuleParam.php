<?php
namespace App\Models\CustomFields;

class BooelanRuleParam extends FieldParam
{
    protected $id = 'boolean_rule';
    protected $title = 'Boolean Rule';
    protected $instructions = '';
    protected $field = 'text';
}