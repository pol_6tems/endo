<?php

namespace App\Models\CustomFields;

class WysiwygParam extends FieldParam
{
    protected $id = 'wysiwyg';
    protected $title = 'WYSIWYG';
    protected $instructions = '';
    protected $field = 'boolean';
}