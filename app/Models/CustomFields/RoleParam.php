<?php

namespace App\Models\CustomFields;

class RoleParam extends FieldParam
{
    protected $id = 'role';
    protected $title = 'Role';
    protected $instructions = '';
    protected $field = 'text';
}