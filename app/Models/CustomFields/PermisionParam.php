<?php

namespace App\Models\CustomFields;

class PermisionParam extends FieldParam
{
    protected $id = 'permision';
    protected $title = 'Permissions';
    protected $instructions = '';
    protected $field = 'text';
}