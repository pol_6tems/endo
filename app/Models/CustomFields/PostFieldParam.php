<?php

namespace App\Models\CustomFields;

class PostFieldParam extends FieldParam
{
    protected $id = 'post_type';
    protected $title = 'Post type';
    protected $instructions = '';
    protected $field = 'text';
}