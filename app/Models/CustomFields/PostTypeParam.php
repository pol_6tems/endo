<?php

namespace App\Models\CustomFields;

class PostTypeParam extends FieldParam
{
    protected $id = 'post_field';
    protected $title = 'Campo';
    protected $instructions = '';
    protected $field = 'text';
}