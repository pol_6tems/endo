<?php

namespace App\Models;

use App\Models\Traits\ModelMetas;
use Illuminate\Database\Eloquent\Model;
use App\Models\CustomField;
use App\Post;

class PostMeta extends Model
{
    use ModelMetas;
    
    protected $table = 'post_meta';
    protected $fillable = ['post_id', 'custom_field_id', 'value', 'file', 'locale', 'parent_id', 'order', 'field_name'];

    public function custom_field()
    {
        return $this->belongsTo(CustomField::class, 'custom_field_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

    public function metas()
    {
        return $this->hasMany(PostMeta::class, 'parent_id', 'id');
    }
}
