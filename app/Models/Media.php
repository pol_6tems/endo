<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    protected $fillable = [
        'user_id',
        'title',
        'legend',
        'alt',
        'description',
        'width',
        'height',
        'file_path',
        'file_name',
        'file_type',
        'file_size',
        'level',
    ];
    protected $media_path = 'storage/media';
    protected $thumbnail_sizes = [
        'thumbnail' => 150,
        'medium' => 300,
        'medium_large' => 768,
        'large' => 1024,
    ];

    protected $file_types = [
        'image' => ['jpg','png', 'jpeg', 'svg', 'webp', 'gif', 'bmp', 'jfif', 'tiff'],
        'doc' => ['pdf', 'xls', 'xlsx', 'doc', 'docx', 'odt'],
        'videos' => ['mp4'],
    ];

    protected $attributes = [
        'user_id' => 0,
        'title' => '',
    ];
    

    public static function get_thumbnail_sizes() {
        return (new Media())->thumbnail_sizes;
    }

    public function dimensions() {
        return $this->width . ' x ' . $this->height;
    }

    public function is_image($type = null) {
        if (empty($type)) $type = $this->file_type;
        return in_array(strtolower($type), $this->file_types['image']);
    }

    
    public function is_video($type = null) {
        if (empty($type)) $type = $this->file_type;
        return in_array(strtolower($type), $this->file_types['videos']);
    }

    public function get_thumbnail_url($size = '') {
        if ( !array_key_exists($size, $this->thumbnail_sizes) || $this->file_type == 'svg') $size = '';
        else $size = '-' . $size;        

        $path = $this->media_path . '/' . $this->file_path . '/' .$this->file_name . $size . '.' . $this->file_type;
        
        $filename = str_replace('+', 'PLUSREPLACE', $this->file_name);
        $filename = rawurlencode($filename);
        $filename = str_replace('PLUSREPLACE', '+', $filename);
        $encodedPath = $this->media_path . '/' . $this->file_path . '/' . $filename . $size . '.' . $this->file_type;
        $url = asset($encodedPath);
        $real_path = public_path($path);
        if ( file_exists($real_path) ) return $url;
        else return '';
        //else return public_path('images/image_placeholder.jpg');
    }

    public function has_thumbnail() {
        return !empty($this->get_thumbnail_url());
    }

    public function remove_thumbnails() {
        $files = [];
        $file = storage_path('app/public/media/') . $this->file_path . '/' . $this->file_name . '.' . $this->file_type;
        if ( \File::exists($file) ) $files[] = $file;
        
        foreach ( $this->thumbnail_sizes as $size => $width ) {
            $file = storage_path('app/public/media/') . $this->file_path . '/' . $this->file_name . '-' . $size . '.' . $this->file_type;
            if ( \File::exists($file) ) $files[] = $file;
        }
        return \File::delete($files);
    }

    public function get_all_thumbnails() {
        $thumbnails = [
            'full' => $this->get_thumbnail_url(),
        ];
        foreach ( $this->thumbnail_sizes as $size => $width ) {
            $thumbnails[$size] = $this->get_thumbnail_url($size);
        }
        return $thumbnails;
    }
    
    public function getFileType() { return $this->file_type; }

    public static function getFileTypesArray($type) {
        return json_encode((new Media())->file_types[$type]);
    }


    public function getFilePath()
    {
        return public_path() .'/storage/media/' . $this->file_path . '/' . $this->file_name . '.' . $this->file_type;
    }

    public function getFileName() {
        return $this->file_name . '.' . $this->file_type;
    }

    public function isCroppable()
    {
        return $this->width && $this->height;
    }
}
