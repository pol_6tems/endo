<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class AdminMenuSub extends Model
{
    protected $table = 'admin_menus_sub';
    
    protected $fillable = ['menu_id', 'name', 'url_type', 'url', 'order', 'controller', 'parameters', 'icon'];

    public function menu()
    {
        return $this->belongsTo(AdminMenu::class, 'id', 'menu_id');
    }
}
