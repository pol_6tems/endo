<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Rol extends Model
{
    protected $table = 'rols';
    
    protected $fillable = ['name', 'level', 'permisos'];

    public function users()
    {
        return $this->hasMany(User::class, 'role', 'name');
    }

    public function get_permisos() {
        if ( empty($this->permisos) ) return array();
        else return json_decode($this->permisos, true);
    }

    public static function can_access($role_name, $route_name) {
        //if ( $role_name == 'admin' ) return true;
        $can_access = false;
        $rol = Rol::where('name', $role_name)->first();
        if ( $rol == null ) return false;

        $modules = scandir(app_path('Modules'));
        $permisos = $rol->get_permisos();
        $routes = \Route::getRoutes()->getRoutes();
        foreach ($routes as $route) {
            if ( $route->getName() != $route_name ) continue;
            $action = $route->getAction();
            $controller = str_replace('Controllers\\', '',
                str_replace('App\Http\\', '', 
                    str_replace('\App\Modules\\', '', $action['controller'])
                )
            );
            $parts = explode('\\', $controller);
            $module_name = $parts[0];
            if ( in_array($module_name, $modules) ) {
                $controller_name = str_replace("$module_name\\", '', $controller);
                $c_parts = explode('@', $controller_name);
                $grup = str_slug($module_name, '-');
                $controller = str_slug($c_parts[0], '-');
                $method = $c_parts[1];
            } else {
                $controller_name = $controller;
                $c_parts = explode('@', $controller_name);
                $grup = str_slug('General', '-');
                $controller = str_slug($c_parts[0], '-');
                $method = $c_parts[1];
            }
            
            $can_access = !empty($permisos[$grup][$controller][$method]) && $permisos[$grup][$controller][$method];
        }
        return $can_access;
    }
}
