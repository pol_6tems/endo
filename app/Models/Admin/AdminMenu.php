<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class AdminMenu extends Model
{
    protected $table = 'admin_menus';
    
    protected $fillable = ['name', 'icon', 'has_url', 'url_type', 'url', 'controller', 'order', 'parameters'];

    public function submenus()
    {
        return $this->hasMany(AdminMenuSub::class, 'menu_id', 'id')->orderBy('order');
    }
}
