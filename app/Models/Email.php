<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = ['name', 'subject', 'greeting', 'lines', 'salutation'];
    
    public function get_lines() {
        return !empty($this->lines) ? json_decode($this->lines, TRUE) : array();
    }
}
