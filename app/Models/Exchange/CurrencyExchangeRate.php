<?php


namespace App\Models\Exchange;


use Illuminate\Database\Eloquent\Model;

class CurrencyExchangeRate extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date'];


    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}