<?php

namespace App\Models;

use Auth;
use App\User;
use App\Models\Chat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Messages\MailMessage;

class Mensaje extends Model {
    const ROL_LEVEL_LIMIT = 50;
    
    protected $fillable = ['user_id', 'mensaje', 'fitxer', 'visto', 'email', 'name', 'type', 'chat', 'params', 'to'];

    /**
     * @Relation
     */
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @Relation
     */
    public function to() {
        return $this->belongsTo(User::class, 'to');
    }

    /**
     * @Relation
     */
    public function get_chat() {
        return $this->belongsTo(Chat::class, 'type', 'type');
    }

    public function scopeWithAll($query) {
        $query->with('user', 'to', 'get_chat');
    }

    public function get_users()
    {
        $my_user_id = Auth::check() ? Auth::user()->id : 0;

        $user_ids = self::where('chat', $this->chat)
            ->whereNotIn('user_id', [0, $my_user_id])
            ->groupBy("user_id")
            ->pluck('user_id')->toArray();

        $names_emails = self::where('chat', $this->chat)
            ->where('email', '<>', '')
            ->whereNotNull('email')
            ->selectRaw("CONCAT(name, '##', email) AS name_email")
            ->groupBy("name_email")
            ->pluck('name_email')->toArray();

        /*var_dump($user_ids);
        die();*/

        $list = array_merge($user_ids, $names_emails);

        // Fix: primer missatge és de petició de reserva retiro
        if (!count($list)) {
            $list = [(int)$this->to];
        }

        return $list;
    }

    public function get_param($name, $show_name = true) {
        $params = !empty($this->params) ? json_decode($this->params, TRUE) : array();
        $value = '';
        if ( !empty($params[$name]) ) {
            if ( $show_name ) $value = $name . ': ' . nl2br($params[$name]);
            else $value = nl2br($params[$name]);
        }
        return $value;
    }
    
    public function get_lines() {
        return !empty($this->lines) ? json_decode($this->lines, TRUE) : array();
    }

    public function show_message($no_action = false) {
        $chat = $this->get_chat;
        $lines = !empty($chat) ? $chat->get_lines() : null;
        
        if ( !empty($this->mensaje) ) return nl2br($this->mensaje);
        
        if ( !empty($lines) ) {

            $mail_message = new MailMessage();

            // Lines
            foreach ( $lines as $line ) {
                if ( empty($line['line']) ) continue;
                
                
                if ( empty($line['is_action']) ) {
                    // Line
                    ob_start();
                    eval("echo " . $line['line'] . ';');
                    $line_value = ob_get_clean();

                    if ( !empty($line_value) ) $mail_message->line( $line_value );

                } else if ( !$no_action ) {
                    // Action
                    $line_parts = explode('##', $line['line']);
        
                    ob_start();
                    eval("echo " . $line_parts[0] . ';');
                    $line_action_url = ob_get_clean();
                    $line_action_txt = $line_action_url;

                    if ( !empty($line_parts[1]) ) {
                        ob_start();
                        eval("echo " . $line_parts[1] . ';');
                        $line_action_txt = ob_get_clean();
                    }
                    
                    $line_value .= "<a href='" . $line_action_url . "'>" . $line_action_txt . "</a>";
        
                    if ( !empty($line_value) ) $mail_message->line( $line_value );
                }
            }
            
            $value = "";
            if ( !empty($mail_message->introLines) ) {
                foreach ( $mail_message->introLines as $line ) {
                    if ( !empty(trim($line)) ) $value .= "$line<br>";
                }
            }
        }
        // end Lines

        if ( empty($value) ) {
            $value = $this->mensaje;
            if ( $no_action ) $value = strip_tags($value);
        }

        return $value;
        
    }
}
