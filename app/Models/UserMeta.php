<?php

namespace App\Models;

use App\Models\Traits\ModelMetas;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Post;
use App\Models\CustomField;

class UserMeta extends Model
{
    use ModelMetas;
    
    protected $table = 'user_meta';
    protected $fillable = ['user_id', 'post_id', 'custom_field_id', 'value'];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function post() {
        return $this->belongsTo(Post::class, 'post_id');
    }

    public function custom_field() {
        return $this->belongsTo(CustomField::class, 'custom_field_id');
    }

    public function get_value() {
        if ( !self::isJson($this->value) ) {
            return $this->value;
        } else {
            return (array)json_decode($this->value);
        }
    }

    public static function isJson($value) {
        json_decode($value);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public static function get_meta($post_id, $custom_field_id) {
        return self::where([
            ['post_id', $post_id],
            ['custom_field_id', $custom_field_id],
        ])->get();
    }
}
