<?php

namespace App\Models;

use App\Facades\PostsRepo;
use App\Models\Traits\Categorizable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class CustomPost extends Model
{
    use Translatable, Categorizable;
    
    public $translatedAttributes = ['title', 'title_plural', 'post_type', 'post_type_plural'];
    protected $fillable = ['params', 'permissions'];
    
    public $useTranslationFallback = true;

    protected $attributes = [
        'params' => '{}',
        'permissions' => '{}',
    ];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->defaultLocale = \App::getLocale();
    }

    function getPermissionsAttribute($value) {
        return json_decode($value);
    }

    function getCFG() {
        return CustomFieldGroup::where('post_type', $this->post_type)->get();
    }

    public static function get_by_post_type($post_type) {
        return PostsRepo::getCPWithTranslation('post_type', $post_type);
    }

    public function scopeOfSingle($query, $type) {
        return $query->withTranslation()->whereTranslation('post_type', $type);
    }

    public function scopeOfPlural($query, $plural) {
        return $query->withTranslation()->whereTranslation('post_type_plural', $plural);
    }
}
