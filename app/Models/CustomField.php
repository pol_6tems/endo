<?php

namespace App\Models;

use App\Models\CustomFieldGroup;
use App\Models\CustomFields\NumberStep;
use App\Models\CustomFields\TextType;
use App\Models\CustomFields\RoleParam;
use App\Models\CustomFields\WidthParam;
use Illuminate\Database\Eloquent\Model;
use App\Models\CustomFields\HeightParam;
use App\Models\CustomFields\ParentParam;
use App\Models\CustomFields\ChoicesParam;
use App\Models\CustomFields\NumRowsParam;
use App\Models\CustomFields\WysiwygParam;
use App\Models\CustomFields\MultipleParam;
use App\Models\CustomFields\PostTypeParam;
use App\Models\CustomFields\RepeaterParam;
use App\Models\CustomFields\RequiredParam;
use App\Models\CustomFields\MaxLengthParam;
use App\Models\CustomFields\MinLengthParam;
use App\Models\CustomFields\PermisionParam;
use App\Models\CustomFields\PostFieldParam;
use App\Models\CustomFields\PostObjectParam;
use App\Models\CustomFields\BooelanRuleParam;
use App\Models\CustomFields\PlaceholderParam;
use App\Models\CustomFields\DefaultValueParam;
use App\Models\CustomFields\InstructionsParam;

class CustomField extends Model
{
    protected $fillable = ['order', 'cfg_id', 'title', 'name', 'type', 'intructions', 'params', 'parent_id'];
    
    protected $attributes = [
        'params' => '{}',
    ];

    protected $params = [
        'text' => [
            RequiredParam::class,
            PlaceholderParam::class,
            MinLengthParam::class,
            MaxLengthParam::class,
            TextType::class,
            NumberStep::class
        ],
        'textarea' => [
            RequiredParam::class,
            PlaceholderParam::class,
            MaxLengthParam::class,
            MinLengthParam::class,
            NumRowsParam::class,
            WysiwygParam::class,
        ],
        'date' => [
            RequiredParam::class,
        ],
        'number' => [
            RequiredParam::class,
            DefaultValueParam::class,
            RoleParam::class,
            MinLengthParam::class,
            MaxLengthParam::class,
        ],
        'media' => [
            RequiredParam::class,
            HeightParam::class,
            WidthParam::class,
        ],
        'form' => [
            RequiredParam::class,
        ],
        'multiple_switch' => [
            RequiredParam::class,
            PostTypeParam::class,
            PostFieldParam::class,
        ],
        'gallery' => [
            RequiredParam::class,
            HeightParam::class,
            WidthParam::class,
            InstructionsParam::class,
            MinLengthParam::class,
        ],
        'selection' => [
            RequiredParam::class,
            PostObjectParam::class,
            RoleParam::class,
            MultipleParam::class,
            ChoicesParam::class,
            MaxLengthParam::class,
            MinLengthParam::class,
            ParentParam::class,
            PermisionParam::class,
            InstructionsParam::class,
        ],
        'gmaps' => [
            RequiredParam::class,
        ],
        'boolean' => [
            RoleParam::class,
            BooelanRuleParam::class,
        ],
        'repeater' => [
            MaxLengthParam::class,
            MinLengthParam::class,
            InstructionsParam::class,
        ],
    ];

    public function group() {
        return $this->belongsTo(CustomFieldGroup::class, 'cfg_id', 'id');
    }

    /**
     * Retorna els parametres del Custom Field
     *
     * @return void
     */
    public function getParams() {
        if ( isset($this->type) && !empty($this->params[$this->type]) ) {
            return $this->params[$this->type];
        } else {
            return $this->params;
        }
    }

    /**
     * Es fa servir?
     *
     * @return void
     */
    public function fields() {
        return $this->hasMany(CustomField::class, 'parent_id', 'id')->orderby('order');
    }

}