<?php


namespace App\Models\Presenters;



use App\Models\Traits\CouponTrait;
use App\Support\Presenter;

class CouponPresenter extends Presenter
{

    public function statusString()
    {
        return __(CouponTrait::getStatuses()[$this->status]);
    }


    public function valueCurrency()
    {
        return $this->value . ' €';
    }

    public function maxUses()
    {
        if (is_null($this->total_uses)) {
            return __('Infinite');
        }

        return $this->total_uses;
    }


    public function timesUsed()
    {
        return $this->purchases->count();
    }
}