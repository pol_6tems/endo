<?php

use App\Post;
use App\Language;
use Faker\Factory;
use App\PostTranslation;
use App\Models\CustomPost;
use App\Models\PostMeta;
use Illuminate\Database\Seeder;

class FillPosts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $languages = Language::where('active', 1)->get();

        $custom_posts = collect(['tipo-formacion', 'area-formacion', 'curso']);
    
        foreach($custom_posts as $cp) {
            for($i = 0; $i < 20; $i++) {
                $post = Post::create([
                    'type' => $cp,
                    'status' => 'publish',
                    'author_id' => 1,
                ]);
                
                if ($cp == 'curso') {
                    $tipo = Post::where('type', 'tipo-formacion')->get()->random();
                    $area = Post::where('type', 'area-formacion')->get()->random();
                }

                foreach($languages as $lang) {
                    PostTranslation::create([
                        'post_id' => $post->id,
                        'title' => $faker->sentence,
                        'description' => $faker->text,
                        'post_name' => $faker->slug,
                        'locale' => $lang->code,
                    ]);

                    if ($cp == 'curso') {
                        // Tipus
                        PostMeta::create([
                            'post_id' => $post->id,
                            'custom_field_id' => 1,
                            'value' => $tipo->id,
                            'locale' => $lang->code,
                            'order' => 0,
                        ]);
                        // Area
                        PostMeta::create([
                            'post_id' => $post->id,
                            'custom_field_id' => 2,
                            'value' => $area->id,
                            'locale' => $lang->code,
                            'order' => 0,
                        ]);
                    }
                }
            }
        }
    }
}
