<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Execute all de Seeders of Active Modules
        if ( \Schema::hasTable('modules') ) {
            foreach (glob(app_path('Modules/*'), GLOB_ONLYDIR) as $dirname) {
                $module = basename($dirname);
                foreach (glob($dirname . '/database/seeds/*.php') as $filename) {
                    $class = pathinfo(basename($filename), PATHINFO_FILENAME);
                    $this->call('App\Modules\\' . $module . '\database\seeds\\'.$class);
                }
            }
        }
    }
}
