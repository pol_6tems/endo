<?php

use App\Post;
use App\Language;
use Faker\Factory;
use Faker\Provider\DateTime;
use Illuminate\Database\Seeder;

class FillAgenda extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $languages = Language::where('active', 1)->get();
        
        for($i=0; $i < 40; $i++) {
            $agenda = [
                'type' => 'agenda',
                'status' => 'publish',
                'author_id' => 1,
            ];
            
            foreach($languages as $lang) {
                $title = $faker->sentence();
                $agenda[$lang->code] = [
                    'title' => $title,
                    'description' => $faker->text(1000),
                    'post_name' => str_slug($title),
                ];
            }
            $post = Post::create($agenda);
            
            foreach($languages as $lang) {
                $post->set_field('organitzador', $faker->sentence(), $lang->code);
                $post->set_field('publicacio', $faker->dateTimeBetween("-1 year")->format("d/m/Y"), $lang->code);
                $post->set_field('localitzacio', $faker->sentence(2), $lang->code);
                $post->set_field('url', $faker->url(), $lang->code);
            }
        }
    }
}
