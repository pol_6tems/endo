<?php

use App\Post;
use App\Language;
use Faker\Factory;
use Illuminate\Database\Seeder;

class FillNoticies extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $languages = Language::where('active', 1)->get();
        
        for($i=0; $i < 40; $i++) {
            $agenda = [
                'type' => 'noticia',
                'status' => 'publish',
                'author_id' => 1,
            ];
            
            foreach($languages as $lang) {
                $title = $faker->sentence();
                $agenda[$lang->code] = [
                    'title' => $title,
                    'description' => $faker->text(1000),
                    'post_name' => str_slug($title),
                    'media_id' => 44
                ];
            }
            $post = Post::create($agenda);
            
            foreach($languages as $lang) {
                $post->set_field('miniatura', 45, $lang->code);
                $post->set_field('data-publicacio', $faker->dateTimeBetween("-1 year")->format("d/m/Y"), $lang->code);
                $post->set_field('url', $faker->url(), $lang->code);
                $post->set_field('pdf', 46, $lang->code);
            }
        }
    }
}
