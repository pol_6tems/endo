<?php

use App\Post;
use App\Language;
use Faker\Factory;
use Illuminate\Database\Seeder;

class FillResources extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $languages = Language::where('active', 1)->get();
        
        $recursos = Post::ofType('categoria-recursos')->get();
    
        for($i=0; $i < 40; $i++) {
            $categoria = $recursos->random();
            $agenda = [
                'type' => 'recurs',
                'status' => 'publish',
                'author_id' => 1,
            ];
            
            foreach($languages as $lang) {
                $title = $faker->sentence();
                $agenda[$lang->code] = [
                    'title' => $title,
                    'description' => $faker->text(1000),
                    'post_name' => str_slug($title),
                ];
            }
            $post = Post::create($agenda);
            
            foreach($languages as $lang) {
                $post->set_field('categoria', $categoria->id, $lang->code);
                $post->set_field('direccion', $faker->address(), $lang->code);
                $post->set_field('telefono', $faker->e164PhoneNumber(), $lang->code);
                $post->set_field('email', $faker->email(), $lang->code);
                $post->set_field('web', $faker->url(), $lang->code);
            }
        }
    }
}
