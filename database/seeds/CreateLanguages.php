<?php

use Illuminate\Database\Seeder;

class CreateLanguages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'languages';
        \DB::table($table)->insert(['name' => 'English', 'code' => 'en']);
        \DB::table($table)->insert(['name' => 'Spanish', 'code' => 'es']);
        \DB::table($table)->insert(['name' => 'Catalan', 'code' => 'ca']);
    }
}
