<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParamsCustomPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_posts', function (Blueprint $table) {
            $table->longText('params');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('custom_posts', 'params') ) {
            Schema::table('custom_posts', function (Blueprint $table) {
                $table->dropColumn('params');
            });
        }
    }
}
