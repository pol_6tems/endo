<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPendingIdToPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('posts', 'pending_id') ) {
            Schema::table('posts', function (Blueprint $table) {
                $table->integer('pending_id')->unsigned()->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('posts', 'pending_id') ) {
            Schema::table('posts', function (Blueprint $table) {
                $table->dropColumn('pending_id');
            });
        }
    }
}
