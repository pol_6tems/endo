<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParametersMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('admin_menus', 'parameters') ) {
            Schema::table('admin_menus', function (Blueprint $table) {
                $table->string('parameters')->nullable();
            });
        }
        if ( !Schema::hasColumn('admin_menus_sub', 'parameters') ) {
            Schema::table('admin_menus_sub', function (Blueprint $table) {
                $table->string('parameters')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('admin_menus', 'parameters') ) {
            Schema::table('admin_menus', function (Blueprint $table) {
                $table->dropColumn('parameters');
            });
        }
        if ( Schema::hasColumn('admin_menus_sub', 'parameters') ) {
            Schema::table('admin_menus_sub', function (Blueprint $table) {
                $table->dropColumn('parameters');
            });
        }
    }
}
