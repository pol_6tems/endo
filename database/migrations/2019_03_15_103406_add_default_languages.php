<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('languages', 'default') ) {
            Schema::table('languages', function (Blueprint $table) {
                $table->boolean('default')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('languages', 'default') ) {
            Schema::table('languages', function (Blueprint $table) {
                $table->dropColumn('default');
            });
        }
    }
}
