<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddButtonsToChats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('chats', 'buttons') ) {
            Schema::table('chats', function (Blueprint $table) {
                $table->text('buttons')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('chats', 'buttons') ) {
            Schema::table('chats', function (Blueprint $table) {
                $table->dropColumn('buttons');
            });
        }
    }
}
