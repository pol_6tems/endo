<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyExchangeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('currency_exchange_rates') ) {
            Schema::create('currency_exchange_rates', function (Blueprint $table) {
                $table->increments('id');
                $table->date('date');
                $table->integer('currency_id')->unsigned();
                $table->string('rate');

                $table->timestamps();

                $table->unique(['date', 'currency_id']);
                
                $table->foreign('currency_id')
                    ->references('id')->on('currencies');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_exchange_rates');
    }
}
