<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParamsMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('media', 'params') ) {
            Schema::table('media', function (Blueprint $table) {
                $table->longtext('params')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('media', 'params') ) {
            Schema::table('media', function (Blueprint $table) {
                $table->dropColumn('params');
            });
        }
    }
}
