<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToToMensaje extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('mensajes', 'to') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->integer('to')->unsigned()->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('mensajes', 'to') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->dropColumn('to');
            });
        }
    }
}
