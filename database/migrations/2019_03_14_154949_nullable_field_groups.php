<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableFieldGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_field_groups', function (Blueprint $table) {
            $table->string('title')->nullable()->change();
            $table->string('post_type')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('custom_field_groups', 'title') ) {
            Schema::table('custom_field_groups', function (Blueprint $table) {
                $table->dropColumn('title');
            });
        }

        if ( Schema::hasColumn('custom_field_groups', 'post_type') ) {
            Schema::table('custom_field_groups', function (Blueprint $table) {
                $table->dropColumn('post_type');
            });
        }
    }
}
