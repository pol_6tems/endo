<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('custom_post_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('custom_post_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('title_plural')->nullable();
            $table->string('post_type')->nullable();
            $table->string('post_type_plural')->nullable();
            $table->string('locale')->index();

            $table->unique(['custom_post_id','locale']);
            $table->foreign('custom_post_id')->references('id')->on('custom_posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_post_translations');
        Schema::dropIfExists('custom_posts');
    }
}
