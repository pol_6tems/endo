<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FileAlterVarcharConfiguracions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( Schema::hasColumn('configuracions', 'file') ) {
            \DB::statement("UPDATE `configuracions` SET file = NULL");
            \DB::statement("ALTER TABLE `configuracions` CHANGE `file` `file` VARCHAR(255) NULL DEFAULT NULL");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('configuracions', 'file') ) {
            \DB::statement("ALTER TABLE `configuracions` CHANGE `file` `file` LONGBLOB NULL DEFAULT NULL;");
        }
    }
}
