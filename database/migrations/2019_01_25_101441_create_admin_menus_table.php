<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('icon')->nullable();
            $table->boolean('has_url')->default(false);
            $table->string('url_type')->nullable();
            $table->string('url')->nullable();
            $table->string('controller')->nullable();
            $table->integer('order')->unsigned()->default(0);
            $table->string('parameters')->nullable();
            $table->timestamps();
        });
        
        Schema::create('admin_menus_sub', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->string('name');
            $table->string('icon')->nullable();
            $table->string('url_type');
            $table->string('url');
            $table->string('controller')->nullable();
            $table->integer('order')->unsigned()->default(0);
            $table->string('parameters')->nullable();
            $table->timestamps();
        });

        \DB::table('admin_menus')->insert([ 'id' => 1, 'name' => 'Administration', 'icon' => 'build', 'order' => 999 ]);
        \DB::table('admin_menus')->insert([ 'id' => 2, 'name' => 'Content', 'icon' => 'library_books', 'order' => 970 ]);
        \DB::table('admin_menus')->insert([ 'id' => 3, 'name' => 'Media', 'icon' => 'perm_media', 'order' => 960, 'has_url' => true, 'url_type' => 'route', 'url' => 'admin.media.index', 'controller' => 'Admin\MediaController' ]);
        \DB::table('admin_menus')->insert([ 'id' => 4, 'name' => 'Dashboard', 'icon' => 'dashboard', 'order' => 0, 'has_url' => true, 'url_type' => 'route', 'url' => 'admin', 'controller' => 'Admin\AdminController' ]);

        \DB::table('admin_menus_sub')->insert([ 'id' => 1, 'menu_id' => 1, 'name' => 'Configuration', 'order' => 0, 'url_type' => 'route', 'url' => 'admin.configuracion.index', 'controller' => 'Admin\ConfiguracionController', 'icon' => 'tune' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 2, 'menu_id' => 1, 'name' => 'Themes', 'order' => 1, 'url_type' => 'route', 'url' => 'admin.themes.index', 'controller' => 'Admin\ThemesController', 'icon' => 'color_lens' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 3, 'menu_id' => 1, 'name' => 'Modules', 'order' => 2, 'url_type' => 'route', 'url' => 'admin.modules.index', 'controller' => 'Admin\ModulesController', 'icon' => 'extension' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 4, 'menu_id' => 1, 'name' => 'Menus', 'order' => 10, 'url_type' => 'route', 'url' => 'admin.menus.index', 'controller' => 'Admin\AdminMenusController', 'icon' => 'menu' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 5, 'menu_id' => 1, 'name' => 'Languages', 'order' => 20, 'url_type' => 'route', 'url' => 'idiomas.index', 'controller' => 'Admin\AdminIdiomasController', 'icon' => 'language' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 6, 'menu_id' => 1, 'name' => 'Translations', 'order' => 30, 'url_type' => 'route', 'url' => 'translations', 'controller' => 'LanguageTranslationController', 'icon' => 'g_translate' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 7, 'menu_id' => 1, 'name' => 'Roles', 'order' => 40, 'url_type' => 'route', 'url' => 'admin.rols.index', 'controller' => 'Admin\RolsController', 'icon' => 'supervisor_account' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 8, 'menu_id' => 1, 'name' => 'Users', 'order' => 50, 'url_type' => 'route', 'url' => 'admin.users.index', 'controller' => 'Admin\UsersController', 'icon' => 'person_pin' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 9, 'menu_id' => 1, 'name' => 'Options', 'order' => 60, 'url_type' => 'route', 'url' => 'admin.opciones.index', 'controller' => 'Admin\OpcionesController', 'icon' => 'ballot' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 10, 'menu_id' => 1, 'name' => 'Custom Posts', 'order' => 70, 'url_type' => 'route', 'url' => 'admin.custom_posts.index', 'controller' => 'Admin\CustomPostsController', 'icon' => 'archive' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 11, 'menu_id' => 1, 'name' => 'Custom Fields', 'order' => 80, 'url_type' => 'route', 'url' => 'admin.custom_fields.index', 'controller' => 'Admin\CustomFieldsController', 'icon' => 'library_add' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 12, 'menu_id' => 1, 'name' => 'Notifications', 'order' => 90, 'url_type' => 'route', 'url' => 'admin.notifications.index', 'controller' => 'Admin\NotificationsController', 'icon' => 'notifications' ]);
        
        \DB::table('admin_menus_sub')->insert([ 'id' => 13, 'menu_id' => 2, 'name' => 'Posts', 'order' => 10, 'url_type' => 'route', 'url' => 'admin.posts.index', 'controller' => 'Admin\PostsController', 'icon' => 'archive' ]);
        \DB::table('admin_menus_sub')->insert([ 'id' => 14, 'menu_id' => 2, 'name' => 'Pages', 'order' => 20, 'url_type' => 'route', 'url' => 'admin.posts.index', 'controller' => 'Admin\PostsController', 'parameters' => 'post_type=page', 'icon' => 'library_books' ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_menus_sub');
        Schema::dropIfExists('admin_menus');
    }
}
