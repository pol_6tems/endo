<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned()->index('post_meta_id');
            $table->integer('custom_field_id')->unsigned()->index('post_meta_cf_id');
            $table->longText('value')->nullable();
            $table->binary('file')->nullable();
            $table->string('locale');
            $table->integer('parent_id')->unsigned()->nullable()->index('post_meta_parent_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_meta');
    }
}
