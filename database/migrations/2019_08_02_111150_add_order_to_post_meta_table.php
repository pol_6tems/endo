<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderToPostMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('post_meta', 'order') ) {
            Schema::table('post_meta', function (Blueprint $table) {
                $table->integer('order')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('post_meta', 'field_name')) {
            Schema::table('post_meta', function (Blueprint $table) {
                $table->dropColumn('order');
            });
        }
    }
}
