<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultiSelCustomFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_fields', function (Blueprint $table) {
            $table->string('post_type')->nullable();
            $table->string('post_field')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // És possible que això ja no ho necessitem
        Schema::table('custom_fields', function (Blueprint $table) {
            $table->dropColumn('post_type');
            $table->dropColumn('post_field');
        });
    }
}
