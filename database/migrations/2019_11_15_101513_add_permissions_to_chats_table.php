<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionsToChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('chats', 'permissions')) {
            Schema::table('chats', function (Blueprint $table) {
                $table->longText('permissions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('chats', 'permissions')) {
            Schema::table('chats', function (Blueprint $table) {
                $table->dropColumn('permissions');
            });
        }
    }
}
