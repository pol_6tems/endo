<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('role')->default('user');
            $table->string('avatar')->nullable()->default('user-endo.jpg');
            $table->rememberToken();
            $table->timestamps();
        });
        \DB::table('users')->insert([
            'name' => 'suport',
            'lastname' => '6TEMS',
            'email' => 'pol@6tems.com',
            'password' => '$2y$10$/naWT4.gfnmgy2sVRAgp/uX0pysltaMvOjn9zxjka8FcnHGwNNA0m',
            'role' => 'admin',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
