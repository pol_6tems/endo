<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('subject');
            $table->string('greeting')->nullable();
            $table->text('lines')->nullable();
            $table->string('salutation')->nullable();
            $table->timestamps();
        });
        if ( Schema::hasTable('admin_menus_sub') ) {
            \DB::table('admin_menus_sub')->insert([ 'menu_id' => 1, 'name' => 'Emails', 'order' => 90, 'url_type' => 'route', 'url' => 'admin.emails.index', 'controller' => 'Admin\EmailsController', 'icon' => 'email' ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
        if ( Schema::hasTable('admin_menus_sub') ) {
            \DB::table('admin_menus_sub')->where('controller', '=', 'Admin\EmailsController')->delete();
        }
    }
}
