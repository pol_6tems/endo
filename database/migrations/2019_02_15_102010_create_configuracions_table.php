<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuracions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->unique();
            $table->longText('value')->nullable();
            $table->string('type')->default('text');
            $table->binary('file')->nullable();
            $table->string('group')->default('general');
            $table->string('title');
            $table->integer('order')->nullable();
            $table->timestamps();
        });

        \DB::statement("ALTER TABLE configuracions MODIFY file LONGBLOB");

        \DB::table('configuracions')->insert([ 'title' => 'Application name', 'key' => 'app_name', 'order' => 10, 'type' => 'text', 'group' => 'general', 'value' => 'Endo' ]);
        \DB::table('configuracions')->insert([ 'title' => 'Application icon', 'key' => 'app_icon', 'order' => 20, 'type' => 'img', 'group' => 'general' ]);
        \DB::table('configuracions')->insert([ 'title' => 'Sidebar image', 'key' => 'img_sidebar', 'order' => 30, 'type' => 'img', 'group' => 'general' ]);
        \DB::table('configuracions')->insert([ 'title' => 'Color', 'key' => 'color', 'order' => 40, 'type' => 'color', 'group' => 'general', 'value' => '131' ]);
        \DB::table('configuracions')->insert([ 'title' => 'Email image', 'key' => 'img_email', 'order' => 50, 'type' => 'img', 'group' => 'general' ]);
        \DB::table('configuracions')->insert([ 'title' => 'Home page', 'key' => 'home_page', 'order' => 0, 'type' => 'page', 'group' => 'general' ]);
        \DB::table('configuracions')->insert([ 'title' => 'Show Admin Bar', 'key' => 'show_admin_bar', 'order' => 0, 'type' => 'true_false', 'group' => 'general', 'value' => true ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuracions');
    }
}
