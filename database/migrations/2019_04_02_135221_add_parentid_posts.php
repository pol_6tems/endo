<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentidPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('posts', 'parent_id') ) {
            Schema::table('posts', function (Blueprint $table) {
                $table->integer('parent_id')->unsigned();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('posts', 'parent_id') ) {
            Schema::table('posts', function (Blueprint $table) {
                $table->dropColumn('parent_id');
            });
        }
    }
}
