<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('coupons') ) {
            Schema::create('coupons', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code')->unique();
                $table->integer('value');
                $table->tinyInteger('status');
                $table->integer('total_uses')->nullable();
                $table->integer('uses_per_user')->nullable();
                $table->date('min_date')->nullable();
                $table->date('max_date')->nullable();
                $table->integer('min_purchase_amount')->nullable();
                $table->integer('owner_id')->unsigned()->nullable();

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
