<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('path');
            $table->binary('thumbnail')->nullable();
            $table->boolean('active')->default(false);
            $table->boolean('admin')->default(false);
            $table->timestamps();
        });

        \DB::statement("ALTER TABLE themes MODIFY thumbnail LONGBLOB");

        \DB::table('themes')->insert([ 'name' => 'Admin', 'path' => 'Admin', 'active' => true, 'admin' => true ]);
        \DB::table('themes')->insert([ 'name' => 'Endo', 'path' => 'Endo', 'active' => true, 'admin' => false ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('themes');
    }
}
