<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIconSubmenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('admin_menus_sub', 'icon') ) {
            Schema::table('admin_menus_sub', function (Blueprint $table) {
                $table->string('icon')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('admin_menus_sub', 'icon') ) {
            Schema::table('admin_menus_sub', function (Blueprint $table) {
                $table->dropColumn('icon');
            });
        }
    }
}
