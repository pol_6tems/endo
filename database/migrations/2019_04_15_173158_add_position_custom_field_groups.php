<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionCustomFieldGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('custom_field_groups', 'position') ) {
            Schema::table('custom_field_groups', function (Blueprint $table) {
                $table->string('position')->default('bottom');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('custom_field_groups', 'position') ) {
            Schema::table('custom_field_groups', function (Blueprint $table) {
                $table->dropColumn('position');
            });
        }
    }
}
