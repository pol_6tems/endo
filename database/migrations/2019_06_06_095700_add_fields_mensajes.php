<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsMensajes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( Schema::hasColumn('mensajes', 'user_id') && Schema::hasColumn('mensajes', 'from') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->dropColumn('user_id');
            });
            \DB::statement("ALTER TABLE `mensajes` CHANGE `from` `user_id` INT(10) UNSIGNED NOT NULL");
        }
        
        if ( !Schema::hasColumn('mensajes', 'email') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->string('email')->nullable();
            });
        }
        if ( !Schema::hasColumn('mensajes', 'name') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->string('name')->nullable();
            });
        }
        if ( !Schema::hasColumn('mensajes', 'type') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->string('type')->default('chat');
            });
        }
        if ( !Schema::hasColumn('mensajes', 'chat') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->string('chat')->nullable();
            });
        }
        if ( !Schema::hasColumn('mensajes', 'params') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->text('params')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('mensajes', 'email') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->dropColumn('email');
            });
        }
        if ( Schema::hasColumn('mensajes', 'name') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->dropColumn('name');
            });
        }
        if ( Schema::hasColumn('mensajes', 'type') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->dropColumn('type');
            });
        }
        if ( Schema::hasColumn('mensajes', 'chat') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->dropColumn('chat');
            });
        }
        if ( Schema::hasColumn('mensajes', 'params') ) {
            Schema::table('mensajes', function (Blueprint $table) {
                $table->dropColumn('params');
            });
        }
    }
}
