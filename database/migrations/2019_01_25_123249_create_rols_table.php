<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rols', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('level')->unsigned()->default(0);
            $table->longText('permisos')->nullable();
            $table->timestamps();
        });
        \DB::table('rols')->insert([ 'name' => 'admin', 'level' => 99, ]);
        \DB::table('rols')->insert([ 'name' => 'subadmin', 'level' => 90, ]);
        \DB::table('rols')->insert([ 'name' => 'editor', 'level' => 2, ]);
        \DB::table('rols')->insert([ 'name' => 'user', 'level' => 1, ]);
        \DB::table('rols')->insert([ 'name' => 'guest', 'level' => 0, ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rols');
    }
}
