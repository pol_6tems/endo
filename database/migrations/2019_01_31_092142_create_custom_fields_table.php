<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_field_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('post_type');
            $table->string('template')->nullable();
            $table->integer('order')->unsigned()->default(0);
            $table->timestamps();
        });

        Schema::create('custom_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->unsigned()->default(0);
            $table->integer('cfg_id')->unsigned();
            $table->string('title');
            $table->string('name');
            $table->string('type');
            $table->string('instructions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field_groups');
        Schema::dropIfExists('custom_fields');
    }
}
