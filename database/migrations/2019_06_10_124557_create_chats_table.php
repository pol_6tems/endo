<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->integer('email_id')->unsigned();
            $table->timestamps();
        });

        if ( Schema::hasTable('admin_menus_sub') ) {
            \DB::table('admin_menus_sub')->insert([ 'menu_id' => 1, 'name' => 'Chats', 'order' => 90, 'url_type' => 'route', 'url' => 'admin.chats.index', 'controller' => 'Admin\ChatsController', 'icon' => 'chat_bubble' ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
        if ( Schema::hasTable('admin_menus_sub') ) {
            \DB::table('admin_menus_sub')->where('controller', '=', 'Admin\ChatsController')->delete();
        }
    }
}
