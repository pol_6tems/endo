<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAuthoridPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasColumn('posts', 'author_id') ) {
            Schema::table('posts', function (Blueprint $table) {
                $table->integer('author_id')->unsigned();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if ( Schema::hasColumn('posts', 'author_id') ) {
            Schema::table('posts', function (Blueprint $table) {
                $table->dropColumn('author_id');
            });
        }
    }
}
