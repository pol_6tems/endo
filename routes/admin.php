<?php

// AdminController
Route::get('/', 'Admin\AdminController@index')->name('admin');

// Themes
include('admin/themes.php');

// Modules
include('admin/modules.php');

// Posts
include('admin/content.php');

// Custom Posts & Custom Fields
include('admin/custom_posts.php');

// Mensajes
Route::post('mensajes/visto', 'Admin\MensajesController@visto')->name('admin.mensajes.visto');
Route::resource('mensajes', "Admin\MensajesController", ['as' => 'admin']);

// Configuracion
Route::get('configuracion', "Admin\ConfiguracionController@index")->name('admin.configuracion.index');
Route::post('configuracion', "Admin\ConfiguracionController@store")->name('admin.configuracion.store');
Route::get('configuracions', "Admin\ConfiguracionController@index")->name('admin.configuracions.index');
Route::post('configuracions', "Admin\ConfiguracionController@store")->name('admin.configuracions.store');

// Media
Route::post('upload', 'Admin\MediaController@upload')->name('admin.media.upload');
Route::post('crop', 'Admin\MediaController@crop')->name('admin.media.crop');
Route::post('media/get_all', "Admin\MediaController@get_all")->name('admin.media.get_all');
Route::post('media/mass_destroy', "Admin\MediaController@massDestroy")->name('admin.media.mass_destroy');
Route::resource('media', "Admin\MediaController", ['as' => 'admin']);


// Cache
Route::get('cache-clear', "Admin\AdminController@clearCache")->name('admin.cache.clear');

// UserMeta
Route::resource('user_meta', "Admin\UserMetaController", ['as' => 'admin']);

// Notifications
Route::put("notifications/{id}/duplicate", "Admin\NotificationsController@duplicate")->name("admin.notifications.duplicate");
Route::post('notifications/active', 'Admin\NotificationsController@active')->name('admin.notifications.active');
Route::resource('notifications', "Admin\NotificationsController", ['as' => 'admin']);

include('admin/administracion.php');

Route::get('search', 'Admin\SearchController@index')->name('admin.search.index');

// Emails
Route::put("emails/{id}/duplicate", "Admin\EmailsController@duplicate")->name("admin.emails.duplicate");
Route::resource('emails', "Admin\EmailsController", ['as' => 'admin']);
// Chats
Route::resource('chats', "Admin\ChatsController", ['as' => 'admin']);
Route::post('chats/{id}/change-permission', 'Admin\ChatsController@changePermission')->name('chats.change-permission');

Route::resource('coupons', "Admin\CouponsController", ['as' => 'admin']);