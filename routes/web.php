<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::pattern('index', 'index|inici|inicio');

Route::get('/robots.txt', function () {
    if (strpos(request()->root(), '6tems.es') !== false ||
        strpos(request()->root(), '6tems.com') !== false) {
        $robotsFile = public_path('endo_robots_disallow.txt');
    } else {
        $robotsFile = public_path('endo_robots.txt');

        $frontRobotsFile = public_path(config('front_theme') . 'robots.txt');
        if (file_exists($frontRobotsFile)) {
            $robotsFile = $frontRobotsFile;
        }
    }

    return response()->file($robotsFile);
});

Route::get('/sitemap.xml', function () {
    $frontSitemapFile = public_path(config('front_theme') . 'sitemap.xml');
    if (file_exists($frontSitemapFile)) {
        return response()->file($frontSitemapFile);
    }
    return response( '',404);
});

Route::prefix('{locale}')->group(function () {
    if (\Illuminate\Support\Facades\Schema::hasTable('configuracions')) {
        $home_page = \App\Models\Configuracion::get_config('home_page');
        if (!empty($home_page)) {
            Route::get('/', function () use ($home_page) {
                return app(\App\Http\Controllers\PostsController::class)->callAction('show', ['item' => $home_page]);
            });
        }
    }

    // Auth
    Auth::routes();

    // Home
    Route::get('/{index?}', 'HomeController@index')->name('index');

    // ADMIN
    Route::group(['middleware' => ['rols', 'auth'], 'prefix' => 'admin'], function () {
        include('admin.php');
    });

    Route::any('ajax', 'AjaxController@index')->name('ajax'); 
});

Route::fallback('PostsController@process_routes');

