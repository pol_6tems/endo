<?php

/* Translations */
Route::post('modules/{id}/translations/update', 'Admin\ModulesController@transUpdate')->name('admin.modules.translations.update.json');
Route::post('modules/{id}/translations/updateKey', 'Admin\ModulesController@transUpdateKey')->name('admin.modules.translations.update.json.key');
Route::delete('modules/{id}/translations/destroy/{key}', 'Admin\ModulesController@transDestroy')->name('admin.modules.translations.destroy');
Route::post('modules/{id}/translations/create', 'Admin\ModulesController@transStore')->name('admin.modules.translations.create');
/* end Translations */

Route::post('modules/reset', 'Admin\ModulesController@reset')->name('admin.modules.reset');
Route::get('modules/refresh', 'Admin\ModulesController@refresh')->name('admin.modules.refresh');
Route::post('modules/active', 'Admin\ModulesController@active')->name('admin.modules.active');
Route::resource('modules', "Admin\ModulesController", ['as' => 'admin']);