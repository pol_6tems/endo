<?php

/* Translations */
Route::post('themes/{id}/translations/update', 'Admin\ThemesController@transUpdate')->name('admin.themes.translations.update.json');
Route::post('themes/{id}/translations/updateKey', 'Admin\ThemesController@transUpdateKey')->name('admin.themes.translations.update.json.key');
Route::delete('themes/{id}/translations/destroy/{key}', 'Admin\ThemesController@transDestroy')->name('admin.themes.translations.destroy');
Route::post('themes/{id}/translations/create', 'Admin\ThemesController@transStore')->name('admin.themes.translations.create');
/* end Translations */

Route::get('themes/refresh', 'Admin\ThemesController@refresh')->name('admin.themes.refresh');
Route::post('themes/active_admin', 'Admin\ThemesController@active_admin')->name('admin.themes.active_admin');
Route::post('themes/active', 'Admin\ThemesController@active')->name('admin.themes.active');
Route::resource('themes', "Admin\ThemesController", ['as' => 'admin']);