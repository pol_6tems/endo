<?php

Route::get('get_posts/{post_type}', 'Admin\PostsController@get_posts')->name('admin.getposts');

// Media
Route::get('images', 'Admin\MediaController@getImages')->name('admin.get.images');
Route::post('updateImage/{id}', 'Admin\MediaController@updateImage')->name('admin.update.image');

// Posts
Route::put("posts/{post}/copy_content", "Admin\PostsController@copy_content")->name("admin.posts.copy_content");
Route::put("posts/{id}/aprove_pending", "Admin\PostsController@aprove_pending")->name("admin.posts.aprove_pending");
Route::put("posts/{id}/duplicate", "Admin\PostsController@duplicate")->name("admin.posts.duplicate");
Route::put("posts/{id}/restore", "Admin\PostsController@restore")->name("admin.posts.restore");
Route::delete("posts/{id}/destroy_permanent", "Admin\PostsController@destroy_permanent")->name("admin.posts.destroy_permanent");
Route::delete("posts/mass_destroy", "Admin\PostsController@massDestroy")->name("admin.posts.mass_destroy");
Route::resource("posts", "Admin\PostsController", ["as" => "admin", 'except' => ['show']]);

// Pages
Route::put("pages/{id}/restore", "Admin\PagesController@restore")->name("admin.pages.restore");
Route::delete("pages/{id}/destroy_permanent", "Admin\PagesController@destroy_permanent")->name("admin.pages.destroy_permanent");
Route::delete("pages/mass_destroy", "Admin\PagesController@massDestroy")->name("admin.pages.mass_destroy");
Route::resource("pages", "Admin\PagesController", ["as" => "admin"]);