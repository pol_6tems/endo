<?php
// Users
Route::resource('users', 'Admin\UsersController', ['as' => 'admin']);

// Rols
Route::resource('rols', 'Admin\RolsController', ['as' => 'admin']);

// Menus
Route::resource('admin_menus', 'Admin\AdminMenusController', ['as' => 'admin']);
Route::resource('menus', 'Admin\AdminMenusController', ['as' => 'admin']);

// Experimental
// Route::resource('menus2', 'Admin\AdminMenusController2', ['as' => 'admin']);

// Menus - Submenus
Route::get('menus/subs/create/{menu_id}', 'Admin\AdminMenusController@submenu_create')->name('admin.menus.subs.create');
Route::post('menus/subs/store', 'Admin\AdminMenusController@submenu_store')->name('admin.menus.subs.store');
Route::get('menus/subs/edit/{id}', 'Admin\AdminMenusController@submenu_edit')->name('admin.menus.subs.edit');
Route::post('menus/subs/update/{id}', 'Admin\AdminMenusController@submenu_update')->name('admin.menus.subs.update');
Route::delete('menus/subs/destroy/{id}', 'Admin\AdminMenusController@submenu_destroy')->name('admin.menus.subs.destroy');

/* Idiomas */
Route::post('idiomas/switch_default', 'Admin\AdminIdiomasController@switch_default')->name('idiomas.switch_default');
Route::post('idiomas/switch_active', 'Admin\AdminIdiomasController@switch_active')->name('idiomas.switch_active');
Route::resource('idiomas', 'Admin\AdminIdiomasController');

/* Translations */
Route::get('translations', 'LanguageTranslationController@index')->name('translations');
Route::post('translations/update', 'LanguageTranslationController@transUpdate')->name('translation.update.json');
Route::post('translations/updateKey', 'LanguageTranslationController@transUpdateKey')->name('translation.update.json.key');
Route::delete('translations/destroy/{key}', 'LanguageTranslationController@destroy')->name('translations.destroy');
Route::post('translations/create', 'LanguageTranslationController@store')->name('translations.create');

/* Opciones */
Route::post('opciones/updateValue', 'Admin\OpcionesController@updateValue')->name('admin.opciones.update_value');
Route::post('opciones/updateKey', 'Admin\OpcionesController@updateKey')->name('admin.opciones.update_key');
Route::post('opciones/updateDesc', 'Admin\OpcionesController@updateDesc')->name('admin.opciones.update_desc');
Route::resource('opciones', 'Admin\OpcionesController', ['as' => 'admin']);