<?php

// Custom Posts
Route::delete("custom_posts/mass_destroy", "Admin\CustomPostsController@massDestroy")->name("admin.custom_posts.mass_destroy");
Route::resource('custom_posts', "Admin\CustomPostsController", ['as' => 'admin']);

// Custom Fields
Route::delete("custom_fields/mass_destroy", "Admin\CustomFieldsController@massDestroy")->name("admin.custom_fields.mass_destroy");
Route::put("custom_fields/{id}/duplicate", "Admin\CustomFieldsController@duplicate")->name("admin.custom_fields.duplicate");
Route::resource('custom_fields', "Admin\CustomFieldsController", ['as' => 'admin']);
Route::get('custom_field_params', "Admin\CustomFieldsController@get_field_params")->name("admin.custom_fields.get_field_params");
// Route::get("custom_fields/reorder", "Admin\CustomFieldsController@reorder")->name("admin.custom_fields.reorder");